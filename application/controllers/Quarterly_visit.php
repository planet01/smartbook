<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quarterly_visit extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->view_product = 'Quarterly_visit/view';
        // page active
        $this->view_page_product = "view-quarterly_visit";
        // table
        $this->table = "quarterly_visit";
        $this->table_fid = "quarterly_visit_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/warehouse";
        // Page Heading
        $this->page_heading = "Quarterly Visit";
		
        $this->no_results_found = "user/no_results_found";
        
		require_once APPPATH .'third_party/FileUploader.php';

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 29) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        if ($this->check_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $res['cityData'] = $this->basic_model->getCustomRows("SELECT * FROM city");
            $this->template_view($this->view_page_product,$res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function fetch_invoice()
    {
        extract($_POST);
        if($this->input->post())
        {
            $invoice_type; //1 => ALL
            $res['setting'] = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            if($invoice_type == 1) //All
            {
                $where = "";
                if($end_user != 0)
                {
                    $where .= " AND inv.end_user= '".$end_user."'";
                }
                if($city_name != '0')
                {
                    $where .= " AND LOWER(inv.invoice_installation_location_details) = LOWER('".$city_name."') ";
                }
                $query = "SELECT * FROM `invoice` inv LEFT JOIN end_user ON inv.end_user = end_user.end_user_id LEFT JOIN city ON end_user.end_user_location = city.city_id LEFT JOIN user ON inv.customer_id = user.user_id where inv.customer_id=".$customer_id.$where." group by inv.invoice_id LIMIT 100";
            }else if($invoice_type == 0)
            {
                $where = "";
                if($due_since != '')
                {
                    
                    $where .= " AND STR_TO_DATE(inv.last_service_date,'%Y-%m-%d') <= '".$due_since."' ";
                    $where .= "OR inv.last_service_date IS NULL ";
                    
                }

                if($end_user != 0)
                {
                    $where .= " AND inv.end_user= '".$end_user."'";
                }
                if($city_name != '0')
                {
                    $where .= " AND LOWER(inv.invoice_installation_location_details) = LOWER('".$city_name."') ";
                }
                $query = "SELECT * FROM `invoice` inv LEFT JOIN end_user ON inv.end_user = end_user.end_user_id LEFT JOIN city ON end_user.end_user_location = city.city_id LEFT JOIN user ON inv.customer_id = user.user_id where inv.customer_id=".$customer_id.$where." group by inv.invoice_id LIMIT 100";
            }
            
            $res['data'] = $this->basic_model->getCustomRows($query);
            $res['base_url'] = base_url();
            // print_b($where);
            echo json_encode($res);
        }
    }

    function add()
    {
        extract($_POST);
        $data = array(
            'invoice_id' => @$invoice_id,
            'service_date' => @$service_date,
            'service_type' => @$service_type,
            'service_detail' => @$service_detail
        );
        $this->basic_model->insertRecord($data, $this->table);

        $data = array(
            'due_since' => $due_since_search,
            'customer_id' => $customer_id_search,
            'invoice_type' => $invoice_type_search,
        );
        echo json_encode(array( "success" => "Record updated successfully", 'fieldsEmpty' => 'yes','quarterly_add' => 'yes','data' => $data));
    }

    function print()
    {
        extract($_GET);
        if ($this->check_print) {
            
            
            // $res['setting'] = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            // $query = "SELECT * FROM `quarterly_visit` LEFT JOIN invoice ON invoice.invoice_id = quarterly_visit.invoice_id WHERE (STR_TO_DATE(quarterly_visit.service_date,'%Y-%m-%d') between STR_TO_DATE('$from_date','%Y-%m-%d') and STR_TO_DATE('$to_date','%Y-%m-%d'))";
            // $res['from_date'] = $from_date;
            // $res['to_date'] = $to_date;
            // $res['data'] = $this->basic_model->getCustomRows($query);

            $res['setting'] = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            $query = "SELECT *,(SELECT ts.status FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = t.ticket_id) = ts.ticket_status_id AND ts.ticket_id = t.ticket_id) as TicketStatus FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user WHERE (STR_TO_DATE(t.created_date,'%Y-%m-%d') between STR_TO_DATE('$from_date','%Y-%m-%d') and STR_TO_DATE('$to_date','%Y-%m-%d')) ";
            if($ticket_type != 00)
            {
                $query .= " AND t.ticket_type=".$ticket_type;
            }
            $res['statusData'] = $this->basic_model->getCustomRows("Select * From ticket_status ts left join product p ON ts.service_id = p.product_id order by ticket_status_id ASC");
            $res['from_date'] = $from_date;
            $res['to_date'] = $to_date;
            $res['statuses'] = ProjectStatusData();
            $res['data'] = $this->basic_model->getCustomRows($query);
            $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
            $this->basic_model->make_pdf(1,$res,'Report','ticket_history_report');
            $this->basic_model->make_pdf(1,$res,'Report','quarterly_visit_report');
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail($invoice_type,$date,$id){
        
        if( isset($id) && $id != ''){
            if($invoice_type == 0)
            {
                $query = "SELECT * FROM  quarterly_visit where invoice_id = '".$id."' ";
            }
            else
            {
                if($date != 0)
                {
                    $query = "SELECT * FROM  quarterly_visit where invoice_id = '".$id."' AND STR_TO_DATE(service_date,'%Y-%m-%d') >= STR_TO_DATE('".$date."','%Y-%m-%d')";
                }else
                {
                    $query = "SELECT * FROM  quarterly_visit where invoice_id = '".$id."'";
                }
                
            }
            $detail = $this->basic_model->getCustomRows($query);

            if($detail){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." History";

                $outputDescription = "";
                
                $count = 0;
                foreach ($detail as $k => $v) {
                    $count ++;
                    $outputDescription .= "<p>".$count.". Service Date : ".$v['service_date']."</p>";
                    $outputDescription .= "<p>Service Type : ".(($v['service_type'] == '1')?"Onsite":"Online")."</p>";
                    $outputDescription .= "<p>Detail: ".$v['service_detail']."</p>";
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => $this->page_heading.' Not Found '));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function ticket_detail($id){
        
        if( isset($id) && $id != ''){
            $query = "SELECT *,(SELECT ts.status FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = t.ticket_id) = ts.ticket_status_id AND ts.ticket_id = t.ticket_id) as TicketStatus,(SELECT ts.service_date FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = t.ticket_id) = ts.ticket_status_id AND ts.ticket_id = t.ticket_id) as last_service_date FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user left JOIN ticket_type tt ON tt.ticket_type_id = t.ticket_type WHERE t.invoice_id = ".$id;
            $detail = $this->basic_model->getCustomRows($query);
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            
            if($detail){
                $outputTitle = "";
                $outputTitle .= "Ticket Status";

                $outputDescription = "";
                
                $count = 0;
                foreach ($detail as $k => $v) {
                    $count ++;
                    $outputDescription .= "<p>".$count.". Ticket No : ". $setval['company_prefix'].$setval['ticket_prfx'].$v['ticket_no']."</p>";
                    $outputDescription .= "<p>Ticket Date : ".$v['ticket_date']."</p>";
                    $outputDescription .= "<p>Last Service Date : ".$v['last_service_date']."</p>";
                    $outputDescription .= "<p>Status: ";
                    if($v['status'] == 1)
                    {
                        if($v['TicketStatus'] == 0)
                        {
                            $outputDescription .= 'Pending';
                        }else if($v['TicketStatus'] == 1)
                        {
                            $outputDescription .= 'Partially';
                        }else if($v['TicketStatus'] == 2)
                        {
                            $outputDescription .= 'Completed';
                        }else if($v['TicketStatus'] == 3)
                        {
                            $outputDescription .= 'Pending By Client';
                        }else 
                        {
                            $outputDescription .= 'Not Initiated';
                        }
                    }else if($v['status'] == 2)
                    {
                        $outputDescription .= 'Accepted';
                    }else if($v['status'] == 3)
                    {
                        $outputDescription .= 'Rejected';
                    }
                    $outputDescription .= "</p>";
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Tickets Not Found '));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

}