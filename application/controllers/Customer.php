    <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'customer/add';
        $this->view_product = 'customer/view';
        $this->edit_product = 'customer/edit';
        $this->delete_product = 'customer/delete';
        $this->detail_product = 'customer/detail';

        $this->additional_view = 'customer/additional_view';
        $this->additional_edit = 'customer/additional_edit';
        $this->additional_add = 'customer/additional_add';
        $this->additional_delete = 'customer/additional_delete';
        // page active
        $this->add_page_product = "add-customer";
        $this->edit_page_product = "edit-customer";
        $this->view_page_product = "view-customer";

        $this->additional_page = "add-additional-customer";
        $this->view_additional_page = "view-additional-customer";
        $this->edit_additional_page = "edit-customer";
        // table
        $this->table = "user";
        $this->table_pid = "user_id";
        $this->orderBy = "DESC";
        $this->userType = 3;
        $this->detail_table = "customer_detail";
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // FileUploader
        require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "Customer";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        } 

        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 8)
            {
                if($v['visible'] == 1)
                {
                    $this->check_view = true;
                }
                if($v['add'] == 1)
                {
                    $this->check_add = true;
                }
                if($v['edit'] == 1)
                {
                    $this->check_edit = true;
                }
            }
            }
        }else
        {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
        }

    }
    function index(){
        if($this->check_view){
        redirect($this->add_product);
    }else
    {
        $res['heading'] = "Permission Not Given";
        $res['message'] = "You don't have permission to access this page.";
        $this->load->view('errors/html/error_404',$res);
    }
    }
    function add(){
        extract($_POST);
        if( $this->input->post()){
            // print_b($_POST);
            if(empty($id)){
                if( empty($user_name) ||  empty($user_city) || empty($user_country) || empty($user_phone) || empty($user_email)  || !isset($warranty_title) || empty($warranty_title) || !isset($terms) || empty($terms)){
                    if(!isset($warranty_title) || empty($warranty_title) || !isset($terms) || empty($terms)){
                        echo json_encode(array("error" => "Please fill warranty term & payment term fields"));
                        exit();
                    }else{
                        echo json_encode(array("error" => "Please fill all fields"));
                        exit();
                    }
                }
                else{
                    /*$Checkemail = $this->basic_model->getRow($this->table, 'user_email', $user_email,$this->orderBy,$this->table_pid);
                    if(@$Checkemail != null && !empty(@$Checkemail)){
                        echo json_encode(array("error" => "E-mail is already Exist"));
                        exit();
                    }

                    $Checkphone = $this->basic_model->getRow($this->table, 'user_phone', $user_phone,$this->orderBy,$this->table_pid);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        echo json_encode(array("error" => "Phone Number is Exist"));
                        exit();
                    }
                    */
                    // if($customer_type == 1)
                    // {
                    //     if(!isset($end_user_name) || empty($end_user_name))
                    //     {
                    //         echo json_encode(array("error" => "Please add end users"));
                    //         exit();
                    //     }
                    // }
                    if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $image_path = $data['files'][0]['name'];
                    }
                    
                    $data = array(
                        "user_name"=> @$user_name,
                        "user_company_name"=> @$user_name,
                        /*"user_display_name" => @$user_display_name,*/
                        "user_email" => $user_email,
                        "user_address" => @$user_address,
                        "user_city" => $user_city,
                        "user_country" => $user_country,
                        "user_phone" => @$user_phone,
                        "user_website" => @$user_website,
                        "user_is_active" => $user_is_active,
                        "user_image" => @$image_path,
                        "customer_type" => @$customer_type,
                        "user_type" => $this->userType,
                        "plan_type" => $plan_type,
                        "tax_reg" => @$tax_reg,
                        "validity_days" => 0,//$validity_days,
                        "support_days" => 0,//$support_days,
                        "warranty_years" => 0,//$warranty_years,
                        "currency_id" => $currency_id
                    );
                     
                    $id = $this->basic_model->insertRecord($data,$this->table);
                    $this->basic_model->updateRecord(["is_used" => 1], 'price_plan', 'price_plan_id', $plan_type);


                    if(isset($terms)){
                        for($i= 0; $i< sizeof($terms); $i++) {
                            $payment_data = [
                                'client_id' => $id,
                                'payment_title' => $terms[$i],
                                'percentage' => $percentage[$i],
                                'payment_days' => @$payment_days[$i]
                            ];
                            $this->basic_model->insertRecord($payment_data,'client_payment_term');
                        }

                    }

                    /*for($i= 0; $i< sizeof($project_terms); $i++) {
                        $project_data = [
                            'client_id' => $id,
                            'project_title' => $project_terms[$i]
                        ];
                        $this->basic_model->insertRecord($project_data,'client_project_term');
                    }*/
                    if(isset($warranty_title)){
                        foreach ($warranty_title as $k => $v) {
                            $data = [
                                'client_id' => $id,
                                'title'   => $warranty_title[$k],
                                'percentage' => $warranty_percentage[$k]
                            ];
                                
                            $this->basic_model->insertRecord($data,'client_warranty_type');
                        }

                    }

                    if(isset($contact_name)){
                        foreach ($contact_name as $k => $v) {
                            $contact_data = [
                                'client_id' => $id,
                                'contact_name'   => $contact_name[$k],
                                'contact_no' => $contact_no[$k],
                                'contact_position' => $contact_position[$k],
                                'contact_email' => $contact_email[$k]
                            ];
                                
                            $this->basic_model->insertRecord($contact_data,'customer_detail');
                        }
                    }

                    if(isset($end_user_name)){
                        foreach ($end_user_name as $k => $v) {
                            $end_user_data = [
                                'user_id' => $id,
                                'end_user_name'   => $end_user_name[$k],
                                'end_user_location' => $end_user_location[$k],
                                'end_user_address' => $end_user_address[$k],
                                'end_user_contact' => $end_user_contact[$k]
                            ];
                                
                            $this->basic_model->insertRecord($end_user_data,'end_user');
                        }
                    }
                    // $types = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    // for($i= 0; $i< sizeof($features); $i++) {
                    //     $data_features = [
                    //         'client_id' => $id,
                    //         'title' => $features[$i]
                    //     ];
                    //     $warn_id = $this->basic_model->insertRecord($data_features,'client_warranty');
                    //     $c = 1;
                    //     foreach($types as $type){
                    //         $data_setting = [
                    //             'warn_id' => $warn_id,
                    //             'client_id' => $id,
                    //             'type_id' => $type['warranty_type_id'],
                    //             'value'   => $_POST['warranty_type_id'.$c][$i],
                    //             'percentage' => $_POST['warranty_percentage'.$c][$i]
                    //         ];
                    //         $c++;
                    //        $this->basic_model->insertRecord($data_setting,'client_warranty_setting');
                    //     }
                    // }

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            //Edit form
            else{
                if( empty($user_name)  || empty($user_city) || empty($user_country) || empty($user_phone) || empty($user_email) || !isset($warranty_title) || empty($warranty_title) || !isset($terms) || empty($terms)  ){
                     if(!isset($warranty_title) || empty($warranty_title) || !isset($terms) || empty($terms)){
                        echo json_encode(array("error" => "Please fill warranty term & payment term fields"));
                        exit();
                    }else{
                        echo json_encode(array("error" => "Please fill all fields"));
                        exit();
                    }
                }else{
                    /*$Checkemail = $this->basic_model->getRow($this->table, 'user_email', $user_email,$this->orderBy,$this->table_pid);
                    if(@$Checkemail != null && !empty(@$Checkemail)){
                        if ($Checkemail['user_email'] != $user_email_old) {
                            echo json_encode(array("error" => "E-mail is already Exist"));
                            exit();
                        }
                    }

                    $Checkphone = $this->basic_model->getRow($this->table, 'user_phone', $user_phone,$this->orderBy,$this->table_pid);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        if ($Checkphone['user_phone'] != $user_phone_old) {
                            echo json_encode(array("error" => "Phone Number is Exist"));
                            exit();
                        }
                    }*/
                   
                    $data = array(
                        "user_name" => @$user_name,
                        "user_company_name" => @$user_name,
                        "user_display_name" => @$user_display_name,
                        "user_email" => @$user_email,
                        "user_address" => @$user_address,
                        "user_city" => $user_city,
                        "user_country" => $user_country,
                        "user_phone" => $user_phone,
                        "user_website" => @$user_website,
                        "customer_type" => @$customer_type,
                        "user_is_active" => $user_is_active,
                        "user_type" => $this->userType,
                        "plan_type" => $plan_type,
                        "tax_reg" => @$tax_reg,
                         "validity_days" => 0,//$validity_days,
                        "support_days" => 0,//$support_days,
                        "warranty_years" => 0,//$warranty_years,
                        "currency_id" => $currency_id
                    );

                    if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $datamy = $FileUploader->upload();

                        $data['user_image'] = $datamy['files'][0]['name'];
                    }
                     
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    $this->basic_model->updateRecord(["is_used" => 1], 'price_plan', 'price_plan_id', $plan_type);
                    

                    $this->basic_model->customQueryDel("DELETE FROM `client_payment_term` where client_id=".$id);
                    if(isset($terms)){
                        for($i= 0; $i< sizeof($terms); $i++) {
                            $payment_data = [
                                'client_id' => $id,
                                'payment_title' => $terms[$i],
                                'percentage' => $percentage[$i],
                                'payment_days' => @$payment_days[$i]
                            ];
                            $this->basic_model->insertRecord($payment_data,'client_payment_term');
                        }

                    }

                    /*$this->basic_model->customQueryDel("DELETE FROM `client_project_term` where client_id=".$id);
                    for($i= 0; $i< sizeof($project_terms); $i++) {
                        $project_data = [
                            'client_id' => $id,
                            'project_title' => $project_terms[$i]
                        ];
                        $this->basic_model->insertRecord($project_data,'client_project_term');
                    }*/

                    $this->basic_model->customQueryDel("DELETE FROM `client_warranty_type` where client_id=".$id);
                    if(isset($warranty_title)){
                        foreach ($warranty_title as $k => $v) {
                            $data = [
                                'client_id' => $id,
                                'title'   => $warranty_title[$k],
                                'percentage' => $warranty_percentage[$k]
                            ];
                              
                            $this->basic_model->insertRecord($data,'client_warranty_type');
                        }

                    }

                    $this->basic_model->customQueryDel("DELETE FROM `customer_detail` where client_id=".$id);
                    if(isset($contact_name)){
                        foreach ($contact_name as $k => $v) {
                            $contact_data = [
                                'client_id' => $id,
                                'contact_name'   => $contact_name[$k],
                                'contact_no' => $contact_no[$k],
                                'contact_position' => $contact_position[$k],
                                'contact_email' => $contact_email[$k]
                            ];
                                
                            $this->basic_model->insertRecord($contact_data,'customer_detail');
                        }
                    }

                    // $this->basic_model->customQueryDel("DELETE FROM `end_user` where user_id=".$id);
                    if(isset($end_user_name)){
                        foreach ($end_user_name as $k => $v) {
                            $end_user_data = [
                                'user_id' => $id,
                                'end_user_name'   => $end_user_name[$k],
                                'end_user_location' => $end_user_location[$k],
                                'end_user_address' => $end_user_address[$k],
                                'end_user_contact' => $end_user_contact[$k]
                            ];
                            if(isset($end_user_id[$k]) && $end_user_id[$k] != '' || $end_user_id[$k] != null)
                            {
                                $this->basic_model->updateRecord($end_user_data, 'end_user', 'end_user_id', $end_user_id[$k]);
                            }else
                            {
                                $this->basic_model->insertRecord($end_user_data,'end_user');
                            }
                            
                        }
                    }
                    // $this->basic_model->customQueryDel("DELETE FROM `client_warranty` where client_id=".$id);
                    // $this->basic_model->customQueryDel("DELETE FROM `client_warranty_setting` where client_id=".$id);
                    // $types = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    // for($i= 0; $i< sizeof($features); $i++) {
                    //     $data_features = [
                    //         'client_id' => $id,
                    //         'title' => $features[$i]
                    //     ];
                    //     $warn_id = $this->basic_model->insertRecord($data_features,'client_warranty');
                    //     $c = 1;
                    //     foreach($types as $type){
                    //         $data_setting = [
                    //             'warn_id' => $warn_id,
                    //             'client_id' => $id,
                    //             'type_id' => $type['warranty_type_id'],
                    //             'value'   => $_POST['warranty_type_id'.$c][$i],
                    //             'percentage' => $_POST['warranty_percentage'.$c][$i]
                    //         ];
                    //         $c++;
                    //        $this->basic_model->insertRecord($data_setting,'client_warranty_setting');
                    //     }
                    // }

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        //Add view
        else{
           
            if($this->check_add)
            {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                $res['city'] = $this->basic_model->getCustomRows("SELECT * FROM  city");
                $res['customer_type'] = $this->basic_model->getCustomRows("SELECT * FROM  customer_type");
                $res['price_list'] = $this->basic_model->getCustomRows("SELECT * FROM  price_plan");
                $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
                $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                $res['currency'] = $this->basic_model->getCustomRows('SELECT *  from currency');
                $res['data'] = $this->basic_model->getRow('setting','setting_id',1,$this->orderBy,'Setting_id');
                $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty');
                // $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, warranty_setting.*  from warranty_type Left Join warranty_setting on warranty_type.warranty_type_id = warranty_setting.type_id And warranty_type.deleted IS NULL');
                $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');
                $res["ckeditor"] = "yes";
                $this->template_view($this->add_page_product,$res);
            }
            else
            {   
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
            
        }
    }

    function edit($id){
        if($this->check_edit)
        {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." WHERE user_type = ".$this->userType." AND ".$this->table_pid." = ".$id." ORDER BY ".$this->table_pid." ".$this->orderBy);
                if($res_edit){
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res['image_upload_dir'] = $this->image_upload_dir;
                    $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                    $res['city'] = $this->basic_model->getCustomRows("SELECT * FROM  city");
                    $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    $res['customer_type'] = $this->basic_model->getCustomRows("SELECT * FROM  customer_type");
                    $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id='.$id);
                    //$res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from client_project_term where client_id='.$id);
                    $res['currency'] = $this->basic_model->getCustomRows('SELECT *  from currency');
                    $res['price_list'] = $this->basic_model->getCustomRows("SELECT * FROM  price_plan");
                    $res['contact'] = $this->basic_model->getCustomRows('SELECT * FROM  customer_detail where client_id='.$id);
                    $res['end_user'] = $this->basic_model->getCustomRows('SELECT * FROM  end_user where user_id='.$id);
    
    
                    if(empty($res['terms'])){
                        $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
                    }
                    if(empty($res['project_terms'])){
                      $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                    }
                    $setting = $this->basic_model->getRow('setting','setting_id',1,$this->orderBy,'Setting_id');
                    $res['data']['validity_days'] = (empty($res['data']['validity_days']))? $setting['validity_days'] : $res['data']['validity_days'];
                    $res['data']['support_days'] = (empty($res['data']['support_days']))? $setting['support_days'] : $res['data']['support_days'];
                    $res['data']['warranty_years'] = (empty($res['data']['warranty_years']))? $setting['warranty_years'] : $res['data']['warranty_years'];
    
                    //$res['warranty'] = $this->basic_model->getCustomRows('SELECT * from client_warranty where client_id='.$id);
                    // $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, client_warranty_setting.*  from warranty_type Left Join client_warranty_setting on warranty_type.warranty_type_id = client_warranty_setting.type_id');
                    /*$res['default_warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, warranty_setting.*  from warranty_type Left Join warranty_setting on warranty_type.warranty_type_id = warranty_setting.type_id');*/
                    $res["ckeditor"] = "yes";
                    $res['warranty_type'] = $this->basic_model->getCustomRows("SELECT * from client_warranty_type where client_id = '".$id."' ");
    
                    if(empty($res['warranty_type'])){
                        //$res['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty');
                        // $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, warranty_setting.*  from warranty_type Left Join warranty_setting on warranty_type.warranty_type_id = warranty_setting.type_id');
                        $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    }
    
    
                    // print_b($res);
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        }else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
        
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["additional_add"] = $this->additional_add;
        $res["additional_view"] = $this->additional_view;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['customer_type'] = $this->basic_model->getCustomRows("SELECT * FROM  customer_type");
        $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
        $res['city'] = $this->basic_model->getCustomRows("SELECT * FROM  city");
        $res['data'] = $this->basic_model->getCustomRows("SELECT user.*, customer_type.title, country.country_name FROM ".$this->table." Left JOIN customer_type ON customer_type.id = user.customer_type Left JOIN country ON country.country_id = user.user_country WHERE user_type = ".$this->userType." ORDER BY ".$this->table_pid." ".$this->orderBy." LIMIT 100");

        // print_b($res['data']);

        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE user.user_type = ".$this->userType;
            if($customer_name != 0)
            {
                $where .= " AND user.user_company_name LIKE '%".$customer_name."%'";
            }
            if($type != 0)
            {
                $where .= " AND user.customer_type =".$type;
            }

            if($country != 0)
            {
                $where .= " AND user.user_country = ".$country;
            }

            if($city != "0")
            {
                $where .= " AND user.user_city = '".$city."'";
            }
            
            $where .= " ORDER BY user.".$this->table_pid."  ".$this->orderBy." LIMIT 100";
            $query = "SELECT user.*, customer_type.title, country.country_name FROM ".$this->table." Left JOIN customer_type ON customer_type.id = user.customer_type Left JOIN country ON country.country_id = user.user_country ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);
            
            echo json_encode($res);
        }
    }
    
    function imageDelete(){
        extract($_POST);
        // print_b($_POST);
        $file_path = dir_path().$this->image_upload_dir.'/'.@$file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $arr = array(
            'user_image' => ''
        );
        $updateRecord = $this->basic_model->updateRecord($arr, $this->table, $this->table_pid, $data['image_post_file_id']);
    }
    
    function delete($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if(isset($res['user_image']) && @$res['user_image'] !=null){
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['user_image'];
                if (file_exists($file_path)) {
                    @unlink($file_path);
                }
            }
            $del = $this->basic_model->deleteRecord($this->table_pid, $id, $this->table);
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }
 
    function detail($id){
        if( isset($id) && $id != ''){
             $res  = $this->basic_model->getCustomRows("SELECT user.*, customer_type.title, price_plan.plan_slog, currency.title as currency_title FROM  user 
                Left JOIN customer_type ON customer_type.id = user.customer_type
                Left Join price_plan ON price_plan.price_plan_id = user.plan_type
                Left Join currency ON currency.id = user.currency_id
                 WHERE user.user_id = ".$id." ORDER BY ".$this->table_pid." ".$this->orderBy);
            $end_user = $this->basic_model->getCustomRows("Select * From end_user JOIN city ON city.city_id = end_user.end_user_location where end_user.user_id=".$id);
            if($res){
                $res = $res[0];
                $countries = $this->basic_model->getCustomRow("SELECT * FROM  country where country_id=".$res['user_country']);
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = "";
                $current_img = '';
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['user_image'];
                if (!is_dir($file_path) && file_exists($file_path)) {
                  $current_img = site_url($this->image_upload_dir.'/'.$res['user_image']);
                }else{
                  $current_img = site_url("uploads/dummy.jpg");
                }
                if($countries){
                    $country = $countries['country_name'];
                }else{
                    $country = '';
                }
                //$outputDescription .= "<p><br> <img src='".$current_img."' width='100px' /></p>";
                //$outputDescription .= "<p>Name : ".$res['user_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Company Name : </span> ".$res['user_company_name']."</p>";
                //$outputDescription .= "<p>Display Name : ".$res['user_display_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Address :".$res['user_address']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>City : </span> ".$res['user_city']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Country : </span> ".@$country."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Contact : </span> ".$res['user_phone']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Website : </span> ".$res['user_website']."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Customer Type : </span> ".$res['title']."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Price ID : </span> ".$res['plan_slog']."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Currency : </span> ".$res['currency_title']."</p>"; 
                //$outputDescription .= "<p>Validity Days : ".$res['validity_days']."</p>"; 
                //$outputDescription .= "<p>Support Days : ".$res['support_days']."</p>"; 
                //$outputDescription .= "<p>Warranty Years : ".$res['warranty_years']."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>T.R.No. : </span> ".$res['tax_reg']."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Status : </span> ".(($res['user_is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";
                
                $outputDescription .= "<p><span style='font-weight:bold;'>End Users : </span></p>"; 
                $count = 0;
                foreach ($end_user as $k => $v) {
                    $count++;
                    $outputDescription .= "<p><span style='font-weight:bold;'>" . $count . ".</span> User Name : " . $v['end_user_name'] . "</p>";
                    $outputDescription .= "<p>location : " . $v['city_name'] . "</p>";
                    $outputDescription .= "<p>Address : " . $v['end_user_address'] . "</p>";
                    $outputDescription .= "<p>Contact : " . $v['end_user_location'] . "</p>";
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
    function additional_add($id = 0){
        extract($_POST);
        extract($_GET);
        if($this->input->post()){
            if(empty($row_id)){
                if( empty($user_id) || empty($name) || empty($number) || empty($position) || empty($notes) ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $Checkphone = $this->basic_model->getRow($this->detail_table, 'number', $number);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        if ($Checkphone['number'] != $user_phone_old) {
                            echo json_encode(array("error" => "Phone Number is Exist"));
                            exit();
                        }
                    }
                    
                    $data = array(
                        "user_id" => $user_id,
                        "name"=> $name,
                        "number" => $number,
                        "position" => $position,
                        "notes" => $notes
                    );
                     
                    $id = $this->basic_model->insertRecord($data,$this->detail_table);
                  
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->additional_view."/".$user_id), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
                 if(empty($user_id) || empty($name) || empty($number) || empty($position) || empty($notes) ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }else{
                    $Checkphone = $this->basic_model->getRow($this->detail_table, 'number', $number);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        if ($Checkphone['number'] != $user_phone_old) {
                            echo json_encode(array("error" => "Phone Number is Exist"));
                            exit();
                        }
                    }

                    $data = array(
                        "user_id" => $user_id,
                        "name"=> $name,
                        "number" => $number,
                        "position" => $position,
                        "notes" => $notes
                    );
                    $updateRecord = $this->basic_model->updateRecord($data, $this->detail_table, 'id', $row_id);
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->additional_view."/".$user_id), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{
            $res['title'] = "Add New ".$this->page_heading;
            $res["page_title"] = "add";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["add_product"] = $this->additional_add;
            $res["data"] = ["user_id" => $id];
            $this->template_view($this->additional_page,$res);
        }
    }

    function additional_edit($id){
        if( isset($id) && $id != ''){
            $res_edit = $this->basic_model->getCustomRow("SELECT * FROM ".$this->detail_table." WHERE id = ".$id);
            if($res_edit){
                $res["id"] = $id;
                $res["title"] = "Edit ".$this->page_heading. " Additional ";
                $res["page_title"] =  "";
                $res["page_heading"] = $this->page_heading. " Additional ";
                $res["active"] = $this->view_page_product;
                $res["add_product"] = $this->additional_add;
                $res['data'] = $this->basic_model->getRow($this->detail_table, 'id', $id);

                $this->template_view($this->additional_page,$res);
            }else{
                redirect($this->no_results_found);    
            }
        }else{
            redirect($this->no_results_found);
        }
    }

    function additional_view($id){
        $res['title'] = "View ".$this->page_heading. " Additional Info";
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading." Additional Info";
        $res["add_product"] = $this->additional_add;
        $res["edit_product"] = $this->additional_edit;
        $res["additional_add"] = $this->additional_view;
        $res["delete_product"] = $this->additional_delete;
        $res['view_product']  = $this->view_product;
        $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->detail_table." WHERE user_id = ".$id." order by id desc");
        $this->template_view($this->view_additional_page,$res);
    }

    function additional_delete($id){
        extract($_GET);
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            $del = $this->basic_model->deleteRecord("id", $id, $this->detail_table);
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->additional_view.'/'.@$user_id), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }
}