<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class price_plan extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
		
        // page action
        $this->add_price_plan = 'price_plan/add';
        $this->view_price_plan = '/price_plan/view';
        $this->edit_price_plan = 'price_plan/edit';
        $this->delete_price_plan = 'price_plan/delete';
        $this->detail_price_plan = 'price_plan/detail';
        $this->add_price_clone   = 'price_plan/clone';

        // page active
        $this->add_page_price_plan = "add-price_plan";
        $this->edit_page_price_plan = "edit-price_plan";
        $this->view_page_price_plan = "view-price_plan";
        // table
        $this->table = "price_plan";
        $this->table_pid = "price_plan_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/price_plan";
		// FileUploader
		require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "Price List";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
	
    function index(){
        redirect($this->add_price_plan);
    }
    function add(){
        extract($_POST);
	   
        if( $this->input->post()){
            if(empty($id)){
                if( empty($plan_slog) || empty($plan_description)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $check_plan_slog = $this->basic_model->getRow($this->table, 'plan_slog', $plan_slog,$this->orderBy,$this->table_pid);
                    if(@$check_plan_slog != null && !empty(@$check_plan_slog)){
                        //echo json_encode(array("error" => "Price ID is Exist"));
                        //exit();
                    }
                    $data = array(
                        "plan_slog"=>$plan_slog,
                        "plan_description"=>$plan_description
                    );
                    $paln_id = $this->basic_model->insertRecord($data,$this->table);
                    for($i = 0; $i<sizeof($product_id); $i++){
                        $price_data = [
                            "prod_id"             => $product_id[$i],
                            "plan_id"             => $paln_id,
                            "retail_base_price"   => empty($retail_base_price[$i])  ? 0 : $retail_base_price[$i],
                            "retail_usd_price"    => empty($retail_usd_price[$i])   ? 0 : $retail_usd_price[$i],
                            "retail_discount"     => empty($retail_discount[$i])    ? 0 : $retail_discount[$i],
                            "reseller_base_price" => empty($reseller_base_price[$i])? 0 : $reseller_base_price[$i],
                            "reseller_usd_price"  => empty($reseller_usd_price[$i]) ? 0 : $reseller_usd_price[$i],
                            "reseller_discount"   => empty($reseller_discount[$i])  ? 0 : $reseller_discount[$i],
                            "partner_base_price"  => empty($partner_base_price[$i]) ? 0 : $partner_base_price[$i],
                            "partner_usd_price"   => empty($partner_usd_price[$i])  ? 0 : $partner_usd_price[$i],
                            "partner_discount"    => empty($partner_discount[$i])   ? 0 : $partner_discount[$i]
                        ];

                     $this->basic_model->insertRecord($price_data,'price');

                    }
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_price_plan), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
                if( empty($plan_slog) || empty($plan_description)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $check_plan_slog = $this->basic_model->getRow($this->table, 'plan_slog', $plan_slog,$this->orderBy,$this->table_pid);
                    if(@$check_plan_slog != null && !empty(@$check_plan_slog)){
                        if ($check_plan_slog['plan_slog'] != $plan_slog) {
                            echo json_encode(array("error" => "Price ID is Exist"));
                            exit();
                        }
                    }
					$data = array(
                        "plan_slog"=>$plan_slog,
                        "plan_description"=>$plan_description
                    );
                    
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    $this->basic_model->deleteRecord('plan_id', $id, 'price');
                     for($i = 0; $i<sizeof($product_id); $i++){
                        if(!empty($product_id[$i])){
                            $price_data = [
                                "prod_id"             => $product_id[$i],
                                "plan_id"             => $id,
                                "retail_base_price"   => empty($retail_base_price[$i])  ? 0 : $retail_base_price[$i],
                                "retail_usd_price"    => empty($retail_usd_price[$i])   ? 0 : $retail_usd_price[$i],
                                "retail_discount"     => empty($retail_discount[$i])    ? 0 : $retail_discount[$i],
                                "reseller_base_price" => empty($reseller_base_price[$i])? 0 : $reseller_base_price[$i],
                                "reseller_usd_price"  => empty($reseller_usd_price[$i]) ? 0 : $reseller_usd_price[$i],
                                "reseller_discount"   => empty($reseller_discount[$i])  ? 0 : $reseller_discount[$i],
                                "partner_base_price"  => empty($partner_base_price[$i]) ? 0 : $partner_base_price[$i],
                                "partner_usd_price"   => empty($partner_usd_price[$i])  ? 0 : $partner_usd_price[$i],
                                "partner_discount"    => empty($partner_discount[$i])   ? 0 : $partner_discount[$i]
                            ];
                            $this->basic_model->insertRecord($price_data,'price');

                            //$this->basic_model->updateRecord($price_data, 'price', 'id', $price_id[$i]);
                        }
                    }
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_price_plan), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{

            $price_add = false;
            if ($this->user_type == 2) {
                foreach ($this->user_role as $k => $v) {
                if($v['module_id'] == 3)
                {
                    if($v['add'] == 1)
                    {
                        $price_add = true;
                    }
                }
                }
            }else
            {
                $price_add = true;
            }
            if ($price_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_price_plan;
                $res["add_product"] = $this->add_price_plan;
                $res["data"] = $this->basic_model->getCustomRows("SELECT * FROM product ORDER BY product_id ".$this->orderBy);

                $this->template_view($this->add_page_price_plan,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function edit($id){
        $price_add = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 3)
            {
                if($v['add'] == 1)
                {
                    $price_add = true;
                }
            }
            }
        }else
        {
            $price_add = true;
        }
        if ($price_add) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_price_plan;
                    $res["add_product"] = $this->add_price_plan;
                    
                    $prices = $this->basic_model->getCustomRows("SELECT * FROM price_plan 
                        LEFT Join price on price.plan_id = price_plan.price_plan_id
                        AND price_plan.price_plan_id = ".$id);
                    $products = $this->basic_model->getCustomRows("SELECT * FROM product ORDER BY product_id ".$this->orderBy);
                    
                    $data = Array();
                    foreach($products as $pro){
                        $find = false;
                        foreach($prices as $price){
                            if($price['prod_id'] == $pro['product_id']){
                                $find = true;
                                $data[] = [
                                    "price_plan_id" => $price['price_plan_id'], 
                                    "plan_slog" => $price['plan_slog'], 
                                    "plan_description" => $price['plan_description'], 
                                    "is_used" => $price['is_used'], 
                                    "id" => $price['id'], 
                                    "prod_id" => $price['prod_id'], 
                                    "plan_id" => $price['plan_id'], 
                                    "retail_base_price" => $price['retail_base_price'], 
                                    "retail_usd_price" => $price['retail_usd_price'], 
                                    "retail_discount" => $price['retail_discount'], 
                                    "reseller_base_price" => $price['reseller_base_price'], 
                                    "reseller_usd_price" => $price['reseller_usd_price'], 
                                    "reseller_discount" => $price['reseller_discount'], 
                                    "partner_base_price" => $price['partner_base_price'], 
                                    "partner_usd_price" => $price['partner_usd_price'], 
                                    "partner_discount" => $price['partner_discount'],
                                    "product_id" => $pro['product_id'],
                                    "product_sku" => $pro['product_sku'],
                                    "product_name" => $pro['product_name']

                                ];
                            }
                        }
                        if($find == false){
                            $data[] = [
                                    "price_plan_id" => '', 
                                    "plan_slog" => '', 
                                    "plan_description" => '', 
                                    "is_used" => '', 
                                    "id" => '', 
                                    "prod_id" => '', 
                                    "plan_id" => '', 
                                    "retail_base_price" => '', 
                                    "retail_usd_price" => '', 
                                    "retail_discount" => '', 
                                    "reseller_base_price" => '', 
                                    "reseller_usd_price" => '', 
                                    "reseller_discount" => '', 
                                    "partner_base_price" => '', 
                                    "partner_usd_price" => '', 
                                    "partner_discount" => '',
                                    "product_id" => $pro['product_id'],
                                    "product_sku" => $pro['product_sku'],
                                    "product_name" => $pro['product_name']
                                ];

                        }
                    }
                    $res["plan_data"] = $prices;
                    $res["data"] = $data;
                    /* $res["data"] = $this->basic_model->getCustomRows("SELECT * FROM product 
                        LEFT Join price on price.prod_id = product.product_id
                        Left Join price_plan on price_plan.price_plan_id = price.plan_id
                        AND price_plan.price_plan_id = ".$id);*/


                    $this->template_view($this->add_page_price_plan,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function view() {

        $price_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 3)
            {
                if($v['visible'] == 1)
                {
                    $price_view = true;
                }
            }
            }
        }else
        {
            $price_view = true;
        }
        if ($price_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_price_plan;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_price_plan;
            $res["edit_product"] = $this->edit_price_plan;
            $res["delete_product"] = $this->delete_price_plan;
            $res["detail_product"] = $this->detail_price_plan; 
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." ORDER BY price_plan_id ".$this->orderBy);
            $res['add_price_clone']= $this->add_price_clone;

            $this->template_view($this->view_page_price_plan,$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
	
	function imageDelete(){
        extract($_POST);
        // print_b($_POST);
        $file_path = dir_path().$this->image_upload_dir.'/'.@$file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $arr = array(
            'product_image' => ''
        );
        $updateRecord = $this->basic_model->updateRecord($arr, $this->table, $this->table_pid, $data['image_post_file_id']);
    }
	
    function delete($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if(is_null(@$res['is_used'])){
                $del = $this->basic_model->deleteRecord($this->table_pid, $id, $this->table);
                $del = $this->basic_model->deleteRecord('plan_id', $id, 'price');
                echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_price_plan), "details"=> true));
            }else{
                echo json_encode(array( "error" => $this->page_heading." list is in use can't be deleted."));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
        
    }
 
    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = "";
                $current_img = '';
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['product_image'];
                if (!is_dir($file_path) && file_exists($file_path)) {
                  $current_img = site_url($this->image_upload_dir.'/'.$res['product_image']);
                }else{
                  $current_img = site_url("uploads/dummy.jpg");
                }
                $outputDescription .= "<p>Image :<br> <img src='".$current_img."' width='100px' /></p>";
                $res_cate = $this->basic_model->getCustomRow("SELECT * FROM `category` WHERE `category_is_active` = 1 AND `category_id` = ".$res['category_id']); 
             
                if(isset($res_cate) && @$res_cate != null){ 
                    $outputDescription .= "<p>Category : ".$res_cate['category_name']."</p>"; 
                }
                $outputDescription .= "<p><span style='font-weight:bold;'>Name : </span> ".$res['product_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Description : </span> ".$res['product_description']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Model No : </span> ".$res['product_sku']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Price : </span> ".$res['product_price']."</p>";
				
				
				$outputDescription .= "<p><span style='font-weight:bold;'>Status : </span>".(($res['product_is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";
                 


                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function clone(){
        $price_add = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 3)
            {
                if($v['add'] == 1)
                {
                    $price_add = true;
                }
            }
            }
        }else
        {
            $price_add = true;
        }
        if ($price_add) {
            if($this->input->post()){
            extract($_POST);
                $data_plan = array(
                    "plan_slog"=>$plan_slog,
                    "plan_description"=>$plan_description
                );
                $paln_id = $this->basic_model->insertRecord($data_plan,$this->table);
                $all_data = $this->basic_model->getCustomRows("SELECT * FROM `price` WHERE `plan_id` = ".$price_id_modal);
                foreach($all_data as $data){
                    $price_data = [
                        "prod_id"             => $data['prod_id'],
                        "plan_id"             => $paln_id,
                        "retail_base_price"   => $data['retail_base_price'],
                        "retail_usd_price"    => $data['retail_usd_price'],
                        "retail_discount"     => $data['retail_discount'],
                        "reseller_base_price" => $data['reseller_base_price'],
                        "reseller_usd_price"  => $data['reseller_usd_price'],
                        "reseller_discount"   => $data['reseller_discount'],
                        "partner_base_price"  => $data['partner_base_price'],
                        "partner_usd_price"   => $data['partner_usd_price'],
                        "partner_discount"    => $data['partner_discount']
                    ];

                $this->basic_model->insertRecord($price_data,'price');

                }
                echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_price_plan), 'fieldsEmpty' => 'yes'));
            }
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }

    }
}