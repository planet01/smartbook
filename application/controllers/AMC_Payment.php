<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AMC_Payment extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->receivables = 'AMC_Receivables/view';
        $this->add_product = 'AMC_Payment/add';
        $this->view_product = 'AMC_Payment/view';
        $this->edit_product = 'AMC_Payment/edit';
        $this->delete_product = 'AMC_Payment/delete';
        $this->detail_product = 'AMC_Payment/detail';
        // page active
        $this->add_page_product = "add-amc-payment";
        $this->edit_page_product = "edit-payment";
        $this->view_page_product = "view-payment";
        // table
        $this->table = "amc_payment";
        $this->tables = "setting";
        $this->table_sid = "setting_id";

        $this->table_pid = "amc_payment_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/payment";
        // Page Heading
        $this->page_heading = "AMC Payment";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index(){
        redirect('payment/view_page');
    }
    function view_page()
    {
        redirect($this->receivables);
    }
    function view()
    {
        extract($_GET);
        if( isset($id) && $id != '' && isset($is_invoice) && $is_invoice != ''){
            $quotation = $this->basic_model->getCustomRow("SELECT * FROM `amc_quotation` WHERE amc_quotation_id = '".$id."' "); 
            $invoice_no = "";
            $customer_id = "";
            if( isset($quotation) && $quotation != '')
            {
               
                $invoice_no = $quotation['amc_quotation_no'];
                $customer_id = $quotation['customer_id'];   
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res['data'] = array(
                    'invoice_no' => $invoice_no, 
                    'invoice_id' => $id, 
                    'total_amount' => @$total_amount, 
                    'toal_received' => @$toal_received, 
                    'overdue_since' => @$overdue_since, 
                    'overdue_amount' => @number_format((float)$overdue_amount, 2, '.', ''), 
                    'pre_invoice_no' => @$pre_invoice_no,
                );

                $res["customer"] = $this->basic_model->getCustomRow("SELECT * from user where user_id=".$customer_id);
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($res["setval"]["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                redirect($this->receivables);
            }
            
        }
        else
        {
            redirect($this->receivables);
        }
    }
    function add(){
        $setval = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
        if( $this->input->post()){ 
            extract($_POST);
            if(empty($id)){
             //print_b($_POST);
                 if( empty($invoice_no) || empty($payment_date) || (empty($payment_amount) && $payment_amount != 0 )  ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $invoice = $this->basic_model->getCustomRow("SELECT q.*,u.user_name FROM amc_quotation q JOIN user u ON u.user_id = q.customer_id  WHERE q.amc_quotation_id = '".$invoice_id."' AND q.amc_quotation_status = 4 AND q.amc_quotation_revised_id = 0 ");
                    $currency = @$invoice['amc_quotation_currency'];
                    $amc_type = $this->basic_model->getCustomRow("SELECT * from amc_type where amc_quotation_id='".$invoice_id."' ORDER BY amc_type_id asc");
                    
                    $total = @$amc_type['amc_type_total_amount'];
                         
                    $where = "invoice_id = '".$invoice_id."'";
                  
                    $payment_data = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM amc_payment WHERE ".$where." AND (payment_revised_id = 0 || payment_revised_id IS NULL) ");
                    $find_total = 0;
                    $get_total = 0;
                    if(!empty($payment_data['total_payment_amount']) || !empty($payment_data['total_payment_adjustment'])){
                        $find_payment = $payment_data['total_payment_amount']+ $payment_amount;
                        $this_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_adjustment = $payment_data['total_payment_adjustment'] + $this_adjustment;
                        $find_total = $find_payment+$find_adjustment;
                        $get_total = $find_payment+$find_adjustment;
                        if($find_total > $total_net_amount){
                            echo json_encode(array("error" => "Payment can not exceed the AMC Quotation Amount".$find_total." > ". $total));
                            return false;
                        }
                    }else{
                        $find_payment =  ($payment_amount == NULL)? 0 : $payment_amount;
                        $this_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_total = $payment_amount + $this_adjustment;
                        $get_total = $find_total;
                    }
                   /* $payment_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                    $get_total = $get_total + $payment_amount + $payment_adjustment;*/

                    $recipt_date_year = explode("-",$payment_date)[0];
                    $receipt_no = serial_no_amc_receivable($recipt_date_year);

                    $payment_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                    $data = array(
                        "invoice_id"=> $invoice_id,
                        "invoice_no"=> $invoice_no,
                        "payment_currency_id" => $currency,
                        "receipt_no" => $receipt_no,
                        "payment_date" => $payment_date,
                        "payment_amount"=> $payment_amount,
                        "payment_cheque"=> $payment_cheque,
                        "payment_notes"=> $payment_notes,
                        "payment_adjustment" => $payment_adjustment,
                        "payment_customer_id" => $payment_customer_id,
                        "payment_revised_id" => 0,

                    );

                    $payment_id = $this->basic_model->insertRecord($data,$this->table); 

                   //Add to voucher
                   if($payment_amount > 0)
                   {

                       $Transaction_Date_year = explode("-",$payment_date)[0];
                       $company_prfx = (substr($setval["company_prefix"], -1) == '-')?$setval["company_prefix"]:$setval["company_prefix"].'-';
                        $t_no = serial_no_voucher($company_prfx.'RV-'.$Transaction_Date_year);
                       
                       $voucher_data = array(
                           'TransactionNo' => $t_no,
                           'voucher_type' => 2,
                           'Transaction_Date' => $payment_date,
                           'Transaction_Detail' => 'AMC Payment Voucher for # '.$setval["company_prefix"].'AMCINV-'.$invoice_no,
                           'CompanyID' => str_replace("-","",$setval["company_prefix"]),
                           'Posted' => 1,
                           'flgDeleted' => 0,
                           'doc_type' => 1,
                           'doc_id' => $payment_id
                       );
                       $voucher_id = $this->basic_model->insertRecord($voucher_data, 'voucher');
                       //credit entry
                       $voucher_detail1 = array(
                           'AccountNo' => $setval['receivable_account'],
                           'voucher_id' => $voucher_id,
                           'invoice_id' => @$invoice_id,
                           'Cheque_No' => @$payment_cheque,
                           'Pay_To_Name' => @$customer_name,
                           'Amount_Cr' => $payment_amount + $payment_adjustment,
                           'Amount_Dr' => 0,
                           'Transaction_Detail' => @$payment_notes,
                           'Remarks' => 'Credit Entry',                     
                       );
                       $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');
                       //debit entry
                       $voucher_detail2 = array(
                           'AccountNo' => $setval['bank_account'],
                           'voucher_id' => $voucher_id,
                           'invoice_id' => @$invoice_id,
                           'Cheque_No' => @$payment_cheque,
                           'Pay_To_Name' => @$customer_name,
                           'Amount_Cr' => 0,
                           'Amount_Dr' => $payment_amount,
                           'Transaction_Detail' => @$payment_notes,
                           'Remarks' => 'Debit Entry',                     
                       );
                       $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');

                       if($payment_adjustment != 0){
                        //debit entry
                            $voucher_detail2 = array(
                                'AccountNo' => $setval['adjustment_account'],
                                'voucher_id' => $voucher_id,
                                'invoice_id' => @$invoice_id,
                                'Cheque_No' => @$payment_cheque,
                                'Pay_To_Name' => @$customer_name,
                                'Amount_Cr' => 0,
                                'Amount_Dr' => $payment_adjustment,
                                'Transaction_Detail' => @$payment_notes,
                                'Remarks' => 'Debit Entry Adjustment',                     
                            );
                            $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
                        }

                   }
                   //End Voucher 
                   
                    //if(floatval($get_total) == floatval($total_net_amount)){
                    if(round($get_total,2) == round($total_net_amount, 2)){
                        $amc_quotation = $this->basic_model->getCustomRow("SELECT * FROM amc_quotation  WHERE amc_quotation_id = '".$invoice_id."'");
                        $amc_quotation_detail = $this->basic_model->getRows('amc_quotation_detail', 'amc_quotation_id',$invoice_id,"amc_quotation_detail_id","DESC");
                        foreach($amc_quotation_detail as $v){
                            if($v['selected_quotation_no'] != ''){
                                $find_inv = $this->basic_model->getCustomRow("SELECT * FROM invoice  WHERE invoice_no = '".$v['selected_quotation_no']."' ");
                                $find_inv['invoice_date'] = $amc_quotation['amc_quotation_expiry_date'];
                                // $this->basic_model->customQueryDel("UPDATE amc_quotation SET amc_quotation_status=7 where amc_quotation_id=".$invoice_id);
                                $this->active_amc($find_inv);

                            }
                        }
                    }
                    
                    $this->basic_model->customQueryDel('UPDATE amc_quotation SET has_payment = 1 WHERE amc_quotation_id = "'.$invoice_id.'"');
                    
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->receivables), 'fieldsEmpty' => 'yes'));
                }
            }else{
                if( empty($invoice_no) || empty($payment_date) || (empty($payment_amount) && $payment_amount != 0 )  ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    
                    $invoice = $this->basic_model->getCustomRow("SELECT q.*,u.user_name FROM amc_quotation q JOIN user u ON u.user_id = q.customer_id  WHERE q.amc_quotation_id = '".$invoice_id."' ");
                    $currency = @$invoice['amc_quotation_currency'];

                    $amc_type = $this->basic_model->getCustomRow("SELECT * from amc_type where amc_quotation_id='".$invoice_id."' ORDER BY amc_type_id asc");
                    
                    $total = @$amc_type['amc_type_total_amount'];
                    $inv_id = 0;
                

                    $where = "invoice_id = '".$invoice_id."'";

                    $inv_id = $invoice_id;

                    $payment_data = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM amc_payment WHERE ".$where." AND (payment_revised_id = 0 || payment_revised_id IS NULL) AND amc_payment_id != ".$id);
                    if(!empty($payment_data['total_payment_amount']) || !empty($payment_data['total_payment_adjustment'])){
                        $find_payment = $payment_data['total_payment_amount']+ $payment_amount;
                        $find_adjustment = $payment_data['total_payment_adjustment'] + ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_total = $find_payment+$find_adjustment;
                        if($find_total > $total_net_amount){
                            echo json_encode(array("error" => "Payment can not exceed the AMC Quotation Amount"));
                            return false;
                        }
                        // if($find_total == $total_net_amount){
                        //     $this->basic_model->customQueryDel("UPDATE amc_quotation SET amc_quotation_status  =1 where amc_quotation_id =".$inv_id);
                        // }
                    }else{
                        $find_payment =  ($payment_amount == NULL)? 0 : $payment_amount;
                        $this_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_total = $payment_amount + $this_adjustment;
                        if($find_total > $total_net_amount){
                            echo json_encode(array("error" => "Payment can not exceed the AMC Quotation Amount"));
                            return false;
                        }
                        // if($find_total < $total_net_amount){
                        //     $this->basic_model->customQueryDel("UPDATE amc_quotation SET amc_quotation_status  =1 where amc_quotation_id =".$inv_id);
                        // }else if($find_total == $total_net_amount){
                        //     $this->basic_model->customQueryDel("UPDATE amc_quotation SET amc_quotation_status  =4 where amc_qquotation_id =".$inv_id);
                        // }
                    }
                    $payment_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                    $data = array(
                        "invoice_id"=> $invoice_id,
                        "invoice_no"=> $invoice_no,
                        "payment_currency_id" => $currency,
                        "receipt_no" => $receipt_no,
                        "payment_date" => $payment_date,
                        "payment_amount"=> $payment_amount,
                        "payment_cheque"=> $payment_cheque,
                        "payment_notes"=> $payment_notes,
                        "payment_adjustment" => $payment_adjustment,
                        "payment_customer_id" => $payment_customer_id,
                        "payment_revised_id" => 0,

                    );
                    $this->makePaymentHistory($id);
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);

                    $this->basic_model->customQueryDel("UPDATE voucher SET flgDeleted=1 where doc_type= 1 AND doc_id=".$id);
                    //Add to voucher
                    if($payment_amount > 0)
                    {
                        $Transaction_Date_year = explode("-",$payment_date)[0];
                        $company_prfx = (substr($setval["company_prefix"], -1) == '-')?$setval["company_prefix"]:$setval["company_prefix"].'-';
                        $t_no = serial_no_voucher($company_prfx.'RV-'.$Transaction_Date_year);
                        
                        $voucher_data = array(
                            'TransactionNo' => $t_no,
                            'voucher_type' => 2,
                            'Transaction_Date' => $payment_date,
                            'Transaction_Detail' => 'AMC Payment Voucher for # '.$setval["company_prefix"].'AMCINV-'.$invoice_no,
                            'CompanyID' => str_replace("-","",$setval["company_prefix"]),
                            'Posted' => 1,
                            'doc_type' => 1,
                            'flgDeleted' => 0,
                            'doc_id' => $id
                        );
                        $voucher_id = $this->basic_model->insertRecord($voucher_data, 'voucher');
                        //credit entry
                        $voucher_detail1 = array(
                            'AccountNo' => $setval['receivable_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => @$payment_cheque,
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => $payment_amount + $payment_adjustment,
                            'Amount_Dr' => 0,
                            'Transaction_Detail' => @$payment_notes,
                            'Remarks' => 'Credit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');
                        //debit entry
                        $voucher_detail2 = array(
                            'AccountNo' => $setval['receivable_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => @$payment_cheque,
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => 0,
                            'Amount_Dr' => $payment_amount,
                            'Transaction_Detail' => @$payment_notes,
                            'Remarks' => 'Debit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');

                        if($payment_adjustment != 0){
                            //debit entry
                                $voucher_detail2 = array(
                                    'AccountNo' => $setval['adjustment_account'],
                                    'voucher_id' => $voucher_id,
                                    'invoice_id' => @$invoice_id,
                                    'Cheque_No' => @$payment_cheque,
                                    'Pay_To_Name' => @$customer_name,
                                    'Amount_Cr' => 0,
                                    'Amount_Dr' => $payment_adjustment,
                                    'Transaction_Detail' => @$payment_notes,
                                    'Remarks' => 'Debit Entry Adjustment',                     
                                );
                                $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
                            }
                    }
                    //End Voucher 

                     if(round($find_total,2) == round($total_net_amount, 2)){
                        $amc_quotation = $this->basic_model->getCustomRow("SELECT * FROM amc_quotation  WHERE amc_quotation_id = '".$invoice_id."'");
                        $amc_quotation_detail = $this->basic_model->getRows('amc_quotation_detail', 'amc_quotation_id',$invoice_id,"amc_quotation_detail_id","DESC");
                        foreach($amc_quotation_detail as $v){
                            if($v['selected_quotation_no'] != ''){
                                $find_inv = $this->basic_model->getCustomRow("SELECT * FROM invoice  WHERE invoice_no = '".$v['selected_quotation_no']."' ");
                                $find_inv['invoice_date'] = $amc_quotation['amc_quotation_expiry_date'];
                                // $this->basic_model->customQueryDel("UPDATE amc_quotation SET amc_quotation_status=7 where amc_quotation_id=".$invoice_id);
                                $this->active_amc($find_inv);

                            }
                        }
                    }
                    $this->basic_model->customQueryDel('UPDATE amc_quotation SET has_payment = 1 WHERE amc_quotation_id = "'.$invoice_id.'"');



                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->receivables), 'fieldsEmpty' => 'yes'));
                }

            }
            
            
        }
        else{
            echo json_encode(array("error" => "Error"));
        }
               
        
        
    }

    function edit($id){
        if( isset($id) && $id != ''){
            /*$invoice = $this->basic_model->getCustomRow("SELECT * FROM `invoice` WHERE invoice_id = '".$id."' ");*/ 
            $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if( $res_edit)
            {
                $res['title'] = "Edit ".$this->page_heading;
                $res["page_title"] = "edit";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res['data'] = $res_edit;
                $res['data']['total_amount'] = @$_GET['total_amount'];
                $tot = (@floatval(@$res_edit['payment_amount']) + @floatval(@$res_edit['payment_adjustment']) );
                //$res['data']['toal_received'] = quotation_num_format(floatval(@$_GET['total_received']) - $tot) ;
                $res['data']['toal_received'] = quotation_num_format(floatval(@$_GET['total_received'])) ;
                $res['data']['overdue_since'] = @$_GET['overdue_since'];
                // $res['data']['overdue_amount'] = @number_format((float)@$_GET['overdue_amount'], 2, '.', '');
                $res['data']['toal_received']  -= $res_edit['payment_amount'] + $res_edit['payment_adjustment'];
                $res['data']['overdue_amount'] = @number_format($res['data']['total_amount'] - $res['data']['toal_received'], 2, '.', '');
                $res['data']['is_invoice'] = @$_GET['is_invoice'];
                $res['data']['payment_id'] = @$id;

                $res["customer"] = $this->basic_model->getCustomRow("SELECT * from user where user_id=".$res_edit['payment_customer_id']);
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $res['data']['pre_invoice_no'] = $res["setval"]['company_prefix'].$res["setval"]['quotation_prfx'].@$res_edit['invoice_no'];
                $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($res["setval"]["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                redirect($this->no_results_found);
            }
            
        }
        else
        {
            redirect($this->no_results_found);
        }
    }

    function makePaymentHistory($id){
        // $revision_id = $this->basic_model->insertRecord(["revision_title"=> 'AMC_Payment'],'tbl_revision');
        $payment_data = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            
        $data = array(
            "invoice_id"=> $payment_data['invoice_id'],
            "invoice_no"=> $payment_data['invoice_no'],
            "payment_currency_id" => $payment_data['payment_currency_id'],
            "receipt_no" => $payment_data['receipt_no'],
            "payment_date" => $payment_data['payment_date'],
            "payment_amount"=> $payment_data['payment_amount'],
            "payment_cheque"=> $payment_data['payment_cheque'],
            "payment_notes"=> $payment_data['payment_notes'],
            "debit_account" => $payment_data['debit_account'],
            "credit_account" => $payment_data['credit_account'],
            "payment_adjustment" => $payment_data['payment_adjustment'],
            "payment_customer_id" => $payment_data['payment_customer_id'],
            "is_invoice" => $payment_data['is_invoice'],
            "payment_revised_id" => 0,//$revision_id,
            "blongs_to" => $id,
        );

        $payment_id = $this->basic_model->insertRecord($data,$this->table);
        return true;
    }

    function invoice_payment(){
        extract($_POST);
         if(isset($id) && $id != ''){
               $payment = $this->basic_model->getCustomRows("SELECT * FROM amc_payment WHERE invoice_id = '".$id."' AND (payment_revised_id = 0 || payment_revised_id IS NULL) ");
               if($payment){
                  echo json_encode($payment);
               }else{ 
                echo json_encode([]);
               }

         }else{
                echo json_encode([]);
         }
    }
    function delete($id)
    {   $this->makePaymentHistory($id);
        $del = $this->basic_model->deleteRecord('amc_payment_id', $id, $this->table);

        $data = array(
            "flgDeleted" => 1,
        ); 
        $this->basic_model->customQueryDel('UPDATE voucher SET flgDeleted = 1 WHERE doc_id = "'.$id.'" && doc_type = 1');
        echo json_encode(array( "success" => "Record removed successfully","detailsDelete"=> true,"qoutationMultiTotalSet"=> true));
    }
    function active_amc($invoice){  
        $this->basic_model->customQueryDel('UPDATE invoice_additional SET additional_expiry_date = "'.$invoice['invoice_date'].'" WHERE invoice_id = "'.$invoice['invoice_id'].'"');
    }
   
    
   
   
} 