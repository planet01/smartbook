<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class delivery_note extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
       //$this->add_product = 'delivery_note/add';
        $this->save_product = 'delivery_note/add';
        
        $this->view_product = 'delivery_note/view'; 
        
        $this->edit_product = 'delivery_note/edit';
        $this->delete_product = 'delivery_note/delete';
        $this->detail_product = 'delivery_note/detail';
        $this->detail_page = 'delivery_note/detail_page';
        // page active
        $this->add_page_product = "add-delivery_note";
        $this->edit_page_product = "edit-delivery_note";
        $this->view_page_product = "view-delivery_note";
        $this->view_detail_product = "view_detail_delivery_note";
        // table
        $this->table = "delivery_note";
        $this->table_detail = "delivery_note_detail";
        $this->table_qd = "invoice";
        $this->tablep = "product";
        $this->tables = "setting";
        $this->print_delivery = 'delivery_note/print';
        
        $this->table_id = "delivery_note_id";
        $this->table_fid = "delivery_note_id";
        $this->table_qid = "invoice_id";
        $this->table_pid = "product_id";
        $this->table_sid = "setting_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/delivery_note";
        // Page Heading
        $this->page_heading = "Delivery Note";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 11) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->add_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function add(){
        extract($_POST);
        if( $this->input->post()){
            // print_b($_POST);
            if(empty($id))
            {
                if(empty($customer_id) || empty($delivery_note_no) || !isset($product_id) || @$product_id == null) {
                    if(!isset($product_id) || @$product_id == null){
                        echo json_encode(array("error" => "Please add item in grid"));

                    }else{
                        echo json_encode(array("error" => "Please fill all fields"));
                    }
                }else{

                    $Checkdelivery_note_detail = $this->basic_model->getRow($this->table, 'delivery_note_no', $delivery_note_no,$this->orderBy,$this->table_id);
                    if(@$Checkdelivery_note_detail != null && !empty(@$Checkdelivery_note_detail)){
                        echo json_encode(array("error" => $this->page_heading." No. is Exist"));
                        exit();
                    }

                    if (isset($delivery_note_detail_current) && @$delivery_note_detail_current != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            echo json_encode(array("error" => "Duplicate product is not allowed."));
                            return false;
                            /*foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $delivery_note_detail_current[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'delivery_note_detail_quantity' => $quantityTotal_new,
                            );*/
                        }

                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'warehouse_id' => $warehouse_id[$k],
                                    'delivery_note_detail_quantity' => $delivery_note_detail_current[$k],
                                );
                            }
                        }
                        //check already delivered or not
                        $invoice = $this->basic_model->getCustomRow("SELECT * FROM invoice WHERE invoice_no = '".$invoice_no."' ");
                        $invoice_detail_data = $this->basic_model->getCustomRows("SELECT  product.product_name AS P_name,dnd.product_id AS product_id, COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) AS delivery_quantity FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id JOIN product ON product.product_id = dnd.product_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' group by dnd.delivery_note_detail_quantity");
                        foreach ($product_check as $k => $v) {
                            $product_check_2 = $this->basic_model->getCustomRow("SELECT COALESCE( SUM(qd.invoice_detail_quantity), 0 ) AS total_quantity FROM invoice_detail qd join invoice q ON q.invoice_id = qd.invoice_id WHERE qd.product_id = '".$v['product_id']."' AND q.invoice_no = '".$invoice_no."' AND q.customer_id = '".$customer_id."' ");
                            if(empty($product_check_2) && @$product_check_2 == null)
                            {
                                $product_check_2 = $this->basic_model->getCustomRow("SELECT COALESCE( SUM(qd.quotation_detail_quantity), 0 ) AS total_quantity FROM quotation_detail qd join quotation q ON q.quotation_id = qd.quotation_id WHERE qd.product_id = '".$v['product_id']."' AND q.quotation_no = '".$invoice_no."' AND q.customer_id = '".$customer_id."' ");
                            }
                            foreach ($invoice_detail_data as $k2 => $v2) {
                                if($v['delivery_note_detail_quantity'] ==0)
                                {
                                   echo json_encode(array("error" => $v2['P_name']." quantity can't be 0"));
                                   exit(); 
                                }
                                /*if($v['product_id'] == $v2['product_id'] && $product_check_2['total_quantity'] <= $v2['delivery_quantity'])
                                {
                                   echo json_encode(array("error" => $v2['P_name']." already delivered ".$product_check_2['total_quantity']));
                                   exit(); 
                                }*/
                            }
                        }
                        
                         // print_b($product_check );
                         
                        //check available in stick
                        foreach ($product_check as $k => $v) {
                            
                            $inventory_quantity_data = inventory_quantity_by_warehouse_product_deileverable($v['product_id'],$v['warehouse_id']);
                            $product_data = $this->basic_model->getCustomRow("SELECT * FROM `product` WHERE `product_id` = '".$v['product_id']."' "); 
                            // echo $product_data['product_id'].' Name: '.$product_data['product_name'].' Stock: '.$inventory_quantity_data;
                            if ($v['delivery_note_detail_quantity'] > $inventory_quantity_data) {
                                echo json_encode(array("error" => $product_data['product_name']." Has Been Out Of Stock. Avaiable: ".$inventory_quantity_data));
                                exit();
                            }
                            
                            // break;
                        }

                        
                    }
                    // product_check end.
                    // print_b($delivery_date);
                    $delivery_note_date_year = explode("-",$delivery_date)[0];
                    $delivery_note_date_mnth = explode("-",$delivery_date)[1];
                    $delivery_note_no = serial_no_delivery_note($delivery_note_date_year,$delivery_note_date_mnth);
                         
                    $data= array(
                        'customer_id' => $customer_id,
                        'employee_id' => empty($employee_id)? 0 : $employee_id,
                        'invoice_id' => $invoice_no,
                        'delivery_note_no' => $delivery_note_no,//$delivery_note_no,
                        'warehouse_id' => 0,
                        'delivery_date' => @$delivery_date,
                        'delivery_notes' => @$delivery_notes,
                        'delivery_po_no' => @$delivery_po_no,
                        'invoice_type' => @$invoice_type,
                    );
                    $this->basic_model->customQueryDel("UPDATE delivery_note SET is_edit =1 WHERE customer_id ='".$customer_id."' AND invoice_id='".$invoice_no."' ");
                    if(isset($invoice_type) && $invoice_type == 2){
                        $find_proforma =  $this->basic_model->getCustomRow("SELECT * from invoice where invoice_no= '".$invoice_no."'  AND revision = 0");
                        if(!empty($find_proforma)){
                            $this->basic_model->customQueryDel("UPDATE delivery_note SET is_edit =1 WHERE customer_id ='".$customer_id."' AND invoice_id='".$find_proforma['invoice_proforma_no']."'");
                        }
                    }
                    $delivery_note_id = $this->basic_model->insertRecord($data,$this->table);
                    if(isset($invoice_type) && $invoice_type == 1){
                        if(!empty($invoice_no) && $invoice_no != null && $invoice_no != ''){
                            $this->basic_model->customQueryDel('UPDATE quotation SET has_invoice = 1 WHERE quotation_no = "'.$invoice_no.'"');
                        }
                    }

                    if (isset($delivery_note_detail_current) && @$delivery_note_detail_current != null && (isset($product_id) && @$product_id != null)) {

                        $i = 0;
                        foreach ($delivery_note_detail_current as $k => $v) {
                            if ($delivery_note_detail_current[$k] > 0) {
                                $inv = $this->basic_model->getCustomRow("SELECT * FROM inventory WHERE product_id = '".$product_id[$k]."' ");
                                if(empty($inv) || $inv == null){
                                    $ar3 = array(
                                        'product_id' => $product_id[$k],
                                        'warehouse_id' => $warehouse_id[$k],
                                        //Jd'belongs_to' => 0,
                                        //Jd'revision' => 0,
                                        'inventory_quantity' => 0,
                                        'inventory_is_active' => 1,
                                        'inventory_type' => 1,
                                        'dn_id' => $delivery_note_id,
                                        'doc_create_date' => $delivery_date
                                    );
                                  $this->basic_model->insertRecord($ar3,'inventory');
                                }
                                // out from inventory_quantity
                                $arr2 = array(
                                    'product_id' => $product_id[$k],
                                    'warehouse_id' => $warehouse_id[$k],
                                    //Jd'belongs_to' => 0,
                                    //Jd'revision' => 0,
                                    'inventory_quantity' => $delivery_note_detail_current[$k],
                                    'inventory_is_active' => 1,
                                    'inventory_type' => 0,
                                    'dn_id' => $delivery_note_id,
                                    'doc_create_date' => $delivery_date
                                );

                                $inventory_id = $this->basic_model->insertRecord($arr2,'inventory');

                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'warehouse_id' => $warehouse_id[$k],
                                    'delivery_note_id' => $delivery_note_id,
                                    'inventory_id' => $inventory_id,
                                    'delivery_note_detail_description' => $delivery_note_detail_description[$k],
                                    'delivery_note_detail_quantity' => $delivery_note_detail_current[$k],
                                    'delivery_note_detail_pending' => $delivery_note_detail_pending[$k],
                                    'delivery_note_detail_delivered' => (!isset($delivery_note_detail_delivered[$k]) || empty($delivery_note_detail_delivered[$k]) ? 0 : $delivery_note_detail_delivered[$k]),
                                    'delivery_note_detail_total_quantity' => $delivery_note_detail_quantity[$k],
                                    'usd_price' => @$usd_price[$k],
                                    'base_price' => @$base_price[$k],

                                );
                                $i++;

                                // echo '<pre>';
                                // print_r($arr);
                                $delivery_note_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            }
                        }
                    }
                    
                    echo json_encode(array( "success" => "Record Inserted successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }else{

                if(empty($customer_id) || empty($delivery_note_no) || !isset($product_id) || @$product_id == null) {

                    if(!isset($product_id) || @$product_id == null){
                        echo json_encode(array("error" => "Please add item in grid"));

                    }else{
                        echo json_encode(array("error" => "Please fill all fields"));
                    }
                }
                else{
                    
                    $Checkdelivery_note_no = $this->basic_model->getRow($this->table, 'delivery_note_no', $delivery_note_no,$this->orderBy,$this->table_id);
                    if(@$Checkdelivery_note_no != null && !empty(@$Checkdelivery_note_no)){
                        if ($Checkdelivery_note_no['delivery_note_no'] != $delivery_note_no_old) {
                            echo json_encode(array("error" => "Delivery Note No. is Exist"));
                            exit();
                        }
                    }

                    if (isset($delivery_note_detail_current) && @$delivery_note_detail_current != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                             echo json_encode(array("error" => "Duplicate product is not allowed."));
                            return false;
                            /*foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $delivery_note_detail_current[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'delivery_note_detail_quantity' => $quantityTotal_new,
                            );*/
                        }

                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'warehouse_id' => $warehouse_id[$k],
                                    'delivery_note_detail_quantity' => $delivery_note_detail_current[$k],
                                );
                            }
                        }
                        //check already delivered or not
                        $invoice = $this->basic_model->getCustomRow("SELECT * FROM invoice WHERE customer_id = '".$customer_id."' AND invoice_no = '".$invoice_no."' ");
                        $invoice_detail_data = $this->basic_model->getCustomRows("SELECT  product.product_name AS P_name,dnd.product_id AS product_id, COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) AS delivery_quantity FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id JOIN product ON product.product_id = dnd.product_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' AND dnd.delivery_note_id != '".$id."' group by dnd.delivery_note_detail_quantity");
                        foreach ($product_check as $k => $v) {
                            $product_check_2 = $this->basic_model->getCustomRow("SELECT COALESCE( SUM(qd.invoice_detail_quantity), 0 ) AS total_quantity FROM invoice_detail qd join invoice q ON q.invoice_id = qd.invoice_id WHERE qd.product_id = '".$v['product_id']."' AND q.invoice_no = '".$invoice_no."' AND q.customer_id = '".$customer_id."' ");
                            if(empty($product_check_2) && @$product_check_2 == null)
                            {
                                $product_check_2 = $this->basic_model->getCustomRow("SELECT COALESCE( SUM(qd.quotation_detail_quantity), 0 ) AS total_quantity FROM quotation_detail qd join quotation q ON q.quotation_id = qd.quotation_id WHERE qd.product_id = '".$v['product_id']."' AND q.quotation_no = '".$invoice_no."' AND q.customer_id = '".$customer_id."' ");
                            }
                                
                            foreach ($invoice_detail_data as $k2 => $v2) {
                                if($v['delivery_note_detail_quantity'] ==0)
                                {
                                   echo json_encode(array("error" => $v2['P_name']." quantity can't be 0"));
                                   exit(); 
                                }
                                /*if($v['product_id'] == $v2['product_id'] && $product_check_2['total_quantity'] <= $v2['delivery_quantity'])
                                {
                                   echo json_encode(array("error" => $v2['P_name']." already delivered ".$v2['delivery_quantity']));
                                   exit(); 
                                }*/
                            }
                        }
                        
                        // print_b($product_check );
                       
                        //check available in stick
                       foreach ($product_check as $k => $v) {
                            
                            $inventory_quantity_data = inventory_quantity_by_warehouse_product_deileverable($v['product_id'],$v['warehouse_id']);
                            $product_data = $this->basic_model->getCustomRow("SELECT * FROM `product` WHERE `product_id` = '".$v['product_id']."' "); 
                            // echo $product_data['product_id'].' Name: '.$product_data['product_name'].' Stock: '.$inventory_quantity_data;
                            if ($v['delivery_note_detail_quantity'] > $inventory_quantity_data) {
                                echo json_encode(array("error" => $product_data['product_name']." Has Been Out Of Stock. Avaiable: ".$inventory_quantity_data));
                                exit();
                            }
                            
                            // break;
                        }
                        
                        
                    }
                    // product_check end.

                    $data= array(
                        'customer_id' => $customer_id,
                        'employee_id' => empty($employee_id)? 0 : $employee_id,
                        'invoice_id' => $invoice_no,
                        'delivery_note_no' => $delivery_note_no,
                        'warehouse_id' => 0,
                        'delivery_date' => @$delivery_date,
                        'delivery_notes' => @$delivery_notes,
                        'delivery_po_no' => @$delivery_po_no,
                        'invoice_type' => @$invoice_type,
                        
                    );

                    $delivery_note_detail_data = $this->basic_model->getRows($this->table_detail,$this->table_fid,$id,"delivery_note_detail_id","DESC");
                    $del = $this->basic_model->deleteRecord($this->table_fid, $id, $this->table_detail);
                    if(!empty($invoice_no) && $invoice_no != null && $invoice_no != ''){
                        $this->basic_model->customQueryDel('UPDATE quotation SET has_invoice = 1 WHERE quotation_no = "'.$invoice_no.'"');
                    }
                    // print_b($quantity);
                    if (isset($delivery_note_detail_quantity) && @$delivery_note_detail_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        
                        foreach ($delivery_note_detail_current as $k => $v) {
                            if ($delivery_note_detail_current[$k] > 0) {
                                $inv = $this->basic_model->getCustomRow("SELECT * FROM inventory WHERE product_id = '".$product_id[$k]."' ");
                                if(empty($inv) || $inv == null){
                                    $ar3 = array(
                                        'product_id' => $product_id[$k],
                                        'warehouse_id' => $warehouse_id[$k],
                                        //Jd'belongs_to' => 0,
                                        //Jd'revision' => 0,
                                        'inventory_quantity' => 0,
                                        'inventory_is_active' => 1,
                                        'inventory_type' => 1,
                                        'dn_id' => $id,
                                        'doc_create_date' => $delivery_date
                                    );
                                  $this->basic_model->insertRecord($ar3,'inventory');
                                }
                                // out from inventory_quantity
                                $arr2 = array(
                                    'product_id' => $product_id[$k],
                                    'warehouse_id' => $warehouse_id[$k],
                                    //Jd'belongs_to' => 0,
                                    //Jd'revision' => 0,
                                    'inventory_quantity' => $delivery_note_detail_current[$k],
                                    'inventory_is_active' => 1,
                                    'inventory_type' => 0,
                                    'dn_id' => $id,
                                    'doc_create_date' => $delivery_date
                                );
                                
                                $inventory_id = $this->basic_model->insertRecord($arr2,'inventory');

                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'warehouse_id' => $warehouse_id[$k],
                                    'delivery_note_id' => $id,
                                    'inventory_id' => $inventory_id,
                                    'delivery_note_detail_description' => $delivery_note_detail_description[$k],
                                    'delivery_note_detail_quantity' => $delivery_note_detail_current[$k],
                                    'delivery_note_detail_pending' => $delivery_note_detail_pending[$k],
                                    'delivery_note_detail_delivered' => (!isset($delivery_note_detail_delivered[$k]) || empty($delivery_note_detail_delivered[$k]) ? 0 : $delivery_note_detail_delivered[$k]),
                                    'delivery_note_detail_total_quantity' => $delivery_note_detail_quantity[$k],
                                    'usd_price' => @$usd_price[$k],
                                    'base_price' => @$base_price[$k],
                                );
                                $i++;
                                foreach ($delivery_note_detail_data as $k2 => $v2) 
                                {
                                    if($v2['product_id'] == $product_id[$k])
                                    {
                                        $this->basic_model->deleteRecord('inventory_id', $v2['inventory_id'], 'inventory');
                                    }
                                }
                                // echo '<pre>';
                                // print_r($arr);
                                $delivery_note_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            }
                        }
                    }
                    // print_b($arr);
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                    
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            // $this->stock_notification();
        }else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                // $res["add_product"] = $this->add_product;
                $res["save_product"] = $this->save_product;
                $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 "); 
                $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1"); 
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                /*$res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 AND p.can_deliver = 0 GROUP BY p.product_id ORDER BY p.product_name ASC"); */
                $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p where p.product_is_active = 1 AND p.can_deliver = 0 GROUP BY p.product_id ORDER BY p.product_name ASC"); 
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
                $res['setting'] = $setting;
                $res["ckeditor"] = "yes";
                $this->template_view($this->add_page_product,$res);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
        }  
    }

    function edit($id){
       
        if ($this->check_edit) {

            $this->edit_data($id, 'edit'); 
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail_page($id){
       $this->edit_data($id, 'view'); 
    }

    function edit_data($id, $view_data){

        if( isset($id) && $id != ''){
            $res_edit = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);

            if($res_edit){
                extract($_POST);
                $customer_id = $res_edit['customer_id'];
                $invoice_no = $res_edit['invoice_id'];
                $warehouse_id = $res_edit['warehouse_id'];
                $res["ckeditor"] = "yes";
                //invoice
                $invoice = $this->basic_model->getCustomRow("SELECT * FROM invoice WHERE customer_id = '".$customer_id."' AND invoice_no = '".$invoice_no."' ");
                $invoice_data = $this->basic_model->getCustomRows("SELECT p.product_id AS p_id, p.product_name, p.product_sku, ( SELECT COALESCE( SUM(inv_d2.invoice_detail_quantity), 0 ) FROM invoice_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.invoice_id = '".$invoice['invoice_id']."' ) AS invoice_total_quantity_per_product, ( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' AND p.product_id = dnd.product_id ) AS delivery_note_inventory_total_quantity_per_product FROM `invoice` inv JOIN `invoice_detail` inv_d ON inv_d.invoice_id = inv.invoice_id JOIN product p ON p.product_id = inv_d.product_id WHERE inv.invoice_id = '".$invoice['invoice_id']."' AND p.can_deliver = 0");

                $res['invoice_data'] = $invoice_data;
                for( $i= 0;$i<sizeof($res['invoice_data']);$i++)
                {
                    $res['invoice_data'][$i]['inventory_total_quantity'] = inventory_quantity_by_warehouse_product_deileverable($res['invoice_data'][$i]['p_id'],$warehouse_id);    
                }
                  
                
                $invoice = '';
                //perfoma invoice
                if(empty($res['invoice_data']) && @$res['invoice_data'] == null)
                {
                    $invoice = $this->basic_model->getCustomRow("SELECT * FROM quotation WHERE customer_id = '".$customer_id."' AND quotation_no = '".$invoice_no."' AND quotation_status= '2' ");
                    $query = "SELECT p.product_id AS p_id, p.product_name, p.product_sku, ( SELECT COALESCE( SUM(inv_d2.quotation_detail_quantity), 0 ) FROM quotation_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.quotation_id = '".$invoice['quotation_id']."' ) AS invoice_total_quantity_per_product, ( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' AND p.product_id = dnd.product_id ) AS delivery_note_inventory_total_quantity_per_product FROM `quotation` inv JOIN `quotation_detail` inv_d ON inv_d.quotation_id = inv.quotation_id JOIN product p ON p.product_id = inv_d.product_id WHERE inv.quotation_id = '".$invoice['quotation_id']."' AND p.can_deliver = 0";
                    $invoice_data_temp = $this->basic_model->getCustomRows($query);
                    $res['invoice_data'] = $invoice_data_temp;
                    for( $i= 0;$i<sizeof($res['invoice_data']);$i++)
                    {
                        $res['invoice_data'][$i]['inventory_total_quantity'] = inventory_quantity_by_warehouse_product_deileverable($res['invoice_data'][$i]['p_id'],$warehouse_id);    
                    }
                    
                   
                    
                }
                $res["title"] = "Edit ".$this->page_heading;
                $res["page_title"] =  "";
                $res["page_heading"] = $this->page_heading;
                $res["active"] = $this->edit_page_product;
                $res["save_product"] =  $this->save_product;
                // $res['image_upload_dir'] = $this->image_upload_dir;
                // $res['countries'] = $this->basic_model->countries;
                $res["data"] = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 "); 
                // print_b($res["data"]);
                $res["delivery_note_detail_data"] = $this->basic_model->getRows($this->table_detail,$this->table_fid,$res["data"]['delivery_note_id'],"delivery_note_detail_id","ASC");
                // print_b($res["delivery_note_detail_data"]);
                $res['invoiceData'] = $this->basic_model->getCustomRows("SELECT * FROM `invoice`");  
                /*$res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 AND p.can_deliver = 0 GROUP BY p.product_id ORDER BY p.product_name ASC");*/
                 $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p where p.product_is_active = 1 AND p.can_deliver = 0 GROUP BY p.product_id ORDER BY p.product_name ASC"); 
                $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1"); 
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);

                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where setting_id = '1' ");
                $res['setting'] = $setting;
                // print_b($res['invoice_data']);
                if($view_data == 'edit'){
  
                    $this->template_view($this->add_page_product,$res);
                }else{

                    $res['view_page'] = $this->view_product;
                    $this->template_view($this->view_detail_product,$res);  
                }
            }else{
                redirect($this->no_results_found);    
            }
        }else{
            redirect($this->no_results_found);
        }
    }

    function progt(){
        if($_POST['pid']) {
            $customer_id = $_POST['customer_id'];
            $invoice_no = $_POST['invoice_id'];
            $warehouse_id = $_POST['warehouse_id'];
            $invoice = $this->basic_model->getCustomRow("SELECT * FROM invoice WHERE customer_id = '".$customer_id."' AND invoice_no = '".$invoice_no."' ");
            $user = $this->basic_model->getCustomRow("SELECT * FROM user
             LEFT JOIN price ON price.plan_id = user.plan_type 
             LEFT JOIN customer_type ON customer_type.id = user.customer_type
             WHERE user.user_id = '".$customer_id."' AND price.prod_id = ".$_POST['pid']);
            
            $data = array();
            if(empty($invoice) && @$invoice == null)
            {
                $invoice = $this->basic_model->getCustomRow("SELECT * FROM quotation WHERE customer_id = '".$customer_id."' AND quotation_no = '".$invoice_no."' AND quotation_status= '2' ");
                $data = $this->basic_model->getCustomRow("SELECT ( SELECT COALESCE( SUM(inv_d2.quotation_detail_quantity), 0 ) FROM quotation_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.quotation_id = '".$invoice['quotation_id']."' ) AS invoice_total_quantity_per_product,( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' AND product.product_id = dnd.product_id ) AS delivery_note_inventory_total_quantity_per_product FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` JOIN quotation_detail inv_d ON inv_d.quotation_id = '".$invoice['quotation_id']."' WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$_POST['pid']."' AND product_is_active = 1 AND product.can_deliver = '0'");
                $data['inventory_total_quantity'] = inventory_quantity_by_warehouse_product_deileverable($_POST['pid'],$warehouse_id);
                //echo "SELECT *,( SELECT COALESCE( SUM(inv_d2.quotation_detail_quantity), 0 ) FROM quotation_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.quotation_id = '".$invoice['quotation_id']."' ) AS invoice_total_quantity_per_product,( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' AND product.product_id = dnd.product_id ) AS delivery_note_inventory_total_quantity_per_product,( SELECT COALESCE(SUM(inventory_quantity), 0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory`.`revision`= 0 AND `inventory_type` = 0 AND `product_id` = '".$_POST['pid']."') AS total_inventory_quantity FROM inventory i3 WHERE i3.revision=0 AND i3.inventory_type = 1 AND i3.product_id = '".$_POST['pid']."' ) AS inventory_total_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` JOIN quotation_detail inv_d ON inv_d.quotation_id = '".$invoice['quotation_id']."' WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$_POST['pid']."' AND product_is_active = 1 AND product.can_deliver = '0' AND ( inventory.revision = 0 || inventory.revision IS NULL )";
            }
            else
            {
                $data = $this->basic_model->getCustomRow("SELECT p.product_id AS p_id, p.product_name, ( SELECT COALESCE( SUM(inv_d2.invoice_detail_quantity), 0 ) FROM invoice_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.invoice_id = '".$invoice['invoice_id']."' ) AS invoice_total_quantity_per_product, ( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' AND p.product_id = dnd.product_id ) AS delivery_note_inventory_total_quantity_per_product FROM `invoice` inv JOIN `invoice_detail` inv_d ON inv_d.invoice_id = inv.invoice_id JOIN product p ON p.product_id = inv_d.product_id WHERE inv.invoice_id = '".$invoice['invoice_id']."' AND inv.belongs_to = '0'AND p.can_deliver = 0 AND `p`.`product_id` = '".$_POST['pid']."' ");
                $data['inventory_total_quantity'] = inventory_quantity_by_warehouse_product_deileverable($_POST['pid'],$warehouse_id);
                
            }
            
             $data['usd_price'] = $user[strtolower($user['title']).'_usd_price'];
             $data['base_price'] = $user[strtolower($user['title']).'_base_price'];
            
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    function customer_invoices()
    {
        if($_POST['customer_id']) {
            

            $data['invoice'] = $this->basic_model->getCustomRows("SELECT * from invoice WHERE customer_id = '".$_POST['customer_id']."' AND revision = 0");
            $data['p_invoice'] = $this->basic_model->getCustomRows("SELECT * from quotation WHERE customer_id = '".$_POST['customer_id']."' AND quotation_status = '2' ");
            
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    function invoice_data()
    {
        // print_b($_POST);
        if($_POST['invoice_id']) {
            //$customer_id = $_POST['customer_id'];
            $invoice_no = $_POST['invoice_id'];
            $invoice_type = $_POST['invoice_type'];
            $warehouse_id = $_POST['warehouse_id'];
            $po_no = "";

            //invoice
            //customer_id = '".$customer_id."' AND
            if($invoice_type == 2)
            {

                $query = "SELECT * FROM invoice WHERE  invoice_no = '$invoice_no' AND (revision = '0' OR revision IS NULL) ";
                $invoice = $this->basic_model->getCustomRow($query);
                $po_no = @$invoice['invoice_po_no'];
                $po_no1 = @$invoice['invoice_proforma_no'];
                
                $cond1 = " inv_d2.invoice_id = '".$invoice['invoice_id']."' ";
                $cond2 = " inv.invoice_id = '".$invoice['invoice_id']."' ";
                $cond3 = " dn.invoice_id = '".$invoice_no."' ";
                if($po_no1 != '' && $po_no1 != 0){
                /*$cond1 = " (inv_d2.invoice_id = '".$invoice['invoice_id']."' || inv_d2.invoice_id = '".$po_no."') ";
                $cond2 = " (inv.invoice_id = '".$invoice['invoice_id']."' || inv.invoice_id = '".$po_no."') ";*/
                $cond3 = " (dn.invoice_id = '".$invoice_no."' || dn.invoice_id = '".$po_no1."') ";
                }
                //dn.customer_id = '".$customer_id."'  AND
                $query = "SELECT p.product_id AS p_id, p.product_name, p.product_sku, inv.customer_id, inv_d.invoice_detail_base_rate as quotation_detail_rate, inv_d.invoice_detail_usd_rate as  quotation_detail_rate_usd, inv.employee_id, ( SELECT COALESCE( SUM(inv_d2.invoice_detail_quantity), 0 ) FROM invoice_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND ".$cond1." ) AS invoice_total_quantity_per_product, ( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE p.product_id = dnd.product_id AND ".$cond3.") AS delivery_note_inventory_total_quantity_per_product FROM `invoice` inv JOIN `invoice_detail` inv_d ON inv_d.invoice_id = inv.invoice_id JOIN product p ON p.product_id = inv_d.product_id WHERE (inv.revision = '0' OR inv.revision IS NULL) AND p.can_deliver = 0  AND ".$cond2;
                $invoice_data = $this->basic_model->getCustomRows($query);
                $data['invoice_data'] = $invoice_data;
                for( $i= 0;$i<sizeof($data['invoice_data']);$i++)
                {
                    $data['invoice_data'][$i]['inventory_total_quantity'] = inventory_quantity_by_warehouse_product_deileverable($data['invoice_data'][$i]['p_id'],$warehouse_id);    
                }
                $data['invoice_type'] = 2;
                $data['customer_id']  = @$invoice['customer_id'];
                $data['employee_id']  = @$invoice['employee_id'];
                if(!empty($data['invoice_data']) && @$data['invoice_data'] != null && @$invoice['invoice_proforma_no'] != ''  && @$invoice['invoice_proforma_no'] != null){
                    $query = "SELECT delivery_note_detail.*, delivery_note.*, product.product_id, product.product_name, product.product_sku,  sum(delivery_note_detail.delivery_note_detail_total_quantity) AS invoice_total_quantity_per_product, sum(delivery_note_detail.delivery_note_detail_quantity) As delivery_note_inventory_total_quantity_per_product FROM delivery_note  
                    Left join delivery_note_detail ON delivery_note.delivery_note_id = delivery_note_detail.delivery_note_id
                    Left join product ON  delivery_note_detail.product_id = product.product_id
                    WHERE  delivery_note.invoice_id = '".$invoice['invoice_proforma_no']."' group by delivery_note_detail.product_id";
                    $d_note = $this->basic_model->getCustomRows($query);

                    $cid = 0;
                    foreach($d_note as $dn){
                        $is_found = 0;
                        for($i= 0; $i<sizeof($data['invoice_data']); $i++){
                            $cid = $data['invoice_data'][$i]['customer_id'];
                            if($dn['product_id'] == $data['invoice_data'][$i]['p_id']){
                                $data['invoice_data'][$i]['delivery_note_inventory_total_quantity_per_product'] = $dn['delivery_note_inventory_total_quantity_per_product'];
                                $is_found = 1;
                                break;
                            } 

                        }
                        if($is_found == 0){
                            $temp = Array(
                                "p_id" => $dn['product_id'],
                                'product_name' => $dn['product_name'],
                                "customer_id" => $cid,
                                "employee_id" => @$invoice['employee_id'],
                                "delivery_note_inventory_total_quantity_per_product" => $dn['delivery_note_inventory_total_quantity_per_product'],
                                "inventory_total_quantity" => inventory_quantity_by_warehouse_product_deileverable($dn['product_id'],$warehouse_id),
                                "invoice_total_quantity_per_product" => $dn['invoice_total_quantity_per_product'],
                            );
                            array_push($data['invoice_data'], $temp);
                        }
                    }

                }
            }
            
            $invoice = '';

            //perfoma invoice
            if($invoice_type == 1)
            {
                $check_pi = $this->basic_model->getCustomRow("Select * from invoice where invoice_proforma_no = '".$invoice_no."'");
               /* print_b("kbb");
                if(empty($check_pi) && $check_pi == null){
                    $data = array();
                    echo json_encode($data);
                    return false;
                }*/
                //customer_id = '".$customer_id."' AND
                $query = "SELECT * FROM quotation WHERE  quotation_no = '".$invoice_no."' AND quotation_status= '4' AND quotation_revised_id = '0' ";
                $invoice = $this->basic_model->getCustomRow($query);
                $po_no = @$invoice['quotation_po_no'];

                // print_b($invoice);
                //dn.customer_id = '".$customer_id."'  AND
                $query = "SELECT p.product_id AS p_id, p.product_name, p.product_sku, inv_d.quotation_detail_rate, inv_d.quotation_detail_rate_usd, ( SELECT COALESCE( SUM(inv_d2.quotation_detail_quantity), 0 ) FROM quotation_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.quotation_id = '".$invoice['quotation_id']."' ) AS invoice_total_quantity_per_product, ( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE  dn.invoice_id = '".$invoice_no."' AND p.product_id = dnd.product_id ) AS delivery_note_inventory_total_quantity_per_product FROM `quotation` inv JOIN `quotation_detail` inv_d ON inv_d.quotation_id = inv.quotation_id JOIN product p ON p.product_id = inv_d.product_id WHERE inv.quotation_id = '".$invoice['quotation_id']."' AND inv.quotation_revised_id = '0' AND p.can_deliver = 0";
                // echo $query;
                // die;
                $invoice_data_temp = $this->basic_model->getCustomRows($query);

                $data['invoice_data'] = $invoice_data_temp;
                for( $i= 0;$i<sizeof($data['invoice_data']);$i++)
                {
                    $data['invoice_data'][$i]['inventory_total_quantity'] = inventory_quantity_by_warehouse_product_deileverable($data['invoice_data'][$i]['p_id'],$warehouse_id);    
                }
                
                $data['invoice_type'] = 1;
                $data['customer_id']  = @$invoice['customer_id'];
                $data['employee_id']  = @$invoice['employee_id'];

                
            }

            //invoice
            // $invoice = $this->basic_model->getCustomRow("SELECT * FROM invoice WHERE customer_id = '".$customer_id."' AND invoice_no = '".$invoice_no."' ");
            // $invoice_data = $this->basic_model->getCustomRows("SELECT p.product_id, p.product_name, ( SELECT COALESCE( SUM(inv_d2.invoice_detail_quantity), 0 ) FROM invoice_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.invoice_id = '".$invoice['invoice_id']."' ) AS invoice_total_quantity_per_product, ( SELECT COALESCE(SUM(inventory_quantity), 0) FROM inventory i2 WHERE i2.revision AND i2.inventory_type = 0 AND i2.product_id = i.product_id ) AS delivery_note_inventory_total_quantity_per_product, ( SELECT COALESCE(SUM(inventory_quantity), 0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory`.`revision`= 0 AND `inventory_type` = 0 AND `product_id` = i.product_id) AS total_inventory_quantity FROM inventory i3 WHERE i3.revision=0 AND i3.inventory_type = 1 AND i3.product_id = i.product_id ) AS inventory_total_quantity FROM `delivery_note` dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id JOIN invoice_detail inv_d ON inv_d.invoice_id = '".$invoice['invoice_id']."' JOIN inventory i ON i.revision=0 AND i.inventory_id = dnd.inventory_id JOIN product p ON p.product_id = dnd.product_id WHERE dn.invoice_id = '".$invoice['invoice_id']."' AND i.inventory_type = 0 AND p.can_deliver = '0' ");

            // if (empty($invoice_data) && @$invoice_data == null) {
            //     $invoice_data2 = $this->basic_model->getCustomRows("SELECT p.product_id, p.product_name, ( SELECT COALESCE( SUM(inv_d2.invoice_detail_quantity), 0 ) FROM invoice_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.invoice_id = '".$invoice['invoice_id']."' ) AS invoice_total_quantity_per_product, 0 AS delivery_note_inventory_total_quantity_per_product, ( SELECT COALESCE(SUM(inventory_quantity), 0) -( SELECT COALESCE(SUM(`inventory_quantity`), 0) FROM `inventory` WHERE `inventory`.`revision` = 0 AND `inventory_type` = 0 AND `product_id` = inv_d.product_id ) AS total_inventory_quantity FROM inventory i3 WHERE i3.revision=0 AND i3.inventory_type = 1 AND i3.product_id = inv_d.product_id ) AS inventory_total_quantity FROM `invoice` inv JOIN `invoice_detail` inv_d ON inv_d.invoice_id = inv.invoice_id JOIN product p ON p.product_id = inv_d.product_id WHERE inv.invoice_id = '".$invoice['invoice_id']."' AND p.can_deliver = '0'");

            //     $data['invoice_data'] = $invoice_data2;
            //     $data['invoice_type'] = 2;
            // }else{
                
            //     $data['invoice_data'] = $invoice_data;
            //     $data['invoice_type'] = 2;
            // }
            // $invoice = '';

            // //perfoma invoice
            // if(empty($data['invoice_data']) && @$data['invoice_data'] == null)
            // {
            //     $invoice = $this->basic_model->getCustomRow("SELECT * FROM quotation WHERE customer_id = '".$customer_id."' AND quotation_no = '".$invoice_no."' AND quotation_status= '2' ");
            //     $query = "SELECT p.product_id, p.product_name, ( SELECT COALESCE( SUM(inv_d2.quotation_detail_quantity), 0 ) FROM quotation_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.quotation_id = '".$invoice['quotation_id']."' ) AS invoice_total_quantity_per_product, ( SELECT COALESCE(SUM(dnd.delivery_note_detail_quantity), 0) FROM delivery_note dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id  WHERE dn.customer_id = '".$customer_id."'  AND dn.invoice_id = '".$invoice_no."' ) AS delivery_note_inventory_total_quantity_per_product, ( SELECT COALESCE(SUM(inventory_quantity), 0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = i.product_id) AS total_inventory_quantity FROM inventory i3 WHERE i3.inventory_type = 1 AND i3.product_id = i.product_id ) AS inventory_total_quantity FROM `delivery_note` dn JOIN delivery_note_detail dnd ON dn.delivery_note_id = dnd.delivery_note_id JOIN quotation_detail inv_d ON inv_d.quotation_id = '".$invoice['quotation_id']."' JOIN inventory i ON i.inventory_id = dnd.inventory_id JOIN product p ON p.product_id = dnd.product_id WHERE dn.invoice_id = '".$invoice['quotation_id']."' AND i.inventory_type = 0 AND p.can_deliver = '0'";
            //     $invoice_data_temp = $this->basic_model->getCustomRows($query);

            //     if (empty($invoice_data_temp) && @$invoice_data_temp == null) {
            //         $query = "SELECT p.product_id, p.product_name, ( SELECT COALESCE( SUM(inv_d2.quotation_detail_quantity), 0 ) FROM quotation_detail inv_d2 WHERE inv_d2.product_id = inv_d.product_id AND inv_d2.quotation_id = '".$invoice['quotation_id']."' ) AS invoice_total_quantity_per_product, 0 AS delivery_note_inventory_total_quantity_per_product, ( SELECT COALESCE(SUM(inventory_quantity), 0) -( SELECT COALESCE(SUM(`inventory_quantity`), 0) FROM `inventory` WHERE `inventory`.`revision` = 0 AND `inventory_type` = 0 AND `product_id` = inv_d.product_id ) AS total_inventory_quantity FROM inventory i3 WHERE i3.revision=0 AND i3.inventory_type = 1 AND i3.product_id = inv_d.product_id ) AS inventory_total_quantity FROM `quotation` inv JOIN `quotation_detail` inv_d ON inv_d.quotation_id = inv.quotation_id JOIN product p ON p.product_id = inv_d.product_id WHERE inv.quotation_id = '".$invoice['quotation_id']."' AND p.can_deliver = '0'";
            //         $invoice_data2 = $this->basic_model->getCustomRows($query);

            //         $data['invoice_data'] = $invoice_data2;
            //         $data['invoice_type'] = 1;
            //     }else{
                    
            //         $data['invoice_data'] = $invoice_data_temp;
            //         $data['invoice_type'] = 1;
            //     }
            // }
            
            
            // $data['invoice_detail_data'] = $invoice_detail_data;
            // $data['product_data'] = $product_data;

            // $invoice_detail_data = $this->basic_model->getCustomRows("SELECT * FROM invoice_detail WHERE invoice_id = '".$_POST['invoice_id']."'");


            // print_b($data);
            $data['po_no'] = $po_no;
            echo json_encode($data);
        }else{
            $data = array();
            echo json_encode($data);
        }
            // echo("<pre>");
            // print_r($invoice_data);
            // print_b($invoice_detail_data);
            // print_r($product_data);
            // echo("</pre>");
            // die;
            /*foreach ($invoice_detail_data as $k => $v) {  
                $html = "";
                $html .= "<tr class='txtMult'>";
                $html .= "<td class='text-center' style='width: 60px'>";
                $html .= "<a href='javascript:void(0);' class='remCF'>Remove</a>";
                $html .= "</td>";
                $html .= "<td>";
                $html .= "<select name='product_id[]' style='width: 200px' id='pproduct_id".$k."' class='form-control prod_name' required>";
                $html .= "<option selected disabled>--- Select Product ---</option>";
                foreach($product_data as $k2 => $product_v){ 
                    $html .= "<option value='".$product_v['product_id']."' ".( $product_v['product_id']== @$v['product_id'])? 'selected': ''." >";
                    $html .= $product_v['product_name'];
                    $html .= "</option>";
                }
                $html .= "</select>";
                $html .= "</td>";
                $html .= "<td>";
                $html .="<input type='text' class='code street' style='width: 200px' id='ddiscount_type".$k."' name='delivery_note_detail_description[]' value='".@$v['invoice_detail_description']."' placeholder='' />";
                $html .= "</td>";
                $html .= "<td>";
                $html .= "<select name='delivery_note_detail_total_discount_type[]' style='width: 200px' class='form-control discount_type'>";

                $html .= "<option value='' ".( $v['invoice_detail_total_discount_type']== 0)? 'selected': ''.">-- select discount type --</option>";
                $html .= "<option value='1' ".( $v['invoice_detail_total_discount_type']== 1)? 'selected': ''.">Percentage (%)</option>";
                $html .= "<option value='2' ".( $v['invoice_detail_total_discount_type']== 2)? 'selected': ''.">Amount (Number)</option>";
                                              
                $html .= "</select>";
                $html .= "</td>";
                $html .= "<td>";
                $html .= "<input type='text' style='width: 60px' class='code discount_amount txtboxToFilter' id='ddiscount_amount<?= $k; ?>' name='delivery_note_detail_total_discount_amount[]' value='". $v['invoice_detail_total_discount_amount']."' placeholder='' />";
                $html .= "</td>";
                $html .= "<td>";
                $html .= "<input type='text' style='width: 60px' class='code quantity txtboxToFilter quatity_num' data-optional='0' id='quantity' name='delivery_note_detail_quantity[]' value='".$v['invoice_detail_quantity']."' placeholder='' />";
                $html .= "</td>";
                $html .= "<td>";
                $html .= "<input type='text' style='width: 60px' readonly='readonly' class='code pro_amount rate_num ' id='pro_amount' value='".$v['invoice_detail_rate']."' name='delivery_note_detail_rate[]' placeholder='' />";
                $html .= "</td>";
                $html .= "<td>";

                $invoice_detail_total_discount_amount_val = 0;
                if ($v['invoice_detail_total_discount_type'] == 1) {
                if($v['invoice_detail_total_discount_amount'] != ''){
                if ($v['invoice_detail_total_discount_amount'] <= 100) {
                $invoice_detail_total_discount_amount_val = ($v['invoice_detail_total_discount_amount'] / 100) * $v['invoice_detail_quantity'] * $v['invoice_detail_rate'];
                }else{

                $invoice_detail_total_discount_amount_val = $v['invoice_detail_quantity'] * $v['invoice_detail_rate'];
                // return false;
                }
                }
                }else if ($v['invoice_detail_total_discount_type'] == 2){
                if($v['invoice_detail_total_discount_amount'] != ''){
                $invoice_detail_total_discount_amount_val = $v['invoice_detail_total_discount_amount'];
                }
                }else{
                $invoice_detail_total_discount_amount_val = 0;
                }


                // $invoice_detail_total_discount_amount_val = $subtotal - $invoice_detail_total_discount_amount_val;


                $amount = 0;
                $amount = ($v['invoice_detail_quantity'] * $v['invoice_detail_rate']) - $invoice_detail_total_discount_amount_val;
                $subtotal += $amount;


                $html .= "<input type='text' style='width: 200px' readonly='readonly' class='code amount  multTotal ' id='amount' name='amount[]' value='".$amount."' placeholder='' />";
                $html .= "</td>";
                $html .= "</tr>";
                echo $html;
                // echo json_encode($html);*/
        //     }
        // }else{
        //     echo 0;
        // }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['print_delivery'] = $this->print_delivery;
        $res['detail_page']    = $this->detail_page;
        $res['data'] = $this->basic_model->getCustomRows("
        SELECT delivery_note.*,user.user_name
        FROM `delivery_note` 
        INNER JOIN `user` 
        ON delivery_note.customer_id = user.user_id ORDER BY delivery_note.create_date DESC limit 100");
        // print_b($res['data']);
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
        $res['setting'] = $setting;
        $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE delivery_note.delivery_note_id != 0 ";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(delivery_note.delivery_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(delivery_note.delivery_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            if($customer_id != 0)
            {
                $where .= " AND delivery_note.customer_id = ".$customer_id;
            }

            if($invoice_no != "")
            {
                $where .= " AND delivery_note.invoice_id LIKE '%".$invoice_no."%'";
            }
            

            $where .= " order By delivery_note.create_date desc limit 100";
            $query = "SELECT delivery_note.*,user.user_name
            FROM `delivery_note` 
            INNER JOIN `user` 
            ON delivery_note.customer_id = user.user_id ".$where;
            
            $temp = $this->basic_model->getCustomRows($query);
            $res['data'] = array();
            foreach (@$temp as $v) 
            {
                $v['rev_no'] = find_rev_no($v['invoice_id']);
                array_push($res['data'],$v);
            }
            
             
            echo json_encode($res);
        }
    }

    function print(){
        if ($this->check_print) {
            extract($_GET);
            // print_b($_GET);
            if(isset($id) && $id != null) {
                $res["deliver_note"] = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                $res['customer_data'] = $this->basic_model->getCustomRow('Select * from user left join department ON department.department_id = user.department_id Where user.user_id = '.$res["deliver_note"]['customer_id']);

                $res["deliver_note_detail"] = $this->basic_model->getCustomRows("SELECT * FROM delivery_note_detail dnd JOIN product p ON p.product_id = dnd.product_id WHERE dnd.delivery_note_id = '".$id."' ");
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $res['employee_data'] = $this->basic_model->getCustomRow("SELECT * FROM `user` left join department ON department.department_id = user.department_id WHERE user.user_type = 2 AND user.user_is_active = 1 AND user.user_id=".@$res["deliver_note"]['employee_id']); 

                //print_b($res['employee_data']);
                $this->basic_model->make_pdf($header_check,$res,'Report','delivery_note_report');
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function delivery_noteDetailDelete($id){
        // $del = $this->basic_model->deleteRecord('delivery_note_detail_id', $id, $this->table_detail);

        $delivery_note_detail_data = $this->basic_model->getCustomRow("Select * from delivery_note_detail where delivery_note_detail_id =".$id);
        $del = $this->basic_model->deleteRecord('delivery_note_detail_id', $id, $this->table_detail);
        $this->basic_model->deleteRecord('inventory_id', $delivery_note_detail_data['inventory_id'], 'inventory');
        echo json_encode(array( "success" => "Record removed successfully","detailsDelete"=> true,"delivery_noteMultiTotalSet"=> true));

    }

    function delete($id){
        $delivery_note_detail_data = $this->basic_model->getRows("delivery_note_detail","delivery_note_id",$id,"delivery_note_id",$this->orderBy);
        $res["deliver_note"] = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
        // print_b($delivery_note_detail_data);
        if (isset($delivery_note_detail_data) && @$delivery_note_detail_data != null) {
            foreach ($delivery_note_detail_data as $k => $v) {
                $del = $this->basic_model->deleteRecord('inventory_id', $v['inventory_id'], 'inventory');
            }
        }

        $del = $this->basic_model->deleteRecord('delivery_note_id', $id, 'delivery_note');
        $del = $this->basic_model->deleteRecord('delivery_note_id', $id, 'delivery_note_detail');
        $inv = $this->basic_model->getCustomRow("Select delivery_note_id as d_id from delivery_note where invoice_id='".$res["deliver_note"]["invoice_id"]."' order by delivery_note_id desc");
        if(!empty($inv) && $inv != null){
          $this->basic_model->customQueryDel("UPDATE delivery_note SET is_edit =0 WHERE delivery_note_id =".$inv['d_id']);
        }
       

        $delivery_data = $this->basic_model->getCustomRow("Select * from delivery_note where invoice_id =".$res["deliver_note"]['invoice_id']);
        if(empty($delivery_data) || $delivery_data == null || $delivery_data == ''){
            $this->basic_model->customQueryDel('UPDATE quotation SET has_invoice = 0 WHERE quotation_no = "'.$res["deliver_note"]['invoice_id'].'"');
        }

        if(isset($res['deliver_note']['invoice_type']) && $res['deliver_note']['invoice_type'] == 2 && (empty($delivery_data) || $delivery_data == null || $delivery_data == '')){
            $find_proforma =  $this->basic_model->getCustomRow("SELECT * from invoice where invoice_no= '".$res["deliver_note"]['invoice_id']."' AND revision = 0");
            if(!empty($find_proforma) && $find_proforma != null && $find_proforma['invoice_proforma_no'] != ''){
                $inv1 = $this->basic_model->getCustomRow("Select delivery_note_id as d_id from delivery_note where invoice_id='".$find_proforma["invoice_proforma_no"]."' order by delivery_note_id desc");
                if(!empty($inv1) && $inv1 != null){
                    $this->basic_model->customQueryDel("UPDATE delivery_note SET is_edit =0 WHERE delivery_note_id =".$inv1['d_id']);
                }
                $delivery_data1 = $this->basic_model->getCustomRow("Select * from delivery_note where invoice_id =".$find_proforma['invoice_proforma_no']);
                if(empty($delivery_data1) || $delivery_data1 == null || $delivery_data1 == ''){
                    $this->basic_model->customQueryDel('UPDATE quotation SET has_invoice = 0 WHERE quotation_no = "'.$find_proforma.'"');
                }
            }


        }
            
        echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "detailsDelete"=> true));

    }

    function detail($id){
        if( isset($id) && $id != ''){
            
                $res_int = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                // print_b($res_int);
                $table_q = "invoice";
                $table_qid = "invoice_id";
                
                $table_c = "user";
                $table_cid = "user_id";

                $table_qd = "delivery_note_detail";
                $table_qd_id = "delivery_note_detail_id";

                // $res_quo = $this->basic_model->getRow($table_q,$table_qid,$res_int['invoice_id'],$this->orderBy,$table_qid);

                $res_quo_det = $this->basic_model->getRow($table_qd,$this->table_id,$res_int['delivery_note_id'],$this->orderBy,$table_qd_id);
                $res_cust = $this->basic_model->getRow($table_c,$table_cid,$res_int['customer_id'],$this->orderBy,$table_cid);
             
            if($res_int){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = ""; 
                $outputDescription .= "<p>Customer Name  : ".$res_cust['user_name']."</p>";
                // $outputDescription .= "<p>Product Description  : ".$res_quo_det['delivery_note_detail_description']."</p>";
                $outputDescription .= "<p>Delivery Note Number  : ".$res_int['delivery_note_no']."</p>";
                // $outputDescription .= "<p>Quantity  : ".$res_int['delivery_note_detail_quantity']."</p>";
                // $outputDescription .= "<p>Rate : ".$res_int['delivery_note_detail_rate']."</p>";
 
                
            }

            $delivery_note_detail_data = $this->basic_model->getCustomRows("SELECT * FROM delivery_note_detail dnd JOIN product p ON p.product_id = dnd.product_id WHERE dnd.delivery_note_id = '".$id."' ");
            
            if($delivery_note_detail_data)
            {
                $outputDescription .= "<h4><b>Delivered Products<b></h4>";
                $x = 0; 
                foreach ($delivery_note_detail_data as $k => $v) 
                {
                    $x++;
                    $outputDescription .= "<p>".$x.". ".$v['product_name']." - Quantity: ".$v['delivery_note_detail_quantity']."</p>"; 
                }
            }

            echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));

        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function find_invoices(){
        extract($_POST);
        if(isset($customer_id) && $customer_id != ''){
            $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` WHERE quotation_revised_id = 0 AND customer_id = ".$customer_id." AND quotation_status= '4' order by quotation.quotation_id desc");
            $res['invoice'] = $this->basic_model->getCustomRows("SELECT invoice_no, invoice_revised_no as revised_no FROM `invoice` where revision = 0 AND  customer_id = ".$customer_id." ORDER BY invoice.create_date DESC");
            echo json_encode($res);
        }
    }
}