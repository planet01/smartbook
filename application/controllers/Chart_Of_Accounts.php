<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chart_Of_Accounts extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'Chart_Of_Accounts/add';
        $this->view_product = 'Chart_Of_Accounts/view';
        $this->edit_product = 'Chart_Of_Accounts/edit';
        $this->detail_product = 'Chart_Of_Accounts/detail';
        $this->delete_product = 'Chart_Of_Accounts/delete';
        $this->print = 'Chart_Of_Accounts/report';

        // page active
        $this->add_page_product = "add-chart_of_accounts";
        $this->edit_page_product = "edit-chart_of_accounts";
        $this->view_page_product = "view-chart_of_accounts";
        
        // table
        $this->table = "chart_of_account";
        $this->table_pid = "AccountNo";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/warehouse";
        // Page Heading
        $this->page_heading = "Chat Of Accounts";
		
        $this->no_results_found = "user/no_results_found";
        
		require_once APPPATH .'third_party/FileUploader.php';

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_delete = false;
        $this->check_print = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 27) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_delete = true;
            $this->check_print = true;
        }
    }

    function index(){
        redirect($this->add_product);
    }

    function view() {

        if ($this->check_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["detail_product"] = $this->detail_product;
            $res["edit_product"] = $this->edit_product;
            $res["delete_product"] = $this->delete_product;
            $res["setval"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." coa JOIN account_group ag on ag.AccountGroupID = coa.AccountGroupID JOIN account_level al on al.AccountLevelID  = coa.AccountLevelID JOIN account_type at on at.AccountTypeID   = coa.AccountTypeID where coa.flgDeleted = 0  ORDER BY AccountNo ASC");
            $this->template_view($this->view_page_product,$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function add(){
        extract($_POST);
        // print_b($_POST);
        if( $this->input->post()){ 
            if(empty($id)){
                if( empty($AccountNo)){
                    echo json_encode(array("error" => "Please fill all fields Add"));
                }
                else{

                    $account_no = $this->basic_model->getRow($this->table, 'AccountNo', $AccountNo, $this->orderBy, $this->table_pid);
                    if (@$account_no != null && !empty(@$account_no)) {
                        echo json_encode(array("error" => "Account No. Already Exist"));
                        exit();
                        return "";
                    }

                    $data = array(
                        'AccountNo' => $AccountNo,
                        'AccountDesc' => $AccountDesc,
                        'AccountGroupID' => $AccountGroupID,
                        'AccountLevelID' => $AccountLevelID,
                        'AccountTypeID' => $AccountTypeID,
                        'BL_PL' => $BL_PL,
                        'InActive_Status' => $InActive_Status,
                        'flgDeleted' => 0,
                    );
                    
                    $this->basic_model->insertRecord($data, $this->table);

                   
                    echo json_encode(array( "success" => "Record added successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
            else{
              
                if( empty($AccountNo) ){
                    echo json_encode(array("error" => "Please fill all fields Edit"));
                }
                else{

                    $account_no = $this->basic_model->getRow($this->table, 'AccountNo', $AccountNo, $this->orderBy, $this->table_pid);

                    if (@$account_no != null && !empty(@$account_no)) {
                        if ($account_no['AccountNo'] != $id) {
                            echo json_encode(array("error" => "Account No. Already Exist"));
                            exit();
                            return "";
                        }
                    }
                    
                    $data = array(
                        'AccountNo' => $AccountNo,
                        'AccountDesc' => $AccountDesc,
                        'AccountGroupID' => $AccountGroupID,
                        'AccountLevelID' => $AccountLevelID,
                        'AccountTypeID' => $AccountTypeID,
                        'BL_PL' => $BL_PL,
                        'InActive_Status' => $InActive_Status,
                    );
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
        }
        else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res["setval"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                $res['account_group'] = $this->basic_model->getCustomRows("SELECT * FROM account_group");
                $res['account_level'] = $this->basic_model->getCustomRows("SELECT * FROM account_level");
                $res['account_type'] = $this->basic_model->getCustomRows("SELECT * FROM account_type WHERE flgDeleted = 0");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function edit($id){
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    extract($_POST);

                    $res['title'] = "Edit ".$this->page_heading;
                    $res["page_title"] = "edit";
                    $res["page_heading"] = $this->page_heading;
                    $res['active'] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res["setval"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                    $res['account_group'] = $this->basic_model->getCustomRows("SELECT * FROM account_group");
                    $res['account_level'] = $this->basic_model->getCustomRows("SELECT * FROM account_level");
                    $res['account_type'] = $this->basic_model->getCustomRows("SELECT * FROM account_type WHERE flgDeleted = 0");
                    
                    $res["data"] = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." WHERE AccountNo =".$id);
                    
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." coa JOIN account_group ag on ag.AccountGroupID = coa.AccountGroupID JOIN account_level al on al.AccountLevelID  = coa.AccountLevelID JOIN account_type at on at.AccountTypeID   = coa.AccountTypeID where coa.AccountNo = ".$id);
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";
                $outputDescription = "";
                $outputDescription .= "<p><span style='font-weight:bold;'>Account No : </span> ".@$setval["AccountNo"]."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Account Description : </span> ".$res["AccountDesc"]."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Account Group : </span> ".$res["AccountGroupName"]."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Account Level : </span> ".$res["AccountLevelDesc"]."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Account Type : </span> ".$res["AccountType_Desc"]."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Status : </span> ".(($res['InActive_Status'] == 0)?"Active":"Disabled")."</p>";
                
                
                
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function delete($id){
        if( isset($id) && $id != ''){
            
            $data = array(
                'flgDeleted' => 1,
            );
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }

    function report()
    {
        if ($this->check_print) {
                
                $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." coa JOIN account_group ag on ag.AccountGroupID = coa.AccountGroupID where coa.flgDeleted = 0  ORDER BY coa.AccountNo ASC");
                $res['group'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." coa JOIN account_group ag on ag.AccountGroupID = coa.AccountGroupID GROUP BY coa.AccountGroupID ORDER BY coa.AccountNo ASC");
                // print_b($res['data']);
                // $this->load->view('chart_of_account_report', $res);
                $this->basic_model->make_pdf(0, $res, 'Report', 'chart_of_account_report',0,0,0,1,'<div class="div-controls div-font-controls" style="text-align:center;padding-top:3%;"><h3 class="main-heading">Smart Matrix General Trading LLC</h3><br><h4 class="main-second-heading">Chart Of Accounts</h4><br></div>');
           
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

}