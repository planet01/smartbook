<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class all_user extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'all_user/add';
        $this->view_product = 'all_user/view';
        $this->edit_product = 'all_user/edit';
        $this->delete_product = 'all_user/delete';
        $this->detail_product = 'all_user/detail';
        // page active
        $this->add_page_product = "add-all_user";
        $this->edit_page_product = "edit-all_user";
        $this->view_page_product = "view-all_user";
        // table
        $this->table = "user";
        $this->table_pid = "id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // Page Heading
        $this->page_heading = "User";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index(){
        redirect($this->add_product);
    }
    function add(){
        extract($_POST);
        if( $this->input->post()){
            // print_b($_POST);
            if(empty($id)){
                if( empty($fname) || empty($lname) || empty($email) || empty($password) || empty($re_password)  || empty($date_of_birth)|| empty($nationality) || empty($phone) || empty($user_type)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    if ($password != $re_password){
                        echo json_encode(array("error" => "Password Not Match"));
                        exit();
                    }
                    $pass = $this->basic_model->encode($password);
                    $Checkemail = $this->basic_model->getRow($this->table, 'email', $email);
                    if(@$Checkemail != null && !empty(@$Checkemail)){
                        echo json_encode(array("error" => "E-mail is Exist"));
                        exit();
                    }

                    $Checkphone = $this->basic_model->getRow($this->table, 'phone', $phone);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        echo json_encode(array("error" => "Phone Number is Exist"));
                        exit();
                    }

                    $image_path = $this->basic_model->uploadFile($_FILES['image'],$this->image_upload_dir, array('jpeg', 'JPEG','jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF'));
                    $fullname = $image_path;
                    $uploads_dir = $this->image_upload_dir.'/';
                    $allowfiles = array('jpeg', 'JPEG','jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF');
                    // 64
                    $width = 64;
                    $height = 64;
                    $res = thumb($fullname, $width, $height,$uploads_dir,$allowfiles);
                    // 128
                    $width = 128;
                    $height = 128;
                    $res = thumb($fullname, $width, $height,$uploads_dir,$allowfiles);

                    $data = array(
                        "fname"=>$fname,
                        "lname"=>$lname,
                        "email" => $email,
                        "password" => $pass,
                        "date_of_birth" => $date_of_birth,
                        "nationality" => $nationality,
                        "about" => $about,
                        "phone" => $phone,
                        "price" => $price,
                        "image" => $image_path,
                        "user_type" => $user_type,
                        "is_active" => $status
                    );

                    $id = $this->basic_model->insertRecord($data,$this->table);

                    if (isset($loc_name) && @$loc_name != null || isset($location) && @$location != null || isset($ad_phone) && @$ad_phone != null) {
                        for ($i=0; $i < count($loc_name); $i++) { 
                            $arr = array(
                                "user_id" => $id,
                                "name" => $loc_name[$i],
                                "location" => $location[$i],
                                "phone" => $ad_phone[$i]
                            );
                            $user_address_id = $this->basic_model->insertRecord($arr,"user_address");
                        }
                    }
                    if ($user_type == 2) {
                        if (isset($category) && @$category != null) {
                            foreach ($category as $k => $v) {
                                $arr = array();
                                $arr = array(
                                    "agent_id" => $id,
                                    "category_id" => $v
                                );
                                $user_address_id = $this->basic_model->insertRecord($arr,"user_category_detail");
                            }
                        }
                    }

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
                if(empty($fname) || empty($lname) || empty($email) || empty($date_of_birth)|| empty($nationality) || empty($phone) || empty($user_type)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    if ($password != $re_password){
                        echo json_encode(array("error" => "Password Not Match"));
                        exit();
                    }
                    $Checkemail = $this->basic_model->getRow($this->table, 'email', $email);
                    if(@$Checkemail != null && !empty(@$Checkemail)){
                        if ($Checkemail['email'] != $email_old) {
                            echo json_encode(array("error" => "E-mail is Exist"));
                            exit();
                        }
                    }

                    $Checkemail = $this->basic_model->getRow($this->table, 'phone', $phone);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        if ($Checkphone['phone'] != $phone_old) {
                            echo json_encode(array("error" => "Phone Number is Exist"));
                            exit();
                        }
                    }

                    $pass = $this->basic_model->encode($password);
                    if (empty($password) && $password == null && empty($re_password) && $re_password == null ) {
                        $pass = $pass_old;
                    }

                    $del = $this->basic_model->deleteRecord('user_id', $id, 'user_address');
                    
                    $data = array(
                        "fname"=>$fname,
                        "lname"=>$lname,
                        "email" => $email,
                        "password" => $pass,
                        "date_of_birth" => $date_of_birth,
                        "nationality" => $nationality,
                        "about" => $about,
                        "phone" => $phone,
                        "price" => $price,
                        "image" => $image_old,
                        "user_type" => $user_type,
                        "is_active" => $status
                    );
                    if ($user_type == 2) {
                        $del = $this->basic_model->deleteRecord('agent_id', $id, 'user_category_detail');
                        if (isset($category) && @$category != null) {
                            foreach ($category as $k => $v) {
                                $arr = array();
                                $arr = array(
                                    "agent_id" => $id,
                                    "category_id" => $v
                                );
                                $user_address_id = $this->basic_model->insertRecord($arr,"user_category_detail");
                            }
                        }

                    }else{
                        $del = $this->basic_model->deleteRecord('agent_id', $id, 'user_category_detail');
                    }
                    if (isset($loc_name) && @$loc_name != null || isset($location) && @$location != null || isset($ad_phone) && @$ad_phone != null) {
                        for ($i=0; $i < count($loc_name); $i++) { 
                            if (isset($loc_name[$i]) && @$loc_name[$i] != null || isset($location[$i]) && @$ad_phone[$i] != null || isset($ad_phone[$i]) && @$location[$i] != null) {
                                $arr = array(
                                    "user_id" => $id,
                                    "name" => $loc_name[$i],
                                    "location" => $location[$i],
                                    "phone" => $ad_phone[$i]
                                );
                                $user_address_id = $this->basic_model->insertRecord($arr,"user_address");
                            }
                        }
                    }
                    if(isset($_FILES) && $_FILES['image']['size'] > 0 ){
                        if(isset($image_old)){
                            $file_path = dir_path().$this->image_upload_dir.'/'.@$image_old;
                            if (file_exists($file_path)) {
                                $pathinfo = pathinfo($image_old);
                                $dirPath = dir_path().$this->image_upload_dir.'/';
                                @unlink($dirPath.$pathinfo['filename']."-64_64.".$pathinfo['extension']);
                                @unlink($dirPath.$pathinfo['filename']."-128_128.".$pathinfo['extension']);
                                @unlink($file_path);
                            }
                        }
                        $image_path = $this->basic_model->uploadFile($_FILES['image'],$this->image_upload_dir, array('jpeg', 'JPEG','jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF'));
                        $fullname = $image_path;
                        $uploads_dir = $this->image_upload_dir.'/';
                        $allowfiles = array('jpeg', 'JPEG','jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF');
                        // 64
                        $width = 64;
                        $height = 64;
                        $res = thumb($fullname, $width, $height,$uploads_dir,$allowfiles);
                        // 128
                        $width = 128;
                        $height = 128;
                        $res = thumb($fullname, $width, $height,$uploads_dir,$allowfiles);

                        $data['image'] = $image_path;
                    }
                    
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{
            $res['title'] = "Add New ".$this->page_heading;
            $res["page_title"] = "add";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->add_page_product;
            $res["add_product"] = $this->add_product;
            $res['countries'] = $this->basic_model->countries;
            // $res['categoryData'] = $this->basic_model->getCustomRows("SELECT * FROM `category` WHERE `is_active` = 1");
            $this->template_view($this->add_page_product,$res);
        }
    }

    function edit($id){
        extract($_POST);
        $res["title"] = "Edit ".$this->page_heading;
        $res["page_title"] =  "";
        $res["page_heading"] = $this->page_heading;
        $res["active"] = $this->edit_page_product;
        $res["add_product"] = $this->add_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['countries'] = $this->basic_model->countries;
        $res["data"] = $this->basic_model->getRow($this->table,$this->table_pid,$id);
        $res["user_address"] = $this->basic_model->getRows("user_address","user_id",$id);
        $res['categoryData'] = $this->basic_model->getCustomRows("SELECT * FROM `category` WHERE `is_active` = 1");

        $res['categorySelect'] = $this->basic_model->getCustomRows("SELECT `c`.`id` as `id` FROM `user_category_detail` `user_cate_d` INNER JOIN `category` `c` ON `user_cate_d`.`category_id` = `c`.`id` WHERE `c`.`is_active` = 1");
        $cate = array();
        foreach ($res['categorySelect'] as $k => $v) {
            $cate[] = $v['id'];
        }
        $res['categorySelect'] = $cate;
        // print_b($res);
        $this->template_view($this->add_page_product,$res);
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` != 1");

        $this->template_view($this->view_page_product,$res);
    }
    function deleteDetails($id){
        $del = $this->basic_model->deleteRecord('id', $id, 'user_address');
        echo json_encode(array( "success" => "Record removed successfully", "details"=> true));
    }

    // function delete($id){
    //     if( isset($id) && $id != ''){
    //         $res = $this->basic_model->getRow($this->table,$this->table_pid,$id);
    //         if(!empty(@$res['image'])){
    //             if(isset($res['image'])){
    //                 $file_path = dir_path().$this->image_upload_dir.'/'.@$res['image'];
    //                 if (file_exists($file_path)) {
    //                     @unlink($file_path);
    //                 }
    //             }
    //         }
    //         $del = $this->basic_model->deleteRecord($this->table_pid, $id, $this->table);
    //         echo json_encode(array("success"=>"Record Succesfully Deleted","deleteRow"=>TRUE));
    //     }else{
    //         echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
    //     }
    // }
    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id);
            if($res){
                $outputTitle = "";
                $outputTitle .= "User Detail";

                $outputDescription = "";
                $current_img = '';
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['image'];
                if (!is_dir($file_path) && file_exists($file_path)) {
                  $current_img = site_url($this->image_upload_dir.'/'.$res['image']);
                }else{
                  $current_img = site_url("uploads/dummy.jpg");
                }
                $outputDescription .= "<p>Image :<br> <img src='".$current_img."' width='100px' /></p>";
                $outputDescription .= "<p>First Name : ".$res['fname']."</p>";
                $outputDescription .= "<p>Last Name : ".$res['lname']."</p>";
                $outputDescription .= "<p>E-mail Address : ".$res['email']."</p>";
                $outputDescription .= "<p>Phone : ".$res['phone']."</p>";
                $outputDescription .= "<p>Date Of Birth : ".$res['date_of_birth']."</p>";
                $countries = $this->basic_model->countries;
                foreach ($countries as $k => $v) {
                    if ($k == $res['nationality']) {
                        $outputDescription .= "<p>Nationality : ".$k." - ".$v."</p>";    
                        break;
                    }
                    
                    
                }
                $outputDescription .= "<p>About Us : ".$res['about']."</p>";
                if ($res['user_type'] == 3) {
                    $outputDescription .= "<p>User Type :<span class='label label-primary'>Customer</span></p>";
                }else if ($res['user_type'] == 2) {
                    $outputDescription .= "<p>User Type :<span class='label label-warning'>Agent</span></p>";
                }else{
                    $outputDescription .= "<p>User Type : N/A</p>";
                }
                if ($res['user_type'] == 2) {
                    $outputDescription .= "<p>Price : ".$res['price']."</p>";
                }
                $outputDescription .= "<p>Status : ".(($res['is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";
                $outputDescription .= "<p>Created Date : ".$res['create_date']."</p>";

                $user_address = $this->basic_model->getRows("user_address","user_id",$id);
                if (isset($user_address) && @$user_address != null) {
                    $count = 1;
                    $outputDescription .= "<h4 class='semi-bold'>User Address</h4>";
                    foreach ($user_address as $k => $v) {
                        $outputDescription .= "<p>Address Name ".$count." : ".$v['name']."</p>";
                        $outputDescription .= "<p>Address Location ".$count." : ".$v['location']."</p>";
                        $outputDescription .= "<p>Address Phone ".$count." : ".$v['phone']."</p>";
                        $count++;
                    }
                }
                if ($res['user_type'] == 2) {
                    $count = 1;
                    $categorySelect = $this->basic_model->getCustomRows("SELECT `c`.`id`,`c`.`name` as `category_name` FROM `user_category_detail` `user_cate_d` INNER JOIN `category` `c` ON `user_cate_d`.`category_id` = `c`.`id` WHERE `user_cate_d`.`agent_id` = '".$id."'");
                    if (isset($categorySelect) && @$categorySelect != null) {
                        $outputDescription .= "<h4 class='semi-bold'>Agent Category</h4>";
                        foreach ($categorySelect as $k => $v) {
                            $outputDescription .= "<p>Category ".$count." : ".$v['category_name']."</p>";
                            $count++;
                        }
                    }
                }


                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
}