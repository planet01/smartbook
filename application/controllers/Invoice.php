<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class invoice extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
       //$this->add_product = 'invoice/add';
        $this->save_product = 'invoice/add';
        
        $this->view_product = 'invoice/view'; 
        
        $this->edit_product = 'invoice/edit';
        $this->delete_product = 'invoice/delete';
        $this->detail_product = 'invoice/detail';
        $this->print_product = 'invoice/print_invoice';
        $this->mail_product = 'invoice/mail_invoice';
        $this->edit_status = 'invoice/change_status';
        $this->history_product = 'invoice/history';
        $this->edit_payment = 'payment/edit';
        $this->delete_payment = "payment/delete";
        $this->print_payment = 'receivables/print';



        // page active
        $this->add_page_product = "add-invoice";
        $this->edit_page_product = "edit-invoice";
        $this->view_page_product = "view-invoice";
        // table
        $this->table = "invoice";
        $this->table_detail = "invoice_detail";
        $this->table_qd = "quotation";
        $this->tablep = "product";
        $this->tables = "setting";
        $this->mail = 'invoice/mail';
        $this->table_id = "invoice_id";
        $this->table_fid = "invoice_id";
        $this->table_qid = "quotation_id";
        $this->table_pid = "product_id";
        $this->table_sid = "setting_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/invoice";
        // Page Heading
        $this->page_heading = "Invoice";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }

        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 12) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->add_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function add(){
       
        extract($_POST);
        if( $this->input->post()){
            if(empty($id))
            {
                if(empty($customer_id) || empty($invoice_no) ) {
                        echo json_encode(array("error" => "Please fill all fields"));
                }else{

                    $Checkinvoice_no = $this->basic_model->getRow($this->table, 'invoice_no', $invoice_no,$this->orderBy,$this->table_id);
                    if(@$Checkinvoice_no != null && !empty(@$Checkinvoice_no)){
                        echo json_encode(array("error" => "Invoice No. is Exist"));
                        exit();
                    }

                    if (isset($invoice_detail_quantity) && @$invoice_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            echo json_encode(array("error" => "Duplicate product is not allowed."));
                            return false;
                            foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $invoice_detail_quantity[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'invoice_detail_quantity' => $quantityTotal_new,
                            );
                        }

                        

                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'invoice_detail_quantity' => $invoice_detail_quantity[$k],
                                );
                            }
                        }

                       /* foreach ($product_check as $k => $v) {
                            $inventory_quantity_data = inventory_quantity_by_product($v['product_id']);
                            if ($inventory_quantity_data['total_inventory_quantity'] <= $v['invoice_detail_quantity']) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                            }
                            break;
                        }*/

                        // product_check end.
                    }
                    $invoice_date_year = explode("-",$invoice_date)[0];
                    $invoice_no = serial_no_invoice($invoice_date_year);
                    
                    $data= array(
                        'customer_id' => $customer_id,
                        'invoice_no' => $invoice_no, //$invoice_no,
                        'invoice_currency_id' => $invoice_currency,
                        'end_user' => @$end_user,
                        'invoice_date' => $invoice_date,
                        'invoice_proforma_no' => $invoice_proforma_no,
                        'invoice_total_discount_type' => isset($invoice_total_discount_type) ? (empty($invoice_total_discount_type) ? 0 :$invoice_total_discount_type) : 0,
                        'invoice_total_discount_amount' => isset($total_discount_amount)? (empty($total_discount_amount) ? 0 : $total_discount_amount) : 0,
                        'invoice_subtotal'=> $invoice_subtotal,
                        'invoice_buyback' => isset($invoice_buyback)? (empty($invoice_buyback)? 0 : $invoice_buyback) : 0,
                        'invoice_tax' => isset($invoice_tax)? (empty($invoice_tax)? 0 : $invoice_tax) : 0,
                        'invoice_tax_amount' => isset($invoice_tax_amount)? (empty($invoice_tax_amount)? 0 : $invoice_tax_amount) : 0,
                        'invoice_net_amount' => $invoice_net_amount,
                        'invoice_total_amount' => $invoice_subtotal,
                        'invoice_expire_date' => $invoice_expire_date,
                        'invoice_expire_date_initial' => $invoice_expire_date,
                        'invoice_installation_location_details' =>$invoice_installation_location_details,
                        "revision" => 0,
                        "belongs_to" => 0,
                        "invoice_status" => 3,
                        "invoice_revised_no" => 0,
                        "employee_id" => $employee_id,
                        "freight_type" => $freight_type,
                        "invoice_notes" => @$invoice_notes,
                        "discount_label" => @$discount_label,
                        'invoice_po_no' => @$invoice_po_no,
                        'invoice_report_line_break' => (($invoice_report_line_break === NULL)?$invoice_report_line_break:0),
                        'invoice_report_line_break_item' => (($invoice_report_line_break_item === NULL)?$invoice_report_line_break_item:0),
                    );
                    $invoice_id = $this->basic_model->insertRecord($data,$this->table);
                    if(!empty($invoice_proforma_no) && $invoice_proforma_no != null && $invoice_proforma_no != ''){
                        $this->basic_model->customQueryDel('UPDATE quotation SET has_invoice = 1 WHERE quotation_no = "'.$invoice_proforma_no.'"');
                    }
                    //$this->status_handler($invoice_id, 'add');
                    $invoice_discrepancy = 0;
                    if (isset($invoice_detail_quantity) && @$invoice_detail_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($invoice_detail_quantity as $k => $v) {
                            if ($invoice_detail_quantity[$k]>0 || $invoice_detail_quantity[$k] == "Lot" || $invoice_detail_quantity[$k] == "lot") {
                                $discrepancy = 0;
                                if($delivery_note_found == 1 && isset($invoice_detail_dn_quantity[$k]) && $can_deliver[$k] == 0 && $invoice_detail_dn_quantity[$k] != 0  && $invoice_detail_dn_quantity[$k] != '' && $invoice_detail_dn_quantity[$k] != $invoice_detail_dn_quantity_old[$k]){
                                    $discrepancy = 1;
                                    $invoice_discrepancy = 1;
                                }
                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'invoice_id' => $invoice_id,
                                    'invoice_detail_pi_quantity' => isset($invoice_detail_pi_quantity[$k])? (empty($invoice_detail_pi_quantity[$k])? 0 : $invoice_detail_pi_quantity[$k]) : 0,
                                    'invoice_detail_dn_quantity' => isset($invoice_detail_dn_quantity[$k]) ? (empty($invoice_detail_dn_quantity[$k])? 0 : $invoice_detail_pi_quantity[$k]) : 0,
                                    'invoice_detail_quantity' => $invoice_detail_quantity[$k],
                                    'invoice_detail_description' => $invoice_detail_description[$k],
                                    'invoice_detail_total_discount_type' => $invoice_detail_total_discount_type[$k],
                                    'invoice_detail_total_discount_amount' => $invoice_detail_total_discount_amount[$k],
                                    'invoice_detail_usd_rate' => $invoice_detail_usd_rate[$k],
                                    'invoice_detail_base_rate' => $invoice_detail_base_rate[$k],
                                    'invoice_detail_discrepancy' => $discrepancy,
                                    'invoice_detail_total_amount' => intval(preg_replace('/[^\d.]/', '', @$invoice_detail_total_amount[$k])),//@@$invoice_detail_total_amount[$k],
                                    "revision" => 0,
                                    "belongs_to" => $invoice_id
                                );
                                $i++;
                                // echo '<pre>';
                                // print_b($arr);
                                $invoice_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            }
                        }
                    }
                    if($invoice_discrepancy == 1){
                         $this->basic_model->updateRecord(['invoice_discrepancy' => 1], $this->table, $this->table_id, $invoice_id);
                    }
                    for($i= 0; $i< sizeof($terms); $i++) {
                        $payment_data = [
                            'client_id' => $customer_id,
                            'invoice_id' => $invoice_id,
                            'payment_title' => $terms[$i],
                            'percentage' => $percentage[$i],
                            'payment_days' => @$payment_days[$i],
                            'payment_amount' => @$payment_amount[$i],
                            "revision" => 0,
                            "belongs_to" => $invoice_id
                        ];
                        $this->basic_model->insertRecord($payment_data,'invoice_payment_term');
                    }

                    $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
                    $customer_name = $this->basic_model->getCustomRow("SELECT * FROM `user` WHERE `user_id` = '".$customer_id."' ")['user_name'];
                    //Add to voucher
                    
                    $Transaction_Date_year = explode("-",$invoice_date)[0];
                    $company_prfx = (substr($setting["company_prefix"], -1) == '-')?$setting["company_prefix"]:$setting["company_prefix"].'-';
                    $t_no = serial_no_voucher($company_prfx.'JV-'.$Transaction_Date_year);
                    
                    $voucher_data = array(
                        'TransactionNo' => $t_no,
                        'voucher_type' => 1,
                        'Transaction_Date' => $invoice_date,
                        'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                        'CompanyID' => str_replace("-","",$setting["company_prefix"]),
                        'Posted' => 1,
                        'flgDeleted' => 0,
                        'doc_type' => 2,
                        'doc_id' => $invoice_id
                    );
                    $voucher_id = $this->basic_model->insertRecord($voucher_data, 'voucher');
                    //credit entry
                    if(isset($invoice_tax_amount) && $invoice_tax_amount != 0){
                        $voucher_detail1 = array(
                            'AccountNo' => $setting['tax_provision_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => '',
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => $invoice_tax_amount,
                            'Amount_Dr' => 0,
                            'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                            'Remarks' => 'Credit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');
                    }

                    $invoice_buyback = isset($invoice_buyback)? (empty($invoice_buyback)? 0 : $invoice_buyback) : 0;
                    $total_discount_amount = isset($total_discount_amount)? (empty($total_discount_amount) ? 0 : $total_discount_amount) : 0;
                    if($invoice_total_discount_type == 1) //percentage
                    {
                        $total_discount_amount = ($invoice_subtotal / 100) * $total_discount_amount;
                        $total_discount_amount = sprintf("%.4f",$total_discount_amount);
                        $total_discount_amount = substr_replace($total_discount_amount, "", -2);
                    }
                    //credit entry
                    $voucher_detail1 = array(
                        'AccountNo' => $setting['revenue_account'],
                        'voucher_id' => $voucher_id,
                        'invoice_id' => @$invoice_id,
                        'Cheque_No' => 'INVOICE',
                        'Pay_To_Name' => @$customer_name,
                        'Amount_Cr' => $invoice_subtotal - $invoice_buyback - $total_discount_amount,
                        'Amount_Dr' => 0,
                        'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                        'Remarks' => 'Credit Entry',                     
                    );
                    $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');

                    //debit entry
                    $voucher_detail2 = array(
                        'AccountNo' => $setting['receivable_account'],
                        'voucher_id' => $voucher_id,
                        'invoice_id' => @$invoice_id,
                        'Cheque_No' => '',
                        'Pay_To_Name' => @$customer_name,
                        'Amount_Cr' => 0,
                        'Amount_Dr' => $invoice_net_amount,
                        'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                        'Remarks' => 'Debit Entry',                     
                    );
                    $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
                    
                    //End Voucher 
                    
                    echo json_encode(array( "success" => "Record Inserted successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }else{
                if(empty($customer_id) || empty($invoice_no) ) {
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    
                    $Checkinvoice_no = $this->basic_model->getRow($this->table, 'invoice_no', $invoice_no,$this->orderBy,$this->table_id);
                    if(@$Checkinvoice_no != null && !empty(@$Checkinvoice_no)){
                        if ($Checkinvoice_no['invoice_no'] != $invoice_no_old) {
                            echo json_encode(array("error" => "Invoice No. is Exist"));
                            exit();
                        }
                    }

                    if (isset($invoice_detail_quantity) && @$invoice_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                         //print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            echo json_encode(array("error" => "Duplicate product is not allowed."));
                            return false;
                            foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $invoice_detail_quantity[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'invoice_detail_quantity' => $quantityTotal_new,
                            );
                        }

                                                 


                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'invoice_detail_quantity' => $invoice_detail_quantity[$k],
                                );
                            }
                        }

                        /*foreach ($product_check as $k => $v) {
                            $q = "SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1 GROUP By inventory.inventory_id";
                            // echo $q;
                            // die;

                            $inventory_quantity_data = $this->basic_model->getCustomRow($q);

                            if ($inventory_quantity_data['total_inventory_quantity'] < $v['invoice_detail_quantity']) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                            }
                            break;
                        }*/

                        // product_check end.
                    }

                    $previous_data = $this->basic_model->getRow($this->table, 'invoice_id', $id,$this->orderBy,$this->table_id);
                    if(!empty($previous_data['invoice_proforma_no']) && $previous_data['invoice_proforma_no'] != null && $previous_data['invoice_proforma_no'] != ''){
                        $this->basic_model->customQueryDel('UPDATE quotation SET has_invoice = 0 WHERE quotation_no = "'.$previous_data['invoice_proforma_no'].'"');
                    }
                    

                    $data= array(
                        'customer_id' => $customer_id,
                        'end_user' => @$end_user,
                        'invoice_no' => $invoice_no,
                        'invoice_currency_id' => $invoice_currency,
                        'invoice_date' => $invoice_date,
                        'invoice_proforma_no' => $invoice_proforma_no,
                        'invoice_total_discount_type' => isset($invoice_total_discount_type) ? (empty($invoice_total_discount_type) ? 0 :$invoice_total_discount_type) : 0,
                        'invoice_total_discount_amount' => isset($total_discount_amount)? (empty($total_discount_amount) ? 0 : $total_discount_amount) : 0,
                        'invoice_subtotal'=> $invoice_subtotal,
                        'invoice_buyback' => isset($invoice_buyback)? (empty($invoice_buyback)? 0 : $invoice_buyback) : 0,
                        'invoice_tax' => isset($invoice_tax)? (empty($invoice_tax)? 0 : $invoice_tax) : 0,
                        'invoice_tax_amount' => isset($invoice_tax_amount)? (empty($invoice_tax_amount)? 0 : $invoice_tax_amount) : 0,
                        'invoice_net_amount' => $invoice_net_amount,
                        'invoice_total_amount' => $invoice_subtotal,
                        'invoice_expire_date_initial' => $invoice_expire_date,
                        'invoice_expire_date' => $invoice_expire_date,
                        'invoice_installation_location_details' =>$invoice_installation_location_details,
                        "revision" => 0,
                        "belongs_to" => $id,
                        "employee_id" => $employee_id,
                        "freight_type" => $freight_type,
                        "invoice_notes" => @$invoice_notes,
                        "discount_label" => @$discount_label,
                        "invoice_po_no" => @$invoice_po_no,
                        'invoice_report_line_break' => @$invoice_report_line_break,
                        'invoice_report_line_break_item' => @$invoice_report_line_break_item,
                    );


                    if($invoice_email_printed_status > 0){
                        //$quotation_revised_no
                        if($invoice_status > 3 || $invoice_status == 1){
                            //History
                             $this->makeInvoiceHistory($id);    
                            //End History
                            $data['invoice_status'] = 2;
                            $data['invoice_revised_no'] = $invoice_revised_no;
                            $this->basic_model->updateRecord(['invoice_final' => 0], 'delivery_note', 'invoice_id', $invoice_no);

                        }
                    }


                    $invoice_id = $id;
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                    if(!empty($invoice_proforma_no) && $invoice_proforma_no != null && $invoice_proforma_no != ''){
                        $this->basic_model->customQueryDel('UPDATE quotation SET has_invoice = 1 WHERE quotation_no = "'.$invoice_proforma_no.'"');
                    }
                    //$del = $this->basic_model->deleteRecord($this->table_fid, $id, $this->table_detail);
                    $this->basic_model->customQueryDel('DELETE from '.$this->table_detail.' where '.$this->table_fid.'='.$id);
                    if (isset($invoice_detail_quantity) && @$invoice_detail_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($invoice_detail_quantity as $k => $v) {
                            if ($invoice_detail_quantity[$k]>0 || $invoice_detail_quantity[$k] == "Lot" || $invoice_detail_quantity[$k] == "lot") {
                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'invoice_id' => $id,
                                    'invoice_detail_pi_quantity' => isset($invoice_detail_pi_quantity[$k])? (empty($invoice_detail_pi_quantity[$k])? 0 : $invoice_detail_pi_quantity[$k]) : 0,
                                    'invoice_detail_dn_quantity' => isset($invoice_detail_dn_quantity[$k]) ? (empty($invoice_detail_dn_quantity[$k])? 0 : $invoice_detail_pi_quantity[$k]) : 0,
                                    'invoice_detail_quantity' => $invoice_detail_quantity[$k],
                                    'invoice_detail_description' => @$invoice_detail_description[$k],
                                    'invoice_detail_total_discount_type' => $invoice_detail_total_discount_type[$k],
                                    'invoice_detail_total_discount_amount' => $invoice_detail_total_discount_amount[$k],
                                    'invoice_detail_usd_rate' => $invoice_detail_usd_rate[$k],
                                    'invoice_detail_base_rate' => $invoice_detail_base_rate[$k],
                                    'invoice_detail_discrepancy' => @$discrepancy,
                                    'invoice_detail_total_amount' =>intval(preg_replace('/[^\d.]/', '', @$invoice_detail_total_amount[$k])),//@ @$invoice_detail_total_amount[$k],
                                    "revision" => 0,
                                    "belongs_to" => $id
                                );
                                /* echo '<pre>';
                                 print_r($arr);*/
                                $i++;
                                $invoice_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            }
                        }
                    }
                    /*$dl = $this->basic_model->customQueryDel('DELETE from invoice_payment_term where invoice_id='.$id.' AND client_id='.$customer_id);*/
                    $this->basic_model->customQueryDel('DELETE  from invoice_payment_term where invoice_id ='.$id);
                    for($i= 0; $i< sizeof($terms); $i++) {
                        $payment_data = [
                            'client_id' => $customer_id,
                            'invoice_id' => $invoice_id,
                            'payment_title' => $terms[$i],
                            'percentage' => $percentage[$i],
                            'payment_days' => @$payment_days[$i],
                            'payment_amount' => @$payment_amount[$i],
                            "revision" => 0,
                            "belongs_to" => $id
                        ];
                        $this->basic_model->insertRecord($payment_data,'invoice_payment_term');
                    }
                    // print_b($arr);
                    //$updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);

                    $this->basic_model->customQueryDel("UPDATE voucher SET flgDeleted=1 where doc_type = 2 AND doc_id=".$id);

                    $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
                    $customer_name = $this->basic_model->getCustomRow("SELECT * FROM `user` WHERE `user_id` = '".$customer_id."' ")['user_name'];
                    //Add to voucher
                    
                    $Transaction_Date_year = explode("-",$invoice_date)[0];
                    $company_prfx = (substr($setting["company_prefix"], -1) == '-')?$setting["company_prefix"]:$setting["company_prefix"].'-';
                    $t_no = serial_no_voucher($company_prfx.'JV-'.$Transaction_Date_year);
                    
                    $voucher_data = array(
                        'TransactionNo' => $t_no,
                        'voucher_type' => 1,
                        'Transaction_Date' => $invoice_date,
                        'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                        'CompanyID' => str_replace("-","",$setting["company_prefix"]),
                        'Posted' => 1,
                        'flgDeleted' => 0,
                        'doc_type' => 2,
                        'doc_id' => $invoice_id
                    );
                    $voucher_id = $this->basic_model->insertRecord($voucher_data, 'voucher');
                    //credit entry
                    if(isset($invoice_tax_amount) && $invoice_tax_amount != 0){
                        $voucher_detail1 = array(
                            'AccountNo' => $setting['tax_provision_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => '',
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => $invoice_tax_amount,
                            'Amount_Dr' => 0,
                            'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                            'Remarks' => 'Credit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');
                    }

                    $invoice_buyback = isset($invoice_buyback)? (empty($invoice_buyback)? 0 : $invoice_buyback) : 0;
                    $total_discount_amount = isset($total_discount_amount)? (empty($total_discount_amount) ? 0 : $total_discount_amount) : 0;
                    if($invoice_total_discount_type == 1) //percentage
                    {
                        $total_discount_amount = ($invoice_subtotal / 100) * $total_discount_amount;
                        $total_discount_amount = sprintf("%.4f",$total_discount_amount);
                        $total_discount_amount = substr_replace($total_discount_amount, "", -2);
                    }
                    //credit entry
                    $voucher_detail1 = array(
                        'AccountNo' => $setting['revenue_account'],
                        'voucher_id' => $voucher_id,
                        'invoice_id' => @$invoice_id,
                        'Cheque_No' => 'INVOICE',
                        'Pay_To_Name' => @$customer_name,
                        'Amount_Cr' => $invoice_subtotal - $invoice_buyback - $total_discount_amount,
                        'Amount_Dr' => 0,
                        'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                        'Remarks' => 'Credit Entry',                     
                    );
                    $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');

                    //debit entry
                    $voucher_detail2 = array(
                        'AccountNo' => $setting['receivable_account'],
                        'voucher_id' => $voucher_id,
                        'invoice_id' => @$invoice_id,
                        'Cheque_No' => '',
                        'Pay_To_Name' => @$customer_name,
                        'Amount_Cr' => 0,
                        'Amount_Dr' => $invoice_net_amount,
                        'Transaction_Detail' => 'Invoice # '.@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice_no,
                        'Remarks' => 'Debit Entry',                     
                    );
                    $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
                    
                    //End Voucher 
                    
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                // $res["add_product"] = $this->add_product;
                $res["save_product"] = $this->save_product;
                $res["ckeditor"] = "yes";
            /* $res['countries'] = $this->basic_model->countries;*/
                $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1"); 
                $res['quotationData'] = $this->basic_model->getCustomRows("SELECT * FROM `quotation`");  
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p  WHERE p.product_is_active = 1 ORDER BY p.product_name ASC"); 
                $res['currencyData'] = $this->basic_model->getCustomRows("SELECT * FROM `currency` order By id desc"); 
                $res["data"] = $this->basic_model->getRow('setting','setting_id',1,$this->orderBy,'setting_id');
                $res['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1"); 

                $setval = $res["setval"];
                $id = $this->basic_model->getCustomRows("SELECT MAX(invoice_id) as id FROM invoice;");
                $id = isset($id[0]['id']) ? $id[0]['id']+1 : 1;
                $res['receipt_no'] = serial_no_invoice();
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
                $res['setting'] = $setting;
                $this->template_view($this->add_page_product,$res);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
        }  
    }
    
    function progt(){
        if($_POST['pid']) {
            $data = array();
            $data = $this->basic_model->getCustomRow("SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$_POST['pid']."')) as total_inventory_quantity FROM `product` 
                JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` JOIN `category_type` `ct` ON `ct`.`id` = `product`.`category_type`
                WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$_POST['pid']."' AND product_is_active = 1    ");
                
           // if(sizeof($data) > 0){
                $user = $this->basic_model->getCustomRow("SELECT user.*, c.*, price.*, currency.title as currency_title from user 
                    LEFT JOIN customer_type c ON c.id = user.customer_type
                    LEFT JOIN currency  ON currency.id = user.currency_id
                    LEFT join price ON user.plan_type = price.plan_id Where price.prod_id = '".$_POST['pid']."' AND price.plan_id ='".$_POST['plan_type']."' AND user.user_id ='".$_POST['user_id']."'");
                if(isset($user['plan_type']) && !empty($user['plan_type'])){
                    $base_price = isset($user[strtolower($user['title']).'_base_price'])? $user[strtolower($user['title'].'_base_price')] : 0;
                    $usd_price  = isset($user[strtolower($user['title']).'_usd_price'])? $user[strtolower($user['title'].'_usd_price')] : 0;
                    $discount   = isset($user[strtolower($user['title']).'_discount'])? $user[strtolower($user['title'].'_discount')] : 0;
                    if(empty($data)){
                      $data = $this->basic_model->getCustomRow("SELECT * from product where `product_id` = '".$_POST['pid']."'");
                    }
                    $data['usd']  = empty($usd_price)? 0 : $usd_price;
                    $data['base'] = empty($base_price)? 0 : $base_price;
                    $data['discount'] = empty($discount)? 0 : $discount;
                    $data['currency_title'] = @$user['currency_title'];
        
                }else{
                    $user = $this->basic_model->getCustomRow("SELECT user.*, c.*, price.*, currency.title as currency_title from user 
                    LEFT JOIN customer_type c ON c.id = user.customer_type
                    LEFT JOIN currency  ON currency.id = user.currency_id
                    LEFT join price ON user.plan_type = price.plan_id Where user.user_id ='".$_POST['user_id']."'");
                    if($user){
                        $data['usd']  =  0;
                        $data['base'] =  0;
                        $data['discount'] = 0;
                        $data['currency_title'] = @$user['currency_title'];
                    }else{
                        echo json_encode(array("error" => "Please select customer type & price id from customers"));
                        exit;
                    }
                }
            //}
            
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    function customer_quotations()
    {
        if($_POST['customer_id']) {
            

            $data = $this->basic_model->getCustomRows("SELECT * from quotation WHERE customer_id = '".$_POST['customer_id']."'");
            
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    function quotation_data()
    {
        if($_POST['quotation_id']) {

            $quotation_data = $this->basic_model->getCustomRow("SELECT * from quotation q WHERE q.quotation_id = '".$_POST['quotation_id']."'");

            $invoice_detail_data = $this->basic_model->getCustomRows("SELECT * FROM invoice_detail WHERE quotation_id = '".$_POST['quotation_id']."'");
            $product_data = $this->basic_model->getCustomRows("SELECT *,SUM(`inventory_quantity`) as total_inventory_quantity FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 GROUP BY i.product_id ORDER BY p.product_name ASC");

            $data['quotation_data'] = $quotation_data;
            $data['invoice_detail_data'] = $invoice_detail_data;
            $data['product_data'] = $product_data;

            echo json_encode($data);
        }else{
            $data = array();
            echo json_encode($data);
        }
    }

    function edit($id){
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["save_product"] =  $this->save_product;
                    $res["ckeditor"] = "yes";
                    // $res['image_upload_dir'] = $this->image_upload_dir;
                    // $res['countries'] = $this->basic_model->countries;
                // $res["data"] = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                    $res["data"] = $this->basic_model->getCustomRow("SELECT invoice.*, user.* FROM `invoice`  Left Join user ON invoice.customer_id = user.user_id where invoice.invoice_id=".$id);
                    $res["invoice_detail_data"] = $this->basic_model->getRows($this->table_detail,$this->table_fid,$res["data"]['invoice_id'],"invoice_detail_id","ASC");
                    $res['end_user'] = $this->basic_model->getRow('end_user','end_user_id',$res["data"]['end_user'],$this->orderBy,'end_user_id');
                    $res['selected_customer_end_users'] = $this->basic_model->getCustomRows("SELECT * FROm end_user JOIN city ON end_user.end_user_location= city.city_id where user_id = ".$res["data"]['customer_id']);
                    //$res['quotationData'] = $this->basic_model->getCustomRows("SELECT * FROM `quotation`");  
                    $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p/* RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND*/ where p.product_is_active = 1 /*GROUP BY p.product_id*/ ORDER BY p.product_name ASC");
                    $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1"); 
                    $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                    $res['currencyData'] = $this->basic_model->getCustomRows("SELECT * FROM `currency` order By id desc"); 
                    $res['terms'] = $this->basic_model->getCustomRows("SELECT * FROM `invoice_payment_term` where invoice_id=".$res["data"]['invoice_id']." order By invoice_payment_id asc"); 
                    $res['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1"); 
                    //$res["data"] = $this->basic_model->getRow('setting','setting_id',1,$this->orderBy,'setting_id');
                    $res['receipt_no'] = @$res['data']['invoice_no'];
                    $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
                $res['setting'] = $setting;

                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res["mail"] = $this->mail_product;
        $res['print'] = $this->print_product;
        $res["edit_payment"] = $this->edit_payment;
        $res["delete_payment"] = $this->delete_payment;
        $res["print_payment"] = $this->print_payment;
        
        $res['data'] = $this->basic_model->getCustomRows("
        SELECT invoice.*,user.user_name,end_user.*,city.*
        FROM `invoice` 
        INNER JOIN `user` ON invoice.customer_id = user.user_id 
        LEFT JOIN end_user ON invoice.end_user = end_user.end_user_id
        LEFT JOIN city ON city.city_id = end_user.end_user_location
        where invoice.revision = 0  ORDER BY invoice.create_date DESC limit 100");
        // print_b($res['data']);
        $res['edit_status'] = $this->edit_status;
        $res['history'] = $this->history_product;
        $res["ckeditor"] = "yes";
        $res['cityData'] = $this->basic_model->getCustomRows("SELECT * FROM city");
        $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
        $res['setting'] = $setting;
        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "where invoice.revision = 0 ";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(invoice.invoice_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(invoice.invoice_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            if($end_user != 0)
            {
                $where .= " AND invoice.end_user =".$end_user;
            }
            if($customer_id != 0)
            {
                $where .= " AND invoice.customer_id =".$customer_id;
            }
            if($invoice_no != "")
            {
                $where .= " AND invoice.invoice_no LIKE '%".$invoice_no."%'";
            }

            if($city_name != '0')
            {
                $where .= " AND LOWER(invoice.invoice_installation_location_details) = LOWER('".$city_name."') ";
            }
            

            $where .= " order By invoice.create_date desc limit 100";
            $query = "SELECT invoice.*,user.user_name,end_user.*,city.*
            FROM `invoice` 
            INNER JOIN `user` ON invoice.customer_id = user.user_id 
            LEFT JOIN end_user ON invoice.end_user = end_user.end_user_id
            LEFT JOIN city ON city.city_id = end_user.end_user_location ".$where;
            
            $temp = $this->basic_model->getCustomRows($query);
            $res['data'] = array();
            foreach (@$temp as $v) 
            {
                $v['perfoma_rev_no'] = find_rev_no($v['invoice_proforma_no']);
                array_push($res['data'],$v);
            }
            
            echo json_encode($res);
        }
    }

    function invoiceDetailDelete($id){
        // $del = $this->basic_model->deleteRecord('invoice_detail_id', $id, $this->table_detail);
        echo json_encode(array( "success" => "Record removed successfully","detailsDelete"=> true,"invoiceMultiTotalSet"=> true));

    }

    function delete($id){
        $del = $this->basic_model->deleteRecord('invoice_id', $id, 'invoice');
        $del = $this->basic_model->deleteRecord('invoice_id', $id, 'invoice_detail');
        echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "detailsDelete"=> true));

    }

    function detail($id){
        if( isset($id) && $id != ''){
            
                $res_int = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);

                $table_q = "quotation";
                $table_qid = "quotation_id";
                
                $table_c = "user";
                $table_cid = "user_id";

                $table_qd = "invoice_detail";
                $table_qd_id = "invoice_detail_id";

                // $res_quo = $this->basic_model->getRow($table_q,$table_qid,$res_int['quotation_id'],$this->orderBy,$table_qid);

                $res_quo_det = $this->basic_model->getRow($table_qd,$this->table_id,$res_int['invoice_id'],$this->orderBy,$table_qd_id);
                $res_cust = $this->basic_model->getRow($table_c,$table_cid,$res_int['customer_id'],$this->orderBy,$table_cid);
             
            if($res_int){
                $outputTitle = "";
                $outputTitle .= "Quotation Detail";

                $outputDescription = ""; 
                $outputDescription .= "<p>Invoice Number : ".$res_int['invoice_no']."</p>";
                $outputDescription .= "<p>Customer Name : ".$res_cust['user_name']."</p>";
                $outputDescription .= "<p>Total Value : ".$res_int['invoice_net_amount']."</p>";
                $outputDescription .= "<p>Proforma Invoice No : ".$res_int['invoice_proforma_no']."</p>";
                $outputDescription .= "<p>Current Status : ".$res_int['invoice_net_amount']."</p>";
                // $outputDescription .= "<p>Product Description  : ".$res_quo_det['invoice_detail_description']."</p>";
                // $outputDescription .= "<p>Quantity  : ".$res_int['invoice_detail_quantity']."</p>";
                // $outputDescription .= "<p>Rate : ".$res_int['invoice_detail_rate']."</p>";
 
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function customer_data(){
      extract($_POST);
      $data = $this->basic_model->getRow('user', 'user_id', $id, $this->orderBy, 'user_id');
      $data['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id='.$id);
      $data['end_user'] = $this->basic_model->getCustomRows('SELECT *  from end_user JOIN city ON end_user.end_user_location= city.city_id where user_id='.$id);
      if(empty($data['terms'])){
            $data['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
      }
      if(empty($data['end_user'])){
        $data['end_user'] = '';
      }
      echo json_encode($data);

    }

    function fetch_proforma_invoice(){
        extract($_POST);
        if($this->input->post()){
           
           /* $data['data'] = $this->basic_model->getCustomRows('SELECT p.product_id as prod_id, p.category_id, p.product_name, p.product_sku, p.product_description, p.product_is_active, p.chargeable, p.can_deliver, p.can_invoice, p.warranty_coverage, p.warranty_days, p.product_note, p.min_quantity, p.notify, p.category_type, user.*, quotation.*, quotation_detail.*, delivery_note.*, delivery_note_detail.*, SUM(quotation_detail.quotation_detail_quantity) as total_sum_pi_quantity, SUM(delivery_note_detail.delivery_note_detail_quantity) as total_sum_dn_quantity from quotation 
                LEFT JOIN quotation_detail ON quotation_detail.quotation_id = quotation.quotation_id
                LEFT JOIN delivery_note ON delivery_note.invoice_id = quotation.quotation_no
                LEFT JOIN delivery_note_detail ON delivery_note_detail.delivery_note_id = delivery_note.delivery_note_id
                LEFT JOIN user ON user.user_id = quotation.customer_id
                Right JOIN product p ON p.product_id = quotation_detail.product_id
                where quotation.quotation_no='.$quotation_number.' AND quotation.quotation_status = 4 AND quotation.quotation_revised_id = 0  '.$cond.' GROUP BY p.product_id Order By quotation_detail.quotation_detail_id ASC');*/
             $data['data'] = $this->basic_model->getCustomRows('SELECT p.product_id as prod_id, p.category_id, p.product_name, p.product_sku, p.product_description, p.product_is_active, p.chargeable, p.can_deliver, p.can_invoice, p.warranty_coverage, p.warranty_days, p.product_note, p.min_quantity, p.notify, p.category_type, user.*, quotation.*, quotation_detail.*,  SUM(quotation_detail.quotation_detail_quantity) as total_sum_pi_quantity, 
                 (Select  SUM(delivery_note_detail.delivery_note_detail_quantity)  FROM delivery_note LEFT JOIN delivery_note_detail on delivery_note.delivery_note_id = delivery_note_detail.delivery_note_id where delivery_note.invoice_id = quotation.quotation_no  AND delivery_note_detail.product_id = p.product_id) total_sum_dn_quantity
                from quotation 
                LEFT JOIN quotation_detail ON quotation_detail.quotation_id = quotation.quotation_id
                LEFT JOIN user ON user.user_id = quotation.customer_id
                Right JOIN product p ON p.product_id = quotation_detail.product_id
                where quotation.quotation_no='.$quotation_number.' AND quotation.quotation_status = 4 AND quotation.quotation_revised_id = 0   GROUP BY p.product_id Order By quotation_detail.quotation_detail_id ASC');
           
            $delivery_data = $this->basic_model->getCustomRows('SELECT p.product_id as prod_id, p.category_id, p.product_name, p.product_sku, p.product_description, p.product_is_active, p.chargeable, p.can_deliver, p.can_invoice, p.warranty_coverage, p.warranty_days, p.product_note, p.min_quantity, p.notify, p.category_type, user.*,  delivery_note.*, delivery_note_detail.*, SUM(delivery_note_detail.delivery_note_detail_quantity) as total_sum_pi_quantity, SUM(delivery_note_detail.delivery_note_detail_quantity) as total_sum_dn_quantity from delivery_note 
                LEFT JOIN delivery_note_detail ON delivery_note_detail.delivery_note_id = delivery_note.delivery_note_id
                LEFT JOIN user ON user.user_id = delivery_note.customer_id
                Right JOIN product p ON p.product_id = delivery_note_detail.product_id
                where delivery_note.invoice_id='.$quotation_number.'  GROUP BY p.product_id');

            foreach($delivery_data as $get_row){
                $is_found = 0;
                for($i= 0; $i<sizeof($data['data']); $i++){
                    $data_row = $data['data'][$i];
                    if($get_row['prod_id'] == $data_row['prod_id']){
                        $is_found = 1;
                       //$data['data'][$i]['total_sum_dn_quantity'] = ($data['data'][$i]['total_sum_pi_quantity'] > $get_row['total_sum_dn_quantity']) ? $data['data'][$i]['total_sum_pi_quantity'] : $get_row['total_sum_dn_quantity'];                           
                    }
                }
                if($is_found == 0){
                    $this_data = [
                            'prod_id' => $get_row['prod_id'],
                            'total_sum_pi_quantity' => $get_row['total_sum_pi_quantity'],
                            'total_sum_dn_quantity' => $get_row['total_sum_dn_quantity'],
                            'quotation_detail_total_discount_type' => 0,
                            'quotation_detail_total_discount_amount' => 0,
                            'quotation_detail_rate' => $get_row['total_sum_dn_quantity'],
                            'quotation_detail_rate' => $get_row['base_price'],
                            'quotation_detail_rate_usd' => $get_row['usd_price'],
                            'quotation_detail_description' => $get_row['product_description'],
                            'quotation_total_discount_amount' => 0,
                            'quotation_total_discount_type' => 0,
                            'can_deliver' => $get_row['can_deliver'],
                            'can_invoice' => $get_row['can_invoice'],
                        ];
                        array_push($data['data'], $this_data);
                }
            }

            $data['productData'] = $this->basic_model->getCustomRows("SELECT p.* FROM `product` p WHERE p.product_is_active = 1  ORDER BY p.product_name ASC"); 
            $data['delivery_note_found'] = 0;

            //replace prices with last invoice
            if(isset($data['data'][0])){
                $i = 0;
                foreach($data['data'] as $row){
                    $price = $this->basic_model->getCustomRows('SELECT * from invoice Left join invoice_detail  ON invoice.invoice_id = invoice_detail.invoice_id where invoice.customer_id="'.@$data['data'][$i]['customer_id'].'" AND invoice_detail.product_id ="'. @$data['data'][$i]['prod_id'].'" ORDER BY invoice.invoice_id DESC');
                    if(!empty($price)){
                        //$data['data'][$i]['quotation_detail_rate_usd'] = @$price[0]['invoice_detail_usd_rate'];
                        //$data['data'][$i]['quotation_detail_rate']     = @$price[0]['invoice_detail_base_rate'];
                    }
                 $i++;
                }
            }

            //find payment terms
            if(isset($data['data'][0]['quotation_id'])){
                //find from quotation
                $data['terms'] = $this->basic_model->getCustomRows('SELECT *  from quotation_payment_term where quotation_id='.@$data['data'][0]['quotation_id']);
                //find from client
                if(empty($data['terms'])){
                    $data['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id='.@$data['data'][0]['customer_id']);
                }
            } 
            //find payment from settings if not found
            if(empty($data['terms'])){
                $data['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
            }
            echo json_encode($data);
            
        }

    }

    function mail_invoice()
    {
        extract($_POST);
        if(isset($id) && $id != null) {
            $invoice = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
            predefine_send_email($_POST);
            if($invoice['invoice_status'] < 4){
                //History
                $this->makeInvoiceHistory($id);    
                //End History
                $data = array();
                if($quotation['invoice_email_printed_status'] == 0)
                {
                    $data['invoice_email_printed_status'] = 1;
                    $data['invoice_revised_no'] = $invoice['invoice_revised_no'] + 1;
                    
                }
                else if($invoice['invoice_email_printed_status'] > 0)
                {
                     $data['invoice_revised_no'] = $invoice['invoice_revised_no'] + 1;
                }
                if($invoice['invoice_final'] == '' || $invoice['invoice_final'] < 1 || $invoice['invoice_status'] == 2){
                    $data['invoice_status'] = 1;
                }
                $this->basic_model->updateRecord(['invoice_final' => 1], 'delivery_note', 'invoice_id', $invoice['invoice_no']);
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                
            }
            echo json_encode(array( "success" => "Mail Send successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
        }
        else
        {
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
        }
        
    }

    function print_invoice()
    {
        if ($this->check_print) {
            extract($_GET);
            if(isset($id) && $id != null) {
                $history_table = "";
                if(isset($history) && $history == 1){
                    $history_table = "_history";
                }
                $invoice = $this->basic_model->getRow($this->table.$history_table,$this->table_id,$id,$this->orderBy,$this->table_id);
                $res['invoice'] = $invoice;
                $res['data'] = $this->basic_model->getCustomRows("
                SELECT invoice.*,user.user_name, invoice_detail.*, product.*, currency.*
                FROM invoice".$history_table." as invoice
                LEFT JOIN `user` ON invoice.customer_id = user.user_id 
                LEFT JOIN `invoice_detail".$history_table."` invoice_detail ON invoice.invoice_id = invoice_detail.invoice_id 
                LEFT JOIN `product` ON invoice_detail.product_id = product.product_id 
                LEFT JOIN `currency` ON invoice.invoice_currency_id = currency.id 
                where  invoice.invoice_id = ".$id." ORDER BY invoice.create_date DESC");
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $res['setting'] = $setting;
                $res['base_currency_id'] = $setting['id'];
                $res["currency_name"] = $setting['title'];
                $res['invoice_currency_name'] = $this->basic_model->getCustomRow("SELECT * FROM `currency` where `id` = '".$invoice['invoice_currency_id']."' ");
                $res['bank'] = $this->basic_model->getCustomRows("SELECT * FROM bank_setting");
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $res['customer_data'] = $this->basic_model->getRow('user','user_id',@$invoice['customer_id'],$this->orderBy,'user_id');
                $res['inline_discount'] = false;
                foreach($res["data"] as $find){
                    //find inline discount
                    if($find['invoice_detail_total_discount_amount'] > 0){
                        $res['inline_discount'] = true;
                    }  
                } 
                $res['employee_data'] = $this->basic_model->getCustomRow("SELECT * FROM `user` left join department ON department.department_id = user.department_id WHERE user.user_type = 2 AND user.user_is_active = 1 AND user.user_id=".@$invoice['employee_id']);
                $res['terms'] = $this->basic_model->getCustomRows("SELECT *  from invoice_payment_term".$history_table." WHERE invoice_id = '".$id."' ");
                if(empty($res['terms'])){
                    $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id='.$invoice['customer_id']);
                }

                if(!isset($view) && $invoice['invoice_status'] < 4 && $invoice['invoice_status'] != 1){
                    //History
                    $this->makeInvoiceHistory($id);    
                    //End History
                    $data = array();
                    if($invoice['invoice_email_printed_status'] == 0)
                    {
                        $data['invoice_email_printed_status'] = 1;
                        $data['invoice_revised_no'] = $invoice['invoice_revised_no'] + 1;
                    }
                    else if($invoice['invoice_email_printed_status'] > 0)
                    {
                        $data['invoice_revised_no'] = $invoice['invoice_revised_no'] + 1;
                        
                    }
                
                    if($invoice['invoice_final'] == '' || $invoice['invoice_final'] < 1 || $invoice['invoice_status'] == 2){
                        $data['invoice_status'] = 1;
                        
                    }
                    $this->basic_model->updateRecord(['invoice_final' => 1], 'delivery_note', 'invoice_id', $invoice['invoice_no']);
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                }

                //print_b($res);
                $this->basic_model->make_pdf($header_check,$res,'Report','invoice_Report');

                // $this->load->view('invoice_Report',$res);

            }
            else
            {
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
        
    }
  
    function change_status(){
        extract($_POST);
        $data = array();
        if(isset($id) && $id != null) {
            if($status_comment != ''){
                $data['invoice_status_comment'] = $status_comment;
                $data = array();
            }
            //History
            $this->makeInvoiceHistory($id);    
            //End History

            $detail = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
            //add in amc
            if($status == 4){
                $this->active_amc($detail);
            }
            $data['invoice_email_printed_status'] = 1;
            $data['invoice_final'] = 1;
            //$data['quotation_revised_no'] = $detail["quotation"]['quotation_revised_no'] + 1;
            // if($detail['quotation_status'] == 0)
            // {
                // $status = 1;
                $data['invoice_status'] = $status;
                $revised = ($detail['invoice_revised_no'] > 0 ) ? $detail['invoice_revised_no'] : 0;
                $data['invoice_revised_no'] = $revised + 1;
                $data['invoice_status_comment'] = $status_comment;
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                
                echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
            // }
            // else
            // {
            //     echo json_encode(array("error" => "Status is Final, can't change."));
            // }
            
            
            // print_b($detail);
            
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
        }
    }

    function makeInvoiceHistory($id){
        // $revision_id = $this->basic_model->insertRecord(["revision_title"=> 'Invoice'],'tbl_revision');
        $invoice_data = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
        $invoice_detail = $this->basic_model->getCustomRows('SELECT * FROM invoice_detail where invoice_id = "'.@$invoice_data['invoice_id'].'" AND (revision IS NULL || revision = 0)');
        $payment_term = $this->basic_model->getCustomRows('SELECT * FROM invoice_payment_term where invoice_id = "'.@$invoice_data['invoice_id'].'" AND (revision IS NULL || revision = 0)');

        $data= array(
            'customer_id' => $invoice_data['customer_id'],
            'invoice_no' => $invoice_data['invoice_no'],
            'invoice_currency_id' => $invoice_data['invoice_currency_id'],
            'invoice_date' => $invoice_data['invoice_date'],
            'invoice_proforma_no' => $invoice_data['invoice_proforma_no'],
            'invoice_total_discount_type' => $invoice_data['invoice_total_discount_type'],
            'invoice_total_discount_amount' => $invoice_data['invoice_total_discount_amount'],
            'invoice_subtotal'=> $invoice_data['invoice_subtotal'],
            'invoice_buyback' => $invoice_data['invoice_buyback'],
            'invoice_tax' => $invoice_data['invoice_tax'],
            'invoice_tax_amount' =>$invoice_data['invoice_tax_amount'],
            'invoice_net_amount' => $invoice_data['invoice_net_amount'],
            'invoice_total_amount' => $invoice_data['invoice_subtotal'],
            "revision" =>  0,//$revision_id,
            "belongs_to" => $id,
            "invoice_status" => $invoice_data['invoice_status'],
            "employee_id" => $invoice_data['employee_id'],
            "freight_type" => $invoice_data['freight_type'],
            "invoice_revised_no" => $invoice_data['invoice_revised_no'],
            "invoice_notes" => $invoice_data['invoice_notes'],
            "discount_label" => $invoice_data['discount_label'],

        );
        $invoice_id = $this->basic_model->insertRecord($data,$this->table."_history");

        foreach ($invoice_detail as $k => $v) {
            $arr = array(
                'product_id' => $v['product_id'],
                'invoice_id' => $invoice_id,
                'invoice_detail_pi_quantity' => $v['invoice_detail_pi_quantity'],
                'invoice_detail_dn_quantity' => $v['invoice_detail_dn_quantity'],
                'invoice_detail_quantity' => $v['invoice_detail_quantity'],
                'invoice_detail_description' => $v['invoice_detail_description'],
                'invoice_detail_total_discount_type' => $v['invoice_detail_total_discount_type'],
                'invoice_detail_total_discount_amount' => $v['invoice_detail_total_discount_amount'],
                'invoice_detail_usd_rate' => $v['invoice_detail_usd_rate'],
                'invoice_detail_base_rate' => $v['invoice_detail_base_rate'],
                'invoice_detail_discrepancy' => $v['invoice_detail_discrepancy'],
                'invoice_detail_total_amount' => $v['invoice_detail_total_amount'],
                "revision" => 0,//$revision_id,
                "belongs_to" => $id
            );
            $invoice_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail."_history");
        }

        foreach($payment_term as $payment){
            $payment_data = [
                'client_id' => $payment['client_id'],
                'invoice_id' => $invoice_id,
                'payment_title' => $payment['payment_title'],
                'percentage' => $payment['percentage'],
                'payment_days' => $payment['payment_days'],
                'payment_amount' => $payment['payment_amount'],
                "revision" => 0,//$revision_id,
                "belongs_to" => $id
            ];
            $this->basic_model->insertRecord($payment_data,'invoice_payment_term_history');
        }
        
        return true;
    }

    function history(){
        if ($this->check_history) {
            extract($_GET);
            // OR quotation_id = '".$id."'
            $data =  $this->basic_model->getCustomRows("SELECT * from invoice_history where belongs_to='".$id."'  ORDER BY invoice_id DESC");
            echo json_encode($data);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
    }

    function follow_up(){
        if ($this->check_history) {
            extract($_GET);
            // OR quotation_id = '".$id."'
            $data =  $this->basic_model->getCustomRows("SELECT * from follow_up JOIN user on follow_up.follow_up_sales_person = user.user_id where follow_up.follow_up_doc_type='3' AND follow_up.follow_up_doc_id='".$id."'  ORDER BY follow_up_id  DESC");
            echo json_encode($data);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
    }

    function find_invoices(){
        extract($_POST);
        if(isset($customer_id) && $customer_id != ''){
            $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT q.quotation_no as invoice_num, q.quotation_revised_no as revised_no, count(inv.invoice_proforma_no) as total_invoice FROM `quotation` q Left join invoice inv on inv.invoice_proforma_no = q.quotation_no WHERE q.quotation_revised_id = 0 AND q.quotation_status = 4 AND q.customer_id = ".$customer_id." group by q.quotation_no order by q.quotation_id desc");
            echo json_encode($res);
        }
    }
    function active_amc($invoice){
        $data = [
            'invoice_id' => $invoice['invoice_id'],
            'customer_id' => $invoice['customer_id'],
            'additional_expiry_date' => $invoice['invoice_expire_date'], //date('d-m-Y'),
            'additional_discount_type' => $invoice['invoice_total_discount_type'],
            'additional_discount_value' => $invoice['invoice_total_discount_amount']
        ];
       $invoice_id = $this->basic_model->insertRecord($data, 'invoice_additional'); 
    }

}