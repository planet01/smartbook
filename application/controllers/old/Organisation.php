<?php
    class Organisation extends MY_Back_Controller{
        public function __CONSTRUCT(){
            parent::__construct();
            $this->add_page_product = "add-organisation";
            $this->add_product = 'Organisation/add';
            $this->view_page_product = "view-all_organisation";
            $this->edit_product = 'organisation/edit';
            $this->delete_product = 'organisation/delete';
            $this->view_product = 'my_account';
            $this->detail_product = 'organisation/detail';
            $this->page_heading = "Organization";
            $this->redirect = 'my_account';
            $this->table = "organisation";
            $this->pid = "id";
            $logged_in = $this->is_login_admin();
            if (!$logged_in) {
                redirect('user');
            }
        }
        public function index(){
            $res['title'] = "Item";
            $res['active'] = "item";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $this->template_view($this->add_page_product,$res);
            
        }
        
        function edit($id){
            extract($_POST);
            $res["title"] = "Edit ".$this->page_heading;
            $res["page_title"] = "";
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res['id'] = $id;
            //$res["data"] = $this->basic_model->getRow($this->table,$this->table_pid,$id);
            $this->template_view($this->add_page_product,$res);
        }
        
    }