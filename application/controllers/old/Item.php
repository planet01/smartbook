<?php
    class Item extends MY_Back_Controller{
        public function __CONSTRUCT(){
            parent::__construct();
            $this->view_page_product = "view-all_item";
            $this->add_page_product = "add-item";
            $this->add_product = 'item/add';
            $this->edit_product = 'item/edit';
            $this->delete_product = 'item/delete';
            $this->view_product = 'my_account';
            $this->detail_product = 'item/detail';
            $this->page_heading = "Item";
            $this->redirect = 'my_account';
            $this->table = "item";
            $this->pid = "id";
            $logged_in = $this->is_login_admin();
            if (!$logged_in) {
                redirect('user');
            }
        }
        public function index(){
            $res['title'] = "Item";
            $res['active'] = "item";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $this->template_view($this->add_page_product,$res);
            
        }
        function add(){
            extract($_POST);
            if( $this->input->post()){
                
            }
            else{
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $this->template_view($this->add_page_product,$res);
            }
        }
        function view() {
            $res['title'] = "View All ".$this->page_heading;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["edit_product"] = $this->edit_product;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product;
            //$res['data'] = $this->basic_model->getCustomRows("SELECT * FROM `item` ");
            $this->template_view($this->view_page_product,$res);
        }
    }