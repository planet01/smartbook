<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AMC_Receivables extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'amc_receivables/add';
        $this->view_product = 'amc_receivables/view';
        $this->add_payment  = 'AMC_Payment/view';
        $this->edit_payment = 'AMC_Payment/edit';
        $this->delete_payment = "AMC_Payment/delete";
        $this->invoice_payment = 'AMC_Payment/invoice_payment';
        $this->delete_product = 'amc_receivables/delete';
        $this->detail_product = 'amc_receivables/detail';
        $this->quotation_print = 'amc_quotation/print';
        $this->invoice_print   = 'invoice/print_invoice';
        $this->receivable_report= 'Receivables/receivable_report';
        // page active
        $this->add_page_product = "add-amc-receivables";
        $this->edit_page_product = "edit-amc-receivables";
        $this->view_page_product = "view-amc-receivables";
        $this->print = 'amc_receivables/print';
        // table
        $this->table = "amc_quotation";
        $this->table_pid = "amc_quotation_id";
        $this->tables = "setting";
        $this->table_sid = "setting_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/product";
        // Page Heading
        $this->page_heading = "Receivables";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 14) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
        }

    }
    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
    
    function view() {
        if ($this->check_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["add_payment"] = $this->add_payment;
            $res["edit_payment"] = $this->edit_payment;
            $res["delete_payment"] = $this->delete_payment;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product;
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res["print"] = $this->print;
            $res["invoice_payment"] = $this->invoice_payment;
            $res["quotation_print"] = $this->quotation_print;
            $res["invoice_print"] = $this->invoice_print;
            $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $res['receivable_report'] = $this->receivable_report;

            /* $invoice = $this->basic_model->getCustomRows("SELECT i.*,u.user_name,u.user_company_name,u.currency_id FROM invoice i JOIN user u ON u.user_id = i.customer_id  WHERE i.invoice_status = 1 AND i.revision = 0 AND (i.invoice_proforma_no = 0 || i.invoice_proforma_no IS NULL)");*/

            $quotation = $this->basic_model->getCustomRows("SELECT q.*, u.* FROM amc_quotation q JOIN user u ON u.user_id = q.customer_id  WHERE  q.amc_quotation_status = 4 AND q.amc_quotation_revised_id = 0 ");
            $setval = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
            $total_paid_amount = 0;
            $total_unpaid_amount = 0; 
            $total_recv_amount = 0;
            //$invoice_detail = $this->basic_model->getCustomRows("SELECT * FROM invoice_detail");
            //$payment = $this->basic_model->getCustomRows("SELECT * FROM payment");
            
            $unpaid_invoice = array();
            foreach($quotation as $q){
            /*
                    $temp_find_inv = $this->basic_model->getCustomRow("SELECT i.*,u.user_name,u.user_company_name,u.currency_id FROM invoice i JOIN user u ON u.user_id = i.customer_id  WHERE i.invoice_proforma_no = ".$q['quotation_no']);*/

                    $payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM amc_payment WHERE invoice_id = '".$q['amc_quotation_id']."' AND (payment_revised_id = 0 || payment_revised_id IS NULL)  ");
                    // $terms = $this->basic_model->getCustomRows("SELECT * FROM `quotation_payment_term` where quotation_id=".$q['quotation_id']." order By quotation_payment_id asc"); 
                    $tot = (!empty($payment['total_payment_amount']))? $payment['total_payment_amount']+$payment['total_payment_adjustment'] : 0;
                    $amc_type = $this->basic_model->getCustomRow("SELECT sum(amc_type_total_amount) as amc_type_total_amount from amc_type where amc_quotation_id='".$q['amc_quotation_id']."' ORDER BY amc_type_id asc");    
                    $qt_price = @$amc_type['amc_type_total_amount'];
                    // print_b($qt_price);
                    if($tot < $qt_price){
                        $rev = ($q['amc_quotation_revised_no'] > 0)?'-R'.$q['amc_quotation_revised_no'] : '';
                        $data = [];
                        
                        $data['total_amount_base'] = $qt_price;
                        $data['paid_amount_base']  = $tot;
                        $data['amount_left_base']  = $qt_price - $tot;
                        $data['invoice_date']      = $q['amc_quotation_date'];
                        $data['invoice_no']        = @$setval["company_prefix"];
                        $data['invoice_no'] .=($q['amc_quotation_status'] == 4 ||  $q['amc_quotation_status'] == 7)? 'AMCINV-': @$setval["quotation_prfx"];
                        $data['invoice_no'] .=($q['amc_quotation_revised_no'] > 0)?@$q['amc_quotation_no'].'-R'.number_format(@$q['amc_quotation_revised_no']):@$q['amc_quotation_no'];
                        // $data['invoice_no']        =  @$setval["company_prefix"].@$setval["quotation_prfx"].$q['amc_quotation_no'].$rev;
                        $data['user_company_name'] = $q['user_company_name'];
                        $data['invoice_id']        = $q['amc_quotation_id'];
                        $data['is_invoice']        = 0;
                        $data['expiry_date_by_pyment_term'] = $q['amc_quotation_expiry_date'];
                        $total_paid_amount = $total_paid_amount + $tot;
                        $get_tot = ($qt_price - $tot);
                        $total_recv_amount = floatval($total_recv_amount) + floatval($get_tot);
                        /* $find_unpaid = $this->find_amount_by_payment_term($terms, $q['amc_quotation_date'], $qt_price, 1);*/
                        $total_unpaid = $qt_price - $tot;
                        $data['expiry_amount_by_pyment_term'] = (floatval($total_unpaid) > 0)? floatval($total_unpaid) : 0;
                        array_push($unpaid_invoice,$data);
                        if($total_unpaid > 0){
                            $total_unpaid_amount = floatval($total_unpaid_amount) + floatval($total_unpaid);
                        }
                    }
                    
                    

                }
                

        
                

            $res['data'] = $unpaid_invoice;
            $res['total_paid_amount'] = number_format((float)$total_paid_amount, 2, '.', '');
            $res['total_unpaid_amount'] = number_format((float)$total_unpaid_amount, 2, '.', '');
            $res['total_recv_amount'] = number_format((float)$total_recv_amount, 2, '.', '');
            $res["setval"] = $setval;
            $res['currencyData'] = $this->basic_model->getCustomRows("SELECT * FROM `currency` order By id desc"); 
            $res["print"] = $this->print;
            $this->template_view($this->view_page_product,$res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }


    function quotation_price($quotation){
           $amc_type = $this->basic_model->getCustomRow("SELECT sum(amc_type_total_amount) as amc_type_total_amount from amc_type where amc_quotation_id='".$quotation['amc_quotation_id']."' ORDER BY amc_type_id asc");    
            $qt_price = @$amc_type['amc_type_total_amount'];
            return Array(
              'total_amount_base' => number_format((float)$qt_price, 2, '.', ''),
              );
    }

    function receivable_report(){
        extract($_GET);
        if(isset($customer_id) && $customer_id != ''){     
                $payment_date_range =  " ";
                $payment_date_range1 =  " ";
                $invoice_date_range =  " ";
                $quotation_date_range =  " ";
                if(isset($date_from) && isset($date_to) && $invoice_type == 1){
                    $date_from = date('Y-m-d', strtotime($date_from));
                    $date_to = date('Y-m-d', strtotime($date_to));
                    $payment_date_range = " AND (STR_TO_DATE(`payment_date`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."')";
                    $payment_date_range1 = " AND STR_TO_DATE(`payment_date`, '%Y-%m-%d') < ".$date_from;
                   
                    $quotation_date_range = " AND (STR_TO_DATE(`q`.`amc_quotation_date`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."')";
                }

              
                $quotation = $this->basic_model->getCustomRows("SELECT q.*, u.* FROM amc_quotation q JOIN user u ON u.user_id = q.customer_id  WHERE  (q.amc_quotation_status = 4 || q.amc_quotation_status = 7) AND q.amc_quotation_revised_id = 0 AND q.customer_id=".$customer_id.$quotation_date_range);
                // print_b("SELECT q.*, u.* FROM amc_quotation q JOIN user u ON u.user_id = q.customer_id  WHERE  (q.amc_quotation_status = 4 || q.amc_quotation_status = 7) AND q.amc_quotation_revised_id = 0 AND q.customer_id=".$customer_id.$quotation_date_range);
                $setval = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $total_paid_amount = 0;
                $total_invoiced_amount = 0;
                $total_unpaid_amount = 0; 
                $total_opening_balance = 0;
                $total_recv_amount = 0;
         
                // print_b($quotation);
                $unpaid_invoice = array();
                foreach($quotation as $q){
                   
                        $payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM amc_payment WHERE invoice_id = '".$q['amc_quotation_id']."' AND (payment_revised_id = 0 || payment_revised_id IS NULL)".$payment_date_range);
                       /* $terms = $this->basic_model->getCustomRows("SELECT * FROM `quotation_payment_term` where quotation_id=".$q['quotation_id']." order By quotation_payment_id asc"); */
                        $tot = (!empty($payment['total_payment_amount']))? $payment['total_payment_amount']+$payment['total_payment_adjustment'] : 0;
                        $qt_price = $this->quotation_price($q);
                       // if($tot < $qt_price['total_amount_base']){
                            $rev = ($q['amc_quotation_revised_no'] > 0)?'-R'.$q['amc_quotation_revised_no'] : '';
                            $data = [];
                            $data['total_amount_base'] = $qt_price['total_amount_base'];
                            $data['paid_amount_base']  = $tot;
                            $data['amount_left_base']  = $qt_price['total_amount_base'] - $tot;
                            $data['invoice_date']      = $q['amc_quotation_date'];
                            $data['invoice_no']        =  @$setval["company_prefix"].@$setval["quotation_prfx"].$q['amc_quotation_no'].$rev;
                            $data['user_company_name'] = $q['user_company_name'];
                            $data['invoice_id']        = $q['amc_quotation_id'];
                            $data['is_invoice']        = 0;
                            $data['expiry_date_by_pyment_term'] = $q['amc_quotation_expiry_date'];                 
                            
                            //$total_unpaid_amount = $total_unpaid_amount + ($qt_price['total_amount_base'] - $tot);
                           /* $find_unpaid = $this->find_amount_by_payment_term($terms, $q['amc_quotation_date'], $qt_price['total_amount_base'], 1);*/
                            $total_unpaid = $qt_price['total_amount_base'] - $tot;
                            $data['expiry_amount_by_pyment_term'] = (floatval($total_unpaid) > 0)? floatval($total_unpaid) : 0;
                            //if($data['amount_left_base'] > 0){
                                $total_paid_amount = $total_paid_amount + $tot;
                                $total_recv_amount = $total_recv_amount + ($qt_price['total_amount_base'] - $tot);
                                $total_invoiced_amount = $total_invoiced_amount + $qt_price['total_amount_base'];
                                array_push($unpaid_invoice,$data);
                            //}
                            if($total_unpaid > 0){
                                $total_unpaid_amount = $total_unpaid_amount + $total_unpaid;
                            }
                            //count opening balance
                            /*$payment1 = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE invoice_id = '".$q['quotation_no']."'  AND (payment_revised_id = 0 || payment_revised_id IS NULL)".$payment_date_range1);
                            $tot1 = (!empty($payment1['total_payment_amount']))? $payment1['total_payment_amount']+$payment1['total_payment_adjustment'] : 0;*/
                            //$total_opening_balance = $total_opening_balance+$tot1;

                        //}


                }

                
              /*  if($invoice_type == 1){

                    $qt_payment = $this->basic_model->getCustomRows("SELECT q.* FROM quotation q LEFT JOIN invoice inv ON q.quotation_no = inv.invoice_proforma_no  WHERE  q.quotation_status = 4 AND q.quotation_revised_id = 0  AND (inv.invoice_proforma_no = 0 || inv.invoice_proforma_no IS NULL) AND q.customer_id=".$customer_id." AND  STR_TO_DATE(q.quotation_date, '%d-%m-%Y') < '".$date_from."'");

                    $inv_payment = $this->basic_model->getCustomRows("SELECT i.*, q.quotation_id FROM invoice i LEFT JOIN quotation q ON q.quotation_no = i.invoice_proforma_no  WHERE i.revision = 0 AND i.customer_id=".$customer_id." AND  STR_TO_DATE(i.invoice_date, '%d-%m-%Y') < '".$date_from."'");

                    $proforma_ids = array_column($inv_payment, 'quotation_id');
                    $quotation_ids = array_column($qt_payment, 'quotation_id');
                    $quotation_ids = array_merge($proforma_ids, $quotation_ids);
                    $qt_ids = implode(",",$quotation_ids);
                    $qt_total_payment = array_column($qt_payment, 'net_amount');
                    $qt_total_payment_amount = array_sum($qt_total_payment);
                    $invoice_ids = array_column($inv_payment, 'invoice_id');
                    $inv_ids = implode(",",$invoice_ids);
                    $inv_total_payment = array_column($inv_payment, 'invoice_net_amount');
                    $inv_total_payment_amount = array_sum($inv_total_payment);
                    $total_paid = 0;
                    if(!empty($invoice_ids)){
                     $size = sizeof($invoice_ids);
                     if($size > 1){
                        $clause = " IN (".$inv_ids.") ";
                     }else{
                        $clause = " = ".$inv_ids;
                     }
                     $rec_inv_payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE invoice_id  ".$clause." AND (payment_revised_id = 0 || payment_revised_id IS NULL) AND is_invoice = 1 ");
                      $total_paid = floatval($total_paid) + floatval(@$rec_inv_payment['total_payment_amount']) + floatval(@$rec_inv_payment['total_payment_adjustment']);
                    }
                    if(!empty($quotation_ids)){
                     $size = sizeof($quotation_ids);
                     if($size > 1){
                        $clause = " IN  (".$qt_ids.") ";
                     }else{
                        $clause = " = ".$qt_ids;
                     }
                     $rec_qt_payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE invoice_id ".$clause."   AND (payment_revised_id = 0 || payment_revised_id IS NULL) AND is_invoice = 0 ");
                      $total_paid = floatval($total_paid) + floatval(@$rec_qt_payment['total_payment_amount']) + floatval(@$rec_qt_payment['total_payment_adjustment']);
                    }

                    $total_opening_balance = (floatval(@$inv_total_payment_amount) + floatval(@$qt_total_payment_amount)) - floatval($total_paid);
                }else{
                    $total_opening_balance = 0;
                }*/
                    $total_opening_balance = 0;

                // print_b($unpaid_invoice);
                $res['date_from'] = date("d/m/Y", strtotime($date_from));
                $res['date_to'] = date("d/m/Y", strtotime($date_to));
                $res['invoice_type'] = $invoice_type;
                $res['total_opening_balance'] = number_format((float)$total_opening_balance, 2, '.', '');;
                $res['total_paid_amount'] = number_format((float)$total_paid_amount, 2, '.', '');
                $res['total_invoiced_amount'] = number_format((float)$total_invoiced_amount, 2, '.', '');
                $res['total_unpaid_amount'] = number_format((float)$total_unpaid_amount, 2, '.', '');
                $res["setval"] = $setval;
                $res['data'] = $unpaid_invoice;
                $res['currencyData'] = $this->basic_model->getCustomRow("SELECT * FROM `currency` where id = 2  order By id desc"); 
                $res['customer_data'] = $this->basic_model->getCustomRow("SELECT * FROM `user` WHERE user_id = ".$customer_id);
                $this->basic_model->make_pdf(0,$res,'Report','soa_Report',0,1);
                //$this->template_view($this->view_page_product,$res);
        }
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $invoice = $this->basic_model->getCustomRow("SELECT i.*,u.user_name FROM invoice i JOIN user u ON u.user_id = i.customer_id  WHERE i.invoice_id = '".$id."' AND i.invoice_status = 2 AND i.revision = 0  ");
        
            if($invoice){
                $invoice_detail = $this->basic_model->getCustomRows("SELECT * FROM invoice_detail WHERE invoice_id='".$id."'");
                $payment = $this->basic_model->getCustomRows("SELECT * FROM payment WHERE invoice_id='".$id."'");

                $amount_base = 0;
                $amount_paid_base = 0;
                foreach ($invoice_detail as $k2 => $v2) {
                    if($v2['invoice_id'] == $invoice['invoice_id'])
                    {
                        $currency_data = $this->basic_model->getCustomRow("SELECT * FROM currency where id = '".$invoice['invoice_currency_id']."' ");
                        $price_field_temp = '';
                        if(trim($currency_data['title']) == 'AED')
                        {
                            $price_field_temp = 'base';   
                        }else
                        {
                            $price_field_temp = strtolower(trim($currency_data['title']));
                        }
                        $price_field = 'invoice_detail_'.$price_field_temp.'_rate';
                        $cal_amount = $v2['invoice_detail_quantity'] * $v2[$price_field];
                        $temp_amount_base = $cal_amount;
                        if($v2['invoice_detail_total_discount_type'] == 1)
                        {
                            if($v2['invoice_detail_total_discount_amount'] > 0 )
                            {
                                $temp_amount_base = $temp_amount_base * ($v2['invoice_detail_total_discount_amount']/100);
                            }
                        }
                        $amount_base += $temp_amount_base;
                    }
                }//amount me total amounty agai discount lga k invoice ki.
                if($invoice['invoice_total_discount_type'] == 1)
                {
                    if($invoice['invoice_total_discount_amount'] > 0)
                    {
                        $amount_base = $amount_base * ($invoice['invoice_total_discount_amount']/100);
                    }
                }

                foreach ($payment as $k3 => $v3) 
                {
                    if($v3['invoice_id'] == $invoice['invoice_id'])
                    {
                        $amount_paid_base += $v3['payment_amount'];
                    }
                }

                if($amount_paid_base < $amount_base)
                {
                    $invoice['total_amount_base'] = $amount_base;
                    $invoice['paid_amount_base'] = $amount_paid_base;
                    $invoice['amount_left_base'] = $amount_base - $amount_paid_base;
                }
                
                $outputTitle = "";
                $outputTitle .= "Receivable Details";

                $outputDescription = "";
                $outputDescription .= "<p>Customer Name : ".$invoice['user_name'];
                $outputDescription .= "<p>Invoice No : ".$invoice['invoice_no']."</p>";
                $outputDescription .= "<p>Invoice Total Payment : ".$invoice['total_amount_base']."</p>";
                $outputDescription .= "<p>Invoice Paid Payment : ".$invoice['paid_amount_base']."</p>";
                $outputDescription .= "<p>Invoice Amount Left : ".$invoice['amount_left_base']."</p>";
                if(isset($payment) && sizeof($payment) > 0){
                    $outputDescription .= "<h4><b>Payment Detail</b></h4>";
                    $count = 1;
                    $outputDescription .= "<table class='table'>";
                    $outputDescription .= "<tr>";
                    $outputDescription .= "<th>#</th>";
                    $outputDescription .= "<th>Payment Date</th>";
                    $outputDescription .= "<th>Payment Amount</th>";
                    $outputDescription .= "<th>Payment Adjustment</th>";
                    $outputDescription .= "</tr>";
                    foreach ($payment as $k => $v) {
                        $outputDescription .= "<tr>";
                        $outputDescription .= "<td>".$count."</td>";
                        $outputDescription .= "<td>".$v['payment_date']."</td>";
                        $outputDescription .= "<td width='160px' class='text-right'>".number_format((float)$v['payment_amount'], 2, '.', '')."</td>";
                        $outputDescription .= "<td width='190px' class='text-right'>".number_format((float)$v['payment_adjustment'], 2, '.', '')."</td>";
                        $outputDescription .= "</tr>";
                        $count ++;
                    }
                    $outputDescription .= "</table>";
                 ;

                }
              
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
    
    function print(){
        extract($_GET);
        // print_b($_GET);
        //id will be customer_id
        if(isset($id) && $id != null) {
           /* $res['image_upload_dir'] = $this->image_upload_dir;

            $res['invoice'] = $this->basic_model->getCustomRows("SELECT i.*,u.user_name,u.currency_id FROM invoice i JOIN user u ON u.user_id = i.customer_id  WHERE i.customer_id = '".$id."' AND i.invoice_status = 2 AND i.revision = 0  ");*/
            
            $res['data'] = $this->basic_model->getCustomRow("SELECT * FROM `amc_payment` WHERE amc_payment_id = ".$id); 
            //$res['customer_name'] = $res['invoice'][0]['user_name'];
            $res['customer_data'] = $this->basic_model->getCustomRow("SELECT * FROM `user` WHERE user_id = ".$res['data']['payment_customer_id']);
            $setval = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid); 
            $res["setval"] = $setval;
            $res['data']['amount_in_words'] = numberTowords(number_format((float)@$res['data']['payment_amount'], 2, '.', ''));
            //print_b($this->convertNumberToWord(56.6));

            // $res['invoice_detail'] = $this->basic_model->getCustomRows("SELECT * FROM invoice_detail id JOIN product p ON p.product_id  = p.product_id");
            // $res['payment'] = $this->basic_model->getCustomRows("SELECT * FROM payment");

            //$this->basic_model->make_pdf($header_check,$res,'Report','receivable_Report');
            $this->basic_model->make_pdf($header_check,$res,'Report','receivable_Report',0,1);

            // $this->basic_model->make_pdf_view($header_check,$res,'Report','receivable_Report');
            
           
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
        }
    }

    function mail()
    {
        extract($_POST);
        // print_b($_POST);
        if(isset($id) && $id != null) {

            //$setting = $this->basic_model->getRow("setting","setting_id",1,$this->orderBy,"setting_id");
            $quotation = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
            //$customer_data = $this->basic_model->getRow("user","user_id",$quotation['customer_id'],$this->orderBy,"user_id");
            //$from = $setting['setting_company_email'];
            //$to = $customer_data['user_email'];
            //$body = $email_body;
            //$subject = $email_subject;
            predefine_send_email($_POST);
            
            //$this->basic_model->sendEmail($from, $to, $body, $subject, $from_name = NULL);
            $data = array();
            if($quotation['quotation_email_printed_status'] == 0)
            {
                $data['quotation_email_printed_status'] = 1;
                $data['amc_quotation_revised_no'] = $quotation['amc_quotation_revised_no'] + 1;
                
            }
            else if($quotation['quotation_email_printed_status'] > 0)
            {
                 $data['amc_quotation_revised_no'] = $quotation['amc_quotation_revised_no'] + 1;
            }
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
            echo json_encode(array( "success" => "Mail Send successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
        }
        else
        {
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
        }
    }

    function invoice_payment(){
        extract($_POST);
         if(isset($id) && $id != ''){
            if($is_invoice == 1){
              $p_no = 0;
              //i.invoice_status = 1 AND
              $invoice = $this->basic_model->getCustomRow("SELECT i.*,u.user_name,u.user_company_name,u.currency_id FROM invoice i JOIN user u ON u.user_id = i.customer_id  WHERE  i.revision = 0 AND i.invoice_id=".$id);
              if($invoice['invoice_proforma_no'] != 0 && $invoice['invoice_proforma_no'] != null && $invoice['invoice_proforma_no'] != ''){
                $quotation = $this->basic_model->getCustomRow("SELECT q.*, u.* FROM quotation q JOIN user u ON u.user_id = q.customer_id  WHERE  q.quotation_status = 4 AND q.quotation_revised_id = 0 AND q.quotation_no=".$invoice['invoice_proforma_no']);
                $p_no = $quotation['quotation_id'];
              }
              $payment1 = $this->basic_model->getCustomRows("SELECT * FROM payment WHERE ((invoice_id = '".$id."' AND is_invoice = 1)  || (invoice_id = '".$p_no."' AND is_invoice = 0) ) AND (payment_revised_id = 0 || payment_revised_id IS NULL)");
              $total_paid_amount = 0;
              $total_invoiced_amount = 0;
              $total_unpaid_amount = 0; 
              $total_opening_balance = 0;
              $total_recv_amount = 0;
              //invoice additional data
              $setval = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid); 
              $terms = $this->basic_model->getCustomRows("SELECT * FROM `invoice_payment_term` where invoice_id=".$id." order By invoice_payment_id asc"); 
                    $payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE invoice_id = '".$id."'  AND (payment_revised_id = 0 || payment_revised_id IS NULL)");
                    $tot = (!empty($payment['total_payment_amount']))? $payment['total_payment_amount']+$payment['total_payment_adjustment'] : 0;
                    $inv_price = $this->invoice_price($invoice);
                       // if($tot < $inv_price['total_amount_base']){
                        $rev = ($invoice['invoice_revised_no'] > 0)?'-R'.$invoice['invoice_revised_no'] : '';
                        $data = [];
                        $data['total_amount_base'] = $inv_price['total_amount_base'];
                        $data['paid_amount_base']  = $tot;
                        $data['amount_left_base']  = $inv_price['total_amount_base'] - $tot;
                        $data['invoice_date']      = $invoice['invoice_date'];
                        $data['invoice_no']        =  @$setval["company_prefix"].@$setval["invoice_prfx"].$invoice['invoice_no'].$rev;
                        $data['user_company_name'] = $invoice['user_company_name'];
                        $data['invoice_id']        = $invoice['invoice_id'];
                        $data['is_invoice']        = 1;
                        $data['expiry_date_by_pyment_term'] = $this->find_expiry_by_payment_term($terms, $invoice['invoice_date'], $inv_price['total_amount_base']);                      
                        $total_paid_amount = $total_paid_amount + $tot;
                        $total_recv_amount = $total_recv_amount + ($inv_price['total_amount_base'] - $tot);

                        $total_invoiced_amount = $total_invoiced_amount + $inv_price['total_amount_base'];
                        //$total_unpaid_amount = $total_unpaid_amount + ($inv_price['total_amount_base'] - $tot);
                        $find_unpaid = $this->find_amount_by_payment_term($terms, $invoice['invoice_date'], $inv_price['total_amount_base']);
                        $total_unpaid = $find_unpaid - $tot;
                        $data['expiry_amount_by_pyment_term'] = (floatval($total_unpaid) > 0)? floatval($total_unpaid) : 0;
                        //array_push($payment,$data);
                        $payment_data['payment'] = $payment1;
                        $payment_data['data'] = $data;
                        if($total_unpaid > 0){
                            $total_unpaid_amount = $total_unpaid_amount + $total_unpaid;
                        }

                        //count opening balance
                        /*$payment1 = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE invoice_id = '".$invoice['invoice_id']."'  AND (payment_revised_id = 0 || payment_revised_id IS NULL)".$payment_date_range1);
                        $tot1 = (!empty($payment1['total_payment_amount']))? $payment1['total_payment_amount']+$payment1['total_payment_adjustment'] : 0;
                        $total_opening_balance = $total_opening_balance+$tot1;*/
                    //}


              echo json_encode($payment_data);

            }else{
                echo json_encode([]);
            }

         }
    }
   
    function quotation_payment(){
        extract($_POST);
         if(isset($id) && $id != ''){
              $p_no = 0;
              $inv_no = 0;
              $invoice_found = 0;
              //i.invoice_status = 1 AND
              $quotation = $this->basic_model->getCustomRow("SELECT q.*, u.* FROM amc_quotation q JOIN user u ON u.user_id = q.customer_id  WHERE q.amc_quotation_revised_id = 0 AND q.amc_quotation_id=".$id);
              $p_no = $quotation['amc_quotation_id'];
              
              $payment1 = $this->basic_model->getCustomRows("SELECT * FROM amc_payment WHERE invoice_id = '".$p_no."' AND (payment_revised_id = 0 || payment_revised_id IS NULL)");
              $total_paid_amount = 0;
              $total_invoiced_amount = 0;
              $total_unpaid_amount = 0; 
              $total_opening_balance = 0;
              $total_recv_amount = 0;
              //invoice additional data
              $setval = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid); 

                /*$terms = $this->basic_model->getCustomRows("SELECT * FROM `quotation_payment_term` where quotation_id=".$id." order By quotation_payment_id asc"); */
                    $payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM amc_payment WHERE invoice_id = '".$id."'  AND (payment_revised_id = 0 || payment_revised_id IS NULL)");
                    $tot = (!empty($payment['total_payment_amount']))? $payment['total_payment_amount']+$payment['total_payment_adjustment'] : 0;
                    $inv_price = $this->quotation_price($quotation);
                        $rev = ($quotation['amc_quotation_revised_no'] > 0)?'-R'.$quotation['amc_quotation_revised_no'] : '';
                        $data = [];
                        $data['total_amount_base'] = $inv_price['total_amount_base'];
                        $data['paid_amount_base']  = $tot;
                        $data['amount_left_base']  = $inv_price['total_amount_base'] - $tot;
                        $data['invoice_date']      = $quotation['amc_quotation_date'];
                        $data['invoice_no']        =  @$setval["company_prefix"].@$setval["quotation_prfx"].$quotation['amc_quotation_no'].$rev;
                        $data['user_company_name'] = $quotation['user_company_name'];
                        $data['invoice_id']        = $quotation['amc_quotation_id'];
                        $data['is_invoice']        = 0;
                        $data['expiry_date_by_pyment_term'] = $quotation['amc_quotation_expiry_date'];                      
                        $total_paid_amount = $total_paid_amount + $tot;
                        $total_recv_amount = $total_recv_amount + ($inv_price['total_amount_base'] - $tot);

                        $total_invoiced_amount = $total_invoiced_amount + $inv_price['total_amount_base'];
                        $total_unpaid = $inv_price['total_amount_base'] - $tot;
                        $data['expiry_amount_by_pyment_term'] = (floatval($total_unpaid) > 0)? floatval($total_unpaid) : 0;
                        //array_push($payment,$data);
                        $payment_data['payment'] = $payment1;
                        $payment_data['data'] = $data;
                        if($total_unpaid > 0){
                            $total_unpaid_amount = $total_unpaid_amount + $total_unpaid;
                        }



              echo json_encode($payment_data);

            }

    }
   
        
} 