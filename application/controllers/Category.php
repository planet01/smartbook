<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class category extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'category/add';
        $this->view_product = 'category/view';
        $this->edit_product = 'category/edit';
        $this->delete_product = 'category/delete';
        $this->detail_product = 'category/detail';
        // page active
        $this->add_page_product = "add-category";
        $this->edit_page_product = "edit-category";
        $this->view_page_product = "view-category";
        // table
        $this->table = "category";
        $this->table_pid = "category_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/category";
        // Page Heading
        $this->page_heading = "Category";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index(){
        redirect($this->add_product);
    }
    function add(){
        extract($_POST);
        if( $this->input->post()){ 
            if(empty($id)){ 
                if( empty($category_name)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    
                     
                    $category_data = $this->basic_model->getRow($this->table,"category_name",$category_name,$this->orderBy,$this->table_pid);
                    if(@$category_data != null && !empty(@$category_data)){
                        echo json_encode(array("error" => "Category Name already exist"));
                        exit();
                    } 
					
                    $data = array(
                        "category_name"=> $category_name,
                        "category_is_active" => $category_is_active,
                        //"category_type" => $category_type
                    );

                    $id = $this->basic_model->insertRecord($data,$this->table); 
 
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
				
			
                if(empty($category_name)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    
                    $category_data = $this->basic_model->getRow($this->table,"category_name",$category_name,$this->orderBy,$this->table_pid);
                    if(@$category_data != null && !empty(@$category_data)){
                        if ($category_data['category_name'] != $category_name_old) {
                            echo json_encode(array("error" => "Category Name already exist"));
                            exit();
                        }
                    }
                    $data = array(
                        "category_name"=>$category_name,
                        "category_is_active" => $category_is_active, 
                        //"category_type" => $category_type
                    );
                    
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{
            $categor_add = false;
            if ($this->user_type == 2) {
                foreach ($this->user_role as $k => $v) {
                if($v['module_id'] == 1)
                {
                    if($v['add'] == 1)
                    {
                        $categor_add = true;
                    }
                }
                }
            }else
            {
                $categor_add = true;
            }

            if ($categor_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                /*$res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country"); */
                $res['categoryTypes'] = $this->basic_model->getCustomRows("SELECT * FROM `category_type`");
                $this->template_view($this->add_page_product, $res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function edit($id){
        $categor_edit = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
               if($v['module_id'] == 1)
               {
                  if($v['edit'] == 1)
                  {
                    $categor_edit = true;
                  }
               }
            }
        }else
        {
            $categor_edit = true;
        }
        if($categor_edit)
        {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){

                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res['image_upload_dir'] = $this->image_upload_dir;
                    /*$res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");*/
                    $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    $res['categoryTypes'] = $this->basic_model->getCustomRows("SELECT * FROM `category_type`"); 

                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        }else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
		
    }

    function view() {
        $categor_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
               if($v['module_id'] == 1)
               {
                  if($v['visible'] == 1)
                  {
                     $categor_view = true;
                  }
               }
            }
        }else
        {
            $categor_view = true;
        }

        if($categor_view)
        {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["edit_product"] = $this->edit_product;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product;
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res['data'] = $this->basic_model->getCustomRows("Select * From ".$this->table);
            /*if(!empty($res['data'][0]['category_type'])){
                $res['categoryType'] = $this->basic_model->getCustomRows("SELECT title FROM `category_type` where id=".@$res['data'][0]['category_type']); 
            }else{
                $res['categoryType'] = "";
            }*/
            $this->template_view($this->view_page_product,$res);
        }else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
	 
    function delete($id){
        if( isset($id) && $id != ''){
            $del = $this->basic_model->deleteRecord($this->table_pid, $id, $this->table);
    		echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    	
    }

   
    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if($res){
                //$type = $this->basic_model->getCustomRows("SELECT title FROM `category_type` where id=".$res['category_type']);
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = "";
                $outputDescription .= "<p><span style='font-weight:bold;'>Category Name : </span> ".$res['category_name']."</p>";
               /* $outputDescription .= "<p>Category Type : ".@$type[0]['title']."</p>";*/
                $outputDescription .= "<p><span style='font-weight:bold;'>Status : </span> ".(($res['category_is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";
              
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
   
   function category_type(){
     echo json_encode(array( "data" => $this->basic_model->getCustomRows("SELECT category_id  FROM `category` where category_id=".$_POST['id']))); 
   }
} 