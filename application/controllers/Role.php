<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'role/add';
        $this->view_product = 'role/view';
        $this->edit_product = 'role/edit';
        $this->delete_product = 'role/delete';
        $this->detail_product = 'role/detail';
        // page active
        $this->add_page_product = "add-role";
        $this->edit_page_product = "edit-role";
        $this->view_page_product = "view-role";
        // table
        $this->table = "role";
        $this->table_pid = "role_id";
        $this->orderBy = "DESC";
        $this->userType = 2;
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // FileUploader
        require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "Role";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in || $this->user_type != 1) {
            redirect('user');
        } 
    }
    function index(){
        redirect($this->add_product);
    }
    function add(){
        extract($_POST);
        if( $this->input->post()){
            if(empty($id)){
                if( empty($role_name) ||  empty($role_description) ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $Checkname = $this->basic_model->getRow($this->table, 'role_name', $role_name,$this->orderBy,$this->table_pid);
                    if(@$$Checkname != null && !empty(@$$Checkname)){
                        echo json_encode(array("error" => "Role Name already Exist"));
                        exit();
                    }

                    $data = array(
                        "role_name"=> $role_name,
                        "role_description" => $role_description,
                    );

                    $id = $this->basic_model->insertRecord($data,$this->table);
                    
                    if(isset($department))
                    {
                        foreach (@$department as $v) {
                            $assign_role_dashboard = array(
                                "department_id"=> $v,
                                "role_id" => $id,
                            );
    
                            $this->basic_model->insertRecord($assign_role_dashboard, 'assign_role_dashboard');
                        }
                    }

                    if(isset($module))
                    {
                        
                        for($i= 0; $i< sizeof($module); $i++) {
                            $assign_role = array(
                                "module_id"=> $module[$i],
                                "role_id" => $id,
                                "visible" => (isset($visible[$i]) && $visible[$i] == 1)? 1 : 0,
                                "add" => (isset($add[$i]) && $add[$i] == 1)? 1 : 0,
                                "edit" => (isset($edit[$i]) && $edit[$i] == 1)? 1 : 0,
                                "delete" => (isset($delete[$i]) && $delete[$i] == 1)? 1 : 0,
                                "print" => (isset($print[$i]) && $print[$i] == 1)? 1 : 0,
                                "status" => (isset($status[$i]) && $status[$i] == 1)? 1 : 0,
                                "payment" => (isset($payment[$i]) && $payment[$i] == 1)? 1 : 0,
                                "history" => (isset($history[$i]) && $history[$i] == 1)? 1 : 0,
                                "clone" => (isset($clone[$i]) && $clone[$i] == 1)? 1 : 0,
                            );
    
                            $this->basic_model->insertRecord($assign_role, 'assign_role');
                        }
                    }

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
                if( empty($role_name) ||  empty($role_description) ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }else{
                    $Checkname = $this->basic_model->getRow($this->table, 'role_name', $role_name,$this->orderBy,$this->table_pid);
                    if(@$Checkname != null && !empty(@$Checkname)){
                        if ($Checkname['role_name'] != $role_name_old) {
                            echo json_encode(array("error" => "Role Name already Exist"));
                            exit();
                        }
                    }

                    

                    $data = array(
                        "role_name"=> $role_name,
                        "role_description" => $role_description,
                    );

                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);

                    $this->basic_model->customQueryDel("DELETE FROM `assign_role` where role_id = ".$id);
                    $this->basic_model->customQueryDel("DELETE FROM `assign_role_dashboard` where role_id = ".$id);

                    if(isset($department))
                    {
                        foreach (@$department as $v) {
                            $assign_role_dashboard = array(
                                "department_id"=> $v,
                                "role_id" => $id,
                            );
    
                            $this->basic_model->insertRecord($assign_role_dashboard, 'assign_role_dashboard');
                        }
                    }

                    if(isset($module))
                    {
                        
                        for($i= 0; $i< sizeof($module); $i++) {
                            $assign_role = array(
                                "module_id"=> $module[$i],
                                "role_id" => $id,
                                "visible" => (isset($visible[$i]) && $visible[$i] == 1)? 1 : 0,
                                "add" => (isset($add[$i]) && $add[$i] == 1)? 1 : 0,
                                "edit" => (isset($edit[$i]) && $edit[$i] == 1)? 1 : 0,
                                "delete" => (isset($delete[$i]) && $delete[$i] == 1)? 1 : 0,
                                "print" => (isset($print[$i]) && $print[$i] == 1)? 1 : 0,
                                "status" => (isset($status[$i]) && $status[$i] == 1)? 1 : 0,
                                "payment" => (isset($payment[$i]) && $payment[$i] == 1)? 1 : 0,
                                "history" => (isset($history[$i]) && $history[$i] == 1)? 1 : 0,
                                "clone" => (isset($clone[$i]) && $clone[$i] == 1)? 1 : 0,
                            );
    
                            $this->basic_model->insertRecord($assign_role, 'assign_role');
                        }
                    }
                    
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{
            $res['title'] = "Add New ".$this->page_heading;
            $res["page_title"] = "add";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->add_page_product;
            $res["add_product"] = $this->add_product;
            $res['edit'] = false;
            $res['departmentData'] = $this->basic_model->getCustomRows("SELECT * FROM `department` ORDER BY `department_id` ASC");
            $res['moduleData'] = $this->basic_model->getCustomRows("SELECT * FROM `module` ORDER BY `module_id` ASC");
            $this->template_view($this->add_page_product,$res);
        }
    }

    function edit($id){
        if( isset($id) && $id != ''){
            $res_edit = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." WHERE role_id = ".$id);
            if($res_edit){
                $res["title"] = "Edit ".$this->page_heading;
                $res["page_title"] =  "";
                $res["page_heading"] = $this->page_heading;
                $res["active"] = $this->edit_page_product;
                $res["add_product"] = $this->add_product;
                $res['image_upload_dir'] = $this->image_upload_dir;
                $res['departmentData'] = $this->basic_model->getCustomRows("SELECT * FROM `department` ORDER BY `department_id` ASC");
                $res['moduleData'] = $this->basic_model->getCustomRows("SELECT * FROM `module` ORDER BY `module_id` ASC");
                $res['edit'] = true;
                $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                $res['dashboard_detail'] = $this->basic_model->getCustomRows("SELECT * FROM  assign_role_dashboard ard  JOIN department d ON d.department_id = ard.department_id WHERE ard.role_id = '".$id."' ");
                $res['detail'] = $this->basic_model->getCustomRows("SELECT * FROM  assign_role ar  JOIN module m ON m.module_id = ar.module_id WHERE ar.role_id = '".$id."' ");
                
                // print_b($res['data']);die();
                $this->template_view($this->add_page_product,$res);
            }else{
                redirect($this->no_results_found);    
            }
        }else{
            redirect($this->no_results_found);
        }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table."");

        // print_b($res['data']);

        $this->template_view($this->view_page_product,$res);
    }
    
    function detail($id){
        if( isset($id) && $id != ''){
             $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
             $res_detail = $this->basic_model->getCustomRows("SELECT * FROM  assign_role ar  JOIN module m ON m.module_id = ar.module_id WHERE ar.role_id = '".$res['role_id']."' ");
             $res_detail_2 = $this->basic_model->getCustomRows("SELECT * FROM  assign_role_dashboard ard  JOIN department d ON d.department_id = ard.department_id WHERE ard.role_id = '".$res['role_id']."' ");
             
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = "";
                
                $outputDescription .= "<p><span style='font-weight:bold;'>Role Name : </span>".$res['role_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Role Description : </span>".$res['role_description']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Dashboard : </span> </p>";
                foreach ($res_detail_2 as $k => $v) 
                {
                    $outputDescription .= "<p>".$v['department_title'] ."</p>";
                }
                $count = 0;
                $outputDescription .= "<p><span style='font-weight:bold;'>Roles : </span> </p>";
                foreach ($res_detail as $k => $v) {
                    $count ++;
                    $outputDescription .= "<p style='text-decoration:underline'><b>".$count.". Module : ".$v['module_name']."</b></p>";
                    $outputDescription .= "<p>Visible : ".(($v['visible'] == '1')?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Add: ".(($v['add'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Edit: ".(($v['edit'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Delete: ".(($v['delete'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Print: ".(($v['print'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Status: ".(($v['status'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Payment: ".(($v['payment'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>History: ".(($v['history'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Clone: ".(($v['clone'] == 1)?"Yes":"No")."</p>";
                }
                 

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
}