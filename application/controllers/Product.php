<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class product extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
		
        // page action
        $this->add_product = 'product/add';
        $this->view_product = '/product/view';
        $this->edit_product = 'product/edit';
        $this->delete_product = 'product/delete';
        $this->detail_product = 'product/detail';
        // page active
        $this->add_page_product = "add-product";
        $this->edit_page_product = "edit-product";
        $this->view_page_product = "view-product";
        // table
        $this->table = "product";
        $this->table_pid = "product_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/product";
		// FileUploader
		require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "Product";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
	
    function index(){
        redirect($this->add_product);
    }
    function add(){
        extract($_POST);

		// print_b($_POST);
	   
        if( $this->input->post()){
            if(empty($id)){
                if( empty($product_name) || empty($product_sku) || @$category_id =='' || @$product_description == ''){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    if(strpos(@$product_description, "&#39;") !== FALSE){
                        echo json_encode(array("error" => "This <’> charcter is not allowed in description field."));
                    }
					if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
						
                        $data = $FileUploader->upload();

                        $image_path = $data['files'][0]['name'];
                    }

                    $pro_data = array(
                        "product_name"=>$product_name,
                        "product_description"=>$product_description,
                        "product_sku"=>$product_sku,
                        "category_id" => $category_id,
                        "product_is_active" => $product_is_active,
						//"product_image" => @$image_path,
                        "chargeable"=> @$chargeable,
                        "can_deliver"=> $can_deliver,
                        "stock_item"=> @$stock_item,
                        "can_invoice"=> $can_invoice,
                        "warranty_coverage"=> $warranty_coverage,
                        "warranty_days"=> @$warranty_days,
                        "min_quantity"=> @$min_quantity,
                        "product_note"=>@$product_note,
                        "print_height" =>@$print_height,
                        "print_width" => @$print_width,
                        'category_type' => @$category_type
                    );
                    $product_id = $this->basic_model->insertRecord($pro_data,$this->table);
                    if(isset($_FILES['fileToUpload']['name'][0]) && isset($data['files'][0]['name'])){
                        for($j = 0; $j<(sizeof($_FILES['fileToUpload']['name']) - 1); $j++){
                            $image_data = [
                                        "image"      => @$data['files'][$j]['name'],
                                        "product_id" => $product_id,
                                    ];

                            $this->basic_model->insertRecord($image_data,'images');
                        }
                    }

                    $plan_id = $this->basic_model->getCustomRows("SELECT * FROM `price_plan`");
                    if(sizeof($plan_id) > 0){
                        for($i = 0; $i<sizeof($plan_id); $i++){
                            if(!empty($plan_id[$i]['price_plan_id'])){
                                $price_data = [
                                    "prod_id"             => $product_id,
                                    "plan_id"             => @$plan_id[$i]['price_plan_id'],
                                    "retail_base_price"   => 0.00,
                                    "retail_usd_price"    => 0.00,
                                    "retail_discount"     => 0.00,
                                    "reseller_base_price" => 0.00,
                                    "reseller_usd_price"  => 0.00,
                                    "reseller_discount"   => 0.00,
                                    "partner_base_price"  => 0.00,
                                    "partner_usd_price"   => 0.00,
                                    "partner_discount"    => 0.00
                                ];

                                $this->basic_model->insertRecord($price_data,'price');
                                
                            }
                        }
                    }

				    /*if (isset($discount_status) && @$discount_status == 1) {
                        $arr = array(
                            'product_id' => $product_id,
                            'product_discount_type' => $product_discount_type,
                            'product_discount_amount' => $product_discount_amount
                        );
                        $product_discount_id = $this->basic_model->insertRecord($arr,
                            'product_discount');                        
                    }*/

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
                if( empty($product_name) || empty($product_sku) || @$category_id =='' || @$product_description == ''){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    if(strpos(@$product_description, "&#39;") !== FALSE){
                        echo json_encode(array("error" => "This <’> charcter is not allowed in description field."));
                        return false;
                    }  
					$data = array(
                        "product_name"=>$product_name,
                        "product_description"=>$product_description,
                        "product_sku"=>$product_sku,
                        "category_id" => $category_id,
                        "product_is_active" => $product_is_active,
                        //"product_image" => @$image_old,
                        "chargeable"=> @$chargeable,
                        "can_deliver"=> $can_deliver,
                        "stock_item"=> @$stock_item,
                        "can_invoice"=> $can_invoice,
                        "warranty_coverage"=> $warranty_coverage,
                        "warranty_days"=> @$warranty_days,
                        "min_quantity"=> @$min_quantity,
                        "product_note"=>@$product_note,
                        "print_height" =>@$print_height,
                        "print_width" => @$print_width,
                        "category_type" => @$category_type
                    );
                    if(isset($plan_id)){
                        for($i = 0; $i<sizeof($plan_id); $i++){
                            if(!empty($plan_id[$i])){
                                $dl = $this->basic_model->customQueryDel('DELETE from price where plan_id='.$plan_id[$i].' AND prod_id='.$id);
                                $price_data = [
                                    "prod_id"             => $id,
                                    "plan_id"             => @$plan_id[$i],
                                    "retail_base_price"   => empty($retail_base_price[$i])  ? 0 : $retail_base_price[$i],
                                    "retail_usd_price"    => empty($retail_usd_price[$i])   ? 0 : $retail_usd_price[$i],
                                    "retail_discount"     => empty($retail_discount[$i])    ? 0 : $retail_discount[$i],
                                    "reseller_base_price" => empty($reseller_base_price[$i])? 0 : $reseller_base_price[$i],
                                    "reseller_usd_price"  => empty($reseller_usd_price[$i]) ? 0 : $reseller_usd_price[$i],
                                    "reseller_discount"   => empty($reseller_discount[$i])  ? 0 : $reseller_discount[$i],
                                    "partner_base_price"  => empty($partner_base_price[$i]) ? 0 : $partner_base_price[$i],
                                    "partner_usd_price"   => empty($partner_usd_price[$i])  ? 0 : $partner_usd_price[$i],
                                    "partner_discount"    => empty($partner_discount[$i])   ? 0 : $partner_discount[$i]
                                ];

                                $this->basic_model->insertRecord($price_data,'price');
                                
                            }
                        }
                        
                    }else if(!isset($plan_id) || $plan_id == ''){
                        $plan_id = $this->basic_model->getCustomRows("SELECT * FROM `price_plan`");
                        if(sizeof($plan_id) > 0){
                            for($i = 0; $i<sizeof($plan_id); $i++){
                                if(!empty($plan_id[$i]['price_plan_id'])){
                                    $price_data = [
                                        "prod_id"             => $id,
                                        "plan_id"             => @$plan_id[$i]['price_plan_id'],
                                        "retail_base_price"   => 0.00,
                                        "retail_usd_price"    => 0.00,
                                        "retail_discount"     => 0.00,
                                        "reseller_base_price" => 0.00,
                                        "reseller_usd_price"  => 0.00,
                                        "reseller_discount"   => 0.00,
                                        "partner_base_price"  => 0.00,
                                        "partner_usd_price"   => 0.00,
                                        "partner_discount"    => 0.00
                                    ];

                                    $this->basic_model->insertRecord($price_data,'price');
                                    
                                }
                            }
                        }

                    }
					if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
						
                        $datamy = $FileUploader->upload();                       
                       
                        //$this->basic_model->customQueryDel('DELETE from images where product_id='.$id);

                        for($j = 0; $j<sizeof($datamy['files']); $j++){
                            $image_data = [
                                        "image"      => @$datamy['files'][$j]['name'],
                                        "product_id" => $id,
                                    ];

                            $this->basic_model->insertRecord($image_data,'images');
                        }
                        //$data['product_image'] = $datamy['files'][0]['name'];
                    }

                					
                    // $del = $this->basic_model->deleteRecord('product_id', $id, 'product_discount');
					
                    /*if (isset($discount_status) && @$discount_status == 1) {
                        $arr = array(
                            'product_id' => $id,
                            'product_discount_type' => $product_discount_type,
                            'product_discount_amount' => $product_discount_amount
                        );
                        $product_discount_id = $this->basic_model->insertRecord($arr,
                            'product_discount');                        
                    }*/
                    
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    //$updateData   = $this->basic_model->updateRecord($price_data, 'price', $this->table_pid, $id);
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{

            $product_add = false;
            if ($this->user_type == 2) {
                foreach ($this->user_role as $k => $v) {
                if($v['module_id'] == 2)
                {
                    if($v['add'] == 1)
                    {
                        $product_add = true;
                    }
                }
                }
            }else
            {
                $product_add = true;
            }

            if ($product_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res['categoryData'] = $this->basic_model->getCustomRows("SELECT * FROM `category` WHERE `category_is_active` = 1 ORDER BY `category_id` DESC");
                $res['currency'] = $this->basic_model->getCustomRows("SELECT * FROM `currency`");
                $res['ckeditor'] = 'yes';
                $res['ckeditor1'] = 'yes';
                $res['categoryTypes'] = $this->basic_model->getCustomRows("SELECT * FROM `category_type`"); 


                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function edit($id)
    {
        $product_edit = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 2) {
                    if ($v['edit'] == 1) {
                        $product_edit = true;
                    }
                }
            }
        } else {
            $product_edit = true;
        }
        if ($product_edit) {
        
            if (isset($id) && $id != '') {
                $res_edit = $this->basic_model->getRow($this->table, $this->table_pid, $id, $this->orderBy, $this->table_pid);
                if ($res_edit) {
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res['image_upload_dir'] = $this->image_upload_dir;
                    $data = $this->basic_model->getCustomRows("Select product.*, price.* from product Left Join price on product.product_id=price.prod_id where product.product_id = ".$id);
                    $res['data'] = $data[0];
                    $res['images'] = $this->basic_model->getCustomRows("Select * from images where product_id=".$id);
                    $res['categoryData'] = $this->basic_model->getCustomRows("SELECT * FROM `category` WHERE `category_is_active` = 1");
                    $res['price_plan'] = $this->basic_model->getCustomRows("SELECT price_plan.*, price.*  FROM `price_plan` Left Join price ON price_plan.price_plan_id = price.plan_id AND price.prod_id = ".$id);
                    $res['currency'] = $this->basic_model->getCustomRows("SELECT * FROM `currency`");
                    $res['ckeditor'] = 'yes';
                    $res['ckeditor1'] = 'yes';
                    $res['categoryTypes'] = $this->basic_model->getCustomRows("SELECT * FROM `category_type`");

                    // $res['productDiscountData'] = $this->basic_model->getCustomRow("SELECT * FROM `product_discount` WHERE `product_id` = '".$res['data']['product_id']."' ORDER BY product_discount_id DESC");

                    // print_b($res['productDiscountData']);
                

                    $this->template_view($this->add_page_product, $res);
                } else {
                    redirect($this->no_results_found);
                }
            } else {
                redirect($this->no_results_found);
            }
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function view() {
        $product_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 2) {
                    if ($v['visible'] == 1) {
                        $product_view = true;
                    }
                }
            }
        } else {
            $product_view = true;
        }
        if ($product_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["edit_product"] = $this->edit_product;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product; 
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." JOIN category ON category.category_id  = product.category_id JOIN category_type ON category_type.id  = product.category_type ORDER BY product_id ".$this->orderBy);
            $this->template_view($this->view_page_product,$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
	
	function imageDelete(){
        extract($_POST);
        // print_b($_POST);
        $file_path = dir_path().$this->image_upload_dir.'/'.@$file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        
        $this->basic_model->customQueryDel('DELETE from images where image="'.@$file.'"');

       /* $arr = array(
            'product_image' => ''
        );
        $updateRecord = $this->basic_model->updateRecord($arr, $this->table, $this->table_pid, $data['image_post_file_id']);*/
    }
	
    function delete($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if(isset($res['product_image']) && @$res['product_image'] !=null){
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['product_image'];
                if (file_exists($file_path)) {
                    @unlink($file_path);
                }
            }
            $del = $this->basic_model->deleteRecord($this->table_pid, $id, $this->table);
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
        
    }
 
    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = "";
                $current_img = '';
                $image = $this->basic_model->getCustomRow("SELECT * FROM `images` WHERE `product_id` = ".$id." ORDER BY image_id"); 
                $file_path = dir_path().$this->image_upload_dir.'/'.@$image['image'];
                if (!is_dir($file_path) && file_exists($file_path)) {
                  $current_img = site_url($this->image_upload_dir.'/'.$image['image']);
                }else{
                  $current_img = site_url("uploads/dummy.jpg");
                }
                // $current_img = $file_path;
                $outputDescription .= "<p><br> <img src='".$current_img."' width='100px' /></p>";
                $res_cate = $this->basic_model->getCustomRow("SELECT * FROM `category` WHERE `category_is_active` = 1 AND `category_id` = ".$res['category_id']); 

                if(isset($res_cate) && @$res_cate != null){ 
                    $outputDescription .= "<p><span style='font-weight:bold;'>Category : </span>".$res_cate['category_name']."</p>"; 
                }
                $outputDescription .= "<p><span style='font-weight:bold;'>Name : </span> ".$res['product_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Description : </span> ".$res['product_description']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Model No : </span> ".$res['product_sku']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Chargeable : </span> ".(($res['chargeable'] == 0)?'<span class="label label-success">Yes</span>':'<span class="label label-danger">No</span>')."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Can Deliver : </span> ".(($res['can_deliver'] == 0)?'<span class="label label-success">Yes</span>':'<span class="label label-danger">No</span>')."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Include in Invoice : </span> ".(($res['can_invoice'] == 0)?'<span class="label label-success">Yes</span>':'<span class="label label-danger">No</span>')."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Warranty Coverage : </span> ".(($res['warranty_coverage'] == 0)?'<span class="label label-success">Yes</span>':'<span class="label label-danger">No</span>')."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Warranty Coverage Days : </span> ".$res['warranty_days']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Product Note : </span> ".$res['product_note']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Min Stock Quantity : </span> ".$res['min_quantity']."</p>";
				
				
				$outputDescription .= "<p><span style='font-weight:bold;'>Status : ".(($res['product_is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";
                 


                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
}