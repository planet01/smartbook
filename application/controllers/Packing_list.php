<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Packing_list extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
       
        $this->save_product = 'Packing_list/add';
        $this->view_product = 'Packing_list/view'; 
        
        // page active
        $this->add_page_product = "add-packing_list";
        $this->view_page_product = "view-packing_list";
        // table
        $this->table = "packing_list";
        $this->table_id = "packing_list_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/invoice";
        // Page Heading
        $this->page_heading = "Packing List";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }

        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_delete = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 25) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['delete'] == 1) {
                        $this->check_delete = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_delete = true;
            $this->check_print = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
        $res['setting'] = $setting;
        
        $res['data'] = $this->basic_model->getCustomRows("SELECT * From ".$this->table." pl JOIN delivery_note dn ON dn.delivery_note_id = pl.packing_list_dn JOIN user on pl.customer_id = user.user_id WHERE pl.flgDeleted = '0' limit 100");
        // print_b($res['data']);
        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE pl.flgDeleted = '0' ";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(pl.packing_list_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(pl.packing_list_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            $where .= " order By pl.packing_list_id desc limit 100";
            $query = "SELECT * From ".$this->table." pl JOIN delivery_note dn ON dn.delivery_note_id = pl.packing_list_dn JOIN user on pl.customer_id = user.user_id  ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);
            
            echo json_encode($res);
        }
    }

    function add(){
       
        extract($_POST);
        if( $this->input->post()){
            if(empty($id))
            {
                if(empty($product_id)  ) {
                        echo json_encode(array("error" => "Please fill all fields"));
                }else{

                    $packing_list_date_year = explode("-",$packing_list_date)[0];
                    $packing_list_date_mnth = explode("-",$packing_list_date)[1];
                    $packing_list_no = serial_no_packing_list($packing_list_date_year,$packing_list_date_mnth);

                    $data= array(
                        'packing_list_date' => $packing_list_date,
                        'packing_list_dn' => $packing_list_dn,
                        'packing_list_no' => $packing_list_no,
                        'customer_id' => $customer_id,
                        'ref_invoice_no' => $ref_invoice_no,
                        'line_gap' => $line_gap
                        
                    );
                    $packing_list = $this->basic_model->insertRecord($data,$this->table);

                    for($i= 0; $i< sizeof($product_id); $i++) {
                        $detail_data = [
                            'packing_list_id' => $packing_list,
                            'product_id' => $product_id[$i],
                            'description' => $description[$i],
                            'delivery_qty' => @$delivery_qty[$i],
                            'carton_qty' => @$carton_qty[$i],
                            'gross_weight' => @$gross_weight[$i],
                            'dimensions' => @$dimensions[$i],
                            'total_gross_weight' => @$total_gross_weight[$i],
                            'line_gap_detail' => $line_gap_detail[$i]
                        ];
                        $this->basic_model->insertRecord($detail_data,'packing_list_detail');
                    }
                    
                    echo json_encode(array( "success" => "Record Inserted successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }else
            {
                if(empty($product_id)  ) {
                    echo json_encode(array("error" => "Please fill all fields"));
                }else{
                    $data= array(
                        'packing_list_date' => $packing_list_date,
                        'packing_list_dn' => $packing_list_dn,
                        'customer_id' => $customer_id,
                        'ref_invoice_no' => $ref_invoice_no,
                        'line_gap' => $line_gap
                        
                    );
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);

                    $this->basic_model->deleteRecord('packing_list_id', $id, 'packing_list_detail');
                    for($i= 0; $i< sizeof($product_id); $i++) {
                        $detail_data = [
                            'packing_list_id' => $id,
                            'product_id' => $product_id[$i],
                            'description' => $description[$i],
                            'delivery_qty' => @$delivery_qty[$i],
                            'carton_qty' => @$carton_qty[$i],
                            'gross_weight' => @$gross_weight[$i],
                            'dimensions' => @$dimensions[$i],
                            'total_gross_weight' => @$total_gross_weight[$i],
                            'line_gap_detail' => $line_gap_detail[$i]
                        ];
                        $this->basic_model->insertRecord($detail_data,'packing_list_detail');
                    }
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }

            }
        }else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->save_product;
                $res["ckeditor"] = "yes";
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $res['setting'] = $setting;
                $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p  WHERE p.product_is_active = 1 ORDER BY p.product_name ASC"); 
                $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1"); 
                $res['delivery_note'] = $this->basic_model->getCustomRows("SELECT * FROM `delivery_note` JOIN user u ON u.user_id = delivery_note.customer_id"); 
                
                $this->template_view($this->add_page_product,$res);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
        }  
    }

    function edit($id){
       
        if ($this->check_edit) {

            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
    
                if($res_edit){
                    $res['data'] = $res_edit;
                    $res['detail_data'] = $this->basic_model->getCustomRows("Select * FROM packing_list_detail pld JOIN product ON pld.product_id = product.product_id WHERE pld.packing_list_id = '".$id."'");
                    $res['title'] = "Edit ".$this->page_heading;
                    $res["page_title"] = "edit";
                    $res["page_heading"] = $this->page_heading;
                    $res['active'] = $this->add_page_product;
                    $res["add_product"] = $this->save_product;
                    $res["ckeditor"] = "yes";
                    $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                    $res['setting'] = $setting;
                    $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p  WHERE p.product_is_active = 1 ORDER BY p.product_name ASC"); 
                    $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1"); 
                    $res['delivery_note'] = $this->basic_model->getCustomRows("SELECT * FROM `delivery_note` JOIN user u ON u.user_id = delivery_note.customer_id"); 
                    $this->template_view($this->add_page_product,$res);  

                }
            }else{
                redirect($this->no_results_found);    
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res = $this->basic_model->getCustomRow("Select * From Packing_list pl JOIN user on pl.customer_id = user.user_id JOIN delivery_note dn ON dn.delivery_note_id = pl.packing_list_dn WHERE pl.packing_list_id = '".$id."'");
             
            if($res){
                $outputTitle = "";
                $outputDescription = "";
                $outputTitle .= $this->page_heading." Detail";

               
                $outputDescription .= "<p><span style='font-weight:bold;'>Date : </span>".date("d-M-Y", strtotime($res["packing_list_date"]))."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Delivery Note No : </span>".@$setting["company_prefix"].@$setting["delivery_prfx"].$res['delivery_note_no']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Customer : </span>".$res['user_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Ref Invoice No : </span>".$res['ref_invoice_no']."</p>";

                $detail_data = $this->basic_model->getCustomRows("SELECT * FROM packing_list_detail pld JOIN product p ON p.product_id = pld.product_id WHERE pld.packing_list_id = '".$id."' ");
                if($detail_data)
                {
                    $outputDescription .= "<h4><b>Products</b></h4>";
                    $x = 0; 
                    foreach ($detail_data as $k => $v) 
                    {
                        $x++;
                        $outputDescription .= "<p>".$x.". <b>".$v['product_name']."</b></p>"; 
                        $outputDescription .= "<p><span style='font-weight:bold;'>Description : </span>".$v['description']."</p>"; 
                        $outputDescription .= "<p><span style='font-weight:bold;'>Delivery Quantity : </span>".$v['delivery_qty']."</p>"; 
                        $outputDescription .= "<p><span style='font-weight:bold;'>Carton Quantity : </span>".$v['carton_qty']."</p>"; 
                        $outputDescription .= "<p><span style='font-weight:bold;'>Gross Weight : </span>".$v['gross_weight']."</p>"; 
                        $outputDescription .= "<p><span style='font-weight:bold;'>Dimension : </span>".$v['dimensions']."</p>"; 
                        $outputDescription .= "<p><span style='font-weight:bold;'>Total Gross : </span>".$v['total_gross_weight']."</p>"; 
                    }
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function print()
    {
        if ($this->check_print) {
            extract($_GET);
            if(isset($id) && $id != null) {
                $res['data'] = $this->basic_model->getCustomRow("Select * From Packing_list pl JOIN user on pl.customer_id = user.user_id JOIN country c ON c.country_id = user.user_country JOIN customer_detail ON customer_detail.client_id = pl.customer_id JOIN delivery_note dn ON dn.delivery_note_id = pl.packing_list_dn WHERE pl.packing_list_id = '".$id."'");
                $res['detail_data'] = $this->basic_model->getCustomRows("SELECT * FROM packing_list_detail pld JOIN product p ON p.product_id = pld.product_id WHERE pld.packing_list_id = '".$id."' ");
                $res['setval'] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                $this->basic_model->make_pdf($header_check,$res,'Report','packing_list_report',0,0,10,0,'',1);
                // $this->template_view('packing_list_report',$res);
            }
            else
            {
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function delete($id){
        if( isset($id) && $id != ''){
            $data = array(
                "flgDeleted" => 1,
            ); 
            $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
            
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }

    function find_delivery_note(){
        extract($_POST);
        if(isset($id) && $id != ''){
            $res['delivery_note'] = $this->basic_model->getCustomRows("SELECT * FROM `delivery_note_detail` dnd JOIN product p ON p.product_id = dnd.product_id WHERE dnd.delivery_note_id = '".$id."'");
            echo json_encode($res);
        }
    }
}