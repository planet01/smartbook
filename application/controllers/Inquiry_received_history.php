<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inquiry_received_history extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
       
        $this->save_product = 'Inquiry_received_history/add';
        $this->view_product = 'Inquiry_received_history/view'; 
        
        // page active
        $this->add_page_product = "add-inquiry_received_history";
        $this->view_page_product = "view-inquiry_received_history";
        // table
        $this->table = "inquiry_received_history";
        $this->table_id = "inquiry_received_history_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/invoice";
        // Page Heading
        $this->page_heading = "Inquiry Received History";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }

        $this->check_view = false;
        $this->check_add = false;
        $this->check_print = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 26) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_print = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
        $res['setting'] = $setting;
        
        $res['data'] = $this->basic_model->getCustomRows("SELECT * From ".$this->table."  JOIN user on inquiry_received_history.inquiry_received_history_sales_person = user.user_id order by inquiry_received_history.inquiry_received_history_id desc LIMIT 100");
        // print_b($res['data']);
        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE inquiry_received_history.inquiry_received_history_id != '0' ";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(inquiry_received_history.inquiry_received_history_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(inquiry_received_history.inquiry_received_history_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            if($company_name != '0')
            {
                $where .= " AND inquiry_received_history.inquiry_received_history_company_name LIKE '%".$company_name."%'";
            }

            $where .= " order By inquiry_received_history.inquiry_received_history_id desc limit 100";
            $query = "SELECT * From ".$this->table."  JOIN user on inquiry_received_history.inquiry_received_history_sales_person = user.user_id ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);
            
            echo json_encode($res);
        }
    }

    function add(){
       
        extract($_POST);
        if( $this->input->post()){
            if(empty($id))
            {
                if(empty($inquiry_received_history_date) || empty($inquiry_received_history_sales_person) ) {
                        echo json_encode(array("error" => "Please fill all fields"));
                }else{
                    $inquiry_date_year = explode("-",$inquiry_received_history_date)[0];
                    $inquiry_no = serial_no_inquiry($inquiry_date_year);
                    $data= array(
                        'inquiry_no' => $inquiry_no,
                        'inquiry_received_history_date' => $inquiry_received_history_date,
                        'inquiry_received_history_company_name' => $inquiry_received_history_company_name,
                        'inquiry_received_history_sales_person' => $inquiry_received_history_sales_person,
                        'inquiry_received_history_contact_person' => @$inquiry_received_history_contact_person,
                        'inquiry_received_history_contact_no' => @$inquiry_received_history_contact_no,
                        'inquiry_received_history_remarks' => @$inquiry_received_history_remarks,
                    );
                    $this->basic_model->insertRecord($data,$this->table);
                    
                    echo json_encode(array( "success" => "Record Inserted successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->save_product;
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $res['setting'] = $setting;
                $res['employee'] = $this->basic_model->getCustomRows("SELECT * FROM user WHERE user_type = 2 ");
                
                $this->template_view($this->add_page_product,$res);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
        }  
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res = $this->basic_model->getCustomRow("SELECT * FROM inquiry_received_history JOIN user on inquiry_received_history.inquiry_received_history_sales_person = user.user_id WHERE inquiry_received_history.inquiry_received_history_id = '".$id."'");
            
            if($res){
                $outputTitle = "";
                $outputDescription = "";
                $outputTitle .= $this->page_heading." Detail";

               
                $outputDescription .= "<p><span style='font-weight:bold;'>Date : </span>".date("d-M-Y", strtotime($res["inquiry_received_history_date"]))."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Inquiry No : </span>".$res['inquiry_no']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Company : </span>".$res['inquiry_received_history_company_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Sales Person : </span>".$res['user_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Contact Person : </span>".$res['inquiry_received_history_contact_person']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Contact No : </span>".$res['inquiry_received_history_contact_no']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Remarks : </span>".$res['inquiry_received_history_remarks']."</p>";
                
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function print_report()
    {
        if ($this->check_print) {
            extract($_POST);
            if(isset($date_from) && isset($date_to)){
                $res['from_date'] = date('d-M-Y', strtotime($date_from));
                $res['to_date'] = date('d-M-Y', strtotime($date_to));

                $date_from = date('Y-m-d', strtotime($date_from));
                $date_to = date('Y-m-d', strtotime($date_to));
               
                $date_range = "  (STR_TO_DATE(`irh`.`inquiry_received_history_date`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."')";
                $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM inquiry_received_history irh JOIN user on irh.inquiry_received_history_sales_person = user.user_id  WHERE  ".$date_range);
                $res['setval'] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                $this->basic_model->make_pdf($header_check,$res,'Report','inquiry_report');
            }
            else
            {
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
}