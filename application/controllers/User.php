<?php

    class user extends MY_Back_Controller{
        public function __CONSTRUCT(){
            parent::__construct();
            $this->add_product = 'user/login';
            $this->view_product = 'dashboard';
            $this->add_page_product = "signin";
            $this->redirect = 'dashboard';
            $this->table = "user";
            $this->no_results_found = "no-result-found";
        }

        public function index(){
            $logged_in = $this->is_login_admin();
            if ($logged_in) {
                redirect($this->redirect,'refresh');
            }
            $res['add_product'] = "Sign In";
            $res['title'] = "Sign In";
            $this->template_signin($this->add_page_product,$res);
        }
        function login(){
           if( $this->input->post() ){
                extract($_POST);
                $pass = $this->basic_model->encode($user_password);
                $res = $this->usermodel->chk_login($this->table,$user_email,$pass,1);
                if( $res ){
                    $sess_data = array("admin_id"=>$res['user_id'],"admin_email"=>$res['user_email'],"admin_password"=>$user_password,"admin_is_logged"=>1,"user_type"=>$res['user_type'],"admin_status"=>1);
                    $this->session->set_userdata($sess_data);
                    echo json_encode(array("success"=>"Logged in","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
                else{
                    echo json_encode(array("error"=>"Invalid username or password"));
                }
           }else{
                redirect($this->redirect,'refresh');
           }
        }

        function logout(){
            $arr = array("admin_id"=>'',"admin_email"=>'',"admin_password"=>'',"admin_is_logged"=>0,"user_type"=>'',"admin_status"=>0);
            $this->session->unset_userdata($arr);
            $this->session->sess_destroy();
            redirect($this->redirect,'refresh');
        }
        function image(){
            $this->image_upload_dir = "uploads/user/";
            $uploads_dir = $this->image_upload_dir;
            $fullname = "1056019962Chrysanthemum.jpg";
            $allowfiles = array('jpeg', 'JPEG','jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF');
            $width = 40;
            $height = 40;
            $res = thumb($fullname, $width, $height,$uploads_dir,$allowfiles);
            echo $res;
        }
        public function no_results_found(){
            $res['add_product'] = "No Results Found";
            $res['title'] = "No Results Found";
            $this->template_view($this->no_results_found,$res);
        }
    }
