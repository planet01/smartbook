<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Follow_Up extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
       
        $this->save_product = 'Follow_Up/add';
        $this->view_product = 'Follow_Up/view'; 
        
        // page active
        $this->add_page_product = "add-follow_up";
        $this->view_page_product = "view-follow_up";
        // table
        $this->table = "follow_up";
        $this->table_id = "follow_up_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/invoice";
        // Page Heading
        $this->page_heading = "Follow Up";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }

        $this->check_view = false;
        $this->check_add = false;
        $this->check_print = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 24) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_print = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
        $res['setting'] = $setting;
        $res['customer'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." GROUP BY follow_up_company_name");
        $res['data'] = $this->basic_model->getCustomRows("SELECT * From ".$this->table." follow JOIN user on follow.follow_up_sales_person = user.user_id order by follow.follow_up_id desc LIMIT 100");
        // print_b($res['data']);
        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE follow.follow_up_id  != 0";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(follow.follow_up_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(follow.follow_up_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            if($customer != '0')
            {
                $where .= " AND follow.follow_up_company_name = '".$customer."'";
            }

            $where .= " order by follow.follow_up_id desc limit 100";
            $query = "SELECT * From ".$this->table." follow JOIN user on follow.follow_up_sales_person = user.user_id ".$where;
            
            $temp = $this->basic_model->getCustomRows($query);
            $res['data'] = array();
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            foreach (@$temp as $v) 
            {
                    $data = [];
                    $data['follow_up_id '] = $v['follow_up_id'];
                    $data['follow_up_date'] = date("Y-m-d", strtotime($v["follow_up_date"]));
                    $data['follow_up_company_name'] = $v['follow_up_company_name']; 
                    if ($v['follow_up_doc_type'] == 1) {
                        $data['follow_up_doc_type'] = 'Quotation';
                    } else if ($v['follow_up_doc_type'] == 2) {
                        $data['follow_up_doc_type'] = 'Perfoma Invoice';
                    } else if ($v['follow_up_doc_type'] == 3) {
                        $data['follow_up_doc_type'] = 'Invoice';
                    } else if ($v['follow_up_doc_type'] == 4) {
                        $data['follow_up_doc_type'] = 'AMC Invoice';
                    } else if ($v['follow_up_doc_type'] == 5) {
                        $data['follow_up_doc_type'] = 'AMC Quotation';
                    }

                    if ($v['follow_up_doc_type'] == 1) {
                        $quotation = getCustomRow("Select * From quotation where quotation_id = '" . $v['follow_up_doc_id'] . "'");
                        $data['follow_up_doc_no'] = $setting['company_prefix'] . $setting['quotation_prfx'] . (($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no']);
                    } else if ($v['follow_up_doc_type'] == 2) {
                        $quotation = getCustomRow("Select * From quotation where quotation_id = '" . $v['follow_up_doc_id'] . "'");
                        $data['follow_up_doc_no'] = $setting['company_prefix'] . $setting['quotation_prfx'] . (($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no']);
                    } else if ($v['follow_up_doc_type'] == 3) {
                        $invoice = getCustomRow("Select * From invoice where invoice_id = '" . $v['follow_up_doc_id'] . "'");
                        $rev = ($invoice['invoice_revised_no'] > 0) ? '-R' . $invoice['invoice_revised_no'] : '';
                        $data['follow_up_doc_no'] = @$setting["company_prefix"] . @$setting["invoice_prfx"].$invoice['invoice_no'] . $rev;
                    } else if ($v['follow_up_doc_type'] == 4) {
                        $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '" . $v['follow_up_doc_id'] . "'");
                        $data['follow_up_doc_no'] = @$setting["company_prefix"].(($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7) ? 'AMCINV-' : @$setting["quotation_prfx"] . (($amc_invoice['amc_quotation_revised_no'] > 0) ? @$amc_invoice['amc_quotation_no'] . '-R' . number_format(@$amc_invoice['amc_quotation_revised_no']) : @$amc_invoice['amc_quotation_no']));
                    } else if ($v['follow_up_doc_type'] == 5) {
                        $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '" . $v['follow_up_doc_id'] . "'");
                        $data['follow_up_doc_no'] = @$setting["company_prefix"]. (($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7) ? 'AMCINV-' : @$setting["quotation_prfx"] . (($amc_invoice['amc_quotation_revised_no'] > 0) ? @$amc_invoice['amc_quotation_no'] . '-R' . number_format(@$amc_invoice['amc_quotation_revised_no']) : @$amc_invoice['amc_quotation_no']));
                    }
                                                
                    $data['follow_up_remarks'] = $v['follow_up_remarks'];
                    array_push($res['data'],$data);
            }
            
            echo json_encode($res);
        }
    }

    function add(){
       
        extract($_POST);
        if( $this->input->post()){
            if(empty($id))
            {
                if(empty($follow_up_date) || empty($follow_up_sales_person) ) {
                        echo json_encode(array("error" => "Please fill all fields"));
                }else{

                    $data= array(
                        'follow_up_doc_type' => $follow_up_doc_type,
                        'follow_up_doc_id' => $follow_up_doc_id,
                        'follow_up_date' => $follow_up_date,
                        'follow_up_company_name' => $follow_up_company_name,
                        'follow_up_sales_person' => $follow_up_sales_person,
                        'follow_up_contact_person' => @$follow_up_contact_person,
                        'follow_up_contact_no' => @$follow_up_contact_no,
                        'follow_up_remarks' => @$follow_up_remarks,
                    );
                    $this->basic_model->insertRecord($data,$this->table);
                    
                    echo json_encode(array( "success" => "Record Inserted successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->save_product;
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $res['setting'] = $setting;
                // $user_id = $this->session->userdata('admin_id');
                // $user_data = $this->basic_model->getCustomRow("SELECT * FROM user where user_id = '".$user_id."'");
                // $res['company'] = $user_data;
                $res['employee'] = $this->basic_model->getCustomRows("SELECT * FROM user WHERE user_type = 2 ");
                
                $this->template_view($this->add_page_product,$res);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
        }  
    }

    function find_invoices(){
        extract($_POST);
            $res['quotation'] = $this->basic_model->getCustomRows("SELECT user.user_name as company,quotation_id as id, quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` INNER JOIN `user`  ON quotation.customer_id = user.user_id WHERE quotation_revised_id = 0 AND quotation_status != '4' order by quotation.quotation_id desc");
            $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT user.user_name as company,quotation_id as id, quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` INNER JOIN `user`  ON quotation.customer_id = user.user_id WHERE quotation_revised_id = 0 AND quotation_status= '4' order by quotation.quotation_id desc");
            $res['invoice'] = $this->basic_model->getCustomRows("SELECT user.user_name as company,invoice_id as id,invoice_no, invoice_revised_no as revised_no FROM `invoice` INNER JOIN `user`  ON invoice.customer_id = user.user_id where revision = 0 ORDER BY invoice.create_date DESC");
            $res['amc_quotation'] = $this->basic_model->getCustomRows("SELECT amc_quotation_id as id, amc_quotation_no as invoice_no,amc_quotation.*,user.user_name as company FROM `amc_quotation`  INNER JOIN `user`  ON amc_quotation.customer_id = user.user_id WHERE amc_quotation.amc_quotation_revised_id = 0 AND amc_quotation_status != '4' order by amc_quotation.amc_quotation_id desc");
            $res['amc_invoice'] = $this->basic_model->getCustomRows("SELECT amc_quotation_id as id,amc_quotation_no as invoice_no,amc_quotation.*,user.user_name as company FROM `amc_quotation`  INNER JOIN `user`  ON amc_quotation.customer_id = user.user_id WHERE amc_quotation.amc_quotation_revised_id = 0 AND amc_quotation_status = '4' order by amc_quotation.amc_quotation_id desc");
            $res['inquiry_history'] = $this->basic_model->getCustomRows("SELECT inquiry_received_history_id  as id,inquiry_no as invoice_no,inquiry_received_history_company_name as company,inquiry_received_history_sales_person as employee,inquiry_received_history.* FROM `inquiry_received_history`  ");
            echo json_encode($res);
        
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res = $this->basic_model->getCustomRow("SELECT * FROM follow_up JOIN user on follow_up.follow_up_sales_person = user.user_id WHERE follow_up.follow_up_id = '".$id."'");
             
            if($res){
                $outputTitle = "";
                $outputDescription = "";
                $outputTitle .= $this->page_heading." Detail";

               
                $outputDescription .= "<p><span style='font-weight:bold;'>Date : </span>".date("d-M-Y", strtotime($res["follow_up_date"]))."</p>";
                if($res['follow_up_doc_type'] == 1)
                {
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc Type : </span> Quotation</p>";
                    $quotation = getCustomRow("Select * From quotation where quotation_id = '".$res['follow_up_doc_id']."'");
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc No : </span>".$setting['company_prefix'].$setting['quotation_prfx'].(($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no'])."</p>";
                    
                }else if($res['follow_up_doc_type'] == 2)
                {
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc Type : </span> Perfoma Invoice</p>";
                    $quotation = getCustomRow("Select * From quotation where quotation_id = '".$res['follow_up_doc_id']."'");
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc No : </span>".$setting['company_prefix'].$setting['quotation_prfx'].(($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no'])."</p>";
                }else if($res['follow_up_doc_type'] == 3)
                {
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc Type : </span>Invoice</p>";
                    $invoice = getCustomRow("Select * From invoice where invoice_id = '".$res['follow_up_doc_id']."'");
                    $rev = ($invoice['invoice_revised_no'] > 0)?'-R'.$invoice['invoice_revised_no'] : '';
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc No : </span>".@$setting["company_prefix"].@$setting["invoice_prfx"].$invoice['invoice_no'].$rev."</p>";
                    
                }else if($res['follow_up_doc_type'] == 4)
                {
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc Type : </span>AMC Invoice</p>";
                    $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '".$res['follow_up_doc_id']."'");
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc No : </span>".@$setting["company_prefix"]; ?><?= ($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7)? 'AMCINV-': @$setting["quotation_prfx"]. (($amc_invoice['amc_quotation_revised_no'] > 0)?@$amc_invoice['amc_quotation_no'].'-R'.number_format(@$amc_invoice['amc_quotation_revised_no']):@$amc_invoice['amc_quotation_no'])."</p>";
                    
                }else if($res['follow_up_doc_type'] == 5)
                {
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc Type : </span>AMC Quotation</p>";
                    $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '".$res['follow_up_doc_id']."'");
                    $outputDescription.= "<p><span style='font-weight:bold;'>Doc No : </span>".@$setting["company_prefix"]; ?><?= ($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7)? 'AMCINV-': @$setting["quotation_prfx"]. (($amc_invoice['amc_quotation_revised_no'] > 0)?@$amc_invoice['amc_quotation_no'].'-R'.number_format(@$amc_invoice['amc_quotation_revised_no']):@$amc_invoice['amc_quotation_no'])."</p>";
                }

                $outputDescription .= "<p><span style='font-weight:bold;'>Company : </span>".$res['follow_up_company_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Sales Person : </span>".$res['user_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Contact Person : </span>".$res['follow_up_contact_person']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Contact No : </span>".$res['follow_up_contact_no']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Remarks : </span>".$res['follow_up_remarks']."</p>";
                

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function print_report()
    {
        if ($this->check_print) {
            extract($_POST);
            
            if(isset($from_date) && isset($to_date)){
                $res['from_date'] = date('F d,Y', strtotime($from_date));
                $res['to_date'] = date('F d,Y', strtotime($to_date));

                $from_date = date('Y-m-d', strtotime($from_date));
                $to_date = date('Y-m-d', strtotime($to_date));
                $date_range = "  (STR_TO_DATE(`fu`.`follow_up_date`, '%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."')  ";
                if($customer != '0')
                {
                    $date_range .= " AND follow_up_company_name = '".$customer."'";
                }
                $date_range .= " order by fu.follow_up_doc_id";
                $query = "SELECT * FROM follow_up fu JOIN user on fu.follow_up_sales_person = user.user_id  WHERE ".$date_range;
                // print_b($query);
                $res['data'] = $this->basic_model->getCustomRows($query);

                $date_range = "  (STR_TO_DATE(`fu`.`follow_up_date`, '%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."') ";
                if(isset($customer) && $customer != '0')
                {
                    $date_range .= " AND follow_up_company_name = '".$customer."'";
                }
                $date_range .= "GROUP BY fu.follow_up_doc_type,fu.follow_up_doc_id";
                $res['unique_data'] = $this->basic_model->getCustomRows("SELECT * FROM follow_up fu JOIN user on fu.follow_up_sales_person = user.user_id  WHERE  ".$date_range);
                // print_b($res['unique_data']);
                $res['setting'] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                $this->basic_model->make_pdf($header_check,$res,'Report','follow_up_report');
                // $this->template_view('follow_up_report',$res);
            }
            else
            {
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
}