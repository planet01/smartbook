<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ticket_management extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->view_product = 'Ticket_management/view';
        $this->add_product = 'Ticket_management/add';
        // page active
        $this->view_page_product = "view-ticket_management";
        $this->add_page_product = "add-ticket_management";
        // table
        $this->table = "tickets";
        $this->table_pid = "ticket_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/warehouse";
        // Page Heading
        $this->page_heading = "Ticket Management";
		
        $this->no_results_found = "user/no_results_found";
        
		require_once APPPATH .'third_party/FileUploader.php';

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 33) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        if ($this->check_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $res['cityData'] = $this->basic_model->getCustomRows("SELECT * FROM city");
            $res['data'] = $this->basic_model->getCustomRows("SELECT *,(SELECT ts.status FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = t.ticket_id) = ts.ticket_status_id AND ts.ticket_id = t.ticket_id) as TicketStatus FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user left JOIN ticket_type tt ON tt.ticket_type_id = t.ticket_type WHERE DATE(t.created_date) = CURDATE()");
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res['setting'] = $setval;
            $res['end_user'] = $this->basic_model->getCustomRows('SELECT *  from end_user');
            $res['statuses'] = ProjectStatusData();
            $res['product'] = $this->basic_model->getCustomRows("SELECT * FROM product ORDER BY product_id");
            $res['ticket_type'] = $this->basic_model->getCustomRows("Select * FROM ticket_type");
            $this->template_view($this->view_page_product,$res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function search_view()
    {
        extract($_GET);
        
            $invoice_type; //1 => ALL
            // $res['setting'] = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            // if($invoice_type == 1) //All
            // {
                $query = "SELECT *,(SELECT ts.status FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = t.ticket_id) = ts.ticket_status_id AND ts.ticket_id = t.ticket_id) as TicketStatus FROM `tickets` t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user left JOIN ticket_type tt ON tt.ticket_type_id = t.ticket_type WHERE t.ticket_id != 0";
                
                if($customer_id != 0)
                {
                    $query .= " AND t.customer_id = ".$customer_id;
                }

                if($end_user != 0)
                {
                    $query .= " AND t.end_user = ".$end_user;
                }
                // if($location != 0)
                // {
                //     $query .= " AND t.location = ".$location;
                // }
                if($from_date != 0)
                {
                    $query .= " AND STR_TO_DATE(t.ticket_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d') ";
                }
                if($to_date != 0)
                {
                    $query .= " AND STR_TO_DATE(t.ticket_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
                }
                if($ticket_type != 00)
                {
                    $query .= " AND t.ticket_type=".$ticket_type;
                }
            // }else if($invoice_type == 0)
            // {
            //     $query = "SELECT *,(SELECT ts.status FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = t.ticket_id) = ts.ticket_status_id AND ts.ticket_id = t.ticket_id) as TicketStatus FROM `tickets` t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user WHERE t.customer_id = ".$customer_id." AND t.status != 4";
            //     if($end_user != 0)
            //     {
            //         $query .= " AND t.end_user = ".$end_user;
            //     }
            //     if($ticket_type != 00)
            //     {
            //         $query .= " AND t.ticket_type=".$ticket_type;
            //     }
            //     // if($location != 0)
            //     // {
            //     //     $query .= " AND t.location = ".$location;
            //     // }
            // }
            $query .= " GROUP BY t.ticket_id ORDER BY t.ticket_id LIMIT 100";
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res['setting'] = $setval;
            $res['data'] = $this->basic_model->getCustomRows($query);
            $res['base_url'] = base_url();
            echo json_encode($res);
        
    }

    function fetch_invoice()
    {
        extract($_POST);
        if($this->input->post())
        {
            $invoice_type; //1 => ALL
            $res['setting'] = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            if($invoice_type == 1) //All
            {
                $query = "SELECT *, CURDATE() as `current_date` FROM `invoice` inv LEFT JOIN quarterly_visit qu ON qu.invoice_id = inv.invoice_id AND qu.quarterly_visit_id  = (SELECT MAX(quarterly_visit_id ) FROM quarterly_visit z WHERE z.invoice_id = qu.invoice_id ) LEFT JOIN end_user ON inv.end_user = end_user.end_user_id LEFT JOIN city ON end_user.end_user_location = city.city_id LEFT JOIN user ON inv.customer_id = user.user_id where inv.customer_id=".$customer_id." group by inv.invoice_id";
            }else if($invoice_type == 0)
            {
                $visit_count = $this->basic_model->getCustomRow("SELECT count(*) FROM `quarterly_visit` qu  where `invoice_id` = '1' ");
                $where = "";
                if($due_since != '')
                {
                    
                    $where = " AND (CASE WHEN (SELECT COUNT(*) FROM quarterly_visit WHERE invoice_id = qu.invoice_id) >= 1 THEN (DATEDIFF(CURDATE(),STR_TO_DATE(qu.service_date,'%Y-%m-%d'))) > ".$due_since." ELSE (DATEDIFF(CURDATE(),STR_TO_DATE(inv.invoice_date,'%Y-%m-%d'))) > ".$due_since." END)";
                    
                }
                
                $query = "SELECT *, CURDATE() as `current_date` FROM `invoice` inv LEFT JOIN quarterly_visit qu ON qu.invoice_id = inv.invoice_id AND qu.quarterly_visit_id = (SELECT MAX(quarterly_visit_id ) FROM quarterly_visit z WHERE z.invoice_id = qu.invoice_id ) LEFT JOIN end_user ON inv.end_user = end_user.end_user_id LEFT JOIN city ON end_user.end_user_location = city.city_id LEFT JOIN user ON inv.customer_id = user.user_id where inv.customer_id=".$customer_id.$where." group by inv.invoice_id;";
                
            }
            
            $res['data'] = $this->basic_model->getCustomRows($query);
            echo json_encode($res);
        }
    }

    function add_invoice_ticket($id)
    {
        if ($this->check_add) {
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res['ticket_invoice_id'] = $id;
            $res['title'] = "Add New ".$this->page_heading;
            $res["page_title"] = "add_invoice_ticket";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->add_page_product;
            $res["add_product"] = $this->add_product;
            $res["setting"] = $setval;
            $res['ckeditor'] = 'yes';
            $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
            $res['project_data'] = $this->basic_model->getCustomRows("SELECT p.project_id,p.project_name,p.doc_type,p.doc_id,p.project_date,p.primary_techinician,p.secondary_techinician,p.status_id,q.customer_id AS quotation_customer_id,inv.invoice_expire_date as inv_expiry_date,inv.end_user, inv.customer_id AS invoice_customer_id FROM `projects` p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id");
            $res['ticket_type'] = $this->basic_model->getCustomRows("Select * FROM ticket_type");
            $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT '' AS end_user,customer_id, quotation_id as ID,'' as inv_expiry_date,quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` WHERE quotation_revised_id = 0 AND quotation_status= '4' order by quotation.quotation_id desc");
            $res['invoice'] = $this->basic_model->getCustomRows("SELECT end_user, customer_id, invoice_id as ID,invoice_no,invoice_expire_date as inv_expiry_date, invoice_revised_no as revised_no FROM `invoice` where revision = 0  ORDER BY invoice.create_date DESC");
            $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category` ");
            $res['customerData'] = $this->basic_model->getCustomRows("SELECT user_name,user_id FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $res['endUserData'] = $this->basic_model->getCustomRows("SELECT * FROM `end_user` ");
            $res['project_task'] = $this->basic_model->getCustomRows("Select * FROM projects_task");
            $this->template_view($this->add_page_product,$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function add(){
        extract($_POST);
        // print_b($_POST);
        $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
        if( $this->input->post()){ 
            if(empty($id)){
                if( empty($ticket_date)){
                    echo json_encode(array("error" => "Please fill all fields Add"));
                }
                else{
                    
                    if($type == 0)
                    {
                        if($project_id == 0)
                        {
                            echo json_encode(array("error" => "Please Select Project"));
                            exit();
                        }
                    }
                    else
                    {
                        if($invoice_id == 0)
                        {
                            echo json_encode(array("error" => "Please Select Invoice."));
                            exit();
                        }
                    }

                    if(empty($task_description))
                    {
                        echo json_encode(array("error" => "Please Add Tasks"));
                        exit();
                    }

                    $ticket_date_year = explode("-",$ticket_date)[0];
                    $t_no = serial_no_ticket($ticket_date_year);

                    $doc_id = 0;
                    $doc_type = 0;
                    if($type == 0) //project
                    {
                        $doc_id = $project_id;
                    } else
                    {
                        $doc_id = $invoice_id;
                        if($invoice_type == 0) //Invoice
                        {
                            $doc_type =1;
                        }else
                        {
                            $doc_type = 2;
                        }
                    }
                    $end_user_data = $this->basic_model->getRow('end_user', 'end_user_id', $end_user_id, 'ASC', 'end_user_id');
                    $data = array(
                        'ticket_no' => $t_no,
                        'ticket_date' => $ticket_date,
                        'ticket_type' => $ticket_type,
                        'type' => $doc_type,
                        'doc_id' => $doc_id,
                        'assigned_to' => $assigned_to,
                        'invoice_no' => $invoice_no,
                        'invoice_id' => $invoice_id,
                        'invoice_expiry_date' => $invoice_expiry_date,
                        'notes' => $notes,
                        'customer_id' => $customer_id,
                        'end_user' => $end_user_id,
                        'location' => @$end_user_data['end_user_location'],                                                                                                                                                             
                        'status' => 1,
                    );
                    $ticket_id = $this->basic_model->insertRecord($data, $this->table);
                    foreach ($task_description as $k => $v) 
                    {
                        $arr = array(
                            'ticket_id' => $ticket_id,
                            'task_description' => $task_description[$k],
                            'no_of_hours' => @$no_of_hours[$k],
                            'type_of_task' => @$type_of_task[$k],
                            'from_time' => @$from_time[$k],
                            'to_time' => @$to_time[$k],             
                        );
                        $this->basic_model->insertRecord($arr, 'ticket_task');
                    }

                    // if($ticket_for_invoice == 1)
                    // {
                        
                            $invoice = array(
                                'last_service_date' => $ticket_date,
                                'ticket_no' => $setval['company_prefix'].$setval['ticket_prfx'].$t_no,
                                'ticket_id' => $ticket_id
                            );
                            
                            $this->basic_model->updateRecord($invoice, 'invoice', 'invoice_id', $invoice_id);
                        
                    // }
				  
                    echo json_encode(array( "success" => "Record added successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
            else{ //UPDATE
                if(empty($ticket_date)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    
                    if(empty($task_description))
                    {
                        echo json_encode(array("error" => "Please Add Tasks"));
                        exit();
                    }
                    
                    $doc_id = 0;
                    $doc_type = 0;
                    if($type == 0) //project
                    {
                        $doc_id = $project_id;
                    } else
                    {
                        if($invoice_type == 0) //Invoice
                        {
                            $doc_type =1;
                            $doc_id = $invoice_id;
                        }else
                        {
                            $doc_type = 2;
                            $doc_id = $invoice_id;
                        }
                    }
                    $end_user_data = $this->basic_model->getRow('end_user', 'end_user_id', $end_user_id, 'ASC', 'end_user_id');
                    

                    $data = array(
                        'ticket_date' => $ticket_date,
                        'type' => $doc_type,
                        'ticket_type' => $ticket_type,
                        'doc_id' => $doc_id,
                        'assigned_to' => $assigned_to,
                        'invoice_no' => $invoice_no,
                        'invoice_id' => $invoice_id,
                        'invoice_expiry_date' => $invoice_expiry_date,
                        'notes' => $notes,
                        'customer_id' => $customer_id,
                        'end_user' => $end_user_id,
                        'location' => $end_user_data['end_user_location'], 
                    );
                    
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    
                    $this->basic_model->customQueryDel('DELETE from ticket_task where '.$this->table_pid.'='.$id);

                    foreach ($task_description as $k => $v) 
                    {
                        $arr = array(
                            'ticket_id' => $id,
                            'task_description' => $task_description[$k],
                            'no_of_hours' => @$no_of_hours[$k],
                            'type_of_task' => @$type_of_task[$k],  
                            'from_time' => @$from_time[$k],
                            'to_time' => @$to_time[$k],           
                        );
                        $this->basic_model->insertRecord($arr, 'ticket_task');
                    }

                    $invoice = array(
                        'last_service_date' => $ticket_date,
                        'ticket_no' => $setval['company_prefix'].$setval['ticket_prfx'].$t_no,
                        'ticket_id' => $id
                    );
                    
                    $this->basic_model->updateRecord($invoice, 'invoice', 'invoice_id', $invoice_id);

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res["setting"] = $setval;
                $res['ckeditor'] = 'yes';
                $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
                $res['project_data'] = $this->basic_model->getCustomRows("SELECT p.project_id,p.project_name,p.doc_type,p.doc_id,p.project_date,p.primary_techinician,p.secondary_techinician,p.status_id,q.customer_id AS quotation_customer_id,inv.invoice_expire_date as inv_expiry_date,inv.end_user, inv.customer_id AS invoice_customer_id FROM `projects` p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id");
                $res['ticket_type'] = $this->basic_model->getCustomRows("Select * FROM ticket_type");
                $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT '' AS end_user,customer_id, quotation_id as ID,'' as inv_expiry_date,quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` WHERE quotation_revised_id = 0 AND quotation_status= '4' order by quotation.quotation_id desc");
                $res['invoice'] = $this->basic_model->getCustomRows("SELECT end_user, customer_id, invoice_id as ID,invoice_no,invoice_expire_date as inv_expiry_date, invoice_revised_no as revised_no FROM `invoice` where revision = 0  ORDER BY invoice.create_date DESC");
                $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category` ");
                $res['customerData'] = $this->basic_model->getCustomRows("SELECT user_name,user_id FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
                $res['endUserData'] = $this->basic_model->getCustomRows("SELECT * FROM `end_user` ");
                $res['project_task'] = $this->basic_model->getCustomRows("Select * FROM projects_task");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function edit($id){
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "Edit";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->add_page_product;
                    $res["add_product"] = $this->add_product;
                    $res['data'] = $this->basic_model->getCustomRow("SELECT * FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user WHERE t.ticket_id =".$id);
                    $res['ticket_task'] = $this->basic_model->getCustomRows("SELECT * FROM `ticket_task` WHERE ticket_id=".$id);
                    $res['ticket_type'] = $this->basic_model->getCustomRows("Select * FROM ticket_type");
                    $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category` ");
                    $res['customerData'] = $this->basic_model->getCustomRows("SELECT user_name,user_id FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
                    $res['endUserData'] = $this->basic_model->getCustomRows("SELECT * FROM `end_user` ");
                    $res['project_task'] = $this->basic_model->getCustomRows("Select * FROM projects_task");
                    $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                    $res["setting"] = $setval;
                    $res['ckeditor'] = 'yes';
                    $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
                    $res['project_data'] = $this->basic_model->getCustomRows("SELECT p.project_id,p.project_name,p.doc_type,p.doc_id,p.project_date,p.primary_techinician,p.secondary_techinician,p.status_id,q.customer_id AS quotation_customer_id,inv.invoice_expire_date as inv_expiry_date,inv.end_user, inv.customer_id AS invoice_customer_id FROM `projects` p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id");
                    $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT '' AS end_user,customer_id, quotation_id as ID,'' as inv_expiry_date,quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` WHERE quotation_revised_id = 0 AND quotation_status= '4' order by quotation.quotation_id desc");
                    $res['invoice'] = $this->basic_model->getCustomRows("SELECT end_user, customer_id, invoice_id as ID,invoice_no,invoice_expire_date as inv_expiry_date, invoice_revised_no as revised_no FROM `invoice` where revision = 0  ORDER BY invoice.create_date DESC");
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function find_invoices()
    {
        $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT '' as inv_expiry_date,customer_id,quotation_id as ID,quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` WHERE quotation_revised_id = 0 AND quotation_status= '4' order by quotation.quotation_id desc");
        $res['invoice'] = $this->basic_model->getCustomRows("SELECT invoice_expire_date as inv_expiry_date,customer_id,end_user, invoice_id as ID,invoice_no, invoice_revised_no as revised_no FROM `invoice` where revision = 0  ORDER BY invoice.create_date DESC");
        $res['project_data'] = $this->basic_model->getCustomRows("SELECT p.project_id,p.project_name,p.doc_type,p.doc_id,p.project_date,p.primary_techinician,p.secondary_techinician,p.status_id,q.customer_id AS quotation_customer_id,inv.invoice_expire_date as inv_expiry_date,inv.end_user, inv.customer_id AS invoice_customer_id FROM `projects` p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id");
        $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
        $res['endUserData'] = $this->basic_model->getCustomRows("SELECT * FROM `end_user` ");
        $res['project_task'] = $this->basic_model->getCustomRows("Select * FROM projects_task");
        $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category`");
        echo json_encode($res);
    }

    function detail($id)
    {
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Detail ".$this->page_heading;
                    $res["page_title"] =  "Detail";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->add_page_product;
                    $res["add_product"] = $this->add_product;
                    $res['data'] = $this->basic_model->getCustomRow("SELECT * FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user WHERE t.ticket_id =".$id);
                    $res['ticket_task'] = $this->basic_model->getCustomRows("SELECT * FROM `ticket_task` WHERE ticket_id=".$id);
                    $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category` ");
                    $res['ticket_type'] = $this->basic_model->getCustomRows("Select * FROM ticket_type");
                    $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                    $res["setting"] = $setval;
                    $res['ckeditor'] = 'yes';
                    $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
                    $res['project_data'] = $this->basic_model->getCustomRows("SELECT p.project_id,p.project_name,p.doc_type,p.doc_id,p.project_date,p.primary_techinician,p.secondary_techinician,p.status_id,q.customer_id AS quotation_customer_id,inv.invoice_expire_date as inv_expiry_date, inv.customer_id AS invoice_customer_id FROM `projects` p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id");
                    $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT quotation_id as ID,quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` WHERE quotation_revised_id = 0 AND quotation_status= '4' order by quotation.quotation_id desc");
                    $res['invoice'] = $this->basic_model->getCustomRows("SELECT invoice_id as ID,invoice_no, invoice_revised_no as revised_no FROM `invoice` where revision = 0  ORDER BY invoice.create_date DESC");
                    $this->template_view('ticket-detail',$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function print_history()
    {
        extract($_GET);
        if ($this->check_print) {
            
            
            $res['setting'] = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            $query = "SELECT *,(SELECT ts.status FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = t.ticket_id) = ts.ticket_status_id AND ts.ticket_id = t.ticket_id) as TicketStatus FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user WHERE (STR_TO_DATE(t.ticket_date,'%Y-%m-%d') between STR_TO_DATE('$from_date','%Y-%m-%d') and STR_TO_DATE('$to_date','%Y-%m-%d')) ";
            if($ticket_type != 00)
            {
                $query .= " AND t.ticket_type=".$ticket_type;
            }
            $res['statusData'] = $this->basic_model->getCustomRows("Select * From ticket_status ts left join product p ON ts.service_id = p.product_id order by ticket_status_id ASC");
            $res['from_date'] = $from_date;
            $res['to_date'] = $to_date;
            $res['statuses'] = ProjectStatusData();
            $res['data'] = $this->basic_model->getCustomRows($query);
            $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
            $this->basic_model->make_pdf(1,$res,'Report','ticket_history_report');
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function status_approval()
    {
        extract($_POST);
        if($ticket_id_status){
            $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$ticket_id_status,$this->orderBy,$this->table_pid);
            if($res_edit['approval_rating'] != null)
            {
                $history = array(
                    'ticket_id' => $ticket_id_status,
                    'approval_rating' => $res_edit['approval_rating'],
                    'status' => $res_edit['status'],
                    'approval_type' => $res_edit['approval_type'],
                    'approval_notes' => $res_edit['approval_notes'],
                );
                
                $this->basic_model->insertRecord($history, 'ticket_approval_history');
            }
            $data = array(
                'approval_rating' => $approval_rating,
                'status' => $approval_status,
                'approval_type' => $approval_type,
                'approval_notes' => $approval_notes,
            );
            
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $ticket_id_status);
            echo json_encode(array("success" => "Record updated successfully", "not_redirect" => 'ticket_approval'));
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be Perform Any Action."));
        }
    }

    function change_status()
    {
        extract($_POST);
        if($ticket_id_status_change){
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res_edit = $this->basic_model->getCustomRow("SELECT * FROM tickets t WHERE t.ticket_id =".$ticket_id_status_change);
            

            $data = array(
                'service_date' => $service_date,
                'ticket_id' => $ticket_id_status_change,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'no_of_hours' => $no_of_hours,
                'service_id' => $service_id,
                'status' => $status,
                'service_detail' => $service_detail,
                'next_follow_up' => $next_follow_up,
            );
            
            $this->basic_model->insertRecord($data, 'ticket_status');

            $tmp = array(
                'status' => 1,
            );
            
            $updateRecord = $this->basic_model->updateRecord($tmp, $this->table, $this->table_pid, $ticket_id_status_change);

            $invoice = array(
                'last_service_date' => $service_date,
                'ticket_no' => $setval['company_prefix'].$setval['ticket_prfx'].$res_edit['ticket_no'],
                'ticket_id' => $res_edit['ticket_id']
            );
            
            $this->basic_model->updateRecord($invoice, 'invoice', 'invoice_id', $res_edit['invoice_id']);

            echo json_encode(array("success" => "Record updated successfully", "not_redirect" => 'ticket_status'));
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be Perform Any Action."));
        }
    }

    function status_history($id)
    {
        if (isset($id) && $id != '') {

            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $history = $this->basic_model->getCustomRows("Select * From ticket_status ts left join product p ON ts.service_id = p.product_id WHERE ts.ticket_id = '" . $id . "'");
            // print_b($history);
            $res_edit = $this->basic_model->getCustomRow("SELECT * FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user WHERE t.ticket_id =".$id);
            $employeeData = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
            if ($history) {
                $outputTitle = "";
                $outputTitle .= "Status History";

                $outputDescription = "";
                $outputDescription .= "<p>Ticket No : " . $setval['company_prefix'].$setval['ticket_prfx'].$res_edit['ticket_no'] . "</p>";
                foreach($employeeData as $k => $v){
                    if($v['user_id'] == @$res_edit['assigned_to'])
                    {
                        $outputDescription .= "<p>Task Assigned To : " . $res_edit['user_name'] . "</p>";
                    }
                }
                
                $count = 0;
                foreach ($history as $k => $v) {
                    $count++;
                    $outputDescription .= "<p><b>" . $count . ".</b> Service Date : " . $v['service_date'] . "</p>";
                    $outputDescription .= "<p>Start Time : " . $v['start_time'] . "</p>";
                    $outputDescription .= "<p>End Time : " . $v['end_time'] . "</p>";
                    $outputDescription .= "<p>No Of Hours : " . $v['no_of_hours'] . "</p>";
                    $outputDescription .= "<p>Service On: " . $v['product_sku'].'-'.$v['product_name'] . "</p>";
                    $outputDescription .= "<p>Status: ";  
                    if($v['status'] == 0)
                    {
                        $outputDescription .= "Pending ";
                    } else if($v['status'] == 1){
                        $outputDescription .= "Partially ";
                    }
                    else if($v['status'] == 2){
                        $outputDescription .= "Completed ";
                    }
                    else if($v['status'] == 3){
                        $outputDescription .= "Pending By Client ";
                    }
                    else if($v['status'] == 4){
                        $outputDescription .= "";
                    }   
                    $outputDescription .= "</p>";
                    $outputDescription .= "<p>Detail : " . $v['service_detail'] . "</p>";
                    $outputDescription .= "<p>Next Follow Up : " . $v['next_follow_up'] . "</p>";
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            } else {
                echo json_encode(array('error' => 'Status History not found.'));
            }
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be deleted."));
        }
    }

    function fetch_ticket()
    {
        extract($_GET);
        
        $res['data'] = $this->basic_model->getCustomRow("SELECT * FROM tickets t WHERE t.ticket_id =".$id);
        $res['statusData'] = $this->basic_model->getCustomRow("SELECT * FROM ticket_status ts WHERE (Select max(t2.ticket_status_id) FROM ticket_status t2 WHERE t2.ticket_status_id > 0 AND ts.ticket_id = '".$id."') = ts.ticket_status_id AND ts.ticket_id =".$id);
        echo json_encode($res);
        
    }

    function get_customer_end_users()
    {
        extract($_GET);
        
        $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM end_user WHERE user_id =".$customer_id);
        echo json_encode($res);
    }

    function approval_history($id)
    {
        if (isset($id) && $id != '') 
        {
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res_edit = $this->basic_model->getCustomRow("SELECT * FROM tickets t LEFT JOIN user u ON t.customer_id = u.user_id LEFT JOIN end_user eu ON eu.end_user_id = t.end_user WHERE t.ticket_id =".$id);
            if ($res_edit) {
                $count = 1;
                $outputTitle = "";
                $outputTitle .= "Approval History";

                $outputDescription = "";
                $outputDescription .= "<p>Ticket No : " . $setval['company_prefix'].$setval['ticket_prfx'].$res_edit['ticket_no'] . "</p>";
                $outputDescription .= "<p>".$count.". Approval Rating : " . $res_edit['approval_rating'] . " Stars</p>";
                $outputDescription .= "<p>Status : Accepted</p>";
                $outputDescription .= "<p>Approval Type : ";
                if($res_edit['approval_type'] == 0)
                {
                    $outputDescription .= "Complain";    
                } else if($res_edit['approval_type'] == 1)
                {
                    $outputDescription .= "Concern";    
                } else if($res_edit['approval_type'] == 2)
                {
                    $outputDescription .= "Appreciation";    
                }
                $outputDescription .= "</p>";
                $outputDescription .= "<p>Approval Note : " . $res_edit['approval_notes'] . "</p>";
                $history = $this->basic_model->getCustomRows("SELECT * FROM ticket_approval_history WHERE ticket_id = ".$id);
                if (isset($history) && @$history != null) {
                    foreach ($history as $k => $v) {
                        $count++;
                        $outputDescription .= "<p>".$count.". Approval Rating : " . $v['approval_rating'] . " Stars</p>";
                        $outputDescription .= "<p>Status : Rejected</p>";
                        $outputDescription .= "<p>Approval Type : ";
                        if($v['approval_type'] == 0)
                        {
                            $outputDescription .= "Complain";    
                        } else if($v['approval_type'] == 1)
                        {
                            $outputDescription .= "Concern";    
                        } else if($v['approval_type'] == 2)
                        {
                            $outputDescription .= "Appreciation";    
                        }
                        $outputDescription .= "</p>";
                        $outputDescription .= "<p>Approval Note : " . $v['approval_notes'] . "</p>";
                    }
                }
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            } else {
            echo json_encode(array('error' => 'Status History not found.'));
        }
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be deleted."));
        }
    }


}