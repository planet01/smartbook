<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Amc_quotation extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
       //$this->add_product = 'quotation/add';
        $this->save_product = 'amc_quotation/add';
        
        $this->view_product = 'amc_quotation/view'; 
        
        $this->edit_product = 'amc_quotation/edit';
        $this->edit_status = 'amc_quotation/change_status';
        $this->print = 'amc_quotation/print';
        $this->mail = 'amc_quotation/mail';
        $this->history = 'amc_quotation/history';
        $this->delete_product = 'amc_quotation/delete';
        $this->detail_product = 'amc_quotation/detail';
        $this->add_customer = 'customer/add';
        // page active
        $this->add_page_product = "add-amc_quotation";
        $this->edit_page_product = "edit-quotation";
        $this->view_page_product = "view-amc_quotation";
        // table
        $this->table = "amc_quotation";
        $this->table_detail = "amc_quotation_detail";
        $this->tablep = "product";
        $this->tables = "setting";
        
        $this->table_id = "amc_quotation_id";
        $this->table_pid = "product_id";
        $this->table_fid = "amc_quotation_id";
        $this->table_sid = "setting_id";
        $this->orderBy = "DESC";
        $this->edit_payment = 'AMC_Payment/edit';
        $this->delete_payment = "AMC_Payment/delete";
        $this->print_payment = 'AMC_Receivables/print';
        // image_upload_dir
        $this->image_upload_dir = "uploads/product";
        // Page Heading
        $this->page_heading = "Annual Maintenance Contract (AMC)";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }

        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 10) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }

    }

    function index(){
        if ($this->check_view) {
            redirect($this->add_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
    
    function add(){
        extract($_POST);
        if( $this->input->post()){
            // print_b($_POST);
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            if(empty($id))
            {
                if(empty($terms) || empty($customer_id) || empty($quotation_no) || empty($quotation_date) ||  empty($quotation_expiry_date) || !isset($terms) || empty($terms) || empty($amc_quotation_cover_letter) || empty($amc_type)) {
                        /*if(!isset($terms) || empty($terms) || !isset($project_terms) || empty($project_terms)){
                            echo json_encode(array("error" => "Please fill project duration & payment term fields"));
                        }*/
                        if(!isset($terms) || empty($terms)){
                            echo json_encode(array("error" => "Please fill payment term fields"));
                        }else if(empty($amc_quotation_cover_letter) || empty($amc_quotation_terms_conditions)){
                            echo json_encode(array("error" => "Please fill AMC quotation cover & terms & condition fields"));
                        }else if(empty($amc_type)){
                            echo json_encode(array("error" => "Please select at least one AMC type")); 
                        }else{
                            echo json_encode(array("error" => "Please fill all fields"));
                        }
                }else{

                    $Checkquotation_no = $this->basic_model->getRow($this->table, 'amc_quotation_no', $quotation_no,$this->orderBy,$this->table_id);
                    if(@$Checkquotation_no != null && !empty(@$Checkquotation_no)){
                        echo json_encode(array("error" => "AMC Quotation No. is Exist"));
                        return;
                    }


                    //validate if any incvoice starting date is > qt expiry date
                    if (isset($amc_quantity) && @$amc_quantity != null && (isset($product_id) && @$product_id != null)) {
                        foreach ($amc_quantity as $k => $v) {
                           if(strtotime(dt_format($amc_start_date[$k])) >=  strtotime(dt_format($quotation_expiry_date))){
                              echo json_encode(array("error" => "There are items for which the start AMC date is greater OR equal to AMC expiry date."));
                              return false;
                           }
                        }
                    }

                    // if (isset($amc_quantity) && @$amc_quantity != null && (isset($product_id) && @$product_id != null)) {
                    //     foreach ($amc_quantity as $k => $v) {
                    //         $q = "SELECT p.*, ( (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = `p`.`product_id` ) - ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND (i.revision = 0 || i.revision IS NUll)  AND `p`.`product_id` = '".$product_id[$k]."' GROUP BY `p`.`product_id`";
                    //         // echo $q;
                    //         $inventory_quantity_data = $this->basic_model->getCustomRow($q);
                    //         if ($inventory_quantity_data['total_inventory_quantity'] <= $amc_quantity[$k]) {
                    //             echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                    //             return;
                    //         }
                    //         // die;
                    //         break;
                    //     }

                    //     // product_check end.
                    // }

                    $quotation_date_year = explode("-",$quotation_date)[0];
                    $quotation_no = serial_no_amc($quotation_date_year);

                    $invoice_no = [];
                    foreach ($quotation as $k => $v) 
                    {
                        $temp = $this->basic_model->getCustomRow("SELECT * FROM `invoice`  where `invoice_id` = '".$v."' ");
                        array_push($invoice_no,$setting["company_prefix"].$setting["invoice_prfx"].$temp['invoice_no']);
                    }

                    $data= array(
                        'customer_id' => $customer_id,
                        'end_user' => @$end_user,
                        'employee_id' => $employee_id,
                        'amc_quotation_no' => $quotation_no,//$quotation_no,
                        'amc_quotation_date' => $quotation_date,
                        'amc_quotation_start_date' => $amc_quotation_start_date,
                        'amc_quotation_expiry_date' => $quotation_expiry_date,
                        'amc_quotation_validity' => $amc_quotation_validity,
                        'amc_quotation_max_visit' => $amc_quotation_max_visit,
                        'amc_quotation_currency' => $quotation_currency,
                        'amc_quotation_cover_letter' => $amc_quotation_cover_letter,
                        "amc_quotation_terms_conditions" => $amc_quotation_terms_conditions,
                        "amc_selected_invoice" => implode(", ",@$quotation),
                        "amc_selected_invoice_no" => implode(", ",$invoice_no),
                        "amc_subtotal" => $grandtotal,
                        "total_rate" => $total_rate,
                        'amc_quotation_report_line_break_invoice' => @$amc_quotation_report_line_break_invoice,
                        'amc_quotation_report_line_break_payment' => @$amc_quotation_report_line_break_payment_invoice,
                        'amc_quotation_report_line_break_bank_detail_invoice' => $amc_quotation_report_line_break_bank_detail_invoice,
                        'amc_quotation_report_line_break' => @$amc_quotation_report_line_break,
                        'amc_quotation_report_line_break_payment' => @$amc_quotation_report_line_break_payment,
                        'amc_quotation_report_line_break_list_item' => @$amc_quotation_report_line_break_list_item,
                        'amc_quotation_report_line_break_annexure' => $amc_quotation_report_line_break_annexure
                        // 'tax_value' => $tax_value,
                        // 'amc_quotation_detail' => $amc_quotation_detail
                        // 'amc_quotation_receivable_status' => $quotation_currency,
                    );

                    //quotation expire date change
                    // foreach ($quotation as $key => $value) {
                    //     $invoice_data = [
                    //         "invoice_expire_date" => $quotation_expiry_date
                    //     ];
                    //     $this->basic_model->updateRecord($invoice_data, "invoice", "invoice_id", $value);
                    // }
                    $amc_type_total_amount = 0;
                    $amc_type_subtotal = 0;
                    $tax_total_value = 0;
                    $quotation_id = $this->basic_model->insertRecord($data,$this->table);
                    if (isset($amc_quantity) && @$amc_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($amc_type_detail as $k => $v) {
                            $this_quotation_total_discount_amount = (!empty($quotation_total_discount_amount[$k])) ? $quotation_total_discount_amount[$k] : 0;
                            $this_subtotal = (!empty($subtotal[$k])) ? $subtotal[$k] : 0;
                            $this_total = (!empty($total[$k])) ? $total[$k] : 0;
                            $arr = array(
                                "amc_quotation_id" => $quotation_id,
                                "amc_type_note" => $amc_type_detail[$k],
                                "amc_type_title" => $amc_type_title[$k],
                                "amc_type_percentage" => $amc_type_percentage[$k],
                                "amc_type_total_discount_type" => $quotation_total_discount_type[$k],
                                "amc_type_total_discount_amount" => $this_quotation_total_discount_amount,
                                "amc_type_discount_detail" => $discount_detail[$k],
                                "amc_type_subtotal" => $this_subtotal,
                                "amc_type_total_amount" => $this_total,
                                'tax_value' => $tax_value[$k],
                            );
                            $this->basic_model->insertRecord($arr,'amc_type');
                            $amc_type_subtotal += $this_subtotal;
                            $amc_type_total_amount += $this_total;
                            $tax_total_value += $tax_value[$k];
                        }
                        foreach ($amc_quantity as $k => $v) {
                            
                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'amc_quotation_id' => $quotation_id,
                                    'selected_quotation_no' => $selected_quotation_no[$k],
                                    'amc_quotation_detail_quantity' => $amc_quantity[$k],
                                    'amc_quotation_detail_rate' => $quotation_detail_rate[$k],
                                    'amc_quotation_detail_rate_usd' => $quotation_detail_rate_usd[$k],
                                    'selected_rate' => $selected_rate[$k],
                                    'amc_quotation_detail_exclude' => @$amc_exclude_quotation[$k],
                                    'amc_quotation_detail_start_date' => $amc_start_date[$k],
                                    'amc_quotation_detail_end_date' => $amc_end_date[$k],
                                    'amc_quotation_detail_amount' => $amc_amount[$k],
                                );
                                $i++;
                                // echo '<pre>';
                                // print_r($arr);
                                $quotation_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            
                        }
                    }
                    for($i= 0; $i< sizeof($terms); $i++) {
                        $payment_data = [
                            'amc_quotation_id' => $quotation_id,
                            'payment_title' => $terms[$i],
                            'percentage' => $percentage[$i],
                            'payment_days' => @$payment_days[$i]
                        ];
                        $this->basic_model->insertRecord($payment_data,'amc_quotation_payment_term');
                    }

                    
                    $customer_name = $this->basic_model->getCustomRow("SELECT * FROM `user` WHERE `user_id` = '".$customer_id."' ")['user_name'];

                    
                    /*for($i= 0; $i< sizeof($project_terms); $i++) {
                        $project_data = [
                            'amc_quotation_id' => $quotation_id,
                            'project_title' => $project_terms[$i],
                            'project_days' => $project_days[$i],
                            'hash_code' => $hash_code[$i],
                            'start_week' => $start_week[$i]
                        ];
                        $this->basic_model->insertRecord($project_data,'amc_quotation_project_term');
                    }*/


                    // $types = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    // for($i= 0; $i< sizeof($features); $i++) {
                    //     $data_features = [
                    //         'amc_quotation_id' => $quotation_id,
                    //         'title' => $features[$i]
                    //     ];
                    //     $warn_id = $this->basic_model->insertRecord($data_features,'amc_quotation_warranty');
                    //     $c = 1;
                    //     foreach($types as $type){
                    //         $data_setting = [
                    //             'warn_id' => $warn_id,
                    //             'amc_quotation_id' => $quotation_id,
                    //             'type_id' => $type['warranty_type_id'],
                    //             'value'   => $_POST['warranty_type_id'.$c][$i],
                    //             'percentage' => $_POST['warranty_percentage'.$c][$i]
                    //         ];
                    //         $c++;
                    //        $this->basic_model->insertRecord($data_setting,'amc_quotation_warranty_setting');
                    //     }
                    // }
                    // echo 1;
                    echo json_encode(array( "success" => "Record Inserted successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }else{
                if(empty($terms) || empty($customer_id) || empty($quotation_no) || empty($quotation_date) ||  empty($quotation_expiry_date) || !isset($terms) || empty($terms) ||   empty($amc_quotation_cover_letter) || empty($amc_type)|| !isset($amc_quantity) ||  @$amc_quantity == null || !isset($product_id) || @$product_id == null) {
                        /*if(!isset($terms) || empty($terms) || !isset($project_terms) || empty($project_terms)){
                            echo json_encode(array("error" => "Please fill project duration & payment term fields"));
                        }*/if(!isset($terms) || empty($terms)){
                            echo json_encode(array("error" => "Please fill payment term fields"));
                        }else if(empty($amc_quotation_cover_letter) || empty($amc_quotation_terms_conditions)){
                            echo json_encode(array("error" => "Please fill AMC quotation cover & terms & condition fields"));
                        }else if(empty($amc_type)){
                            echo json_encode(array("error" => "Please select at least one AMC type")); 
                        }else{
                            echo json_encode(array("error" => "Please fill all fields"));
                        }
                }else{
                   
                // $temp_quotation = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                // $temp_quotation_detail = $this->basic_model->getRows($this->table_detail,$this->table_fid,$id,"amc_quotation_detail_id","DESC");
                
                // $temp_data = array(
                    
                //     'customer_id' => $temp_quotation['customer_id'],
                //     'amc_quotation_no' => $temp_quotation['amc_quotation_no'],
                //     'amc_quotation_date' => $temp_quotation['amc_quotation_date'],
                //     'amc_quotation_expiry_date' => $temp_quotation['amc_quotation_expiry_date'],
                //     'amc_quotation_total_discount_type' => $temp_quotation['amc_quotation_total_discount_type'],
                //     'amc_quotation_total_discount_amount' => $temp_quotation['amc_quotation_total_discount_amount'],
                //     'amc_buypack_discount' => $temp_quotation['amc_buypack_discount'],
                //     'amc_quotation_customer_note' => $temp_quotation['amc_quotation_customer_note'],
                //     'amc_quotation_terms_conditions' => $temp_quotation['amc_quotation_terms_conditions'],
                //     'amc_quotation_currency' => $temp_quotation['amc_quotation_currency'],
                //     'amc_quotation_email_printed_status' => $temp_quotation['amc_quotation_email_printed_status'],
                //     'amc_quotation_type' => $temp_quotation['amc_quotation_type'],
                //     'amc_quotation_revised_id' => $temp_quotation['amc_quotation_id'],
                //     'amc_quotation_status' => $temp_quotation['amc_quotation_status'],
                //     );
                // $quotation_history_id = $this->basic_model->insertRecord($temp_data,$this->table);
                // if($quotation_history_id)
                // {
                //     foreach($temp_quotation_detail as $value) 
                //     {
                //         $arr = array(
                //             'product_id' => $value['product_id'],
                //             'amc_quotation_id' => $quotation_history_id,
                //             'amc_quotation_detail_quantity' => $value['amc_quotation_detail_quantity'],
                //             'amc_quotation_detail_rate' => $value['amc_quotation_detail_rate'],
                //             'amc_quotation_detail_exclude' => @$value['amc_quotation_detail_exclude'],
                //             'amc_quotation_detail_start_date' => $value['amc_quotation_detail_start_date'],
                //             'amc_quotation_detail_end_date' => $value['amc_quotation_detail_end_date'],
                            
                //         );
                //         $this->basic_model->insertRecord($arr,$this->table_detail);        
                //     }
                    
                // }
                //quotation exist ka masla arha ha...!
                    $Checkquotation_no = $this->basic_model->getRow($this->table, 'amc_quotation_no', $quotation_no,$this->orderBy,$this->table_id);
                    if(@$Checkquotation_no != null && !empty(@$Checkquotation_no)){
                        if ($Checkquotation_no['amc_quotation_no'] != $quotation_no_old && $Checkquotation_no['amc_quotation_revised_id'] != '0' && $Checkquotation_no['amc_quotation_id'] != $id) {
                            echo json_encode(array("error" => "AMC Quotation No. is Exist"));
                            return;
                        }
                    }



                    //validate if any incvoice starting date is > qt expiry date
                    if (isset($amc_quantity) && @$amc_quantity != null && (isset($product_id) && @$product_id != null)) {
                        foreach ($amc_quantity as $k => $v) {
                          if(strtotime(dt_format($amc_start_date[$k])) >=  strtotime(dt_format($quotation_expiry_date))){
                              echo json_encode(array("error" => "There are items for which the start AMC date is greater OR equal to AMC expiry date."));
                              return false;
                           }
                        }
                    }



                    // if (isset($amc_quantity) && @$amc_quantity != null && (isset($product_id) && @$product_id != null)) {
                    //     foreach ($amc_quantity as $k => $v) {
                    //         $q = "SELECT p.*, ( (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = `p`.`product_id` ) - ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND (i.revision = 0 || i.revision IS NUll)  AND `p`.`product_id` = '".$product_id[$k]."' GROUP BY `p`.`product_id`";
                    //         // echo $q;
                    //         $inventory_quantity_data = $this->basic_model->getCustomRow($q);
                    //         if ($inventory_quantity_data['total_inventory_quantity'] <= $amc_quantity[$k]) {
                    //             echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                    //             return;
                    //         }
                    //         // die;
                    //         break;
                    //     }

                    //     // product_check end.
                    // }

                    //make history
                    // make_amc_quotation_history($id);
                    $invoice_no = [];
                    foreach ($quotation as $k => $v) 
                    {
                        $temp = $this->basic_model->getCustomRow("SELECT * FROM `invoice`  where `invoice_id` = '".$v."' ");
                        array_push($invoice_no,$setting["company_prefix"].$setting["invoice_prfx"].$temp['invoice_no']);
                    }

                    $data= array(
                        'customer_id' => $customer_id,
                        'end_user' => @$end_user,
                        'employee_id' => $employee_id,
                        'amc_quotation_no' => $quotation_no,
                        'amc_quotation_date' => $quotation_date,
                        'amc_quotation_start_date' => $amc_quotation_start_date,
                        'amc_quotation_expiry_date' => $quotation_expiry_date,
                        'amc_quotation_validity' => $amc_quotation_validity,
                        'amc_quotation_max_visit' => $amc_quotation_max_visit,
                        'amc_quotation_currency' => $quotation_currency,
                        'amc_quotation_cover_letter' => $amc_quotation_cover_letter,
                        "amc_quotation_terms_conditions" => $amc_quotation_terms_conditions,
                        "amc_selected_invoice" => implode(", ",@$quotation),
                        "amc_selected_invoice_no" => implode(", ",$invoice_no),
                        "amc_quotation_status_comment" => '',
                        "amc_subtotal" => $grandtotal,
                        "total_rate" => $total_rate,
                        'amc_quotation_report_line_break_invoice' => @$amc_quotation_report_line_break_invoice,
                        'amc_quotation_report_line_break_payment_invoice' => @$amc_quotation_report_line_break_payment_invoice,
                        'amc_quotation_report_line_break_bank_detail_invoice' => $amc_quotation_report_line_break_bank_detail_invoice,
                        'amc_quotation_report_line_break' => @$amc_quotation_report_line_break,
                        'amc_quotation_report_line_break_payment' => @$amc_quotation_report_line_break_payment,
                        'amc_quotation_report_line_break_list_item' => @$amc_quotation_report_line_break_list_item,
                        'amc_quotation_report_line_break_annexure' => $amc_quotation_report_line_break_annexure
                        

                        //'amc_quotation_status' => 4,
                    );
                    //quotation expire date change
                    // foreach ($quotation as $key => $value) {
                    //     $invoice_data = [
                    //         "invoice_expire_date" => $quotation_expiry_date
                    //     ];
                    //     $this->basic_model->updateRecord($invoice_data, "invoice", "invoice_id", $value);
                    // }
                    if($amc_quotation_email_printed_status > 0){
                        //$quotation_revised_no
                        if($amc_quotation_status > 3 || $amc_quotation_status == 1){
                            //History
                             make_amc_quotation_history($id);    
                            //End History
                            $data['amc_quotation_status'] = 2;
                            $data['amc_quotation_revised_no'] = $amc_quotation_revised_no;

                        }
                    }
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);

                    $del = $this->basic_model->deleteRecord($this->table_fid, $id, $this->table_detail);
                    $this->basic_model->customQueryDel("DELETE FROM `amc_type` where amc_quotation_id=".$id);
                    // print_b($quantity);
                    $amc_type_total_amount = 0;
                    $amc_type_subtotal = 0;
                    $tax_total_value = 0;
                    if (isset($amc_quantity) && @$amc_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($amc_type_detail as $k => $v) {
                            $this_quotation_total_discount_amount = (!empty($quotation_total_discount_amount[$k])) ? $quotation_total_discount_amount[$k] : 0;
                            $this_subtotal = (!empty($subtotal[$k])) ? $subtotal[$k] : 0;
                            $this_total = (!empty($total[$k])) ? $total[$k] : 0;
                            $arr = array(
                                "amc_quotation_id" => $id,
                                "amc_type_note" => $amc_type_detail[$k],
                                "amc_type_title" => $amc_type_title[$k],
                                "amc_type_percentage" => $amc_type_percentage[$k],
                                "amc_type_total_discount_type" => $quotation_total_discount_type[$k],
                                "amc_type_total_discount_amount" => $this_quotation_total_discount_amount,
                                "amc_type_discount_detail" => $discount_detail[$k],
                                "amc_type_subtotal" => $this_subtotal,
                                "amc_type_total_amount" => $this_total,
                                'tax_value' => $tax_value[$k],
                            );
                            $this->basic_model->insertRecord($arr,'amc_type');
                            $amc_type_subtotal += $this_subtotal;
                            $amc_type_total_amount += $this_total;
                            $tax_total_value += $tax_value[$k];
                        }

                        foreach ($product_id as $k => $v) {
                            // if ($quotation_detail_quantity[$k]>0) {
                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'amc_quotation_id' => $id,
                                    'selected_quotation_no'  => $selected_quotation_no[$k],
                                    'amc_quotation_detail_quantity' => $amc_quantity[$k],
                                    'amc_quotation_detail_rate' => $quotation_detail_rate[$k],
                                    'amc_quotation_detail_rate_usd' => $quotation_detail_rate_usd[$k],
                                    'selected_rate' => $selected_rate[$k],
                                    'amc_quotation_detail_exclude' => @$amc_exclude_quotation[$k],
                                    'amc_quotation_detail_start_date' => $amc_start_date[$k],
                                    'amc_quotation_detail_end_date' => $amc_end_date[$k],
                                    'amc_quotation_detail_amount' => $amc_amount[$k],
                                    
                                );
                                $i++;
                                // echo '<pre>';
                                // print_r($arr);
                                $quotation_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            // }
                        }
                    }

                    $this->basic_model->customQueryDel("DELETE FROM `amc_quotation_payment_term` where amc_quotation_id=".$id);
                    for($i= 0; $i< sizeof($terms); $i++) {
                        $payment_data = [
                            'amc_quotation_id' => $id,
                            'payment_title' => $terms[$i],
                            'percentage' => $percentage[$i],
                            'payment_days' => @$payment_days[$i]
                        ];
                        $this->basic_model->insertRecord($payment_data,'amc_quotation_payment_term');
                    }

                    /*$this->basic_model->customQueryDel("DELETE FROM `amc_quotation_project_term` where amc_quotation_id=".$id);
                    for($i= 0; $i< sizeof($project_terms); $i++) {
                        $project_data = [
                            'amc_quotation_id' => $id,
                            'project_title' => $project_terms[$i],
                            'project_days' => $project_days[$i],
                            'hash_code' => $hash_code[$i],
                            'start_week' => $start_week[$i]
                        ];
                        $this->basic_model->insertRecord($project_data,'amc_quotation_project_term');
                    }*/


                    // $types = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    // $this->basic_model->customQueryDel("DELETE FROM `amc_quotation_warranty` where amc_quotation_id=".$id);
                    // $this->basic_model->customQueryDel("DELETE FROM `amc_quotation_warranty_setting` where amc_quotation_id=".$id);
                    // for($i= 0; $i< sizeof($features); $i++) {
                    //     $data_features = [
                    //         'amc_quotation_id' => $id,
                    //         'title' => $features[$i]
                    //     ];
                    //     $warn_id = $this->basic_model->insertRecord($data_features,'amc_quotation_warranty');
                    //     $c = 1;
                    //     foreach($types as $type){
                    //         $data_setting = [
                    //             'warn_id' => $warn_id,
                    //             'amc_quotation_id' => $id,
                    //             'type_id' => $type['warranty_type_id'],
                    //             'value'   => $_POST['warranty_type_id'.$c][$i],
                    //             'percentage' => $_POST['warranty_percentage'.$c][$i]
                    //         ];
                    //         $c++;
                    //        $this->basic_model->insertRecord($data_setting,'amc_quotation_warranty_setting');
                    //     }
                    // }
                    // print_b($arr);
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }else{
            if ($this->check_add) {
                $data['title'] = "Add New ".$this->page_heading;
                $data["page_title"] = "add";
                $data["page_heading"] = $this->page_heading;
                $data['active'] = $this->add_page_product;
                // $data["add_product"] = $this->add_product;
                $data["save_product"] = $this->save_product;
                $data['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $data['base_currency'] = $setting['title'];
                $data['base_currency_id'] = $setting['id'];
                $data['currencyData'] = $this->basic_model->getCustomRows("SELECT * FROM `currency`  "); 
                $data['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN country ON user.user_country = country.country_id LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user.user_type = 3 AND user.`user_is_active` = 1 "); 
                $data['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1"); 
                $data["setval"] = $this->basic_model->getCustomRow("SELECT `setting_id`,`company_prefix`,`setting_company_name`,`setting_company_postal`,`setting_company_city`,`setting_company_country`,`setting_company_reg`,`quotation_prfx`,`invoice_prfx`,`delivery_prfx`,`material_prfx`,`validity_days`,`support_days`,`warranty_years`,`tax` FROM `setting` WHERE setting_id = 1");
                $data['quotation_cover'] = $this->basic_model->getCustomRow("SELECT `quotation_cover` FROM `setting` WHERE setting_id = 1")['quotation_cover'];
                $data["add_customer"] = $this->add_customer;
                $data['quotationData'] = $this->basic_model->getCustomRows("SELECT `quotation_id`, `customer_id`, `employee_id`, `quotation_no`, `quotation_date`, `quotation_expiry_date`, `quotation_total_discount_type`, `quotation_total_discount_amount`, `quotation_buypack_discount`, `quotation_revised_no`, `quotation_freight_type`, `quotation_currency`, `quotation_status`, `quotation_email_printed_status`, `quotation_revised_id`, `quotation_validity`, `quotation_support`, `quotation_warranty`, `quotation_detail`, `revision_id` FROM quotation WHERE quotation.quotation_revised_id = 0 AND quotation.quotation_expiry_date < CURDATE()");
                /*$data['quotationDetailData'] = $this->basic_model->getCustomRows("SELECT `qd`.`quotation_detail_id`, `qd`.`quotation_id`, `qd`.`product_id`, `qd`.`quotation_detail_total_discount_type`, `qd`.`quotation_detail_total_discount_amount`, `qd`.`quotation_detail_quantity`, `qd`.`quotation_detail_rate`, `qd`.`quotation_detail_rate_usd`, `qd`.`quotation_detail_optional`, `qd`.`quotation_detail_existing`, `qd`.`quotation_detail_provided`, `qd`.`quotation_detail_required`, `p`.`product_id`, `p`.`category_id`, `p`.`product_name`, `p`.`product_sku`,  `p`.`product_is_active`, `p`.`chargeable`, `p`.`can_deliver`, `p`.`can_invoice`, `p`.`warranty_coverage`, `p`.`warranty_days`, `p`.`min_quantity`, `p`.`notify`, `p`.`print_height`, `p`.`print_width`, `p`.`create_date`, `p`.`update_date` FROM `quotation_detail` `qd` JOIN `product` p ON `qd`.`product_id` = `p`.`product_id`");*/
                $data['quotationDetailData'] = $this->basic_model->getCustomRows("SELECT `invoice_additional`.*, `inv`.invoice_expire_date,`inv`.invoice_currency_id,
                    `inv_d`.invoice_detail_id, inv_d.invoice_id, inv_d.product_id, inv_d.invoice_detail_pi_quantity, inv_d.invoice_detail_dn_quantity, inv_d.invoice_detail_total_discount_type, inv_d.invoice_detail_total_discount_amount, inv_d.invoice_detail_total_amount, inv_d.invoice_detail_quantity, inv_d.invoice_detail_base_rate, inv_d.invoice_detail_usd_rate, inv_d.invoice_detail_discrepancy, inv_d.revision, inv_d.belongs_to, inv.invoice_no,  
                    `p`.`product_id`, `p`.`category_id`, `p`.`product_name`, `p`.`product_sku`,  `p`.`product_is_active`, `p`.`chargeable`, `p`.`can_deliver`, `p`.`can_invoice`, `p`.`warranty_coverage`, `p`.`warranty_days`, `p`.`min_quantity`, `p`.`notify`, `p`.`print_height`, `p`.`print_width`, `p`.`create_date`, `p`.`update_date`  FROM `invoice_additional` 
                    Left JOIN `invoice_detail` inv_d  ON `inv_d`.`invoice_id` = `invoice_additional`.`invoice_id`
                    Left JOIN `invoice` inv  ON `inv`.`invoice_id` = inv_d.invoice_id
                    Left JOIN `product` p ON `inv_d`.`product_id` = `p`.`product_id`");
                $data['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');
                $data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p  WHERE  p.product_is_active = 1 ORDER BY p.product_name ASC"); 
                $data['price_list'] = $this->basic_model->getCustomRows("SELECT * FROM  price_plan");
                $data['customer_type'] = $this->basic_model->getCustomRows("SELECT * FROM  customer_type");
                $data['currency'] = $this->basic_model->getCustomRows('SELECT *  from currency');
                $data['ckeditor'] = 'yes';
                $data['ckeditor1'] = 'yes';
                $data['selected_invoice'] = '';
                $this->template_view($this->add_page_product,$data);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
        }  
    }

    function edit($id){
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "edit";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["save_product"] =  $this->save_product;
                    // $res['image_upload_dir'] = $this->image_upload_dir;
                    // $res['countries'] = $this->basic_model->countries;
                    $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1'");
                    $res['base_currency'] = $setting['title'];

                    $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                    $res['base_currency'] = $setting['title'];
                    $res['base_currency_id'] = $setting['id'];
                    $res['currencyData'] = $this->basic_model->getCustomRows("SELECT * FROM `currency`  "); 
                    $res["data"] = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                    $res["quotation_detail_data"] = $this->basic_model->getRows($this->table_detail,$this->table_fid,$res["data"]['amc_quotation_id'],"selected_quotation_no","ASC");
                    $res['end_user'] = $this->basic_model->getCustomRow('SELECT *  from end_user where end_user_id ='.$res["data"]['end_user']);
                    $res["add_customer"] = $this->add_customer;
                    $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p  WHERE  p.product_is_active = 1 ORDER BY p.product_name ASC");/* 
                    $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id  WHERE `user_type` = 3 AND `user_is_active` = 1"); */
                    $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN country ON user.user_country = country.country_id LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user.user_type = 3 AND user.`user_is_active` = 1 "); 
                    $res['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1"); 
                    $res["setval"] = $this->basic_model->getCustomRow("SELECT `setting_id`,`company_prefix`,`setting_company_name`,`setting_company_postal`,`setting_company_city`,`setting_company_country`,`setting_company_reg`,`quotation_prfx`,`invoice_prfx`,`delivery_prfx`,`material_prfx`,`validity_days`,`support_days`,`warranty_years`, `tax` FROM `setting` WHERE setting_id = 1");
                    $res['quotation_cover'] = $this->basic_model->getCustomRow("SELECT `quotation_cover` FROM `setting` WHERE setting_id = 1")['quotation_cover'];
                    //for list box
                    $res['quotationData'] = $this->basic_model->getCustomRows("SELECT `quotation_id`, `customer_id`, `employee_id`, `quotation_no`, `quotation_date`, `quotation_expiry_date`, `quotation_total_discount_type`, `quotation_total_discount_amount`, `quotation_buypack_discount`, `quotation_revised_no`, `quotation_freight_type`, `quotation_currency`, `quotation_status`, `quotation_email_printed_status`, `quotation_revised_id`, `quotation_validity`, `quotation_support`, `quotation_warranty`, `quotation_detail`, `revision_id`  FROM quotation WHERE quotation.quotation_revised_id = 0 AND quotation.quotation_expiry_date < CURDATE()");
                    /*$res['quotationDetailData'] = $this->basic_model->getCustomRows("SELECT `qd`.`quotation_detail_id`, `qd`.`quotation_id`, `qd`.`product_id`, `qd`.`quotation_detail_total_discount_type`, `qd`.`quotation_detail_total_discount_amount`, `qd`.`quotation_detail_quantity`, `qd`.`quotation_detail_rate`, `qd`.`quotation_detail_rate_usd`, `qd`.`quotation_detail_optional`, `qd`.`quotation_detail_existing`, `qd`.`quotation_detail_provided`, `qd`.`quotation_detail_required`, `p`.`product_id`, `p`.`category_id`, `p`.`product_name`, `p`.`product_sku`,  `p`.`product_is_active`, `p`.`chargeable`, `p`.`can_deliver`, `p`.`can_invoice`, `p`.`warranty_coverage`, `p`.`warranty_days`, `p`.`min_quantity`, `p`.`notify`, `p`.`print_height`, `p`.`print_width`, `p`.`create_date`, `p`.`update_date` FROM `quotation_detail` `qd` JOIN `product` p ON `qd`.`product_id` = `p`.`product_id`");*/
                    $res['quotationDetailData'] = $this->basic_model->getCustomRows("SELECT `invoice_additional`.*, `inv`.invoice_expire_date,`inv`.invoice_currency_id,
                    `inv_d`.invoice_detail_id, inv_d.invoice_id, inv_d.product_id, inv_d.invoice_detail_pi_quantity, inv_d.invoice_detail_dn_quantity, inv_d.invoice_detail_total_discount_type, inv_d.invoice_detail_total_discount_amount, inv_d.invoice_detail_total_amount, inv_d.invoice_detail_quantity, inv_d.invoice_detail_base_rate, inv_d.invoice_detail_usd_rate, inv_d.invoice_detail_discrepancy, inv_d.revision, inv_d.belongs_to, inv.invoice_no,  
                    `p`.`product_id`, `p`.`category_id`, `p`.`product_name`, `p`.`product_sku`,  `p`.`product_is_active`, `p`.`chargeable`, `p`.`can_deliver`, `p`.`can_invoice`, `p`.`warranty_coverage`, `p`.`warranty_days`, `p`.`min_quantity`, `p`.`notify`, `p`.`print_height`, `p`.`print_width`, `p`.`create_date`, `p`.`update_date`  FROM `invoice_additional` 
                    Left JOIN `invoice_detail` inv_d  ON `inv_d`.`invoice_id` = `invoice_additional`.`invoice_id`
                    Left JOIN `invoice` inv  ON `inv`.`invoice_id` = inv_d.invoice_id
                    Left JOIN `product` p ON `inv_d`.`product_id` = `p`.`product_id`");
                    //End for list box
                        //print_b($res['end_user']);
                    $res['terms'] = $this->basic_model->getCustomRows("SELECT *  from amc_quotation_payment_term WHERE amc_quotation_id = '".$id."' ");
                    $res['project_terms'] = $this->basic_model->getCustomRows("SELECT *  from amc_quotation_project_term WHERE amc_quotation_id = '".$id."'");

                    $res['price_list'] = $this->basic_model->getCustomRows("SELECT * FROM  price_plan");
                    $res['customer_type'] = $this->basic_model->getCustomRows("SELECT * FROM  customer_type");
                    $res['currency'] = $this->basic_model->getCustomRows('SELECT *  from currency');
                    $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');

                    
                    
                    //if term not enter add client term
                    if(empty($res['terms'])){
                        $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id='.$res["data"]['customer_id']);
                    }
                    /*if(empty($res['project_terms'])){
                    $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from client_project_term where client_id='.$res["data"]['customer_id']);
                    }*/

                    //if client term also not entered add setting term
                    if(empty($res['terms'])){
                        $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_terms');
                    }
                    if(empty($res['project_terms'])){
                    $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                    }
                    $selected_invoice = explode(",", $res['data']['amc_selected_invoice']);
                    $res['selected_invoice'] = json_encode($selected_invoice);

                    $res['ckeditor'] = 'yes';
                    $res['ckeditor1'] = 'yes';
                    $res['thisCustomerData'] = $this->basic_model->getCustomRow("SELECT * FROM `user` LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id  WHERE `user_type` = 3 AND `user_is_active` = 1 AND `user_id` = ".@$res['data']['customer_id']); 
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
    
    function progt(){
        if($_POST['pid']) {

            $data = array();
            // $this->tablep,$this->table_pid,$_POST['pid'],$this->orderBy,$this->table_pid
            $query = "SELECT ct.*, p.*, price.*, ( ( SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`revision` = 0 AND `i2`.`product_id` = `p`.`product_id` ) -( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`revision` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` JOIN `price` ON `price`.`plan_id` = '".$_POST['plan_id']."' JOIN `category_type` `ct` ON `ct`.`id` = `p`.`category_type` WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND( i.revision = 0 || i.revision IS NULL ) AND `p`.`product_id` = '".$_POST['pid']."' AND `price`.`prod_id` = '".$_POST['pid']."' ";
            $data = $this->basic_model->getCustomRow($query);
 
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    function customer_quotation(){
        if($_POST['cid']) {

            $data = array();
             $query= "SELECT * FROM invoice_additional 
                      Left join invoice on invoice_additional.invoice_id = invoice.invoice_id
                      Left join invoice_detail on invoice.invoice_id =  invoice_detail.invoice_id
             WHERE invoice_additional.customer_id = '".$_POST['cid']."' AND invoice.revision = 0 group by invoice.invoice_id";
            $data = $this->basic_model->getCustomRows($query);
            // $this->tablep,$this->table_pid,$_POST['pid'],$this->orderBy,$this->table_pid
           /* $query= "SELECT * FROM quotation WHERE quotation.quotation_revised_id = 0 AND quotation.quotation_expiry_date < CURDATE() AND quotation.customer_id = '".$_POST['cid']."' ";
            $data = $this->basic_model->getCustomRows($query);*/
 
            echo json_encode($data);
        }else{
            echo 0;
        }
    }
    function customer_amc_type()
    {
        if($_POST['cid']) {
            $cid = $_POST['cid'];
            $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM client_warranty_type Left join user ON user.user_id = ".$cid."  WHERE client_id = '".$cid."'");

            echo json_encode($res);
        }else{
            echo 0;
        }
    }

    function customer_terms(){
        if($_POST['cid']) {
            $cid = $_POST['cid'];
            
            $data = array();
            $res = array();
            
            $customer = $this->basic_model->getCustomRow("SELECT *,max(invoice_id) AS max_invoice_id from invoice WHERE customer_id = '".$cid."' AND revision = 0 ");
            $id = $customer['max_invoice_id'];
            $invoice_proforma_no = $customer['invoice_proforma_no'];
            $customer_id = $customer['customer_id'];
            $warranty_type = $this->basic_model->getCustomRows("SELECT * FROM client_warranty_type WHERE client_id ='".$customer_id."' AND deleted IS NULL");
            $terms = $this->basic_model->getCustomRows("SELECT *  from invoice_payment_term WHERE invoice_id = '".$id."' ");

            $qt_customer = $this->basic_model->getCustomRow("SELECT *,max(quotation_id) AS max_quotation_id from quotation WHERE customer_id = '".$cid."' AND quotation_revised_id = 0 ");
            $setting = $this->basic_model->getCustomRow("SELECT *  from setting");
            if($invoice_proforma_no != null && !empty($invoice_proforma_no) && isset($qt_customer['max_quotation_id']) && !empty($qt_customer['max_quotation_id'])){
                $qt_id = $qt_customer['max_quotation_id'];
                $project_terms = $this->basic_model->getCustomRows("SELECT *  from quotation_project_term WHERE quotation_id = '".$qt_id."'");
            }else{
                $project_terms = $this->basic_model->getCustomRows("SELECT *  from project_terms");

            }
            if((isset($qt_customer['quotation_validity']) && !empty($qt_customer['quotation_validity']) || isset($qt_customer['quotation_support']) && !empty($qt_customer['quotation_support']) || isset($qt_customer['quotation_warranty']) && !empty($qt_customer['quotation_warranty']))){
                $res['quotation_validity'] = @$qt_customer['quotation_validity'];
                $res['quotation_support']  = @$qt_customer['quotation_support'];
                $res['quotation_warranty'] = @$qt_customer['quotation_warranty'];  
            }else{
                $res['quotation_validity'] = $setting['validity_days'];
                $res['quotation_support']  = $setting['support_days'];
                $res['quotation_warranty'] = $setting['warranty_years'];

            }
            
            $res['quotation_cover_letter'] = $setting['quotation_cover'];
            $res['end_user'] = $this->basic_model->getCustomRows('SELECT *  from end_user where user_id='.$cid);
            if(empty($res['end_user'])){
                $res['end_user'] = '';
              }
            // $warranty = $this->basic_model->getCustomRows("SELECT * from warranty");
            // $warranty_setting = $this->basic_model->getCustomRows("SELECT warranty_type.*, warranty_setting.*  from warranty_type Left Join warranty_setting on warranty_type.warranty_type_id = warranty_setting.type_id ");
            // $warranty_type = $this->basic_model->getCustomRows('SELECT * from warranty_type');
           

            if(@$warranty_type != null && !empty(@$terms)){
                
                $res['amc_type'] = $warranty_type;  
            }
            
            
            if(@$terms != null && !empty(@$terms)){
              
                $res['payment_terms'] = $terms;
            }

            $html = '';
            if(@$project_terms != null && !empty(@$project_terms)){
                
                $res['project_duration'] = $project_terms;
            } 

            // $html = '';
            // if(@$warranty != null && !empty(@$warranty)){
            //     $check_data = Array();
            //     $new_data   = Array();
            //     foreach($warranty as $warranty){
                                                     

            //         $html .= "<tr class='warn_txtMult'>";
            //         $html .= "<td class='text-center' style='width:60px;'><a href='javascript:void(0);' class='warn_remCF'>Remove</a></td>";
            //         $html .= "<td>";
            //         $html .= "<input type='text' class='features' required style='width:100%' id='features' name='features[]' value='".@$warranty['title']."' placeholder='' />";
            //         $html .= "</td>";
                                                            
            //         $c= 1;
                                                            
            //         foreach(@$warranty_type as $type){
            //             $value = 2;
            //             $warn_percentage = 0;
            //             $find = true;
            //             foreach($warranty_setting as $setting){     
            //                 $tmp = $warranty['warranty_id'];                           
            //                 if($tmp == $setting['warn_id'] && $type['warranty_type_id'] == $setting['type_id']){
            //                     $value = $setting['value'];
            //                     $warn_percentage = $setting['percentage'];
            //                 }
            //             }
                                                                
            //             $html .= "<td>";
            //             $html .= "<select name='warranty_type_id". $c ."[]' style='width:100%' id='warranty_type_id' class='form-control warranty_type_id' required>";
            //             $html .= "<option value='' disabled='' selected=''>-- Select Option --</option>";
            //             $html .= "<option value='1' ";
            //             if( @$value == 1 ){
            //                 $html .= 'selected';
            //             } 
            //             $html .= ">Enabled</option>";
            //             $html .= "<option value='0' ";
            //             if( @$value == 1 ){
            //                 $html .= 'selected';
            //             } 
            //             $html .= ">Disabled</option>";
            //             $html .= "</select>";
            //             $html .= "</td>";
            //             $html .= "<td>";
            //             $html .= "<input type='text' class='warranty_percentage' required style='width:100%' id='warranty_percentage' name='warranty_percentage".$c."[]' value='". @$warn_percentage ."' placeholder='' />";
            //             $html .= "</td>";                                    

                                                          
            //             $c++; 
            //         } 
            //         $html .= "</tr>";
            //     } 
            //     $res['warranty_terms'] = $html;
            // } 

            echo json_encode($res);
        }else{
            echo 0;
        }
    } 

    

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res["print"] = $this->print;
        $res["mail"] = $this->mail;
        $res["edit_payment"] = $this->edit_payment;
        $res["print_payment"] = $this->print_payment;
        $res["delete_payment"] = $this->delete_payment;
        
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['data'] = $this->basic_model->getCustomRows(" SELECT end_user.*,city.*,amc_quotation.*,user.user_name FROM `amc_quotation`  INNER JOIN `user`  ON amc_quotation.customer_id = user.user_id LEFT JOIN end_user ON amc_quotation.end_user = end_user.end_user_id LEFT JOIN city ON city.city_id = end_user.end_user_location WHERE amc_quotation.amc_quotation_revised_id = 0 order by amc_quotation.amc_quotation_id desc LIMIT 100");
        $res['detail_data'] =$this->basic_model->getCustomRows("SELECT * FROM amc_quotation_detail");
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
        $res['setting'] = $setting;
        $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
        $res['base_currency_id'] = $setting['id'];
        $res["currency_name"] = $setting['title'];
          $res["ckeditor"] = "yes";
        // print_b($res['data']);
        $res['edit_status'] =$this->edit_status;
        $res['history'] = $this->history;
        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE amc_quotation.amc_quotation_revised_id = 0";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(amc_quotation.amc_quotation_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(amc_quotation.amc_quotation_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            if($customer_id != 0)
            {
                $where .= " AND amc_quotation.customer_id = ".$customer_id;
            }

            if($quotation_no != "")
            {
                $where .= " AND amc_quotation.amc_quotation_no LIKE '%".$quotation_no."%'";
            }
            

            $where .= " order by amc_quotation.amc_quotation_id desc LIMIT 100";
            $query = "SELECT end_user.*,city.*,amc_quotation.*,user.user_name FROM `amc_quotation`  INNER JOIN `user`  ON amc_quotation.customer_id = user.user_id LEFT JOIN end_user ON amc_quotation.end_user = end_user.end_user_id LEFT JOIN city ON city.city_id = end_user.end_user_location ".$where;
            
            $temp = $this->basic_model->getCustomRows($query);
            $res['data'] = array();
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $QuotationStatusData = AmcQuotationStatusData();
            
            foreach (@$temp as $v) 
            {
                    $data = [];
                    $data['amc_quotation_id'] = $v['amc_quotation_id'];
                    $data['amc_quotation_date'] = date("Y-m-d", strtotime($v["amc_quotation_date"]));
                    $data['amc_no'] =  @$setting["company_prefix"].(($v['amc_quotation_status'] == 4 ||  $v['amc_quotation_status'] == 7) ? 'AMCINV-' : 'AMC' . @$setting["quotation_prfx"]).(($v['amc_quotation_revised_no'] > 0) ? @$v['amc_quotation_no'] . '-R' . number_format(@$v['amc_quotation_revised_no']) : @$v['amc_quotation_no']);  
                    $data['amc_quotation_expiry_date'] = date("d-m-Y", strtotime($v["amc_quotation_expiry_date"]));
                    $invoices = explode(", ", $v['amc_selected_invoice_no']);
                    $data['amc_selected_invoice_no'] = '';
                    foreach (@$invoices as $iv) {
                        $data['amc_selected_invoice_no'] .= $iv . '<br>';
                    }
                    $data['user_name'] = $v['user_name'].'<br>'.(($v['end_user_name'] != '') ? $v['end_user_name'] . ' / ' . $v['city_name'] : '');
                    $data['amc_total'] = quotation_num_format($v['amc_subtotal']);

                          foreach ($QuotationStatusData as $k2 => $v2) {
                            if ($k2 == $v['amc_quotation_status']) {
                              if ($k2 == 2) {
                          
                                $data['status_name'] = $v2 . '-' . $v['amc_quotation_revised_no'];
                                          
                              } else {
                          
                                $data['status_name'] = $v2;
                                          
                              }
                            }
                          }

                    $data['amc_quotation_status'] = $v['amc_quotation_status'];
                    $data['has_payment'] = $v['has_payment'];
                    
                    array_push($res['data'],$data);
            }
            
            echo json_encode($res);
        }
    }

    function change_status(){
        extract($_POST);
        // print_b($_GET);
        $data = array();
        $detail = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
        if(isset($id) && $id != null) {
            if($status_comment == '' && $status !=  '4'){
               echo json_encode(array( "error" => "Status comment field is required"));
               return ''; 
            }
            if($status_comment != ''){
                $data['quotation_status_comment'] = $status_comment;
                //$this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                $data = array();
            }
            if($status == 4){
                $ct = 0;
                $amc_type = $this->basic_model->getCustomRows("SELECT * FROM `amc_type` `at` WHERE `at`.`amc_quotation_id` = '".$id."' AND at.amc_type_total_amount > 0");
                if($amc_type && count($amc_type) > 1){
                    echo json_encode(array( "error" => "More than one AMC type is selected"));
                    return ''; 
                }

                $this->basic_model->customQueryDel("UPDATE voucher SET flgDeleted=1 where doc_type = 3 AND doc_id=".$id);

                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
                $customer_name = $this->basic_model->getCustomRow("SELECT * FROM `user` WHERE `user_id` = '".$detail['customer_id']."' ")['user_name'];

                $Transaction_Date_year = explode("-",$detail['amc_quotation_date'])[0];
                $company_prfx = (substr($setting["company_prefix"], -1) == '-')?$setting["company_prefix"]:$setting["company_prefix"].'-';
                $t_no = serial_no_voucher($company_prfx.'JV-'.$Transaction_Date_year);

                $amc_type_total_amount = 0;
                $amc_type_subtotal = 0;
                $tax_total_value = 0;
                $amc_types = $this->basic_model->getCustomRows("SELECT * FROM `amc_type` WHERE amc_quotation_id = '".$id."'");
                
                foreach ($amc_types as $k => $v) 
                {
                    if($v['amc_type_total_discount_type'] == 1) //percentage
                    {
                        $percentage = ($v['amc_type_total_discount_amount']/100);
                        $discount_amount = ($v['amc_type_subtotal'] * $percentage);
                        $discount_amount = sprintf("%.4f",$discount_amount);
                        $discount_amount = substr_replace($discount_amount, "", -2);
                        $amc_type_subtotal += $v['amc_type_subtotal'] - $discount_amount;
                    } else if($v['amc_type_total_discount_type'] == 2)
                    {
                        $amount = $v['amc_type_total_discount_amount'];
                        $amc_type_subtotal += $v['amc_type_subtotal'] - $amount;
                    }
                    else
                    {
                        $amc_type_subtotal += $v['amc_type_subtotal'];
                        
                    }
                    
                    $amc_type_total_amount += $v['amc_type_total_amount'];

                    if($v['amc_type_subtotal'] != 0 && $v['amc_type_total_amount'] !=0)
                    {
                        $tax_total_value += $v['tax_value'];
                    }
                    
                }
                
                $voucher_data = array(
                    'TransactionNo' => $t_no,
                    'voucher_type' => 1,
                    'Transaction_Date' => $detail['amc_quotation_date'],
                    'Transaction_Detail' => 'AMC Invoice # '.@$setting["company_prefix"].'AMC'.@$setting["invoice_prfx"].$detail['amc_quotation_no'],
                    'CompanyID' => str_replace("-","",$setting["company_prefix"]),
                    'Posted' => 1,
                    'flgDeleted' => 0,
                    'doc_type' => 3,
                    'doc_id' => $id
                );
                $voucher_id = $this->basic_model->insertRecord($voucher_data, 'voucher');
                //credit entry
                if($tax_total_value != 0){
                    $voucher_detail1 = array(
                        'AccountNo' => $setting['tax_provision_account'],
                        'voucher_id' => $voucher_id,
                        'invoice_id' => @$id,
                        'Cheque_No' => 'TAX',
                        'Pay_To_Name' => @$customer_name,
                        'Amount_Cr' => $tax_total_value,
                        'Amount_Dr' => 0,
                        'Transaction_Detail' => 'AMC Invoice # '.@$setting["company_prefix"].'AMC'.@$setting["invoice_prfx"].$detail['amc_quotation_no'],
                        'Remarks' => 'Credit Entry',                     
                    );
                    $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');
                }
                    $data['invoice_tax_amount'] = $tax_total_value;
                    $amc_type_subtotal = sprintf("%.4f",$amc_type_subtotal);
                    $amc_type_subtotal = substr_replace($amc_type_subtotal, "", -2);
                    //credit entry
                    $voucher_detail1 = array(
                        'AccountNo' => $setting['revenue_account'],
                        'voucher_id' => $voucher_id,
                        'invoice_id' => @$id,
                        'Cheque_No' => 'AMC INVOICE',
                        'Pay_To_Name' => @$customer_name,
                        'Amount_Cr' => $amc_type_subtotal,
                        'Amount_Dr' => 0,
                        'Transaction_Detail' => 'AMC Invoice # '.@$setting["company_prefix"].'AMC'.@$setting["invoice_prfx"].$detail['amc_quotation_no'],
                        'Remarks' => 'Credit Entry',                     
                    );
                    $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');

                    //debit entry
                    $voucher_detail2 = array(
                        'AccountNo' => $setting['receivable_account'],
                        'voucher_id' => $voucher_id,
                        'invoice_id' => @$id,
                        'Cheque_No' => '',
                        'Pay_To_Name' => @$customer_name,
                        'Amount_Cr' => 0,
                        'Amount_Dr' => $amc_type_total_amount,
                        'Transaction_Detail' => 'AMC Invoice # '.@$setting["company_prefix"].'AMC'.@$setting["invoice_prfx"].$detail['amc_quotation_no'],
                        'Remarks' => 'Debit Entry',                     
                    );
                    $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
            }
            if($status == 7){
                  $this->basic_model->customQueryDel("UPDATE amc_quotation SET has_payment=1 where amc_quotation_id=".$id);
                  $amc_quotation = $this->basic_model->getCustomRow("SELECT * FROM amc_quotation  WHERE amc_quotation_id = '".$id."'");
                  $amc_quotation_detail = $this->basic_model->getRows('amc_quotation_detail', 'amc_quotation_id',$id,"amc_quotation_detail_id","DESC");
                        
                    foreach($amc_quotation_detail as $v){
                            if($v['selected_quotation_no'] != ''){
                                $find_inv = $this->basic_model->getCustomRow("SELECT * FROM invoice  WHERE invoice_no = '".$v['selected_quotation_no']."' ");
                                $find_inv['invoice_date'] =   date('Y-m-d', strtotime($amc_quotation['amc_quotation_expiry_date'] ));
                                $this->basic_model->customQueryDel("UPDATE invoice SET invoice_expire_date = '".$amc_quotation['amc_quotation_expiry_date']."' where invoice_no= '".$v['selected_quotation_no']."' ");
                                $this->active_amc($find_inv);

                            }
                        }
            }
           //History
            make_amc_quotation_history($id);    
            //End History
            
            
            // $status = 1;
            $data['amc_quotation_email_printed_status'] = 1;
            $data['amc_quotation_final'] = 1;
            $data['amc_quotation_status'] = $status;
            $revised = ($detail['amc_quotation_revised_no'] > 0 ) ? $detail['amc_quotation_revised_no'] : 0;
            $data['amc_quotation_revised_no'] = $revised + 1;
            $data['amc_quotation_status_comment'] = $status_comment;
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
            echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
            
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
        }
    }

    function print(){
        if ($this->check_print) {
            extract($_GET);
            // print_b($_GET);
            if(isset($id) && $id != null) {
                $history_table = "";
                if(isset($history) && $history == 1){
                    $history_table = "_history";
                }
                $res["amc_quotation"] = $this->basic_model->getRow($this->table.$history_table,$this->table_id,$id,$this->orderBy,$this->table_id);
                $res["amc_quotation_detail_data"] = $this->basic_model->getCustomRows("SELECT *,  Sum(qd.amc_quotation_detail_quantity) as total_quantity FROM `amc_quotation_detail".$history_table."` `qd` JOIN `product` `p` ON `p`.`product_id` = `qd`.`product_id` WHERE `qd`.`amc_quotation_id` = '".$id."' group by p.product_id ");
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $res['setting'] = $setting;
                
                
                $res['customer_data'] = $this->basic_model->getRow('user','user_id',$res["amc_quotation"]['customer_id'],$this->orderBy,'user_id');
                $res['customer_data']['validity_days'] = (empty($res['data']['validity_days']))? $setting['validity_days'] : $res['data']['validity_days'];
                $res['customer_data']['support_days'] = (empty($res['data']['support_days']))? $setting['support_days'] : $res['data']['support_days'];
                $res['customer_data']['warranty_years'] = (empty($res['data']['warranty_years']))? $setting['warranty_years'] : $res['data']['warranty_years'];
                
                $res['base_currency_id'] = $setting['id'];
                $res["currency"] = $this->basic_model->getCustomRow("Select * FROM currency where id = '".$res['customer_data']['currency_id']."'");
                $res["currency_name"] = $res["currency"]['title'];
                
                $res['amc_warranty_types'] = $this->basic_model->getCustomRows("SELECT * FROM `amc_type` WHERE amc_quotation_id = '".$id."'");
                $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty');
                $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, warranty_setting.*  from warranty_type Left Join warranty_setting on warranty_type.warranty_type_id = warranty_setting.type_id AND warranty_type.deleted IS NULL');
                $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');

                //History
                // make_amc_quotation_history($id);    
                //End History

                if(!isset($view) && $res['amc_quotation']['amc_quotation_status'] < 4 && $res['amc_quotation']['amc_quotation_status'] != 1){
                    //History
                    make_amc_quotation_history($id);    
                    //End History
                    $data = array();
                    if($res["amc_quotation"]['amc_quotation_email_printed_status'] == 0)
                    {
                        $data['amc_quotation_email_printed_status'] = 1;
                        $data['amc_quotation_revised_no'] = $res["amc_quotation"]['amc_quotation_revised_no'] + 1;
                        
                    }
                    else if($res["amc_quotation"]['amc_quotation_email_printed_status'] > 0)
                    {
                        $data['amc_quotation_revised_no'] = $res["amc_quotation"]['amc_quotation_revised_no'] + 1;
                            
                    }
                
                    //$data['quotation_status'] = 1;
                    if($res['amc_quotation']['amc_quotation_final'] == '' || $res['amc_quotation']['amc_quotation_final'] < 1 || $res['amc_quotation']['amc_quotation_status'] == 2){
                        $data['amc_quotation_status'] = 1;
                        
                    }
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                }

                /*$data = array();
                if($res["amc_quotation"]['amc_quotation_email_printed_status'] == 0)
                {
                    $data['amc_quotation_email_printed_status'] = 1;
                    $data['amc_quotation_revised_no'] = $res["amc_quotation"]['amc_quotation_revised_no'] + 1;
                    
                }
                else if($res["amc_quotation"]['amc_quotation_email_printed_status'] > 0)
                {
                    $data['amc_quotation_revised_no'] = $res["amc_quotation"]['amc_quotation_revised_no'] + 1;
                }
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);*/

                $res['terms'] = $this->basic_model->getCustomRows("SELECT *  from amc_quotation_payment_term WHERE amc_quotation_id = '".$id."' ");
                $res['project_terms'] = $this->basic_model->getCustomRows("SELECT *  from amc_quotation_project_term WHERE amc_quotation_id = '".$id."'");
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);

                // $res['warranty'] = $this->basic_model->getCustomRows("SELECT * from amc_quotation_warranty where amc_quotation_id = '".$id."'");
                // $res['warranty_setting'] = $this->basic_model->getCustomRows("SELECT warranty_type.*, amc_quotation_warranty_setting.*  from warranty_type Left Join amc_quotation_warranty_setting on warranty_type.warranty_type_id = amc_quotation_warranty_setting.type_id WHERE amc_quotation_id = '".$id."' ");
                
                
                //if term not enter add client term
                if(empty($res['terms'])){
                    $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id='.$res["amc_quotation"]['customer_id']);
                }
                if(empty($res['project_terms'])){
                $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms ');
                }
                $res['bank'] = $this->basic_model->getCustomRows("SELECT * FROM bank_setting");
                //if client term also not entered add setting term
                // if(empty($res['terms'])){
                //     $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
                // }
                // if(empty($res['project_terms'])){
                //   $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                // }

                // $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');

                // if(empty($res['warranty'])){
                //     $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from client_warranty where client_id='.$res["data"]['customer_id']);
                //     $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, client_warranty_setting.*  from warranty_type Left Join client_warranty_setting on warranty_type.warranty_type_id = client_warranty_setting.type_id');
                //     $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                // }
                /*if($res['amc_quotation']['amc_quotation_status'] == 4 || $res['amc_quotation']['amc_quotation_status'] == 7 || isset($is_invoice)){*/

                    //print_b($res);
                    $selected_invoice = explode(",", $res['amc_quotation']['amc_selected_invoice_no']);
                    $res['selected_invoice'] = $selected_invoice;
                if(isset($is_invoice) && $is_invoice == 1){
                    $this->basic_model->make_pdf($header_check,$res,'Report','amc_invoice_report','amc_invoice_report');
                }else{
                    $this->basic_model->make_pdf($header_check,$res,'Report','amc_quotation_report','amc_quotation_report');
                   // print_b($res);
                }
                // $this->load->view('amc_invoice_report',$res);
                
            
            }else{
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function mail()
    {
        extract($_POST);

        if(isset($id) && $id != null) {

            $setting = $this->basic_model->getRow("setting","setting_id",1,$this->orderBy,"setting_id");
            $quotation = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
            /*$customer_data = $this->basic_model->getRow("user","user_id",$quotation['customer_id'],$this->orderBy,"user_id");
            $from = $setting['setting_company_email'];
            $to = $customer_data['user_email'];
            $body = $email_body;
            $subject = $email_subject;*/
            predefine_send_email($_POST);
           

            if($quotation['amc_quotation_status'] < 4){
                //History
                make_amc_quotation_history($id);    
                //End History

                //$this->basic_model->sendEmail($from, $to, $body, $subject, $from_name = NULL);
                $data = array();
                if($quotation['quotation_email_printed_status'] == 0)
                {
                    $data['amc_quotation_email_printed_status'] = 1;
                    $data['amc_quotation_revised_no'] = $quotation['amc_quotation_revised_no'] + 1;
                    
                }
                else if($quotation['amc_quotation_email_printed_status'] > 0)
                {
                     $data['amc_quotation_revised_no'] = $quotation['amc_quotation_revised_no'] + 1;
                }
                if($quotation['amc_quotation_final'] == '' || $quotation['amc_quotation_final'] < 1 || $quotation['amc_quotation_status'] == 2){
                    $data['amc_quotation_status'] = 1;
                }
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                
            }

            //$this->basic_model->sendEmail($from, $to, $body, $subject, $from_name = NULL);
           /* $data = array();
            if($quotation['amc_quotation_email_printed_status'] == 0)
            {
                $data['amc_quotation_email_printed_status'] = 1;
                $data['amc_quotation_revised_no'] = $quotation['amc_quotation_revised_no'] + 1;
                
            }
            else if($quotation['amc_quotation_email_printed_status'] > 0)
            {
                 $data['amc_quotation_revised_no'] = $quotation['amc_quotation_revised_no'] + 1;
            }
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);*/
            echo json_encode(array( "success" => "Mail Send successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
        }
        else
        {
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
        }
        
    }

    function quotationDetailDelete($id){
        $del = $this->basic_model->deleteRecord('quotation_detail_id', $id, $this->table_detail);
        echo json_encode(array( "success" => "Record removed successfully","detailsDelete"=> true,"qoutationMultiTotalSet"=> true));

    }


    function detail($id){
        if( isset($id) && $id != ''){
            
                $res_int = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                $table_q = "amc_quotation";
                $table_qid = "amc_quotation_id";
                
                $table_c = "user";
                $table_cid = "user_id";

                $table_qd = "amc_quotation_detail";
                $table_qd_id = "amc_quotation_detail_id";


            
                $res_quo = $this->basic_model->getRow($table_q,$table_qid,$res_int['amc_quotation_id'],$this->orderBy,$table_qid);
                $res_quo_det = $this->basic_model->getCustomRows("Select * From amc_quotation_detail  qd join product p on p.product_id = qd.product_id where amc_quotation_id='".$res_int['amc_quotation_id']."'");
                $res_cust = $this->basic_model->getRow($table_c,$table_cid,$res_int['customer_id'],$this->orderBy,$table_cid);
             
            if($res_int){
                $outputTitle = "";
                $outputTitle .= "AMC Quotation Detail";

                $outputDescription = ""; 
                $outputDescription .= "<p>Customer Name  : ".$res_cust['user_name']."</p>";
                // $outputDescription .= "<p>Product Description  : ".$res_quo_det['quotation_detail_description']."</p>";
                $outputDescription .= "<p>Quotation Number  : ".$res_quo['amc_quotation_no']."</p>";
                // $outputDescription .= "<p>Quantity  : ".$res_quo_det['quotation_detail_quantity']."</p>";
                // $outputDescription .= "<p>Rate : ".$res_quo_det['quotation_detail_rate']."</p>";
                $outputDescription .= "<p>Quotation Date : ".$res_quo['amc_quotation_date']."</p>";
                $outputDescription .= "<p>Expiry Date : ".$res_quo['amc_quotation_expiry_date']."</p>";  
                
                $count = 0;;
                foreach ($res_quo_det as $k => $v) {
                    $count ++;
                    $outputDescription .= "<p>".$count.". Product Name : ".$v['product_name']."</p>";
                    $outputDescription .= "<p>Rate : ".$v['amc_quotation_detail_rate']."</p>";
                    $outputDescription .= "<p>USD Rate : ".$v['amc_quotation_detail_rate_usd']."</p>";
                    $outputDescription .= "<p>Quotation Exclude: ";
                    if($v['amc_quotation_detail_exclude'] != NULL && $v['amc_quotation_detail_exclude'] == 1)
                        {
                            $outputDescription .= "Yes";
                        }else
                        {
                            $outputDescription .= "No";
                        }
                        $outputDescription .= "</p>";
                    $outputDescription .= "<p>Quantity: ".$v['amc_quotation_detail_quantity']."</p>";
                    $outputDescription .= "<p>Start Date: ".$v['amc_quotation_detail_start_date']."</p>";
                    $outputDescription .= "<p>End Date: ".$v['amc_quotation_detail_end_date']."</p>";
                }
 
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function history(){
        if ($this->check_history) {
            extract($_GET);
            $data =  $this->basic_model->getCustomRows("SELECT * from amc_quotation_history where amc_quotation_revised_id='".$id."'  ORDER BY amc_quotation_id DESC");
            echo json_encode($data);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function follow_up(){
        if ($this->check_history) {
            extract($_GET);
            // OR quotation_id = '".$id."'
            $data =  $this->basic_model->getCustomRows("SELECT * from follow_up JOIN user on follow_up.follow_up_sales_person = user.user_id where (follow_up.follow_up_doc_type='4' || follow_up.follow_up_doc_type='5') AND follow_up.follow_up_doc_id='".$id."'  ORDER BY follow_up_id  DESC");
            echo json_encode($data);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
    }

    function active_amc($invoice){  
        $this->basic_model->customQueryDel('UPDATE invoice_additional SET additional_expiry_date = "'.$invoice['invoice_date'].'" WHERE invoice_id = "'.$invoice['invoice_id'].'"');
    }
}