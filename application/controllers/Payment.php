<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->receivables = 'receivables';
        $this->add_product = 'payment/add';
        $this->view_product = 'payment/view';
        $this->edit_product = 'payment/edit';
        $this->delete_product = 'payment/delete';
        $this->detail_product = 'payment/detail';
        // page active
        $this->add_page_product = "add-payment";
        $this->edit_page_product = "edit-payment";
        $this->view_page_product = "view-payment";
        // table
        $this->table = "payment";
        $this->tables = "setting";
        $this->table_sid = "setting_id";

        $this->table_pid = "payment_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/payment";
        // Page Heading
        $this->page_heading = "Payment";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index(){
        redirect('payment/view_page');
    }
    function view_page()
    {
        redirect($this->receivables);
    }
    function view()
    {
        extract($_GET);
        if( isset($id) && $id != '' && isset($is_invoice) && $is_invoice != ''){
            $invoice = $this->basic_model->getCustomRow("SELECT * FROM `invoice` WHERE invoice_id = '".$id."' "); 
            $quotation = $this->basic_model->getCustomRow("SELECT * FROM `quotation` WHERE quotation_id = '".$id."' "); 
            $invoice_no = "";
            $customer_id = "";
            if( (isset($invoice) && $invoice != '') || (isset($quotation) && $quotation != ''))
            {
                if(isset($invoice) && $invoice != '' && $is_invoice == 1){
                   $invoice_no = $invoice['invoice_no'];
                   $customer_id = $invoice['customer_id'];  
                }else{
                    $invoice_no = $quotation['quotation_no'];
                    $customer_id = $quotation['customer_id'];   
                } 
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res['data'] = array(
                    'invoice_no' => $invoice_no, 
                    'invoice_tax' => $invoice['invoice_tax'],
                    'invoice_id' => $id, 
                    'is_invoice' => $is_invoice, 
                    'total_amount' => @$total_amount, 
                    'toal_received' => @$toal_received, 
                    'overdue_since' => @$overdue_since, 
                    'overdue_amount' => @number_format((float)$overdue_amount, 2, '.', ''), 
                    'pre_invoice_no' => @$pre_invoice_no,
                );

                $res["customer"] = $this->basic_model->getCustomRow("SELECT * from user where user_id=".$customer_id);
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($res["setval"]["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                redirect($this->receivables);
            }
            
        }
        else
        {
            redirect($this->receivables);
        }
    }
    function add(){
        $setval = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
        if( $this->input->post()){ 
            extract($_POST);
            if(empty($id)){
             //print_b($_POST);
                 if( empty($invoice_no) || empty($payment_date) || (empty($payment_amount) && $payment_amount != 0 )  ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $invoice = $this->basic_model->getCustomRow("SELECT i.*,u.user_name FROM invoice i JOIN user u ON u.user_id = i.customer_id  WHERE i.invoice_id = '".$invoice_id."' AND i.invoice_status = 1 AND i.revision = 0  ");
                    $currency = @$invoice['invoice_currency_id'];
                    $total = @$invoice['invoice_net_amount'];
                    
                    if(empty($invoice) || $invoice == null || $is_invoice == 0){
                        $invoice = $this->basic_model->getCustomRow("SELECT q.*,u.user_name FROM quotation q JOIN user u ON u.user_id = q.customer_id  WHERE q.quotation_id = '".$invoice_id."' AND q.quotation_status = 4 AND q.quotation_revised_id = 0 ");
                        $currency = @$invoice['quotation_currency'];
                        $total = @$invoice['net_amount'];
                         
                    }
                    if($is_invoice == 0){
                        $where = "(invoice_id = '".$invoice_id."' AND is_invoice = 0)";
                    }else{
                        if(isset($invoice['invoice_proforma_no']) && $invoice['invoice_proforma_no'] != '' && $invoice['invoice_proforma_no'] > 0){
                            $qt = $this->basic_model->getCustomRow("SELECT * FROM quotation where quotation_no='".$invoice['invoice_proforma_no']."'");
                            $where = "((invoice_id = '".$invoice_id."' AND is_invoice = 1) || (invoice_id = '".@$qt['quotation_id']."' AND is_invoice = 0))";
                        }else{
                          $where = "(invoice_id = '".$invoice_id."' AND is_invoice = 1)";

                        }
                    }
                    $payment_data = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE ".$where." AND (payment_revised_id = 0 || payment_revised_id IS NULL) ");
                    $find_total = 0;
                    if(!empty($payment_data['total_payment_amount']) || !empty($payment_data['total_payment_adjustment'])){
                        $find_payment = $payment_data['total_payment_amount']+ $payment_amount;
                        $this_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_adjustment = $payment_data['total_payment_adjustment'] + $this_adjustment;
                        $find_total = $find_payment+$find_adjustment;
                        if($find_total > $total_net_amount){
                            echo json_encode(array("error" => "Payment can not exceed the P.I. / Invoice Amount".$find_total." > ". $total));
                            return false;
                        }
                    }else{
                        $find_payment =  ($payment_amount == NULL)? 0 : $payment_amount;
                        $this_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_total = $payment_amount + $this_adjustment;
                    }

                    $payment_date_year = explode("-",$payment_date)[0];
                    $payment_no = serial_no_receivable($payment_date_year);
                    // print_b($payment_no);
                    $payment_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                    $data = array(
                        "invoice_id"=> $invoice_id,
                        "invoice_no"=> $invoice_no,
                        "payment_currency_id" => $currency,
                        "receipt_no" => $payment_no,
                        "payment_date" => $payment_date,
                        "payment_amount"=> $payment_amount,
                        "payment_cheque"=> $payment_cheque,
                        "payment_notes"=> $payment_notes,
                        "payment_adjustment" => $payment_adjustment,
                        "payment_customer_id" => $payment_customer_id,
                        "is_invoice" => $is_invoice,
                        "payment_revised_id" => 0,

                    );

                    $payment_id = $this->basic_model->insertRecord($data,$this->table); 
                    //Add to voucher
                    if($payment_amount > 0)
                    {

                        $Transaction_Date_year = explode("-",$payment_date)[0];
                        $company_prfx = (substr($setval["company_prefix"], -1) == '-')?$setval["company_prefix"]:$setval["company_prefix"].'-';
                        $t_no = serial_no_voucher($company_prfx.'RV-'.$Transaction_Date_year);
                        
                        $voucher_data = array(
                            'TransactionNo' => $t_no,
                            'voucher_type' => 2,
                            'Transaction_Date' => $payment_date,
                            'Transaction_Detail' => 'Payment Voucher for # '.$setval["company_prefix"].(($is_invoice == 0)?$setval["quotation_prfx"]:$setval["invoice_prfx"]).$invoice_no,
                            'CompanyID' => str_replace("-","",$setval["company_prefix"]),
                            'Posted' => 1,
                            'flgDeleted' => 0,
                            'doc_id' => $payment_id
                        );
                        $voucher_id = $this->basic_model->insertRecord($voucher_data, 'voucher');
                        //credit entry
                        
                        $voucher_detail1 = array(
                            'AccountNo' => $setval['receivable_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => @$payment_cheque,
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => $payment_amount + $payment_adjustment,
                            'Amount_Dr' => 0,
                            'Transaction_Detail' => @$payment_notes,
                            'Remarks' => 'Credit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');
                        //debit entry
                        $voucher_detail2 = array(
                            'AccountNo' => $setval['bank_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => @$payment_cheque,
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => 0,
                            'Amount_Dr' => $payment_amount ,
                            'Transaction_Detail' => @$payment_notes,
                            'Remarks' => 'Debit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');

                        if($payment_adjustment != 0)
                        {
                            //debit entry
                            $voucher_detail2 = array(
                                'AccountNo' => $setval['adjustment_account'],
                                'voucher_id' => $voucher_id,
                                'invoice_id' => @$invoice_id,
                                'Cheque_No' => @$payment_cheque,
                                'Pay_To_Name' => @$customer_name,
                                'Amount_Cr' => 0,
                                'Amount_Dr' => $payment_adjustment,
                                'Transaction_Detail' => @$payment_notes,
                                'Remarks' => 'Debit Entry Adjustment',                     
                            );
                            $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
                        }
                    }
                    //End Voucher 
                    

                    if(!empty($payment_data) && $payment_data != null){
                        if($find_total == $total_net_amount || $find_total > $total_net_amount){
                           $find_inv = $this->basic_model->getCustomRow("SELECT * FROM invoice  WHERE invoice_id = '".$invoice_id."' AND invoice_status = 1 AND revision = 0 ");
                           if(!empty($find_inv) && $find_inv != null && $is_invoice != 0){
                                $this->basic_model->customQueryDel("UPDATE invoice SET invoice_status=4 where invoice_id=".$invoice_id);
                                $this->active_amc($find_inv);

                           }
                        }
                    }
                    if($is_invoice == 0){
                        $this->basic_model->customQueryDel('UPDATE quotation SET has_payment = 1 WHERE quotation_id = "'.$invoice_id.'"');
                    }else if($is_invoice == 1){
                        $this->basic_model->customQueryDel('UPDATE invoice SET has_payment = 1 WHERE invoice_id = "'.$invoice_id.'"');
                    }
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->receivables), 'fieldsEmpty' => 'yes'));
                }
            }else{
                if( empty($invoice_no) || empty($payment_date) || (empty($payment_amount) && $payment_amount != 0 )  ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $invoice = $this->basic_model->getCustomRow("SELECT i.*,u.user_name FROM invoice i  JOIN user u ON u.user_id = i.customer_id  WHERE i.invoice_id = '".$invoice_id."' AND (i.invoice_status = 1 || i.invoice_status = 4) AND i.revision = 0  ");
                    $currency = @$invoice['invoice_currency_id'];
                    $total = @$invoice['invoice_total_amount'];

                    if(empty($invoice) || $invoice == null){
                        $invoice = $this->basic_model->getCustomRow("SELECT q.*,u.user_name FROM quotation q JOIN user u ON u.user_id = q.customer_id  WHERE q.quotation_id = '".$invoice_id."' AND q.quotation_status = 4 AND q.quotation_revised_id = 0 ");
                        $currency = @$invoice['quotation_currency'];
                        $total = @$invoice['net_amount'];
                    }
                    $inv_id = 0;
                    if(isset($is_invoice) && $is_invoice == 0){
                        $qt = $this->basic_model->getCustomRow("SELECT * FROM quotation where quotation_id='".$invoice_id."'");
                        $inv = $this->basic_model->getCustomRow("SELECT * FROM invoice where invoice_proforma_no    ='".$qt['quotation_no']."'");
                        $inv_id = $inv['invoice_id'];
                        $where = "((invoice_id = '".$invoice_id."' AND is_invoice = 0) || (invoice_no = '".$inv['invoice_no']."' AND is_invoice = 1))";
                    }else{
                        if(isset($invoice['invoice_proforma_no']) && $invoice['invoice_proforma_no'] != '' && $invoice['invoice_proforma_no'] > 0){
                            $qt = $this->basic_model->getCustomRow("SELECT * FROM quotation where quotation_no='".$invoice['invoice_proforma_no']."'");
                            $where = "((invoice_id = '".$invoice_id."' AND is_invoice = 1) || (invoice_id = '".@$qt['quotation_id']."' AND is_invoice = 0))";
                        }else{
                          $where = "(invoice_id = '".$invoice_id."' AND is_invoice = 1)";

                        }
                        $inv_id = $invoice_id;

                    }
                    $payment_data = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE ".$where." AND (payment_revised_id = 0 || payment_revised_id IS NULL) AND payment_id != ".$id);
                    if(!empty($payment_data['total_payment_amount']) || !empty($payment_data['total_payment_adjustment'])){
                        $find_payment = $payment_data['total_payment_amount']+ $payment_amount;
                        $find_adjustment = $payment_data['total_payment_adjustment'] + ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_total = $find_payment+$find_adjustment;
                        if($find_total > $total_net_amount){
                            echo json_encode(array("error" => "Payment can not exceed the P.I. / Invoice Amount"));
                            return false;
                        }
                        if($find_total < $total_net_amount){
                            if($is_invoice == 1  || $from_invoice == 1){
                                $this->basic_model->customQueryDel("UPDATE invoice SET invoice_status=1 where invoice_id=".$inv_id);
                            }
                        }
                    }else{
                        $find_payment =  ($payment_amount == NULL)? 0 : $payment_amount;
                        $this_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                        $find_total = $payment_amount + $this_adjustment;
                        if($find_total > $total_net_amount){
                            echo json_encode(array("error" => "Payment can not exceed the P.I. / Invoice Amount"));
                            return false;
                        }
                        if($find_total < $total_net_amount){
                            if($is_invoice == 1  || $from_invoice == 1){
                                $this->basic_model->customQueryDel("UPDATE invoice SET invoice_status=1 where invoice_id=".$inv_id);
                            }
                        }
                    }
                    $payment_adjustment = ($payment_adjustment == NULL)?0:$payment_adjustment;
                    
                    $data = array(
                        "invoice_id"=> $invoice_id,
                        "invoice_no"=> $invoice_no,
                        "payment_currency_id" => $currency,
                        "receipt_no" => $receipt_no,
                        "payment_date" => $payment_date,
                        "payment_amount"=> $payment_amount,
                        "payment_cheque"=> $payment_cheque,
                        "payment_notes"=> $payment_notes,
                        "payment_adjustment" => $payment_adjustment,
                        "payment_customer_id" => $payment_customer_id,
                        "is_invoice" => $is_invoice,
                        "payment_revised_id" => 0,

                    );
                    $this->makePaymentHistory($id);
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);

                    $this->basic_model->customQueryDel("UPDATE voucher SET flgDeleted=1 where doc_type = 0 AND doc_id=".$id);
                    //Add to voucher
                    if($payment_amount > 0)
                    {
                        $Transaction_Date_year = explode("-",$payment_date)[0];
                        $company_prfx = (substr($setval["company_prefix"], -1) == '-')?$setval["company_prefix"]:$setval["company_prefix"].'-';
                        $t_no = serial_no_voucher($company_prfx.'RV-'.$Transaction_Date_year);
                        
                        $voucher_data = array(
                            'TransactionNo' => $t_no,
                            'voucher_type' => 2,
                            'Transaction_Date' => $payment_date,
                            'Transaction_Detail' => 'Payment Voucher for # '.$setval["company_prefix"].(($is_invoice == 0)?$setval["quotation_prfx"]:$setval["invoice_prfx"]).$invoice_no,
                            'CompanyID' => str_replace("-","",$setval["company_prefix"]),
                            'Posted' => 1,
                            'flgDeleted' => 0,
                            'doc_id' => $id
                        );
                        $voucher_id = $this->basic_model->insertRecord($voucher_data, 'voucher');
                        //credit entry
                        $voucher_detail1 = array(
                            'AccountNo' => $setval['receivable_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => @$payment_cheque,
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => $payment_amount + $payment_adjustment,
                            'Amount_Dr' => 0,
                            'Transaction_Detail' => @$payment_notes,
                            'Remarks' => 'Credit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail1, 'voucher_detail');
                        //debit entry
                        $voucher_detail2 = array(
                            'AccountNo' => $setval['bank_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id,
                            'Cheque_No' => @$payment_cheque,
                            'Pay_To_Name' => @$customer_name,
                            'Amount_Cr' => 0,
                            'Amount_Dr' => $payment_amount,
                            'Transaction_Detail' => @$payment_notes,
                            'Remarks' => 'Debit Entry',                     
                        );
                        $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
                        if($payment_adjustment != 0){
                            //debit entry
                            $voucher_detail2 = array(
                                'AccountNo' => $setval['adjustment_account'],
                                'voucher_id' => $voucher_id,
                                'invoice_id' => @$invoice_id,
                                'Cheque_No' => @$payment_cheque,
                                'Pay_To_Name' => @$customer_name,
                                'Amount_Cr' => 0,
                                'Amount_Dr' => $payment_adjustment,
                                'Transaction_Detail' => @$payment_notes,
                                'Remarks' => 'Debit Entry Adjustment',                     
                            );
                            $this->basic_model->insertRecord($voucher_detail2, 'voucher_detail');
                        }
                    }
                    //End Voucher 
                    
                
                    if(!empty($payment_data) && $payment_data != null){
                        if($find_total == $total_net_amount || $find_total > $total_net_amount){
                           $find_inv = $this->basic_model->getCustomRow("SELECT * FROM invoice  WHERE invoice_id = '".$inv_id."' AND invoice_status = 1 AND revision = 0 ");
                           if(!empty($find_inv) && $find_inv != null && ($is_invoice != 0 || $from_invoice == 1)){
                                 $this->basic_model->customQueryDel("UPDATE invoice SET invoice_status=4 where invoice_id=".$inv_id);
                                $this->active_amc($find_inv);
                           }
                        }
                    }
                    if($is_invoice == 0){
                        $this->basic_model->customQueryDel('UPDATE quotation SET has_payment = 1 WHERE quotation_id = "'.$invoice_id.'"');
                    }else if($is_invoice == 1){
                        $this->basic_model->customQueryDel('UPDATE invoice SET has_payment = 1 WHERE invoice_id = "'.$invoice_id.'"');
                    }


                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->receivables), 'fieldsEmpty' => 'yes'));
                }

            }
            
            
        }
        else{
            echo json_encode(array("error" => "Error"));
        }
               
        
        
    }

    function edit($id){
        if( isset($id) && $id != ''){
            /*$invoice = $this->basic_model->getCustomRow("SELECT * FROM `invoice` WHERE invoice_id = '".$id."' ");*/ 
            $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            //print_b($res_edit);
            if( $res_edit)
            {
                $res['title'] = "Edit ".$this->page_heading;
                $res["page_title"] = "edit";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res['data'] = $res_edit;
                $res['data']['total_amount'] = @$_GET['total_amount'];
                $tot = (@floatval(@$res_edit['payment_amount']) + @floatval(@$res_edit['payment_adjustment']) );
                //$res['data']['toal_received'] = quotation_num_format(floatval(@$_GET['total_received']) - $tot) ;
                $res['data']['toal_received'] = quotation_num_format(floatval(@$_GET['total_received'])) ;
                $res['data']['overdue_since'] = @$_GET['overdue_since'];
                $res['data']['toal_received']  -= $res_edit['payment_amount'] + $res_edit['payment_adjustment']; 
                $res['data']['overdue_amount'] = @number_format($res['data']['total_amount'] - $res['data']['toal_received'], 2, '.', '');
                $res['data']['pre_invoice_no'] = @$_GET['pre_invoice_no'];
                $res['data']['is_invoice'] = @$_GET['is_invoice'];
                $res["customer"] = $this->basic_model->getCustomRow("SELECT * from user where user_id=".$res_edit['payment_customer_id']);
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
                $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($res["setval"]["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                redirect($this->no_results_found);
            }
            
        }
        else
        {
            redirect($this->no_results_found);
        }
    }

    function makePaymentHistory($id){
        // $revision_id = $this->basic_model->insertRecord(["revision_title"=> 'Payment'],'tbl_revision');
        $payment_data = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
        $revision_id = $this->basic_model->getCustomRow("SELECT count(*) as revision_id from payment where blongs_to = ".$id);
        $revision_id['revision_id'] += 1;
        $data = array(
            "invoice_id"=> $payment_data['invoice_id'],
            "invoice_no"=> $payment_data['invoice_no'],
            "payment_currency_id" => $payment_data['payment_currency_id'],
            "receipt_no" => $payment_data['receipt_no'],
            "payment_date" => $payment_data['payment_date'],
            "payment_amount"=> $payment_data['payment_amount'],
            "payment_cheque"=> $payment_data['payment_cheque'],
            "payment_notes"=> $payment_data['payment_notes'],
            "payment_adjustment" => $payment_data['payment_adjustment'],
            "payment_customer_id" => $payment_data['payment_customer_id'],
            "is_invoice" => $payment_data['is_invoice'],
            "payment_revised_id" => $revision_id['revision_id'],//$revision_id,
            "blongs_to" => $id,
        );

        $payment_id = $this->basic_model->insertRecord($data,$this->table);
        return true;
    }

    function invoice_payment(){
        extract($_POST);
         if(isset($id) && $id != ''){
            if($is_invoice == 1){
              $p_no = 0;
              $invoice = $this->basic_model->getCustomRow("SELECT i.*,u.user_name,u.user_company_name,u.currency_id FROM invoice i JOIN user u ON u.user_id = i.customer_id  WHERE i.invoice_status = 1 AND i.revision = 0 AND i.invoice_id=".$id);
              if($invoice['invoice_proforma_no'] != 0 && $invoice['invoice_proforma_no'] != null && $invoice['invoice_proforma_no'] != ''){
                $quotation = $this->basic_model->getCustomRow("SELECT q.*, u.* FROM quotation q JOIN user u ON u.user_id = q.customer_id  WHERE  q.quotation_status = 4 AND q.quotation_revised_id = 0 AND q.quotation_no=".$invoice['invoice_proforma_no']);
                $p_no = $quotation['quotation_id'];
              }
              $payment = $this->basic_model->getCustomRows("SELECT * FROM payment WHERE ((invoice_id = '".$id."' AND `is_invoice` = 1) OR (invoice_id = '".$p_no."' AND `is_invoice` = 0)) AND (payment_revised_id = 0 || payment_revised_id IS NULL)");
              echo json_encode($payment);

            }else if($is_invoice == 0){
              /* $quotation = $this->basic_model->getCustomRow("SELECT q.*, u.* FROM quotation q JOIN user u ON u.user_id = q.customer_id  WHERE  q.quotation_status = 4 AND q.quotation_revised_id = 0 AND q.quotation_id=".$id);*/
               $payment = $this->basic_model->getCustomRows("SELECT * FROM payment WHERE invoice_id = '".$id."' AND (payment_revised_id = 0 || payment_revised_id IS NULL)  AND is_invoice = 0");
               if($payment){
                  echo json_encode($payment);
               }else{ 
                echo json_encode([]);
               }

            }else{
                echo json_encode([]);
            }

         }
    }
    function delete($id)
    {   $this->makePaymentHistory($id);
        $del = $this->basic_model->deleteRecord('payment_id', $id, $this->table);

        $data = array(
            "flgDeleted" => 1,
        ); 
        $this->basic_model->customQueryDel('UPDATE voucher SET flgDeleted = 1 WHERE doc_id = "'.$id.'" && doc_type = 0');
        
        echo json_encode(array( "success" => "Record removed successfully","detailsDelete"=> true,"qoutationMultiTotalSet"=> true));
    }
    function active_amc($invoice){
        $data = [
            'invoice_id' => $invoice['invoice_id'],
            'customer_id' => $invoice['customer_id'],
            'additional_expiry_date' => @amc_activation_date($invoice['invoice_date']),
            'additional_discount_type' => $invoice['invoice_total_discount_type'],
            'additional_discount_value' => $invoice['invoice_total_discount_amount'],

        ];
       $invoice_id = $this->basic_model->insertRecord($data, 'invoice_additional'); 
    }
    
   
   
} 