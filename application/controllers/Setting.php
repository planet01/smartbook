<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class setting extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        
        // page action
        $this->add_product = 'setting/add';
        $this->view_product = '/setting/view';
        $this->edit_product = 'setting/edit';
        $this->delete_product = 'setting/delete';
        $this->detail_product = 'setting/detail';
        $this->add_email  = 'setting/manager_email';
        $this->edit_email = 'setting/edit_email';
        $this->add_quotation_cover = 'setting/quotation_cover';
        $this->edit_quotation_cover = 'setting/edit_quotation_cover';
        $this->add_quotation_customer = 'setting/quotation_customer';
        $this->edit_quotation_customer = 'setting/edit_quotation_customer';
        $this->add_invoice_customer = 'setting/invoice_customer';
        $this->edit_invoice_customer = 'setting/edit_invoice_customer';
        $this->add_proforma_invoice = 'setting/proforma_invoice';
        $this->edit_proforma_invoice = 'setting/edit_proforma_invoice';
        $this->add_warranty_detail = 'setting/warranty_detail';
        $this->edit_warranty_detail = 'setting/edit_warranty_detail';
        $this->add_warranty_type = 'setting/warranty_type';
        $this->edit_warranty_type = 'setting/edit_warranty_type';

        $this->add_terms  = 'setting/terms';
        $this->edit_terms = 'setting/edit_terms';


        // page active
        $this->add_page_product = "add-setting";
        $this->edit_page_product = "edit-setting";
        $this->view_page_product = "view-setting";
        // table
        $this->table = "setting";
        $this->table_pid = "setting_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/settings";
        // FileUploader
        require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "Setting";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    
    function index(){
        redirect($this->edit_product);
    }
    function add(){
        extract($_POST);
        // print_b($_POST);

        if( $this->input->post()){
            if(empty($id)){
               if(empty($setting_company_name) || empty($setting_company_name)   || empty($setting_company_reg)){
                    echo json_encode(array("error" => "Please fill all fields marked red"));
                }
                else{
                    $row_data = array(
                        "setting_company_name" => $setting_company_name,
                        "setting_company_address" => $setting_company_address,
                        "setting_company_postal" => @$setting_company_postal,
                        "setting_company_city" => @$setting_company_city,
                        "setting_company_country" => @$setting_company_country,
                        "setting_company_reg" => $setting_company_reg,
                        "packing_prfx" => $packing_prfx,
                        "quotation_prfx" => $quotation_prfx,
                        "invoice_prfx" => $invoice_prfx,
                        "ticket_prfx" => $ticket_prfx,
                        "material_prfx" => $material_prfx,
                        "delivery_prfx" => $delivery_prfx,
                        "proforma_inv_prfx" =>$proforma_inv_prfx,
                        "company_prefix" => $company_prefix,
                        "currency_id" => $currency_id,
                        "tax" => $tax,
                        "po_no" => @$po_no,
                        "receivable_prefix" => @$receivable_prefix,
                        "material_issue_prefix" => @$material_issue_prefix,
                        "email_footer" => $email_footer,
                        "receivable_account" => $receivable_account,
                        "tax_receivable_account" => $tax_receivable_account,
                        "tax_payable_account" => $tax_payable_account,
                        "revenue_account" => $revenue_account,
                        "tax_provision_account" => $tax_provision_account,
                        "bank_account" => $bank_account,
                        "adjustment_account" => $adjustment_account,
                        
                    );
                    if(@$_FILES['header_image']['name'][0] != '' && isset($_FILES['header_image']['name'][0])){

                        $FileUploader = new FileUploader('header_image', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $row_data['header_image'] = $data['files'][0]['name'];
                    }
                    if(@$_FILES['footer_image']['name'][0] != '' && isset($_FILES['footer_image']['name'][0])){
                        $FileUploader = new FileUploader('footer_image', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                         $row_data['footer_image'] = $data['files'][0]['name'];
                    }
                    if(@$_FILES['logo']['name'][0] != '' && isset($_FILES['logo']['name'][0])){

                        $FileUploader = new FileUploader('logo', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $row_data['logo'] = $data['files'][0]['name'];
                    }
                    /* if(@$_FILES['email_footer']['name'][0] != '' && isset($_FILES['email_footer']['name'][0])){

                        $FileUploader = new FileUploader('email_footer', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $row_data['email_footer'] = $data['files'][0]['name'];
                    }*/

                    $this->basic_model->customQueryDel("DELETE FROM `bank_setting`");
                    //bank setting
                    if(isset($bank) && !empty($bank)){
                        
                         for($i = 0; $i<sizeof($bank); $i++){
                            $bank_data = [
                                'bank' => $bank[$i],
                                'account_no' => @$account_no[$i],
                                'iban' => @$iban[$i],
                                'branch' => @$branch[$i],
                                'swift_code' => @$swift_code[$i]
                            ];
                           // $updateRecord = $this->basic_model->updateRecord($row_data, $this->table, $this->table_pid, $id);
                          
                            $this->basic_model->insertRecord($bank_data,'bank_setting');
                            
                        }
                        
                    }
                   
                    
                   // $updateRecord = $this->basic_model->updateRecord($row_data, $this->table, $this->table_pid, $id);
                    
                    $id = $this->basic_model->insertRecord($row_data,$this->table);

                    echo json_encode(array( "success" => "Record added successfully","redirect"=>site_url($this->edit_product), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
                if(empty($setting_company_name) || empty($setting_company_name)   || empty($setting_company_reg)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
                }
                else{
                    $row_data = array(
                        "setting_company_name" => $setting_company_name,
                        "setting_company_address" => $setting_company_address,
                        "setting_company_postal" => @$setting_company_postal,
                        "setting_company_city" => @$setting_company_city,
                        "setting_company_country" => @$setting_company_country,
                        "setting_company_reg" => $setting_company_reg,
                        "packing_prfx" => $packing_prfx,
                        "quotation_prfx" => $quotation_prfx,
                        "invoice_prfx" => $invoice_prfx,
                        "ticket_prfx" => $ticket_prfx,
                        "material_prfx" => $material_prfx,
                        "delivery_prfx" => $delivery_prfx,
                        "proforma_inv_prfx" =>$proforma_inv_prfx,
                        "company_prefix" => $company_prefix,
                        "currency_id" => $currency_id,
                        "tax" => $tax,
                        "po_no" => @$po_no,
                        "receivable_prefix" => @$receivable_prefix,
                        "material_issue_prefix" => @$material_issue_prefix,
                        "email_footer" => $email_footer,
                        "receivable_account" => $receivable_account,
                        "tax_receivable_account" => $tax_receivable_account,
                        "tax_payable_account" => $tax_payable_account,
                        "revenue_account" => $revenue_account,
                        "tax_provision_account" => $tax_provision_account,
                        "bank_account" => $bank_account,
                        "adjustment_account" => $adjustment_account,
                    );

                    if(@$_FILES['header_image']['name'][0] != '' && isset($_FILES['header_image']['name'][0])){

                        $FileUploader = new FileUploader('header_image', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $row_data['header_image'] = $data['files'][0]['name'];
                    }
                    if(@$_FILES['footer_image']['name'][0] != '' && isset($_FILES['footer_image']['name'][0])){
                        $FileUploader = new FileUploader('footer_image', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                         $row_data['footer_image'] = $data['files'][0]['name'];
                    }
                    if(@$_FILES['logo']['name'][0] != '' && isset($_FILES['logo']['name'][0])){

                        $FileUploader = new FileUploader('logo', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $row_data['logo'] = $data['files'][0]['name'];
                    }
                    /* if(@$_FILES['email_footer']['name'][0] != '' && isset($_FILES['email_footer']['name'][0])){

                        $FileUploader = new FileUploader('email_footer', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $row_data['email_footer'] = $data['files'][0]['name'];
                    }*/

                    $this->basic_model->customQueryDel("DELETE FROM `bank_setting`");
                    //bank setting
                    if(isset($bank) && !empty($bank)){
                        for($i = 0; $i<sizeof($bank); $i++){
                            $bank_data = [
                                'bank' => $bank[$i],
                                'account_no' => @$account_no[$i],
                                'iban' => @$iban[$i],
                                'branch' => @$branch[$i],
                                'swift_code' => @$swift_code[$i]
                            ];
                           // $updateRecord = $this->basic_model->updateRecord($row_data, $this->table, $this->table_pid, $id);
                            $this->basic_model->insertRecord($bank_data,'bank_setting');
                        }
                        
                    }
                    
                    $updateRecord = $this->basic_model->updateRecord($row_data, $this->table, $this->table_pid, $id);

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }else{
            redirect($this->edit_product);
        }
    }

    function edit(){
       // extract($_POST);
        $res["title"] = "Edit ".$this->page_heading;
        $res["page_title"] =  "";
        $res["page_heading"] = 'General Details';
        $res["active"] = $this->edit_page_product;
        $res["add_product"] = $this->add_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
        $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,1,$this->orderBy,$this->table_pid);
        $res['currency'] = $this->basic_model->getCustomRows('SELECT * from currency');
        $res['bank'] = $this->basic_model->getCustomRows('SELECT * from bank_setting');
        $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($res["data"]["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
        $res['ckeditor'] = 'yes';
        $this->template_view($this->add_page_product,$res);
    }

    function delete(){
        extract($_POST);
        $file_path = dir_path().$this->image_upload_dir.'/'.@$file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $arr = array(
            $data['image_post_file_id'] => Null
        );
        $updateRecord = $this->basic_model->updateRecord($arr, $this->table, $this->table_pid, 1);

    }
    function edit_email(){
        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 15)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Manegerial Email";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Manegerial Email";
            $res["active"] = "manager_email";
            $res["add_product"] = $this->add_email;
            $res['emails'] = $this->basic_model->getCustomRows('SELECT * from email_setting');
            $this->template_view('view-setting-manager_email',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
        
    }
    function manager_email(){
        extract($_POST);
        if( $this->input->post()){
            if(empty($email)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }
            else{
              $this->basic_model->customQueryDel("DELETE FROM `email_setting`");
              //email setting
              for($i = 0; $i<sizeof($email); $i++){
                $email_data = [
                    'email' => $email[$i]
                ];
                $id = $this->basic_model->insertRecord($email_data,'email_setting');
            }
             echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_email), 'fieldsEmpty' => 'yes'));
                
            }
        }else{
            redirect($this->edit_email);
        }

    }

    function edit_quotation_cover(){

        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 16)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Quotation Cover Letter";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Quotation Cover Letter";
            $res["active"] = "quotation_cover";
            $res["add_product"] = $this->add_quotation_cover;
            $res['data'] = $this->basic_model->getCustomRows('SELECT * from '.$this->table.' where '.$this->table_pid.' = '. 1);
            $res['ckeditor'] = 'yes';

            $this->template_view('view-setting-quotation_cover',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
    function quotation_cover(){
        extract($_POST);
        if($this->input->post()){
            if(empty($quotation_cover)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }else if(empty($id)){
                echo json_encode(array("error" => "Please fill general details form first"));
                return false;
            }
            else{
                $data = [
                    'quotation_cover' => $quotation_cover
                ];
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
             echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_quotation_cover), 'fieldsEmpty' => 'yes'));
            }
                
        }else{
            redirect($this->edit_quotation_cover);
        }

    }

    function edit_quotation_customer(){
        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 17)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Quotation Customer Email Body";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Quotation Customer Email Body";
            $res["active"] = "quotation_customer";
            $res["add_product"] = $this->add_quotation_customer;
            $res['data'] = $this->basic_model->getCustomRows('SELECT * from '.$this->table.' where '.$this->table_pid.' = '. 1);
            $res['ckeditor'] = 'yes';

            $this->template_view('view-setting-quotation_customer',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
    function quotation_customer(){
        extract($_POST);
        if($this->input->post()){
            if(empty($quotation_customer)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }else if(empty($id)){
                echo json_encode(array("error" => "Please fill general details form first"));
                return false;
            }
            else{
                $data = [
                    'quotation_customer' => $quotation_customer
                ];
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
             echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_quotation_customer), 'fieldsEmpty' => 'yes'));
            }
                
        }else{
            redirect($this->edit_quotation_customer);
        }

    }

    function edit_invoice_customer(){
        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 18)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Invoice Customer Email Body";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Invoice Customer Email Body";
            $res["active"] = "invoice_customer";
            $res["add_product"] = $this->add_invoice_customer;
            $res['data'] = $this->basic_model->getCustomRows('SELECT * from '.$this->table.' where '.$this->table_pid.' = '. 1);
            $res['ckeditor'] = 'yes';

            $this->template_view('view-setting-invoice_customer',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
    function invoice_customer(){
        extract($_POST);
        if($this->input->post()){
            if(empty($invoice_customer)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }else if(empty($id)){
                echo json_encode(array("error" => "Please fill general details form first"));
                return false;
            }
            else{
                $data = [
                    'invoice_customer' => $invoice_customer
                ];
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
             echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_invoice_customer), 'fieldsEmpty' => 'yes'));
            }
                
        }else{
            redirect($this->edit_invoice_customer);
        }

    }

    
    function edit_proforma_invoice(){

        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 19)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Proforma Invoice Email Body";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Proforma Invoice Email Body";
            $res["active"] = "proforma_invoice";
            $res["add_product"] = $this->add_proforma_invoice;
            $res['data'] = $this->basic_model->getCustomRows('SELECT * from '.$this->table.' where '.$this->table_pid.' = '. 1);
            $res['ckeditor'] = 'yes';
            
            $this->template_view('view-setting-proforma_invoice',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
    function proforma_invoice(){
        extract($_POST);
        if($this->input->post()){
            if(empty($proforma_invoice)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }else if(empty($id)){
                echo json_encode(array("error" => "Please fill general details form first"));
                return false;
            }
            else{
                $data = [
                    'proforma_invoice' => $proforma_invoice
                ];
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
             echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_proforma_invoice), 'fieldsEmpty' => 'yes'));
            }
                
        }else{
            redirect($this->edit_proforma_invoice);
        }

    }

    function edit_warranty_detail(){
        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 21)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Warranty Details";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Warranty Details";
            $res["active"] = "warranty_detail";
            $res["add_product"] = $this->add_warranty_detail;
            $res['data'] = $this->basic_model->getCustomRows('SELECT * from '.$this->table.' where '.$this->table_pid.' = '. 1);
            $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty');
            $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, warranty_setting.*  from warranty_type Left Join warranty_setting on warranty_type.warranty_type_id = warranty_setting.type_id AND warranty_type.deleted IS NULL');
            $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');
            $this->template_view('view-setting-warranty_detail',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
    function warranty_detail(){
        extract($_POST);
        if($this->input->post()){
            if(empty($features)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }else{
                $this->basic_model->customQueryDel("DELETE FROM `warranty`");
                $this->basic_model->customQueryDel("DELETE FROM `warranty_setting`");
                $types = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');
                for($i= 0; $i< sizeof($features); $i++) {
                    $data = [
                        'title' => $features[$i]
                    ];
                    $id = $this->basic_model->insertRecord($data,'warranty');
                    $c = 1;
                    foreach($types as $type){
                        $data_setting = [
                            'warn_id' => $id,
                            'type_id' => $type['warranty_type_id'],
                            'value'   => $_POST['warranty_type_id_all'.$c][$i]
                        ];
                        $c++;
                       $this->basic_model->insertRecord($data_setting,'warranty_setting');
                    }
                }
                echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_warranty_detail), 'fieldsEmpty' => 'yes'));
            }
                
        }else{
            redirect($this->edit_warranty_detail);
        }

    }

    function edit_warranty_type(){
        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 20)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Warranty Type";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Warranty Type";
            $res["active"] = "warranty_type";
            $res["add_product"] = $this->add_warranty_type;
            $res['data'] = $this->basic_model->getCustomRows('SELECT *  from warranty_type where deleted IS NULL');
            $this->template_view('view-setting-warranty_type',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
    function warranty_type(){
        extract($_POST);
        if($this->input->post()){
            if(empty($warranty_type)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }else{
                $warn_data = $this->basic_model->getCustomRows('SELECT *  from warranty_type where deleted IS NULL');
                for($i= 0; $i< sizeof($warranty_type); $i++) {
                    if($warranty_type_id[$i] == 0 || $warranty_type_id[$i] == ''){
                        $data = [
                            'title' => $warranty_type[$i],
                            'percentage' => $percentage[$i]
                        ];
                     $this->basic_model->insertRecord($data,'warranty_type');
                    }else{
                        $data = [
                            'title' => $warranty_type[$i],
                            'percentage' => $percentage[$i]
                        ];
                        $this->basic_model->updateRecord($data, 'warranty_type', 'warranty_type_id', $warranty_type_id[$i]);
                    }
                }
                echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_warranty_type), 'fieldsEmpty' => 'yes'));
            }
                
        }else{
            redirect($this->edit_warranty_type);
        }

    }

    function warranty_type_delete(){
        $data = [
            'deleted' => 1
        ];
        $this->basic_model->updateRecord($data, 'warranty_type', 'warranty_type_id', $_POST['id']);
        echo true;
    }


    function edit_terms(){
        $check_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 22)
            {
                if($v['visible'] == 1)
                {
                    $check_view = true;
                }
            }
            }
        }else
        {
            $check_view = true;
        }
        if($check_view)
        {
            $res["title"] = "Edit Terms & Condition";
            $res["page_title"] =  "Settings";
            $res["page_heading"] = "Terms & Condition";
            $res["active"] = "terms";
            $res["add_product"] = $this->add_terms;
            $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,1,$this->orderBy,$this->table_pid);
            $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
            $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
            $this->template_view('view-setting-terms_and_condition',$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }
    function terms(){
        extract($_POST);
        if($this->input->post()){
            if(empty($setting_id)){
                echo json_encode(array("error" => "Please fill general details form first"));
                return false;
            }
            if(!empty($warranty_type)){
                    echo json_encode(array("error" => "Please fill all fields edit"));
            }else{
                $setting_data = [
                    'validity_days'  => $validity_days,
                    'support_days'   => $support_days,
                    'warranty_years' => $warranty_years
                ];

                $updateRecord = $this->basic_model->updateRecord($setting_data, $this->table, $this->table_pid, 1);


                $this->basic_model->customQueryDel("DELETE FROM `payment_terms`");
                if(isset($terms) && !empty($terms)){
                    for($i= 0; $i< sizeof($terms); $i++) {
                        $data = [
                            'payment_title' => $terms[$i],
                            'percentage' => $percentage[$i]
                        ];
                        $this->basic_model->insertRecord($data,'payment_terms');
                    }
                }

                $this->basic_model->customQueryDel("DELETE FROM `project_terms`");
                if(isset($project_terms) && !empty($project_terms)){
                    for($i= 0; $i< sizeof($project_terms); $i++) {
                        $project_data = [
                            'project_title' => $project_terms[$i],
                            'hash_code' => $hash_code[$i],
                            'start_week' => $start_week[$i]
                        ];
                        $this->basic_model->insertRecord($project_data,'project_terms');
                    }
                }
                echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_terms), 'fieldsEmpty' => 'yes'));
            }
                
        }else{
            redirect($this->edit_terms);
        }

    }

}