<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'employee/add';
        $this->view_product = 'employee/view';
        $this->edit_product = 'employee/edit';
        $this->delete_product = 'employee/delete';
        $this->detail_product = 'employee/detail';
        // page active
        $this->add_page_product = "add-employee";
        $this->edit_page_product = "edit-employee";
        $this->view_page_product = "view-employee";
        // table
        $this->table = "user";
        $this->table_pid = "user_id";
        $this->orderBy = "DESC";
        $this->userType = 2;
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // FileUploader
        require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "Employee";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        } 
    }
    function index(){
        redirect($this->add_product);
    }
    function add(){
        extract($_POST);
        if( $this->input->post()){
            if(empty($id)){
                if( empty($user_name) ||  empty($user_display_name) || empty($user_email) || empty($user_phone) || empty($department_id)|| empty($user_pos_title) || empty($user_password) /*||empty($user_address) || empty($user_city) || empty($user_country) || empty($user_website)*/   ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $Checkemail = $this->basic_model->getRow($this->table, 'user_email', $user_email,$this->orderBy,$this->table_pid);
                    if(@$Checkemail != null && !empty(@$Checkemail)){
                        echo json_encode(array("error" => "E-mail is already Exist"));
                        exit();
                    }

                    $Checkphone = $this->basic_model->getRow($this->table, 'user_phone', $user_phone,$this->orderBy,$this->table_pid);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        echo json_encode(array("error" => "Phone Number is Exist"));
                        exit();
                    }

                    $Checkpwd = $this->basic_model->getRow($this->table, 'user_password', $user_phone,$this->orderBy,$this->table_pid);
                    if(@$Checkpwd != null && !empty(@$Checkpwd)){
                        if ($Checkpwd['user_password'] != $user_password) {
                            echo json_encode(array("error" => "User Password Already Exist"));
                            exit();
                        }
                    }

                    if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $image_path = $data['files'][0]['name'];
                    }
                    
                    $data = array(
                        "user_name"=> $user_name,
                        "user_company_name"=> '',//$user_company_name,
                        "user_display_name" => $user_display_name,
                        "user_email" => $user_email,
                        "department_id" => $department_id,
                        "user_pos_title"=> $user_pos_title,
                        "quotation_email" => isset($quotation_email)? $quotation_email : 0,
                        "invoice_email" => isset($invoice_email)? $invoice_email : 0,
                        "proforma_invoice_email" => isset($proforma_invoice_email)? $proforma_invoice_email : 0,
                        "user_password"=> $this->basic_model->encode($user_password),
                        "user_address" => '',//$user_address,
                        "user_city" => '',//$user_city,
                        //"user_country" => '',//$user_country,
                        "user_phone" => $user_phone,
                        "user_website" => '',//$user_website,
                        "user_is_active" => $user_is_active,
                        "user_image" => @$image_path,
                        "role_id" => @$role_id,
                        "user_type" => 2
                    );

                    // print_b($data);
                    $id = $this->basic_model->insertRecord($data,$this->table);
                  
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            else{
                if( empty($user_name) || empty($user_display_name) || empty($user_email) || empty($user_phone) || @$department_id ==''
            || empty($user_pos_title) || empty($user_password) /*|| empty($user_address) || empty($user_city) || empty($user_country) || empty($user_website)*/  ){
                    echo json_encode(array("error" => "Please fill all fields"));
                }else{
                    $Checkemail = $this->basic_model->getRow($this->table, 'user_email', $user_email,$this->orderBy,$this->table_pid);
                    if(@$Checkemail != null && !empty(@$Checkemail)){
                        if ($Checkemail['user_email'] != $user_email_old) {
                            echo json_encode(array("error" => "E-mail is already Exist"));
                            exit();
                        }
                    }

                    $Checkphone = $this->basic_model->getRow($this->table, 'user_phone', $user_phone,$this->orderBy,$this->table_pid);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        if ($Checkphone['user_phone'] != $user_phone) {
                            echo json_encode(array("error" => "Phone Number is Exist"));
                            exit();
                        }
                    }
                    $Checkpwd = $this->basic_model->getRow($this->table, 'user_password', $user_phone,$this->orderBy,$this->table_pid);
                    if(@$Checkpwd != null && !empty(@$Checkpwd)){
                        if ($Checkpwd['user_password'] != $user_password) {
                            echo json_encode(array("error" => "User Password Already Exist"));
                            exit();
                        }
                    }

                    $data = array(
                        "user_name" => $user_name,
                        "user_company_name" => '',//$user_company_name,
                        "user_display_name" => $user_display_name,
                        "user_email" => $user_email,
                        "user_address" => '',//$user_address,
                        "user_city" => '',//$user_city,
                        //"user_country" => '',//$user_country,
                        "user_phone" => $user_phone,
                        "department_id" => $department_id,
                        "user_pos_title"=> $user_pos_title,
                        "user_password"=> $this->basic_model->encode($user_password),
                        "user_website" => '',//$user_website,
                        "quotation_email" => isset($quotation_email)? $quotation_email : 0,
                        "invoice_email" => isset($invoice_email)? $invoice_email : 0,
                        "proforma_invoice_email" => isset($proforma_invoice_email)? $proforma_invoice_email : 0,
                        "role_id" => @$role_id,
                        "user_is_active" => $user_is_active
                    );

                    if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $datamy = $FileUploader->upload();

                        $data['user_image'] = $datamy['files'][0]['name'];
                    }
                     
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{
            $res['title'] = "Add New ".$this->page_heading;
            $res["page_title"] = "add";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->add_page_product;
            $res["add_product"] = $this->add_product;
            $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
            $res['roles'] = $this->basic_model->getCustomRows("SELECT * FROM  role");
            $res['departmentData'] = $this->basic_model->getCustomRows("SELECT * FROM `department` ORDER BY `department_id` DESC");
            $this->template_view($this->add_page_product,$res);
        }
    }

    function edit($id){
        if( isset($id) && $id != ''){
            $res_edit = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." WHERE user_type = ".$this->userType." AND ".$this->table_pid." = ".$id." ORDER BY ".$this->table_pid." ".$this->orderBy);
            if($res_edit){
                $res["title"] = "Edit ".$this->page_heading;
                $res["page_title"] =  "";
                $res["page_heading"] = $this->page_heading;
                $res["active"] = $this->edit_page_product;
                $res["add_product"] = $this->add_product;
                $res['image_upload_dir'] = $this->image_upload_dir;
                $res['roles'] = $this->basic_model->getCustomRows("SELECT * FROM  role");
                $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                $res['departmentData'] = $this->basic_model->getCustomRows("SELECT * FROM `department` ORDER BY `department_id` DESC");
                $res['warranty_type'] = $this->basic_model->getCustomRows("SELECT * from client_warranty_type where client_id = '".$id."' ");

                // $res["user_password"] = $this->basic_model->decode($res["data"]['user_password']);
                // $res['departmentData'] = $this->basic_model->getCustomRows("SELECT * FROM `department`");
                $res['edit'] = true;
                $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                $res['data']['user_password'] = $this->basic_model->g_decode(@$res['data']['user_password']);
                // print_b($res['data']);die();
                $this->template_view($this->add_page_product,$res);
            }else{
                redirect($this->no_results_found);    
            }
        }else{
            redirect($this->no_results_found);
        }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." WHERE user_type = ".$this->userType." ORDER BY ".$this->table_pid." ".$this->orderBy);

        // print_b($res['data']);

        $this->template_view($this->view_page_product,$res);
    }
    
    function imageDelete(){
        extract($_POST);
        // print_b($_POST);
        $file_path = dir_path().$this->image_upload_dir.'/'.@$file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $arr = array(
            'user_image' => ''
        );
        $updateRecord = $this->basic_model->updateRecord($arr, $this->table, $this->table_pid, $data['image_post_file_id']);
    }
    
    function delete($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if(isset($res['user_image']) && @$res['user_image'] !=null){
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['user_image'];
                if (file_exists($file_path)) {
                    @unlink($file_path);
                }
            }
            $del = $this->basic_model->deleteRecord($this->table_pid, $id, $this->table);
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }
 
    function detail($id){
        if( isset($id) && $id != ''){
             $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
             $countries = $this->basic_model->getCustomRows("SELECT * FROM  country");
             $role = $this->basic_model->getCustomRow("SELECT * FROM  role where role_id = '".$res['role_id']."'");
             $role_detail = $this->basic_model->getCustomRows("SELECT * FROM  assign_role ar  JOIN module m ON m.module_id = ar.module_id WHERE ar.role_id = '".$res['role_id']."' ");
             $role_detail_2 = $this->basic_model->getCustomRows("SELECT * FROM  assign_role_dashboard ard  JOIN department d ON d.department_id = ard.department_id WHERE ard.role_id = '".$res['role_id']."' ");
       //     $res = true;
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = "";
                $current_img = '';
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['user_image'];
                $res_dept = $this->basic_model->getCustomRow("SELECT * FROM `department` where department_id = ".$res['department_id']);
                

                if (!is_dir($file_path) && file_exists($file_path)) {
                  $current_img = site_url($this->image_upload_dir.'/'.$res['user_image']);
                }else{
                  $current_img = site_url("uploads/dummy.jpg");
                }
                $outputDescription .= "<p><br> <img src='".$current_img."' width='100px' /></p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Name : </span>".$res['user_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>E-mail : </span>".$res['user_email']."</p>";
                if(isset($res_dept) && @$res_dept != null)
                { 
                    $outputDescription .= "<p><span style='font-weight:bold;'>Department : </span>".$res_dept['department_title']."</p>"; 
                }
                // $outputDescription .= "<p>Company Name : ".$res['user_company_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Display Name : </span>".$res['user_display_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>E-mail : </span>".$res['user_email']."</p>";
                // $outputDescription .= "<p>Password : ".$res['user_password']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Position Title : </span>".$res['user_pos_title']."</p>";
                // $outputDescription .= "<p>Address : ".$res['user_address']."</p>";
                // $outputDescription .= "<p>City : ".$res['user_city']."</p>";
                // $outputDescription .= "<p>Country : ".$res['user_country']."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Contact : </span>".$res['user_phone']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Status : </span>".(($res['user_is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";
                
                $outputDescription .= "<p><span style='font-weight:bold;'>Role Name : </span>".$role['role_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Role Description : </span>".$role['role_description']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Dashboard : </span> </p>";
                foreach ($role_detail_2 as $k => $v) 
                {
                    $outputDescription .= "<p>".$v['department_title'] ."</p>";
                }
                $count = 0;
                $outputDescription .= "<p><span style='font-weight:bold;'>Roles : </span> </p>";
                foreach ($role_detail as $k => $v) {
                    $count ++;
                    $outputDescription .= "<p>".$count.". Module : ".$v['module_name']."</p>";
                    $outputDescription .= "<p>Visible : ".(($v['visible'] == '1')?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Add: ".(($v['add'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Delete: ".(($v['delete'] == 1)?"Yes":"No")."</p>";
                    $outputDescription .= "<p>Print: ".(($v['print'] == 1)?"Yes":"No")."</p>";
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
}