<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quotation extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
       //$this->add_product = 'quotation/add';
        $this->save_product = 'quotation/add';
		
        $this->view_product = 'quotation/view'; 
		
        $this->edit_product = 'quotation/edit';
        $this->delete_product = 'quotation/delete';
        $this->detail_product = 'quotation/detail';
        // page active
        $this->add_page_product = "add-quotation";
        $this->edit_page_product = "edit-quotation";
        $this->view_page_product = "view-quotation";
        // table
        $this->table = "quotation";
        $this->table_detail = "quotation_detail";
        $this->tablep = "product";
        $this->tables = "setting";
		
        $this->table_id = "quotation_id";
        $this->table_pid = "product_id";
        $this->table_fid = "quotation_id";
        $this->table_sid = "setting_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // Page Heading
        $this->page_heading = "Quotation";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index(){
        redirect($this->add_product);
    }
    function add(){
        extract($_POST);
        if( $this->input->post()){
            // print_b($_POST);
            if(empty($id))
            {
                if(empty($customer_id) || empty($quotation_no) || empty($quotation_date) ||  empty($quotation_expiry_date) || empty($quotation_customer_note) || empty($quotation_terms_conditions)) {
                        echo json_encode(array("error" => "Please fill all fields"));
                }else{

                    $Checkquotation_no = $this->basic_model->getRow($this->table, 'quotation_no', $quotation_no,$this->orderBy,$this->table_id);
                    if(@$Checkquotation_no != null && !empty(@$Checkquotation_no)){
                        echo json_encode(array("error" => "Quotation No. is Exist"));
                        exit();
                    }

                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $quotation_detail_quantity[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'quotation_detail_quantity' => $quantityTotal_new,
                            );
                        }

                        

                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                );
                            }
                        }

                        foreach ($product_check as $k => $v) {
                            $q = "SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1 GROUP BY inventory.inventory_id";
                            // echo $q;
                            

                            $inventory_quantity_data = $this->basic_model->getCustomRow($q);
                            if ($inventory_quantity_data['total_inventory_quantity'] <= $v['quotation_detail_quantity']) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                            }
                            // die;
                            break;
                        }

                        // product_check end.
                    }

                    $data= array(
                        'customer_id' => $customer_id,
                        'quotation_no' => $quotation_no,
                        'quotation_date' => $quotation_date,
                        'quotation_expiry_date' => $quotation_expiry_date,
                        'quotation_customer_note' => $quotation_customer_note,
                        'quotation_terms_conditions' => $quotation_terms_conditions,
                        'quotation_total_discount_type' => $quotation_total_discount_type,
                        'quotation_total_discount_amount' => $quotation_total_discount_amount
                    );

                    $quotation_id = $this->basic_model->insertRecord($data,$this->table);
                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($quotation_detail_quantity as $k => $v) {
                            if ($quotation_detail_quantity[$k]>0) {
                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'quotation_id' => $quotation_id,
                                    'quotation_detail_description' => $quotation_detail_description[$k],
                                    'quotation_detail_total_discount_type' => $quotation_detail_total_discount_type[$k],
                                    'quotation_detail_total_discount_amount' => $quotation_detail_total_discount_amount[$k],
                                    'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                    'quotation_detail_rate' => $quotation_detail_rate[$k],
                                );
                                $i++;
                                // echo '<pre>';
                                // print_r($arr);
                                $quotation_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            }
                        }
                    }
                    
                    echo json_encode(array( "success" => "Record Inserted successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }else{
                if(empty($customer_id) || empty($quotation_no) || empty($quotation_date) ||  empty($quotation_expiry_date) || empty($quotation_customer_note) || empty($quotation_terms_conditions)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    
                    $Checkquotation_no = $this->basic_model->getRow($this->table, 'quotation_no', $quotation_no,$this->orderBy,$this->table_id);
                    if(@$Checkquotation_no != null && !empty(@$Checkquotation_no)){
                        if ($Checkquotation_no['quotation_no'] != $quotation_no_old) {
                            echo json_encode(array("error" => "Quotation No. is Exist"));
                            exit();
                        }
                    }



                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $quotation_detail_quantity[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'quotation_detail_quantity' => $quantityTotal_new,
                            );
                        }

                        

                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                );
                            }
                        }

                        foreach ($product_check as $k => $v) {
                            $q = "SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1 GROUP BY inventory.inventory_id";
                            // echo $q;
                            // die;

                            $inventory_quantity_data = $this->basic_model->getCustomRow($q);

                            if ($inventory_quantity_data['total_inventory_quantity'] < $v['quotation_detail_quantity']) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                            }
                            break;
                        }

                        // product_check end.
                    }

                    $data= array(
                        'customer_id' => $customer_id,
                        'quotation_no' => $quotation_no,
                        'quotation_date' => $quotation_date,
                        'quotation_expiry_date' => $quotation_expiry_date,
                        'quotation_customer_note' => $quotation_customer_note,
                        'quotation_terms_conditions' => $quotation_terms_conditions,
                        'quotation_total_discount_type' => $quotation_total_discount_type,
                        'quotation_total_discount_amount' => $quotation_total_discount_amount
                    );

                    $del = $this->basic_model->deleteRecord($this->table_fid, $id, $this->table_detail);
                    // print_b($quantity);
                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($quotation_detail_quantity as $k => $v) {
                            if ($quotation_detail_quantity[$k]>0) {
                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'quotation_id' => $id,
                                    'quotation_detail_description' => $quotation_detail_description[$k],
                                    'quotation_detail_total_discount_type' => $quotation_detail_total_discount_type[$k],
                                    'quotation_detail_total_discount_amount' => $quotation_detail_total_discount_amount[$k],
                                    'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                    'quotation_detail_rate' => $quotation_detail_rate[$k],
                                );
                                $i++;
                                // echo '<pre>';
                                // print_r($arr);
                                $quotation_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                            }
                        }
                    }
                    // print_b($arr);
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                    
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }else{
            $data['title'] = "Add New ".$this->page_heading;
            $data["page_title"] = "add";
            $data["page_heading"] = $this->page_heading;
            $data['active'] = $this->add_page_product;
            // $data["add_product"] = $this->add_product;
            $data["save_product"] = $this->save_product;
            $data['countries'] = $this->basic_model->countries;
            $data['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 3 AND `user_is_active` = 1"); 
            $data["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);
            $data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 /*GROUP BY p.product_id*/ ORDER BY p.product_name ASC"); 
 
             $this->template_view($this->add_page_product,$data);
        }  
    }
	
	function progt(){
		if($_POST['pid']) {
			$data = array();
            // $this->tablep,$this->table_pid,$_POST['pid'],$this->orderBy,$this->table_pid
            $data = $this->basic_model->getCustomRow("SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$_POST['pid']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$_POST['pid']."' AND product_is_active = 1 Group By inventory.inventory_id");
 
			echo json_encode($data);
		}else{
            echo 0;
		}
	}

// 	 function addAssginOrder()
//     {
       
 
// // 			foreach ($_POST['quantity'] as $key => $value) 
// // 			{
				

// // 				$Productname = $this->input->post('Productname')[$key];
// //         		$description = $this->input->post('description')[$key];
// //         		$quantity = $this->input->post('quantity')[$key]; 
// //         		$rate = $this->input->post('rate')[$key];  
// //         		$amount = $this->input->post('amount')[$key];  
				
// //         		$data= array(
// //         				'product_id' =>$Productname , 
// //         				'description' =>$description ,
// //         				'quantity' =>$quantity ,
// //         				'rate' =>$rate , 
// //         				'amount' =>$amount,
          				
// //         			);
        			
// // 				$data['customer_id'] = $this->input->post('customer');
// // 				$data['quotation_no'] = $this->input->post('quotation_no');
				
// // 				$res = $this->basic_model->getRow($this->tables,$this->table_sid,1);
				
// // 				$data['quotation_no'] = $res["quotation_prfx"].''.$data['quotation_no'];
				
				
// // 				$data['quotation_date'] = $this->input->post('quotation_date');
// // 				$data['expiry_date'] = $this->input->post('expiry_date');
// // 				$data['customer_note'] = $this->input->post('customer_note');
// // 				$data['terms_conditions'] = $this->input->post('terms_conditions');
// // 				$data['total'] = $this->input->post('total');
				
// // 					$old_date_timestamp = strtotime($data['quotation_date']);
// // 					$data['quotation_date'] = date('Y-m-d', $old_date_timestamp);   
					
// // 					$old_date_timestamp2 = strtotime($data['expiry_date']);
// // 					$data['expiry_date'] = date('Y-m-d', $old_date_timestamp2);   
       	 			 
// // 			  $id = $this->basic_model->insertRecord($data,$this->table);
				 
// // //		 print_b($data);
				 
// // 			}
// // 			        echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
// //                    // echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
 
// //      }
//      }

    function edit($id){
        if( isset($id) && $id != ''){
            $res_edit = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
            if($res_edit){
                extract($_POST);
                $res["title"] = "Edit ".$this->page_heading;
                $res["page_title"] =  "";
                $res["page_heading"] = $this->page_heading;
                $res["active"] = $this->edit_page_product;
                $res["save_product"] =  $this->save_product;
                // $res['image_upload_dir'] = $this->image_upload_dir;
                // $res['countries'] = $this->basic_model->countries;
                $res["data"] = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                $res["quotation_detail_data"] = $this->basic_model->getRows($this->table_detail,$this->table_fid,$res["data"]['quotation_id'],"quotation_detail_id","DESC");

                $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 GROUP BY p.product_id ORDER BY p.product_name ASC");
                $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1"); 
                $res["setval"] = $this->basic_model->getRow($this->tables,$this->table_sid,1,$this->orderBy,$this->table_sid);

                $this->template_view($this->add_page_product,$res);
            }else{
                redirect($this->no_results_found);    
            }
        }else{
            redirect($this->no_results_found);
        }
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['data'] = $this->basic_model->getCustomRows("
        SELECT quotation.*,user.user_name
        FROM `quotation` 
        INNER JOIN `user` 
        ON quotation.customer_id = user.user_id ORDER BY quotation.quotation_date DESC");
        // print_b($res['data']);

        $this->template_view($this->view_page_product,$res);
    }

    function quotationDetailDelete($id){
        $del = $this->basic_model->deleteRecord('quotation_detail_id', $id, $this->table_detail);
        echo json_encode(array( "success" => "Record removed successfully","detailsDelete"=> true,"qoutationMultiTotalSet"=> true));

    }

    // function delete($id){
    //     $del = $this->basic_model->deleteRecord('quotation_id', $id, 'quotation');
    //       echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));

    // }

    function detail($id){
        if( isset($id) && $id != ''){
            
                $res_int = $this->basic_model->getRow($this->table,$this->table_id,$id,$this->orderBy,$this->table_id);
                $table_q = "quotation";
                $table_qid = "quotation_id";
                
                $table_c = "user";
                $table_cid = "user_id";

                $table_qd = "quotation_detail";
                $table_qd_id = "quotation_detail_id";


            
                $res_quo = $this->basic_model->getRow($table_q,$table_qid,$res_int['quotation_id'],$this->orderBy,$table_qid);
                $res_quo_det = $this->basic_model->getRow($table_qd,$table_qid,$res_int['quotation_id'],$this->orderBy,$table_qd_id);
                $res_cust = $this->basic_model->getRow($table_c,$table_cid,$res_int['customer_id'],$this->orderBy,$table_cid);
             
            if($res_int){
                $outputTitle = "";
                $outputTitle .= "Quotation Detail";

                $outputDescription = ""; 
                $outputDescription .= "<p>Customer Name  : ".$res_cust['user_name']."</p>";
                // $outputDescription .= "<p>Product Description  : ".$res_quo_det['quotation_detail_description']."</p>";
                $outputDescription .= "<p>Quotation Number  : ".$res_quo['quotation_no']."</p>";
                // $outputDescription .= "<p>Quantity  : ".$res_quo_det['quotation_detail_quantity']."</p>";
                // $outputDescription .= "<p>Rate : ".$res_quo_det['quotation_detail_rate']."</p>";
                $outputDescription .= "<p>Quotation Date : ".$res_quo['quotation_date']."</p>";
                $outputDescription .= "<p>Expiry Date : ".$res_quo['quotation_expiry_date']."</p>";  
 
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
}