<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project_summary extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'Project_summary/add';
        $this->view_product = 'Project_summary/view';
        $this->edit_product = 'Project_summary/edit';
        // page active
        $this->add_page_product = "add-project_summary";
        $this->edit_page_product = "edit-project_summary";
        $this->view_page_product = "view-project_summary";
        // table
        $this->table = "projects";
        $this->table_pid = "project_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/warehouse";
        // Page Heading
        $this->page_heading = "Project Summary";
		
        $this->no_results_found = "user/no_results_found";
        
		require_once APPPATH .'third_party/FileUploader.php';

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 32) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        if ($this->check_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["setval"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res['data'] = $this->basic_model->getCustomRows("SELECT *,q.customer_id AS quotation_customer_id, inv.customer_id AS invoice_customer_id FROM `projects` p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id LEFT JOIN project_status ps ON p.status_id = ps.status_id order by p.project_id desc limit 100");
            $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $res['endUserData'] = $this->basic_model->getCustomRows("SELECT * FROM `end_user` ");
            $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
            $res['projectStatusData'] = getCustomRows("SELECT * FROM `project_status`");
            $this->template_view($this->view_page_product,$res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE p.project_id != 0";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(p.project_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(p.project_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            if($project_name != "")
            {
                $where .= " AND p.project_name LIKE '%".$project_name."%'";
            }

            if($project_status != 0)
            {
                $where .= " AND p.status_id =".$project_status;
            }
            

            $where .= " order by p.project_id desc limit 100";
            $query = "SELECT *,q.customer_id AS quotation_customer_id, inv.customer_id AS invoice_customer_id FROM `projects` p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id LEFT JOIN project_status ps ON p.status_id = ps.status_id ".$where;
            
            $temp = $this->basic_model->getCustomRows($query);
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $customerData = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $endUserData = $this->basic_model->getCustomRows("SELECT * FROM `end_user` ");
            $employeeData = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
            $res['data'] = array();
            
            foreach (@$temp as $v) 
            {
                    $data = [];
                    $data['project_id'] = $v['project_id'];
                    $data['project_date'] = date("Y-m-d", strtotime($v["project_date"]));
                    $data['project_name'] = $v['project_name']; 
                    $data['user_name'] = '';
                    if($v['doc_type'] == 1) //PI
                    {
                        foreach(@$customerData as $k2 => $v2) 
                        {
                            if($v2['user_id'] == $v['quotation_customer_id'])
                            {
                                $data['user_name'] .= $v2['user_name'].'<br>';
                            }
                        }
                        
                    }
                    else
                    {
                        if($v['end_user'] == null || $v['end_user'] == 0)
                        {
                            foreach(@$customerData as $k2 => $v2) 
                            {
                                if($v2['user_id'] == $v['invoice_customer_id'])
                                {
                                    $data['user_name'] .= $v2['user_name'].'<br>';
                                }
                            }
                        }else
                        {
                            foreach(@$endUserData as $k2 => $v2) 
                            {
                                if($v2['end_user_id'] == $v['end_user'])
                                {
                                    $data['user_name'] .= $v2['end_user_name'].'<br>';
                                }
                            }
                        }
                    }
                        if($v['doc_type'] == 1) //PI
                        {
                            $data['user_name'] .= @$setval["company_prefix"] . @$setval["quotation_prfx"]. (($v['quotation_revised_no'] > 0) ? @$v['quotation_no'] . '-R' . number_format(@$v['quotation_revised_no']) : @$v['quotation_no']);
                        }else   //INvoice
                        {
                            $rev = ($v['invoice_revised_no'] > 0)?'-R'.$v['invoice_revised_no'] : '';
                            $rev1 = ($v['invoice_status'] == 2)?'-'.$v['invoice_revised_no'] : '';
                            $data['user_name'] .= @$setval["company_prefix"].@$setval["invoice_prfx"]. $v['invoice_no'].$rev.'<br>'.(($v['invoice_proforma_no'] != 0)?@$setval["company_prefix"].@$setval["quotation_prfx"].$v['invoice_proforma_no'].find_rev_no($v['invoice_proforma_no']):'');
                        }

                        $data['employee_name'] = '';
                        foreach(@$employeeData as $k2 => $v2) 
                        {
                            if($v['primary_techinician'] == $v2['user_id'])
                            {
                                $data['employee_name'] .= $v2['user_name'].'<br>';

                            }
                            if($v['secondary_techinician'] == $v2['user_id'])
                            {
                                $data['employee_name'] .= $v2['user_name'];
                            }
                        }
                        $data['secondary_techinician'] = $v['secondary_techinician'];
                        $data['primary_techinician'] = $v['primary_techinician'];
                        $data['status_id'] = $v['status_id'];
                        $data['status'] = '';
                        if($v['status_id'] != 5){
                            $data['status'] = '<button class="btn btn-success btn-demo-space project_status" data-status-id="'.$v['status_id'].'" data-id="'. $v['project_id'].'">'. $v['status_name'].'</button>';
                            }
                            else{
                                $data['status'] = '<button class="btn btn-success btn-demo-space" data-status-id="'.$v['status_id'].'" data-id="'.$v['project_id'].'"> '.$v['status_name'].'</button>';
                        } 
                        
                    array_push($res['data'],$data);
            }
            
            echo json_encode($res);
        }
    }

    function add(){
        extract($_POST);
        // print_b($_POST);
        $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
        if( $this->input->post()){ 
            // print_b($_POST);
            if(empty($id)){
                if( empty($project_name) || empty($project_date)){
                    echo json_encode(array("error" => "Please fill all fields Add"));
                }
                else{
                   
                     $data = array(
                        'project_name' => $project_name,
                        'doc_type' => $doc_type,
                        'doc_id' => $doc_id,
                        'project_date' => $project_date,
                        'project_note' => $project_note,
                        'primary_techinician' => $primary_techinician,
                        'secondary_techinician' => $secondary_techinician,
                        'entered_by' => $this->session->userdata('admin_id')
                    );
                    $project_id = $this->basic_model->insertRecord($data, $this->table);
                    
                    foreach ($milestone as $k => $v) 
                    {   
                        if(isset($milestone[$k]['pt_description'])){
                            foreach ($milestone[$k]['pt_description'] as $k2 => $v2) 
                            {
                                    $arr = array(
                                        'project_id' => $project_id,
                                        'milestone_id' => @$milestone[$k]['milestone_id'],
                                        'pt_description' => @$milestone[$k]['pt_description'][$k2],
                                        'type_of_task' => @$milestone[$k]['type_of_task'][$k2],
                                        'no_of_hours' => @$milestone[$k]['no_of_hours'][$k2],
                                        'remarks' => @$milestone[$k]['remarks'][$k2],                     
                                    );
                                    $this->basic_model->insertRecord($arr, 'projects_task');
                                
                            }
                        }
                    }

                    echo json_encode(array( "success" => "Record added successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
            else{
              
                if( empty($project_name) || empty($project_date)){
                    echo json_encode(array("error" => "Please fill all fields Edit"));
                }
                else{
                    
                    $data = array(
                        'project_name' => $project_name,
                        'doc_type' => $doc_type,
                        'doc_id' => $doc_id,
                        'project_date' => $project_date,
                        'project_note' => $project_note,
                        'primary_techinician' => $primary_techinician,
                        'secondary_techinician' => $secondary_techinician,
                        'entered_by' => $this->session->userdata('admin_id')
                    );
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    
                    $this->basic_model->customQueryDel('DELETE from projects_task WHERE project_id='.$id);
                    foreach ($milestone as $k => $v) 
                    {   
                        if(isset($milestone[$k]['pt_description'])){
                            foreach ($milestone[$k]['pt_description'] as $k2 => $v2) 
                            {
                                    $arr = array(
                                        'project_id' => $id,
                                        'milestone_id' => @$milestone[$k]['milestone_id'],
                                        'pt_description' => @$milestone[$k]['pt_description'][$k2],
                                        'type_of_task' => @$milestone[$k]['type_of_task'][$k2],
                                        'no_of_hours' => @$milestone[$k]['no_of_hours'][$k2],
                                        'remarks' => @$milestone[$k]['remarks'][$k2],                     
                                    );
                                    $this->basic_model->insertRecord($arr, 'projects_task');
                                
                            }
                        }
                    }

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
        }
        else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res["setting"] = $setval;
                $data['ckeditor'] = 'yes';
                $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
                $res['milestoneData'] = getCustomRows("SELECT * FROM `milestones` WHERE `milestone_status` = 1");
                $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category` ");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function find_invoices()
    {
        $res['proforma_invoice'] = $this->basic_model->getCustomRows("SELECT quotation_id as ID,quotation_no as invoice_no, quotation_revised_no as revised_no, (SELECT COUNT(*) from invoice where invoice_proforma_no = quotation.quotation_no) as invoice_found FROM `quotation` WHERE quotation_revised_id = 0 AND quotation_status= '4' order by quotation.quotation_id desc");
        $res['invoice'] = $this->basic_model->getCustomRows("SELECT invoice_id as ID,invoice_no, invoice_revised_no as revised_no FROM `invoice` where revision = 0  ORDER BY invoice.create_date DESC");
        echo json_encode($res);
    }

    function edit($id){
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "Edit";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res["data"] = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id WHERE p.project_id=".$id);
                    $res["project_task_data"] = $this->basic_model->getCustomRows("SELECT * FROM projects_task WHERE project_id=".$id);
                    $res['ckeditor'] = 'yes';
                    $res["setting"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                    $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
                    $res['milestoneData'] = getCustomRows("SELECT * FROM `milestones` WHERE `milestone_status` = 1");
                    $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category` ");
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function edit_tech(){
        extract($_POST);
        if(!empty($project_id))
        {
            $data = array(
                'primary_techinician' => $primary_techinician,
                'secondary_techinician' => $secondary_techinician,
            );
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $project_id);
            echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
        }
    }

    function change_status()
    {
        extract($_POST);
        if(isset($id) && $id != null) {
            $res = $this->basic_model->getCustomRow("SELECT * FROM  projects psh where psh.project_id = '".$id."'");
            $data = array(
                'status_id' => $status_id,
                'status_comments' => $comments
            );
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);

            $arr = array(
                'project_id' => $id,
                'status_id' => $res['status_id'],
                'comments' => $res['status_comments'],
                'entered_by' => $this->session->userdata('admin_id'),                     
            );
            $this->basic_model->insertRecord($arr, 'project_status_history');
        }
    }

    function detail($id){
        if ($this->check_view) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Detail ".$this->page_heading;
                    $res["page_title"] =  "Detail";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res["data"] = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." p LEFT JOIN quotation q on q.quotation_id = p.doc_id LEFT JOIN invoice inv on inv.invoice_id = p.doc_id WHERE p.project_id=".$id);
                    $res["project_task_data"] = $this->basic_model->getCustomRows("SELECT * FROM projects_task WHERE project_id=".$id);
                    $res['ckeditor'] = 'yes';
                    $res["setting"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                    $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
                    $res['milestoneData'] = getCustomRows("SELECT * FROM `milestones` WHERE `milestone_status` = 1");
                    $res['projectCateogryData'] = getCustomRows("SELECT * FROM `project_category` ");
                    $this->template_view('project_summary_detail',$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view_status_history($id){
        if( isset($id) && $id != ''){
             
            $res = $this->basic_model->getCustomRows("SELECT * FROM  project_status_history psh JOIN user u ON u.user_id = psh.entered_by JOIN project_status ps ON psh.status_id = ps.status_id where psh.project_id = '".$id."'");
             
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." History";

                $outputDescription = "";
                $count = 0;
                foreach ($res as $k => $v) {
                    $count ++;
                    $outputDescription .= "<p><b>".$count.". Status : </b>".$v['status_name']."</p>";
                    $outputDescription .= "<p>Comments: ".$v['comments']."</p>";
                    $outputDescription .= "<p>Created Date: ".$v['created_date']."</p>";
                    $outputDescription .= "<p>Created By: ".$v['user_name']."</p>";
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => $this->page_heading.' History Not Found.'));
            }
        }else{
            echo json_encode(array("error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function invoice_detail()
    {
        extract($_POST);
        if($doc_type == 2) //invoice
        {
            $res['detail'] = $this->basic_model->getCustomRows("SELECT * FROM invoice_detail detail JOIN product p ON p.product_id = detail.product_id WHERE detail.invoice_id = '".$doc_id."'");
        }
        else //PI
        {
            $res['detail'] = $this->basic_model->getCustomRows("SELECT * FROM quotation_detail detail JOIN product p ON p.product_id = detail.product_id WHERE detail.quotation_id = '".$doc_id."'");
        }
        echo json_encode($res);
    }
}