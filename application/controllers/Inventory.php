<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class inventory extends MY_Back_Controller
{
    public function __construct()
    {
        parent::__construct();
        // page action
        $this->add_product = 'inventory/add';
        $this->view_product = 'inventory/view';
        $this->view_summary = 'inventory/summary';
        $this->edit_product = 'inventory/edit';
        $this->delete_product = 'inventory/delete';
        $this->detail_product = 'inventory/detail';
        $this->detail_stock   = "inventory/stock_detail";
        // page active
        $this->add_page_product = "add-inventory";
        $this->edit_page_product = "edit-inventory";
        $this->view_page_product = "view-inventory";
        $this->view_page_summary = "view-inventory-summary";
        // table
        $this->table = "inventory";
        $this->table_pid = "inventory_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/product";
        // Page Heading
        $this->page_heading = "Inventory";

        $this->no_results_found = "user/no_results_found";

        $this->print_inventory = "inventory/print_inventory";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index()
    {
        redirect($this->add_product);
    }
    //Jd function add()
    // {

    //     extract($_POST);
    //     if ($this->input->post()) {
    //         if (empty($id)) {
    //             if (empty($product_id) || empty($inventory_quantity) || empty($warehouse_id) || empty($shipment_type)) {
    //                 echo json_encode(array("error" => "Please fill all fields"));
    //             } else {
    //                 $Checkreceipt_no = $this->basic_model->getRow('inventory_receive', 'receipt_no', $receipt_no, $this->orderBy, 'id');
    //                 if (@$Checkreceipt_no != null && !empty(@$Checkreceipt_no)) {
    //                     echo json_encode(array("error" => "Receipt No. is Exist"));
    //                     exit();
    //                 }
    //                 /* $re_id = $this->basic_model->getCustomRows("SELECT MAX(inventory_id) as id FROM inventory;");
    //                 $res_id = isset($re_id[0]['id']) ? $re_id[0]['id']+1 : 1;
    //                 $res_no = date('Y').'-'.$res_id;*/

    //                 $shippment_date_year = explode("-", $shipment_date)[0];
    //                 $serial_no = serial_no_inventory($shippment_date_year);
    //                 // print_b($serial_no);
    //                 $data = [
    //                     'receipt_no' => $serial_no,
    //                     'note' => $note,
    //                     "shipment_date" => $shipment_date,
    //                     "shipment_type" => $shipment_type,
    //                     "warehouse_from" => ($warehouse_from > 0) ? $warehouse_from : Null,
    //                     "revision" => 0,
    //                     "belongs_to" => 0,

    //                 ];
    //                 $receipt_id = $this->basic_model->insertRecord($data, 'inventory_receive');
    //                 foreach ($product_id as  $k => $v) {
    //                     $str = $product_id[$k];
    //                     $pro_sku = (explode(",", $str));
    //                     $data = array(
    //                         "product_id" => $pro_sku[0],
    //                         "product_sku" => $pro_sku[1],
    //                         "inventory_quantity" => $inventory_quantity[$k],
    //                         "warehouse_id" => $warehouse_id[$k],
    //                         "inventory_type" => 1,
    //                         "inventory_is_active" => 1, //$inventory_is_active[$k],
    //                         'receipt_id' => $receipt_id,
    //                         "revision" => 0,
    //                         "belongs_to" => $receipt_id,
    //                         'doc_create_date' => $shipment_date
    //                     );


    //                     $id = $this->basic_model->insertRecord($data, $this->table);
    //                     $this->update_notify($pro_sku[0]);
    //                 }
    //                 //tranfer entry
    //                 if ($warehouse_from > 0) {
    //                     foreach ($product_id as  $k => $v) {
    //                         $str = $product_id[$k];
    //                         $pro_sku = (explode(",", $str));
    //                         $data = array(
    //                             "product_id" => $pro_sku[0],
    //                             "product_sku" => $pro_sku[1],
    //                             "inventory_quantity" => $inventory_quantity[$k],
    //                             "warehouse_id" => $warehouse_from,
    //                             "inventory_type" => 0,
    //                             "inventory_is_active" => 1, //$inventory_is_active[$k],
    //                             'receipt_id' => $receipt_id,
    //                             "revision" => 0,
    //                             "belongs_to" => $receipt_id,
    //                             'doc_create_date' => $shipment_date
    //                         );

    //                         $this->basic_model->insertRecord($data, $this->table);

    //                         //$this->update_notify($pro_sku[1]);
    //                     }
    //                 }


    //                 echo json_encode(array("success" => "Record Inserted successfully", "redirect" => site_url($this->view_summary), 'fieldsEmpty' => 'yes'));
    //             }
    //         } else {
    //             if (empty($product_id) || empty($inventory_quantity) || empty($warehouse_id) || empty($shipment_type)) {
    //                 echo json_encode(array("error" => "Please fill all fields"));
    //             } else {

    //                 $Checkreceipt_no = $this->basic_model->getCustomRows("SELECT * from inventory_receive where receipt_no = '" . $receipt_no . "'  AND id != " . $id . " AND (revision = 0 || revision IS Null)");

    //                 if (@$Checkreceipt_no != null && !empty(@$Checkreceipt_no)) {
    //                     echo json_encode(array("error" => "Receipt No. is already in use"));
    //                     exit();
    //                 }
    //                 if ($shipment_type_old == 'shipment' && $shipment_type == 'transfer') {
    //                     foreach ($product_id as  $k => $v) {
    //                         $str = $product_id[$k];
    //                         $pro_sku = (explode(",", $str));
    //                         $prod = $pro_sku[0];
    //                         $qy = inventory_quantity_by_warehouse($prod, $warehouse_from);
    //                         if ($qy < $inventory_quantity[$k]) {
    //                             echo json_encode(array("error" => "Stock is not enough to transfer"));
    //                             exit();
    //                         }
    //                     }
    //                 } else if ($shipment_type_old == 'transfer' && $shipment_type == 'shipment') {
    //                     foreach ($product_id as  $k => $v) {
    //                         $str = $product_id[$k];
    //                         $pro_sku = (explode(",", $str));
    //                         $prod = $pro_sku[0];
    //                         $qy = inventory_quantity_by_warehouse($prod, $warehouse_id[$k]);
    //                         if ($qy < $inventory_quantity[$k]) {
    //                             echo json_encode(array("error" => "Stock is not enough to transfer"));
    //                             exit();
    //                         }
    //                     }
    //                 }

    //                 $revision_id = $this->basic_model->insertRecord(["revision_title" => 'Inventory'], 'tbl_revision');
    //                 $this->basic_model->updateRecord(["revision" => $revision_id], 'inventory_receive', 'id', $id);
    //                 //$this->basic_model->updateRecord(["revision" => $revision_id], $this->table, 'receipt_id', $id);
    //                 $this->basic_model->customQueryDel("UPDATE `inventory` SET `revision` = '" . $revision_id . "' WHERE `receipt_id` = '" . $id . "' AND `inventory_type` != 0");

    //                 $belongs_to = ($belongs_to) ? $belongs_to : $id;

    //                 $data_receipt = [
    //                     'receipt_no' => $receipt_no,
    //                     'note' => $note,
    //                     "shipment_date" => $shipment_date,
    //                     "shipment_type" => $shipment_type,
    //                     "warehouse_from" => ($warehouse_from > 0) ? $warehouse_from : Null,
    //                     "revision" => 0,
    //                     "belongs_to" => $belongs_to
    //                 ];
    //                 //$receipt_id = $this->basic_model->updateRecord($data_receipt, 'inventory_receive', 'id', $id);
    //                 $receipt_id = $this->basic_model->insertRecord($data_receipt, 'inventory_receive');

    //                 //$this->basic_model->deleteRecord('receipt_id', $id, $this->table);
    //                 foreach ($product_id as  $k => $v) {
    //                     $str = $product_id[$k];
    //                     $pro_sku = (explode(",", $str));
    //                     $data = array(
    //                         "product_id" => $pro_sku[0],
    //                         "product_sku" => $pro_sku[1],
    //                         "inventory_quantity" => $inventory_quantity[$k],
    //                         "warehouse_id" => $warehouse_id[$k],
    //                         "inventory_type" => 1,
    //                         "inventory_is_active" => 1, //$inventory_is_active[$k],
    //                         'receipt_id' => $receipt_id,
    //                         "revision" => 0,
    //                         "belongs_to" => $belongs_to,
    //                         'doc_create_date' => $shipment_date
    //                     );
    //                     $this->basic_model->insertRecord($data, $this->table);
    //                     $this->update_notify($pro_sku[0]);
    //                 }

    //                 //tranfer entry
    //                 if ($warehouse_from > 0) {
    //                     foreach ($product_id as  $k => $v) {
    //                         $str = $product_id[$k];
    //                         $pro_sku = (explode(",", $str));

    //                         $qty  = 0;
    //                         $type = 0;
    //                         if ($inventory_quantity[$k] != $inventory_quantity_old[$k] && $inventory_quantity_old[$k] != '' && $inventory_quantity_old[$k] != 0) {
    //                             if ($inventory_quantity_old[$k] > $inventory_quantity[$k]) {
    //                                 $qty = $inventory_quantity_old[$k] - $inventory_quantity[$k];
    //                                 $type = 1;
    //                             } else if ($inventory_quantity[$k] > $inventory_quantity_old[$k]) {
    //                                 $qty = $inventory_quantity[$k] - $inventory_quantity_old[$k];
    //                                 $type = 0;
    //                             }

    //                             //$qty = $inventory_quantity[$k];
    //                             $data = array(
    //                                 "product_id" => $pro_sku[0],
    //                                 "product_sku" => $pro_sku[1],
    //                                 "inventory_quantity" => $qty,
    //                                 "warehouse_id" => $warehouse_from,
    //                                 "inventory_type" => $type,
    //                                 "inventory_is_active" => 1, //$inventory_is_active[$k],
    //                                 'receipt_id' => NUll, //$receipt_id,
    //                                 "revision" => 0,
    //                                 "belongs_to" => $receipt_id,
    //                                 'doc_create_date' => $shipment_date
    //                             );

    //                             $this->basic_model->insertRecord($data, $this->table);
    //                         } else if ($shipment_type_old == 'shipment' && $shipment_type == 'transfer') {
    //                             $qty = $inventory_quantity[$k];
    //                             $data = array(
    //                                 "product_id" => $pro_sku[0],
    //                                 "product_sku" => $pro_sku[1],
    //                                 "inventory_quantity" => $qty,
    //                                 "warehouse_id" => $warehouse_from,
    //                                 "inventory_type" => 0,
    //                                 "inventory_is_active" => 1, //$inventory_is_active[$k],
    //                                 'receipt_id' => NUll, //$receipt_id,
    //                                 "revision" => 0,
    //                                 "belongs_to" => $receipt_id,
    //                                 'doc_create_date' => $shipment_date
    //                             );
    //                             $this->basic_model->insertRecord($data, $this->table);
    //                         } else if ($shipment_type_old == 'transfer' && $shipment_type == 'shipment') {
    //                             $qty = $inventory_quantity[$k];
    //                             $data = array(
    //                                 "product_id" => $pro_sku[0],
    //                                 "product_sku" => $pro_sku[1],
    //                                 "inventory_quantity" => $qty,
    //                                 "warehouse_id" => $warehouse_from_old,
    //                                 "inventory_type" => 1,
    //                                 "inventory_is_active" => 1, //$inventory_is_active[$k],
    //                                 'receipt_id' => NUll, //$receipt_id,
    //                                 "revision" => 0,
    //                                 "belongs_to" => $receipt_id,
    //                                 'doc_create_date' => $shipment_date
    //                             );
    //                             $this->basic_model->insertRecord($data, $this->table);
    //                         } else if ($inventory_quantity[$k] > 0 && empty($inventory_quantity_old[$k]) && $shipment_type == 'transfer') {

    //                             $qty = $inventory_quantity[$k];
    //                             $type = 0;

    //                             //$qty = $inventory_quantity[$k];
    //                             $data = array(
    //                                 "product_id" => $pro_sku[0],
    //                                 "product_sku" => $pro_sku[1],
    //                                 "inventory_quantity" => $qty,
    //                                 "warehouse_id" => $warehouse_from,
    //                                 "inventory_type" => $type,
    //                                 "inventory_is_active" => 1, //$inventory_is_active[$k],
    //                                 'receipt_id' => NUll, //$receipt_id,
    //                                 "revision" => 0,
    //                                 "belongs_to" => $receipt_id,
    //                                 'doc_create_date' => $shipment_date
    //                             );

    //                             $this->basic_model->insertRecord($data, $this->table);
    //                         }

    //                         //$this->update_notify($pro_sku[1]);
    //                     }
    //                 }

    //                 echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_summary), 'fieldsEmpty' => 'yes'));
    //             }
    //         }
    //     } else {
    //         $res['title'] = "Add New " . $this->page_heading;
    //         $res["page_title"] = "add";
    //         $res["page_heading"] = $this->page_heading;
    //         $res['active'] = $this->add_page_product;
    //         $res["add_product"] = $this->add_product;
    //         /*$res['countries'] = $this->basic_model->countries;*/
    //         $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 ");
    //         $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` WHERE `product_is_active` = 1 AND can_deliver = 0");
    //         $setval = $this->basic_model->getRow('setting', 'setting_id', 1, $this->orderBy, 'setting_id');
    //         $res['receipt_no'] = '';
    //         $this->template_view($this->add_page_product, $res);
    //     }
    // }

    function add()
    {
        extract($_POST);
        if ($this->input->post()) {
            if (empty($product_id) || empty($inventory_quantity) || empty($warehouse_id) || empty($shipment_type)) {
                echo json_encode(array("error" => "Please fill all fields"));
                return;
            }
            if(!empty($id))
            {
                $this->basic_model->customQueryDel("Delete from `inventory` WHERE `receipt_id` = '" . $id . "'");
                $this->basic_model->customQueryDel("Delete from `inventory_receive` WHERE `id` = '" . $id . "'");
                $this->basic_model->customQueryDel("Delete from `inventory_receive_detail` WHERE `receipt_id` = '" . $id . "'");
            }

            $shippment_date_year = explode("-", $shipment_date)[0];
            $serial_no = serial_no_inventory($shippment_date_year);
                    // print_b($serial_no);
            $data = [
                'receipt_no' => $serial_no,
                'note' => $note,
                "shipment_date" => $shipment_date,
                "shipment_type" => $shipment_type,
                "warehouse_from" => ($warehouse_from > 0) ? $warehouse_from : Null,
                        //Jd"revision" => 0,
                        //Jd"belongs_to" => 0,

            ];

            $receipt_id = $this->basic_model->insertRecord($data, 'inventory_receive');
            foreach ($product_id as  $k => $v) {
                $str = $product_id[$k];
                $pro_sku = (explode(",", $str));
                $data = array(
                    "product_id" => $pro_sku[0],
                            //Jd"product_sku" => $pro_sku[1],
                    "inventory_quantity" => $inventory_quantity[$k],
                    "warehouse_id" => $warehouse_id[$k],
                    "inventory_type" => 1,
                            "inventory_is_active" => 1, //$inventory_is_active[$k],
                            'receipt_id' => $receipt_id,
                            //Jd"revision" => 0,
                            //Jd"belongs_to" => $receipt_id,
                            'doc_create_date' => $shipment_date
                        );

                $id = $this->basic_model->insertRecord($data, $this->table);
                $this->update_notify($pro_sku[0]);
            }


            foreach ($product_id as  $k => $v) {
                $str = $product_id[$k];
                $pro_sku = (explode(",", $str));
                $data = array(
                    "product_id" => $pro_sku[0],
                    "inventory_receive_id" =>  $receipt_id,
                    "inventory_quantity" => $inventory_quantity[$k],
                    "warehouse_id" => $warehouse_id[$k],
                    "inventory_type" => 1,
                            "inventory_is_active" => 1, //$inventory_is_active[$k],
                            'receipt_id' => $receipt_id,
                            'doc_create_date' => $shipment_date
                        );


                $id = $this->basic_model->insertRecord($data, "inventory_receive_detail");
                $this->update_notify($pro_sku[0]);
            }

                    //tranfer entry
            if ($warehouse_from > 0) {
                foreach ($product_id as  $k => $v) {
                    $str = $product_id[$k];
                    $pro_sku = (explode(",", $str));
                    $data = array(
                        "product_id" => $pro_sku[0],
                                //Jd"product_sku" => $pro_sku[1],
                        "inventory_quantity" => $inventory_quantity[$k],
                        "warehouse_id" => $warehouse_from,
                        "inventory_type" => 0,
                                "inventory_is_active" => 1, //$inventory_is_active[$k],
                                'receipt_id' => $receipt_id,
                                //Jd"revision" => 0,
                                //Jd"belongs_to" => $receipt_id,
                                'doc_create_date' => $shipment_date
                            );

                    $this->basic_model->insertRecord($data, $this->table);

                            //$this->update_notify($pro_sku[1]);
                }
            }


            echo json_encode(array("success" => "Record Inserted successfully", "redirect" => site_url($this->view_summary), 'fieldsEmpty' => 'yes'));
        }
        else {
            $res['title'] = "Add New " . $this->page_heading;
            $res["page_title"] = "add";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->add_page_product;
            $res["add_product"] = $this->add_product;
            /*$res['countries'] = $this->basic_model->countries;*/
            $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 ");
            $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` WHERE `product_is_active` = 1 AND can_deliver = 0");
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, $this->orderBy, 'setting_id');
            $res['receipt_no'] = '';
            $this->template_view($this->add_page_product, $res);
        }

    }


    function add_()
    {
        extract($_POST);

        if ($this->input->post()) {
            if (empty($id)) {
                if (empty($product_id) || empty($inventory_quantity) || empty($warehouse_id) || empty($shipment_type)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else {
                    $Checkreceipt_no = $this->basic_model->getRow('inventory_receive', 'receipt_no', $receipt_no, $this->orderBy, 'id');
                    if (@$Checkreceipt_no != null && !empty(@$Checkreceipt_no)) {
                        echo json_encode(array("error" => "Receipt No. is Exist"));
                        exit();
                    }

                    /* $re_id = $this->basic_model->getCustomRows("SELECT MAX(inventory_id) as id FROM inventory;");
                    $res_id = isset($re_id[0]['id']) ? $re_id[0]['id']+1 : 1;
                    $res_no = date('Y').'-'.$res_id;*/

                    $shippment_date_year = explode("-", $shipment_date)[0];
                    $serial_no = serial_no_inventory($shippment_date_year);
                    // print_b($serial_no);
                    $data = [
                        'receipt_no' => $serial_no,
                        'note' => $note,
                        "shipment_date" => $shipment_date,
                        "shipment_type" => $shipment_type,
                        "warehouse_from" => ($warehouse_from > 0) ? $warehouse_from : Null,
                        //Jd"revision" => 0,
                        //Jd"belongs_to" => 0,
                        
                    ];

                    $receipt_id = $this->basic_model->insertRecord($data, 'inventory_receive');
                    foreach ($product_id as  $k => $v) {
                        $str = $product_id[$k];
                        $pro_sku = (explode(",", $str));
                        $data = array(
                            "product_id" => $pro_sku[0],
                            //Jd"product_sku" => $pro_sku[1],
                            "inventory_quantity" => $inventory_quantity[$k],
                            "warehouse_id" => $warehouse_id[$k],
                            "inventory_type" => 1,
                            "inventory_is_active" => 1, //$inventory_is_active[$k],
                            'receipt_id' => $receipt_id,
                            //Jd"revision" => 0,
                            //Jd"belongs_to" => $receipt_id,
                            'doc_create_date' => $shipment_date
                        );

                        $id = $this->basic_model->insertRecord($data, $this->table);
                        $this->update_notify($pro_sku[0]);
                    }


                    foreach ($product_id as  $k => $v) {
                        $str = $product_id[$k];
                        $pro_sku = (explode(",", $str));
                        $data = array(
                            "product_id" => $pro_sku[0],
                            "inventory_receive_id" =>  $receipt_id,
                            "inventory_quantity" => $inventory_quantity[$k],
                            "warehouse_id" => $warehouse_id[$k],
                            "inventory_type" => 1,
                            "inventory_is_active" => 1, //$inventory_is_active[$k],
                            'receipt_id' => $receipt_id,
                            'doc_create_date' => $shipment_date
                        );


                        $id = $this->basic_model->insertRecord($data, "inventory_receive_detail");
                        $this->update_notify($pro_sku[0]);
                    }

                    //tranfer entry
                    if ($warehouse_from > 0) {
                        foreach ($product_id as  $k => $v) {
                            $str = $product_id[$k];
                            $pro_sku = (explode(",", $str));
                            $data = array(
                                "product_id" => $pro_sku[0],
                                //Jd"product_sku" => $pro_sku[1],
                                "inventory_quantity" => $inventory_quantity[$k],
                                "warehouse_id" => $warehouse_from,
                                "inventory_type" => 0,
                                "inventory_is_active" => 1, //$inventory_is_active[$k],
                                'receipt_id' => $receipt_id,
                                //Jd"revision" => 0,
                                //Jd"belongs_to" => $receipt_id,
                                'doc_create_date' => $shipment_date
                            );

                            $this->basic_model->insertRecord($data, $this->table);

                            //$this->update_notify($pro_sku[1]);
                        }
                    }


                    echo json_encode(array("success" => "Record Inserted successfully", "redirect" => site_url($this->view_summary), 'fieldsEmpty' => 'yes'));
                }
            } else {



                if (empty($product_id) || empty($inventory_quantity) || empty($warehouse_id) || empty($shipment_type)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else {

                    //Jd$Checkreceipt_no = $this->basic_model->getCustomRows("SELECT * from inventory_receive where receipt_no = '" . $receipt_no . "'  AND id != " . $id . " AND (revision = 0 || revision IS Null)");
                    $Checkreceipt_no = $this->basic_model->getCustomRows("SELECT * from inventory_receive where receipt_no = '" . $receipt_no . "'  AND id != " . $id . "");

                    if (@$Checkreceipt_no != null && !empty(@$Checkreceipt_no)) {
                        echo json_encode(array("error" => "Receipt No. is already in use"));
                        exit();
                    }

                    if ($shipment_type_old == 'shipment' && $shipment_type == 'transfer') {
                        foreach ($product_id as  $k => $v) {
                            $str = $product_id[$k];
                            $pro_sku = (explode(",", $str));
                            $prod = $pro_sku[0];
                            $qy = inventory_quantity_by_warehouse($prod, $warehouse_from);
                            if ($qy < $inventory_quantity[$k]) {
                                echo json_encode(array("error" => "Stock is not enough to transfer"));
                                exit();
                            }
                        }
                    } else if ($shipment_type_old == 'transfer' && $shipment_type == 'shipment') {
                        foreach ($product_id as  $k => $v) {
                            $str = $product_id[$k];
                            $pro_sku = (explode(",", $str));
                            $prod = $pro_sku[0];
                            $qy = inventory_quantity_by_warehouse($prod, $warehouse_id[$k]);
                            if ($qy < $inventory_quantity[$k]) {
                                echo json_encode(array("error" => "Stock is not enough to transfer"));
                                exit();
                            }
                        }
                    }

                    // $revision_id = $this->basic_model->insertRecord(["revision_title" => 'Inventory'], 'tbl_revision');
                    //Jd $this->basic_model->updateRecord(["revision" => $revision_id], 'inventory_receive', 'id', $id);

                    //Jd new code
                    $this->basic_model->customQueryDel("Delete from `inventory_receive` WHERE `id` = '" . $id . "'");
                    $this->basic_model->customQueryDel("Delete from `inventory_receive_detail` WHERE `inventory_receive_id` = '" . $id . "'");


                    //$this->basic_model->updateRecord(["revision" => $revision_id], $this->table, 'receipt_id', $id);
                    //Jd $this->basic_model->customQueryDel("UPDATE `inventory` SET `revision` = '" . $revision_id . "' WHERE `receipt_id` = '" . $id . "' AND `inventory_type` != 0");

                    //Jd new code
                    $this->basic_model->customQueryDel("Delete from `inventory` WHERE `receipt_id` = '" . $id . "' AND `inventory_type` != 0");

                    //Jd$belongs_to = ($belongs_to) ? $belongs_to : $id;

                    $data_receipt = [
                        'receipt_no' => $receipt_no,
                        'note' => $note,
                        "shipment_date" => $shipment_date,
                        "shipment_type" => $shipment_type,
                        "warehouse_from" => ($warehouse_from > 0) ? $warehouse_from : Null,
                       //Jd "revision" => 0,
                       //Jd "belongs_to" => $belongs_to
                    ];
                    //$receipt_id = $this->basic_model->updateRecord($data_receipt, 'inventory_receive', 'id', $id);
                    $receipt_id = $this->basic_model->insertRecord($data_receipt, 'inventory_receive');

                    //$this->basic_model->deleteRecord('receipt_id', $id, $this->table);
                    foreach ($product_id as  $k => $v) {
                        $str = $product_id[$k];
                        $pro_sku = (explode(",", $str));
                        $data = array(
                            "product_id" => $pro_sku[0],
                            //Jd"product_sku" => $pro_sku[1],
                            "inventory_quantity" => $inventory_quantity[$k],
                            "warehouse_id" => $warehouse_id[$k],
                            "inventory_type" => 1,
                            "inventory_is_active" => 1, //$inventory_is_active[$k],
                            'receipt_id' => $receipt_id,
                            //Jd"revision" => 0,
                            //Jd"belongs_to" => $belongs_to,
                            'doc_create_date' => $shipment_date
                        );
                        $this->basic_model->insertRecord($data, $this->table);
                        $this->update_notify($pro_sku[0]);
                    }


                    foreach ($product_id as  $k => $v) {
                        $str = $product_id[$k];
                        $pro_sku = (explode(",", $str));
                        $data = array(
                            "product_id" => $pro_sku[0],
                            "inventory_receive_id" => $receipt_id,
                            "inventory_quantity" => $inventory_quantity[$k],
                            "warehouse_id" => $warehouse_id[$k],
                            "inventory_type" => 1,
                            "inventory_is_active" => 1, //$inventory_is_active[$k],
                            'receipt_id' => $receipt_id,
                            'doc_create_date' => $shipment_date
                        );
                        $this->basic_model->insertRecord($data, "inventory_receive_detail");
                        $this->update_notify($pro_sku[0]);
                    }

                    //tranfer entry
                    if ($warehouse_from > 0) {
                        foreach ($product_id as  $k => $v) {
                            $str = $product_id[$k];
                            $pro_sku = (explode(",", $str));

                            $qty  = 0;
                            $type = 0;
                            if ($inventory_quantity[$k] != $inventory_quantity_old[$k] && $inventory_quantity_old[$k] != '' && $inventory_quantity_old[$k] != 0) {
                                if ($inventory_quantity_old[$k] > $inventory_quantity[$k]) {
                                    $qty = $inventory_quantity_old[$k] - $inventory_quantity[$k];
                                    $type = 1;
                                } else if ($inventory_quantity[$k] > $inventory_quantity_old[$k]) {
                                    $qty = $inventory_quantity[$k] - $inventory_quantity_old[$k];
                                    $type = 0;
                                }

                                //$qty = $inventory_quantity[$k];
                                $data = array(
                                    "product_id" => $pro_sku[0],
                                    //Jd"product_sku" => $pro_sku[1],
                                    "inventory_quantity" => $qty,
                                    "warehouse_id" => $warehouse_from,
                                    "inventory_type" => $type,
                                    "inventory_is_active" => 1, //$inventory_is_active[$k],
                                    'receipt_id' => NUll, //$receipt_id,
                                    //Jd"revision" => 0,
                                    //Jd"belongs_to" => $receipt_id,
                                    'doc_create_date' => $shipment_date
                                );

                                $this->basic_model->insertRecord($data, $this->table);
                            } else if ($shipment_type_old == 'shipment' && $shipment_type == 'transfer') {
                                $qty = $inventory_quantity[$k];
                                $data = array(
                                    "product_id" => $pro_sku[0],
                                    //Jd"product_sku" => $pro_sku[1],
                                    "inventory_quantity" => $qty,
                                    "warehouse_id" => $warehouse_from,
                                    "inventory_type" => 0,
                                    "inventory_is_active" => 1, //$inventory_is_active[$k],
                                    'receipt_id' => NUll, //$receipt_id,
                                    //Jd"revision" => 0,
                                    //Jd"belongs_to" => $receipt_id,
                                    'doc_create_date' => $shipment_date
                                );
                                $this->basic_model->insertRecord($data, $this->table);
                            } else if ($shipment_type_old == 'transfer' && $shipment_type == 'shipment') {
                                $qty = $inventory_quantity[$k];
                                $data = array(
                                    "product_id" => $pro_sku[0],
                                    //Jd"product_sku" => $pro_sku[1],
                                    "inventory_quantity" => $qty,
                                    "warehouse_id" => $warehouse_from_old,
                                    "inventory_type" => 1,
                                    "inventory_is_active" => 1, //$inventory_is_active[$k],
                                    'receipt_id' => NUll, //$receipt_id,
                                    //Jd"revision" => 0,
                                    //Jd"belongs_to" => $receipt_id,
                                    'doc_create_date' => $shipment_date
                                );
                                $this->basic_model->insertRecord($data, $this->table);
                            } else if ($inventory_quantity[$k] > 0 && empty($inventory_quantity_old[$k]) && $shipment_type == 'transfer') {

                                $qty = $inventory_quantity[$k];
                                $type = 0;

                                //$qty = $inventory_quantity[$k];
                                $data = array(
                                    "product_id" => $pro_sku[0],
                                    //Jd"product_sku" => $pro_sku[1],
                                    "inventory_quantity" => $qty,
                                    "warehouse_id" => $warehouse_from,
                                    "inventory_type" => $type,
                                    "inventory_is_active" => 1, //$inventory_is_active[$k],
                                    'receipt_id' => NUll, //$receipt_id,
                                    //Jd"revision" => 0,
                                    //Jd"belongs_to" => $receipt_id,
                                    'doc_create_date' => $shipment_date
                                );

                                $this->basic_model->insertRecord($data, $this->table);
                            }

                            //$this->update_notify($pro_sku[1]);
                        }
                    }

                    echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_summary), 'fieldsEmpty' => 'yes'));
                }
            }
        } else {
            $res['title'] = "Add New " . $this->page_heading;
            $res["page_title"] = "add";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->add_page_product;
            $res["add_product"] = $this->add_product;
            /*$res['countries'] = $this->basic_model->countries;*/
            $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 ");
            $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` WHERE `product_is_active` = 1 AND can_deliver = 0");
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, $this->orderBy, 'setting_id');
            $res['receipt_no'] = '';
            $this->template_view($this->add_page_product, $res);
        }
    }

    function edit($id)
    {

        if (isset($id) && $id != '') {
            $res_edit = $this->basic_model->getRow('inventory_receive', 'id', $id, $this->orderBy, 'id');
            if ($res_edit) {
                extract($_POST);
                $res["title"] = "Edit " . $this->page_heading;
                $res["page_title"] =  "";
                $res["page_heading"] = $this->page_heading;
                $res["active"] = $this->edit_page_product;
                $res["add_product"] = $this->add_product;
                $res['image_upload_dir'] = $this->image_upload_dir;
               //Jd $res["data"] = $this->basic_model->getCustomRows('SELECT inventory_receive.*, inventory.* from inventory Left JOIN inventory_receive on inventory.receipt_id = inventory_receive.id where inventory.receipt_id = ' . $id . " AND `inventory`.`inventory_type` = 1 AND (inventory.revision = 0 || inventory.revision IS NUll)");
                $res["data"] = $this->basic_model->getCustomRows('SELECT   inventory_receive.*, inventory.* from inventory Left JOIN inventory_receive on inventory.receipt_id = inventory_receive.id where inventory.receipt_id = ' . $id . " AND `inventory`.`inventory_type` = 1 order by inventory.inventory_id");
                $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 ");
                $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` WHERE `product_is_active` = 1 AND can_deliver = 0");
                $res['warehouse'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`");
                $res['receipt_no'] = @$res['data'][0]['receipt_no'];
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
                $res['setval'] = $setting;
              // print_b($res);
              // die;
                $this->template_view($this->add_page_product, $res);
            } else {
                redirect($this->no_results_found);
            }
        } else {
            redirect($this->no_results_found);
        }
    }

    function view($id)
    {
        $res['title'] = "View All Products";
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = "Products";
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['view_summary']    = $this->view_summary;

  //     Jd  $res['data'] = $this->basic_model->getCustomRows("
	 //    SELECT inventory.*,warehouse.warehouse_name,product.product_name 
		// FROM `inventory` 
		// INNER JOIN `warehouse` 
		// ON inventory.warehouse_id = warehouse.warehouse_id
		// INNER JOIN `product` 
  //       ON inventory.product_id = product.product_id WHERE `inventory`.`inventory_type` = 1 AND (inventory.revision = 0 || inventory.revision IS NUll) AND inventory.receipt_id = " . $id);

        $res['data'] = $this->basic_model->getCustomRows("
            SELECT inventory.*,warehouse.warehouse_name,product.product_name 
            FROM `inventory` 
            INNER JOIN `warehouse` 
            ON inventory.warehouse_id = warehouse.warehouse_id
            INNER JOIN `product` 
            ON inventory.product_id = product.product_id WHERE `inventory`.`inventory_type` = 1 AND inventory.receipt_id = " . $id ." order by `inventory`.inventory_id asc");
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
        $res['setval'] = $setting;
        $this->template_view($this->view_page_product, $res);
    }

    function summary()
    {

        $material_recieve_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 5) {
                    if ($v['visible'] == 1) {
                        $material_recieve_view = true;
                    }
                }
            }
        } else {
            $material_recieve_view = true;
        }
        if ($material_recieve_view) {
            $res['title'] = "View All " . $this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = 'Material Recieve Note';
            $res["add_product"] = $this->add_product;
            $res["edit_product"] = $this->edit_product;
            $res["view_product"] = $this->view_product;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product;
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res['print_inventory'] = $this->print_inventory;

            //Jd$res['data'] = $this->basic_model->getCustomRows("SELECT * from inventory_receive  where (revision = 0 || revision IS NUll) order By id desc");
            $res['data'] = $this->basic_model->getCustomRows("SELECT * from inventory_receive order By id desc limit 100");
			 $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting`  where `setting_id` = '1' ");
            $res['setval'] = $setting;

            $this->template_view($this->view_page_summary, $res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
    function search_view(){
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE id != 0 ";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(shipment_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(shipment_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }
            

            $where .= " ORDER BY id DESC limit 100";
            $query = "SELECT * FROM inventory_receive ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);

            
             
            echo json_encode($res);
        }
    }
    function stock_data()
    {
        $res['title'] = "View All Quantity Remaining" . $this->page_heading;
        $res['active'] = "quantity_remaining-inventory";
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;

        //Jd$res['data'] = $this->basic_model->getCustomRows("SELECT p.*, ( (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = `p`.`product_id` ) - ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND (i.revision = 0 || i.revision IS NUll)GROUP BY `p`.`product_id`");
        $res['data'] = $this->basic_model->getCustomRows("SELECT p.*, ( (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = `p`.`product_id` ) - ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 GROUP BY `p`.`product_id`");

        return $res;
    }

    function quantity_remaining()
    {
        $res = $this->stock_data();
        $this->template_view("quantity_remaining-inventory", $res);
    }

// Jd    function current_stock_status()
//     {


//        $current_stock_status_view = false;
//        if ($this->user_type == 2) {
//         foreach ($this->user_role as $k => $v) {
//             if ($v['module_id'] == 6) {
//                 if ($v['visible'] == 1) {
//                     $current_stock_status_view = true;
//                 }
//             }
//         }
//     } else {
//         $current_stock_status_view = true;
//     }
//     if ($current_stock_status_view) {
//         $res['title'] = "View All Stock Remaining" . $this->page_heading;
//         $res['active'] = "current_stock_status-inventory";
//         $res["page_heading"] = $this->page_heading;
//         $res["add_product"] = $this->add_product;
//         $res["edit_product"] = $this->edit_product;
//         $res["delete_product"] = $this->delete_product;
//         $res["detail_product"] = $this->detail_stock;
//         $res['image_upload_dir'] = $this->image_upload_dir;


//             // $res['data'] = $this->basic_model->getCustomRows("
//             //     SELECT p.*, i.*, warehouse.*, 
//             //     ( 
//             //         (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND (i2.revision = 0 || i2.revision IS NUll) AND `i2`.`product_id` = `p`.`product_id` AND `i2`.`warehouse_id` = warehouse.warehouse_id 
//             //         )  - 
//             //         ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND (i3.revision = 0 || i3.revision IS NUll) AND `i3`.`product_id` = `p`.`product_id` AND `i3`.`warehouse_id` = warehouse.warehouse_id 
//             //         ) 
//             //     ) AS total_inventory_quantity FROM `product` `p` 
//             //     Right JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` 
//             //     Right JOIN `warehouse` ON i.warehouse_id = warehouse.warehouse_id 
//             //     WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND (i.revision = 0 || i.revision IS NUll) ");

//         $res['data'] = $this->basic_model->getCustomRows("Select *, i.create_date as inventory_create_date From inventory i JOIN warehouse ON warehouse.warehouse_id = i.warehouse_id JOIN product p ON i.product_id = p.product_id WHERE  `p`.product_is_active = 1  order by i.create_date DESC,i.product_id DESC, i.warehouse_id DESC");
//         $res['warehouse'] = $this->basic_model->getCustomRows("SELECT * FROM warehouse");
//         $res['product'] = $this->basic_model->getCustomRows("SELECT * FROM product");
//         $this->template_view("current_stock_status-inventory", $res);
//     } else {
//         $res['heading'] = "Permission Not Given";
//         $res['message'] = "You don't have permission to access this page.";
//         $this->load->view('errors/html/error_404', $res);
//     }
// }

    function current_stock_status()
    {
       $res["product_id"] = "";
       $res["address"] = "";
       $res["from_date"] = "yyyy-mm-dd";
       $res["to_date"] = "yyyy-mm-dd";
       $current_stock_status_view = false;
       if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
            if ($v['module_id'] == 6) {
                if ($v['visible'] == 1) {
                    $current_stock_status_view = true;
                }
            }
        }
        } else {
            $current_stock_status_view = true;
        }
        if ($current_stock_status_view) {
            $res['title'] = "View All Stock Remaining" . $this->page_heading;
            $res['active'] = "current_stock_status-inventory";
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["edit_product"] = $this->edit_product;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_stock;
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res['previous_balance'] = "0";
            $res['data'] = [];
            $res['warehouse'] = [];
            $res['product'] = [];
            $this->template_view("current_stock_status-inventory", $res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
}
function filter_current_stock_status($productid, $address, $fromdate, $todate)
{

    $res["product_id"] = $productid;
    $res["address"] = $address;
    $res["from_date"] = $fromdate;
    $res["to_date"] = $todate;


    $current_stock_status_view = false;
    if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
            if ($v['module_id'] == 6) {
                if ($v['visible'] == 1) {
                    $current_stock_status_view = true;
                }
            }
        }
    } else {
        $current_stock_status_view = true;
    }
    if ($current_stock_status_view) {
        $res['title'] = "View All Stock Remaining" . $this->page_heading;
        $res['active'] = "current_stock_status-inventory";
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_stock;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['previous_balance'] = $this->basic_model->getCustomRows("Select  SUM( CASE WHEN(inventory_type = 1) THEN `inventory_quantity` ELSE 0 END) AS InQuantity, SUM( CASE WHEN (inventory_type = 0) THEN `inventory_quantity` ELSE 0 END ) AS OutQuantity , SUM( CASE WHEN (inventory_type = 1) THEN `inventory_quantity` ELSE 0 END ) - SUM( CASE WHEN (inventory_type = 0) THEN `inventory_quantity` ELSE 0 END ) AS Balance  From inventory i JOIN warehouse ON warehouse.warehouse_id = i.warehouse_id JOIN product p ON i.product_id = p.product_id WHERE  p.product_id = ".$productid." AND (warehouse.`warehouse_address` LIKE '%".$address."%' OR '-1' = '".$address."')  AND (i.doc_create_date < '".$fromdate."') and `p`.product_is_active = 1  order by i.create_date ASC,i.product_id DESC, i.warehouse_id DESC");
        $res['previous_balance'] = (($res['previous_balance'][0]["Balance"]) == "") ? "0" : $res['previous_balance'][0]["Balance"];
      //Jd  $res['data'] = $this->basic_model->getCustomRows("Select *, i.create_date as inventory_create_date From inventory i JOIN warehouse ON warehouse.warehouse_id = i.warehouse_id JOIN product p ON i.product_id = p.product_id WHERE  p.product_id = ".$productid." AND (warehouse.`warehouse_address` LIKE '%".$address."%' OR '-1' = '".$address."')  AND (i.doc_create_date >= '".$fromdate."' AND i.`doc_create_date` <= '".$todate."') and `p`.product_is_active = 1  order by i.create_date ASC,i.product_id DESC, i.warehouse_id DESC");
	    $query = "Select *, i.create_date as inventory_create_date From inventory i JOIN warehouse ON warehouse.warehouse_id = i.warehouse_id JOIN product p ON i.product_id = p.product_id WHERE  p.product_id = ".$productid." AND (warehouse.`warehouse_address` LIKE '%".$address."%' OR '-1' = '".$address."')  AND (i.doc_create_date >= '".$fromdate."' AND i.`doc_create_date` <= '".$todate."') and `p`.product_is_active = 1  order by i.doc_create_date ASC,i.product_id DESC, i.warehouse_id DESC";
        // print_b($query);
        $res['data'] = $this->basic_model->getCustomRows($query);
        $res['warehouse'] = $this->basic_model->getCustomRows("SELECT * FROM warehouse");
        $res['product'] = $this->basic_model->getCustomRows("SELECT * FROM product");
	
        $this->template_view("current_stock_status-inventory", $res);
    } else {
        $res['heading'] = "Permission Not Given";
        $res['message'] = "You don't have permission to access this page.";
        $this->load->view('errors/html/error_404', $res);
    }
}

function delete($id)
{
    $this->basic_model->deleteRecord('id', $id, 'inventory_receive');
    $this->basic_model->deleteRecord('receipt_id', $id, 'inventory');
    echo json_encode(array("success" => "Record deleted successfully", "redirect" => site_url($this->view_summary), 'fieldsEmpty' => 'yes'));
}

function detail($id)
{
    $this->inventory_detail($id);
}
function stock_detail($id)
{
    $this->inventory_detail($id, 1);
}
function inventory_detail($id, $stock = 0)
{
    if (isset($id) && $id != '') {

        $res_int = $this->basic_model->getRow($this->table, $this->table_pid, $id, $this->orderBy, $this->table_pid);
        $table_w = "warehouse";
        $table_wid = "warehouse_id";
        $table_s = "product";
        $table_sid = "product_id";

        $res_war = $this->basic_model->getRow($table_w, $table_wid, $res_int['warehouse_id'], $this->orderBy, $table_wid);
        $res_pro = $this->basic_model->getRow($table_s, $table_sid, $res_int['product_id'], $this->orderBy, $table_sid);
        $res_image = $this->basic_model->getRow('images', $table_sid, $res_int['product_id'], $this->orderBy, $table_sid);
        if ($res_int) {
            $outputTitle = "";
            $title = "";
            $count_qty = 0;
            if ($stock == 1) {
                $title = "Stock Detail";
                $count_qty = inventory_quantity_by_warehouse($res_int['product_id'], $res_int['warehouse_id']);
            } else {
                $title = "Inventory Detail";
                $count_qty = $res_int['inventory_quantity'];
            }
            $outputTitle .= $title;
            $current_img = '';

            $file_path = dir_path() . $this->image_upload_dir . '/' . @$res_image['image'];
            if (!is_dir($file_path) && file_exists($file_path)) {
                $current_img = site_url($this->image_upload_dir . '/' . $res_image['image']);
            } else {
                $current_img = site_url("uploads/dummy.jpg");
            }
            $outputDescription = "";
            $outputDescription .= "<p><br> <img src='" . $current_img . "' width='100px' /></p>";
            $outputDescription .= "<p><span style='font-weight:bold;'>PRODUCT NAME  : </span> " . $res_pro['product_name'] . "</p>";
                //Jd$outputDescription .= "<p><span style='font-weight:bold;'>PRODUCT MODEL NO  : </span> " . $res_pro['product_sku'] . "</p>";
            $outputDescription .= "<p><span style='font-weight:bold;'>QUANTITY : </span> " . $count_qty . "</p>";
            $outputDescription .= "<p><span style='font-weight:bold;'>LOCATION : </span> " . $res_war['warehouse_name'] . "</p>";
                //$outputDescription .= "<p>No. Of SHELF : ".$res_int['inventory_no_of_shelf']."</p>";
                //$outputDescription .= "<p>No. Of SECTION : ".$res_int['inventory_no_of_section']."</p>"; 
                //$outputDescription .= "<p>STATUS : ".(($res_int['inventory_is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";


            echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
        } else {
            echo json_encode(array('error' => 'Not Found ' . $this->page_heading . '.'));
        }
    } else {
        echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be deleted."));
    }
}

function warehouse_data()
{
    extract($_POST);
    $res = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  where warehouse_id=" . $id);
    echo json_encode($res);
}

function product_desc()
{
    extract($_POST);
    $res = $this->basic_model->getCustomRows("SELECT product_description FROM `product`  where `product_is_active` = 1  AND product_id=" . $id);
    echo json_encode($res[0]);
}

function print_inventory($id)
{
        //Jd $res = $this->basic_model->getCustomRows("SELECT p.*, i.*, w.*, w_from.warehouse_name as w_from,  inventory_receive.* from inventory_receive  
        //     Left JOIN `inventory` i 
        //      ON i.receipt_id = inventory_receive.id 
        //     Left JOIN `product` p 
        //      ON p.product_id = i.product_id
        //     Left JOIN `warehouse` w 
        //      ON w.warehouse_id = i.warehouse_id
        //     Left JOIN `warehouse`  w_from 
        //      ON w_from.warehouse_id = inventory_receive.warehouse_from
        //      WHERE `i`.`inventory_type` = 1 AND (inventory_receive.revision = 0 || inventory_receive.revision IS NUll) AND id=" . $id);


    $res = $this->basic_model->getCustomRows("SELECT p.*, i.*, w.*, w_from.warehouse_name as w_from,w.warehouse_name as w_to,inventory_receive.* from inventory_receive  
        Left JOIN `inventory` i 
        ON i.receipt_id = inventory_receive.id 
        Left JOIN `product` p 
        ON p.product_id = i.product_id
        Left JOIN `warehouse` w 
        ON w.warehouse_id = i.warehouse_id
        Left JOIN `warehouse`  w_from 
        ON w_from.warehouse_id = inventory_receive.warehouse_from
        WHERE `i`.`inventory_type` = 1  AND id=" . $id);

    $setval = $this->basic_model->getRow('setting', 'setting_id', 1, $this->orderBy, 'setting_id');
    // print_b($res);
    $this->basic_model->make_pdf(0, ['data' => $res, 'setval' => $setval], 'Report', 'inventory_Report');
        /*$mpdf = new \Mpdf\Mpdf();
        $content = $this->load->view('inventory_Report', ['data' => $res], true);
        //print_b($content);
        $mpdf->defaultheaderline = 0;
        $mpdf->defaultfooterline = 0;
        $mpdf->SetTitle("Inventory Report");
        $mpdf->AddPage('', // L - landscape, P - portrait 
        '', '', '', '',
        0, // margin_left
        0, // margin right
        40, // margin top
        0, // margin bottom
        0, // margin header
        0); // margin footer
        $mpdf->WriteHTML($content);
        $mpdf->Output();
        json_encode($res[0]);*/
    }

    public function test_excel()
    {
        extract($_GET);
        $file_data = array();
        $warehouse = $this->basic_model->getCustomRows("SELECT * FROM warehouse");
        $product = $this->basic_model->getCustomRows("SELECT * FROM product");
        // $data = $this->basic_model->getCustomRows("
        //         SELECT p.*, i.*, warehouse.*, 
        //         ( 
        //             (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND (i2.revision = 0 || i2.revision IS NUll) AND `i2`.`product_id` = `p`.`product_id` AND `i2`.`warehouse_id` = warehouse.warehouse_id 
        //             )  - 
        //             ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND (i3.revision = 0 || i3.revision IS NUll) AND `i3`.`product_id` = `p`.`product_id` AND `i3`.`warehouse_id` = warehouse.warehouse_id 
        //             ) 
        //         ) AS total_inventory_quantity FROM `product` `p` 
        //         Right JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` 
        //         Right JOIN `warehouse` ON i.warehouse_id = warehouse.warehouse_id 
        //         WHERE `p`.product_is_active = 1 AND (i.revision = 0 || i.revision IS NUll) ");
        $query = "SELECT p.*, i.*, warehouse.*, 
        ( 
        (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = `p`.`product_id` AND `i2`.`warehouse_id` = warehouse.warehouse_id 
        )  - 
        ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0  AND `i3`.`product_id` = `p`.`product_id` AND `i3`.`warehouse_id` = warehouse.warehouse_id 
        ) 
        ) AS total_inventory_quantity FROM `product` `p` 
        Right JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` 
        Right JOIN `warehouse` ON i.warehouse_id = warehouse.warehouse_id 
        WHERE `p`.product_is_active = 1  ";
        $where = "";
        if($product_id != 0)
        {
            $where .= "AND `p`.product_id = ".$product_id;
        }
        if($warehouse_id != 0)
        {
            $where .= "     AND `warehouse`.warehouse_id = ".$warehouse_id;
        }
        $data = $this->basic_model->getCustomRows($query.$where." group by `p`.product_id, `warehouse`.warehouse_id");
        // print_b($data);
        $count = 1;
        // foreach ($warehouse as $warehouse) {
        //     foreach ($product as $products) {
                foreach (@$data as $v) {
                    // if ($v['total_inventory_quantity'] > 0) {
                    // if ($warehouse['warehouse_id'] == $v['warehouse_id'] && $products['product_id'] == $v['product_id']) {
                        $temp_data = array(
                            "1" => $count,
                            "2" => $v['product_sku'],
                            "3" => $v['product_name'],
                            "4" => $v['total_inventory_quantity'],
                            "5" => $v['warehouse_name'],
                        );
                        array_push($file_data, $temp_data);
                        $count++;
                        // break;
                    // }
                    // }
                }
        //     }
        // }
        $header = ['Sr No','SKU', 'Product Name', 'Quantity', 'Location'];
        $title = 'Current-Stock';
        $file_name = 'Current-Stock';

        $this->basic_model->customExportExcel($header, $file_data, $title, $file_name);
    }


    public function export_to_excel_stock_transaction($productid, $address, $fromdate, $todate)
    {
     $res["product_id"] = $productid;
     $res["address"] = $address;
     $res["from_date"] = $fromdate;
     $res["to_date"] = $todate;



     $res['previous_balance'] = $this->basic_model->getCustomRows("Select  SUM( CASE WHEN(inventory_type = 1) THEN `inventory_quantity` ELSE 0 END) AS InQuantity, SUM( CASE WHEN (inventory_type = 0) THEN `inventory_quantity` ELSE 0 END ) AS OutQuantity , SUM( CASE WHEN (inventory_type = 1) THEN `inventory_quantity` ELSE 0 END ) - SUM( CASE WHEN (inventory_type = 0) THEN `inventory_quantity` ELSE 0 END ) AS Balance  From inventory i JOIN warehouse ON warehouse.warehouse_id = i.warehouse_id JOIN product p ON i.product_id = p.product_id WHERE  p.product_id = ".$productid." AND (warehouse.`warehouse_address` LIKE '%".$address."%' OR '-1' = '".$address."')  AND (i.doc_create_date < '".$fromdate."') and `p`.product_is_active = 1  order by i.create_date ASC,i.product_id DESC, i.warehouse_id DESC");
     $balance = (($res['previous_balance'][0]["Balance"]) == "") ? "0" : $res['previous_balance'][0]["Balance"];
     $res['data'] = $this->basic_model->getCustomRows("Select *, i.create_date as inventory_create_date From inventory i JOIN warehouse ON warehouse.warehouse_id = i.warehouse_id JOIN product p ON i.product_id = p.product_id WHERE  p.product_id = ".$productid." AND (warehouse.`warehouse_address` LIKE '%".$address."%' OR '-1' = '".$address."')  AND (i.doc_create_date >= '".$fromdate."' AND i.`doc_create_date` <= '".$todate."') and `p`.product_is_active = 1  order by i.create_date ASC,i.product_id DESC, i.warehouse_id DESC");

     $header = ['Sr No', 'Date', 'Product Name', 'Location', 'In', 'Out', 'Balance'];
     $title = 'Stock-Transactions';
     $file_name = 'Stock-Transactions';
     $file_data = array();
     $count = 0;

     $temp_data = array(
        "1" => "",
        "2" => "",
        "3" => "",
        "4" => "",
        "5" => "",
        "6" => "",
        "7" => $balance,
    );
     array_push($file_data, $temp_data);

     foreach ($res['data'] as $item) {
         if($item['inventory_type'] == 1)
         {
            $balance = $balance + $item['inventory_quantity'];
        }
        else{
            $balance = $balance - $item['inventory_quantity'];
        }
        $temp_data = array(
            "1" => $count + 1,
            "2" => dt_format($item['doc_create_date']),
            "3" => $item['product_name'],
            "4" => $item['warehouse_name'],
            "5" => ($item['inventory_type'] == 1) ? $item['inventory_quantity'] : '0',
            "6" => ($item['inventory_type'] == 1) ? '0' : $item['inventory_quantity'],
            "7" => $balance,
        );
        array_push($file_data, $temp_data);
        $count++;



    }
    $this->basic_model->customExportExcel($header, $file_data, $title, $file_name);
}



public function current_stock_detail($id)
{

    if( isset($id) && $id != '')
    {
        $setval = $this->basic_model->getRow('setting','setting_id',1,'DESC','setting_id');
        $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
        if($res['receipt_id'] != NULL)
        {
            $data = $this->basic_model->getRow('inventory_receive', 'id', $res['receipt_id'], $this->orderBy, 'id');
            if($data)
            {
                $outputTitle = "";
                $outputTitle .= "Material Recieve Detail";

                $outputDescription = "";
                $outputDescription .= "<p><b>Receipt No</b> : ".$data['receipt_no']."</p>";
                $outputDescription .= "<p><b>Note</b> : ".$data['note']."</p>";
                $outputDescription .= "<p><b>Date</b> : ".dt_format($data['shipment_date'])."</p>";
                $outputDescription .= "<p><b>Shipment Type</b> : ".$data['shipment_type']."</p>";
                if($data['shipment_type'] == 'transfer')
                {
                    $warehouse = $this->basic_model->getRow('warehouse', 'warehouse_id', $data['warehouse_from'], $this->orderBy, 'warehouse_id');
                    $outputDescription .= "<p><b>From Warehouse</b> : ".$warehouse['warehouse_name']."</p>";
                }

                   //Jd $detail = $this->basic_model->getCustomRows('SELECT w.*,p.*,inventory_receive.*, inventory.* from inventory left JOIN warehouse w on w.warehouse_id = inventory.warehouse_id left JOIN product p on p.product_id = inventory.product_id Left JOIN inventory_receive on inventory.receipt_id = inventory_receive.id where inventory.receipt_id = ' . $res['receipt_id'] . " AND `inventory`.`inventory_type` = 1 AND (inventory.revision = 0 || inventory.revision IS NUll)");

                $detail = $this->basic_model->getCustomRows('SELECT w.*,p.*,inventory_receive.*, inventory.* from inventory left JOIN warehouse w on w.warehouse_id = inventory.warehouse_id left JOIN product p on p.product_id = inventory.product_id Left JOIN inventory_receive on inventory.receipt_id = inventory_receive.id where inventory.receipt_id = ' . $res['receipt_id'] . " AND `inventory`.`inventory_type` = 1 ");
                if (@$detail != null && !empty(@$detail)) {
                    $count =1;
                    foreach ($detail as $k => $v) {
                        $outputDescription .= "<p>".$count.". <b>Product Name :</b> ".$v['product_name']."</p>";
                        $outputDescription .= "<p><b>Location</b> : ".$v['warehouse_name']."</p>";
                        $outputDescription .= "<p><b>Quantity</b> : ".$v['inventory_quantity']."</p>";
                        $count++;
                    }
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }
        }
        else if ($res['dn_id'] != NULL)
        {
            $data = $this->basic_model->getRow('delivery_note', 'delivery_note_id', $res['dn_id'], $this->orderBy, 'delivery_note_id');
            if($data)
            {
                $outputTitle = "";
                $outputTitle .= "Delivery Note Detail";

                $outputDescription = "";
                $outputDescription .= "<p><b>Delivery Note No</b> : ".$setval["company_prefix"] . @$setval["delivery_prfx"] .$data['delivery_note_no']."</p>";
                $outputDescription .= "<p><b>Delivery Date</b> : ".dt_format($data['delivery_date'])."</p>";
                $outputDescription .= "<p><b>Note</b> : ".$data['delivery_notes']."</p>";
                if ($data['customer_id'] != NULL)
                {
                    $customer_data = $this->basic_model->getRow('user', 'user_id', $data['customer_id'], $this->orderBy, 'user_id');
                    $outputDescription .= "<p><b>Customer</b> : ".$customer_data['user_name']."</p>";
                }
                if ($data['employee_id'] != NULL)
                {
                    $employee_data = $this->basic_model->getRow('user', 'user_id', $data['employee_id'], $this->orderBy, 'user_id');
                    $outputDescription .= "<p><b>Employee</b> : ".$employee_data['user_name']."</p>";
                }

                $detail = $this->basic_model->getCustomRows("SELECT * FROM delivery_note_detail dnd LEFT JOIN product p ON p.product_id = dnd.product_id LEFT JOIN warehouse w ON w.warehouse_id = dnd.warehouse_id WHERE dnd.delivery_note_id = '".$data['delivery_note_id']."' ");
                if (@$detail != null && !empty(@$detail)) {
                    $count =1;
                    foreach ($detail as $k => $v) {
                        $outputDescription .= "<p>".$count.". <b>Product Name :</b> ".$v['product_name']."</p>";
                        $outputDescription .= "<p><b>Location</b> : ".$v['warehouse_name']."</p>";
                        $outputDescription .= "<p><b>Quantity</b> : ".$v['delivery_note_detail_quantity']."</p>";
                        $outputDescription .= "<p><b>Pending Quantity</b> : ".$v['delivery_note_detail_pending']."</p>";
                        $outputDescription .= "<p><b>Delivered Quantity</b> : ".$v['delivery_note_detail_delivered']."</p>";
                        $outputDescription .= "<p><b>Total Quantity</b> : ".$v['delivery_note_detail_total_quantity']."</p>";
                        $count++;
                    }
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }
        }
        else if ($res['min_id'] != NULL)
        {
            $data = $this->basic_model->getRow('material_issue_note', 'material_issue_note_id', $res['min_id'], $this->orderBy, 'material_issue_note_id');
            if($data)
            {
                $outputTitle = "";
                $outputTitle .= "Material Issue Note Detail";

                $outputDescription = "";
                $outputDescription .= "<p><b>Material Issue Note No</b> : ".$setval["company_prefix"] . @$setval["material_issue_prefix"] .$data['material_issue_note_no']."</p>";
                $outputDescription .= "<p><b>Material Issue Date</b> : ".dt_format($data['material_issue_note_date'])."</p>";
                $outputDescription .= "<p><b>Terms & Condition</b> : ".$data['material_issue_note_terms_conditions']."</p>";

                if ($data['customer_id'] != NULL)
                {
                    $customer_data = $this->basic_model->getRow('user', 'user_id', $data['customer_id'], $this->orderBy, 'user_id');
                    $outputDescription .= "<p><b>Customer</b> : ".$customer_data['user_name']."</p>";
                }
                if ($data['employee_id'] != NULL)
                {
                    $employee_data = $this->basic_model->getRow('user', 'user_id', $data['employee_id'], $this->orderBy, 'user_id');
                    $outputDescription .= "<p><b>Employee</b> : ".$employee_data['user_name']."</p>";
                }

                $detail = $this->basic_model->getCustomRows("SELECT * FROM material_issue_note_detail dnd LEFT JOIN product p ON p.product_id = dnd.product_id LEFT JOIN warehouse w ON w.warehouse_id = dnd.material_issue_note_detail_warehouse_id WHERE dnd.material_issue_note_id = '".$data['material_issue_note_id']."' ");
                if (@$detail != null && !empty(@$detail)) {
                    $count =1;
                    foreach ($detail as $k => $v) {
                        $outputDescription .= "<p>".$count.". <b>Product Name :</b> ".$v['product_name']."</p>";
                        $outputDescription .= "<p><b>Location</b> : ".$v['warehouse_name']."</p>";
                        $outputDescription .= "<p><b>Quantity</b> : ".$v['material_issue_note_detail_quantity']."</p>";
                        $outputDescription .= "<p><b>Description</b> : ".$v['material_issue_note_detail_description']."</p>";
                        $count++;
                    }
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }
        }
        else{
            echo json_encode(array( "error" => $this->page_heading." id not found."));
        }
    }
    else{
        echo json_encode(array( "error" => $this->page_heading." id not found."));
    }
}
}
