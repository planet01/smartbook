<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Scheduling extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->view_product = 'Scheduling/view';
        $this->add_product = 'Scheduling/add';
        $this->edit_product = 'Scheduling/edit';
        $this->detail_product = 'Scheduling/detail';
        // page active
        $this->view_page_product = "view-scheduling";
        $this->add_page_product = "add-scheduling";
        // table
        $this->table = "schedule";
        $this->table_pid = "schedule_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/warehouse";
        // Page Heading
        $this->page_heading = "Scheduling";
		
        $this->no_results_found = "user/no_results_found";
        
		require_once APPPATH .'third_party/FileUploader.php';

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 34) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }

    function index(){
        if ($this->check_view) {
            redirect($this->view_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function view() {
        if ($this->check_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." s JOIN user u ON s.standby_employee = u.user_id ORDER BY s.".$this->table_pid." ".$this->orderBy." limit 10");
            $this->template_view($this->view_page_product,$res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE ";

            if($from_date != 0)
            {
                $where .= "STR_TO_DATE(schedule_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= "STR_TO_DATE(schedule_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            $where .= " ORDER BY ".$this->table_pid." DESC";
            $query = "SELECT * FROM ".$this->table." ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);
            echo json_encode($res);
        }
    }

    function add(){
        extract($_POST);
        // print_b($_POST);
        if( $this->input->post()){ 
            if(empty($id)){
                if( empty($schedule_date) || empty($standby_employee)){
                    echo json_encode(array("error" => "Please fill all fields "));
                }
                else{

                    $Check_date = $this->basic_model->getRow($this->table, 'schedule_date', $schedule_date,$this->orderBy,$this->table_pid);
                    if(@$Check_date != null && !empty(@$Check_date)){
                        echo json_encode(array("error" => "Schedule already Exist"));
                        exit();
                    }
                    
                    $data = array(
                        'schedule_date' => $schedule_date,
                        'standby_employee' => $standby_employee,
                        'schedule_note' => $schedule_note,
                    );
                    $this->basic_model->insertRecord($data, $this->table);
 				  
                    echo json_encode(array( "success" => "Record added successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
            else{
              
                if( empty($schedule_date) || empty($standby_employee)){
                    echo json_encode(array("error" => "Please fill all fields "));
                }
                else{

                    $Check_date = $this->basic_model->getRow($this->table, 'schedule_date', $schedule_date,$this->orderBy,$this->table_pid);
                    if(@$Check_date != null && !empty(@$Check_date)){
                        if ($Check_date['schedule_date'] != $schedule_date_old) {
                            echo json_encode(array("error" => "Invoice No. is Exist"));
                            exit();
                        }
                    }

                    $data = array(
                        'schedule_date' => $schedule_date,
                        'standby_employee' => $standby_employee,
                        'schedule_note' => $schedule_note,
                    );
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
        }
        else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res["ckeditor"] = "yes";
                $res['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1"); 
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function fetch_tickets()
    {
        extract($_POST);
        if($this->input->post())
        {
            $query = "SELECT t.*,u.user_name as customer_name,u2.user_name as employee_name FROM tickets t LEFT JOIN user u2 ON t.assigned_to = u2.user_id LEFT JOIN user u ON t.customer_id = u.user_id WHERE STR_TO_DATE(t.ticket_date,'%Y-%m-%d') = STR_TO_DATE('$schedule_date','%Y-%m-%d')";
            // if($employee_id != '' || !empty($employee_id))
            // {
            //     $query .= " AND t.assigned_to = '".$employee_id."'";
            // }
            $temp_tickets = $this->basic_model->getCustomRows($query);
            $tickets = array();
            foreach($temp_tickets as $k => $v)
            {
                $doc = '';
                if($v['type'] ==  0) //project
                {
                    $doc = $this->basic_model->getCustomRow("SELECT * FROM projects where project_id = ".$v['doc_id'])['project_name'];
                }else //invoice
                {
                    $doc = $v['invoice_no'];
                }
                
                $ticket_detail = $this->basic_model->getCustomRows("SELECT * FROM ticket_task WHERE ticket_id = ".$v['ticket_id']);
                $v['ticket_detail'] = $ticket_detail;
                $v['doc'] = $doc;
                array_push($tickets,$v);
            }
            $res['data'] = $tickets;
            echo json_encode($res);
        }   
    }

    function edit($id){
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "Edit";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->add_page_product;
                    $res["add_product"] = $this->add_product;
                    $res["ckeditor"] = "yes";
                    $res['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1"); 
                    $res["data"] = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." WHERE ".$this->table_pid." = ".$id);
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getCustomRow("SELECT s.*,u.user_name FROM ".$this->table." s JOIN user u ON s.standby_employee = u.user_id WHERE s.".$this->table_pid." = ".$id);
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";
                $outputDescription = "";
                $outputDescription .= "<p><span style='font-weight:bold;'>Scehedule Date : </span> ".date("d-M-Y", strtotime($res["schedule_date"]))."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Stand By Employee : </span> ".$res["user_name"]."</p>";
                $outputDescription .= $res['schedule_note'] ;
                
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

}