<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class material_issue_note extends MY_Back_Controller
{
    public function __construct()
    {
        parent::__construct();
        // page action
        //$this->add_product = 'material_issue_note/add';
        $this->save_product = 'material_issue_note/add';

        $this->view_product = 'material_issue_note/view';

        $this->edit_product = 'material_issue_note/edit';
        $this->detail_page = 'material_issue_note/detail_page';
        $this->print_product = 'material_issue_note/print';


        $this->delete_product = 'material_issue_note/delete';
        $this->detail_product = 'material_issue_note/detail';
        // page active
        $this->add_page_product = "add-material_issue_note";
        $this->edit_page_product = "edit-material_issue_note";
        $this->view_page_product = "view-material_issue_note";
        // table
        $this->table = "material_issue_note";
        $this->table_detail = "material_issue_note_detail";
        $this->tablep = "product";
        $this->tables = "setting";

        $this->table_id = "material_issue_note_id";
        $this->table_pid = "product_id";
        $this->table_fid = "material_issue_note_id";
        $this->table_sid = "setting_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // Page Heading
        $this->page_heading = "Material Issue Note";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index()
    {
        redirect($this->add_product);
    }

    function add()
    {
        extract($_POST);
        if ($this->input->post()) {
            if (empty($material_issue_note_no) || empty($material_issue_note_date)) {
                echo json_encode(array("error" => "Please fill all fields"));
                return;
            }
            if (!empty($id)) {
                $this->basic_model->customQueryDel("Delete from `material_issue_note_detail` WHERE `material_issue_note_id` = '" . $id . "'");
                $this->basic_model->customQueryDel("Delete from `material_issue_note` WHERE `material_issue_note_id` = '" . $id . "'");
                $this->basic_model->customQueryDel("Delete from `inventory` WHERE `min_id` = '" . $id . "'");
            }
            if (isset($material_issue_note_detail_quantity) && @$material_issue_note_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                // product_check start
                $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                $product_check = array();
                // print_b($product_data);
                $productCount = 0;
                $quantityTotal_new = 0;
                if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                    foreach ($product_data['duplicate'] as $k => $v) {
                        $product_id_new = $v;
                        $quantityTotal_new += $material_issue_note_detail_quantity[$k];
                    }
                    $product_check[$productCount] = array(
                        'product_id' => $product_id_new,
                        'material_issue_note_detail_quantity' => $quantityTotal_new,
                    );
                }



                if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                    foreach ($product_data['unique'] as $k => $v) {
                        $productCount++;
                        $product_check[$productCount] = array(
                            'product_id' => $v,
                            'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k],
                        );
                    }
                }


                // product_check end.
            }

            $material_date_year = explode("-", $material_issue_note_date)[0];
            $serial_no = serial_no_material_issue_note($material_date_year);
            // print_b($serial_no);

            $data = array(
                'employee_id' => (@$employee_id == 0) ? null : @$employee_id,
                'customer_id' => (@$customer_id == 0) ? null : @$customer_id,
                'material_issue_note_no' => $serial_no,
                'material_issue_note_customer_note' => $material_issue_note_customer_note,
                'material_issue_note_date' => $material_issue_note_date,
                // 'material_issue_note_terms_conditions' => $material_issue_note_terms_conditions
            );
            $material_issue_note_id = $this->basic_model->insertRecord($data, $this->table);

            if (isset($material_issue_note_detail_quantity) && @$material_issue_note_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                $i = 0;
                foreach ($material_issue_note_detail_quantity as $k => $v) {
                    if ($material_issue_note_detail_quantity[$k] > 0) {
                        // out from inventory_quantity
                        $arr2 = array(
                            'product_id' => $product_id[$k],
                            'inventory_quantity' => $material_issue_note_detail_quantity[$k],
                            'warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                            'inventory_is_active' => 1,
                            'inventory_type' => 0,
                            'min_id' => $material_issue_note_id,
                            'doc_create_date' => $material_issue_note_date
                        );

                        $inventory_id = $this->basic_model->insertRecord($arr2, 'inventory');

                        $arr = array(
                            'product_id' => $product_id[$k],
                            'material_issue_note_id' => $material_issue_note_id,
                            'inventory_id' => $inventory_id,
                            'material_issue_note_detail_warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                            //'material_issue_note_detail_description' => $material_issue_note_detail_description[$k],
                            'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k]
                        );
                        $i++;

                        // echo '<pre>';
                        // print_r($arr);
                        $material_issue_note_detail_id = $this->basic_model->insertRecord($arr, $this->table_detail);
                        //$this->stock_notification();
                    }
                }
            }

            echo json_encode(array("success" => "Record Inserted successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
        } else {
            $data['title'] = "Add New " . $this->page_heading;
            $data["page_title"] = "add";
            $data["page_heading"] = $this->page_heading;
            $data['active'] = $this->add_page_product;
            // $data["add_product"] = $this->add_product;
            $data["save_product"] = $this->save_product;
            $data['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1");
            $data['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user.user_type = 3 AND user.`user_is_active` = 1 ");
            $data['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 ");
            $data["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
            /* $data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 GROUP BY p.product_id ORDER BY p.product_name ASC"); */
            $data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p where p.product_is_active = 1 AND p.category_type = 2 GROUP BY p.product_id ORDER BY p.product_name ASC");

            $this->template_view($this->add_page_product, $data);
        }
    }

    function add_old()
    {
        extract($_POST);
        if ($this->input->post()) {
            // print_b($_POST);
            if (empty($id)) {
                if (empty($material_issue_note_no)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else {

                    /*$Checkmaterial_issue_note_no = $this->basic_model->getRow($this->table, 'material_issue_note_no', $material_issue_note_no,$this->orderBy,$this->table_id);
                    if(@$Checkmaterial_issue_note_no != null && !empty(@$Checkmaterial_issue_note_no)){
                        echo json_encode(array("error" => $this->page_heading." No. is Exist"));
                        exit();
                    }*/

                    if (isset($material_issue_note_detail_quantity) && @$material_issue_note_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $material_issue_note_detail_quantity[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'material_issue_note_detail_quantity' => $quantityTotal_new,
                            );
                        }



                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k],
                                );
                            }
                        }

                        /* foreach ($product_check as $k => $v) {
                            $q = "SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1 GROUP BY inventory.inventory_id";
                            

                            $inventory_quantity_data = $this->basic_model->getCustomRow($q);
                            if ($inventory_quantity_data['total_inventory_quantity'] <= $v['material_issue_note_detail_quantity']) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                            }
                            break;
                        }*/

                        // product_check end.
                    }

                    $material_date_year = explode("-", $material_issue_note_date)[2];
                    $serial_no = serial_no_material_issue_note($material_date_year);
                    // print_b($serial_no);

                    $data = array(
                        'employee_id' => (@$employee_id == 0) ? null : @$employee_id,
                        'customer_id' => (@$customer_id == 0) ? null : @$customer_id,
                        'material_issue_note_no' => $serial_no,
                        'material_issue_note_customer_note' => $material_issue_note_customer_note,
                        'material_issue_note_date' => $material_issue_note_date,
                        // 'material_issue_note_terms_conditions' => $material_issue_note_terms_conditions
                    );
                    $material_issue_note_id = $this->basic_model->insertRecord($data, $this->table);

                    if (isset($material_issue_note_detail_quantity) && @$material_issue_note_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        $i = 0;
                        foreach ($material_issue_note_detail_quantity as $k => $v) {
                            if ($material_issue_note_detail_quantity[$k] > 0) {
                                // out from inventory_quantity
                                $arr2 = array(
                                    'product_id' => $product_id[$k],
                                    'inventory_quantity' => $material_issue_note_detail_quantity[$k],
                                    'warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                    'inventory_is_active' => 1,
                                    'inventory_type' => 0,
                                    'min_id' => $material_issue_note_id,
                                    'doc_create_date' => $material_issue_note_date
                                );

                                $inventory_id = $this->basic_model->insertRecord($arr2, 'inventory');

                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'material_issue_note_id' => $material_issue_note_id,
                                    'inventory_id' => $inventory_id,
                                    'material_issue_note_detail_warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                    //'material_issue_note_detail_description' => $material_issue_note_detail_description[$k],
                                    'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k]
                                );
                                $i++;

                                // echo '<pre>';
                                // print_r($arr);
                                $material_issue_note_detail_id = $this->basic_model->insertRecord($arr, $this->table_detail);
                                //$this->stock_notification();
                            }
                        }
                    }

                    echo json_encode(array("success" => "Record Inserted successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            } else {
                if (empty($material_issue_note_no)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else {

                    $Checkmaterial_issue_note_no = $this->basic_model->getRow($this->table, 'material_issue_note_no', $material_issue_note_no, $this->orderBy, $this->table_id);
                    if (@$Checkmaterial_issue_note_no != null && !empty(@$Checkmaterial_issue_note_no)) {
                        if ($Checkmaterial_issue_note_no['material_issue_note_no'] != $material_issue_note_no_old) {
                            echo json_encode(array("error" => "material_issue_note No. is Exist"));
                            exit();
                        }
                    }

                    $material_issue_note_detail_data = $this->basic_model->getRows("material_issue_note_detail", "material_issue_note_id", $id, "material_issue_note_id", $this->orderBy);
                    // print_b($material_issue_note_detail_data);
                    // if (isset($material_issue_note_detail_data) && @$material_issue_note_detail_data != null) {
                    //     foreach ($material_issue_note_detail_data as $k => $v) {
                    //         $del = $this->basic_model->deleteRecord('inventory_id', $v['inventory_id'], 'inventory');

                    //     }
                    // }

                    // $del = $this->basic_model->deleteRecord('material_issue_note_id', $id, 'material_issue_note_detail');


                    if (isset($material_issue_note_detail_quantity) && @$material_issue_note_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $material_issue_note_detail_quantity[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'material_issue_note_detail_quantity' => $quantityTotal_new,
                            );
                        }



                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k],
                                );
                            }
                        }

                        /*foreach ($product_check as $k => $v) {
                            $q = "SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1";

                            $inventory_quantity_data = $this->basic_model->getCustomRow($q);

                            if ($inventory_quantity_data['total_inventory_quantity'] < $v['material_issue_note_detail_quantity']) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                            }
                            break;
                        }*/

                        // product_check end.
                    }


                    $data = array(
                        'employee_id' => (@$employee_id == 0) ? null : @$employee_id,
                        'customer_id' => (@$customer_id == 0) ? null : @$customer_id,
                        'material_issue_note_no' => $material_issue_note_no,
                        'material_issue_note_customer_note' => $material_issue_note_customer_note,
                        'material_issue_note_date' => $material_issue_note_date,
                        // 'material_issue_note_terms_conditions' => $material_issue_note_terms_conditions
                    );

                    // if (isset($material_issue_note_detail_quantity) && @$material_issue_note_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                    //     $i = 0;
                    //     foreach ($material_issue_note_detail_quantity as $k => $v) {
                    //         if ($material_issue_note_detail_quantity[$k]>0) {
                    //             // out from inventory_quantity
                    //             $arr2 = array(
                    //                 'product_id' => $product_id[$k],
                    //                 'inventory_quantity' => $material_issue_note_detail_quantity[$k],
                    //                 'warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                    //                 'inventory_is_active' => 1,
                    //                 'inventory_type' => 0,
                    //                 'min_id' => $id
                    //             );

                    //             $inventory_id = $this->basic_model->insertRecord($arr2,'inventory');

                    //             $arr = array(
                    //                 'product_id' => $product_id[$k],
                    //                 'material_issue_note_id' => $id,
                    //                 'inventory_id' => $inventory_id,
                    //                 'material_issue_note_detail_warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                    //                 //'material_issue_note_detail_description' => $material_issue_note_detail_description[$k],
                    //                 'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k]
                    //             );
                    //             $i++;

                    //             // echo '<pre>';
                    //             // print_r($arr);
                    //             $material_issue_note_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail);
                    //         }
                    //     }
                    // }
                    // print_b($arr);


                    foreach ($material_issue_note_detail_quantity as $k => $v) {
                        if (isset($material_issue_note_detail_data) && @$material_issue_note_detail_data != null) {
                            foreach ($material_issue_note_detail_data as $k1 => $v1) {
                                if ($v1['product_id'] == $product_id[$k] && $v1['material_issue_note_detail_warehouse_id'] == $material_issue_note_detail_warehouse_id[$k]) {
                                    $arr2 = array(
                                        'product_id' => $product_id[$k],
                                        'inventory_quantity' => $material_issue_note_detail_quantity[$k],
                                        'warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                        'inventory_is_active' => 1,
                                        'inventory_type' => 0,
                                        'min_id' => $id,
                                        'doc_create_date' => $material_issue_note_date
                                    );
                                    $this->basic_model->updateRecord($arr2, 'inventory', 'inventory_id', $v1['inventory_id']);

                                    $arr = array(
                                        'product_id' => $product_id[$k],
                                        'material_issue_note_id' => $id,
                                        'material_issue_note_detail_warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                        //'material_issue_note_detail_description' => $material_issue_note_detail_description[$k],
                                        'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k]
                                    );
                                    $this->basic_model->updateRecord($arr, $this->table_detail, 'material_issue_note_detail_id', $v1['material_issue_note_detail_id']);
                                } else {
                                    $arr2 = array(
                                        'product_id' => $product_id[$k],
                                        'inventory_quantity' => $material_issue_note_detail_quantity[$k],
                                        'warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                        'inventory_is_active' => 1,
                                        'inventory_type' => 0,
                                        'min_id' => $id,
                                        'doc_create_date' => $material_issue_note_date
                                    );

                                    $inventory_id = $this->basic_model->insertRecord($arr2, 'inventory');

                                    $arr = array(
                                        'product_id' => $product_id[$k],
                                        'material_issue_note_id' => $id,
                                        'inventory_id' => $inventory_id,
                                        'material_issue_note_detail_warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                        //'material_issue_note_detail_description' => $material_issue_note_detail_description[$k],
                                        'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k]
                                    );

                                    // echo '<pre>';
                                    // print_r($arr);
                                    $material_issue_note_detail_id = $this->basic_model->insertRecord($arr, $this->table_detail);
                                }
                            }
                        } else {
                            $arr2 = array(
                                'product_id' => $product_id[$k],
                                'inventory_quantity' => $material_issue_note_detail_quantity[$k],
                                'warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                'inventory_is_active' => 1,
                                'inventory_type' => 0,
                                'min_id' => $id,
                                'doc_create_date' => $material_issue_note_date
                            );

                            $inventory_id = $this->basic_model->insertRecord($arr2, 'inventory');

                            $arr = array(
                                'product_id' => $product_id[$k],
                                'material_issue_note_id' => $id,
                                'inventory_id' => $inventory_id,
                                'material_issue_note_detail_warehouse_id' => $material_issue_note_detail_warehouse_id[$k],
                                //'material_issue_note_detail_description' => $material_issue_note_detail_description[$k],
                                'material_issue_note_detail_quantity' => $material_issue_note_detail_quantity[$k]
                            );

                            // echo '<pre>';
                            // print_r($arr);
                            $material_issue_note_detail_id = $this->basic_model->insertRecord($arr, $this->table_detail);
                        }
                    }

                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                    echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        } else {
            $data['title'] = "Add New " . $this->page_heading;
            $data["page_title"] = "add";
            $data["page_heading"] = $this->page_heading;
            $data['active'] = $this->add_page_product;
            // $data["add_product"] = $this->add_product;
            $data["save_product"] = $this->save_product;
            $data['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1");
            $data['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user.user_type = 3 AND user.`user_is_active` = 1 ");
            $data['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 ");
            $data["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
            /* $data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 GROUP BY p.product_id ORDER BY p.product_name ASC"); */
            $data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p where p.product_is_active = 1 AND p.category_type = 2 GROUP BY p.product_id ORDER BY p.product_name ASC");

            $this->template_view($this->add_page_product, $data);
        }
    }

    function progt()
    {
        if ($_POST['pid']) {
            $data = array();
            // $this->tablep,$this->table_pid,$_POST['pid'],$this->orderBy,$this->table_pid
            $data = $this->basic_model->getCustomRow("SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '" . $_POST['pid'] . "')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '" . $_POST['pid'] . "' AND product_is_active = 1 GROUP By  `inventory`.`inventory_id`");

            echo json_encode($data);
        } else {
            echo 0;
        }
    }

    //   function addAssginOrder()
    //     {


    // //           foreach ($_POST['quantity'] as $key => $value) 
    // //           {


    // //               $Productname = $this->input->post('Productname')[$key];
    // //               $description = $this->input->post('description')[$key];
    // //               $quantity = $this->input->post('quantity')[$key]; 
    // //               $rate = $this->input->post('rate')[$key];  
    // //               $amount = $this->input->post('amount')[$key];  

    // //               $data= array(
    // //                       'product_id' =>$Productname , 
    // //                       'description' =>$description ,
    // //                       'quantity' =>$quantity ,
    // //                       'rate' =>$rate , 
    // //                       'amount' =>$amount,

    // //                   );

    // //               $data['customer_id'] = $this->input->post('customer');
    // //               $data['material_issue_note_no'] = $this->input->post('material_issue_note_no');

    // //               $res = $this->basic_model->getRow($this->tables,$this->table_sid,1);

    // //               $data['material_issue_note_no'] = $res["material_issue_note_prfx"].''.$data['material_issue_note_no'];


    // //               $data['material_issue_note_date'] = $this->input->post('material_issue_note_date');
    // //               $data['expiry_date'] = $this->input->post('expiry_date');
    // //               $data['customer_note'] = $this->input->post('customer_note');
    // //               $data['terms_conditions'] = $this->input->post('terms_conditions');
    // //               $data['total'] = $this->input->post('total');

    // //                   $old_date_timestamp = strtotime($data['material_issue_note_date']);
    // //                   $data['material_issue_note_date'] = date('Y-m-d', $old_date_timestamp);   

    // //                   $old_date_timestamp2 = strtotime($data['expiry_date']);
    // //                   $data['expiry_date'] = date('Y-m-d', $old_date_timestamp2);   

    // //             $id = $this->basic_model->insertRecord($data,$this->table);

    // // //         print_b($data);

    // //           }
    // //                   echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
    // //                    // echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));

    // //      }
    //      }

    function edit($id)
    {
        $material_issue_note_edit = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 7) {
                    if ($v['edit'] == 1) {
                        $material_issue_note_edit = true;
                    }
                }
            }
        } else {
            $material_issue_note_edit = true;
        }
        if ($material_issue_note_edit) {
            $this->edit_data($id, 'edit');
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail_page($id)
    {
        $this->edit_data($id, 'view');
    }

    function edit_data($id, $view_data)
    {
        if (isset($id) && $id != '') {
            $res_edit = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
            if ($res_edit) {
                extract($_POST);

                $res["title"] = "Edit " . $this->page_heading;
                $res["page_title"] =  "";
                $res["page_heading"] = $this->page_heading;
                $res["active"] = $this->edit_page_product;
                $res["save_product"] =  $this->save_product;
                // $res['image_upload_dir'] = $this->image_upload_dir;
                // $res['countries'] = $this->basic_model->countries;
                $res["data"] = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
                $res["material_issue_note_detail_data"] = $this->basic_model->getRows($this->table_detail, $this->table_fid, $res["data"]['material_issue_note_id'], "material_issue_note_detail_id", "ASC");

                /* $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p RIGHT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND p.product_is_active = 1 GROUP BY p.product_id ORDER BY p.product_name ASC");*/
                $res['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
                $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user.user_type = 3 AND user.`user_is_active` = 1 ");
                $res["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
                $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `warehouse_is_active` = 1 ");
                $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p where p.product_is_active = 1 AND p.category_type = 2 GROUP BY p.product_id ORDER BY p.product_name ASC");

                if ($view_data == 'edit') {
                    $this->template_view($this->add_page_product, $res);
                } else {
                    $res['view_page'] = $this->view_product;
                    $res["title"] = "View " . $this->page_heading;
                    $res['is_detail'] = true;
                    $res['disable'] = 'disabled';
                    $this->template_view($this->add_page_product, $res);

                    //$this->template_view($this->view_detail_product,$res);  
                }
            } else {
                redirect($this->no_results_found);
            }
        } else {
            redirect($this->no_results_found);
        }
    }

    function view()
    {

        $material_issue_note_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 7) {
                    if ($v['visible'] == 1) {
                        $material_issue_note_view = true;
                    }
                }
            }
        } else {
            $material_issue_note_view = true;
        }

        if ($material_issue_note_view) {
            $res['title'] = "View All " . $this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["save_product"] = $this->save_product;
            $res["edit_product"] = $this->edit_product;
            $res["detail_page"] = $this->detail_page;
            $res["print_product"] = $this->print_product;

            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product;
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res['data'] = $this->basic_model->getCustomRows("
            SELECT material_issue_note.*,user.user_name,cu.user_name as customer_name
            FROM `material_issue_note` 
            Left JOIN `user` cu ON material_issue_note.customer_id = cu.user_id 
            Left JOIN `user` ON material_issue_note.employee_id = user.user_id 
            order By material_issue_note.material_issue_note_id desc limit 100");
            $res["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
            // print_b($res['data']);

            $this->template_view($this->view_page_product, $res);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function search_view(){
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE material_issue_note.material_issue_note_id != 0 ";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(material_issue_note.material_issue_note_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(material_issue_note.material_issue_note_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }
            

            $where .= " order By material_issue_note.material_issue_note_id desc limit 100";
            $query = "SELECT material_issue_note.*,user.user_name,cu.user_name as customer_name
            FROM `material_issue_note` 
            Left JOIN `user` cu ON material_issue_note.customer_id = cu.user_id 
            Left JOIN `user` ON material_issue_note.employee_id = user.user_id ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);

            
             
            echo json_encode($res);
        }
    }

    function material_issue_noteDetailDelete($id)
    {
        $del = $this->basic_model->deleteRecord('material_issue_note_detail_id', $id, $this->table_detail);
        echo json_encode(array("success" => "Record removed successfully", "detailsDelete" => true, "qoutationMultiTotalSet" => true));
    }

    function delete($id)
    {
        $material_issue_note_delete = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 7) {
                    if ($v['delete'] == 1) {
                        $material_issue_note_delete = true;
                    }
                }
            }
        } else {
            $material_issue_note_delete = true;
        }
        if ($material_issue_note_delete) {
            $material_issue_note_detail_data = $this->basic_model->getRows("material_issue_note_detail", "material_issue_note_id", $id, "material_issue_note_id", $this->orderBy);
            // print_b($material_issue_note_detail_data);
            if (isset($material_issue_note_detail_data) && @$material_issue_note_detail_data != null) {
                foreach ($material_issue_note_detail_data as $k => $v) {
                    $del = $this->basic_model->deleteRecord('inventory_id', $v['inventory_id'], 'inventory');
                }
            }

            $del = $this->basic_model->deleteRecord('material_issue_note_id', $id, 'material_issue_note');
            $del = $this->basic_model->deleteRecord('material_issue_note_id', $id, 'material_issue_note_detail');

            echo json_encode(array("success" => "Record removed successfully", "redirect" => site_url($this->view_product), "details" => true));
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail($id)
    {
        if (isset($id) && $id != '') {

            $res_int = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
            $table_q = "material_issue_note";
            $table_qid = "material_issue_note_id";

            $table_c = "user";
            $table_cid = "user_id";

            $table_qd = "material_issue_note_detail";
            $table_qd_id = "material_issue_note_detail_id";



            $res_quo = $this->basic_model->getRow($table_q, $table_qid, $res_int['material_issue_note_id'], $this->orderBy, $table_qid);
            $res_quo_det = $this->basic_model->getRow($table_qd, $table_qid, $res_int['material_issue_note_id'], $this->orderBy, $table_qd_id);
            $res_cust = $this->basic_model->getRow($table_c, $table_cid, $res_int['employee_id'], $this->orderBy, $table_cid);

            if ($res_int) {
                $outputTitle = "";
                $outputTitle .= $this->page_heading . " Detail";

                $outputDescription = "";
                $outputDescription .= "<p>Employee Name  : " . $res_cust['user_name'] . "</p>";
                // $outputDescription .= "<p>Product Description  : ".$res_quo_det['material_issue_note_detail_description']."</p>";
                $outputDescription .= "<p>Material Issue Note Number  : " . $res_quo['material_issue_note_no'] . "</p>";
                // $outputDescription .= "<p>Quantity  : ".$res_quo_det['material_issue_note_detail_quantity']."</p>";
                // $outputDescription .= "<p>Rate : ".$res_quo_det['material_issue_note_detail_rate']."</p>";

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            } else {
                echo json_encode(array('error' => 'Not Found ' . $this->page_heading . '.'));
            }
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be deleted."));
        }
    }

    function print()
    {
        $material_issue_note_print = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 7) {
                    if ($v['print'] == 1) {
                        $material_issue_note_print = true;
                    }
                }
            }
        } else {
            $material_issue_note_print = true;
        }
        if ($material_issue_note_print) {
            extract($_GET);
            // print_b($_GET);
            if (isset($id) && $id != null) {
                $res["material_issue_note"] = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
                if ($res["material_issue_note"]['employee_id'] != null) {
                    $res['employee_data'] = $this->basic_model->getCustomRow('Select * from user left join department ON department.department_id = user.department_id Where user.user_id = ' . $res["material_issue_note"]['employee_id']);
                } else {
                    $res['employee_data'] = [];
                }
                if ($res["material_issue_note"]['customer_id'] != null) {
                    $res['customer_data'] = $this->basic_model->getCustomRow('Select * from user left join department ON department.department_id = user.department_id Where user.user_id = ' . $res["material_issue_note"]['customer_id']);
                } else {
                    $res['customer_data'] = [];
                }
                $res["material_issue_note_detail"] = $this->basic_model->getCustomRows("SELECT * FROM material_issue_note_detail dnd JOIN product p ON p.product_id = dnd.product_id WHERE dnd.material_issue_note_id = '" . $id . "' ");
                $res["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
                /*$res['employee_data'] = $this->basic_model->getCustomRow("SELECT * FROM `user` left join department ON department.department_id = user.department_id WHERE user.user_type = 2 AND user.user_is_active = 1 AND user.user_id=".@$res["deliver_note"]['employee_id']); */

                //print_b($res['employee_data']);
                $this->basic_model->make_pdf($header_check, $res, 'Report', 'material_issue_note_report');
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
}
