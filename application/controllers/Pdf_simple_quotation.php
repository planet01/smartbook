<?php
/* @property mpdf_model $mpdf_model */
class Pdf_simple_quotation extends CI_Controller{
    function __construct(){
        parent::__construct();
    }
    public function index()
    {
        $mpdf = new \Mpdf\Mpdf();
        $header = $this->load->view('include/header_report_simple_quotation',[],true);
        $footer = $this->load->view('include/footer_report_simple_quotation',[],true);
        $content = $this->load->view('report_simple_quotation',[],true);
        $mpdf->defaultheaderline = 0;
        $mpdf->defaultfooterline = 0;
        $mpdf->SetTitle('Report');
        $mpdf->SetHeader($header);
        $mpdf->SetFooter($footer);
        $mpdf->WriteHTML($content);
        $mpdf->Output(); // opens in browser
        //$mpdf->Output('arjun.pdf','D'); // it downloads the file into the user system, with give name
    }
}
/* End of file dashboard.php */
/* Location: ./system/application/modules/matchbox/controllers/dashboard.php */