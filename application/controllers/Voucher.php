<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class voucher extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'voucher/add';
        $this->view_product = 'voucher/view';
        $this->edit_product = 'voucher/edit';
        $this->delete_product = 'voucher/delete';
        $this->detail_product = 'voucher/detail';
        $this->status = 'voucher/change_status';
        $this->history = 'voucher/history';
        // page active
        $this->add_page_product = "add-voucher";
        $this->edit_page_product = "edit-voucher";
        $this->view_page_product = "view-voucher";
        // table
        $this->table = "voucher";
        $this->table_detail = "voucher_detail";
        $this->table_fid = "voucher_detail_id";
        $this->table_pid = "voucher_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/warehouse";
        // Page Heading
        $this->page_heading = "Voucher";
		
        $this->no_results_found = "user/no_results_found";
        
		require_once APPPATH .'third_party/FileUploader.php';

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 23) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }

    function index(){
        redirect($this->add_product);
    }

    function view() {

        if ($this->check_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["edit_product"] = $this->edit_product;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product;
            $res["status_product"] = $this->status;
            $res["history"] = $this->history;
            $res['image_upload_dir'] = $this->image_upload_dir;
            $res["setval"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            $res['data'] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table." WHERE flgDeleted = 0 AND voucher_type = 0 AND voucher_created_at > CURDATE() - INTERVAL 30 DAY  ORDER BY TransactionNo DESC LIMIT 100");
            $this->template_view($this->view_page_product,$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $voucher_type; 
            $where = "WHERE flgDeleted = 0";
            if($voucher_type == 0) //PV
            {  
                $where .= " AND voucher_type = '0'";
            }else if($voucher_type == 1) //JV
            {
                $where .= " AND voucher_type = '1'";
            }
            else if($voucher_type == 2) //RV
            {
                $where .= " AND voucher_type = '2'";
            }

            if($from_date != 0)
            {
                $where .= "AND STR_TO_DATE(Transaction_Date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= "AND STR_TO_DATE(Transaction_Date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }
            
            $detail = trim($detail);

            if($detail != "")
            {
                $where .= " AND Transaction_Detail LIKE '%".$detail."%'";
            }

            $where .= " ORDER BY TransactionNo DESC limit 100";
            $query = "SELECT * FROM voucher ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);

            
             
            echo json_encode($res);
        }
    }

    function add(){
        extract($_POST);
        // print_b($_POST);
        $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
        if( $this->input->post()){ 
            if(empty($id)){
                if( empty($Transaction_Date) || empty($AccountNo)){
                    echo json_encode(array("error" => "Please fill all fields Add"));
                }
                else{
                    
                    foreach ($AccountNo as $k => $v) 
                    {
                        if(empty($Amount_Cr[$k]) && empty($Amount_Dr[$k]))
                        {
                            echo json_encode(array("error" => "Please fill credit or debit field"));
                            exit();
                        }

                        if($Amount_Cr[$k] > 0 && $Amount_Dr[$k] > 0)
                        {
                            echo json_encode(array("error" => "Add 1 at a time creadit or debit"));
                            exit();
                        }

                        if(!empty($tax[$k]) || $tax[$k] != 0)
                        {
                            if(empty($Amount_Dr[$k]) || $Amount_Dr[$k] == 0)
                            {
                                echo json_encode(array("error" => "Only debit entry accepts tax."));
                                exit();
                            }
                            if($AccountNo[$k] < 5000)
                            {
                                echo json_encode(array("error" => "Account ID > 5000 accpets tax."));
                                exit();
                            }
                            
                        }
                    }

                    $Transaction_Date_year = explode("-",$Transaction_Date)[0];
                    $company_prfx = (substr($setval["company_prefix"], -1) == '-')?$setval["company_prefix"]:$setval["company_prefix"].'-';
                    $t_no = serial_no_voucher($company_prfx.'PV-'.$Transaction_Date_year);
                    $data = array(
                        'TransactionNo' => $t_no,
                        'Transaction_Date' => $Transaction_Date,
                        'Transaction_Detail' => $Transaction_Detail_master,
                        'CompanyID' => str_replace("-","",$CompanyID),
                        'Posted' => 0,
                        'flgDeleted' => 0,
                    );
                    $voucher_id = $this->basic_model->insertRecord($data, $this->table);
                    $total_tax = 0;
                    foreach ($AccountNo as $k => $v) 
                    {
                        $arr = array(
                            'AccountNo' => $AccountNo[$k],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => @$invoice_id[$k],
                            'Cheque_No' => @$Cheque_No[$k],
                            'Pay_To_Name' => @$Pay_To_Name[$k],
                            'Amount_Cr' => empty($Amount_Cr[$k]) ? 0 : $Amount_Cr[$k],
                            'Amount_Dr' => empty($Amount_Dr[$k]) ? 0 : $Amount_Dr[$k],
                            'Transaction_Detail' => @$Transaction_Detail[$k],
                            'tax' => @$tax[$k],
                            'Remarks' => @$Remarks[$k],                     
                        );
                        $this->basic_model->insertRecord($arr, $this->table_detail);
                        $total_tax += empty($tax[$k]) ? 0 : $tax[$k];
                    }

                    if($total_tax > 0){
                        $arr = array(
                            'AccountNo' => $setval['tax_receivable_account'],
                            'voucher_id' => $voucher_id,
                            'invoice_id' => 0,
                            'Cheque_No' => 'Tax',
                            'Pay_To_Name' => '',
                            'Amount_Cr' => 0,
                            'Amount_Dr' => $total_tax,
                            'Transaction_Detail' => 'Tax Adjustment - V # '.@$setval["company_prefix"].'ACC-'.$t_no.'. ',
                            'tax' => 0,
                            'Remarks' => 'Total Tax',
                            'is_hide' => 1                     
                        );
                        $this->basic_model->insertRecord($arr, $this->table_detail);
                    }
 
				  
                    echo json_encode(array( "success" => "Record added successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
            else{
              
                if( empty($Transaction_Date) || empty($AccountNo)){
                    echo json_encode(array("error" => "Please fill all fields Edit"));
                }
                else{
                    foreach ($AccountNo as $k => $v) 
                    {
                        if(empty($Amount_Cr[$k]) && empty($Amount_Dr[$k]))
                        {
                            echo json_encode(array("error" => "Please fill credit or debit field"));
                            exit();
                        }

                        if($Amount_Cr[$k] > 0 && $Amount_Dr[$k] > 0)
                        {
                            echo json_encode(array("error" => "Add 1 at a time creadit or debit"));
                            exit();
                        }

                        if(!empty($tax[$k]) && $tax[$k] != 0 || $tax[$k] != 0.00)
                        {
                            if(empty($Amount_Dr[$k]) || $Amount_Dr[$k] == 0 )
                            {
                                echo json_encode(array("error" => $tax[$k]."Only debit entry accepts tax."));
                                exit();
                            }
                            if($AccountNo[$k] < 5000)
                            {
                                echo json_encode(array("error" => "Account ID > 5000 accpets tax."));
                                exit();
                            }
                            
                        }
                    }
                    $this->makeVoucherHistory($id);
                    $this->basic_model->customQueryDel('DELETE  from '.$this->table_detail.' where '.$this->table_pid.' ='.$id);
                    $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    $data = array(
                        'Transaction_Date' => $Transaction_Date,
                        'Transaction_Detail' => $Transaction_Detail_master,
                        'CompanyID' => str_replace("-","",$CompanyID),
                        'Posted' => 0,
                        'flgDeleted' => 0,
                        'voucher_revised_no' => number_format($voucher_revised_no) + 1
                    );
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    $total_tax = 0;

                    foreach ($AccountNo as $k => $v) 
                    {
                        $arr = array(
                            'AccountNo' => $AccountNo[$k],
                            'voucher_id' => $id,
                            'invoice_id' => @$invoice_id[$k],
                            'Cheque_No' => @$Cheque_No[$k],
                            'Pay_To_Name' => @$Pay_To_Name[$k],
                            'Amount_Cr' => empty($Amount_Cr[$k]) ? 0 : $Amount_Cr[$k],
                            'Amount_Dr' => empty($Amount_Dr[$k]) ? 0 : $Amount_Dr[$k],
                            'Transaction_Detail' => @$Transaction_Detail[$k],
                            'tax' => @$tax[$k],
                            'Remarks' => @$Remarks[$k],                     
                        );
                        $this->basic_model->insertRecord($arr, $this->table_detail);
                        $total_tax += empty($tax[$k]) ? 0 : $tax[$k];
                    }

                    if($total_tax > 0){
                        $arr = array(
                            'AccountNo' => $setval['tax_receivable_account'],
                            'voucher_id' => $id,
                            'invoice_id' => 0,
                            'Cheque_No' => 'Tax',
                            'Pay_To_Name' => '',
                            'Amount_Cr' => 0,
                            'Amount_Dr' => $total_tax,
                            'Transaction_Detail' => 'Tax Adjustment - V # '.@$setval["company_prefix"].'ACC-'.$res_edit['TransactionNo'].'. ',
                            'tax' => 0,
                            'Remarks' => 'Total Tax',
                            'is_hide' => 1                     
                        );
                        $this->basic_model->insertRecord($arr, $this->table_detail);
                    }
                    
                    

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
        }
        else{
            if ($this->check_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res["setval"] = $setval;
                $res["tax"] = $setval['tax'];
                $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($res["setval"]["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
                $res['invoices'] = $this->basic_model->getCustomRows("SELECT * FROM `invoice`");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function edit($id){
        if ($this->check_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    extract($_POST);
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "Edit";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res["data"] = $this->basic_model->getCustomRow("SELECT * FROM ".$this->table." WHERE voucher_id=".$id);
                    $res["detail_data"] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->table_detail." WHERE ".$this->table_pid."=".$id." AND is_hide = 0");
                    $res["setval"] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                    $res["tax"] = $res["setval"]['tax'];
                    $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($res["setval"]["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
                    $res['invoices'] = $this->basic_model->getCustomRows("SELECT * FROM `invoice`");
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";
                $rev = ($res['voucher_revised_no'] > 0)?'-R'.$res['voucher_revised_no'] : '';
                $outputDescription = "";
                $outputDescription .= "<p><span style='font-weight:bold;'>Transaction No : </span> ".$res['TransactionNo'].$rev."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Date : </span> ".date("d-M-Y", strtotime($res["Transaction_Date"]))."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Remarks : </span> ".$res['Transaction_Detail']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Company ID : </span> ".@$res['CompanyID']."</p>";
                
                if($res['doc_type'] == 1 || $res['doc_type'] == 3)
                {
                    $res_quo_det = $this->basic_model->getCustomRows("Select * From voucher_detail vd LEFT JOIN amc_quotation v ON v.amc_quotation_id  = vd.invoice_id JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo where vd.voucher_id = '" . $id . "'");
                }elseif($res['doc_type'] == 0 || $res['doc_type'] == 2)
                {
                    $res_quo_det = $this->basic_model->getCustomRows("Select * From voucher_detail vd LEFT JOIN invoice v ON v.invoice_id = vd.invoice_id JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo where vd.voucher_id = '" . $id . "'");
                }
                
                $count = 0;
                $setting = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                foreach ($res_quo_det as $k => $v) {
                    $count++;
                    if($res['doc_type'] == 1)
                    {
                        $rev = ($v['amc_quotation_revised_no'] > 0)?@$v['amc_quotation_no'].'-R'.number_format(@$v['amc_quotation_revised_no']):@$v['amc_quotation_no'];
                    }elseif($res['doc_type'] == 0)
                    {
                        $rev = (@$v['invoice_revised_no'] > 0)?'-R'.@$v['invoice_revised_no'] : '';
                    }
                    
                    $outputDescription .= "<p>" . $count . ". Account No : " . $v['AccountNo'].'-'.$v['AccountDesc'] . "</p>";
                    if($v['invoice_id'] != 0)
                    {

                        $outputDescription .= "<p>Invoice ID : " . $setting["company_prefix"];
                        if($res['doc_type'] == 1){
                            if($v['amc_quotation_status'] == 4 ||  $v['amc_quotation_status'] == 7)
                            { 
                                $outputDescription .= 'AMCINV-';
                            }
                            else
                            { 
                                $outputDescription .= @$setting["quotation_prfx"];
                            }
                        }
                        elseif($res['doc_type'] == 0 || $res['doc_type'] == 2)
                        {
                            $outputDescription .= @$setting["invoice_prfx"].@$v['invoice_no'] ;
                        }
                        $outputDescription .= $rev. "</p>";
                    }else
                    {
                        $outputDescription .= "<p>Invoice ID : Not Selected</p>";
                    }
                    $outputDescription .= "<p>Cheque No : " . $v['Cheque_No'] . "</p>";
                    $outputDescription .= "<p>Pay To Name : " . $v['Pay_To_Name'] . "</p>";
                    $outputDescription .= "<p>Amount Credit: " . $v['Amount_Cr'] . "</p>";
                    $outputDescription .= "<p>Amount Debit: " . $v['Amount_Dr'] . "</p>";
                    $outputDescription .= "<p>Detail: " . $v['Transaction_Detail'] . "</p>";
                    // $outputDescription .= "<p>Remarks: " . $v['Remarks'] . "</p>";
                }
                
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function change_status($id)
    {
        $data = array(
            "Posted" => 1,
        ); 
        $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
        echo json_encode(array("success" => "Status Changed To Posted", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
    }

    function delete($id){
        if( isset($id) && $id != ''){
            $data = array(
                "flgDeleted" => 1,
            ); 
            $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
            
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }

    function makeVoucherHistory($id){
        // $revision_id = $this->basic_model->insertRecord(["revision_title"=> 'Voucher'],'tbl_revision');
        $voucher_data = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
        $voucher_detail = $this->basic_model->getCustomRows('SELECT * FROM '.$this->table_detail.' where '.$this->table_pid.' = "'.@$voucher_data['voucher_id'].'"');
        

        $data = array(
            'TransactionNo' => $voucher_data['TransactionNo'],
            'Transaction_Date' => $voucher_data['Transaction_Date'],
            'Transaction_Detail' => $voucher_data['Transaction_Detail'],
            'CompanyID' => $voucher_data['CompanyID'],
            'voucher_revised_no' => $voucher_data['voucher_revised_no'],
            'Posted' => 0,
            'flgDeleted' => 0,
            'belongs_to' => $id
        );
        $voucher_id = $this->basic_model->insertRecord($data,$this->table."_history");

        foreach ($voucher_detail as $k => $v) {
            $arr = array(
                'AccountNo' => $v['AccountNo'],
                'voucher_id' => $voucher_id,
                'invoice_id' => $v['invoice_id'],
                'Cheque_No' => $v['Cheque_No'],
                'Pay_To_Name' => $v['Pay_To_Name'],
                'Amount_Cr' => $v['Amount_Cr'],
                'Amount_Dr' => $v['Amount_Dr'],
                'tax' => $v['tax'],
                'Transaction_Detail' => $v['Transaction_Detail'],
                'Remarks' => $v['Remarks'],
                'is_hide' => $v['is_hide'],
            );
            $voucher_detail_id = $this->basic_model->insertRecord($arr,$this->table_detail."_history");
        }
        
        return true;
    }

    function history($id){
        if ($this->check_history) {
            
            $res = $this->basic_model->getCustomRows("Select * From voucher_history where belongs_to = '".$id."'");
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                if($res){
                    $outputTitle = "";
                    $outputTitle .= $this->page_heading." History";
                    $outputDescription = "";
                    $outputDescription .= "<table class='table'>";
                    $outputDescription .= "<thead>";
                    $outputDescription .= "<tr>";

                    $outputDescription .= "<th>Transaction No</th>";
                    $outputDescription .= "<th>Date</th>";
                    $outputDescription .= "<th>Details</th>";
                    $outputDescription .= "</tr>";
                    $outputDescription .= "</thead>";
                    $outputDescription .= "<tbody>";
                    
                    foreach ($res as $k => $v) {
                        $outputDescription .= "<tr>";
                        $rev = ($v['voucher_revised_no'] > 0)?'-R'.$v['voucher_revised_no'] : '-R0';
                        $outputDescription .= "<td><a target='_blank' href='".site_url('voucher/print_report?id='.@$v['voucher_id'].'&header_check=1&view=0&history=1')."'>".$v['TransactionNo'].$rev."</a></td>"; 
                        $outputDescription .= "<td>".date("d-M-Y", strtotime($v["Transaction_Date"]))."</td>";
                        $outputDescription .= "<td>".$v['Transaction_Detail']."</td>";
                        $outputDescription .= "</tr>";
                    }
                    $outputDescription .= "</tbody>";
                    $outputDescription .= "</table>";
                    echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
                }else{
                    echo json_encode(array('error' => 'No History Found For This '.$this->page_heading.'.'));
                }
           
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function print_report()
    {
        if ($this->check_print) {
            extract($_GET);
            if(isset($id) && $id != null) {
                if($history == 1)
                {
                    $res['data'] = $this->basic_model->getRow("voucher_history",$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    $res['detail_data'] = $this->basic_model->getCustomRows("Select * From voucher_detail_history vd LEFT JOIN invoice v ON v.invoice_id = vd.invoice_id lEFT JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo where vd.voucher_id = '" . $id . "'");
                }else
                {
                    $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    $res['detail_data'] = $this->basic_model->getCustomRows("Select * From voucher_detail vd LEFT JOIN invoice v ON v.invoice_id = vd.invoice_id LEFT JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo where vd.voucher_id = '" . $id . "'");
                }
                $res['history'] = $history;
                $res['setval'] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                $this->basic_model->make_pdf($header_check,$res,'Report','voucher_report');
            }
            else
            {
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

}