<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class warehouse extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'warehouse/add';
        $this->view_product = 'warehouse/view';
        $this->edit_product = 'warehouse/edit';
        $this->delete_product = 'warehouse/delete';
        $this->detail_product = 'warehouse/detail';
        // page active
        $this->add_page_product = "add-warehouse";
        $this->edit_page_product = "edit-warehouse";
        $this->view_page_product = "view-warehouse";
        // table
        $this->table = "warehouse";
        $this->table_pid = "warehouse_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/warehouse";
        // Page Heading
        $this->page_heading = "Location";
		
        $this->no_results_found = "user/no_results_found";
        
		require_once APPPATH .'third_party/FileUploader.php';

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index(){
        redirect($this->add_product);
    }
    function add(){
        extract($_POST);
        // print_b($_POST);
        if( $this->input->post()){ 
            if(empty($id)){
                if( empty($warehouse_name) ||  empty($warehouse_address) || empty($warehouse_city) || empty($warehouse_country)  || $warehouse_no_of_shelf == '' || $warehouse_no_of_section == ''){
                    echo json_encode(array("error" => "Please fill all fields Add"));
                }
                else{
                    $find_warehouse = $this->basic_model->getCustomRow("SELECT * FROM  warehouse where warehouse_name = '".rtrim(ltrim($warehouse_name))."'");
					if(!empty($find_warehouse) && $warehouse_no_of_section != ''){
                        echo json_encode(array("error" => "Location is already exist with same name."));
                        return false;
                    }
					if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $data = $FileUploader->upload();

                        $image_path = $data['files'][0]['name'];
                    }

                    $data = array(
                        "warehouse_name" => $warehouse_name,
                        "warehouse_address" => $warehouse_address,
                        "warehouse_city" => $warehouse_city,
                        "warehouse_country" => $warehouse_country,
                        "warehouse_no_of_shelf" => $warehouse_no_of_shelf,
                        "warehouse_no_of_section" => $warehouse_no_of_section,
                        "warehouse_is_active" => $warehouse_is_active
                    ); 
					
					$data['warehouse_image'] = isset($image_path)? $image_path : ''; 

                    $id = $this->basic_model->insertRecord($data,$this->table);
 
				  
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
            else{
              
                if( empty($warehouse_name) ||  empty($warehouse_address) || empty($warehouse_city) || empty($warehouse_country)  || $warehouse_no_of_shelf == '' || $warehouse_no_of_section == ''){
                    echo json_encode(array("error" => "Please fill all fields Edit"));
                }
                else{
                    $find_warehouse = $this->basic_model->getCustomRow("SELECT * FROM  warehouse where warehouse_name = '".rtrim(ltrim($warehouse_name))."' AND warehouse_id != ".$id);
                    if(!empty($find_warehouse) && $warehouse_no_of_section != ''){
                        echo json_encode(array("error" => "Location is already exist with same name."));
                        return false;
                    }

                    $data = array(
                        "warehouse_name" => $warehouse_name,
                        "warehouse_address" => $warehouse_address,
                        "warehouse_city" => $warehouse_city,
                        "warehouse_country" => $warehouse_country,
                        "warehouse_no_of_shelf" => $warehouse_no_of_shelf,
                        "warehouse_no_of_section" => $warehouse_no_of_section,
                        "warehouse_is_active" => $warehouse_is_active
                    ); 

                    if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $datamy = $FileUploader->upload();

                        $data['warehouse_image'] = $datamy['files'][0]['name'];
                    }
					
					$id = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
 
				  
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                  }
            }
        }
        else{
            $location_add = false;
            if ($this->user_type == 2) {
                foreach ($this->user_role as $k => $v) {
                if($v['module_id'] == 4)
                {
                    if($v['add'] == 1)
                    {
                        $location_add = true;
                    }
                }
                }
            }else
            {
                $location_add = true;
            }

            if ($location_add) {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                // // $res['categoryData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse` WHERE `warehouse_is_active` = 1");
                $this->template_view($this->add_page_product,$res);
            }
            else
            {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
        }
    }

    function edit($id){
        $location_edit = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 4)
            {
                if($v['edit'] == 1)
                {
                    $location_edit = true;
                }
            }
            }
        }else
        {
            $location_edit = true;
        }

        if ($location_edit) {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res['image_upload_dir'] = $this->image_upload_dir;
                    $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                    $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    // print_b($res);
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
                }else{
                    redirect($this->no_results_found);
                }
            }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function view() {

        $location_view = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 4)
            {
                if($v['visible'] == 1)
                {
                    $location_view = true;
                }
            }
            }
        }else
        {
            $location_view = true;
        }

        if ($location_view) {
            $res['title'] = "View All ".$this->page_heading;
            $res['active'] = $this->view_page_product;
            $res["page_heading"] = $this->page_heading;
            $res["add_product"] = $this->add_product;
            $res["edit_product"] = $this->edit_product;
            $res["delete_product"] = $this->delete_product;
            $res["detail_product"] = $this->detail_product;
            $res['image_upload_dir'] = $this->image_upload_dir;
            //$res['data'] = $this->basic_model->getMultiData($this->table,$this->table_pid,$this->orderBy);
            $res['data'] = $this->basic_model->getCustomRows("SELECT warehouse.*,  country.country_name FROM ".$this->table." Left JOIN country ON country.country_id = warehouse.warehouse_country ORDER BY ".$this->table_pid." ".$this->orderBy);
            $this->template_view($this->view_page_product,$res);
        }
        else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function imageDelete(){
        extract($_POST);
        // print_b($_POST);
        $file_path = dir_path().$this->image_upload_dir.'/'.@$file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $arr = array(
            'warehouse_image' => ''
        );
        $updateRecord = $this->basic_model->updateRecord($arr, $this->table, $this->table_pid, $data['image_post_file_id']);
    }

    function delete($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if(isset($res['warehouse_image']) && @$res['warehouse_image'] !=null){
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['warehouse_image'];
                if (file_exists($file_path)) {
                    @unlink($file_path);
                }
            }
            $del = $this->basic_model->deleteRecord($this->table_pid, $id, $this->table);
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }

    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            if($res){
                $countries = $this->basic_model->getCustomRow("SELECT country_name FROM  country where country_id=".$res['warehouse_country']);
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";

                $outputDescription = "";
                $current_img = '';
                $file_path = dir_path().$this->image_upload_dir.'/'.@$res['warehouse_image'];
                if (!is_dir($file_path) && file_exists($file_path)) {
                  $current_img = site_url($this->image_upload_dir.'/'.$res['warehouse_image']);
                }else{
                  $current_img = site_url("uploads/dummy.jpg");
                }
                $outputDescription .= "<p><br> <img src='".$current_img."' width='100px' /></p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Name : </span> ".$res['warehouse_name']."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Addresss : </span> ".$res['warehouse_address']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>City : </span> ".$res['warehouse_city']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Country : </span> ".@$countries['country_name']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>No of Shelves : </span> ".$res['warehouse_no_of_shelf']."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>No of Sections : </span> ".$res['warehouse_no_of_section']."</p>"; 
				$outputDescription .= "<p><span style='font-weight:bold;'>Status : </span> ".(($res['warehouse_is_active'] == 1)?'<span class="label label-success">Enabled</span>':'<span class="label label-danger">Disabled</span>')."</p>";
                
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }
}