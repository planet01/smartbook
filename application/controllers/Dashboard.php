<?php
    class dashboard extends MY_Back_Controller{
        public function __CONSTRUCT(){
            parent::__construct();
            $this->add_page_product = "dashboard";
            $this->add_product = 'user/login';
            $this->view_product = 'my_account';
            $this->print_product = 'invoice/print_invoice';
            $this->detail_below_level = "dashboard/stock_bifurcation";
            $this->redirect = 'my_account';
            $this->table = "user";
            $this->pid = "id";
            $logged_in = $this->is_login_admin();
            if (!$logged_in) {
                redirect('user');
            }
            $this->invoicedelivery = false;
            $this->warrantystatus = false;
            $this->minimumlevel = false;
            $this->pendingqotations = false;
            $this->taxcalculation = false;
            $this->totalsales = false;
            $this->dashboard_account_ledger = false;
            $this->dashboard_account_transaction = false;

            if ($this->user_type == 2) {
                //   print_r($this->user_role);
                foreach ($this->user_role as $k => $v) {
                    if($v['module_id'] == 30)
                    {
                        if($v['visible'] == 1)
                        {
                            $this->dashboard_account_transaction = true;
                        }
                    }

                    if($v['module_id'] == 31)
                    {
                        if($v['visible'] == 1)
                        {
                            $this->dashboard_account_ledger = true;
                        }
                    }
                }
                foreach ($this->user_role_dashboard as $d => $item) {
                    $this->dashboard_account_ledger = true;
                    $this->dashboard_account_transaction = true;
                   if(isset($item['department_id']) && $item['department_id'] ==1)
                   {
                      $this->invoicedelivery = true;
                      $this->warrantystatus = true;
                      $this->minimumlevel = true;
                      $this->pendingqotations = true;
                   }
                   if(isset($item['department_id']) && $item['department_id'] ==3)
                   {
                      $this->invoicedelivery = true;
                      $this->warrantystatus = true;
                      $this->minimumlevel = true;
                      $this->pendingqotations = true;
                      $this->taxcalculation = true;
                      $this->totalsales = true;
                   }
                   if(isset($item['department_id']) && $item['department_id'] ==2)
                   {
                      $this->warrantystatus = true;
                      $this->minimumlevel = true;
                   }
                }
            }
            else
            {
                $this->invoicedelivery = true;
                $this->warrantystatus = true;
                $this->minimumlevel = true;
                $this->pendingqotations = true;
                $this->taxcalculation = true;
                $this->totalsales = true;
                $this->dashboard_account_ledger = true;
                $this->dashboard_account_transaction = true;
            }
        }
        public function index(){
            $res['title'] = "Dashboard";
            $res['active'] = "dashboard";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $this->template_view($this->add_page_product,$res);
            
        }

        public function invoice_delivery_note_matching()
        {
            if (!$this->invoicedelivery) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }
            if($this->input->get('fromdate') != "" && $this->input->get('todate') != "")
            {
                $res['fromdate'] = $fromdate = $this->input->get('fromdate');
                $res['todate'] = $todate = $this->input->get('todate');
                $res['customer_id'] = $customer_id = $this->input->get('customer_id');
                
                $query = "SELECT dn.delivery_note_id,dn.customer_id,dn.delivery_date,
                (select i.invoice_revised_no from invoice i where i.invoice_no = dn.invoice_id) as invoice_revised_no,
                (select i.invoice_status from invoice i where i.invoice_no = dn.invoice_id) as invoice_status,
                                (select i.invoice_id from invoice i where i.invoice_no = dn.invoice_id) as invoice_id,
                                                dn.invoice_id as invoice_no,
                                                (select p.product_name from product p where p.product_id =  dnd.product_id) as product_name
                                                ,dnd.delivery_note_detail_total_quantity as total_quantity,dnd.delivery_note_detail_pending as pending_quantity,dnd.delivery_note_detail_delivered as delivered_quantity FROM delivery_note dn join delivery_note_detail dnd 
                                                where dnd.delivery_note_detail_pending > 0 and (STR_TO_DATE(dn.delivery_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')) ";
                $query .= ($customer_id == null)?"":"and dn.customer_id = $customer_id";
                $query .= " order by invoice_no";
                $res['delivery_note'] = getCustomRows($query);
            }
            else
            {
                $res['delivery_note'] = [];
            }
            $res['customerData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $res['title'] = "Dashboard";
            $res['print'] = $this->print_product;
            $res['active'] = "invoice_delivery_note_matching";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $res["detail_below_level"] = $this->detail_below_level;
            $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res['setting'] = $setting;
            $this->template_view("invoice-delivery-note-matching",$res);
        }
        public function invoice_warranty_status()
        {
            if (!$this->warrantystatus) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }
            $res['customer_id'] = $customer_id = $this->input->get('customer_id');
            $whereCondition = "";
            $query = "select invoice.invoice_date,invoice.customer_id,invoice.invoice_status,
                invoice.invoice_total_amount,invoice.invoice_id,invoice.invoice_no,invoice.invoice_revised_no,invoice.invoice_tax_amount,
                invoice.`invoice_buyback`+invoice.`invoice_total_discount_amount`+(select sum(invoice_detail.invoice_detail_total_discount_amount) from invoice_detail where invoice_detail.invoice_id = invoice.invoice_id) as discount,
                invoice.invoice_net_amount,invoice_additional.additional_expiry_date from invoice join invoice_additional on invoice.invoice_id = invoice_additional.invoice_id
                where";
            if($this->input->get('expires') != "" && $this->input->get('expires') == "30days")
            {
                $whereCondition =" DATEDIFF(STR_TO_DATE(invoice_additional.additional_expiry_date,'%d-%m-%Y'), CURDATE()) between 1 and 30 ";
                $query .=  $whereCondition;
                $query .= ($customer_id == null)?"":"and invoice.customer_id = $customer_id";
                $query .= " order by invoice_no";
                $res['warranty_status'] = getCustomRows($query);
                // var_dump($res['warranty_status']);

            }else if($this->input->get('expires') != "" && $this->input->get('expires') == "60days")
            {
                $whereCondition =" DATEDIFF(STR_TO_DATE(invoice_additional.additional_expiry_date,'%d-%m-%Y'), CURDATE()) between 1 and 60 ";
                $query .=  $whereCondition;
                $query .= ($customer_id == null)?"":" and invoice.customer_id = $customer_id";
                $query .= " order by invoice_no";
                $res['warranty_status'] = getCustomRows($query);
            }
            else if($this->input->get('expires') != "" && $this->input->get('expires') == "past30days")
            {
                $whereCondition =" DATEDIFF(STR_TO_DATE(invoice_additional.additional_expiry_date,'%d-%m-%Y'), CURDATE()) between -1 and -60 ";
                $query .=  $whereCondition;
                $query .= ($customer_id == null)?"":" and invoice.customer_id = $customer_id";
                $query .= " order by invoice_no";
                $res['warranty_status'] = getCustomRows($query);
            }
            else if($this->input->get('expires') != "" && $this->input->get('expires') == "daterange" && $this->input->get('fromdate') != "" && $this->input->get('todate') != "")
            {
                $res['fromdate'] = $fromdate = $this->input->get('fromdate');
                $res['todate'] = $todate = $this->input->get('todate');
                $whereCondition =" (STR_TO_DATE(invoice.invoice_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')) ";
                $query .=  $whereCondition;
                $query .= ($customer_id == null)?"":"and invoice.customer_id = $customer_id";
                $query .= " order by invoice_no";
                $res['warranty_status'] = getCustomRows($query);
            }
            else
            {
                $res['warranty_status'] = [];
            }
            $res['expires'] = $this->input->get('expires');
            $res['customerData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $res['title'] = "Dashboard";
            $res['active'] = "invoice_warranty_status";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $res['print'] = $this->print_product;
            $res["detail_below_level"] = $this->detail_below_level;
            $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res['setting'] = $setting;
            $this->template_view("invoice-warranty-status",$res);
        }
        public function stock_below_minimum_level()
        {
            if (!$this->minimumlevel) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }
            $res['stockslevel'] = getCustomRows("SELECT inven.product_id,
            (select product.product_name from product where product.product_id = inven.product_id) as product_name,
            (select product.min_quantity from product where product.product_id = inven.product_id) as min_quantity, 
            (select sum(i.inventory_quantity) from inventory i where product_id = inven.product_id and inventory_type = 1)-(select sum(i.inventory_quantity) from inventory i where product_id = inven.product_id and inventory_type = 0) as inventory_quantity
            FROM `inventory` inven GROUP by inven.product_id having min_quantity > inventory_quantity");
            $res['title'] = "Dashboard";
            $res['active'] = "stock_below_minimum_level";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $res["detail_below_level"] = $this->detail_below_level;
            $this->template_view("stock-below-minimum-level",$res);
        }
        public function stock_bifurcation($id)
        {
            if( isset($id) && $id != ''){
                $locationWiseStock = getCustomRows("Select warehouse_id,
                (select wh.warehouse_name from warehouse wh where wh.warehouse_id = inventory.warehouse_id) as warehouse_name,
                COALESCE((select sum(i.inventory_quantity) from inventory i where product_id = $id and inventory_type = 1 and i.warehouse_id = inventory.warehouse_id),0)-
                COALESCE((select sum(i.inventory_quantity) from inventory i where product_id = $id and inventory_type = 0 and i.warehouse_id = inventory.warehouse_id),0)
                 as inventory_quantity
                from inventory group by warehouse_id");
                $outputTitle = "Stock Bifurcation Location Wise";
                $outputDescription = "<table class='table table-bordered'>";
                $outputDescription .= "<tr><th>Location</th><th>Stock Quantity</th></tr>";
                foreach ($locationWiseStock as $key => $value) {
                    $outputDescription .= "<tr><td>".$value['warehouse_name']."</td><td>".$value['inventory_quantity']."</td></tr>";
                }
                $outputDescription .= "</table>";
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }
            else
            {
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
            }
        }
        public function pending_quotations()
        {
            if (!$this->pendingqotations) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }
            $whereCondition ="";
            if($this->input->get('types') != null && count($this->input->get('types')) > 0)
            {
                $res['types'] = $this->input->get('types');
                $whereCondition .= " and quotation.quotation_status in (".implode(',',$this->input->get('types')).") ";
            }
            if($this->input->get('customer_id') != '')
            {
               $res['customer_id']= $customer_id = $this->input->get('customer_id');
                $whereCondition .= " and quotation.customer_id = $customer_id ";
            }
            if($this->input->get('fromdate') != "" && $this->input->get('todate') != "")
            {
                $res['fromdate']= $fromdate = $this->input->get('fromdate');
                $res['todate']=$todate = $this->input->get('todate');
                $res['pendingquotation'] = getCustomRows("SELECT quotation.*,user.user_name, user.user_country FROM `quotation`  INNER JOIN `user`  ON quotation.customer_id = user.user_id 
                WHERE quotation.quotation_revised_id = 0 and (STR_TO_DATE(quotation.quotation_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')) $whereCondition
                order by quotation.quotation_id desc");
                $res['pendingquotationcount'] = getCustomRow("SELECT sum(quotation.net_amount) as total_net FROM `quotation`  INNER JOIN `user`  ON quotation.customer_id = user.user_id 
                WHERE quotation.quotation_revised_id = 0 and (STR_TO_DATE(quotation.quotation_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')) $whereCondition
                order by quotation.quotation_id desc")["total_net"];
                // var_dump($res['pendingquotationcount']);
            }
            else
            {
                $res['pendingquotation'] =[];
                $res['pendingquotationcount'] = 0.00;
            }
            $res['customerData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res["print"] = 'quotation/print';
            $res['setting'] = $setting;
            $res['title'] = "Dashboard";
            $res['active'] = "pending_quotations";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $res["detail_below_level"] = $this->detail_below_level;
            $this->template_view("pending-quotations",$res);
        }
        public function tax_calculation()
        {
            if (!$this->taxcalculation) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }
            if($this->input->get('fromdate') != "" && $this->input->get('todate') != "")
            {
                $res['fromdate'] = $fromdate = $this->input->get('fromdate');
                $res['todate'] = $todate = $this->input->get('todate');
                $res['tax_calculation'] = getCustomRows("SELECT i.invoice_id,i.invoice_no,i.invoice_status,i.invoice_revised_no,i.`invoice_net_amount`,i.`invoice_tax_amount`,i.`invoice_date`,
                COALESCE((select sum(payment.payment_amount) from payment where payment.invoice_id = i.invoice_id),0) as recieve_amount
                FROM `invoice` as i where
                STR_TO_DATE(i.invoice_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')");
                $res['total_tax'] = getCustomRow("SELECT sum(invoice.invoice_tax_amount) as total FROM `invoice` where
                STR_TO_DATE(invoice.invoice_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')");
                $res['pending_tax'] = getCustomRow("SELECT sum(invoice.invoice_tax_amount) as total FROM `invoice` where
                (STR_TO_DATE(invoice.invoice_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')) and invoice.has_payment = 0");
            }
            else
            {
                $res['tax_calculation']=[];
            }
            $res['print'] = $this->print_product;
            $res['title'] = "Dashboard";
            $res['active'] = "tax_calculation";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $res["detail_below_level"] = $this->detail_below_level;
            $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res['setting'] = $setting;
            $this->template_view("tax-calculation",$res);
        }

        public function total_sales()
        {
            if (!$this->totalsales) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }
            $whereCondition ="";
            if($this->input->get('customer_id') != '')
            {
               $res['customer_id']= $customer_id = $this->input->get('customer_id');
                $whereCondition .= "and i.customer_id = $customer_id ";
            }
            if($this->input->get('employee_id') != '')
            {
                $res['employee_id']=$emp_id = $this->input->get('employee_id');
                $whereCondition .= "and i.employee_id = $emp_id ";
            }
            if($this->input->get('pendingpayment') != null)
            {
               $res['pendingpayment']= $pendingpayment = $this->input->get('customer_id');
                $whereCondition .= "and i.has_payment = 1 ";
            }
            if($this->input->get('fromdate') != '' && $this->input->get('todate') != '')
            {
                $res['fromdate']= $fromdate = $this->input->get('fromdate');
                $res['todate']=$todate = $this->input->get('todate');
                $whereCondition .= "and (STR_TO_DATE(i.invoice_date,'%d-%m-%Y') between STR_TO_DATE('$fromdate','%d-%m-%Y') and STR_TO_DATE('$todate','%d-%m-%Y')) ";

                $res['total_sales_count'] = getCustomRow("SELECT sum(i.`invoice_total_amount`) as total FROM `invoice` i 
                where i.`invoice_status`  = 4 $whereCondition");
                $res['net_sales_count'] = getCustomRow("SELECT sum(i.`invoice_net_amount`) as total FROM `invoice` i 
                where i.`invoice_status`  = 4 $whereCondition");
                $res['total_sales'] = getCustomRows("SELECT i.invoice_id,i.invoice_no,i.invoice_status,i.`invoice_revised_no`,i.`invoice_net_amount`,i.`invoice_tax_amount`,i.`invoice_total_amount`,i.`invoice_buyback`+i.`invoice_total_discount_amount`+(select sum(invoice_detail.invoice_detail_total_discount_amount) from invoice_detail where invoice_detail.invoice_id = i.invoice_id) as discount,i.`invoice_date`,i.customer_id,i.employee_id FROM `invoice` i
                where i.`invoice_status`  = 4 $whereCondition");
            }
            else
            {
                $res['total_sales'] =[];
            }
            
            $res['employeeData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 2 AND `user_is_active` = 1");
            $res['customerData'] = getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
            
            $res['title'] = "Dashboard";
            $res['active'] = "total_sales";
            $res['uri'] = $this->uri->segment(1);
            $res['counts'] = "";
            $res["detail_below_level"] = $this->detail_below_level;
            $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res['setting'] = $setting;
            $res['print'] = $this->print_product;
            $this->template_view("total-sales",$res);
        }

        
        
        public function dashboard_account_transaction()
        {
            if (!$this->dashboard_account_transaction) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }
            
            $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($setting["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
            
            
            $res['title'] = "Dashboard";
            $res['active'] = "dashboard_account_transaction";
            $res['setting'] = $setting;
            $res['print'] = $this->print_product;
            $this->template_view("dashboard_account_transaction",$res);
        }
        public function fetch_account_transaction()
        {
            extract($_POST);
            if($this->input->post())
            {
                $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $res['setting'] = $setting;
                $where = "";
                if($AccountNo != '0')
                {
                    $where = " AND vd.AccountNo = '".$AccountNo."'";
                }

                $query = "Select * From voucher v JOIN voucher_detail vd ON v.voucher_id = vd.voucher_id JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo WHERE v.`flgDeleted` = 0 and (STR_TO_DATE(v.Transaction_Date,'%Y-%m-%d') between STR_TO_DATE('$date_from','%Y-%m-%d') and STR_TO_DATE('$date_to','%Y-%m-%d'))".$where." group by vd.voucher_detail_id";
               
                $res['data'] = $this->basic_model->getCustomRows($query);
                $res['base_url'] = site_url(); 
                echo json_encode($res);
            }
        }

        public function account_transaction_print()
        {
            extract($_GET);
            $res['from_date'] = $from_date;
            $res['to_date'] = $to_date;
            $where = "";
            if($AccountNo != '0')
            {
                $where = " AND vd.AccountNo = '".$AccountNo."'";
            }
            $query = "Select * From voucher v JOIN voucher_detail vd ON v.voucher_id = vd.voucher_id JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo WHERE v.`flgDeleted` = 0 and (STR_TO_DATE(v.Transaction_Date,'%Y-%m-%d') between STR_TO_DATE('$from_date','%Y-%m-%d') and STR_TO_DATE('$to_date','%Y-%m-%d'))".$where;
            //print_b($query);
            $res['data'] = $this->basic_model->getCustomRows($query);
            $res['voucher_data'] = $this->basic_model->getCustomRows($query." group by v.voucher_id");
            $this->basic_model->make_pdf(1,$res,'Report','account_transaction_report');
        }   

        public function dashboard_account_ledger()
        {
            if (!$this->dashboard_account_ledger) {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
                return;
            }

            $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res['accounts'] = $this->basic_model->getCustomRows("SELECT * FROM `chart_of_account` WHERE `AccountLevelID` = 3 && CompanyID = '".substr($setting["company_prefix"], 0, 2)."' && (InActive_Status != 1 || flgDeleted != 1)");
            
            $setting = getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $res['title'] = "Dashboard";
            $res['active'] = "dashboard_account_ledger";
            $res['setting'] = $setting;
            $res['print'] = $this->print_product;
            $this->template_view("dashboard_account_ledger",$res);
        }

        public function account_ledger_print()
        {
            extract($_GET);
            $res['from_date'] = $from_date;
            $res['to_date'] = $to_date;
            $order_by = " order by v.TransactionNo ASC";
            $query = "Select * From voucher v JOIN voucher_detail vd ON v.voucher_id = vd.voucher_id JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo WHERE v.`flgDeleted` = 0 and (STR_TO_DATE(v.Transaction_Date,'%Y-%m-%d') between STR_TO_DATE('$from_date','%Y-%m-%d') and STR_TO_DATE('$to_date','%Y-%m-%d')) ";
            $data = $this->basic_model->getCustomRows($query.$order_by);
            $res['account_data'] = $this->basic_model->getCustomRows($query." group by vd.AccountNo");
            $res['data'] = [];
            foreach($data as $v)
            {
                $balance_query = $this->basic_model->getCustomRow('Select sum(Amount_Cr) - sum(Amount_Dr) as balance,(Select sum(Amount_Cr) - sum(Amount_Dr) From voucher_detail vd where vd.voucher_detail_id < "'.$v['voucher_detail_id'].'" AND vd.AccountNo = "'.$v['AccountNo'].'") as previous_balance From voucher_detail where voucher_detail_id <= "'.$v['voucher_detail_id'].'" AND AccountNo = "'.$v['AccountNo'].'"');
                $balance = $balance_query['balance'];
                $previous_balance = $balance_query['previous_balance'];
                $v['balance'] = number_format((float)$balance, 2, '.', '');
                $v['previous_balance'] = number_format((float)$previous_balance, 2, '.', '');
                array_push($res['data'],$v);
            }

            $query = "Select sum(Amount_Cr) - sum(Amount_Dr) as balance,(Select sum(vd2.Amount_Cr) - sum(vd2.Amount_Dr) From voucher v2 JOIN voucher_detail vd2 ON v2.voucher_id = vd2.voucher_id WHERE STR_TO_DATE(v2.Transaction_Date,'%Y-%m-%d') <= STR_TO_DATE('$from_date','%Y-%m-%d')) AS previous_balance From voucher v JOIN voucher_detail vd ON v.voucher_id = vd.voucher_id WHERE v.`flgDeleted` = 0 and (STR_TO_DATE(v.Transaction_Date,'%Y-%m-%d') between STR_TO_DATE('$from_date','%Y-%m-%d') and STR_TO_DATE('$to_date','%Y-%m-%d')) ";
            $temp = $this->basic_model->getCustomRows($query);
            $res['balance'] = number_format((float)$temp[0]['balance'], 2, '.', '');
            $res['previous_balance'] = number_format((float)$temp[0]['previous_balance'], 2, '.', '');
            $this->basic_model->make_pdf(1,$res,'Report','account_ledger_report');
        } 

        public function account_ledger_summary_print()
        {
            extract($_GET);
            $res['from_date'] = $from_date;
            $res['to_date'] = $to_date;
            $order_by = " order by v.TransactionNo ASC";
            $query = "Select coa.AccountNo,coa.AccountDesc, sum(vd.Amount_Cr) as credit, sum(vd.Amount_Dr) as debit, ( Select sum(vd2.Amount_Cr) - sum(vd2.Amount_Dr) From voucher_detail vd2 JOIN voucher v2 ON v2.voucher_id = vd2.voucher_id WHERE STR_TO_DATE(v2.voucher_created_at,'%Y-%m-%d') < STR_TO_DATE('$from_date','%d-%m-%Y') AND vd.AccountNo = vd2.AccountNo ) as previous_balance, sum(vd.Amount_Cr) - sum(vd.Amount_Dr) as balance From voucher v JOIN voucher_detail vd ON v.voucher_id = vd.voucher_id JOIN chart_of_account coa ON coa.AccountNo = vd.AccountNo WHERE v.`flgDeleted` = 0 and (STR_TO_DATE(v.voucher_created_at,'%Y-%m-%d') between STR_TO_DATE('$from_date','%Y-%m-%d') and STR_TO_DATE('$to_date','%Y-%m-%d')) group by vd.AccountNo;";
            // print_b($query);
            $res['data'] = $this->basic_model->getCustomRows($query);
            $this->basic_model->make_pdf(1,$res,'Report','account_ledger_summary_report');
        } 
        
    }