<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class profile extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'profile/add';
        $this->view_product = 'profile/view';
        $this->edit_product = 'profile/edit';
        $this->delete_product = 'profile/delete';
        $this->detail_product = 'profile/detail';
        // page active
        $this->add_page_product = "add-profile";
        $this->edit_page_product = "edit-profile";
        $this->view_page_product = "view-profile";
        // table
        $this->table = "user";
        $this->table_pid = "user_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // FileUploader
        require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "User";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index(){
        redirect($this->edit_product);
    }
    function add(){
        extract($_POST);
        if( $this->input->post()){
            // print_b($_POST);
            if(empty($id)){
                redirect($this->edit_product);
            }
            else{
                if( empty($user_name) || empty($user_phone) || empty($user_email) || empty($user_password)){
                    echo json_encode(array("error" => "Please fill all fields"));
                }
                else{
                    $Checkemail = $this->basic_model->getRow($this->table, 'user_email', $user_email,$this->orderBy,$this->table_pid);
                    if(@$Checkemail != null && !empty(@$Checkemail)){
                        if ($Checkemail['user_email'] != $user_email_old) {
                            echo json_encode(array("error" => "E-mail is Exist"));
                            exit();
                        }
                    }

                    $Checkphone = $this->basic_model->getRow($this->table, 'user_phone', $user_phone,$this->orderBy,$this->table_pid);
                    if(@$Checkphone != null && !empty(@$Checkphone)){
                        if ($Checkphone['user_phone'] != $user_phone_old) {
                            echo json_encode(array("error" => "Phone Number is Exist"));
                            exit();
                        }
                    }

                    $pass = $this->basic_model->encode($user_password);
                    
                    $data = array(
                        // "user_name"=>$user_name,
                        // "user_email" => $user_email,
                        "user_password" => $pass,
                        // "user_phone" => $user_phone,
                        // "Display Name" => $display_name ,
                        // "Position Title" => $position_title ,
                        // "Department" => $department ,
                        // "quotation_email" => isset($quotation_email)? $quotation_email : 0,
                        // "invoice_email" => isset($invoice_email)? $invoice_email : 0,
                        // "user_is_active" => $user_is_active 



                    );

                    if(@$_FILES['fileToUpload']['name'][0] != '' && isset($_FILES['fileToUpload']['name'][0])){

                        $FileUploader = new FileUploader('fileToUpload', array(
                            'uploadDir' => $this->image_upload_dir."/",
                            'title' => 'auto',
                        ));
                        
                        $datamy = $FileUploader->upload();

                        $data['user_image'] = $datamy['files'][0]['name'];
                    }
                    
                    $sess_data = array("admin_id"=>$id,"admin_email"=>$user_email,"admin_password"=>$user_password,"admin_is_logged"=>1,"admin_status"=>1);
                    $this->session->set_userdata($sess_data);

                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->edit_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        else{
            redirect($this->edit_product);
        }
    }

    function edit(){
        $id = $this->session->userdata('admin_id');
        $res["title"] = "Edit ".$this->page_heading;
        $res["page_title"] =  "";
        $res["page_heading"] = $this->page_heading;
        $res["active"] = $this->edit_page_product;
        $res["add_product"] = $this->add_product;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res["data"] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
        // print_b($res["data"]);
        $res["passwords"] = $this->basic_model->decode($res["data"]['user_password']);
        $res['departmentData'] = $this->basic_model->getCustomRows("SELECT * FROM `department` ORDER BY `department_id` DESC");
        // print_b($res);
        $this->template_view($this->add_page_product,$res);
    }
    
    function imageDelete(){
        extract($_POST);
        // print_b($_POST);
        $file_path = dir_path().$this->image_upload_dir.'/'.@$file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $arr = array(
            'user_image' => ''
        );
        $updateRecord = $this->basic_model->updateRecord($arr, $this->table, $this->table_pid, $data['image_post_file_id']);
    }
}