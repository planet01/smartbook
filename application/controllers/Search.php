<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class search extends MY_Back_Controller
{
    public function __construct()
    {
        parent::__construct();
        // page action
        $this->add_product = 'search/add';
        $this->view_product = 'search/view';
        $this->edit_product = 'search/edit';
        $this->delete_product = 'search/delete';
        $this->detail_product = 'search/detail';
        // page active
        $this->add_page_product = "add-search";
        $this->edit_page_product = "edit-search";
        $this->view_page_product = "view-search";
        // table
        $this->table = "search";
        $this->table_pid = "id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/product";
        // Page Heading
        $this->page_heading = "search";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }
    }
    function index()
    {
        redirect($this->add_product);
    }
    function add()
    {
        extract($_POST);

        if ($this->input->post()) {
            if (empty($id)) {

                if (empty($product_id) || empty($quantity) || empty($warehouse_id) || empty($floor_no) || empty($shelf_no)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else {
                    $str = $product_id;
                    $pro_sku = (explode(",", $str));
                    $data = array(
                        "product_id" => $pro_sku[0],
                        "product_sku" => $pro_sku[1],
                        "quantity" => $quantity,
                        "warehouse_id" => $warehouse_id,
                        "floor_no" => $floor_no,
                        "shelf_no" => $shelf_no,
                        "status" => $status
                    );


                    $id = $this->basic_model->insertRecord($data, $this->table);


                    echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            } else {
                if (empty($product_id) || empty($quantity) || empty($warehouse_id) || empty($floor_no) || empty($shelf_no)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else {
                    $str = $product_id;
                    $pro_sku = (explode(",", $str));
                    $data = array(
                        "product_id" => $pro_sku[0],
                        "product_sku" => $pro_sku[1],
                        "quantity" => $quantity,
                        "warehouse_id" => $warehouse_id,
                        "floor_no" => $floor_no,
                        "shelf_no" => $shelf_no,
                        "status" => $status
                    );


                    $id = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);

                    echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        } else {
            $res['title'] = "Add New " . $this->page_heading;
            $res["page_title"] = "add";
            $res["page_heading"] = $this->page_heading;
            $res['active'] = $this->add_page_product;
            $res["add_product"] = $this->add_product;
            $res['countries'] = $this->basic_model->countries;
            $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `status` = 1 ");
            $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` WHERE `status` = 1 ");
            //print_b($res);
            $this->template_view($this->add_page_product, $res);
        }
    }

    function edit($id)
    {
        extract($_POST);
        $res["title"] = "Edit " . $this->page_heading;
        $res["page_title"] =  "";
        $res["page_heading"] = $this->page_heading;
        $res["active"] = $this->edit_page_product;
        $res["add_product"] = $this->add_product;
        $res['image_upload_dir'] = $this->image_upload_dir;

        $res["data"] = $this->basic_model->getRow($this->table, $this->table_pid, $id);

        $res['warehouseData'] = $this->basic_model->getCustomRows("SELECT * FROM `warehouse`  WHERE `status` = 1 ");
        $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` WHERE `status` = 1 ");
        $this->template_view($this->add_page_product, $res);
    }

    function view()
    {
        extract($_GET);
        // print_b($_GET);

        $res['title'] = "View All " . $this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res['image_upload_dir'] = $this->image_upload_dir;


        if (!empty($product_id) && !empty($product_sku) && !empty($warehouse_id)) {

            $query = "SELECT product.product_id,`product`.`product_sku` as p_model, images.image as product_image, inventory.*,product.*,warehouse.*
                FROM  `product`  
                RIGHT JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                Left JOIN `images` ON images.product_id = product.product_id
                RIGHT JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id
                WHERE product.product_id = '" . $product_id . "'
                AND product.product_sku ='" . $product_sku . "'
                AND warehouse.warehouse_id ='" . $warehouse_id . "' GROUP BY inventory.warehouse_id";

            $res['data'] = $this->basic_model->getCustomRows($query);
            $res['warefilter'] = $warehouse_id;
        } elseif (!empty($product_sku) && empty($product_id) && empty($warehouse_id)) {

            $res['data'] = $this->basic_model->getCustomRows(" 
            SELECT product.product_id,`product`.`product_sku` as p_model, inventory.*,product.*,warehouse.*
                    
                FROM  `product`  
                
                LEFT JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                
                LEFT JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id
                WHERE product.product_sku ='" . $product_sku . "' GROUP BY inventory.warehouse_id");
        } elseif (!empty($product_id) && empty($product_sku) && empty($warehouse_id)) {
           /* $query = "SELECT `product`.`product_sku` as p_model, inventory.*,product.*,warehouse.*  FROM  `product` LEFT JOIN `inventory`  ON  product.product_id = inventory.product_id LEFT JOIN `warehouse` ON inventory.warehouse_id = warehouse.warehouse_id WHERE product.product_id = '" . $product_id . "' GROUP BY product_name";*/
            $query = "SELECT product.product_id,`product`.`product_sku` as p_model, images.image as product_image, inventory.*, warehouse.*, product.*, images.* FROM `inventory` RIGHT JOIN `warehouse` ON warehouse.warehouse_id = inventory.warehouse_id RIGHT JOIN `product` ON inventory.product_id = product.product_id  Left JOIN `images` ON images.product_id = product.product_id WHERE product.product_id = '" . $product_id . "' GROUP BY warehouse.warehouse_id, product.product_id";
            $res['data'] = $this->basic_model->getCustomRows($query);
        } elseif (!empty($warehouse_id) && empty($product_id) && empty($product_sku)) {

            /*$res['data'] = $this->basic_model->getCustomRows(" 
            SELECT `product`.`product_sku` as p_model, inventory.*,product.*,warehouse.*
                    
                FROM  `product`  
                
                LEFT JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                
                LEFT JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id
                WHERE warehouse.warehouse_id ='" . $warehouse_id . "' GROUP BY inventory.warehouse_id");*/
            $res['data'] = $this->basic_model->getCustomRows(" SELECT product.product_id,`product`.`product_sku` as p_model,images.image as product_image,  inventory.*,product.*,warehouse.*
                    
                FROM  `product`  
                
                Right JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                
                Right JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id
                Left JOIN `images` ON images.product_id = product.product_id
                WHERE warehouse.warehouse_id ='" . $warehouse_id . "' GROUP BY inventory.warehouse_id, product.product_id");
        } elseif (empty($product_id) && empty($product_sku) && empty($warehouse_id)) {

            $res['data'] = $this->basic_model->getCustomRows(" 
            SELECT product.product_id,`product`.`product_sku` as p_model, images.image as product_image, inventory.*,product.*,warehouse.*
                    
                FROM  `product`  
                
                LEFT JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                Left JOIN `images` ON images.product_id = product.product_id
                LEFT JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id GROUP BY inventory.warehouse_id, product.product_id");
        } elseif (!empty($product_id) && !empty($product_sku) && empty($warehouse_id)) {

            $query = "SELECT product.product_id,`product`.`product_sku` as p_model, inventory.*,product.*,warehouse.*
                    
                FROM  `product`  

                RIGHT JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                
                RIGHT JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id

                WHERE product.product_id ='" . $product_id . "'
                AND product.product_sku ='" . $product_sku . "' GROUP BY inventory.warehouse_id";

            $res['data'] = $this->basic_model->getCustomRows($query);
        } elseif (!empty($product_id) && empty($product_sku) && !empty($warehouse_id)) {

            $query = "SELECT product.product_id,`product`.`product_sku` as p_model, images.image as product_image, inventory.*,product.*,warehouse.*
                    
                FROM  `product`  

                RIGHT JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                
                RIGHT JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id
                Left JOIN `images` ON images.product_id = product.product_id

                WHERE product.product_id ='" . $product_id . "'
                AND warehouse.warehouse_id ='" . $warehouse_id . "' GROUP BY inventory.warehouse_id";

            $res['data'] = $this->basic_model->getCustomRows($query);

            $res['warefilter'] = $warehouse_id;
        } elseif (empty($product_id) && !empty($product_sku) && !empty($warehouse_id)) {

            $query = "SELECT product.product_id,`product`.`product_sku` as p_model, inventory.*,product.*,warehouse.*
                    
                FROM  `product`  

                RIGHT JOIN `inventory`  
                ON  product.product_id = inventory.product_id
                
                RIGHT JOIN `warehouse` 
                ON inventory.warehouse_id = warehouse.warehouse_id

                WHERE product.product_sku ='" . $product_sku . "'
                AND warehouse.warehouse_id ='" . $warehouse_id . "' GROUP BY inventory.warehouse_id";

            $res['data'] = $this->basic_model->getCustomRows($query);
            $res['warefilter'] = $warehouse_id;
        }

        // print_b($res);

        $this->template_view($this->view_page_product, $res);
    }
    function delete($id)
    {
        $del = $this->basic_model->deleteRecord('id', $id, 'inventory');
        echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
    }

    function detail($id)
    {
        if (isset($id) && $id != '') {

            $res_int = $this->basic_model->getRow($this->table, $this->table_pid, $id);
            $table_w = "warehouses";
            $table_wid = "id";
            $table_s = "product";
            $table_sid = "id";

            $res_war = $this->basic_model->getRow($table_w, $table_wid, $res_int['warehouse_id']);
            $res_pro = $this->basic_model->getRow($table_s, $table_sid, $res_int['product_id']);

            if ($res_int) {
                $outputTitle = "";
                $outputTitle .= "Inventory Detail";

                $outputDescription = "";
                $outputDescription .= "<p>PRODUCT NAME  : " . $res_pro['name'] . "</p>";
                $outputDescription .= "<p>PRODUCT SKU  : " . $res_pro['sku'] . "</p>";
                $outputDescription .= "<p>QUANTITY : " . $res_int['quantity'] . "</p>";
                $outputDescription .= "<p>WARE HOUSE : " . $res_war['name'] . "</p>";
                $outputDescription .= "<p>FLOOR : " . $res_int['floor_no'] . "</p>";
                $outputDescription .= "<p>SHELF : " . $res_int['shelf_no'] . "</p>";
                $outputDescription .= "<p>Status : " . (($res_int['status'] == 1) ? '<span class="label label-success">Enabled</span>' : '<span class="label label-danger">Disabled</span>') . "</p>";


                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            } else {
                echo json_encode(array('error' => 'Not Found ' . $this->page_heading . '.'));
            }
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be deleted."));
        }
    }
}
