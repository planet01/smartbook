<?php
/* @property mpdf_model $mpdf_model */
class Print_pdf extends CI_Controller{
    function __construct(){
        parent::__construct();
        //require(APPPATH."third_party/mpdf/mpdf.php");
    }
    public function index()
    {
        $mpdf = new \Mpdf\Mpdf();
        $header = $this->load->view('include/header_report',$res,true);
        $content = $this->load->view('Report',[],true);
        $footer = $this->load->view('include/footer_report',[],true);
        $html = $this->load->view('Report',[],true);
        $mpdf->defaultheaderline = 0;
        $mpdf->defaultfooterline = 0;
        $mpdf->SetTitle('Report');
        $mpdf->SetHeader($header);
        $mpdf->SetFooter($footer);
        $mpdf->AddPage('', // L - landscape, P - portrait 
        '', '', '', '',
        0, // margin_left
        0, // margin right
       40, // margin top
        0, // margin bottom
        0, // margin header
        0); // margin footer
        $mpdf->WriteHTML($content);
        $mpdf->Output(); // opens in browser
        //$mpdf->Output('arjun.pdf','D'); // it downloads the file into the user system, with give name
    }   
}
/* End of file dashboard.php */
/* Location: ./system/application/modules/matchbox/controllers/dashboard.php */