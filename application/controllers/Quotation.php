<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quotation extends MY_Back_Controller
{
    public function __construct()
    {
        parent::__construct();
        // page action
        //$this->add_product = 'quotation/add';
        $this->save_product = 'quotation/add';

        $this->view_product = 'quotation/view';

        $this->edit_product = 'quotation/edit';
        $this->edit_status = 'quotation/change_status';
        $this->print = 'quotation/print';
        $this->mail = 'quotation/mail';
        $this->delete_product = 'quotation/delete';
        $this->detail_product = 'quotation/detail';
        $this->history_product = 'quotation/history';
        $this->add_customer = 'customer/add';

        $this->edit_payment = 'payment/edit';
        $this->delete_payment = "payment/delete";
        $this->print_payment = 'receivables/print';

        // page active
        $this->add_page_product = "add-quotation";
        $this->edit_page_product = "edit-quotation";
        $this->view_page_product = "view-quotation";
        // table
        $this->table = "quotation";
        $this->table_detail = "quotation_detail";
        $this->tablep = "product";
        $this->tables = "setting";

        $this->table_id = "quotation_id";
        $this->table_pid = "product_id";
        $this->table_fid = "quotation_id";
        $this->table_sid = "setting_id";
        $this->orderBy = "DESC";
        // image_upload_dir
        $this->image_upload_dir = "uploads/product";
        // Page Heading
        $this->page_heading = "Quotation";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        }

        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_print = false;
        $this->check_status = false;
        $this->check_payment = false;
        $this->check_history = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 9) {
                    if ($v['visible'] == 1) {
                        $this->check_view = true;
                    }
                    if ($v['add'] == 1) {
                        $this->check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $this->check_edit = true;
                    }
                    if ($v['print'] == 1) {
                        $this->check_print = true;
                    }
                    if ($v['status'] == 1) {
                        $this->check_status = true;
                    }
                    if ($v['payment'] == 1) {
                        $this->check_payment = true;
                    }
                    if ($v['history'] == 1) {
                        $this->check_history = true;
                    }
                }
            }
        } else {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_print = true;
            $this->check_status = true;
            $this->check_payment = true;
            $this->check_history = true;
        }
    }
    function index()
    {
        if ($this->check_view) {
            redirect($this->add_product);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }
    function add()
    {
        extract($_POST);
        if ($this->input->post()) {
            if (empty($id)) {
                //die($Checkquotation_no['quotation_no']);
                if (empty($customer_id) || empty($quotation_no) || empty($quotation_date) ||  empty($quotation_expiry_date)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else if (!isset($project_terms) || !isset($terms) || empty($project_terms) || empty($terms)) {
                    echo json_encode(array("error" => "Please fill Payment Terms & Project Duration grid."));
                } else {

                    $Checkquotation_no = $this->basic_model->getRow($this->table, 'quotation_no', $quotation_no, $this->orderBy, $this->table_id);
                    if (@$Checkquotation_no != null && !empty(@$Checkquotation_no)) {
                        echo json_encode(array("error" => "Quotation No. is Exist"));
                        exit();
                        return "";
                    }

                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            echo json_encode(array("error" => "Duplicate product is not allowed."));
                            return false;
                            /* foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $quotation_detail_quantity[$k];
                            }*/
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'quotation_detail_quantity' => $quantityTotal_new,
                            );
                        }



                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                );
                            }
                        }

                        foreach ($product_check as $k => $v) {
                            /* $q = "SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1";
                            // echo $q;
                            

                            $inventory_quantity_data = $this->basic_model->getCustomRow($q);*/
                            /*$inventory_quantity_data = inventory_quantity_by_product($v['product_id']);

                            if ($inventory_quantity_data['total_inventory_quantity'] <= $v['quotation_detail_quantity'] && $inventory_quantity_data['can_deliver'] == 0) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                            }
                            break;*/
                        }

                        // product_check end.
                    }
                    $quotation_date_year = explode("-",$quotation_date)[0];
                    $quotation_no = serial_no($quotation_date_year);

                    $data = array(
                        'customer_id' => $customer_id,
                        'employee_id' => $employee_id,
                        'quotation_no' => $quotation_no,
                        'quotation_date' => $quotation_date,
                        'quotation_expiry_date' => $quotation_expiry_date,
                        'quotation_total_discount_type' => empty($quotation_total_discount_type) ? 0 : $quotation_total_discount_type,
                        'quotation_total_discount_amount' => empty($quotation_total_discount_amount) ? 0 : $quotation_total_discount_amount,
                        'quotation_discount_notes' => @$quotation_discount_notes,
                        'quotation_buypack_discount' => empty($quotation_buypack_discount) ? 0 : $quotation_buypack_discount,
                        'quotation_currency' => $quotation_currency,
                        'quotation_freight_type' => @$quotation_freight_type,
                        'quotation_validity' => $quotation_validity,
                        'quotation_support' => $quotation_support,
                        'quotation_warranty' => $quotation_warranty,
                        'quotation_cover_letter' => $quotation_cover_letter,
                        'quotation_detail' => $quotation_detail,
                        'quotation_terms_conditions' => $quotation_terms_conditions,
                        'net_amount' => $total,
                        'subtotal' => $subtotal,
                        'quotation_tax' => @$quotation_tax,
                        'quotation_tax_amount' => @$quotation_tax_amount,
                        'quotation_report_line_break' => @$quotation_report_line_break,
                        'quotation_report_line_break_payment' => @$quotation_report_line_break_payment
                        // 'quotation_report_line_break_payment_pi' => @$quotation_report_line_break_payment_pi


                    );
                    /* echo "<pre>";
                    print_r($data);
                    echo "<pre>";*/


                    $quotation_id = $this->basic_model->insertRecord($data, $this->table);

                    $customer_data = $this->basic_model->getCustomRow("SELECT * FROM `user` LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user_id = '" . $customer_id . "' AND user_type = 3 AND `user_is_active` = 1 ");
                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($product_id as $k => $v) {
                            // if ($quotation_detail_quantity[$k]>0) {
                            /*$price_data = $this->basic_model->getCustomRow("SELECT price.* FROM `product` `p` RIGHT JOIN `price` ON `price`.`prod_id` = `p`.`product_id` WHERE `p`.product_is_active = 1 AND `price`.`prod_id` = '".$product_id[$k]."' AND `price`.`plan_id` = '".$customer_data['plan_type']."'");
                                $price_base = strtolower($customer_data['title']).'_base_price';
                                $price_usd = strtolower($customer_data['title']).'_usd_price';
                                $find_base = (empty($price_data[$price_base]) || $price_data[$price_base] == null) ? 0 : $price_data[$price_base];
                                $find_usd = (empty($price_data[$price_usd]) || $price_data[$price_usd] == null) ? 0 : $price_data[$price_usd];*/
                            if ($quotation_currency == 2) {
                                $base_price[$k] = $quotation_detail_rate[$k];
                            } else if ($quotation_currency == 1) {
                                $usd_price[$k] = $quotation_detail_rate[$k];
                            }

                            $arr = array(
                                'product_id' => $product_id[$k],
                                'quotation_id' => $quotation_id,
                                'quotation_detail_description' => $quotation_desc[$k],
                                'quotation_detail_total_discount_type' => $quotation_detail_total_discount_type[$k],
                                'quotation_detail_total_discount_amount' => empty($quotation_detail_total_discount_amount[$k]) ? 0 : $quotation_detail_total_discount_amount[$k],
                                'quotation_detail_total_amount' => $amount[$k],
                                'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                'quotation_detail_rate' =>     $base_price[$k],
                                'quotation_detail_rate_usd' => $usd_price[$k],
                                'quotation_detail_optional' => @$quotation_detail_optional[$k],
                                'quotation_detail_existing' => @$quotation_detail_existing[$k],
                                'quotation_detail_provided' => @$quotation_detail_provided[$k],
                                'quotation_detail_required' => @$quotation_detail_required[$k],
                                'quotation_detail_max_discount' => @$quotation_detail_max_discount[$k],
                                'quotation_detail_note' => @$quotation_detail_note[$k],
                            );
                            $i++;
                                /*echo '<pre>';
                                 print_r($arr)*/;
                            $quotation_detail_id = $this->basic_model->insertRecord($arr, $this->table_detail);
                            // }
                        }
                    }
                    if (isset($terms) && @$terms != null) {
                        for ($i = 0; $i < sizeof($terms); $i++) {
                            $payment_data = [
                                'quotation_id' => $quotation_id,
                                'payment_title' => $terms[$i],
                                'percentage' => $percentage[$i],
                                'payment_days' => @$payment_days[$i],
                                'payment_amount' => @$payment_amount[$i]
                            ];
                            $this->basic_model->insertRecord($payment_data, 'quotation_payment_term');
                        }
                    }

                    if (isset($project_terms) && @$project_terms != null) {
                        for ($i = 0; $i < sizeof($project_terms); $i++) {
                            $project_data = [
                                'quotation_id' => $quotation_id,
                                'project_title' => $project_terms[$i],
                                'start_week' => $start_week[$i],
                                'project_days' => $project_days[$i],
                                'hash_code' => $hash_code[$i],
                            ];
                            $this->basic_model->insertRecord($project_data, 'quotation_project_term');
                        }
                    }

                    //Warranty
                    // $types = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    // for($i= 0; $i< sizeof($features); $i++) {
                    //     $data_features = [
                    //         'quotation_id' => $quotation_id,
                    //         'title' => $features[$i]
                    //     ];
                    //     $warn_id = $this->basic_model->insertRecord($data_features,'quotation_warranty');
                    //     $c = 1;
                    //     foreach($types as $type){
                    //         $data_setting = [
                    //             'warn_id' => $warn_id,
                    //             'quotation_id' => $quotation_id,
                    //             'type_id' => $type['warranty_type_id'],
                    //             'value'   => $_POST['warranty_type_id'.$c][$i],
                    //             'percentage' => $_POST['warranty_percentage'.$c][$i]
                    //         ];
                    //         $c++;
                    //        $this->basic_model->insertRecord($data_setting,'quotation_warranty_setting');
                    //     }
                    // }
                    echo json_encode(array("success" => "Record Inserted successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            } else {
                if (empty($customer_id) || empty($quotation_no) || empty($quotation_date) ||  empty($quotation_expiry_date)) {
                    echo json_encode(array("error" => "Please fill all fields"));
                } else if (!isset($project_terms) || !isset($terms) || empty($project_terms) || empty($terms)) {
                    echo json_encode(array("error" => "Please fill Payment Terms & Project Duration grid."));
                } else {


                    //History
                    //make_quotation_history($id);    
                    //End History

                    $Checkquotation_no = $this->basic_model->getRow($this->table, 'quotation_no', $quotation_no, $this->orderBy, $this->table_id);

                    if (@$Checkquotation_no != null && !empty(@$Checkquotation_no)) {
                        if ($Checkquotation_no['quotation_no'] != $quotation_no_old && $Checkquotation_no['quotation_revised_id'] == '0') {
                            echo json_encode(array("error" => "Quotation No. is Exist"));
                            exit();
                            return "";
                        }
                    }


                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {

                        // product_check start
                        $product_data = $this->basic_model->array_unique_not_unique_value($product_id);
                        $product_check = array();
                        // print_b($product_data);
                        $productCount = 0;
                        $quantityTotal_new = 0;
                        if (isset($product_data['duplicate']) && @$product_data['duplicate'] != null) {
                            echo json_encode(array("error" => "Duplicate product is not allowed."));
                            return false;
                            /*foreach ($product_data['duplicate'] as $k => $v) {
                                $product_id_new = $v;
                                $quantityTotal_new += $quotation_detail_quantity[$k];
                            }
                            $product_check[$productCount] = array(
                                'product_id' => $product_id_new,
                                'quotation_detail_quantity' => $quantityTotal_new,
                            );*/
                        }



                        if (isset($product_data['unique']) && @$product_data['unique'] != null) {
                            foreach ($product_data['unique'] as $k => $v) {
                                $productCount++;
                                $product_check[$productCount] = array(
                                    'product_id' => $v,
                                    'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                );
                            }
                        }
                        foreach ($product_check as $k => $v) {
                            /*$q = "SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `inventory`.`revision` =0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `inventory`.`revision` = 0 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1";
                            // echo $q;
                            // die;

                            $inventory_quantity_data = $this->basic_model->getCustomRow($q);*/
                            /*$inventory_quantity_data = inventory_quantity_by_product($v['product_id']);
                            if ($inventory_quantity_data['total_inventory_quantity'] < $v['quotation_detail_quantity'] && $inventory_quantity_data['can_deliver'] == 0) {
                                echo json_encode(array("error" => $inventory_quantity_data['product_name']." Has Been Out Of Stock."));
                                exit();
                                return "";
                            }
                            break;*/
                        }

                        // product_check end.
                    }

                    $data = array(
                        'customer_id' => $customer_id,
                        'employee_id' => $employee_id,
                        'quotation_no' => $quotation_no,
                        'quotation_date' => $quotation_date,
                        'quotation_expiry_date' => $quotation_expiry_date,
                        'quotation_total_discount_type' => empty($quotation_total_discount_type) ? 0 : $quotation_total_discount_type,
                        'quotation_total_discount_amount' => empty($quotation_total_discount_amount) ? 0 : $quotation_total_discount_amount,
                        'quotation_discount_notes' => $quotation_discount_notes,
                        'quotation_buypack_discount' => empty($quotation_buypack_discount) ? 0 : $quotation_buypack_discount,
                        'quotation_currency' => $quotation_currency,
                        'quotation_freight_type' => @$quotation_freight_type,
                        //'quotation_revised_no' => $quotation_revised_no,
                        'quotation_validity' => $quotation_validity,
                        'quotation_support' => $quotation_support,
                        'quotation_warranty' => $quotation_warranty,
                        'quotation_cover_letter' => $quotation_cover_letter,
                        'quotation_detail' => $quotation_detail,
                        //'quotation_status' => 4,
                        //'quotation_email_printed_status' => 0,
                        'quotation_terms_conditions' => $quotation_terms_conditions,
                        'net_amount' => $total,
                        'subtotal' => $subtotal,
                        'quotation_tax' => @$quotation_tax,
                        'quotation_tax_amount' => @$quotation_tax_amount,
                        'quotation_report_line_break' => @$quotation_report_line_break,
                        'quotation_report_line_break_payment' => @$quotation_report_line_break_payment
                        // 'quotation_report_line_break_payment_pi' => @$quotation_report_line_break_payment_pi

                    );
                    if ($quotation_email_printed_status > 0) {
                        //$quotation_revised_no
                        if ($quotation_status > 3 || $quotation_status == 1) {
                            //History
                            make_quotation_history($id);
                            //End History
                            $data['quotation_status'] = 2;
                            $data['quotation_revised_no'] = $quotation_revised_no;
                        }
                    }
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);

                    $customer_data = $this->basic_model->getCustomRow("SELECT * FROM `user` LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user_id = '" . $customer_id . "' AND user_type = 3 AND `user_is_active` = 1 ");
                    $del = $this->basic_model->deleteRecord($this->table_fid, $id, $this->table_detail);
                    // print_b($quantity);
                    if (isset($quotation_detail_quantity) && @$quotation_detail_quantity != null && (isset($product_id) && @$product_id != null)) {
                        $i = 0;
                        foreach ($quotation_detail_quantity as $k => $v) {
                            if ($quotation_detail_quantity[$k] > 0 || $quotation_detail_quantity[$k] == 'lot' || $quotation_detail_quantity[$k] == 'Lot') {
                                /* $price_data = $this->basic_model->getCustomRow("SELECT price.* FROM `product` `p` RIGHT JOIN `price` ON `price`.`prod_id` = `p`.`product_id` WHERE `p`.product_is_active = 1 AND `price`.`prod_id` = '".$product_id[$k]."' AND `price`.`plan_id` = '".$customer_data['plan_type']."'");
                                $price_base = strtolower($customer_data['title']).'_base_price';
                                $price_usd = strtolower($customer_data['title']).'_usd_price';
                                $find_base = (empty($price_data[$price_base]) || $price_data[$price_base] == null) ? 0 : $price_data[$price_base];
                                $find_usd = (empty($price_data[$price_usd]) || $price_data[$price_usd] == null) ? 0 : $price_data[$price_usd];*/

                                if ($quotation_currency == 2) {
                                    $base_price[$k] = $quotation_detail_rate[$k];
                                } else if ($quotation_currency == 1) {
                                    $usd_price[$k] = $quotation_detail_rate[$k];
                                }
                                $arr = array(
                                    'product_id' => $product_id[$k],
                                    'quotation_id' => $id,
                                    'quotation_detail_description' => $quotation_desc[$k],
                                    'quotation_detail_total_discount_type' => $quotation_detail_total_discount_type[$k],
                                    'quotation_detail_total_discount_amount' => empty($quotation_detail_total_discount_amount[$k]) ? 0 : $quotation_detail_total_discount_amount[$k],
                                    'quotation_detail_total_amount' => $amount[$k],
                                    'quotation_detail_quantity' => $quotation_detail_quantity[$k],
                                    'quotation_detail_rate' => $base_price[$k],
                                    'quotation_detail_rate_usd' => $usd_price[$k],
                                    'quotation_detail_optional' => empty($quotation_detail_optional[$k]) ? 0 : $quotation_detail_optional[$k],
                                    'quotation_detail_existing' => empty($quotation_detail_existing[$k]) ? 0 : $quotation_detail_existing[$k],
                                    'quotation_detail_provided' => empty($quotation_detail_provided[$k]) ? 0 : $quotation_detail_provided[$k],
                                    'quotation_detail_required' => empty($quotation_detail_required[$k]) ? 0 : $quotation_detail_required[$k],
                                    'quotation_detail_max_discount' => empty($quotation_detail_max_discount[$k]) ? 0 : $quotation_detail_max_discount[$k],
                                    'quotation_detail_note' => @$quotation_detail_note[$k],

                                );
                                $i++;
                                //echo '<pre>';
                                //print_r($arr);
                                $quotation_detail_id = $this->basic_model->insertRecord($arr, $this->table_detail);
                            }
                        }
                    }

                    $this->basic_model->customQueryDel("DELETE FROM `quotation_payment_term` where quotation_id=" . $id);
                    if (isset($terms) && @$terms != null) {

                        for ($i = 0; $i < sizeof($terms); $i++) {
                            $payment_data = [
                                'quotation_id' => $id,
                                'payment_title' => $terms[$i],
                                'percentage' => $percentage[$i],
                                'payment_days' => @$payment_days[$i],
                                'payment_amount' => @$payment_amount[$i],

                            ];
                            $this->basic_model->insertRecord($payment_data, 'quotation_payment_term');
                        }
                    }

                    $this->basic_model->customQueryDel("DELETE FROM `quotation_project_term` where quotation_id=" . $id);
                    if (isset($project_terms) && @$project_terms != null) {

                        for ($i = 0; $i < sizeof($project_terms); $i++) {
                            $project_data = [
                                'quotation_id' => $id,
                                'project_title' => $project_terms[$i],
                                'start_week' => $start_week[$i],
                                'project_days' => $project_days[$i],
                                'hash_code' => $hash_code[$i],
                            ];
                            $this->basic_model->insertRecord($project_data, 'quotation_project_term');
                        }
                    }


                    //Warranty
                    // $types = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    // $this->basic_model->customQueryDel("DELETE FROM `quotation_warranty` where quotation_id=".$id);
                    // $this->basic_model->customQueryDel("DELETE FROM `quotation_warranty_setting` where quotation_id=".$id);
                    // for($i= 0; $i< sizeof($features); $i++) {
                    //     $data_features = [
                    //         'quotation_id' => $id,
                    //         'title' => $features[$i]
                    //     ];
                    //     $warn_id = $this->basic_model->insertRecord($data_features,'quotation_warranty');
                    //     $c = 1;
                    //     foreach($types as $type){
                    //         $data_setting = [
                    //             'warn_id' => $warn_id,
                    //             'quotation_id' => $id,
                    //             'type_id' => $type['warranty_type_id'],
                    //             'value'   => $_POST['warranty_type_id'.$c][$i],
                    //             'percentage' => $_POST['warranty_percentage'.$c][$i]
                    //         ];
                    //         $c++;
                    //        $this->basic_model->insertRecord($data_setting,'quotation_warranty_setting');
                    //     }
                    // }
                    // print_b($arr);


                    echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        } else {
            if ($this->check_add) {
                $data['title'] = "Add New " . $this->page_heading;
                $data["page_title"] = "add";
                $data["page_heading"] = $this->page_heading;
                $data['active'] = $this->add_page_product;
                // $data["add_product"] = $this->add_product;
                $data["save_product"] = $this->save_product;
                $data['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $data['base_currency'] = $setting['title'];
                $data['base_currency_id'] = $setting['id'];
                $data['company_country'] = $setting['setting_company_country'];

                $data['currencyData'] = $this->basic_model->getCustomRows("SELECT * FROM `currency`  ");
                $data['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1");
                $data['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN country ON user.user_country = country.country_id  JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user.user_type = 3 AND user.`user_is_active` = 1 ");
                $data["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
                /*$data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p LEFT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` AND `i`.`inventory_type` = 1 AND `i`.`revision` = 0 AND p.product_is_active = 1 GROUP BY p.product_id  ORDER BY p.product_name ASC");*/
                $data['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` where product_is_active = 1  ORDER BY product_name ASC");
                // $data['quotation_no'] = str_pad(20, 5, '0', STR_PAD_LEFT);
                $data["add_customer"] = $this->add_customer;

                $data['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
                $data['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                // $data['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty');
                // $data['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, warranty_setting.*  from warranty_type Left Join warranty_setting on warranty_type.warranty_type_id = warranty_setting.type_id And warranty_type.deleted IS NULL');

                $data['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');
                $data['warranty'] = json_encode($data['warranty']);

                $data['quotation_validity'] = @$data["setval"]['validity_days'];
                $data['quotation_support']  = @$data["setval"]['support_days'];
                $data['quotation_warranty'] = @$data["setval"]['warranty_years'];
                $data["ckeditor"] = "yes";
                $data['ckeditor1'] = 'yes';
                $data['ckeditor2'] = 'yes';
                $data['ckeditor3'] = 'yes';
                $data['price_list'] = $this->basic_model->getCustomRows("SELECT * FROM  price_plan");
                $data['currency'] = $this->basic_model->getCustomRows('SELECT *  from currency');
                $data['customer_type'] = $this->basic_model->getCustomRows("SELECT * FROM  customer_type");
                $data["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
                $data['term_days'] = json_encode(array(
                    'validity' => $data['setval']['validity_days'],
                    'support'  => $data['setval']['support_days'],
                    'warranty' => $data['setval']['warranty_years'],
                ));
                // print_b($data['quotation_no']);
                $this->template_view($this->add_page_product, $data);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
        }
    }

    function edit($id)
    {
        if ($this->check_edit) {
            if (isset($id) && $id != '') {
                $res_edit = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
                if ($res_edit) {
                    extract($_POST);
                    $res["data"] = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
                    $res["quotation_detail_data"] = $this->basic_model->getRows($this->table_detail, $this->table_fid, $res["data"]['quotation_id'], "quotation_detail_id", "asc");

                    $res["title"] = "Edit " . $this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["save_product"] =  $this->save_product;
                    // $res['image_upload_dir'] = $this->image_upload_dir;
                    $res['countries'] = $this->basic_model->getCustomRows("SELECT * FROM  country");
                    $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1'");
                    $res['base_currency'] = $setting['title'];
                    $res['company_country'] = $setting['setting_company_country'];

                    $res['base_currency'] = $setting['title'];
                    $res['base_currency_id'] = $setting['id'];
                    $res['currencyData'] = $this->basic_model->getCustomRows("SELECT * FROM `currency`  ");
                    $res['employeeData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE user_type = 2 AND `user_is_active` = 1");
                    $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` LEFT JOIN country ON user.user_country = country.country_id LEFT JOIN `customer_type` `c` ON `user`.customer_type = `c`.id WHERE user.user_type = 3 AND user.`user_is_active` = 1 ");
                    $res['user_country'] = @$res['customerData'][0]['user_country'];
                    $res["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
                    $res['term_days'] = json_encode(array(
                        'validity' => $res['setval']['validity_days'],
                        'support' => $res['setval']['support_days'],
                        'warranty' => $res['setval']['warranty_years'],
                    ));

                    /*$res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` p LEFT JOIN `inventory` i ON `i`.`product_id` = `p`.`product_id` AND `i`.`inventory_type` = 1 AND `i`.`revision` = 0 AND p.product_is_active = 1 GROUP BY p.product_id ORDER BY p.product_name ASC");*/
                    $res['productData'] = $this->basic_model->getCustomRows("SELECT * FROM `product` where product_is_active = 1 ORDER BY product_name ASC");
                    $res["add_customer"] = $this->add_customer;

                    $res['terms'] = $this->basic_model->getCustomRows("SELECT *  from quotation_payment_term WHERE quotation_id = '" . $id . "' ");
                    $res['project_terms'] = $this->basic_model->getCustomRows("SELECT *  from quotation_project_term WHERE quotation_id = '" . $id . "'");
                    $res['ckeditor'] = 'yes';
                    $res['ckeditor1'] = 'yes';
                    $res['ckeditor2'] = 'yes';
                    $res['ckeditor3'] = 'yes';

                    // $res['warranty'] = $this->basic_model->getCustomRows("SELECT * from quotation_warranty where quotation_id = '".$id."'");
                    // $res['warranty_setting'] = $this->basic_model->getCustomRows("SELECT warranty_type.*, quotation_warranty_setting.*  from warranty_type Left Join quotation_warranty_setting on warranty_type.warranty_type_id = quotation_warranty_setting.type_id WHERE quotation_id = '".$id."' ");


                    //if term not enter add client term
                    if (empty($res['terms'])) {
                        $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id=' . $res["data"]['customer_id']);
                    }
                    if (empty($res['project_terms'])) {
                        //$res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from client_project_term where client_id='.$res["data"]['customer_id']);
                        $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                    }

                    //if client term also not entered add setting term
                    if (empty($res['terms'])) {
                        $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
                    }
                    if (empty($res['project_terms'])) {
                        $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                    }

                    $res['quotation_validity'] = @$res["data"]['quotation_validity'];
                    $res['quotation_support']  = @$res["data"]['quotation_support'];
                    $res['quotation_warranty'] = @$res["data"]['quotation_warranty'];

                    $res['price_list'] = $this->basic_model->getCustomRows("SELECT * FROM  price_plan");
                    $res['currency'] = $this->basic_model->getCustomRows('SELECT *  from currency');
                    $res['customer_type'] = $this->basic_model->getCustomRows("SELECT * FROM  customer_type");


                    $res['warranty'] = $this->basic_model->getCustomRows("SELECT * from client_warranty_type where client_id = '" . @$res["data"]['customer_id'] . "'");

                    if (empty($res['warranty'])) {
                        $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty_type where deleted IS NULL');
                    }

                    $res['warranty'] = json_encode($res['warranty']);
                    // $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');

                    // if(empty($res['warranty'])){
                    //     $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from client_warranty where client_id='.$res["data"]['customer_id']);
                    //     $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, client_warranty_setting.*  from warranty_type Left Join client_warranty_setting on warranty_type.warranty_type_id = client_warranty_setting.type_id');
                    //     $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                    // }

                    $this->template_view($this->add_page_product, $res);
                } else {
                    redirect($this->no_results_found);
                }
            } else {
                redirect($this->no_results_found);
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function progt()
    {
        if ($_POST['pid']) {
            //plan_id
            $data = array();
            // $query = "SELECT p.*,price.*, ( (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = `p`.`product_id` ) - ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` JOIN `price` ON `price`.`plan_id` = '".$_POST['plan_id']."' WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND (i.revision = 0 || i.revision IS NUll) AND `p`.`product_id` = '".$_POST['pid']."'";
            /* $query = "SELECT ct.*, p.*, price.*, ( ( SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`revision` = 0 AND `i2`.`product_id` = `p`.`product_id` ) -( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`revision` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` RIGHT JOIN `price` ON `price`.`plan_id` = '".$_POST['plan_id']."' JOIN `category_type` `ct` ON `ct`.`id` = `p`.`category_type` WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND( i.revision = 0 || i.revision IS NULL ) AND `p`.`product_id` = '".$_POST['pid']."' AND `price`.`prod_id` = '".$_POST['pid']."' ";*/
            $query = "SELECT ct.*, p.*, price.* FROM `product` `p` RIGHT JOIN `price` ON `price`.`plan_id` = '" . $_POST['plan_id'] . "' JOIN `category_type` `ct` ON `ct`.`id` = `p`.`category_type` WHERE  `p`.product_is_active = 1 AND `p`.`product_id` = '" . $_POST['pid'] . "' AND `price`.`prod_id` = '" . $_POST['pid'] . "' ";
            $data = $this->basic_model->getCustomRow($query);
            if ($data == '' || $data == null) {
                $query = "SELECT ct.*, p.*, price.* FROM `product` `p` RIGHT JOIN `price` ON `price`.`plan_id` = '" . $_POST['plan_id'] . "' JOIN `category_type` `ct` ON `ct`.`id` = `p`.`category_type` WHERE  `p`.product_is_active = 1 AND `p`.`product_id` = '" . $_POST['pid'] . "'";
                $data = $this->basic_model->getCustomRow($query);
                $data['reseller_base_price'] = 0;
                $data['reseller_usd_price']  = 0;
                $data['reseller_discount']   = 0;
                $data['retail_base_price']   = 0;
                $data['retail_usd_price']    = 0;
                $data['retail_discount']     = 0;
                $data['partner_base_price']  = 0;
                $data['partner_usd_price']   = 0;
                $data['partner_discount']    = 0;
            }
            if (isset($_POST['quotation_id']) && $_POST['quotation_id'] != '') {
                $quotation_detail_id = (isset($_POST['quotation_detail_id']) && $_POST['quotation_detail_id'] != 0) ? " AND quotation_detail_id= " . $_POST['quotation_detail_id'] : "";
                $query = "SELECT * from quotation_detail where quotation_id=" . $_POST['quotation_id'] . " AND product_id=" . $_POST['pid'] . " " . $quotation_detail_id;
                $price = $this->basic_model->getCustomRow($query);
                if (!empty($price) && sizeof($price) > 0) {
                    $data['reseller_base_price'] = @$price['quotation_detail_rate'];
                    $data['reseller_usd_price']  = @$price['quotation_detail_rate_usd'];
                    $data['retail_base_price']   = @$price['quotation_detail_rate'];
                    $data['retail_usd_price']    = @$price['quotation_detail_rate_usd'];
                    $data['partner_base_price']  = @$price['quotation_detail_rate'];
                    $data['partner_usd_price']   = @$price['quotation_detail_rate_usd'];
                }
            }
            echo json_encode($data);
        } else {
            echo 0;
        }
    }

    function customer_terms()
    {
        if ($_POST['cid']) {
            $id = $_POST['cid'];

            $data = array();
            $res = array();
            $terms = $this->basic_model->getCustomRows("SELECT *  from client_payment_term WHERE client_id = '" . $id . "' ");
            $res['customer'] = $this->basic_model->getCustomRow("SELECT *  from user where user_id=" . $id);
            $project_terms = $this->basic_model->getCustomRows('SELECT *  from project_terms');


            $res['warranty'] = $this->basic_model->getCustomRows("SELECT * from client_warranty_type where client_id = '" . $id . "'");
            // $warranty_setting = $this->basic_model->getCustomRows("SELECT warranty_type.*, client_warranty_setting.*  from warranty_type Left Join client_warranty_setting on warranty_type.warranty_type_id = client_warranty_setting.type_id WHERE client_id = '".$id."' ");
            if ($res['warranty'] == '') {
                $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');
            }
            $res['warranty'] = json_encode($res['warranty']);
            $html = '';
            if (@$terms == null || empty(@$terms)) {

                $terms =  $this->basic_model->getCustomRows("SELECT *  from payment_terms");
            }
            if (@$terms != null && !empty(@$terms)) {
                foreach ($terms as $row) {


                    $html .= "<tr class='txtMult'>";
                    $html .= "<td class='text-center' style='width:60px;''><a href='javascript:void(0);'' class='premCF'>Remove</a></td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='terms' required style='width:100%'' id='terms'  name='terms[]'' value='" . @$row['payment_title'] . "' placeholder='' />";
                    $html .= "</td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='numeric-align percentage txtboxToFilter' required style='width:100%''  data-optional='0' name='percentage[]' value='" . @$row['percentage'] . "' placeholder='' />";
                    $html .= "</td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='numeric-align payment_days txtboxToFilter' required style='width:100%' data-optional='0' name='payment_days[]' value='" . @$row['payment_days'] . "' placeholder='' />";
                    $html .= "</td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='numeric-align payment_amount txtboxToFilter' required readonly style='width:100%' data-optional='0' name='payment_amount[]' value='" . @$row['payment_amount'] . "' placeholder='' />";
                    $html .= "</td>";
                    $html .= "</tr>";
                }
                $res['payment_terms'] = $html;
            }

            $html = '';
            if (@$project_terms != null && !empty(@$project_terms)) {
                foreach ($project_terms as $row) {
                    $html .= "<tr class='new_txtMult'>";
                    $html .= "<td class='text-center' style='width:60px;'><a href='javascript:void(0);' class='new_remCF'>Remove</a></td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='project_terms' required style='width:100%' id='project_terms'  name='project_terms[]' value='" . @$row['project_title'] . "' placeholder='' />";
                    $html .= "</td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='hash_code' required  style='width:100%' data-optional='0' name='hash_code[]' value='" . @$row['hash_code'] . "' placeholder='' />";
                    $html .= "</td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='numeric-align code quantity txtboxToFilter start_week' required  style='width:100%' data-optional='0' name='start_week[]' value='" . @$row['start_week'] . "' placeholder='' />";
                    $html .= "</td>";
                    $html .= "<td>";
                    $html .= "<input type='text' class='numeric-align code quantity txtboxToFilter project_days' required  style='width:100%' data-optional='0' name='project_days[]' value='' placeholder='' />";
                    $html .= "</td>";
                    $html .= "</tr>";
                }
                $res['project_duration'] = $html;
            }

            // $html = '';
            // if(@$warranty != null && !empty(@$warranty)){
            //     $check_data = Array();
            //     $new_data   = Array();
            //     foreach($warranty as $warranty){


            //         $html .= "<tr class='warn_txtMult'>";
            //         $html .= "<td class='text-center' style='width:60px;'><a href='javascript:void(0);' class='warn_remCF'>Remove</a></td>";
            //         $html .= "<td>";
            //         $html .= "<input type='text' class='features' required style='width:100%' id='features' name='features[]' value='".@$warranty['title']."' placeholder='' />";
            //         $html .= "</td>";

            //         $c= 1;

            //         foreach(@$warranty_type as $type){
            //             $value = 2;
            //             $warn_percentage = 0;
            //             $find = true;
            //             foreach($warranty_setting as $setting){     
            //                 $tmp = isset($warranty['quotation_warranty_id'])?$warranty['quotation_warranty_id']:$warranty['warranty_id'];                           
            //                 if($tmp == $setting['warn_id'] && $type['warranty_type_id'] == $setting['type_id']){
            //                     $value = $setting['value'];
            //                     $warn_percentage = $setting['percentage'];
            //                 }
            //             }

            //             $html .= "<td>";
            //             $html .= "<select name='warranty_type_id". $c ."[]' style='width:100%' id='warranty_type_id' class='form-control warranty_type_id' required>";
            //             $html .= "<option value='' disabled='' selected=''>-- Select Option --</option>";
            //             $html .= "<option value='1' ";
            //             if( @$value == 1 ){
            //                 $html .= 'selected';
            //             } 
            //             $html .= ">Enabled</option>";
            //             $html .= "<option value='0' ";
            //             if( @$value == 1 ){
            //                 $html .= 'selected';
            //             } 
            //             $html .= ">Disabled</option>";
            //             $html .= "</select>";
            //             $html .= "</td>";
            //             $html .= "<td>";
            //             $html .= "<input type='text' class='warranty_percentage' required style='width:100%' id='warranty_percentage' name='warranty_percentage".$c."[]' value='". @$warn_percentage ."' placeholder='' />";
            //             $html .= "</td>";                                    


            //             $c++; 
            //         } 
            //         $html .= "</tr>";
            //     } 
            //     $res['warranty_terms'] = $html;
            // } 

            echo json_encode($res);
        } else {
            echo 0;
        }
    }

    function view()
    {
        $res['title'] = "View All " . $this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["save_product"] = $this->save_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res["print"] = $this->print;
        $res["mail"] = $this->mail;
        $res["edit_payment"] = $this->edit_payment;
        $res["delete_payment"] = $this->delete_payment;
        $res["print_payment"] = $this->print_payment;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['end_user'] = $this->basic_model->getCustomRows('SELECT *  from end_user');
        $res['customerData'] = $this->basic_model->getCustomRows("SELECT * FROM `user` WHERE `user_type` = 3 AND `user_is_active` = 1");
        $res['cityData'] = $this->basic_model->getCustomRows("SELECT * FROM city");
        $res['data'] = $this->basic_model->getCustomRows(" SELECT quotation.*,user.user_name, user.user_country FROM `quotation`  INNER JOIN `user`  ON quotation.customer_id = user.user_id WHERE quotation.quotation_revised_id = 0 order by quotation.quotation_id desc limit 100");
        $res['detail_data'] = $this->basic_model->getCustomRows("SELECT * FROM quotation_detail");
        $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
        $res['setting'] = $setting;
        $res['base_currency_id'] = $setting['id'];
        $res["currency_name"] = $setting['title'];
        $res["ckeditor"] = "yes";
        $res['history'] = $this->history_product;
        // print_b($res['data']);
        $res['edit_status'] = $this->edit_status;
        $this->template_view($this->view_page_product, $res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE quotation.quotation_revised_id = 0";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE(quotation.quotation_date,'%Y-%m-%d') >= STR_TO_DATE('$from_date','%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE(quotation.quotation_date,'%Y-%m-%d') <= STR_TO_DATE('$to_date','%Y-%m-%d')";
            }

            if($customer_id != 0)
            {
                $where .= " AND quotation.customer_id = ".$customer_id;
            }

            if($quotation_no != "")
            {
                $where .= " AND quotation.quotation_no LIKE '%".$quotation_no."%'";
            }
            

            $where .= " order by quotation.quotation_id desc limit 100";
            $query = "SELECT quotation.*,user.user_name, user.user_country FROM `quotation`  INNER JOIN `user`  ON quotation.customer_id = user.user_id  ".$where;
            
            $temp = $this->basic_model->getCustomRows($query);
            $res['data'] = array();
            $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
            $QuotationStatusData = QuotationStatusData();
            
            foreach (@$temp as $v) 
            {
                    $data = [];
                    $data['quotation_id'] = $v['quotation_id'];
                    $data['quotation_date'] = date("Y-m-d", strtotime($v["quotation_date"]));
                    $data['quotation_no'] = @$setting["company_prefix"] . @$setting["quotation_prfx"]. (($v['quotation_revised_no'] > 0) ? @$v['quotation_no'] . '-R' . number_format(@$v['quotation_revised_no']) : @$v['quotation_no']); 
                    $data['user_name'] = $v['user_name'];
                    $data['net_amount'] = quotation_num_format($v['net_amount']);
                    $data['quotation_status'] = $v['quotation_status'];
                    foreach ($QuotationStatusData as $k2 => $v2) {
                        if ($k2 == $v['quotation_status']) {
                            if ($k2 == 2) { 
                                $data['status_name'] = $v2 . '-' . $v['quotation_revised_no']; 
                            } else {
                                $data['status_name'] = $v2; 
                            }
                        }
                    }
                    $data['quotation_status_comment'] = $v['quotation_status_comment'];
                    $data['has_invoice'] = $v['has_invoice'];
                    $data['has_payment'] = $v['has_payment'];
                    
                    array_push($res['data'],$data);
            }
            
            echo json_encode($res);
        }
    }

    function change_status()
    {
        extract($_POST);
        $data = array();
        if (isset($id) && $id != null) {
            if ($status_comment == '' && $status !=  '4') {
                echo json_encode(array("error" => "Status comment field is required"));
                return '';
            }
            if ($po_no == '' && $status ==  '4') {
                echo json_encode(array("error" => "P.O.# field is required"));
                return '';
            }
            if ($status ==  '4') {
                $additional  = $this->basic_model->getCustomRow("
                  SELECT * FROM `quotation_detail` where `quotation_id` = '" . $id . "' AND (`quotation_detail_optional` = 1 || `quotation_detail_existing` = 1 || `quotation_detail_provided` = 1 || `quotation_detail_required` = 1)");
                if ($additional != '' && !empty($additional)) {
                    echo json_encode(array("popup" => true, "msg" => "Please remove optional and other items that are not part of P.I."));
                    return '';
                }
            }


            if ($status_comment != '') {
                $data['quotation_status_comment'] = $status_comment;
                //$this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                $data = array();
            }
            if ($po_no != '') {
                $data['quotation_po_no'] = $po_no;
            }
            //History
            make_quotation_history($id);
            //End History

            $detail = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
            $data['quotation_email_printed_status'] = 1;
            $data['quotation_final'] = 1;
            //$data['quotation_revised_no'] = $detail["quotation"]['quotation_revised_no'] + 1;
            // if($detail['quotation_status'] == 0)
            // {
            // $status = 1;
            $data['quotation_status'] = $status;
            $revised = ($detail['quotation_revised_no'] > 0) ? $detail['quotation_revised_no'] : 0;
            $data['quotation_revised_no'] = $revised + 1;
            $data['quotation_status_comment'] = $status_comment;
            $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);

            echo json_encode(array("success" => "Record updated successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
            // }
            // else
            // {
            //     echo json_encode(array("error" => "Status is Final, can't change."));
            // }


            // print_b($detail);

        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be Perform Any Action."));
        }
    }

    function print()
    {
        if ($this->check_print) {
            extract($_GET);
            // print_b($_GET);
            if (isset($id) && $id != null) {
                $history_table = "";
                if (isset($history) && $history == 1) {
                    $history_table = "_history";
                }
                $res["quotation"] = $this->basic_model->getRow($this->table . $history_table, $this->table_id, $id, $this->orderBy, $this->table_id);
                $res['customer_data'] = $this->basic_model->getRow('user', 'user_id', $res["quotation"]['customer_id'], $this->orderBy, 'user_id');
                $res["quotation_detail_data"] =  $this->basic_model->getCustomRows("SELECT p.*, qd.*, price.retail_base_price, price.retail_usd_price ,  GROUP_CONCAT(img.image ORDER BY img.image_id) AS images FROM `quotation_detail" . $history_table . "` `qd` 
                LEFT JOIN `product` `p` ON `p`.`product_id` = `qd`.`product_id` 
                LEFT JOIN `price`  ON `price`.`prod_id` = `p`.`product_id` 
                LEFT JOIN `images` `img` ON `qd`.`product_id` = `img`.`product_id`
                WHERE `qd`.`quotation_id` = '" . $id . "'  AND price.plan_id ='" . $res['customer_data']['plan_type'] . "' group by `qd`.`product_id` order by qd.quotation_detail_id asc");
                /*if($header_check == 2  || $header_check == 3){
                $header_check = ($header_check == 2) ? 1 : 0;
                $res["quotation_detail_data"] = $this->basic_model->getCustomRows("SELECT * FROM `quotation_detail` `qd` JOIN `product` `p` ON `p`.`product_id` = `qd`.`product_id` WHERE `qd`.`quotation_id` = '".$id."'"); 
                }else{*/

                //}
                $setting = $this->basic_model->getCustomRow("SELECT * FROM `setting` `s` join `currency` `c`ON `c`.id = `s`.`currency_id` where `s`.`setting_id` = '1' ");
                $res['setting'] = $setting;
                if (isset($res['quotation']['quotation_cover_letter'])) {
                    $res['setting']['quotation_cover'] = $res['quotation']['quotation_cover_letter'];
                }
                $res['base_currency_id'] = $setting['id'];
                $res["currency_name"] = $setting['title'];
                $res["optional_count"] = $this->basic_model->getCustomRow("Select count(*) As option_count From quotation_detail" . $history_table . " where `quotation_id` = '" . $id . "' AND quotation_detail_optional = '1' ")["option_count"];

                $res['customer_data']['validity_days'] = (empty($res['data']['validity_days'])) ? $setting['validity_days'] : $res['data']['validity_days'];
                $res['customer_data']['support_days'] = (empty($res['data']['support_days'])) ? $setting['support_days'] : $res['data']['support_days'];
                $res['customer_data']['warranty_years'] = (empty($res['data']['warranty_years'])) ? $setting['warranty_years'] : $res['data']['warranty_years'];
                $res['retail_id'] = 1;
                $res['quotation_currency_name'] = $this->basic_model->getCustomRow("SELECT * FROM `currency` where `id` = '" . $res["quotation"]['quotation_currency'] . "' ");
                if (!isset($view) && $res['quotation']['quotation_status'] < 4 && $res['quotation']['quotation_status'] != 1) {
                    //History
                    make_quotation_history($id);
                    //End History
                    $data = array();
                    if ($res["quotation"]['quotation_email_printed_status'] == 0) {
                        $data['quotation_email_printed_status'] = 1;
                        $data['quotation_revised_no'] = $res["quotation"]['quotation_revised_no'] + 1;
                    } else if ($res["quotation"]['quotation_email_printed_status'] > 0) {
                        $data['quotation_revised_no'] = $res["quotation"]['quotation_revised_no'] + 1;
                    }

                    //$data['quotation_status'] = 1;
                    if ($res['quotation']['quotation_final'] == '' || $res['quotation']['quotation_final'] < 1 || $res['quotation']['quotation_status'] == 2) {
                        $data['quotation_status'] = 1;
                    }
                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
                }

                $res['terms'] = $this->basic_model->getCustomRows("SELECT *  from quotation_payment_term" . $history_table . " WHERE quotation_id = '" . $id . "' ");
                $res['project_terms'] = $this->basic_model->getCustomRows("SELECT *  from quotation_project_term" . $history_table . " WHERE quotation_id = '" . $id . "'");

                // $res['warranty'] = $this->basic_model->getCustomRows("SELECT * from quotation_warranty where quotation_id = '".$id."'");
                // $res['warranty_setting'] = $this->basic_model->getCustomRows("SELECT warranty_type.*, quotation_warranty_setting.*  from warranty_type Left Join quotation_warranty_setting on warranty_type.warranty_type_id = quotation_warranty_setting.type_id WHERE quotation_id = '".$id."' ");


                //if term not enter add client term
                if (empty($res['terms'])) {
                    $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from client_payment_term where client_id=' . $res["quotation"]['customer_id']);
                }
                if (empty($res['project_terms'])) {
                    //$res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from client_project_term where client_id='.$res["quotation"]['customer_id']);
                    $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                }

                //if client term also not entered add setting term
                if (empty($res['terms'])) {
                    $res['terms'] = $this->basic_model->getCustomRows('SELECT *  from payment_terms');
                }
                if (empty($res['project_terms'])) {
                    $res['project_terms'] = $this->basic_model->getCustomRows('SELECT *  from project_terms');
                }

                $res["setval"] = $this->basic_model->getRow($this->tables, $this->table_sid, 1, $this->orderBy, $this->table_sid);
                $res['optional_found'] = false;
                $res['non_optional_found'] = false;
                $res['inline_discount'] = false;
                foreach ($res["quotation_detail_data"] as $find) {
                    if ($find['quotation_detail_optional'] == 1) {
                        $res['optional_found'] = true;
                    }
                    if ($find['quotation_detail_optional'] != 1) {
                        $res['non_optional_found'] = true;
                    }

                    //find inline discount
                    if ($find['quotation_detail_total_discount_amount'] > 0) {
                        $res['inline_discount'] = true;
                    }
                }
                $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');


                $res['bank'] = $this->basic_model->getCustomRows("SELECT * FROM bank_setting");
                // if(empty($res['warranty'])){
                //     $res['warranty'] = $this->basic_model->getCustomRows('SELECT * from client_warranty where client_id='.$res["quotation"]['customer_id']);
                //     $res['warranty_setting'] = $this->basic_model->getCustomRows('SELECT warranty_type.*, client_warranty_setting.*  from warranty_type Left Join client_warranty_setting on warranty_type.warranty_type_id = client_warranty_setting.type_id');
                //     $res['warranty_type'] = $this->basic_model->getCustomRows('SELECT * from warranty_type');
                // }

                //$this->load->view('quotation_Report',$res);
                if ($res['quotation']['quotation_status'] == 4) {
                    //print_b($res);
                    $this->basic_model->make_pdf($header_check, $res, 'Report', 'pi_report');
                } else {
                    $this->basic_model->make_pdf($header_check, $res, 'Report', 'quotation_Report');
                }
                
            } else {
                echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function mail()
    {
        extract($_POST);
        // print_b($_POST);
        if (isset($id) && $id != null) {

            //$setting = $this->basic_model->getRow("setting","setting_id",1,$this->orderBy,"setting_id");
            $quotation = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
            //$customer_data = $this->basic_model->getRow("user","user_id",$quotation['customer_id'],$this->orderBy,"user_id");
            //$from = $setting['setting_company_email'];
            //$to = $customer_data['user_email'];
            //$body = $email_body;
            //$subject = $email_subject;
            predefine_send_email($_POST);
            if ($quotation['quotation_status'] < 4) {
                //History
                make_quotation_history($id);
                //End History

                //$this->basic_model->sendEmail($from, $to, $body, $subject, $from_name = NULL);
                $data = array();
                if ($quotation['quotation_email_printed_status'] == 0) {
                    $data['quotation_email_printed_status'] = 1;
                    $data['quotation_revised_no'] = $quotation['quotation_revised_no'] + 1;
                } else if ($quotation['quotation_email_printed_status'] > 0) {
                    $data['quotation_revised_no'] = $quotation['quotation_revised_no'] + 1;
                }
                if ($quotation['quotation_final'] == '' || $quotation['quotation_final'] < 1 || $quotation['quotation_status'] == 2) {
                    $data['quotation_status'] = 1;
                }
                $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_id, $id);
            }
            echo json_encode(array("success" => "Mail Send successfully", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be Perform Any Action."));
        }
    }

    function quotationDetailDelete($id)
    {
        $del = $this->basic_model->deleteRecord('quotation_detail_id', $id, $this->table_detail);
        echo json_encode(array("success" => "Record removed successfully", "detailsDelete" => true, "qoutationMultiTotalSet" => true));
    }

    // function delete($id){
    //     $del = $this->basic_model->deleteRecord('quotation_id', $id, 'quotation');
    //       echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));

    // }

    function detail($id)
    {
        if (isset($id) && $id != '') {

            $res_int = $this->basic_model->getRow($this->table, $this->table_id, $id, $this->orderBy, $this->table_id);
            $table_q = "quotation";
            $table_qid = "quotation_id";

            $table_c = "user";
            $table_cid = "user_id";

            $table_qd = "quotation_detail";
            $table_qd_id = "quotation_detail_id";



            $res_quo = $this->basic_model->getRow($table_q, $table_qid, $res_int['quotation_id'], $this->orderBy, $table_qid);
            $res_quo_det = $this->basic_model->getCustomRows("Select * From quotation_detail  qd join product p on p.product_id = qd.product_id where quotation_id = '" . $id . "'");
            $res_cust = $this->basic_model->getRow($table_c, $table_cid, $res_int['customer_id'], $this->orderBy, $table_cid);

            if ($res_int) {
                $outputTitle = "";
                $outputTitle .= "Quotation Detail";

                $outputDescription = "";
                $outputDescription .= "<p>Customer Name  : " . $res_cust['user_name'] . "</p>";
                // $outputDescription .= "<p>Product Description  : ".$res_quo_det['quotation_detail_description']."</p>";
                $outputDescription .= "<p>Quotation Number  : " . $res_quo['quotation_no'] . "</p>";
                // $outputDescription .= "<p>Quantity  : ".$res_quo_det['quotation_detail_quantity']."</p>";
                // $outputDescription .= "<p>Rate : ".$res_quo_det['quotation_detail_rate']."</p>";
                $outputDescription .= "<p>Quotation Date : " . $res_quo['quotation_date'] . "</p>";
                $outputDescription .= "<p>Expiry Date : " . $res_quo['quotation_expiry_date'] . "</p>";

                $count = 0;;
                foreach ($res_quo_det as $k => $v) {
                    $count++;
                    $outputDescription .= "<p>" . $count . ". Product Name : " . $v['product_name'] . "</p>";
                    $outputDescription .= "<p>Description : " . $v['quotation_detail_description'] . "</p>";
                    $outputDescription .= "<p>Discount Type : " . $v['quotation_detail_total_discount_type'] . "</p>";
                    $outputDescription .= "<p>Discount Amount : " . $v['quotation_detail_total_discount_amount'] . "</p>";
                    $outputDescription .= "<p>Quantity: " . $v['quotation_detail_quantity'] . "</p>";
                }

                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            } else {
                echo json_encode(array('error' => 'Not Found ' . $this->page_heading . '.'));
            }
        } else {
            echo json_encode(array("error" => $this->page_heading . " id is incorrect can't be deleted."));
        }
    }

    function history()
    {
        if ($this->check_history) {
            extract($_GET);
            // OR quotation_id = '".$id."'
            $data =  $this->basic_model->getCustomRows("SELECT * from quotation_history where quotation_revised_id='" . $id . "'  ORDER BY quotation_id DESC");
            echo json_encode($data);
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function follow_up(){
        if ($this->check_history) {
            extract($_GET);
            // OR quotation_id = '".$id."'
            $data =  $this->basic_model->getCustomRows("SELECT * from follow_up JOIN user on follow_up.follow_up_sales_person = user.user_id where (follow_up.follow_up_doc_type='1' || follow_up.follow_up_doc_type='2') AND follow_up.follow_up_doc_id='".$id."'  ORDER BY follow_up_id  DESC");
            echo json_encode($data);
            } else {
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404', $res);
            }
    }
}
