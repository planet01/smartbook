    <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tax_calculation extends MY_Back_Controller {
    public function __construct() {
        parent::__construct();
        // page action
        $this->add_product = 'tax_calculation/add';
        $this->view_product = 'tax_calculation/view';
        $this->edit_product = 'tax_calculation/edit';
        $this->delete_product = 'tax_calculation/delete';
        $this->detail_product = 'tax_calculation/detail';
        $this->status = 'tax_calculation/change_status';
        $this->post = 'tax_calculation/change_post';
        
        // page active
        $this->add_page_product = "add-tax_calculation";
        $this->edit_page_product = "edit-tax_calculation";
        $this->view_page_product = "view-tax_calculation";

        // table
        $this->table = "tax_calculation";
        $this->table_pid = "tax_id";
        $this->orderBy = "DESC";
        $this->userType = 3;
        $this->detail_table = "tax_calculation_detail";
        // image_upload_dir
        $this->image_upload_dir = "uploads/user";
        // FileUploader
        require_once APPPATH .'third_party/FileUploader.php';
        // Page Heading
        $this->page_heading = "Tax Calculation";

        $this->no_results_found = "user/no_results_found";

        $logged_in = $this->is_login_admin();
        if (!$logged_in) {
            redirect('user');
        } 

        $this->check_view = false;
        $this->check_add = false;
        $this->check_edit = false;
        $this->check_history = false;
        $this->check_print = false;

        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
            if($v['module_id'] == 28)
            {
                if($v['visible'] == 1)
                {
                    $this->check_view = true;
                }
                if($v['add'] == 1)
                {
                    $this->check_add = true;
                }
                if($v['edit'] == 1)
                {
                    $this->check_edit = true;
                }
                if($v['history'] == 1)
                {
                    $this->check_history = true;
                }
                if($v['print'] == 1)
                {
                    $this->check_print = true;
                }
            }
            }
        }else
        {
            $this->check_view = true;
            $this->check_add = true;
            $this->check_edit = true;
            $this->check_history = true;
            $this->check_print = true;
        }

    }
    function index(){
        if($this->check_view){
            redirect($this->add_product);
        }else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
    }

    function add(){
        extract($_POST);
        if( $this->input->post()){
            // print_b($_POST);
            if(empty($id)){
                if( empty($start_date) ||  empty($end_date) || empty($invoice_id) ){
                    if(empty($invoice_id))
                    {
                        echo json_encode(array("error" => "Please add Invoices"));
                    }else
                    {
                        echo json_encode(array("error" => "Please fill all fields"));
                    }
                }
                else{
                    
                    $Tax_Date_year = explode("-",$tax_date)[0];
                    $t_no = serial_no_tax($Tax_Date_year);

                    $data = array(
                        "tax_date" => @$tax_date,
                        "tax_no" => $t_no,
                        "start_date"=> @$start_date,
                        "end_date"=> @$end_date,
                        "remarks" => $remarks,
                    );
                     
                    $id = $this->basic_model->insertRecord($data,$this->table);
                    

                    if(isset($invoice_id)){
                        for($i= 0; $i< sizeof($invoice_id); $i++) {
                            $detail_data = [
                                'tax_id' => $id,
                                'invoice_id' => $invoice_id[$i],
                                'tax' => $tax[$i],
                                'paid_amount' => $paid_amount[$i],
                                'invoice_date' => $invoice_date[$i],
                                'invoice_no' => $invoice_no[$i],
                                'is_amc' => $is_amc[$i],
                            ];
                            $this->basic_model->insertRecord($detail_data,$this->detail_table);
                        }

                    }

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
            //Edit form
            else{
                if( empty($start_date) ||  empty($end_date) || empty($invoice_id)  ){
                    if(empty($invoice_id))
                    {
                        echo json_encode(array("error" => "Please add Invoices"));
                    }else
                    {
                        echo json_encode(array("error" => "Please fill all fields"));
                    }
                    
                }else{
                   
                    $this->makeTaxHistory($id);
                    $data = array(
                        "start_date"=> @$start_date,
                        "end_date"=> @$end_date,
                        "remarks" => $remarks,
                        'tax_revised_no' => number_format($tax_revised_no) + 1
                    );

                    $updateRecord = $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
                    
                    $this->basic_model->customQueryDel("DELETE FROM `".$this->detail_table."` where ".$this->table_pid."=".$id);

                    if(isset($invoice_id)){
                        for($i= 0; $i< sizeof($invoice_id); $i++) {
                            $detail_data = [
                                'tax_id' => $id,
                                'invoice_id' => $invoice_id[$i],
                                'tax' => $tax[$i],
                                'paid_amount' => $paid_amount[$i],
                                'invoice_date' => $invoice_date[$i],
                                'invoice_no' => $invoice_no[$i],
                                'is_amc' => $is_amc[$i],
                            ];
                            $this->basic_model->insertRecord($detail_data,$this->detail_table);
                        }

                    }

                    echo json_encode(array( "success" => "Record updated successfully","redirect"=>site_url($this->view_product), 'fieldsEmpty' => 'yes'));
                }
            }
        }
        //Add view
        else{
           
            if($this->check_add)
            {
                $res['title'] = "Add New ".$this->page_heading;
                $res["page_title"] = "add";
                $res["page_heading"] = $this->page_heading;
                $res['active'] = $this->add_page_product;
                $res["add_product"] = $this->add_product;
                $this->template_view($this->add_page_product,$res);
            }
            else
            {   
                $res['heading'] = "Permission Not Given";
                $res['message'] = "You don't have permission to access this page.";
                $this->load->view('errors/html/error_404',$res);
            }
            
        }
    }

    function invoice_data()
    {
        extract($_POST);
        if($start_date && $end_date) {
            
            $unpaid_invoice = array();
            $setval = $this->basic_model->getRow('setting','setting_id',1,$this->orderBy,'setting_id');
            $date_range = " WHERE ( STR_TO_DATE(`inv`.`invoice_date`, '%Y-%m-%d') < '".$start_date."' AND `inv`.`tax_paid`=0 ) OR (STR_TO_DATE(`inv`.`invoice_date`, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
            $query = "SELECT * FROM invoice inv ".$date_range;
            $invoice = $this->basic_model->getCustomRows($query);
            for($i=0; $i < sizeof($invoice); $i++) {
                $payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM payment WHERE invoice_id = '".$invoice[$i]['invoice_id']."' AND is_invoice = 1  AND (payment_revised_id = 0 || payment_revised_id IS NULL)");
                $tot = (!empty($payment['total_payment_amount']))? $payment['total_payment_amount']+$payment['total_payment_adjustment'] : 0;
                $rev = ($invoice[$i]['invoice_revised_no'] > 0)?'-R'.$invoice[$i]['invoice_revised_no'] : '';
                $temp_data = [];
                $temp_data['invoice_id']        = $invoice[$i]['invoice_id'];
                $temp_data['invoice_tax_amount'] = $invoice[$i]['invoice_tax_amount'];
                $temp_data['paid_amount']  = $tot;
                $temp_data['invoice_date']      = date("Y-m-d", strtotime($invoice[$i]['invoice_date']));
                $temp_data['invoice_no']        =  @$setval["company_prefix"].@$setval["invoice_prfx"].$invoice[$i]['invoice_no'].$rev;
                $temp_data['is_amc'] = 0;
                array_push($unpaid_invoice,$temp_data);
            }

            $date_range = " WHERE ( STR_TO_DATE(`inv`.`amc_quotation_date`, '%Y-%m-%d') < '".$start_date."' AND `inv`.`tax_paid`=0 ) OR (STR_TO_DATE(`inv`.`amc_quotation_date`, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."'  AND `inv`.`tax_paid`=0)";
            $query = "SELECT * FROM amc_quotation inv ".$date_range;
            $amc_invoice = $this->basic_model->getCustomRows($query);
            for($i=0; $i < sizeof($amc_invoice); $i++) {
                $payment = $this->basic_model->getCustomRow("SELECT *, sum(payment_amount) as total_payment_amount, sum(payment_adjustment) as total_payment_adjustment FROM amc_payment WHERE invoice_id = '".$amc_invoice[$i]['amc_quotation_id']."' AND (payment_revised_id = 0 || payment_revised_id IS NULL)");
                $tot = (!empty($payment['total_payment_amount']))? $payment['total_payment_amount']+$payment['total_payment_adjustment'] : 0;
                $rev = ($amc_invoice[$i]['amc_quotation_revised_no'] > 0)?'-R'.$amc_invoice[$i]['amc_quotation_revised_no'] : '';
                $temp_data = [];
                $temp_data['invoice_id']        = $amc_invoice[$i]['amc_quotation_id'];
                $temp_data['invoice_tax_amount'] = $amc_invoice[$i]['invoice_tax_amount'];
                $temp_data['paid_amount']  = $tot;
                $temp_data['invoice_date']      = date("Y-m-d", strtotime($amc_invoice[$i]['amc_quotation_date']));
                $temp_data['invoice_no']        =  @$setval["company_prefix"].(($amc_invoice[$i]['amc_quotation_status'] == 4 ||  $amc_invoice[$i]['amc_quotation_status'] == 7) ? 'AMCINV-' : 'AMC').$amc_invoice[$i]['amc_quotation_no'].$rev;
                $temp_data['is_amc'] = 1;
                array_push($unpaid_invoice,$temp_data);
            }

            $data['invoice_data'] = $unpaid_invoice;
            echo json_encode($data);
        }else{
            $data = array();
            echo json_encode($data);
        }
            
    }

    function edit($id){
        if($this->check_edit)
        {
            if( isset($id) && $id != ''){
                $res_edit = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                if($res_edit){
                    $res["title"] = "Edit ".$this->page_heading;
                    $res["page_title"] =  "";
                    $res["page_heading"] = $this->page_heading;
                    $res["active"] = $this->edit_page_product;
                    $res["add_product"] = $this->add_product;
                    $res['data'] = $res_edit;
                    $res["detail_data"] = $this->basic_model->getCustomRows("SELECT * FROM ".$this->detail_table." WHERE ".$this->table_pid."=".$id);
    
    
                    $this->template_view($this->add_page_product,$res);
                }else{
                    redirect($this->no_results_found);    
                }
            }else{
                redirect($this->no_results_found);
            }
        }else
        {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404',$res);
        }
        
    }

    function view() {
        $res['title'] = "View All ".$this->page_heading;
        $res['active'] = $this->view_page_product;
        $res["page_heading"] = $this->page_heading;
        $res["add_product"] = $this->add_product;
        $res["edit_product"] = $this->edit_product;
        $res["delete_product"] = $this->delete_product;
        $res["detail_product"] = $this->detail_product;
        $res["status_product"] = $this->status;
        $res["post_product"] = $this->post;
        $res['image_upload_dir'] = $this->image_upload_dir;
        $res['setval'] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
        $res['data'] = $this->basic_model->getCustomRows("SELECT *,sum(td.tax) as all_tax,sum(td.paid_amount) as paid_amount FROM ".$this->table." t left JOIN ".$this->detail_table." td on t.".$this->table_pid." = td.".$this->table_pid." WHERE flg_deleted = 0 GROUP BY t.tax_id ORDER BY t.tax_id LIMIT 100");
        $this->template_view($this->view_page_product,$res);
    }

    function search_view()
    {
        extract($_POST);
        if($this->input->post())
        {
            $where = "WHERE flg_deleted = 0 ";
            if($from_date != 0)
            {
                $where .= " AND STR_TO_DATE('$from_date','%Y-%m-%d') BETWEEN STR_TO_DATE(t.start_date,'%Y-%m-%d') AND STR_TO_DATE(t.end_date,'%Y-%m-%d')";
            }
            if($to_date != 0)
            {
                $where .= " AND STR_TO_DATE('$to_date','%Y-%m-%d') BETWEEN STR_TO_DATE(t.start_date,'%Y-%m-%d') AND STR_TO_DATE(t.end_date,'%Y-%m-%d')";
            }

            $where .= " GROUP BY t.tax_id ORDER BY t.tax_id LIMIT 100";
            $query = "SELECT *,sum(td.tax) as all_tax,sum(td.paid_amount) as paid_amount FROM ".$this->table." t left JOIN ".$this->detail_table." td on t.".$this->table_pid." = td.".$this->table_pid." ".$where;
            
            $res['data'] = $this->basic_model->getCustomRows($query);
            
            echo json_encode($res);
        }
    }
    
    function delete($id){
        if( isset($id) && $id != ''){
            $data = array(
                "flg_deleted" => 1,
            ); 
            $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
            echo json_encode(array( "success" => "Record removed successfully","redirect"=>site_url($this->view_product), "details"=> true));
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }   
    }

    function change_status()
    {
        extract($_GET);
        $data = array(
            "status" => $status,
        ); 
        $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $tax_id);
        echo json_encode(array("success" => "Status Changed", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
    }

    function change_post($id)
    {
        $data = array(
            "status" => 3,
        ); 
        $this->basic_model->updateRecord($data, $this->table, $this->table_pid, $id);
        $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
        $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
        $res_det = $this->basic_model->getCustomRows("SELECT * FROM ".$this->detail_table." WHERE ".$this->table_pid."=".$id);
        $total_tax = 0;
        foreach ($res_det as $k => $v) {
            $total_tax += $v['tax'];
            $invoice_data = array(
                "tax_paid" => 1,
            ); 

            if($v['is_amc'] == 0)
            {
                $this->basic_model->updateRecord($invoice_data, 'invoice', 'invoice_id', $v['invoice_id']);
            }else
            {
                $this->basic_model->updateRecord($invoice_data, 'amc_quotation', 'amc_quotation_id', $v['invoice_id']);
            }
            
        }

        $Transaction_Date_year = explode("-",$res['tax_date'])[0];
        $company_prfx = (substr($setval["company_prefix"], -1) == '-')?$setval["company_prefix"]:$setval["company_prefix"].'-';
        $t_no = serial_no_voucher($company_prfx.'JV-'.$Transaction_Date_year);
        $data = array(
            'TransactionNo' => $t_no,
            'voucher_type' => 1,
            'Transaction_Date' => $res['tax_date'],
            'Transaction_Detail' => $res['remarks'],
            'CompanyID' => str_replace("-","",$setval["company_prefix"]),
            'flgDeleted' => 0,
            "Posted" => 1,
        );
        $voucher_id = $this->basic_model->insertRecord($data, 'voucher');

        $arr = array(
            'AccountNo' => $setval['tax_provision_account'],
            'voucher_id' => $voucher_id,
            'invoice_id' => 0,
            'Cheque_No' => 'Tax',
            'Pay_To_Name' => '',
            'Amount_Cr' => 0,
            'Amount_Dr' => $total_tax,
            'Transaction_Detail' => 'Tax Calculation - V # '.@$setval["company_prefix"].'TX-'.$res['tax_no'].'. ',
            'tax' => 0,
            'Remarks' => $res['remarks'],
            'is_hide' => 0                     
        );
        $this->basic_model->insertRecord($arr, 'voucher_detail');

        $arr = array(
            'AccountNo' => $setval['tax_payable_account'],
            'voucher_id' => $voucher_id,
            'invoice_id' => 0,
            'Cheque_No' => 'Tax',
            'Pay_To_Name' => '',
            'Amount_Cr' => $total_tax,
            'Amount_Dr' => 0,
            'Transaction_Detail' => 'Tax Calculation - V # '.@$setval["company_prefix"].'TX-'.$res['tax_no'].'. ',
            'tax' => 0,
            'Remarks' => $res['remarks'],
            'is_hide' => 0                     
        );
        $this->basic_model->insertRecord($arr, 'voucher_detail');
        
        echo json_encode(array("success" => "Record Posted", "redirect" => site_url($this->view_product), 'fieldsEmpty' => 'yes'));
    }
 
    function detail($id){
        if( isset($id) && $id != ''){
            $res = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
            if($res){
                $outputTitle = "";
                $outputTitle .= $this->page_heading." Detail";
                $outputDescription = "";
                $rev = ($res['tax_revised_no'] > 0)?'-R'.$res['tax_revised_no'] : '';
                $outputDescription .= "<p><span style='font-weight:bold;'>Tax No : </span> ".@$setval["company_prefix"].'TX-'.$res['tax_no'].$rev."</p>"; 
                $outputDescription .= "<p><span style='font-weight:bold;'>Date : </span> ".date("d-M-Y", strtotime($res["tax_date"]))."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Start Date : </span> ".date("d-M-Y", strtotime($res["start_date"]))."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>End Date : </span> ".date("d-M-Y", strtotime($res["end_date"]))."</p>";
                $outputDescription .= "<p><span style='font-weight:bold;'>Remarks : </span> ".$res['remarks']."</p>";
                
                $count = 0;
                $res_quo_det = $this->basic_model->getCustomRows("SELECT * FROM ".$this->detail_table." WHERE ".$this->table_pid."=".$id);
                foreach ($res_quo_det as $k => $v) {
                    $count++;
                    
                    $outputDescription .= "<p>Invoice ID : " . $v['invoice_no'] . "</p>";
                    $outputDescription .= "<p>Tax : " . $v['tax'] . "</p>";
                    $outputDescription .= "<p>Paid Amount : " . $v['paid_amount'] . "</p>";
                   
                }
                
                echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
            }else{
                echo json_encode(array('error' => 'Not Found '.$this->page_heading.'.'));
            }
        }else{
            echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be deleted."));
        }
    }

    function print_report()
    {
        if ($this->check_print) {
            extract($_GET);
            if(isset($id) && $id != null) {
                if($history == 1)
                {
                    $res['data'] = $this->basic_model->getRow("tax_calculation_history",$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    $res['detail_data'] = $this->basic_model->getCustomRows('SELECT * FROM '.$this->detail_table.'_history where '.$this->table_pid.' = "'.@$id.'"');
                }else
                {
                    $res['data'] = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
                    $res['detail_data'] = $this->basic_model->getCustomRows('SELECT * FROM '.$this->detail_table.' where '.$this->table_pid.' = "'.@$id.'"');
                }
                $res['history'] = $history;
                $res['setval'] = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                $this->basic_model->make_pdf($header_check,$res,'Report','tax_report');
            }
            else
            {
                echo json_encode(array( "error" => $this->page_heading." id is incorrect can't be Perform Any Action."));
            }
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

    function makeTaxHistory($id){
        $tax_data = $this->basic_model->getRow($this->table,$this->table_pid,$id,$this->orderBy,$this->table_pid);
        $tax_detail = $this->basic_model->getCustomRows('SELECT * FROM '.$this->detail_table.' where '.$this->table_pid.' = "'.@$tax_data['tax_id'].'"');
        
        $data = array(
            'tax_date' => $tax_data['tax_date'],
            'tax_no' => $tax_data['tax_no'],
            'start_date' => $tax_data['start_date'],
            'end_date' => $tax_data['end_date'],
            'remarks' => $tax_data['remarks'],
            'tax_revised_no' => $tax_data['tax_revised_no'],
            'status' => $tax_data['status'],
            'flg_deleted' => $tax_data['flg_deleted'],
            'belongs_to' => $id,

        );
        $tax_id = $this->basic_model->insertRecord($data,$this->table."_history");

        foreach ($tax_detail as $k => $v) {
            $arr = array(
                'tax_id' => $tax_id,
                'invoice_id' => $v['invoice_id'],
                'invoice_date' => $v['invoice_date'],
                'invoice_no' => $v['invoice_no'],
                'tax' => $v['tax'],
                'paid_amount' => $v['paid_amount'],
            );
            $tax_detail_id = $this->basic_model->insertRecord($arr,$this->detail_table."_history");
        }
        
        return true;
    }

    function history($id){
        if ($this->check_history) {
            
            $res = $this->basic_model->getCustomRows("Select * From tax_calculation_history where belongs_to = '".$id."'");
            $setval = $this->basic_model->getRow('setting', 'setting_id', 1, 'ASC', 'setting_id');
                if($res){
                    $outputTitle = "";
                    $outputTitle .= $this->page_heading." History";
                    $outputDescription = "";
                    $outputDescription .= "<table class='table'>";
                    $outputDescription .= "<thead>";
                    $outputDescription .= "<tr>";

                    $outputDescription .= "<th>Tax No</th>";
                    $outputDescription .= "<th>Date</th>";
                    $outputDescription .= "<th>Details</th>";
                    $outputDescription .= "</tr>";
                    $outputDescription .= "</thead>";
                    $outputDescription .= "<tbody>";
                    
                    foreach ($res as $k => $v) {
                        $outputDescription .= "<tr>";
                        $rev = ($v['tax_revised_no'] > 0)?'-R'.$v['tax_revised_no'] : '-R0';
                        $outputDescription .= "<td><a target='_blank' href='".site_url('tax_calculation/print_report?id='.@$v['tax_id'].'&header_check=1&view=0&history=1')."'>".@$setval["company_prefix"].'TX-'.$v['tax_no'].$rev."</a></td>"; 
                        $outputDescription .= "<td>".date("d-M-Y", strtotime($v["tax_date"]))."</td>";
                        $outputDescription .= "<td>".$v['remarks']."</td>";
                        $outputDescription .= "</tr>";
                    }
                    $outputDescription .= "</tbody>";
                    $outputDescription .= "</table>";
                    echo json_encode(array("success" => "Record Succesfully Fetched.", "outputTitle" => $outputTitle, "outputDescription" => $outputDescription));
                }else{
                    echo json_encode(array('error' => 'No History Found For This '.$this->page_heading.'.'));
                }
           
        } else {
            $res['heading'] = "Permission Not Given";
            $res['message'] = "You don't have permission to access this page.";
            $this->load->view('errors/html/error_404', $res);
        }
    }

}