<?php
class Common_model extends CI_Model {
	function __construct(){
        parent::__construct();
        $this->load->model("basic_model");
    }
    
    public function getCategoriesChilds($table_name,$column='id', $order = "ASC") {

        $query = "select  * from  `" . $table_name . "` `c` JOIN `category_description` `cd` ON `c`.`category_id` = `cd`.`category_id` GROUP BY `cd`.`category_id` ORDER BY `cd`.`category_description_name` ".$order;

        $result = $this->db->query($query);

        return $result->result_array();
    }
    
    public function getFaqDescriptions($faq_id) {
        $faq_description_data = array();
        $query2 = $this->db->query("SELECT * FROM faq_description WHERE faq_id = '" . (int)$faq_id . "'");
        foreach ($query2->result_array() as $result) {
            $faq_description_data[$result['language_id']] = array(
                'faq_description_title'       => $result['faq_description_title'],
                'faq_description_description' => $result['faq_description_description']
            );
        }
        
        return $faq_description_data;
    }
    public function getPackagesDescriptions($packages_id) {
        $packages_description_data = array();
        $query2 = $this->db->query("SELECT * FROM packages_description WHERE packages_id = '" . (int)$packages_id . "'");
        foreach ($query2->result_array() as $result) {
            $packages_description_data[$result['language_id']] = array(
                'title'       => $result['title'],
                'description' => $result['description']
            );
        }
        
        return $packages_description_data;
    }
    public function getCategoryDescriptions($category_id) {
        $category_description_data = array();
        $query2 = $this->db->query("SELECT * FROM category_description WHERE category_id = '" . (int)$category_id . "'");
        foreach ($query2->result_array() as $result) {
            $category_description_data[$result['language_id']] = array(
                'category_description_name'       => $result['category_description_name']
            );
        }
        
        return $category_description_data;
    }


}