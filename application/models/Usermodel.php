<?php
    class usermodel extends CI_Model{
        protected $table = 'user';
        protected $id = 'id';
        function __construct(){
            parent::__construct();
            $this->load->model("basic_model");
        }
        function chk_login($table,$email,$password,$type){
            //$sql =  'select * from '.$table.' where user_email ="'.$email.'" AND user_password = "'.$password.'" AND `user_type` = "'.$type.'"';
            $sql =  'select * from '.$table.' where user_email ="'.$email.'" AND user_password = "'.$password.'" AND user_type  != 3';
            $res= $this->db->query($sql);
            if( $res->num_rows() > 0 ){
                return $res->row_array();
            }
            else{
                return "0";
            }
        }
        function insert($data)
        { 
            $sql = $this->db->insert($this->table, $data);
            if($sql)
            {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
            else 
            {
                return false;
            }
        }
        function select_id($id)
        { 
            $this->db->where($this->id,$id);
            $query = $this->db->get($this->table);
		    $result = $query->row_array();
            return $result;
        }
        public function edit($id,$data)
        {
            $this->db->where($this->id,$id);
            $this->db->update($this->table,$data);
        }
        
}