<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function print_b($value = '')
{
  echo "<pre>";
  print_r($value);
  echo "</pre>";
    die;
}
function limit_string($x, $length)
{
  if(strlen($x)<=$length){
    echo $x;
  }
  else{
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}
function dir_path(){
    $path = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR;
    return $path."/smartbooks/";
}
function dt_format($date) {
   return date("d-M-Y", strtotime($date));
}
function thumb($fullname, $width, $height,$uploads_dir,$allowfiles){
  // Path to image thumbnail in your root
  $dir = dir_path().$uploads_dir;
  $url = base_url() . $uploads_dir;
  // Get the CodeIgniter super object
  $CI = &get_instance();
  // get src file's extension and file name
  $extension = pathinfo($fullname, PATHINFO_EXTENSION);
  $filename = pathinfo($fullname, PATHINFO_FILENAME);
  $image_org = $dir . $filename . "." . $extension;
  $image_thumb = $dir . $filename . "-" . $height . '_' . $width . "." . $extension;
  $image_returned = $url . $filename . "-" . $height . '_' . $width . "." . $extension;
  $path = $filename.".".$extension;
  $ext = pathinfo($path, PATHINFO_EXTENSION);
  if (in_array($ext, $allowfiles)) {
    if (!file_exists($image_thumb)) {
        // LOAD LIBRARY
        $CI->load->library('image_lib');
        // CONFIGURE IMAGE LIBRARY
        $config['source_image'] = $image_org;
        $config['new_image'] = $image_thumb;
        $config['width'] = $width;
        $config['height'] = $height;
        @$CI->image_lib->initialize(@$config);
        @$CI->image_lib->resize();
        @$CI->image_lib->clear();
    }
    return true;
  }else{
    return false;
  }
  return '';
}
function getMultiData($table_name,$column = 'id',$order = "ASC"){
  $ci=& get_instance();
  $ci->load->database(); 
  $sql = "select  * from  `" . $table_name . "` ORDER BY `".$column."` " . $order;
  $query = $ci->db->query($sql);
    return $query->result_array();
}
function getRows($table_name, $column, $criteria, $orderby = "id" ,$order = "ASC") {
  $ci =& get_instance();
  $ci->load->database(); 
    $query = "select  * from  `" . $table_name . "` where `" . $column . "`= '" . $criteria."' order by ".$orderby." ".$order;
        $result = $ci->db->query($query);
        return $result->result_array();
}
function getRow($table_name, $column, $criteria, $orderby = "id" ,$order = "ASC") {
    $ci= &get_instance();
    $ci->load->database(); 
    $query = "select  * from  `" . $table_name . "` where `" . $column . "`= '" . $criteria."' order by ".$orderby." ".$order;
        $result = $ci->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
}
function getCount($table_name, $column, $criteria, $order = "ASC") {
      $ci =& get_instance();
      $ci->load->database(); 
      $query = "select  COUNT(*) AS total from  `" . $table_name . "` where `" . $column . "`=" . $criteria . " ORDER BY `id` " . $order;
      $result = $ci->db->query($query);
      if ($result->num_rows() > 0) {
        return $result->row_array();
      }
}
function customQuery($query) {
    $ci =& get_instance();
    $ci->load->database(); 
    $result = $ci->db->query($query);
    return $result->result_array();
}

function getCustomRows($query) {
    $ci =& get_instance();
    $ci->load->database(); 
    $result = $ci->db->query($query);
    return $result->result_array();
}

function getCustomRow($query) {
    $ci =& get_instance();
    $ci->load->database(); 
    $result = $ci->db->query($query);
    return $result->row_array();
}
function ratingFunction($rating,$full,$half,$blank,$totalStars = 5){
  $star = '';
  for($x=1; $x<= $rating; $x++) {
    $star .= $full;
  }
  if (strpos($rating,'.')) {
    $star .= $half;
    $x++;
  }
  while ($x<=$totalStars) {
    $star .= $blank;
    $x++;
  }
  return $star;
}
function percentage($value, $total = 100){
  $percentage = $value;
  $totalWidth = $total;
  $new_width = ($percentage / 100) * $totalWidth;
  return $new_width;
}
function unlimited_no_of_request(){
    $path = 100000;
    return $path;
}
function QuotationStatusData() {
  $QuotationStatusData = array(
    0 => "Expire",
    1 => "Final",
    2 => "Revision",
    3 => "Draft",
    4 => "Converted to P.I",
    5 => "Client Not Interested",
    6 => "Decision Postponed"
  );
  return $QuotationStatusData;
}
function AmcQuotationStatusData() {
  $QuotationStatusData = array(
    0 => "Expire",
    1 => "Final",
    2 => "Revision",
    3 => "Draft",
    4 => "Converted to Invoice",
    5 => "Client Not Interested",
    6 => "Decision Postponed",
    7 => "Payment Received",
  );
  return $QuotationStatusData;
}
function serial_no($year= false) {
    $basic_model = new basic_model();
    $query = "SELECT max(quotation_no) as serial_no FROM `quotation` WHERE `quotation_no` LIKE '".$year."%'";
    $res = $basic_model->getCustomRow($query);
    $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
    return $num;
}
function serial_no_voucher($year= false) {
  $basic_model = new basic_model();
  $query = "SELECT max(TransactionNo) as serial_no FROM `voucher` WHERE `TransactionNo` LIKE '".$year."%'";
  $res = $basic_model->getCustomRow($query);
  $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : add_one($res['serial_no']);
  return $num;
}
function add_one($string) {
  $nwMsg = explode("-",$string);
  $inMsg = number_format($nwMsg[2], 2, '.', '')+1 ;
  $finStr =$nwMsg[0].'-'.$nwMsg[1].'-'.$inMsg;
  return $finStr ;
}
function serial_no_ticket($year= false) {
  $basic_model = new basic_model();
  $query = "SELECT max(ticket_no) as serial_no FROM `tickets` WHERE `ticket_no` LIKE '".$year."%'";
  $res = $basic_model->getCustomRow($query);
  $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
  return $num;
}
function serial_no_tax($year= false) {
  $basic_model = new basic_model();
  $query = "SELECT max(tax_no) as serial_no FROM `tax_calculation` WHERE `tax_no` LIKE '".$year."%'";
  $res = $basic_model->getCustomRow($query);
  $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
  return $num;
}
function serial_no_amc($year= false) {
    $basic_model = new basic_model();
    $res = $basic_model->getCustomRow("Select Max(amc_quotation_no) as serial_no from amc_quotation WHERE `amc_quotation_no` LIKE '".$year."%'");
    $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
    return $num;
}
function serial_no_packing_list($year = false,$month = false) {
  $basic_model = new basic_model();
    $query = "SELECT max(packing_list_no) as serial_no FROM `packing_list` WHERE `packing_list_no` LIKE '".$year."%'";
    $res = $basic_model->getCustomRow($query);
    //return date("Ym").$res['serial_no']+1;
    $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
    
    return $num;
  
}
function serial_no_receivable($year= false) {
    $basic_model = new basic_model();
    $query = "SELECT max(receipt_no) as serial_no FROM `payment` WHERE `receipt_no` LIKE '".$year."%'";
    $res = $basic_model->getCustomRow($query);
    $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
    return $num;
}

function serial_no_amc_receivable($year= false) {
  $basic_model = new basic_model();
  $query = "SELECT max(receipt_no) as serial_no FROM `amc_payment` WHERE `receipt_no` LIKE '".$year."%'";
  $res = $basic_model->getCustomRow($query);
  $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
  return $num;
}
function serial_no_invoice($year= false) {
    $basic_model = new basic_model();
    $query = "SELECT max(invoice_no) as serial_no FROM `invoice` WHERE `invoice_no` LIKE '".$year."%'";
    $res = $basic_model->getCustomRow($query);
    //return date("Ym").$res['serial_no']+1;
    $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
    
    return $num;
    
    
}
function serial_no_material_issue_note($year= false) {
    $basic_model = new basic_model();
    $query = "SELECT max(material_issue_note_no) as serial_no FROM `material_issue_note` WHERE `material_issue_note_no` LIKE '".$year."%'";
    $res = $basic_model->getCustomRow($query);

    $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
    
    return $num;
}

function serial_no_inquiry($year= false) {
  $basic_model = new basic_model();
  $query = "SELECT max(inquiry_no) as serial_no FROM `inquiry_received_history` WHERE `inquiry_no` LIKE '".$year."%'";
  $res = $basic_model->getCustomRow($query);
  $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
  return $num;
}
function inventory_quantity_by_product($id){
  $basic_model = new basic_model();
  //return $basic_model->getCustomRow("SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$id."')) as total_inventory_quantity FROM `product` 
    //JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id`
    //WHERE `inventory`.`inventory_type` = 1 AND inventory.revision = 0 AND `product`.`product_id` = '".$id."' AND product_is_active = 1   AND (inventory.revision = 0 || inventory.revision IS NUll) GROUP By inventory.inventory_id");
	
	return $basic_model->getCustomRow("SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$id."')) as total_inventory_quantity FROM `product` 
    JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id`
    WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$id."' AND product_is_active = 1  GROUP By inventory.inventory_id");
}
//Jd function inventory_quantity_by_warehouse($id, $warehouse_id){
//   $basic_model = new basic_model();
//   $data = $basic_model->getCustomRow("SELECT *,
//     (
//       (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND (i2.revision = 0 || i2.revision IS NUll) AND `i2`.`product_id` = '".$id."' AND `i2`.`warehouse_id` = '".$warehouse_id."')
//        -
//       (SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND (i3.revision = 0 || i3.revision IS NUll) AND `i3`.`product_id` = '".$id."' AND `i3`.`warehouse_id` ='".$warehouse_id."'
//       ) 
//     ) As total_quantity
//     FROM `inventory` 
//     WHERE warehouse_id = '".$warehouse_id."'  AND `product_id` = '".$id."' AND (revision = 0 || revision IS NUll) ");
//   return @$data['total_quantity'];
// }

function inventory_quantity_by_warehouse($id, $warehouse_id){
  $basic_model = new basic_model();
  $data = $basic_model->getCustomRow("SELECT *,
    (
      (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = '".$id."' AND `i2`.`warehouse_id` = '".$warehouse_id."')
       -
      (SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0  AND `i3`.`product_id` = '".$id."' AND `i3`.`warehouse_id` ='".$warehouse_id."'
      ) 
    ) As total_quantity
    FROM `inventory` 
    WHERE warehouse_id = '".$warehouse_id."'  AND `product_id` = '".$id."'");
  return @$data['total_quantity'];
}

function currency_data($id){
    $basic_model = new basic_model();
    return $basic_model->getCustomRow("SELECT * FROM currency WHERE id = '".$id."'");
  }
function int_to_float($val){
  return sprintf("%.2f",$val);
}
function make_quotation_history($id)
{
  $basic_model = new basic_model();
  $temp_quotation = $basic_model->getRow("quotation","quotation_id",$id,"DESC","quotation_id");
  $temp_quotation_detail = $basic_model->getRows("quotation_detail","quotation_id",$id,"quotation_detail_id","ASC");
  $temp_quotation_payment_term = $basic_model->getRows("quotation_payment_term","quotation_id",$id,"quotation_payment_id","ASC");
  $temp_quotation_project_term = $basic_model->getRows("quotation_project_term","quotation_id",$id,"quotation_project_id","ASC");
  // $temp_quotation_warranty = $basic_model->getRows("quotation_warranty","quotation_id",$id,"quotation_warranty_id","DESC");
  // $temp_quotation_warranty_setting = $basic_model->getRows("quotation_warranty_setting","quotation_id",$id,"quotation_setting_id","DESC");
  
  $revision = array(
      'revision_title' => 'quotation',
    );
  // $revision_id = $basic_model->insertRecord($revision,"tbl_revision");

  $temp_data = array(
      'quotation_revised_id' => $temp_quotation['quotation_id'],
      'customer_id' => $temp_quotation['customer_id'],
      'employee_id' => $temp_quotation['employee_id'],
      'quotation_no' => $temp_quotation['quotation_no'],
      'quotation_date' => $temp_quotation['quotation_date'],
      'quotation_revised_no' => $temp_quotation['quotation_revised_no'],
      'quotation_expiry_date' => $temp_quotation['quotation_expiry_date'],
      'quotation_total_discount_type' => $temp_quotation['quotation_total_discount_type'],
      'quotation_total_discount_amount' => $temp_quotation['quotation_total_discount_amount'],
      'quotation_buypack_discount' => $temp_quotation['quotation_buypack_discount'],
      'quotation_currency' => $temp_quotation['quotation_currency'],
      'quotation_status' => $temp_quotation['quotation_status'],
      'quotation_freight_type' => @$temp_quotation['quotation_freight_type'],
      'quotation_email_printed_status' => $temp_quotation['quotation_email_printed_status'],
      'quotation_validity' => $temp_quotation['quotation_validity'],
      'quotation_support' => $temp_quotation['quotation_support'],
      'quotation_warranty' => $temp_quotation['quotation_warranty'],
      'quotation_cover_letter' => $temp_quotation['quotation_cover_letter'],
      'quotation_detail' => $temp_quotation['quotation_detail'],
      'quotation_status_comment' => $temp_quotation['quotation_status_comment'],
      'quotation_terms_conditions' => $temp_quotation['quotation_terms_conditions'],
      'revision_id' => 0,//$revision_id,
      'net_amount'=> $temp_quotation['net_amount'],
      'subtotal' => $temp_quotation['subtotal'],
      'has_invoice' => $temp_quotation['has_invoice'],
      'quotation_po_no' => $temp_quotation['quotation_po_no'],
      'quotation_tax' => $temp_quotation['quotation_tax'],
      'quotation_tax_amount' => $temp_quotation['quotation_tax_amount'],
      'quotation_report_line_break' => $temp_quotation['quotation_report_line_break'],
      'quotation_report_line_break_payment' => $temp_quotation['quotation_report_line_break_payment'],
      //'quotation_report_line_break_payment_pi' => $temp_quotation['quotation_report_line_break_payment_pi'],
      'has_payment' => $temp_quotation['has_payment'],

  );

  $quotation_history_id = $basic_model->insertRecord($temp_data,"quotation_history");
  
  if($quotation_history_id)
  {
      foreach($temp_quotation_detail as $value) 
      {
          $arr = array(
              'product_id' => $value['product_id'],
              'quotation_id' => $quotation_history_id,
              'quotation_detail_description' => $value['quotation_detail_description'],
              'quotation_detail_total_discount_type' => $value['quotation_detail_total_discount_type'],
              'quotation_detail_total_discount_amount' => $value['quotation_detail_total_discount_amount'],
              'quotation_detail_quantity' => $value['quotation_detail_quantity'],
              'quotation_detail_rate' => $value['quotation_detail_rate'],
              'quotation_detail_total_amount' => $value['quotation_detail_total_amount'],
              'quotation_detail_rate_usd' => $value['quotation_detail_rate_usd'],
              'quotation_detail_optional' => @$value['quotation_detail_optional'],
              'quotation_detail_existing' => @$value['quotation_detail_existing'],
              'quotation_detail_provided' => @$value['quotation_detail_provided'],
              'quotation_detail_required' => @$value['quotation_detail_required'],
              
          );
          $basic_model->insertRecord($arr,"quotation_detail_history");        
      }

      
      foreach($temp_quotation_payment_term as $v) {
          $payment_data = [
              'quotation_id' => $quotation_history_id,
              'payment_title' => $v['payment_title'],
              'percentage' => $v['percentage'],
              'payment_days' => $v['payment_days'],
              'payment_amount' => $v['payment_amount'],
              'quotation_revised_id' => $id,
          ];
          $basic_model->insertRecord($payment_data,'quotation_payment_term_history');
      }

      
      foreach($temp_quotation_project_term as $v) {
          $project_data = [
              'quotation_id' => $quotation_history_id,
              'project_title' => $v['project_title'],
              'project_days' => $v['project_days'],
              'start_week' => $v['start_week'],
              'hash_code' => $v['hash_code'],
              'quotation_revised_id' => $id,
          ];
          $basic_model->insertRecord($project_data,'quotation_project_term_history');
      }

      //Warranty
      // $types = $basic_model->getCustomRows('SELECT * from warranty_type');
      // foreach($temp_quotation_warranty as $v) {
      //     $data_features = [
      //         'quotation_id' => $quotation_history_id,
      //         'title' => $v['title'],
      //         'quotation_revised_id' => $id,
      //     ];
      //     $warn_id = $basic_model->insertRecord($data_features,'quotation_warranty');
      //     $i = 0;
      //     foreach($types as $type){
      //         $data_setting = [
      //             'warn_id' => $warn_id,
      //             'quotation_id' => $quotation_history_id,
      //             'type_id' => $type['warranty_type_id'],
      //             'value'   => $temp_quotation_warranty_setting[$i]['value'],
      //             'percentage' => $temp_quotation_warranty_setting[$i]['percentage'],
      //             'quotation_revised_id' => $id,
      //         ];
      //         $i++;
      //        $basic_model->insertRecord($data_setting,'quotation_warranty_setting');
      //     }
      // }
      
  }
  
}

function make_amc_quotation_history($id)
{
  $basic_model = new basic_model();
  $temp_quotation = $basic_model->getRow("amc_quotation","amc_quotation_id",$id,"DESC","amc_quotation_id");
  $temp_quotation_detail = $basic_model->getRows("amc_quotation_detail","amc_quotation_id",$id,"amc_quotation_detail_id","DESC");
  $temp_quotation_payment_term = $basic_model->getRows("amc_quotation_payment_term","amc_quotation_id",$id,"amc_quotation_payment_id","DESC");
  $temp_quotation_project_term = $basic_model->getRows("amc_quotation_project_term","amc_quotation_id",$id,"amc_quotation_project_id","DESC");
  // $temp_quotation_warranty = $basic_model->getRows("amc_quotation_warranty","amc_quotation_id",$id,"amc_quotation_warranty_id","DESC");
  // $temp_quotation_warranty_setting = $basic_model->getRows("amc_quotation_warranty_setting","amc_quotation_id",$id,"amc_quotation_setting_id","DESC");
  
  $revision = array(
      'revision_title' => 'amc_quotation',
    );
  // $revision_id = $basic_model->insertRecord($revision,"tbl_revision");

  $temp_data = array(
      'amc_quotation_revised_id' => $temp_quotation['amc_quotation_id'],
      'customer_id' => $temp_quotation['customer_id'],
      'employee_id' => $temp_quotation['employee_id'],
      'amc_quotation_no' => $temp_quotation['amc_quotation_no'],
      'amc_quotation_date' => $temp_quotation['amc_quotation_date'],
      'amc_quotation_expiry_date' => $temp_quotation['amc_quotation_expiry_date'],
      'amc_quotation_total_discount_type' => $temp_quotation['amc_quotation_total_discount_type'],
      'amc_quotation_total_discount_amount' => $temp_quotation['amc_quotation_total_discount_amount'],
      'amc_quotation_revised_no' => $temp_quotation['amc_quotation_revised_no'],
      'amc_quotation_currency' => $temp_quotation['amc_quotation_currency'],
      'amc_quotation_status' => $temp_quotation['amc_quotation_status'],
      'amc_quotation_email_printed_status' => $temp_quotation['amc_quotation_email_printed_status'],
      'amc_quotation_type' => $temp_quotation['amc_quotation_type'],
      'amc_quotation_receivable_status' => $temp_quotation['amc_quotation_receivable_status'],
      'amc_quotation_revised_id' => $temp_quotation['amc_quotation_id'],
      'amc_quotation_cover_letter' => $temp_quotation['amc_quotation_cover_letter'],
      'amc_quotation_validity' => $temp_quotation['amc_quotation_validity'],
      'amc_quotation_max_visit' => $temp_quotation['amc_quotation_max_visit'],
      'amc_quotation_warranty' => $temp_quotation['amc_quotation_warranty'],
      'amc_quotation_terms_conditions' => $temp_quotation['amc_quotation_terms_conditions'],
      'amc_selected_invoice' => $temp_quotation['amc_selected_invoice'],
      'amc_quotation_final' => $temp_quotation["amc_quotation_final"],
      'amc_quotation_status_comment' => $temp_quotation['amc_quotation_status_comment'],
      'amc_subtotal' => $temp_quotation['amc_subtotal'],
      /*'amc_quotation_support' => $temp_quotation['amc_quotation_support'],
      */
      'amc_quotation_report_line_break' => $temp_quotation['amc_quotation_report_line_break'],
      'amc_quotation_report_line_break_payment' => $temp_quotation['amc_quotation_report_line_break_payment'],
      'amc_quotation_report_line_break_invoice' => $temp_quotation['amc_quotation_report_line_break_invoice'],
      'amc_quotation_report_line_break_payment_invoice' => $temp_quotation['amc_quotation_report_line_break_payment_invoice'],
      'revision_id' => 0//$revision_id,
  );

  $quotation_history_id = $basic_model->insertRecord($temp_data,"amc_quotation_history");
  
  if($quotation_history_id)
  {
      foreach($temp_quotation_detail as $value) 
      {
          $arr = array(
              'product_id' => $value['product_id'],
              'amc_quotation_id' => $quotation_history_id,
              'amc_quotation_detail_quantity' => $value['amc_quotation_detail_quantity'],
              'amc_quotation_detail_rate' => $value['amc_quotation_detail_rate'],
              'amc_quotation_detail_rate_usd' => $value['amc_quotation_detail_rate_usd'],
              'amc_quotation_detail_exclude' => @$value['amc_quotation_detail_exclude'],
              'amc_quotation_detail_start_date' => $value['amc_quotation_detail_start_date'],
              'amc_quotation_detail_end_date' => $value['amc_quotation_detail_end_date'],
              'amc_quotation_detail_amount' => $value['amc_quotation_detail_amount'],
          );
          $basic_model->insertRecord($arr,"amc_quotation_detail_history");        
      }

      
      foreach($temp_quotation_payment_term as $v) {
          $payment_data = [
              'amc_quotation_id' => $quotation_history_id,
              'payment_title' => $v['payment_title'],
              'percentage' => $v['percentage'],
              'payment_days' => $v['payment_days'],
              'payment_amount' => $v['payment_amount'],
              'quotation_revised_id' => $id,
          ];
          $basic_model->insertRecord($payment_data,'amc_quotation_payment_term_history');
      }

      
      foreach($temp_quotation_project_term as $v) {
          $project_data = [
              'amc_quotation_id' => $quotation_history_id,
              'project_title' => $v['project_title'],
              'project_days' => $v['project_days'],
              'hash_code' => $v['hash_code'],
              'start_week' => $v['start_week'],
              'quotation_revised_id' => $id,
          ];
          $basic_model->insertRecord($project_data,'amc_quotation_project_term_history');
      }
  }
  

}
function predefined_email_string(){
  $basic_model = new basic_model();
  $setting_emails = $basic_model->getCustomRows("SELECT email FROM `email_setting`"); 
  return implode(',', array_map(function ($entry) {
        return $entry['email'];
    }, $setting_emails)
  );

}
function company_email_string(){
  $basic_model = new basic_model();
  $setting = $basic_model->getRow("setting","setting_id",1,'DESC',"setting_id");
  return isset($setting['setting_company_email'])? $setting['setting_company_email'] : '';
}
function predefine_send_email($data){
  extract($data);
  $basic_model = new basic_model();
  if(isset($email_to)){
    $to_mail = explode(",",$email_to);
    for($i=0; $i< sizeof($to_mail); $i++){
        $from = $email_from;
        $to = $to_mail[$i];
        $body = $email_body;
        $subject = $email_subject;
        $basic_model->sendEmail($from, $to, $body, $subject, $from_name = NULL);

    }
    return true;
  }
  return false;
}

function serial_no_delivery_note($year = false,$month = false) {
    $basic_model = new basic_model();
    $query = "SELECT max(delivery_note_no) as serial_no FROM `delivery_note` WHERE `delivery_note_no` LIKE '".$year.$month."%'";
    $res = $basic_model->getCustomRow($query);
    $num = ($res['serial_no'] == '')? $year.$month.sprintf("%04s", 1) : $res['serial_no']+1;
    return $num;
    
    //return date("Y").'-'.str_pad((int) $res['serial_no']+1,3,"0",STR_PAD_LEFT);
    //return date("Y").'-'.str_pad((int) $res['serial_no']+1,2,"0",STR_PAD_LEFT);
}
function serial_no_inventory($year = false) {
    $basic_model = new basic_model();
    
    $query = "SELECT max(receipt_no) as serial_no FROM `inventory_receive` WHERE `receipt_no` LIKE '".$year."%'";
    $res = $basic_model->getCustomRow($query);
    $num = ($res['serial_no'] == '')? $year.sprintf("%04s", 1) : $res['serial_no']+1;
    
    return $num;
    //return date("Y").'-'.str_pad((int) $res['serial_no']+1,3,"0",STR_PAD_LEFT);
    //return date("Y").'-'.str_pad((int) $res['serial_no']+1,2,"0",STR_PAD_LEFT);
}
function inventory_quantity_by_warehouse_product_deileverable($id, $warehouse_id){
  // For delivery_note
  $basic_model = new basic_model();
  if($warehouse_id != 0)
  {
    $data = $basic_model->getCustomRow("SELECT *,
    (
      (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1  AND `i2`.`product_id` = '".$id."' AND `i2`.`warehouse_id` = '".$warehouse_id."')
       -
      (SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0  AND `i3`.`product_id` = '".$id."' AND `i3`.`warehouse_id` ='".$warehouse_id."'
      ) 
    ) As total_quantity
    FROM `inventory` 
    WHERE `inventory_type` = 1   AND warehouse_id = '".$warehouse_id."'  AND `product_id` = '".$id."' ");
  }else
  {
    $data = $basic_model->getCustomRow("SELECT *,
    (
      (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1  AND `i2`.`product_id` = '".$id."' )
       -
      (SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0  AND `i3`.`product_id` = '".$id."' 
      ) 
    ) As total_quantity
    FROM `inventory` 
    WHERE `inventory_type` = 1   AND `product_id` = '".$id."'  ");
  }
  
  return ($data['total_quantity'] == null)?0:$data['total_quantity'];
}

function InvoiceStatusData() {
  $InvoiceStatusData = array(
    0 => "Expire",
    1 => "Final",
    2 => "Revision",
    3 => "Draft",
    4 => "Payment Received",
  );
  return $InvoiceStatusData;
}

function ProjectStatusData() {
  $basic_model = new basic_model();
  $query = "SELECT * FROM `project_status` ";
  $res = $basic_model->getCustomRows($query);
  return $res;
}
function basic_setting(){
  $basic_model = new basic_model();
  $setting = $basic_model->getRow("setting","setting_id",1,'DESC',"setting_id");
  return $setting;
}

function customized_round($num)
{

   if(strpos($num, ".") !== false)
        {
          $num =  strval($num);
          $tmp =  explode(".", $num);
          $firstVal = $tmp[0];

          $secVal = "00";
          if(strlen($tmp[1])>=2)
           $secVal= substr($tmp[1],0,2);

          if(strlen($tmp[1])==1)
           $secVal= $tmp[1] ."0";

          $final = $firstVal . "." . $secVal;
          $num =  floatval($final);
          return $num;
        }
        else{
          return $num;
        }
}

function numberTowords($num)
    {
        $ones = array(
        0 =>"ZERO",
        1 => "ONE",
        2 => "TWO",
        3 => "THREE",
        4 => "FOUR",
        5 => "FIVE",
        6 => "SIX",
        7 => "SEVEN",
        8 => "EIGHT",
        9 => "NINE",
        10 => "TEN",
        11 => "ELEVEN",
        12 => "TWELVE",
        13 => "THIRTEEN",
        14 => "FOURTEEN",
        15 => "FIFTEEN",
        16 => "SIXTEEN",
        17 => "SEVENTEEN",
        18 => "EIGHTEEN",
        19 => "NINETEEN",
        "014" => "FOURTEEN"
        );
        $tens = array( 
        0 => "ZERO",
        1 => "TEN",
        2 => "TWENTY",
        3 => "THIRTY", 
        4 => "FORTY", 
        5 => "FIFTY", 
        6 => "SIXTY", 
        7 => "SEVENTY", 
        8 => "EIGHTY", 
        9 => "NINETY" 
        ); 
        $hundreds = array( 
        "HUNDRED", 
        "THOUSAND", 
        "MILLION", 
        "BILLION", 
        "TRILLION", 
        "QUARDRILLION" 
        ); /*limit t quadrillion */
        $num = number_format($num,2,".",","); 
        $num_arr = explode(".",$num); 
        $wholenum = $num_arr[0]; 
        $decnum = $num_arr[1]; 
        $whole_arr = array_reverse(explode(",",$wholenum)); 
        krsort($whole_arr,1); 
        $rettxt = ""; 
        foreach($whole_arr as $key => $i){
            while(substr($i,0,1)=="0")
                $i=substr($i,1,5);
                if($i < 20){ 
                $rettxt .= $ones[$i]; 
                }elseif($i < 100){ 
                    if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
                    if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
                }else{ 
                    if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
                    if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
                    if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
                } 
                if($key > 0){ 
                    $rettxt .= " ".$hundreds[$key]." "; 
                }
        } 
        if($decnum > 0){
            $rettxt .= " and ";
            if($decnum < 20){
                //$rettxt .= $ones[$decnum];
            }elseif($decnum < 100){
                //$rettxt .= $tens[substr($decnum,0,1)];
                if($ones[substr($decnum,1,1)] != "ZERO"){
                   // $rettxt .= " ".$ones[substr($decnum,1,1)];
                }
            }

            $rettxt .= $decnum."/100";
        }
        return $rettxt;
    }
    function find_dn_qunatity_by_qt_no($quotation, $prod_id){
        $basic_model = new basic_model();
        $delivery_data = $basic_model->getCustomRow('SELECT p.product_id as prod_id,  delivery_note.*, delivery_note_detail.*,  SUM(delivery_note_detail.delivery_note_detail_quantity) as total_sum_dn_quantity from delivery_note 
                LEFT JOIN delivery_note_detail ON delivery_note_detail.delivery_note_id = delivery_note.delivery_note_id
                Right JOIN product p ON p.product_id = delivery_note_detail.product_id
                where delivery_note.invoice_id='.$quotation.' AND  delivery_note_detail.product_id = '.$prod_id);
        return @$delivery_data['total_sum_dn_quantity'];
    }
    function find_rev_no($no){
      if(!empty($no)){
          $basic_model = new basic_model();
          $get_no = 0;
          $data = $basic_model->getCustomRow("SELECT invoice_revised_no
            FROM `invoice` where invoice_id = '".$no."' AND revision = 0 ");
          if($data){
            $get_no = $data['invoice_revised_no'];
          }else{
            $data = $basic_model->getCustomRow("SELECT q.quotation_revised_no
            FROM `quotation` q where q.quotation_no = '".$no."' AND q.quotation_revised_id = 0 ");
            if($data){
              $get_no =  $data['quotation_revised_no'];
            }else{
              $get_no = 0;
            }
          }
          return ($get_no > 0)? '-R'.number_format($get_no) : '';
      }
     

    }
    function quotation_num_format($val){
      //return number_format((float)round($val), 2, '.', '');
      return number_format((float)$val, 2, '.', '');
      //return cutNum($val);
    }
    function cutNum($num, $precision = 2) {
      return floor($num) . substr(str_replace(floor($num), '', $num), 0, $precision + 1);
    }

    function amc_activation_date($date){
      $timestamp = strtotime($date);
      $day = date('d', $timestamp);
      $month = date('m', $timestamp);
      $year = date('Y', $timestamp);
      $yesterday = mktime (0, 0, 0, $month, $day - 1, $year + 1);
      return date ('Y-m-d', $yesterday);
    }

    function numberTowords2(float $number, $a,$b)
    {
      $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'One', 2 => 'Two',
        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
        7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
        10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
        13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
        16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
        19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
        40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
        70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $digits = array('', 'Hundred','Thousand','lakh', 'crore');

    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }

    $rupees = implode('', array_reverse($str));
    $paise = '';

    if ($decimal) {
        $paise = 'and ';
        $decimal_length = strlen($decimal);

        if ($decimal_length == 2) {
            if ($decimal >= 20) {
                $dc = $decimal % 10;
                $td = $decimal - $dc;
                $ps = ($dc == 0) ? '' : '-' . $words[$dc];

                $paise .= $words[$td] . $ps;
            } else {
                $paise .= $words[$decimal];
            }
        } else {
            $paise .= $words[$decimal % 10];
        }

        $paise .= ' '.$b;
    }

    return ($rupees ? $rupees . $a.' ' : '') . $paise ;
  }

    
?>