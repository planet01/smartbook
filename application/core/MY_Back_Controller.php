<?php
class MY_Back_Controller extends CI_Controller {
	public $user_id;
    public $current_date;
    public $logged_in;

    public function __CONSTRUCT(){
        parent::__construct();
        $this->load->model('usermodel');
        $this->load->model('basic_model');
        $this->load->model('common_model');
        $this->load->database();
        $this->user_type =''; //1 => admin, 2 => employee, 3 => customer	
        $this->user_role = '';
        $this->user_role_dashboard = '';
        // $logged_in = $this->session->userdata('admin_is_logged');
        // $this->user_id = $this->session->userdata('id');
        // $this->current_date = date("Y-m-d h:i:s A");
    }
    function is_login_admin(){
        $logged_in = $this->session->userdata('admin_is_logged');
        $user_id = $this->session->userdata('admin_id');
        $this->user_type = $this->session->userdata('user_type');
        $user_data = $this->basic_model->getCustomRow("SELECT * FROM user where user_id = '".$user_id."'");
        if(@$user_data['user_type'] == 2)
        {
          $this->user_role = $this->basic_model->getCustomRows("SELECT * FROM  assign_role ar  JOIN module m ON m.module_id = ar.module_id WHERE ar.role_id = '".$user_data['role_id']."' ");
          $this->user_role_dashboard = $this->basic_model->getCustomRows("SELECT * FROM  assign_role_dashboard ar  JOIN department d ON d.department_id = ar.department_id WHERE ar.role_id = '".$user_data['role_id']."' ");
        }
        // print_b($this->user_role);
        if( $logged_in ){
            return true;
        }else{
            return false;
        }
    }
	function template_signin($main_content='', $data = array()){
		$this->load->view('include/header_signin',$data);
        $this->load->view($main_content);
        $this->load->view('include/footer_signin');
	}
    function template_view($main_content='', $data = array()){
        $this->load->view('include/header',$data);
        $this->load->view('include/sidebar');
        $this->load->view($main_content);
        $this->load->view('include/footer');
    }
    function stock_notification(){
      $data = $this->basic_model->getCustomRows("SELECT p.*, ( (SELECT COALESCE(SUM(`i2`.`inventory_quantity`), 0) FROM `inventory` `i2` WHERE `i2`.`inventory_type` = 1 AND `i2`.`product_id` = `p`.`product_id` ) - ( SELECT COALESCE(SUM(`i3`.`inventory_quantity`), 0) FROM `inventory` `i3` WHERE `i3`.`inventory_type` = 0 AND `i3`.`product_id` = `p`.`product_id` ) ) AS total_inventory_quantity FROM `product` `p` RIGHT JOIN `inventory` `i` ON `i`.`product_id` = `p`.`product_id` WHERE `i`.`inventory_type` = 1 AND `p`.product_is_active = 1 AND `p`.notify = 0 GROUP BY `p`.`product_id`");
      $product = Array();
      if($data){
          $temp  = "<table>";
          $temp .= "<tr>";
          $temp .= "<td>";
          $temp .= "Product Name";
          $temp .= "</td>";
          $temp .= "<td>";
          $temp .= "Available Quantity";
          $temp .= "</td>";
          $temp .= "</tr>";
          foreach($data as $data){
            if($data['min_quantity'] && $data['total_inventory_quantity'] <= $data['min_quantity']){
              $product[]=[
                'id' => $data['product_id'],
                'name' => $data['product_name'],
                'total'=> $data['total_inventory_quantity']
              ];
              $temp .= "<tr>"; 
              $temp .= "<td>"; 
              $temp .=  $data['product_name'];
              $temp .= "</td>"; 
              $temp .= "<td>"; 
              $temp .=  $data['total_inventory_quantity'];
              $temp .= "</td>"; 
              $temp .= "</tr>"; 

            }
          }
          $temp .= "</table>";
          try{
            $this->basic_model->endEmailSimple(EMAIL_FROM, EMAIL_TO, $temp, "Stock Alert"); 
            foreach($product as $prod){
                $this->basic_model->updateRecord(['notify' => 1], 'product', 'product_id', $prod['id']);
            }
            return [
                'status' => true,
                'message' => "Success"
            ];
          }catch(Exeception $e){
            return [
                'status' => false,
                'message' => $e
            ];
          }
          
      }
    }

    function update_notify($id){
       $this->basic_model->updateRecord(['notify' => 0], 'product', 'product_id', $id);
    }
}
?>
