<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Settings
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':''; ?>
                        <?= (isset($page_heading))?$page_heading:''; ?>
                </a>
            </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="">                             
                                <h3 class="heading-controls">Comprehensive & service warranty coverage area detail</h3>
                                <div class="clearfix"></div>
                                     <div class="form-group">
                                        <div class="row" >
                                            <div class="col-md-12">
                                              <div class="dataTables_wrapper-1440" >
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <!-- <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th colspan="3"><center>ammar</center></th>
                                                    </tr> -->
                                                    <tr>
                                                      <th class="text-center" style="min-width:60px">Add/Remove Row</th>
                                                      <th class="text-center" style="min-width:200px">Features</th>
                                                      <?php
                                                       foreach(@$warranty_type as $type){
                                                                ?>
                                                         <th class="text-center" style="min-width:160px"><?= $type['title']?></th>
                                                       <?php }  ?>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="customFields">
                                                    <tr class="txtMult">
                                                      <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                                      <td colspan="6"></td>
                                                    </tr>
                                                      <?php
                                                      if(@$warranty != null && !empty(@$warranty)){
                                                      foreach(@$warranty as $warranty){
                                                     ?>

                                                     <tr class="txtMult">
                                                            <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                            <td>
                                                                <input type="text" class="features" required style="width:100%" id="features" name="features[]"
                                                                value="<?= @$warranty['title'] ?>" placeholder="" />
                                                            </td>
                                                            <?php 
                                                            $c= 1;
                                                            foreach(@$warranty_type as $type){
                                                                $value = 2;
                                                                foreach($warranty_setting as $setting){
                                                                    if($warranty['warranty_id'] == $setting['warn_id'] && $type['warranty_type_id'] == $setting['type_id']){
                                                                        $value = $setting['value'];
                                                                    }
                                                                }
                                                                ?>
                                                             <td >
                                                                <!-- <input onchange="check_hendler('<?= $c ?>_<?= $warranty['warranty_id'] ?>')" id="warranty_type_id<?= $c ?>_<?= $warranty['warranty_id'] ?>" type="checkbox" <?=( @$value == 1 )? 'checked': '' ?> name="warranty_type_id<?= $c ?>[]" > -->
                                                                <div class="row">
                                                                    <div class="col-md-offset-4 col-md-12">
                                                                        <div class="wranty-det-style checkbox checkbox-inline check-success m-xl-auto">
                                                                            <input onchange="check_hendler('<?= $c ?>_<?= $warranty['warranty_id'] ?>')" id="warranty_type_id<?= $c ?>_<?= $warranty['warranty_id'] ?>" type="checkbox" <?=( @$value == 1 )? 'checked': '' ?> name="warranty_type_id<?= $c ?>[]" />
                                                                            <label style="padding-left:25px" for="warranty_type_id<?= $c ?>_<?= $warranty['warranty_id'] ?>"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <label class="control control-checkbox">
                                                                    <input onchange="check_hendler('<?= $c ?>_<?= $warranty['warranty_id'] ?>')" id="warranty_type_id<?= $c ?>_<?= $warranty['warranty_id'] ?>" type="checkbox" <?=( @$value == 1 )? 'checked': '' ?> name="warranty_type_id<?= $c ?>[]" />
                                                                    <div class="control_indicator"></div>
                                                                </label> -->

                                                                <input type="hidden" value="<?=( @$value == 1 )? '1': '0' ?>" id="warranty_type_id_all<?= $c ?>_<?= $warranty['warranty_id'] ?>" name="warranty_type_id_all<?= $c ?>[]" />
                                                                <!-- <select name="warranty_type_id<?= $c ?>[]" style="width:100%" id="warranty_type_id" class="form-control warranty_type_id" required>
                                                                    <option value="" selected="selected" disabled="">- Select Type -</option>
                                                                    <option value="1" <?=( @$value == 1 )? 'selected': '' ?>>Enabled</option>
                                                                    <option value="0" <?=( @$value == 0 )? 'selected ': '' ?>>Disabled</option>
                                                                </select> -->
                                                            </td>
                                                        <?php   
                                                            $c++; } ?>
                                                        </tr>
                                                    <?php } } ?>

                                                  </tbody>
                                                </table>
                                              </div>  
                                            </div>
                                    </div>
                                </div>
                                 <div class="clearfix"></div>                                
                            </div>

                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['setting_id']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {
        $('#header_image, #footer_image, #logo, #email_footer').fileuploader({
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-inner">' +
                '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                '<p>or</p>' +
                '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
               onRemove: function(item) {
                 $.post('<?= site_url('setting/delete'); ?>', {
                 file: item.name,
                 data: {
                   image_file_id:"<?= @$data['product_id']; ?>",
                   file:item.name,
                   image_post_file_id:item.data.image_file_id
                 }
                 });
               },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            },
        });
   
    });
    $(document).ready(function() {

        
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });

    });
    $(document).ready(function() {

        $("form.validate").validate({
            rules: {
                warranty_detail: {
                    required: true
                },
            },
            messages: {
                setting_company_name: "This field is required.",
                setting_company_address: "This field is required.",
                setting_company_postal: "This field is required.",
                setting_company_city: "This field is required.",
                setting_company_country: "This field is required.",
                setting_company_reg: "This field is required.",
                setting_company_bank_ac_no: "This field is required.",
                setting_company_bank_branch_name: "This field is required.",
                quotation_prfx: "This field is required.",
                invoice_prfx: "This field is required.",
                delivery_prfx: "This field is required.",
                material_prfx: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit   
                error("Please input all the mandatory values marked as red");
 
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    $(document).ready(function(){
        var max_fields      = 6; 
        var add_button      = $("#customFields .addCF");
        var x = 1; 
        var box_size = "<?php echo sizeof($warranty_type); ?>";
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="features" required  style="width:100%" id="features'+x+'" data-optional="0" name="features[]" value="" placeholder="" />';
            temp    += '</td>';
            for(var c = 1; c<=box_size; c++){
                var nid = parseInt(x+c);
                temp    += '<td>';
                /*temp    += '<select required class="core form-control input-signin-mystyle select2" id="warranty_type_id'+nid+'"name="warranty_type_id'+c+'[]">';
                temp    += '<option value="1"> Enabled</option>';
                temp    += '<option value="0"> Disabled</option>';
                temp    += '</select>';*/
                /*temp    += '<label class="control control-checkbox"><input type="checkbox" onchange="check_hendler(\'new_'+c+'_'+x+'\')" id="warranty_type_idnew_'+c+'_'+x+'" "name="warranty_type_id'+c+'[]"/><div class="control_indicator"></div></label>';
                temp    += '<input type="hidden" value="0"  id="warranty_type_id_allnew_'+c+'_'+x+'" name="warranty_type_id_all'+c+'[]"/>';*/

                temp    += '<div class="row">';
                temp    += '<div class="col-md-offset-4 col-md-12">';
                temp    += '<div class="wranty-det-style checkbox checkbox-inline check-success m-xl-auto">';
                temp    +=  '<input onchange="check_hendler(\'new_'+c+'_'+x+'\')" id="warranty_type_idnew_'+c+'_'+x+'" type="checkbox" name="warranty_type_id'+c+'[]"><label style="padding-left:25px" for="warranty_type_idnew_'+c+'_'+x+'"></label>';
                temp    += '</div>';
                temp    += '</div>';
                temp    += '</div>';
                 temp    += '<input type="hidden" value="0"  id="warranty_type_id_allnew_'+c+'_'+x+'" name="warranty_type_id_all'+c+'[]"/>'
                temp    += '</td>';
           }
        
            temp    += '</tr>';
            $("#customFields").append(temp);
          // }
        });
        $("#customFields").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });
      
    });
   /* $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){ 
                $(this).attr('value','1');
            }
            else if($(this).prop("checked") == false){
                $(this).attr('value','0');
            }
        });*/
    function check_hendler(id) {
        var box = $("#warranty_type_id"+id);
        if(box.prop("checked") == true){
           $("#warranty_type_id_all"+id).val(1);
        }
        else if(box.prop("checked") == false){
           $("#warranty_type_id_all"+id).val(0);
        }
    }

</script>
