<style type="text/css">
.table1-td-h4 {
    color: #333333;
}

.table1-td-h4-2 {
    color: #7f7f7f;
}

.table1-td-quotHeading {
    text-align: right;
    font-size: 22px;
    font-weight: bold;
}

.product-table2-th-details {
    border: 1px solid #adadad;
    background-color: #3c3d3a;
    color: #fff;
}

.product-table2-txt-align {
    text-align: center;
}

.product-table1-txt-align {
    position: relative;
    right: 0;
}

.product-table2-td-details {
    border: 1px solid #adadad;
}

.bank-table2-th-details {
    border: 1px solid #adadad;
    background-color: #3c3d3a;
    color: #fff;
    /*font-family: Calibri;
        font-size: 16px;*/
    font-family: Calibri;
    font-size: 12px;
}

.bank-table2-td-details {
    background-color: #3c3d3a;
    color: #fff;
    border: 1px solid #adadad;
    /*font-family: Calibri;
        font-size: 16px;*/
    font-family: Calibri;
    font-size: 12px;
}

.sub-total-heading {
    font-weight: bold;
    border-left: 1px solid #adadad;
    text-align: right;
}

.sub-total-detail {
    border-right: 1px solid #adadad;
    text-align: right;
    padding-right: 7px;
}

.less-special-discount-heading {
    font-weight: bold;
    border-left: 1px solid #adadad;
    text-align: right;
}

.less-special-discount-detail {
    border-right: 1px solid #adadad;
    text-align: right;
    padding-right: 7px;
}

.buyback-discount-heading {
    font-weight: bold;
    border-left: 1px solid #adadad;
    text-align: right;
}

.buyback-discount-detail {
    border-right: 1px solid #adadad;
    text-align: right;
    padding-right: 7px;
}

.netAmount-heading {
    font-weight: bold;
    text-align: right;
    border-top: 1px solid #adadad;
    border-left: 1px solid #adadad;
}

.netAmount-detail {
    font-weight: bold;
    text-align: right;
    border-top: 1px solid #adadad;
    border-right: 1px solid #adadad;
    padding-right: 7px;
}

.vat-heading {
    font-weight: bold;
    text-align: right;
    border-left: 1px solid #adadad;
    border-bottom: 1px solid #adadad;
}

.vat-details {
    text-align: right;
    border-right: 1px solid #adadad;
    border-bottom: 1px solid #adadad;
    padding-right: 7px;
}

.netAmount-inc_VAT-heading {
    font-weight: bold;
    text-align: right;
    border-left: 1px solid #adadad;
    border-bottom: 1px solid #adadad;
}

.netAmount-inc_VAT-detail {
    font-weight: bold;
    text-align: right;
    border-right: 1px solid #adadad;
    border-bottom: 1px solid #adadad;
    padding-right: 7px;
}

.net_amount {
    font-weight: bold;
    text-align: right;
    border-right: 1px solid #adadad;
    padding-right: 7px;
}

.border-bot {
    border-bottom: 1px solid #adadad;
}

.div-controls {
    margin-left: 7%;
    margin-right: 7%;
}

.table_align {
    margin-left: 10%;
    margin-right: 10%;
}

.font-controls {
    font-family: Calibri;
    font-size: 16px;
}

.item-dtable-font-controls {
    font-family: Calibri;
    font-size: 16px;
}

.payment-font-controls,
.bankDetails-font-controls {
    font-family: Calibri;
    font-size: 12px;
}

.optional-table-th {
    border: 1px solid #adadad;
    background-color: #c4bb98;
}

.optional-table-td {
    border: 1px solid #adadad;
}

.tr1-th-first {
    border-top: 1px solid black;
    text-align: right;
    border-left: 1px solid black;
}

.tr1-th-sec {
    text-align: right;
    border-left: 1px solid black;
}

.tr1-th {
    border-top: 1px solid black;
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr1-th-sec {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr2-th-first {
    text-align: right;
    border-left: 1px solid black;
    border-bottom: none;
}

.tr2-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr3-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr3-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr4-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr4-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr5-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr5-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr6-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr6-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr7-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr7-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr8-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr8-th {
    border-right: 1px solid black;
    border-left: 1px solid black
}

.tr9-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr9-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr10-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr10-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr11-th-first {
    text-align: right;
    border-left: 1px solid black;
}

.tr11-th {
    border-right: 1px solid black;
    border-left: 1px solid black;
}

.tr12-th-first {
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    text-align: right;
    border-left: 1px solid black;
    background-color: #d9d9d9;
}

.tr12-th {
    border-bottom: 1px solid black;
    border-top: 1px solid black;
    border-right: 1px solid black;
    border-left: 1px solid black;
    background-color: #d9d9d9;
}

tr:nth-child(3) .tr1-th-first,
tr:nth-child(3) .tr1-th {
    border-top: none;
}

tr:nth-child(5) .tr1-th-first,
tr:nth-child(5) .tr1-th {
    border-top: none;
}

tr:nth-child(7) .tr1-th-first,
tr:nth-child(7) .tr1-th {
    border-top: none;
}

tr:nth-child(9) .tr1-th-first,
tr:nth-child(9) .tr1-th {
    border-top: none;
}

tr:nth-child(11) .tr1-th-first,
tr:nth-child(11) .tr1-th {
    border-top: none;
}

.notes-font-controls {
    font-family: Calibri;
    font-size: 12px;
}

.Thanks-font-controls {
    font-family: Calibri;
    font-size: 16px;
}

.covr_letter {
    margin-right: 8%;
    margin-left: 8%;
    font-family: calibri;
}

.img_margin {
    padding: 10px 10px;
}

@page toc {
    size: A4;
}

span,
strong,
em,
i,
p {
    font-family: calibri;
}

.font-inline-discount {
    /* font-size: 20px !important; */
}
</style>


<table style="border-collapse: collapse;" width="100%" class="table_align">

    <tr>
        <td width="500px" align="left" style="padding-top: 10px;">
            <h4 style="font-size: 18px;" class="table1-td-h4"><span>Accounts
                    <!-- <?= @$employee_data['department_title'] ?> --> Department
                </span><br><span><?= @$customer_data['user_company_name'] ?></span></h4>
            <?php if ($customer_data['tax_reg']) { ?>
            <h4 class="" style="font-family:calibri;font-size: 18px;text-align: right;font-weight: bold;">T.R.N #
                <?= @$customer_data['tax_reg']; ?></h4>
            <?php } ?>
            <h5 style="font-size: 18px;" class="table1-td-h4-2 ">
                <span>
                    <!-- <?= @$setval['setting_company_address']; ?> --><?= @$customer_data['user_address']; ?>
                </span>
            </h5>
        </td>

        <td align="right" width="500px" style="">
            <span class="table1-td-quotHeading"
                style="font-family:calibri;"><?= ($setval['setting_company_country'] == $customer_data['user_country']) ? 'TAX' : ''; ?>
                INVOICE</span><br>
            <h4 class="" style="font-family:calibri;font-size: 18px;text-align: right;font-weight: bold;">#
                <?= @$setval["company_prefix"] . @$setval["invoice_prfx"] ?><?= (@$invoice['invoice_revised_no'] > 0) ? @$invoice['invoice_no'] . '-R' . number_format(@$invoice['invoice_revised_no']) : @$invoice['invoice_no']; ?>
            </h4>
            <!--  <h4 class="" style="font-family:calibri;font-size: 18px;text-align: right;font-weight: bold;">T.R.N # <?= @$setval['setting_company_reg']; ?></h4> -->

            <?= ($invoice['invoice_po_no'] != '') ? ' <h4 class="" style="font-size: 18px;text-align: right;font-weight: bold;font-family:calibri;">P.O. # ' . $invoice['invoice_po_no'] . '</h4>' : '' ?>
            <h4 class="" style="font-family:calibri;font-size: 18px;text-align: right;font-weight: bold;">T.R.N #
                <?= @$setval['setting_company_reg']; ?>
            </h4>
            <span style="font-size: 18px;" class="">Date : <?= @$invoice['invoice_date']; ?></span>
        </td>
    </tr>

    <!-- <tr>
                <td  align="left" style="" >
                
                </td>
                <td colspan="3" width="500px" align="right" style="">
                
                </td>
              </tr> -->

</table>



<?php if ($inline_discount) { ?>
<table style="border-collapse: collapse;margin-top: 10px;" class="table_align" width="100%">

    <thead>

        <tr>

            <th align="center" class="product-table2-th-details font-controls font-inline-discount" height="30px"
                width="40px">#</th>

            <th class="product-table2-th-details font-controls font-inline-discount" align="left" height="30px"
                width="500px" style="padding:0 0 0 10px;">Item & Description</th>

            <th class="product-table2-th-details font-controls font-inline-discount" height="30px" width="60px">Qty</th>

            <th class="product-table2-th-details font-controls font-inline-discount" height="50px" width="140px">Unit
                Price <br> <?= @$invoice_currency_name['title'] ?></th>

            <th class="product-table2-th-details font-controls font-inline-discount" height="50px" width="120px">
                Discount</th>

            <th class="product-table2-th-details font-controls font-inline-discount" height="50px" width="120px">Amount
                <br> <?= @$invoice_currency_name['title'] ?></th>

        </tr>

    </thead>

    <tbody>
        <?php
         $i = 1;
         $total_line_discount_amount = 0;
         foreach ($data as $dt) {
         ?>
        <tr>

            <td class="product-table2-td-details font-inline-discount" style="text-align:center;" height="30px">
                <?= $i; ?></td>

            <td class="product-table2-td-details font-controls font-inline-discount" style="padding:15px 25px; ">
                <div
                    style="<?= (!empty($dt['invoice_detail_description'])) ? 'font-weight: bold' : 'font-weight: normal !important;' ?>">
                    <?= @$dt['product_sku'] ?> - <?= @$dt['product_name'] ?></div>
                <?php if ($dt['invoice_detail_description'] != '') { ?>
                <br><?= $dt['invoice_detail_description'] ?>
                <?php } ?>
            </td>

            <td align="center" class="product-table2-td-details font-controls font-inline-discount"
                style="vertical-align: top;padding-top: 1%;"><?= $dt['invoice_detail_quantity'] ?>
            </td>

            <td align="center" class="product-table2-td-details font-controls font-inline-discount"
                style="vertical-align: top;padding-top: 1%;padding-right:10px;padding-left:10px;">
                <?php
                  if (strtolower($invoice_currency_name['title']) != 'usd') {
                     $price = $dt['invoice_detail_base_rate'];
                  } else {
                     $price = $dt['invoice_detail_usd_rate'];
                  }
                  echo number_format((float)$price, 2, '.', '');
                  ?>
            </td>

            <td align="center" class="product-table2-td-details font-controls font-inline-discount"
                style="vertical-align: top;padding-top: 1%;">
                <?php
                  $discount_amount = 0.00;
                  $discount_percentage = '';
                  if ($dt['invoice_detail_total_discount_type'] == 1) {

                     if($dt['invoice_detail_quantity'] == "Lot")
                     {
                        $discount_amount = ($price * (int)1 / 100) * $dt['invoice_detail_total_discount_amount'];
                        $discount_percentage = number_format((float)$dt['invoice_detail_total_discount_amount'], 2, '.', '') . '%';
                     }
                     else{
                        $discount_amount = ($price * (int)$dt['invoice_detail_quantity'] / 100) * $dt['invoice_detail_total_discount_amount'];
                        $discount_percentage = number_format((float)$dt['invoice_detail_total_discount_amount'], 2, '.', '') . '%';
                     }

                  } else if ($dt['invoice_detail_total_discount_type'] == 2) {
                     $discount_amount = $dt['invoice_detail_total_discount_amount'];
                     $discount_percentage = '';
                  }
                  ?>
                <?php
                  $total_line_discount_amount += number_format((float) $discount_amount, 2, '.', '');
                  ?>
                <?= number_format((float) $discount_amount, 2, '.', ''); ?>
                <br> <?= @$discount_percentage; ?>
            </td>

            <td class="product-table2-td-details font-controls font-inline-discount"
                style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 1%;">
                <?= number_format((float)$dt['invoice_detail_total_amount'], 2, '.', ''); ?>

            </td>

        </tr>

        <?php
            $i++;
         }
         ?>
        <?php
            if (@$invoice['invoice_report_line_break_item'] > 0) {
               for ($i = 0; $i < $invoice['invoice_report_line_break_item']; $i++) {
            ?>
        <tr style="border:none;">
            <td colspan="5" height="30px" class="sub-total-heading font-controls font-inline-discount"></td>
            <td colspan="6" height="30px" class="sub-total-detail font-controls font-inline-discount"
                style="color:white;">
                <?= number_format((float)$invoice['invoice_subtotal'], 2, '.', ''); ?>
            </td>
        </tr>
        <?php
               }
            }
            ?>
        <tr style="border:1px solid #adadad; border-bottom: none;">

            <td colspan="5" height="30px" class="sub-total-heading font-controls font-inline-discount">Sub Total
                (Including <?= @$invoice_currency_name['title'] ?>
                <?= number_format((float) $total_line_discount_amount, 2, '.', ''); ?>/-Discount)</td>

            <td colspan="6" height="30px" class="sub-total-detail font-controls font-inline-discount">

                <?= number_format((float)$invoice['invoice_subtotal'], 2, '.', ''); ?>

            </td>
        </tr>

        <?php 
      if ($invoice['invoice_buyback'] != '' &&  $invoice['invoice_buyback'] > 0) { ?>

        <tr>

            <td colspan="5" height="30px" class="buyback-discount-heading font-controls font-inline-discount">Buyback
                Dsicount</td>

            <td colspan="6" height="30px" class="buyback-discount-detail font-controls font-inline-discount">
                <?= number_format((float)$invoice['invoice_buyback'], 2, '.', ''); ?>
            </td>

        </tr>
        <?php } ?>
        <span class="font-inline-discount">
            <?php $bottom_line = ($setval['setting_company_country'] != $customer_data['user_country']) ? " netAmount-inc_VAT-detail " : ""; ?>
        </span>

        <?php if ($setval['setting_company_country'] == $customer_data['user_country']) { ?>
        <tr>

            <td colspan="5" height="30px" class="vat-heading font-controls"><?= $setval['tax'] ?>% VAT</td>

            <td colspan="6" height="30px" class="vat-details font-controls">
                <?php $tax = ($invoice['invoice_subtotal'] / 100) * $invoice['invoice_tax'] ?>
                <?= number_format((float) $invoice['invoice_tax_amount'], 2, '.', ''); ?>

            </td>

        </tr>

        <tr>
            <?php $nt_amount = $invoice['invoice_net_amount']; ?>
            <td colspan="3" height="30px" style="text-align:left;border-right:none;border-bottom:1px solid #adadad"
                class="netAmount-heading font-controls font-inline-discount <?= $bottom_line; ?>" width="630px">
                <?= numberTowords2(number_format((float) $nt_amount, 2, '.', ''), $invoice_currency_name['base_name'], $invoice_currency_name['Deci_name']) ?>
            </td>
            <td colspan="2" height="30px" style="border-left:none;" class="netAmount-inc_VAT-heading font-controls">Net
                Amount (Incl. VAT)</td>

            <td colspan="6" height="30px" class="netAmount-inc_VAT-detail font-controls"><b>
                    <?= number_format((float) $invoice['invoice_net_amount'], 2, '.', ''); ?>
                </b></td>

        </tr>
        <?php } else {
         ?>
        <tr>
            <?php $nt_amount = $invoice['invoice_subtotal'] - $invoice['invoice_buyback']; ?>
            <td colspan="3" height="30px" style="text-align:left;border-right:none;"
                class="netAmount-heading font-controls font-inline-discount <?= $bottom_line; ?>" width="630px">
                <?= numberTowords2(number_format((float) $nt_amount, 2, '.', ''), $invoice_currency_name['base_name'], $invoice_currency_name['Deci_name']) ?>
            </td>
            <td colspan="2" height="30px" style="border-left:none;border-right: none"
                class="netAmount-heading font-controls <?= $bottom_line; ?> font-inline-discount">Net Amount</td>

            <td colspan="6" height="30px"
                class="netAmount-detail font-controls <?= $bottom_line; ?> font-inline-discount">
                <b>

                    <?php 
                     $nt_amount = sprintf("%.4f",$nt_amount);
                     $nt_amount = substr_replace($nt_amount, "", -2);
                     echo $nt_amount; ?>
                </b>
            </td>

        </tr>
        <?php
         } ?>

    </tbody>

</table>
<?php } else { ?>
<table style="border-collapse: collapse;margin-top: 10px;" class="table_align" width="100%">
    <thead>
        <tr>

            <th align="center" class="product-table2-th-details font-controls" height="30px" width="40px">#</th>

            <th class="product-table2-th-details font-controls" align="left" height="30px" width="650px"
                style="padding:0 0 0 10px;">Item & Description</th>

            <th class="product-table2-th-details font-controls" height="30px" width="60px">Qty</th>

            <th class="product-table2-th-details font-controls" height="50px" width="140px">Unit Price <br>
                <?= @$invoice_currency_name['title'] ?></th>

            <th class="product-table2-th-details font-controls" height="50px" width="120px">Amount <br>
                <?= @$invoice_currency_name['title'] ?></th>

        </tr>

    </thead>

    <tbody>
        <?php
         $i = 1;
         foreach ($data as $dt) {
         ?>
        <tr>
            <td class="product-table2-td-details font-controls" style="text-align:center;" height="30px"><?= $i; ?></td>

            <td class="product-table2-td-details font-controls" style="padding:15px 25px;">
                <div
                    style="<?= (!empty($dt['invoice_detail_description'])) ? 'font-weight: bold' : 'font-weight: normal !important;' ?>">
                    <?= @$dt['product_sku'] ?> - <?= @$dt['product_name'] ?></div>
                <?php if ($dt['invoice_detail_description'] != '') { ?>
                <br><?= $dt['invoice_detail_description'] ?>
                <?php } ?>
            </td>

            <td align="center" class="product-table2-td-details font-controls"
                style="vertical-align: top;padding-top: 1%;"><?= $dt['invoice_detail_quantity'] ?>
            </td>
            <td align="center" class="product-table2-td-details font-controls"
                style="vertical-align: top;padding-top: 1%;padding-right:10px;padding-left:10px;">
                <?php
                  if (strtolower($invoice_currency_name['title']) != 'usd') {
                     $price = $dt['invoice_detail_base_rate'];
                  } else {
                     $price = $dt['invoice_detail_usd_rate'];
                  }
                  echo number_format((float)$price, 2, '.', '');
                  ?>
            </td>
            <td class="product-table2-td-details font-controls"
                style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 1%;">
                <?= number_format((float)$dt['invoice_detail_total_amount'], 2, '.', ''); ?>
            </td>

        </tr>
        <?php $i++;
         } ?>
    </tbody>
</table>
<?php
   if (@$invoice['invoice_report_line_break_item'] > 0) {
      for ($i = 0; $i < $invoice['invoice_report_line_break_item']; $i++) {
   ?>
<br>
<?php
      }
   }
   ?>
<table style="border-collapse: collapse;border-top: 1px solid #adadad;" width="100%" class="table_align">
    <tr>

        <td bgcolor="" colspan="4" height="30px" width="880px" class="sub-total-heading font-controls">Sub Total</td>
        <td colspan="5" height="30px" width="120px" class="sub-total-detail font-controls">
            <?= number_format((float)$invoice['invoice_subtotal'], 2, '.', ''); ?></td>
    </tr>
    <?php if ($invoice['invoice_total_discount_amount'] != '' &&  $invoice['invoice_total_discount_amount'] > 0) { ?>
    <tr>

        <td colspan="4" height="30px" class="sub-total-heading font-controls">
            <?php
               if (strlen(str_replace(' ', '', $invoice['discount_label'])) > 0) {
                  echo $invoice['discount_label'];
               } else {
                  echo "Less Special Discount";
               }
               ?>
        </td>

        <td colspan="5" height="30px" class="sub-total-detail font-controls">
            <?php
               $dicounted_amount = @$invoice['invoice_total_discount_amount'];
               if ($invoice['invoice_total_discount_type'] == 1) {
                  $dicounted_amount = ($invoice['invoice_subtotal'] / 100) * $invoice['invoice_total_discount_amount'];
               }
               ?>
            <?php 
               $dicounted_amount = sprintf("%.4f",$dicounted_amount);
               $dicounted_amount = substr_replace($dicounted_amount, "", -2);
               echo $dicounted_amount ?>
        </td>

    </tr>

    <?php }
      if ($invoice['invoice_buyback'] != '' &&  $invoice['invoice_buyback'] > 0) { ?>
    <tr>

        <td colspan="4" height="30px" class="buyback-discount-heading font-controls" width="630px">Buyback Dsicount</td>

        <td colspan="5" height="30px" class="buyback-discount-detail font-controls">
            <?= number_format((float)$invoice['invoice_buyback'], 2, '.', ''); ?>
        </td>

    </tr>
    <?php }
      $bottom_line = ($setval['setting_company_country'] != $customer_data['user_country']) ? " netAmount-inc_VAT-detail " : "";
      ?>

    <?php if ($setval['setting_company_country'] == $customer_data['user_country']) { ?>
    <tr>
        <?php $nt_amount = ((float) $invoice['invoice_subtotal'] - ((float)$invoice['invoice_buyback']) - (float)@$dicounted_amount); ?>
        <td colspan="4" height="30px" style="border-right: none !important;"
            class="netAmount-heading <?= $bottom_line; ?>  font-controls" width="630px">Net Amount</td>
        <td colspan="5" height="30px" class="netAmount-detail <?= $bottom_line; ?> net_amount font-controls"><b>
                <?php
                  $nt_amount = sprintf("%.4f",$nt_amount);
                  $nt_amount = substr_replace($nt_amount, "", -2);
                  echo $nt_amount ?>
            </b></td>

    </tr>
    <tr>

        <td colspan="4" height="30px" class="vat-heading font-controls" width="630px"><?= $setval['tax'] ?>% VAT</td>

        <td colspan="5" height="30px" class="vat-details font-controls" bgcolor="">
            <?php $tax = ($invoice['invoice_subtotal'] / 100) * $invoice['invoice_tax'] ?>
            <?= number_format((float) $invoice['invoice_tax_amount'], 2, '.', ''); ?>
        </td>

    </tr>

    <tr>

        <td colspan="2" height="30px" style="text-align:left;border-right:none;border-bottom:1px solid #adadad"
            class="netAmount-heading font-controls font-inline-discount <?= $bottom_line; ?>" width="315px">
            <?= numberTowords2(number_format((float) $nt_amount + $invoice['invoice_tax_amount'], 2, '.', ''), $invoice_currency_name['base_name'], $invoice_currency_name['Deci_name']) ?>
        </td>
        <td colspan="2" height="30px" style="border-left: none;" class="netAmount-inc_VAT-heading font-controls"
            width="310px">Net Amount (Incl. VAT)</td>

        <td colspan="5" height="30px" class="netAmount-inc_VAT-detail font-controls"><b>
                <?php
                  $temp = '';
                  $temp = sprintf("%.4f",$nt_amount + $invoice['invoice_tax_amount']);
                  $temp = substr_replace($temp, "", -2); 
                  echo $temp; ?>
            </b></td>
    </tr>
    <?php
      } else {
         $tax = 0;
      ?>
    <tr>
        <?php $nt_amount = ((float) $invoice['invoice_subtotal'] - ((float)$invoice['invoice_buyback']) - (float)$dicounted_amount); ?>
        <td colspan="2" height="30px" style="text-align:left;border-right:none;"
            class="netAmount-heading font-controls font-inline-discount <?= $bottom_line; ?>" width="315px">
            <?= numberTowords2(number_format((float) $nt_amount, 2, '.', ''), $invoice_currency_name['base_name'], $invoice_currency_name['Deci_name']) ?>
        </td>
        <td colspan="2" height="30px" style="border-left: none;border-right: none !important;"
            class="netAmount-heading <?= $bottom_line; ?>  font-controls" width="315px">Net Amount</td>
        <td colspan="5" height="30px" class="netAmount-detail <?= $bottom_line; ?> net_amount font-controls"><b>
                <?php
                  $nt_amount = sprintf("%.4f",$nt_amount);
                  $nt_amount = substr_replace($nt_amount, "", -2);
                  echo $nt_amount; ?>
            </b></td>

    </tr>
    <?php
      }
      ?>
</table>
<?php } ?>
<br />
<?php
if (strlen(str_replace(' ', '', $invoice['invoice_notes'])) > 0) {
?>
<div class="div-controls">
    <span class="notes-font-controls"><b>Notes :</b></span><br>
    <span class="notes-font-controls"><?= $invoice['invoice_notes']; ?></span>
</div>

<br /><br />
<?php } ?>

<div class="div-controls">
    <span class="payment-font-controls"><b>Payment :</b></span><br>
    <span class="payment-font-controls">
        <?php
      foreach ($terms as $term) {
      ?>
        <?= @$term['percentage'] ?>% <?= @$term['payment_title'] ?>
        (<?= (isset($term['payment_days']) && $term['payment_days'] > 0) ? 'After ' . $term['payment_days'] . ' Day' : ((isset($term['payment_days']) && $term['payment_days'] == 0) ? 'Immediate' : '') ?>)<br />
        <?php } ?>
    </span>
</div>

<br />

<?php
if (@$invoice['invoice_report_line_break'] > 0) {
   for ($i = 0; $i < $invoice['invoice_report_line_break']; $i++) {
?>
<br>
<?php
   }
}
?>

<div class="div-controls">
    <span class="bankDetails-font-controls"><b>Bank Details :</b></span>
</div>
<table style="border-collapse: collapse;margin-top: 10px;" class="table_align " width="100%">

    <thead>

        <tr>

            <th align="center" class="bank-table2-th-details font-controls" height="40px" width="250px">Beneficiary</th>

            <th colspan="<?= sizeof($bank) ?>" class="bank-table2-th-details font-controls" height="40px" width="750px"
                style="padding:0 0 0 10px;"><?= @$setval['setting_company_name']; ?></th>

        </tr>

    </thead>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Bank</td>
        <?php
      foreach ($bank as $bnk1) {
      ?>
        <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk1['bank']; ?>
        </td>
        <?php
      }
      ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Account No.</td>
        <?php
      foreach ($bank as $bnk2) {
      ?>
        <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk2['account_no']; ?>
        </td>
        <?php
      }
      ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">IBAN</td>

        <?php
      foreach ($bank as $bnk3) {
      ?>
        <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk3['iban']; ?>
        </td>
        <?php
      }
      ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Branch</td>

        <?php
      foreach ($bank as $bnk4) {
      ?>
        <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk4['branch']; ?>
        </td>
        <?php
      }
      ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Swift Code</td>

        <?php
      foreach ($bank as $bnk5) {
      ?>
        <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk5['swift_code']; ?>
        </td>
        <?php
      }
      ?>
    </tr>
    <br />

</table>

<br /><br />

<div class="div-controls">
    <span class="Thanks-font-controls"><b>Thanks,</b></span>
</div>

<br /><br /><br />

<div class="div-controls">
    <span class="Thanks-font-controls"><b>For <?= @$setval['setting_company_name'] ?></b></span>
</div>

<!--<pagebreak/>

             <table style="border-collapse: collapse;" width="100%" class="table_align">

              <tr>
                <td width="500px" height="10px" >
                    <h4 class="table1-td-h4"><span>Accounts Department</span><br><span>TetraPack</span></h4>
                </td>

                <td align="right" width="500px" height="30px">
                    <span class="table1-td-quotHeading">INVOICE</span><br>
                    <h4 class="product-table1-txt-align" style="text-align: right;font-weight: bold;"># SMGT/FM/201801</h4>
                    <h4 class="product-table1-txt-align" style="text-align: right;font-weight: bold;">T.R.N # 101010020204567</h4>
                </td>
              </tr>
              
              <tr>
                <td height="10px">
                  <h5 style="font-size: 14px;" class="table1-td-h4-2"><span>Suite# 1018,Al-Qusais Plaza,</span><br><span>Al-Qusais Ind.Area 2,Sharjah,</span><br><span>U.A.E</span></h5>
                </td>
                <td colspan="3" width="500px" align="right" style="padding-top: 30px;">
                  <span>Date : 19/08/2018</span>
                </td>
              </tr>

            </table>
            


            <table style="border-collapse: collapse;margin-top: 10px;" class="table_align" width="100%">

              <thead>

                <tr>

                  <th align="center" class="product-table2-th-details" height="30px" width="40px">#</th>

                  <th class="product-table2-th-details product-table2-txt-align" align="left" height="30px" width="650px" style="padding:0 0 0 10px;">Item & Description</th>

                  <th class="product-table2-th-details" height="30px" width="60px">Qty</th>

                  <th class="product-table2-th-details" height="50px" width="140px">Unit Price <br> AED</th>
                                    
                  <th class="product-table2-th-details" height="50px" width="120px">Amount <br> AED</th>

                </tr>

              </thead>
                 
              <tbody>
                
                <tr>

                    <td  class="product-table2-td-details" style="text-align:center;" height="30px">1</td>

                    <td class="product-table2-td-details" style="padding:15px 25px;">
                      <span style="font-size: 16px;font-weight: bold;">EQ-LD12En - Counter Display LD12</span>
                    </td>

                    <td align="center" class="product-table2-td-details" style="vertical-align: top;padding-top: 1%;" >10
                    </td>
                  
                    <td align="center" class="product-table2-td-details" style="vertical-align: top;padding-top: 1%;padding-right:10px;padding-left:10px;" >2000.00
                    </td>
                        
                    <td class="product-table2-td-details" style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 2%;">2100.00
                    </td>

                </tr>

                <tr>

                    <td  class="product-table2-td-details" style="text-align:center;" height="30px">1</td>

                    <td class="product-table2-td-details" style="padding:15px 25px;">
                      <span style="font-size: 16px;font-weight: bold;">EQ-LD12En - Counter Display LD12</span>
                    </td>

                    <td align="center" class="product-table2-td-details" style="vertical-align: top;padding-top: 1%;" >10</td>
                  
                    <td align="center" class="product-table2-td-details" style="vertical-align: top;padding-top: 1%;padding-right:10px;padding-left:10px;" >2000.00</td>               
                    
                    <td class="product-table2-td-details" style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 2%;">2100.00</td>

                </tr>

                <tr>
                   
                   <td colspan="4" height="30px" class="sub-total-heading">Sub Total</td>

                   <td colspan="5" height="30px" class="sub-total-detail">2100.00</td>                  
                </tr>
                 
                <tr>
                   
                   <td colspan="4" height="30px" class="sub-total-heading">Less Special Discount</td>

                   <td colspan="5" height="30px" class="sub-total-detail">100.00</td>                  
                
                </tr>

                <tr>
                
                <td colspan="4" height="30px" class="buyback-discount-heading" width="630px">Buyback Dsicount</td>

                <td colspan="5" height="30px" class="buyback-discount-detail" >500.00</td>
              
                </tr>               
                
                <tr>
                  
                  <td  colspan="4" height="30px" class="netAmount-heading" width="630px">Net Amount</td>

                  <td  colspan="5" height="30px" class="netAmount-detail" ><b>1500.00</b></td>
                
                </tr>
                <tr>
                  
                  <td colspan="4" height="30px" class="vat-heading" width="630px">5% VAT</td>

                  <td colspan="5" height="30px" class="vat-details" >100.00</td>
                
                </tr>

                <tr>
                  

                  <td colspan="4" height="30px" class="netAmount-inc_VAT-heading" width="630px">Net Amount (Incl. VAT)</td>

                  <td colspan="5" height="30px" class="netAmount-inc_VAT-detail" ><b>1400.00</b></td>
                
                </tr>
                
              </tbody>

            </table> 
            
            <br/>

            <div class="div-controls font-controls">
               <label><b>Notes:</b></label><br>
               <label>Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum </label>
            </div>

            <br/><br/>
            
            <div class="div-controls font-controls">
               <label><b>Payment :</b></label><br>
               <label>Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum </label>
            </div>

            <br/><br/>

            <div class="div-controls font-controls">
               <label><b>Bank Details :</b></label>
            </div>
            <table style="border-collapse: collapse;margin-top: 10px;" class="table_align" width="100%">

              <thead>

                <tr>

                  <th align="center" class="bank-table2-th-details" height="40px" width="250px">Beneficiary</th>

                  <th colspan="2" class="bank-table2-th-details product-table2-txt-align"  height="40px" width="750px" style="padding:0 0 0 10px;">SMART MATRIX GENERAL TRADING LLC.</th>

                </tr>

              </thead>

                <tr>
                    <td class="bank-table2-td-details" height="30px" style="padding-left: 20px;">Bank</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">RAK BANK</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">EMIRATES ISLAMIC</td>
                </tr>

                <tr>
                    <td class="bank-table2-td-details" height="30px" style="padding-left: 20px;">Account No.</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">123-1234-123-5436</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">123-1234-123-5436</td>
                </tr>

                <tr>
                    <td class="bank-table2-td-details" height="30px" style="padding-left: 20px;">IBAN</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">123-1234-123-5436</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">123-1234-123-5436</td>
                </tr>

                <tr>
                    <td class="bank-table2-td-details" height="30px" style="padding-left: 20px;">Branch</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">Al-Qusais Branch</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">NARAKAEAK</td>
                </tr>

                <tr>
                    <td class="bank-table2-td-details" height="30px" style="padding-left: 20px;">Swift Code</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">Al-Twar Branch</td>
                    
                    <td width="375px" style="padding-left: 10px;" class="product-table2-td-details">MEBLEAED</td>
                </tr>
                <br/>

              

            </table>

            <br/><br/>
            
            <div class="div-controls font-controls">
                <label><b>Thanks,</b></label>
            </div>

            <br/><br/><br/>
            
            <div class="div-controls font-controls">
                <label><b>For Company Name</b></label>
            </div> -->