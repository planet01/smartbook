<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Price Plan
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> -->
                </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" id="saa" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-4">
                                            <label class="form-label">Price ID</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <input name="plan_slog" type="text" value="<?= @$plan_data[0]['plan_slog']; ?>" class="form-control" placeholder="Enter price id">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                               <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-4">
                                            <label class="form-label">Description</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls main_editor">
                                                <i class=""></i>
                                                <!-- <textarea name="plan_description"  id="" class="form-control" placeholder="Enter Description"><?= @$plan_data[0]['plan_description']; ?></textarea> -->
                                                <input name="plan_description" type="text" id="" class="form-control" placeholder="Enter Description" value="<?= @$plan_data[0]['plan_description']; ?>">
                                                <span id="description_error" class="custom_error error" style="display: none;">This field is required.
                                                </span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>                                                                                   

                              <div class="clearfix"></div>
                                <div class="grid-body dataTables_wrapper-1440" >
                                    <table class="table dataTable" id="" >
                                        <thead>
                                            <tr class="priceplan_dt-controls">
                                                <th class="price_plan-dt-th1">Sr.No.</th>
                                                <th class="price_plan-dt-th2" >Model #</th>
                                                <th class="price_plan-dt-th3">Product Name</th>
                                                <th class="price_plan-dt-th4" colspan="3">Retail Price</th>
                                                <th class="price_plan-dt-th5" colspan="3">Reseller Price</th>
                                                <th class="price_plan-dt-th6" colspan="3">Partner Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <tr class="priceplan_dt-controls">
                                                <td class="price_plan-dt-td1" colspan="3"></td>
                                                <td class="price_plan-dt-td2">AED</td>
                                                <td class="price_plan-dt-td3">USD</td>
                                                <td class="price_plan-dt-td4">Max Discount in %</td>
                                                <td class="price_plan-dt-td5">AED</td>
                                                <td class="price_plan-dt-td6">USD</td>
                                                <td class="price_plan-dt-td7">Max Discount in %</td>
                                                <td class="price_plan-dt-td8">AED</td>
                                                <td class="price_plan-dt-td9">USD</td>
                                                <td class="price_plan-dt-td10">Max Discount in %</td>
                                            </tr>
                                            <?php
                                            if (isset($data) && @$data != null) {
                                                $count = 1;
                                                foreach($data as $k => $v) {
                                            ?>
                                                <tr class="txt-align-control">
                                                    <td><?php echo $count; ?></td>
                                                    <td>
                                                       <?= $v['product_sku']; ?>
                                                       <input type="hidden" name="product_id[]" value="<?= $v['product_id']; ?>" />
                                                       <input type="hidden" name="price_id[]" value="<?= @$v['id']; ?>" />
                                                    </td>
                                                    <td>
                                                       <?= $v['product_name']; ?>
                                                    </td>
                                                    <td>
                                                        <input name="retail_base_price[]" id="amount_deci1" type="text" value="<?= @int_to_float($v['retail_base_price']); ?>" class="form-control txtboxToFilter" data-optional="0"  placeholder="AED Price"/>
                                                    </td>
                                                    <td>
                                                        <input name="retail_usd_price[]" id="amount_deci2" type="text" value="<?= @int_to_float($v['retail_usd_price']); ?>" class="form-control txtboxToFilter" placeholder="USD Price"/>
                                                    </td>
                                                    <td>
                                                         <input name="retail_discount[]" id='retail_discount<?=$count?>' type="text" min="0"  style="width:100px;" value="<?= @$v['retail_discount']; ?>" class="form-control txtboxToFilter" placeholder="Discount"/>
                                                    </td>
                                                    <td>
                                                        <input name="reseller_base_price[]" id="amount_deci1" type="text" value="<?= @int_to_float($v['reseller_base_price']); ?>" class="form-control txtboxToFilter" placeholder="AED Price"/>
                                                    </td>
                                                    <td>
                                                         <input name="reseller_usd_price[]" id="amount_deci2" type="text" value="<?= @int_to_float($v['reseller_usd_price']); ?>" class="form-control txtboxToFilter" placeholder="USD Price"/>
                                                    </td>
                                                    <td>
                                                         <input name="reseller_discount[]" id='reseller_discount<?=$count?>'  type="text" min="0" style="width:100px;" value="<?= @$v['reseller_discount']; ?>" class="form-control txtboxToFilter" placeholder="Discount"/>
                                                    </td>
                                                     <td>
                                                        <input name="partner_base_price[]" id="amount_deci2" type="text" value="<?= @int_to_float($v['partner_base_price']); ?>" class="form-control txtboxToFilter" placeholder="AED Price"/>
                                                    </td>
                                                    <td>
                                                        <input name="partner_usd_price[]" id="amount_deci1" type="text" value="<?= @int_to_float($v['partner_usd_price']); ?>" class="form-control txtboxToFilter" placeholder="USD Price"/>
                                                    </td>
                                                    <td>
                                                        <input name="partner_discount[]" id='partner_discount<?=$count?>' type="text" min="0"  style="width:100px;" value="<?= @$v['partner_discount']; ?>" class="form-control txtboxToFilter" placeholder="Discount"/>
                                                    </td>
                                                </tr>
                                            <?php
                                                $count++; 
                                                }
                                            } 
                                            ?>

                                        </tbody>
                                    </table>
                                </div>

                              <div class="col-md-12 mt-4">
                                <div class="form-group price_plan-btn text-center">
                                  <!-- <button class="btn btn-success btn-cons pull-right" type="submit"><?php // ($page_title == 'add')?'Add New':'Edit'; ?> Product</button> -->
                                  <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                      <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                  <input name="id" type="hidden" value="<?= @$plan_data[0]['price_plan_id']; ?>">
                                  <input type="hidden" id="cat_type" value="">
                                </div>
                              </div>
                            </div>
                        </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script type="text/javascript">

    $(document).ready(function() { 
        $.validator.addMethod(
            "checkmax",
            function(value, element) {
                var check = true;
                
                if (value > 100) {
                    check = false;
                }
                return check;
            },
            
        );
        $("form.validate").validate({
           focusInvalid: true,
            rules: {
                plan_title: {
                    required: true
                },
                plan_description: {
                    required: true
                },
                plan_type:{
                    required: true
                },
                "retail_discount[]": {
                    checkmax: true
                },
                "reseller_discount[]": {
                    checkmax: true
                },
                "partner_discount[]": {
                    checkmax: true
                },
            },
            messages: {
                product_name: "This field is required.",
                product_sku: "This field is required.",
                product_price:{
                    required: "This field is required.",
                    number: "Please Insert Number"
                },
                "retail_discount[]": {
                    checkmax: "Value should be < 100."
                },
                "reseller_discount[]": {
                    checkmax: "Value should be < 100."
                },
                "partner_discount[]": {
                    checkmax: "Value should be < 100."
                },
                /*retail_base_price:{
                    required: "This field is required.",
                    number: "Please Insert Number"
                },*/
                product_discount_type: "This field is required.",
                category_id: "This field is required.",
                product_is_active: "This field is required."
            },
           invalidHandler: function(event, validator) {
                //display error alert on form submit  
                error("Please input all the mandatory values marked as red");
  
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
        // $("#amount_deci1,#amount_deci2").keypress(function(e){
        //     var keyCode = e.which;
        //     if ( (keyCode != 8 || keyCode ==32 ) && (keyCode < 48 || keyCode > 57)) { 
        //         $("#amount_deci1,#amount_deci2").css("border", "2px solid red");
        //         return false;
        //     }
        // });
        
        $("#amount_deci1,#amount_deci2").change(function() {
            var $this = $(this);
            if($.isNumeric($this.val())){
                $this.val(parseFloat($this.val()).toFixed(2));        
            }else{
               $this.val(parseFloat(0).toFixed(2)); 
            }
        });
        });

</script>