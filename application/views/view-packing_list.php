<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
        </ul>

        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
                        <?php
                        $location_add = false;
                        $location_edit = false;
                        $location_print = false;
                        $location_delete = false;
                        if ($this->user_type == 2) {
                            foreach ($this->user_role as $k => $v) {
                                if ($v['module_id'] == 25) {
                                    if ($v['add'] == 1) {
                                        $location_add = true;
                                    }
                                    if ($v['edit'] == 1) {
                                        $location_edit = true;
                                    }
                                    if ($v['print'] == 1) {
                                        $location_print = true;
                                    }
                                    if ($v['delete'] == 1) {
                                        $location_delete = true;
                                    }
                                }
                            }
                        } else {
                            $location_add = true;
                            $location_edit = true;
                            $location_print = true;
                            $location_delete = true;
                        }
                        ?>
                        <?php if ($location_add) { ?>
                            <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('Packing_list/add'); ?>">Add</a>
                        <?php } ?>

                    </div>
                    <div class="grid-body ">
                        <div class="row">
                            <form class="validate">
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>From Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>To Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <table class="table" id="custom_table">
                            <thead>
                                <tr>
                                    <th width="200px">Sr.No.</th>
                                    <th width="200px">P/L No</th>
                                    <th width="200px">Date</th>
                                    <th width="200px">REF. Delivery Note No</th>
                                    <th width="200px">Customer Name</th>
                                    <th width="200px">Ref Invoice Number</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach (@$data as $k => $v) {
                                ?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <td><?= @$setting["company_prefix"] . @$setting["packing_prfx"] . $v['packing_list_no'] ?></td>
                                            <td>
                                                <?= date("Y-m-d", strtotime($v["packing_list_date"])) ?>
                                            </td>
                                            <td><?= @$setting["company_prefix"] . @$setting["delivery_prfx"]; ?><?php echo $v['delivery_note_no'] ?></td>
                                            <td><?= $v['user_name'] ?></td>
                                            <td><?= $v['ref_invoice_no'] ?></td>
                                            <td>
                                                <a data-id="<?= @$v['packing_list_id']; ?>" data-path="Packing_list/detail/" class="btn-primary btn btn-sm myModalBtn" title="View Detail"><i class="fa fa-eye"></i></a>
                                                <?php
                                                if ($location_edit) {
                                                ?>
                                                    <a href="<?php echo site_url('Packing_list/edit/' . @$v['packing_list_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

                                                <?php } ?>
                                                <?php if ($location_print) { ?>
                                                    <a class="btn btn-sm" href="<?= site_url('Packing_list/print?id=' . @$v['packing_list_id'] . '&header_check=1&view=0&history=0') ?>" target='_blank'><i class="fa fa-print"></i></a>
                                                <?php } ?>

                                                <?php if ($location_delete) { ?>
                                                    <a href="<?= site_url('Packing_list/delete/' . @$v['packing_list_id']) ?>" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm"><i class="fa fa-times"></i></a>
                                                <?php } ?>
                                            </td>

                                        </tr>
                                <?php
                                        $count++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $(document).on("click", "#fetch", function() {

            var from_date = $('#from_date').val();
            if (from_date == '') {
                from_date = 0;
            }

            var to_date = $('#to_date').val();
            if (to_date == '') {
                to_date = 0;
            }

            $.ajax({
                url: "<?php echo site_url('packing_list/search_view'); ?>",
                dataType: "json",
                type: "POST",
                data: {
                    from_date: from_date,
                    to_date: to_date,
                },
                cache: false,
                success: function(result) {
                    custom_table.clear().draw();
                    var count = 0;
                    for (var i = 0; i < result.data.length; i++) {
                        count++;
                        var html = '';
                        html += '<tr>';

                        html += '<td >';
                        html += count;
                        html += '</td>';

                        html += '<td >';
                        html += "<?= @$setting["company_prefix"] . @$setting["packing_prfx"] ?>" + result.data[i].packing_list_no;
                        html += '</td>';

                        html += '<td >';
                        html += result.data[i].packing_list_date;
                        html += '</td>';

                        html += '<td >';
                        html += "<?= @$setting["company_prefix"] . @$setting["delivery_prfx"] ?>" + result.data[i].delivery_note_no;
                        html += '</td>';

                        html += '<td >';
                        html += result.data[i].user_name;
                        html += '</td>';

                        html += '<td>';
                        html += result.data[i].ref_invoice_no;
                        html += '</td>';

                        html += '<td>';


                        html += '<a data-id="'+result.data[i].packing_list_id+'" data-path="Packing_list/detail/" class="btn-primary btn btn-sm myModalBtn" title="View Detail"><i class="fa fa-eye"></i></a>';

                        html += '<?php if ($location_edit) { ?>';
                        html += '<a href="<?php echo site_url("Packing_list/edit/") ?>' + result.data[i].packing_list_id + '" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                        html += '<?php } ?>';

                        html += '<?php if ($location_print) { ?>';
                        html += '<a href="<?php echo site_url("Packing_list/print?id=") ?>' + result.data[i].packing_list_id + '&header_check=1&view=0&history=0" target="_blank" title="View Detail" data-id="' + result.data[i].quotation_id + '" class="btn-primary btn btn-sm"><i class="fa fa-eye"></i></a>';
                        html += '<?php } ?>';

                        html += '<?php if ($location_delete) { ?>';
                        html += '<a href="<?php echo site_url("Packing_list/delete/") ?>' + result.data[i].packing_list_id + '" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm"><i class="fa fa-times"></i></a>';
                        html += '<?php } ?>';

                        html += '</td>';
                        html += '</tr>';
                        custom_table.row.add($(html)).draw(false);
                    }

                }
            });

        });
    });
</script>