<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">Account Transaction</a> </li>
            
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>Account Transaction</h4>
                    </div>
                    <div class="grid-body">
                        <form class="row validate" autocomplete="off" id="dashboard-form" style="margin-bottom:20px">
                            <div class="form-group col-sm-12 col-md-6 col-lg-3 date_input">
                                <label >From Date:</label>
                                <input type="text" name="fromdate" value="<?=@$fromdate?>" id="fromdate" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >To Date:</label>
                                <input type="text" name="todate" id="todate" value="<?=@$todate?>" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >Accounts:</label>
                                <select name="AccountNo" id="AccountNo" class="form-control select2" >
                                    <option value='0'>Select Account</option>
                                    <?php
                                        foreach($accounts as $k => $v){
                                        ?>
                                        <option value="<?= $v['AccountNo']; ?>" <?= @$account_id == $v['AccountNo']?'selected':''?> >
                                        <?= $v['AccountNo'].'-'.$v['AccountDesc'] ?>
                                        </option>
                                        <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                            <button type="submit" class="btn btn-primary" style="margin-top:25px">Quick Preview</button>
                            <a href="" id="print" class="btn btn-success" style="margin-top:25px">Print</a>
                            </div>
                            
                        </form>
                        <table class="table" id="example3" >
                            <thead>
                                <tr>
                                    <th width="200px">Transaction No</th>
                                    <th width="200px">Date</th>
                                    <th width="400px">Detail</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th width="300px">Action</th>
                                </tr>
                            </thead>
                            <tbody id="custom_row">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        
        $( ".datepicker" ).datepicker({
           format: "yyyy-mm-dd",
           autoclose: true
           
        });
    });
    $("form.validate").validate({
      rules: { 
        fromdate:{
          required: true
        }, 
        todate:{
          required: true
        }, 
        
      }, 
      messages: {
        fromdate:"This field is required.",
        todate:"This field is required."
      },
        invalidHandler: function (event, validator) {
        //display error alert on form submit    
          error("Please input all the mandatory values marked as red");
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });
      $(document).ready(function() {
          $('#print').on("click", function(event){
            event.preventDefault();
              var AccountNo = $("#AccountNo").valid();
              var date_from = $("#fromdate").valid();
              var date_to   = $("#todate").valid();
              if(!date_from || !date_to || !AccountNo){
                return false;
              }
              else
              {
                AccountNo = $('#AccountNo option:selected').val();
                from_date = $('#fromdate').val();
                to_date = $('#todate').val();

                window.open('account_transaction_print?from_date='+from_date+'&to_date='+to_date+'&AccountNo='+AccountNo);  
                $("#dashboard-form").submit();
                return false;
              }
          });
          $("#dashboard-form").on("submit", function(event){
              event.preventDefault();
              var base_url = "<?= base_url()?>";
             
              var AccountNo = $("#AccountNo").valid();
              var date_from = $("#fromdate").valid();
              var date_to   = $("#todate").valid();
              if(!date_from || !date_to || !AccountNo){
                return false;
              }
              else
              {
                AccountNo = $("#AccountNo option:selected").val();
                date_from = $("#fromdate").val();
                date_to   = $("#todate").val();
                $("#custom_row").find("tr").remove();
                $.ajax({
                url: "<?php echo site_url('Dashboard/fetch_account_transaction'); ?>",
                dataType: "json",
                type: "POST",
                data: {
                    date_from:date_from,
                    date_to: date_to,
                    AccountNo:AccountNo
                },
                cache: false,
                success: function(result) {
                  $('#example3').DataTable().clear().draw();
                    for (var i = 0; i < result.data.length; i++) {
                        
                            var $rev; 
                            if(result.data[i].invoice_revised_no > 0){
                                $rev = '-R'+result.data[i].voucher_revised_no
                            }
                            else{
                                $rev = '';
                            }
                            
                            
                            var Amount_Dr = '';
                            if(result.data[i].Amount_Dr > 0){
                                Amount_Dr = result.data[i].Amount_Dr
                            }

                            var Amount_Cr = '';
                            if(result.data[i].Amount_Cr > 0){
                              Amount_Cr = result.data[i].Amount_Cr
                            }
                           
                        $('#example3').DataTable().row.add( [
                            result.setting.company_prefix + 'ACC-' + result.data[i].TransactionNo + $rev,
                            result.data[i].Transaction_Date,
                            result.data[i].AccountNo+' - '+result.data[i].AccountDesc+'<br>' +result.data[i].Transaction_Detail,
                            Amount_Dr,
                            Amount_Cr,
                            '<a href="#" class="btn-primary btn btn-sm myModalBtn mr-2" data-path="voucher/history" data-id="'+result.data[i].voucher_id+'" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>' + '<a href="'+base_url+'voucher/print_report?id='+result.data[i].voucher_id+'&header_check=1&view=0&history=0" target="_blank" title="View Detail" data-id="'+result.data[i].voucher_id+'" class="btn-primary btn btn-sm"><i class="fa fa-print"></i></a>'
                        ] ).draw( false )
                    }
                    
                }
                });
              }
          });
      });
</script>