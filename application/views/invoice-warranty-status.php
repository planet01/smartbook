<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">Invoice Warranty Status</a> </li>
            
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>Invoice Warranty Status</h4>
                    </div>
                    <div class="grid-body">
                        <form class="row validate" autocomplete="off" id="dashboard-form" style="margin-bottom:20px">
                        <div class="col-sm-12" style="margin-bottom:25px">
                        <label class="radio-inline">
                        <input type="radio" name="expires" class="radiobuttons" id="inlineRadio1" value="30days" <?= $expires == null || $expires == '30days'?'checked':'' ?>><b> Going to Expire in 30 days</b>
                        </label>
                        <label class="radio-inline">
                        <input type="radio" name="expires" class="radiobuttons" id="inlineRadio2" value="60days" <?= $expires == '60days'?'checked':'' ?>> <b>Going to Expire in 60 days</b>
                        </label>
                        <label class="radio-inline">
                        <input type="radio" name="expires" class="radiobuttons" id="inlineRadio3" value="past30days" <?= $expires == 'past30days'?'checked':'' ?>> <b>Expired in past 30 days</b>
                        </label>
                        <label class="radio-inline">
                        <input type="radio" name="expires" class="radiobuttons" id="inlineRadio4" value="daterange" <?= $expires == 'daterange'?'checked':'' ?>> <b>Check Expire via date range</b>
                        </label>
                        </div>
                        <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >Customer:</label>
                                <select name="customer_id" id="customer_id" class="select2 form-control" >
                                    <option value>Select Customer</option>
                                    <?php
                                        foreach($customerData as $k => $v){
                                        ?>
                                        <option value="<?= $v['user_id']; ?>"  <?= $customer_id ==$v['user_id']?'selected':'' ?>>
                                        <?= $v['user_name']; ?>
                                        </option>
                                        <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3 data-range" style="<?= $expires == 'daterange'?'':'display:none' ?>">
                                <label >From Date:</label>
                                <input type="text" name="fromdate" id="fromdate" value="<?= @$fromdate ?>" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3 data-range" style="<?= $expires == 'daterange'?'':'display:none' ?>">
                                <label >To Date:</label>
                                <input type="text" name="todate" id="todate" value="<?= @$todate ?>"  class="datepicker form-control">
                            </div>
                            
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                            <button type="submit" class="btn btn-primary" style="margin-top:25px">Search</button>
                            </div>
                            
                        </form>
                        <table class="table" id="example3" >
                            <thead>
                                <tr>
                                    <th class="prod_sr_no">Sr.No.</th>
                                    <th class="prod_name">Date</th>
                                    <th>Invoice #</th>
                                    <th>Total Amount</th>
                                    <th>Discount</th>
                                    <th>Tax Amount</th>
                                    <th>Net Amount</th>
                                    <th>Expire Date</th>
                                    <th>Status</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($warranty_status as $key =>$item){
                                $rev = ($item['invoice_revised_no'] > 0)?'-R'.$item['invoice_revised_no'] : '';
                                $rev1 = ($item['invoice_status'] == 2)?'-'.$item['invoice_revised_no'] : '';
                            ?>
                                <tr>
                                    <td><?= ++$key ?></td>
                                    <td><?= @$item['invoice_date'] ?></td>
                                    <td><?php echo @$setting["company_prefix"].@$setting["invoice_prfx"]; ?><?php echo $item['invoice_no'].$rev ?></td>
                                    <td><?= @$item['invoice_total_amount'] ?></td>
                                    <td><?= @$item['discount'] ?></td>
                                    <td><?= @$item['invoice_tax_amount'] ?></td>
                                    <td><?= @$item['invoice_net_amount'] ?></td>
                                    <td>expire date</td>
                                    <td>present</td>
                                    <td>
                                    <a class="btn btn-primary" href="<?= site_url($print.'?id='.@$item['invoice_id'].'&header_check=0')?>" target='_blank' ><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <?php
                              }
                                ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>
<script>
$(document).on('change','.radiobuttons',function(){
    if ($('#inlineRadio4').prop("checked")) {
        $('.data-range').css('display','block');
    }
    else
    {
        $('.data-range').css('display','none');
    }
});
    $(document).ready(function() {
        
        $( ".datepicker" ).datepicker({
           format: "dd-mm-yyyy",
           autoclose: true
           
        });
    });
    $("form.validate").validate({
      rules: {
        fromdate:{
          required: true
        }
      }, 
      messages: {
        fromdate:"This field is required.",
      },
        invalidHandler: function (event, validator) {
        //display error alert on form submit    
          error("Please input all the mandatory values marked as red");
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });
      $(document).ready(function() {
          $("#dashboard-form").on("submit", function(event){
              event.preventDefault();
              if ($('#inlineRadio4').prop("checked")) {
                var date_from = $("#fromdate").valid();
                if(!date_from){
                    return false;
                }
              }
            document.getElementById('dashboard-form').submit();
          });
      });
</script>