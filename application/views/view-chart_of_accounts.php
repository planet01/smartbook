  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Chart Of Acoounts
        </li>
        <li><a href="#" class="active">View All <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span> <span class="fa fa-sort-desc"></spam></h4>
              <?php
                $check_add = false;
                $check_edit = false;
                $check_delete = false;
                $check_print = false;
                if ($this->user_type == 2) {
                    foreach ($this->user_role as $k => $v) {
                    if($v['module_id'] == 1)
                    {
                        if($v['add'] == 1)
                        {
                            $check_add = true;
                        }
                        if($v['edit'] == 1)
                        {
                            $check_edit = true;
                        }
                        if($v['delete'] == 1)
                        {
                            $check_delete = true;
                        }
                        if($v['print'] == 1)
                        {
                            $check_print = true;
                        }
                    }
                    }
                }else
                {
                    $check_add = true;
                    $check_edit = true;
                    $check_delete = true;
                    $check_print = true;
                }
              ?>
              <?php 
              if($check_print)
              {
                ?>
                <a style="    position: absolute; right: 172px;" href="<?= site_url('Chart_Of_Accounts/report')?>" target='_blank' title="Print" data-id="<?= @$v['invoice_id']; ?>" class="btn btn-success btn-cons">Report</a>
                <?php
              }
              if($check_add){?>
              <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url($add_product); ?>">Add</a>
              <?php } ?>
            </div>
            <div class="grid-body ">
              <table class="table" id="example3" >
                <thead>
                  <tr>
                    <th class="">Account #</th>
                    <th class="">Description</th>
                    <th class="">Group</th>
                    <th class="">Level</th>
                    <th class="">Type</th>
                    <th width="200px">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                if (isset($data) && @$data != null) {
                  $count = 1;
                  foreach(@$data as $k => $v) {
                ?>
                <tr class="">
                    <td><?php echo $v['AccountNo']; ?></td>
                    <td><?php echo $v['AccountDesc']; ?></td>
                    <td><?php echo $v['AccountGroupName']; ?></td>
                    <td><?php echo $v['AccountLevelDesc']; ?></td>
                    <td><?php echo $v['AccountType_Desc']; ?></td>
                    <td class="">
                      <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['AccountNo']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                      <?php 
                        if($check_edit)
                        {
                          ?>
                          <a href="<?php echo site_url($edit_product.'/'.@$v['AccountNo'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></a>
                          <?php
                        }
                        if($check_delete){
                      ?>
                        <a href="<?php echo site_url($delete_product.'/'.@$v['AccountNo'])?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a>
                      <?php }
                       ?>
                    </td>
                  </tr>
                  <?php
                    $count++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
    <!-- /.modal -->
  </div>
