<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?>
                    <?= (isset($page_heading)) ? $page_heading : ''; ?>
                </a>
            </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--    <h3><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product) ?>" method="post">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="form-label">Transaction No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <div class="row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <?php
                                                            if($page_title == 'Edit'){
                                                                $rev = (@$data['voucher_revised_no'] > 0)?'-R'.@$data['voucher_revised_no'] : '';
                                                            }
                                                            ?>
                                                            <input style="" readonly type="text" value="<?= ($page_title == 'add') ? '' : @$setval["company_prefix"].'ACC-'.@$data['TransactionNo'].@$rev; ?>" class="form-control my-quot-no" placeholder="">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="form-label">Transaction Date</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <div class="row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <input type="text" class="form-control input-signin-mystyle datepicker2" id="Transaction_Date" name="Transaction_Date" placeholder="" value="<?= date("Y-m-d", strtotime(@$data["Transaction_Date"])); ?>">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="form-label">Remarks</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <div class="row">
                                                        <div class="col-md-12 col-xs-12">
                                                            <input type="text" class="form-control input-signin-mystyle" id="Transaction_Detail_master" name="Transaction_Detail_master" placeholder="" value="<?= @$data['Transaction_Detail']; ?>">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <!-- <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="form-label">Company ID</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <div class="row">
                                                        <div class="col-md-12 col-xs-12"> -->
                                                            <input type="hidden" readonly class="form-control input-signin-mystyle" id="CompanyID" name="CompanyID" placeholder="" value="<?= ($page_title == 'add') ? substr($setval["company_prefix"], 0, 2) : @$data['CompanyID']; ?>">
                                                        <!-- </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->


                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="my-table-controls dataTables_wrapper-1440 DTs_wrapper-quotation" style="width: 100%;">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center" style="width:60px;">Add/Remove Row</th>
                                                                <th class="text-center" style="min-width: 600px;">Acount ID <br> <strong>Transaction Details</strong></th>
                                                                <th class="text-center" style="min-width:400px;">Invoice No <br> <strong>Pay To Name</strong></th>
                                                                <th class="text-center" style="min-width:60px;">Cheque #</th>
                                                                <!-- <th class="text-center" style="min-width:60px;">Pay To Name</th> -->
                                                                <th class="text-center" style="min-width:60px;">Debit</th>
                                                                <th class="text-center" style="min-width:60px;">Tax</th>
                                                                <th class="text-center" style="min-width:60px;">Credit</th>
                                                                <!-- <th class="text-center" style="min-width:150px;">Detail</th> -->
                                                                <!-- <th class="text-center" style="min-width:150px;">Remarks</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tr class="txtMult">
                                                            <td class="text-center"><a href="javascript:void(0);" class="addCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                                            <td colspan="5"></td>
                                                        </tr>
                                                        <tbody id="customFields">
                                                        <?php if (isset($detail_data) && @$detail_data != null) {
                                                                foreach ($detail_data as $k => $d) {
                                                                ?>
                                                                    <tr class="txtMult">
                                                                        <td class="text-center" style="width:60px;">
                                                                            <a href="javascript:void(0);" class="remCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>
                                                                        </td>
                                                                        <td>
                                                                            <select name="AccountNo[]" id="AccountNo'+x+'" class="-control acc_name mb-3" style="width: 100%;" required>
                                                                                <option selected disabled>--- Select Account ---</option>
                                                                                <?php foreach($accounts as $k => $v){ ?>
                                                                                    <option <?= ($d['AccountNo'] == $v['AccountNo']?'Selected':'' ) ?> value="<?= $v['AccountNo']; ?>"> <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?> </option>
                                                                                <?php } ?>
                                                                            </select>
                                                                            <br>
                                                                            <input type='text' class=''  style='width:100%' id='Transaction_Detail"+x+"' name='Transaction_Detail[]' value="<?= $d['Transaction_Detail']?>"  placeholder='' />
                                                                        </td>
                                                                        <td>
                                                                            <select name="invoice_id[]" id="invoice_id'+x+'" class="-control inv_name mb-3" style="width: 100%;" >
                                                                                <option selected value="0" >--- Select Invoice ---</option>
                                                                                <?php foreach($invoices as $k => $v){ ?>
                                                                                    <option <?= ($d['invoice_id'] == $v['invoice_id']?'Selected':'' ) ?> value="<?= $v['invoice_id']; ?>"> <?= @$setval["company_prefix"].@$setval["invoice_prfx"].$v['invoice_no']; ?> </option>
                                                                                <?php } ?>
                                                                            </select>
                                                                            <br>
                                                                            <input type='text' class=''  style='width:100%' id='Pay_To_Name"+x+"' name='Pay_To_Name[]' value="<?= $d['Pay_To_Name']?>"  placeholder='' />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class=""   style="width:100%" id="Cheque_No'+x+'" name="Cheque_No[]" value="<?= $d['Cheque_No']?>" placeholder="" />
                                                                        </td>
                                                                        <!-- <td>
                                                                            <input type='text' class=''  style='width:100%' id='Pay_To_Name"+x+"' name='Pay_To_Name[]' value="<?= $d['Pay_To_Name']?>"  placeholder='' />
                                                                        </td> -->
                                                                        <td>
                                                                            <input type='text' class='txtboxToFilterCustom Amount_Dr'  style='width:100%' id='Amount_Dr"+x+"' value="<?= number_format((float)$d['Amount_Dr'], 2, '.', '')?>" data-optional='0' name='Amount_Dr[]'  placeholder='' />
                                                                        </td>
                                                                        <td>
                                                                        <input type='text' class='txtboxToFilterCustom tax'  style='width:100%' id='tax"+x+"' value="<?= $d['tax']?>"  name='tax[]'  placeholder='' />
                                                                        </td>
                                                                        <td>
                                                                            <input type='text' class='txtboxToFilterCustom Amount_Cr'  style='width:100%' id='Amount_Cr"+x+"' value="<?= number_format((float)$d['Amount_Cr'], 2, '.', '')?>"  name='Amount_Cr[]'  placeholder='' />
                                                                        </td>
                                                                        <!-- <td>
                                                                            <input type='text' class=''  style='width:100%' id='Transaction_Detail"+x+"' name='Transaction_Detail[]' value="<?= $d['Transaction_Detail']?>"  placeholder='' />
                                                                        </td> -->
                                                                        <!-- <td>
                                                                            <input type='text' class=''  style='width:100%' id='Remarks"+x+"' name='Remarks[]' value="<?= $d['Remarks']?>"   placeholder='' />
                                                                        </td> -->
                                                                    </tr>
                                                        <?php 
                                                                }
                                                            } ?>
                                                        </tbody>
                                                        <tr>
                                                        <th colspan="3" class="text-right" ></th>
                                                            <th class="text-right" style="border-right: 1px solid #696969 !important;vertical-align: middle;"><b style="font-size:15px;">Total</b></th>
                                                            <th class="text-center"> 
                                                                <input type="text" readonly="readonly" class="numeric-align" id="grandtotaldb" name="totaldb" style="width:100%" value=""  placeholder="0"  /> 
                                                            </th>
                                                            
                                                            <th class="text-center" style="border-right: 1px solid #696969 !important;">
                                                                <input type="text" readonly="readonly" class="numeric-align" id="grandtotaltax" name="totaltax" style="width:100%" value=""  placeholder="0"  />
                                                            </th>
                                                            
                                                            <th class="text-center" ><input type="text" readonly="readonly" class="numeric-align" id="grandtotalcd" name="totalcd" style="width:100%" value=""  placeholder="0"  /> </th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                    <!-- <a href="<?= site_url("Print_pdf"); ?>" target="_blank" class="btn btn-success btn-cons">Print Quotation</a> -->
                                    <br>
                                    <button class="btn btn-success btn-cons ajaxFormSubmitAlter " type="button"><?= ($page_title == 'add')?'':''; ?> Submit</button> 
                                    <input name="id"  type="hidden" value="<?= @$data['voucher_id']; ?>"> 
                                    <input name="voucher_revised_no"  type="hidden" value="<?= @$data['voucher_revised_no']; ?>"> 
                                    
                                </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>

    $(document).ready(function() {
        $(".acc_name").select2();
        $('.inv_name').select2({
            minimumInputLength: 4
        });
        amount_cr();
        amount_dr();
        tax();
        
        $('.datepicker2').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            // setDate: new Date(),
        });

        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $( '.datepicker2' ).datepicker( 'setDate', today );
        $( '.datepicker2' ).trigger('keyup');

        var page_title = "<?= $page_title; ?>";  
  if(page_title == 'add'){
    $('.datepicker2').datepicker().val('');
  }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $.validator.addMethod(
            "checkCredit",
            function(value, element) {
                var check = true;
                var total_percentage = 0;
                var TCRamount = 0;
                var TDRamount = 0;
                $("#customFields tr.txtMult").each(function() {

                    var row = $(this).closest('tr'); // get the row
                    var CRamount = row.find('.Amount_Cr').val();
                    var DRamount = row.find('.Amount_Dr').val();
                    var tax      = row.find('.tax').val();
                    CRamount     = (CRamount == undefined || CRamount == '') ? 0 : CRamount;
                    DRamount     = (DRamount == undefined || DRamount == '') ? 0 : DRamount;
                    tax          = (tax == undefined || tax == '') ? 0 : tax;
                    TCRamount    = parseFloat(TCRamount) + parseFloat(CRamount);
                    TDRamount    = parseFloat(TDRamount) + parseFloat(DRamount) + parseFloat(tax);
 
                });

                if (TDRamount != TCRamount)   {
                    check = false;
                }
                console.log("Debit: "+TDRamount);
                console.log("Credit: "+TCRamount);
                console.log("Check: "+check);
                return check;
                

            },
            "Required"
        );
        $("form.validate").validate({
            rules: {
                Transaction_Date: {
                    required: true
                },
                "AccountNo[]": {
                    required: true
                },
                // "Amount_Cr[]":
                // {
                //     checkCredit: true
                // },
                totaldb: { checkCredit: true },
                totalcd: { checkCredit: true },
            },
            messages: {
                Transaction_Date: "This field is required.",
                totaldb: 'Credit & Debit should be equal',
                totalcd: 'Credit & Debit should be equal'
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit    
                error("Please input all the mandatory values marked as red");

            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                if(label[0].innerText != '')
                {
                    $('<span class="error"></span>').insertAfter(element).append(label);
                }
                
                
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('error-control').addClass('success-control');

            }
            // submitHandler: function (form) {
            // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        var x = 1;
        $(".addCF").live('click', function(e){
            $(".gridAddBtn").remove();
            e.preventDefault();
            x++;
            var temp = '<tr class="txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="addCF gridAddBtn"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a><a href="javascript:void(0);" class="remCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>';
            temp    += "<td>";
            temp += '<select name="AccountNo[]" id="AccountNo'+x+'" class="-control acc_name mb-3" style="width: 100%;" required>';
            temp += '<option selected disabled>--- Select Account ---</option>';
            temp += "<?php foreach($accounts as $k => $v){ ?><option value='<?= $v['AccountNo']; ?>'> <?= htmlentities($v['AccountNo']).'-'.htmlentities($v['AccountDesc']); ?> </option><?php } ?>";
            temp += '</select> ';
            temp    += "<br><input type='text' class=''  style='width:100%' id='Transaction_Detail"+x+"' name='Transaction_Detail[]'  placeholder='' />";
            temp    += '</td>';
            temp    += "<td>";
            temp += '<select name="invoice_id[]" id="invoice_id'+x+'" class="-control inv_name mb-3" style="width: 100%;" >';
            temp += '<option selected value="0" >--- Select Invoice ---</option>';
            temp += '<?php foreach($invoices as $k => $v){ ?><option value="<?= $v['invoice_id']; ?>"> <?= @$setval["company_prefix"].@$setval["invoice_prfx"].$v['invoice_no']; ?> </option><?php } ?>';
            temp += '</select> ';
            temp    += "<br><input type='text' class=''  style='width:100%' id='Pay_To_Name"+x+"' name='Pay_To_Name[]'  placeholder='' />";
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class=""   style="width:100%" id="Cheque_No'+x+'" name="Cheque_No[]" value="" placeholder="" />';
            temp    += '</td>';
            // temp    += '<td>';
            // temp    += "<input type='text' class=''  style='width:100%' id='Pay_To_Name"+x+"' name='Pay_To_Name[]'  placeholder='' />";
            // temp    += '</td>';
            temp    += '<td>';
            temp    += "<input type='text' class='txtboxToFilterCustom Amount_Dr'  style='width:100%' id='Amount_Dr"+x+"' data-optional='0' name='Amount_Dr[]'  placeholder='' />";
            temp    += '</td>';
            temp    += '<td>';
            temp    += "<input type='text' class='txtboxToFilterCustom tax'  style='width:100%' id='tax"+x+"'  name='tax[]'  placeholder='' />";
            temp    += '</td>';
            temp    += '<td>';
            temp    += "<input type='text' class='txtboxToFilterCustom Amount_Cr'  style='width:100%' id='Amount_Cr"+x+"'  name='Amount_Cr[]'  placeholder='' />";
            temp    += '</td>';
            // temp    += '<td>';
            // temp    += "<input type='text' class=''  style='width:100%' id='Transaction_Detail"+x+"' name='Transaction_Detail[]'  placeholder='' />";
            // temp    += '</td>';
            // temp    += '<td>';
            // temp    += "<input type='text' class=''  style='width:100%' id='Remarks"+x+"' name='Remarks[]'  placeholder='' />";
            // temp    += '</td>';
            temp    += '</tr>';
            $("#customFields").append(temp);
            $("#AccountNo"+x).select2();
            $("#invoice_id"+x).select2({minimumInputLength: 4});
        });    

        $(document).on('click', '.remCF', function() {
            $(this).parent().parent().remove();
        });

        $(document).on('keydown keypress keyup change blur', ".Amount_Cr", function() {
            var tax_percentage = "<?= $tax;?>";
            var row = $(this).closest('tr');
            var value = $(this).val()*(tax_percentage/100);
            // row.find('.tax').val(value.toFixed(2));
            amount_cr();
        });
       

        $(document).on('keydown keypress keyup change blur', ".Amount_Dr", function() {
            
            var tax_percentage = "<?= $tax;?>";
            var row = $(this).closest('tr');
            // row.find('.tax').val($(this).val()*(tax_percentage/100));
            amount_dr();
        });

        $(document).on('keydown keypress keyup change blur', ".tax", function() {
            tax();
        });

        

    });
    function amount_cr()
    {
        
        var total = 0;
        $("#customFields tr.txtMult").each(function() {
                var row = $(this).closest('tr'); // get the row
                var amount = row.find('.Amount_Cr').val();
                amount = (amount == undefined || amount == '') ? 0 : amount;
                total += parseFloat(amount);
     
        });
        tax();
        $('#grandtotalcd').val(total);
    }
    function amount_dr()
    {
        var total = 0;
        $("#customFields tr.txtMult").each(function() {
                var row = $(this).closest('tr'); // get the row
                var amount = row.find('.Amount_Dr').val();
                amount = (amount == undefined || amount == '') ? 0 : amount;
                total += parseFloat(amount);

        });
        tax();
        $('#grandtotaldb').val(total);
    }

    function tax()
    {
        var total = 0;
        $("#customFields tr.txtMult").each(function() {
                var row = $(this).closest('tr'); // get the row
                var amount = row.find('.tax').val();
                amount = (amount == undefined || amount == '') ? 0 : amount;
                total += parseFloat(amount);
        });
        $('#grandtotaltax').val(total);
    }

    
</script>