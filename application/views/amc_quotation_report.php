<style type="text/css">
  .table_align{
        margin-left: 10%;
        margin-right: 10%;
  }
  .table1-td-h4 {
        color: #333333;
  }
  .table1-td-h4-2{
        color: #7f7f7f;
  }
  .table1-td-quotHeading {
        text-align: right;
        font-size: 20px;
        font-weight: bold;
  }
  .product-table1-txt-align {
        position: relative;
        right: 0;
  }
  .product-table2-th-details {
        /*border: 1px solid #adadad; */
        background-color:#3c3d3a;
        color: #fff;
        font-size:18px;
  }
  .product-table2-td-details {
        border:1px solid #adadad;
        font-size:18px;
  }
  .amc-table2-th-details {
        border: 1px solid #3c3d3a /*#adadad*/; 
        background-color:#595959;
        color: #fff;
  }
  .amc-table2-td-details {
        border: 1px solid #3c3d3a /*#adadad*/; 
  }
  .comprehnsve-table-td-left{
    border-left: 1px solid #adadad;
    border-top: 1px solid #adadad;
    border-bottom: 1px solid #adadad;
  }
  .div-controls{
    margin-left: 8%;
    margin-right: 8%;
  }
  .div-font-controls{
    font-family: Calibri;
    font-size: 12px;
  }
  @page toc { sheet-size: A4; }
  span, strong, em, i, p{
    font-family: calibri;
  }
  .end-table-td-details {
    border:1px solid #010203;
  }
  .covr_letter{
    margin-right:8%;
    margin-left:8%;
    font-family:calibri;
  }

  .div-amc-quot-terms_condition .MsoTableGrid tr td {
    padding:2px 10px;
  }
  .pagebreak{
    page-break-after: always;
  }
  
</style>    
    
            <div class="covr_letter">
            <?= $amc_quotation['amc_quotation_cover_letter']; ?>         
            </div>
            <pagebreak/>
            <table style="border-collapse: collapse;" width="100%" class="table_align">
              <tr>
                <td width="435px" height="30px" align="left" style="padding-top: -0.5%;padding-bottom: -0.5%;" >
                  <h4 class="table1-td-h4" style="font-size: 18px;"><?= @$customer_data['user_company_name'] ?> </h4>
                  <?php if($customer_data['tax_reg'] != ''){ ?>
                    <h4 class="table1-td-h4" style="font-size: 18px;">T.R.N # <?= @$customer_data['tax_reg']; ?></h4>
                  <?php } ?>
                </td>

                <td align="right" width="435px" height="30px" >
                  <span class="table1-td-quotHeading">AMC QUOTATION</span><br>
                  <h4 class="product-table1-txt-align" style="text-align: right;"> # <?= @$setting["company_prefix"].@$setting["quotation_prfx"] ?><?= (@$amc_quotation['amc_quotation_revised_no'] > 0)?@$amc_quotation['amc_quotation_no'].'-R'.number_format(@$amc_quotation['amc_quotation_revised_no']):@$amc_quotation['amc_quotation_no']; ?></h4><br>
                </td>
              </tr>
              <tr>
                <td width="500px" align="left" style="padding-top: -0.5%;">
                  <h5 style="font-size: 18px;" class="table1-td-h4-2"><span><?= @$customer_data['user_address'] ?></span>  </h5>
                </td>
                <td width="500px" align="right" style="padding-top: -0.5%;">
                  <?php if($setval['setting_company_reg'] != ''){ ?>
                    <span>T.R.N # <?= @$setval['setting_company_reg']; ?></span>
                  <?php } ?>
                    <br/>
                  <span>Date : <?= str_replace(' ','',@$amc_quotation['amc_quotation_date']); ?></span> 
                </td>
              </tr>
            </table>

            <br/>

            <table style="border-collapse: collapse;" width="100%" class="table_align">
              <thead>

                <tr>

                  <th align="center" class="product-table2-th-details" height="30px" width="40px">#</th>

                  <th align="left" class="product-table2-th-details" height="30px" width="440px" style="padding:0px 10px;">Features</th>

                  <?php
                    foreach(@$warranty_type as $type){
                  ?>
                  
                  <th class="product-table2-th-details product-table2-txt-align" height="30px" width="140px"><?= $type['title']?>
                  </th>
                  
                  <?php 
                    }  
                  ?>

                </tr>

              </thead>
              <tbody>
                <?php
                  $correct_checkbox = base_url().'assets/admin/img/right-checkbox.png';
                  $cross_checkbox = base_url().'assets/admin/img/cross-checkbox.png';
                  if(@$warranty != null && !empty(@$warranty)){
                    $c = 1;
                    foreach(@$warranty as $warranty){
                ?>
                    <tr>
                      <td align="center" class="product-table2-td-details" height="30px"><?= $c ?>.</td>
                      
                      <td align="left" class="product-table2-td-details" height="30px" style="padding:15px 15px;"><?= @$warranty['title'] ?></td>
                      <?php 
                        foreach(@$warranty_type as $type){
                          $value = 2;
                          foreach($warranty_setting as $set){
                              if($warranty['warranty_id'] == $set['warn_id'] && $type['warranty_type_id'] == $set['type_id']){
                                  $value = $set['value'];
                              }
                          }
                          ?>
                      ?>
                          <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 10px;">
                            
                            <img src="<?=( @$value == 1 )? $correct_checkbox : $cross_checkbox ?>"style="width:30px;height: auto;" >
                          </td>
                        <?php } ?>                   
                    </tr>
                <?php
                    $c++;
                    }
                  }
                ?>
    
              </tbody>

            </table>
            
            <br/>
            <!-- <pagebreak/> -->
            <?php
              if($amc_quotation['amc_quotation_report_line_break'] > 0)
              {
                for($i=0;$i<$amc_quotation['amc_quotation_report_line_break'];$i++)
                {
                  ?>
                    <br>
                  <?php
                }
              }
            ?>

                <?php 
                  if(@$amc_warranty_types != null && !empty(@$amc_warranty_types)){
                    $j =1;
                    foreach($amc_warranty_types as $warranty){
                      $discount_amount = 0;
                      $subtotal = @$warranty['amc_type_subtotal'];
                      if($warranty['amc_type_total_discount_type'] == '1'){
                        $discount_amount = ($subtotal/100)*$warranty['amc_type_total_discount_amount']; 
                      }else if($warranty['amc_type_total_discount_type'] == '2'){
                        $discount_amount = $warranty['amc_type_total_discount_amount'];
                      }
                      $discount_amount = customized_round($discount_amount);
                      $subtotal = $subtotal - $discount_amount;
                      $find_vat = 0;
                      if($setval['setting_company_country'] == $customer_data['user_country']){ 
                        $find_vat = customized_round(($subtotal/100)*@$setting['tax']);
                      }
                      //$toal_amount = $subtotal + $find_vat;
                      $toal_amount = @$warranty['amc_type_total_amount'];
                      if($toal_amount > 0){
                ?>

            <table style="border-collapse: collapse;" width="100%" class="table_align ">
              <tr>
                <th colspan="5" align="left" style="padding: 10px 10px;" class="product-table2-th-details" width="500px" height="40px"><h3>Option <?= $j?>: <?= @$warranty['amc_type_title']?></h3>
                </th>
              </tr>
              <tr>
                <td colspan="2"  rowspan="<?= (@$discount_amount > 0)? 2 : 1 ; ?>" width="600px" class="comprehnsve-table-td-left" align="left"  style="padding: 10px 10px;vertical-align: top;">
                  <span style="font-size:18px;">Notes:</span><br/>
                  <span style="font-size:18px;"><?= @$warranty['amc_type_note']?></span>
                </td>
                
                <td colspan="2"  width="100px" align="right" style="font-size:18px;padding: 10px 10px;font-weight: bold;border-top: 1px solid #adadad;">Sub total</td>
                <td colspan="2"  width="100px" class="" align="right" rowspan="1" style="font-size:18px;padding: 10px 10px;border-right: 1px solid #adadad;border-top: 1px solid #adadad;"><?= number_format((float)@$warranty['amc_type_subtotal'], 2, '.', ''); ?>
                </td>
              </tr>
               <?php if($discount_amount > 0){?> 
              <tr>
                <td colspan="2" width="100px"  class="" align="right" style="font-size:18px;padding: 10px 10px;font-weight: bold;border-bottom:1px solid #adadad; ">
                  <?php 
                    if(strlen( str_replace(' ', '',$warranty['amc_type_discount_detail'])) > 0){
                      echo $warranty['amc_type_discount_detail'];
                    }else{
                      echo "Less Special Discount";
                    }
                  ?>    
                </td>
                <td colspan="2" width="100px"  class="" align="right" style="font-size:18px;padding: 10px 10px;border-bottom:1px solid #adadad;border-right: 1px solid #adadad; ">
                  <?= number_format((float)$discount_amount, 2, '.', ''); ?>
                </td>
              </tr>
            <?php } ?>
              <tr>
                <td  colspan="4" width="100px"  class="" align="right" style="font-size:18px;padding: 10px 10px;font-weight: bold;border-top:1px solid #adadad;border-left:1px solid #adadad; ">
                  Net Amount  
                </td>
                <td  colspan="2" width="100px"  class="" align="right" style="font-size:18px;border-top:1px solid #adadad;padding: 10px 10px;border-right: 1px solid #adadad; ">
                  <?= number_format(($warranty['amc_type_subtotal'] - (float)$discount_amount), 2, '.', ''); ?></span>
                </td>
              </tr>
            <?php 
               if($setval['setting_company_country'] == $customer_data['user_country']) { 
            ?>
              <tr>
                <td colspan="4"  class="" align="right" style="font-size:18px;padding: 10px 10px;font-weight: bold;border-bottom: 1px solid #adadad;border-left:1px solid #adadad;"><?= @$setting['tax']?>% VAT
                </td> 
                <td  colspan="5"  class="" align="right" style="font-size:18px;padding: 10px 10px;border-bottom: 1px solid #adadad;border-right:1px solid #adadad; "><?= number_format((float)$find_vat, 2, '.', ''); ?>
                </td>
              </tr>
              <?php
                }
              ?>
              <tr>
                <td colspan="2"  style="padding: 10px 10px;font-weight: bold;border-bottom: 1px solid #adadad;border-left:1px solid #adadad;">
                  <b style="font-size:18px;">
                    <?= numberTowords2(number_format((float) $toal_amount, 2, '.', ''),$currency['base_name'], $currency['Deci_name'])?>
                  </b>
                </td>
                <td colspan="2" width="350px" class=""  align="right" style="font-size:18px;padding: 10px 10px;font-weight: bold;border-bottom: 1px solid #adadad;">Net Amount in <?= @$currency_name ?>  <?= ($setval['setting_company_country'] == $customer_data['user_country'])? "(Incl. VAT)" : ''; ?>
                </td>
                <td colspan="5" class=""  align="right" style="font-size:18px;padding: 10px 10px;border-bottom: 1px solid #adadad;border-right:1px solid #adadad; "><?= number_format((float)$toal_amount, 2, '.', ''); ?>
                </td>
              </tr>
            </table>
            <br/>
                  <?php 
                      $j++;
                      }
                    }
                  }
                  ?>
            
            <?php
              if($amc_quotation['amc_quotation_report_line_break_payment'] > 0)
              {
                for($i=0;$i<$amc_quotation['amc_quotation_report_line_break_payment'];$i++)
                {
                  ?>
                  <span style="color:white">.</span>
                    <br/>
                  <?php
                }
              }
            ?>

            <div class="div-controls div-font-controls">
               <label><b>Validity :</b></label><br>
               <label><?= @$amc_quotation['amc_quotation_validity']; ?> days from the Date of the document</label>
            </div>

            <br/>

            <div class="div-controls div-font-controls">
               <label><b>Payment :</b></label><br>
                <?php
                  if(@$terms != null && !empty(@$terms)){
                    foreach($terms as $row){
                 ?>
                    <label><?= @$row['percentage'] ?>% <?= @$row['payment_title'] ?> </label>
                    <b>
                       (<?=  (isset($row['payment_days']) && $row['payment_days'] > 0) ? 'After '.$row['payment_days'].' Day' : ((isset($row['payment_days']) && $row['payment_days'] == 0) ? 'Immediate' : '') ?>)
                    </b>
                 <?php
                    }
                  }
                 ?>
            </div>

            <br/>
            

            <div class="div-controls div-font-controls">
               <label><b>List of Items Covered Under AMC :</b></label><br>
               <label>Below is the list of items included under AMC and are combined together from all the invoices mentioned in the section 1a in AMC Annexure</label>
            </div>

            <br/>
            <?php
              if(@$amc_quotation['amc_quotation_report_line_break_list_item'] > 0)
              {
                for($i=0;$i<$amc_quotation['amc_quotation_report_line_break_list_item'];$i++)
                {
                  ?>
                  <span style="color:white">.</span>
                    <br/>
                  <?php
                }
              }
            ?>

            <table style="border-collapse: collapse;" width="100%" class="table_align">
              <thead>
                <tr>

                  <th align="center" class="amc-table2-th-details" height="40px" width="50px">S.No.</th>

                  <th align="center" class="amc-table2-th-details" height="40px" width="780px" style="padding:10px 10px;">Items Included In the AMC</th>

                  <th align="center" class="amc-table2-th-details" height="40px" width="50px">Qty</th>

                </tr>
              </thead>
              
              <tbody>
                <?php
                if (isset($amc_quotation_detail_data) && @$amc_quotation_detail_data != null) {
                  $i = 1; 
                  foreach ($amc_quotation_detail_data as $k => $v) {?>
                        <tr>
                          <td align="center" class="product-table2-td-details" height="30px"><?= $i?>.</td>
                          
                          <td align="left" class="product-table2-td-details" height="30px" style="padding:15px 15px;"><?= $v['product_name'];?>
                          </td>
                          
                          <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 10px;">
                            <?= $v['total_quantity'];?>
                          </td>
                        </tr>
                <?php  
                    $i++;
                  } 
                }
                ?>
              </tbody>
            </table>

            <br/>
            <?php
              if(@$amc_quotation['amc_quotation_report_line_break_annexure'] > 0)
              {
                for($i=0;$i<$amc_quotation['amc_quotation_report_line_break_annexure'];$i++)
                {
                  ?>
                  <span style="color:white">.</span>
                    <br/>
                  <?php
                }
              }
            ?>
            <div class="div-controls">
               <p style="text-align: center;font-size: 18px;font-family: Calibri;">AMC ANNEXURE</p>              
            </div>
            <div class="div-controls div-font-controls div-amc-quot-terms_condition">
              <?= $amc_quotation['amc_quotation_terms_conditions']; ?>
            </div>
            <!-- <div class="div-controls div-font-controls">
               <label>From this point onwards, vendor will be used for EaZy-Q team and Client will be mentioned for <<Customer>>. SLA term will be used for service level agreement between both parties</label>
            
            <ol>
              <li><b>Coverage Details</b></li>
              
              <br>
              
              <ol style="list-style-type: lower-alpha;" >
                <li style="padding:10px 0;"><b>Invoices included and their duration of AMC</b>
                  
                  <ol style="list-style-type: upper-roman;">
                    
                    <br>
                    
                    <li>
                      <<.lnvoice No.1.>> will remain valid from (Start) to (End). The AMC may be
                      further extended with a mutual consent.
                    </li>
                    
                    <br>
                    
                    <li>
                      <<.lnvoice No.2.>> will remain valid from (Start) to (End). The AMC may be
                      further extended with a mutual consent.
                    </li>
                  
                  </ol>
                
                </li>
                
                <br>
                
                <li style="padding:10px 0;">
                  <b>No. Of Maximum Visits During AMC contract</b><br>
                  <label>
                  There is no limit to the site visits during the contract period. For more detailsplease refer to the section 4 for the response time [if the value is zero in thquotation input box]
                </label>
                
                <br/><br/>
                
                <label>
                  There are <<Max>> visits allowed during the contract period. For more details, please refer to the section 4 for the response time [if the value is greater than zero in the quotation Max No. of onsite visits input box]
                </label>
            
                </li>
                
                
              
              </ol>
              
              <li style="padding:10px 0;"><b>Procedure of Rendering Services</b><br>
              <label>
                In case of any breakdown or any malfunction in the items mentioned in section 1b, vendor
                must be informed to register the complaint using SLA document provided after order
                confirmation. It is vendor responsibility to resolve the issue with-in the time frame
                mentioned in section 5.<br>
              </label>
              </li>
              
            
            
              <li style="padding:10px 0;"><b>Schedule Preventive Maintenance</b><br>
              <label>
                It is vendor's responsibility to commence preventive <<Duration>> months during the AMC period. Schedule will be mutually decided by both parties. In case of any change from any party, an advance notice must be provided by the vendor or the client (whichever is applicable). The number of visits must be with the limits if defined under the section 1b
              </label>
              </li>
              
                          
              <li style="padding:10px 0;"><b>Response Time</b><br>
              <label>
                Below time lines excludes public holidays and after normal business hours which is between
                11am to 5pm. Please note that onsite support will only be applicable within 4 to 5 days.
              </label>
              <br><br>
              <div class="div-font-controls" style="padding-bottom:20px;border-top-style: solid;border-color: #010203;border-width: 2px;padding-left:5px;border-right-style: solid;border-left-style: solid;">
                <label><b>Criticality Level:High</b><label><br>
                <label>Issues which cause the entire queue operation stops working.</label>
                <br><br>
                <label><b>Response Time:</b></label>
                <ul>
                  <li>Telephonic & Online Support -> Within 30 minutes</li>
                  <li>On-Site Support (if applicable)-> Within 3 to 4 hours</li>
                </ul>
                <label><b>Resolution Time:</b></label>
                <ul>
                  <li>Same Day (if applicable) = Within 5 to 6 hours (Backup and / or parts will be provided
                  as required)</li>
                </ul>
            </div>
            
            <div class="div-font-controls" style="padding-bottom:20px;border-top-style: solid;border-color: #010203;border-width: 2px;padding-left:5px;border-right-style: solid;border-left-style: solid;">
              <label><b>Criticality Level:Medium</b><label><br>
              <label>Issues which cause the entire queue operation stops working.</label>
              <br><br>
              <label><b>Response Time:</b></label>
              <ul>
                <li>Telephonic & Online Support -> Within 30 minutes</li>
                <li>On-Site Support (if applicable)-> Within 3 to 4 hours</li>
              </ul>
              <label><b>Resolution Time:</b></label>
              <ul>
                <li>Same Day (if applicable) = Within 5 to 6 hours (Backup and / or parts will be provided
                as required)</li>
              </ul>              
            </div>
            
            <div class="div-font-controls" style="padding-bottom:20px;border-top-style: solid;border-bottom-style: solid;border-color: #010203;border-width: 2px;padding-left:5px;border-right-style: solid;border-left-style: solid;">
              <label><b>Criticality Level:Low</b><label><br>
              <label>Issues which cause the entire queue operation stops working.</label>
              <br><br>
              <label><b>Response Time:</b></label>
              <ul>
                <li>Telephonic & Online Support -> Within 30 minutes</li>
                <li>On-Site Support (if applicable)-> Within 3 to 4 hours</li>
              </ul>
              <label><b>Resolution Time:</b></label>
              <ul>
                <li>Same Day (if applicable) = Within 5 to 6 hours (Backup and / or parts will be provided
                as required)</li>
              </ul>              
            </div>
              
              </li>
              
            
            
            
            <li style="padding:10px 0;"><b>Coverage Hours / Emergency Visits</b><br>
              <label>
                All visits will be done during vendor's business hours which are between gam to 5pm and
                excludes public / statutory holidays. Please note that onsite support will only be applicable within <<base>>
              </label>
            </li>
            
            <li style="padding:10px 0;"><b>Escalation Matrix</b><br>
              <label>
                Proper SLA will be furnished upon order confirmation. For the basic idea / concept please
                refer to the below details.
                <br/><br/>
                <b>Escalation Level 1 -></b> Project Coordinator<br/>
                <b>Escalation Level 2 -></b> Project Manager<br/>
                <b>Escalation Level 3 -></b> General Manager<br/>
              </label>
            </li>
            
            <li style="padding:10px 0;"><b>System Tempering and Physical Damages</b><br>
              <label>
                Any Hardware Fault due to Identifiable Physical Damage, Misuse or Power Fluctuation will not be covered and will be charged separately
              </label>
            </li>
            
            </ol>    
            </div>
            
            <br/>
            
            <table style="border-collapse: collapse;" width="100%" class="table_align">
              <tbody>
                <tr>
                  <td align="left" class="end-table-td-details" width="350px" height="40px" style="padding:10px 10px;">Vendor : Smart Matrix General Trading LLC</td>
                  
                  <td align="left" class="end-table-td-details" width="550px" height="40px" style="padding:10px 10px;">Client : XYZ Corporation LLC</td>
                  
                  </td>
                </tr>
            
                <tr>
                  <td align="left" class="end-table-td-details" height="80px" style="padding:10px 10px;vertical-align: top;">Signature :</td>
                  
                  <td align="left" class="end-table-td-details" height="80px" style="padding:10px 10px;vertical-align: top;">Signature :</td>
                  
                  </td>
                </tr>
                
                <tr>
                  <td align="left" class="end-table-td-details" height="30px" style="padding:10px 10px;">Name : Name of an employee</td>
                  
                  <td align="left" class="end-table-td-details" height="30px" style="padding:10px 10px;">Name : Name of an employee</td>
                  
                </tr>
                
                <tr>
                  <td align="left" class="end-table-td-details" height="50px" style="vertical-align:top;padding:10px 10px;">Designation : designation of an employee</td>
                  
                  <td align="left" class="end-table-td-details" height="40px" style="vertical-align:top;padding:10px 10px;">Designation : designation of an employee</td>
                  
                </tr>
                
                <tr>
                  <td align="left" class="end-table-td-details" height="30px" style="padding:10px 10px;">Date: Print Date</td>
                  
                  <td align="left" class="end-table-td-details" height="30px" style="padding:10px 10px;">Date:</td>
                  
                </tr>
            
                <tr>
                  <td align="left" class="end-table-td-details" height="130px" style="vertical-align: top;padding:10px 10px;">Official Stamp :</td>
                  
                  <td align="left" class="end-table-td-details" height="130px" style="vertical-align: top;padding:10px 10px;">Official Stamp :</td>
                  
                </tr>
              </tbody>
            </table>
             -->



