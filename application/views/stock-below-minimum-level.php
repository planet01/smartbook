<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">Stock below Minimum Level</a> </li>
            
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>Items below Minimum Stock Level </h4>
                    </div>
                    <div class="grid-body ">
                        <table class="table" id="example3">
                            <thead>
                                <tr>
                                    <th class="prod_sr_no">Sr.No.</th>
                                    <th class="prod_name">Product Name</th>
                                    <th>Min.Stock Level</th>
                                    <th>Total Quantity</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($stockslevel as $key =>$item)
                                    {
                                ?>
                                <tr>
                                        <td><b><?= ++$key ?></b></td>
                                        <td><?= $item['product_name'] ?></td>
                                        <td><?= $item['min_quantity'] ?></td>
                                        <td><?= $item['inventory_quantity'] ?></td>
                                        <td>
                                            <button class="btn btn-primary myModalBtn" data-toggle="tooltip" title="stock bifurcation location wise" data-id="<?= @$item['product_id']; ?>" data-path="<?= $detail_below_level; ?>"><i class="fa fa-eye"></i></button>
                                        </td>
                                </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>