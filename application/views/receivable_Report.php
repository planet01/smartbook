<style type="text/css">
  .table_align{
        margin-left: 3%;
        margin-right: 3%;
  }
  .table1-td-h4 {
        color: #333333;
  }
  .table1-td-h4-2{
        color: #7f7f7f;
  }
  .table1-td-quotHeading {
        /*text-align: right;*/
        font-size: 14px;
        font-weight: bold;
  }
  /*.product-table1-txt-align {
        position: relative;
        right: 0;
  }*/
  .product-table2-th-details {
        /*border: 1px solid #adadad; */
        background-color:#3c3d3a;
        color: #fff;
  }
  .product-table2-td-details {
        border:1px solid #adadad;
  }
  .div-controls{
    margin-left: 3%;
    margin-right: 3%;
  }
  .div-font-controls{
    font-family: Calibri;
    font-size: 14px;
  }
  @page toc { sheet-size: A4; }
  span, strong, em, i, p{
    font-family: calibri;
  }
  .td-border{
    border: 7px double #000;
  }
  .td1-tr1-trn-no{
    text-align: right;
    font-size: 18px;
  }
  .td1-tr1-receipt-no{
    text-align: right;
    font-size: 14px;
  }
  
</style>   

          <table style="border-collapse: collapse;margin-bottom: 5px;" width="100%" class="table_align">
              <tr>
                <td align="left" width="400px" style="padding-top: 2%;">
                  <img src="<?= base_url().'uploads/settings/'.basic_setting()['logo'] ?>" style="
                  width: 350px;height: 100px;margin-top: -5%;margin-left: -2%;" >
                </td>
                <td align="right" width="400px"  style="padding-top: 2%;padding-bottom: -5px;" >
                  <!-- <span class="td1-tr1-trn-no" >TRN <?= @$customer_data['tax_reg']?></span> -->
                  <span class="td1-tr1-trn-no" >TRN <?= @$setval['setting_company_reg']?></span>
                  <h1 class="">Receipt Voucher</h1>
                  <hr style="width:50%;text-align:right;border: 1px solid #000;margin:8px 0 ;">
                  <span class="td1-tr1-receipt-no" style=""><b>Receipt No :</b> <?= @$data['receipt_no']; ?></span>
                  <hr style="width:50%;text-align:right;border: 1px solid #000;margin: 8px 0 ;">
                </td>
              </tr>

              <tr>
                <td align="left" width="400px" style="">
                  <h4 class="table1-td-h4" style="font-size: 14px;"><span>Customer Detail :</span><br><span><?= @$customer_data['user_company_name']?></span></h4>
                </td>

                <td align="right" width="400px">
                  <table>
                    <tr>
                      <th colspan="" class="table1-td-quotHeading" bgcolor="#eaeae9" style="padding: 7px 7px;" align="left" width="300px">
                        Received Amount Details
                      </th>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>
                <td align="left" width="400px" style="padding-bottom: 70px;"  >
                  <h5 style="font-size: 14px;" class="table1-td-h4-2">
                    <span style="font-size: 14px;"><?= @$customer_data['user_address']?></span>
                    <!-- <span style="font-size: 14px;">Diera,Dubai,U.A.E</span>
                    <br>
                    <span style="font-size: 14px;">Dubai,U.A.E.</span> -->
                    <br>
                    <?php if($customer_data['user_country'] == @$setval['setting_company_country']){ ?>
                    <span>TRN <?= @$customer_data['tax_reg']?></span>
                    <?php } ?>
                  </h5>
                </td>
                <td align="right" width="400px" >
                  <table>
                    <tr>
                      <td align="left" width="150px" style="padding: 2px 10px;">
                        <h5 style="font-weight: normal;font-size: 14px;">Date</h5>
                      </td>
                      <td align="right" width="150px" style="padding: 2px 10px;">
                        <h5 style="font-weight: normal;font-size: 14px;"><?= date("M-d-Y", strtotime($data['payment_date'])); ?></h5>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" width="150px" style="padding: 2px 10px;">
                        <h5 style="font-weight: normal;font-size: 14px;">Amount Received</h5>
                      </td>
                      <td align="right" width="150px" style="padding: 2px 10px;">
                        <h5 style="font-weight: normal;font-size: 14px;">AED <?= number_format((float)$data['payment_amount'], 2, '.', ''); ?></h5>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" width="150px" style="padding: 2px 10px;">
                        <h5 style="font-weight: normal;font-size: 14px;">By Cash/By Cheque</h5>
                      </td>
                      <td align="right" width="150px" style="padding: 2px 10px;">
                        <h5 style="font-weight: normal;font-size: 14px;"> <?= @$data['payment_cheque']; ?></h5>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" width="300px"><hr width="100%" style="border: 1px solid #000;"></td>
                    </tr>
                  </table> 
                </td>
              </tr>
          </table>

          <table style="border-collapse: collapse;" width="100%" class="table_align">
              <thead>

                  <tr>
                    <th align="center" class="product-table2-th-details div-font-controls" height="30px" width="23px" style="padding:0 5px;">#</th>

                    <th align="center" class="product-table2-th-details div-font-controls" height="30px" width="662px" style="padding:0px 10px;">Notes</th>

                    <th class="product-table2-th-details product-table2-txt-align div-font-controls" height="30px" width="120px">Amount</th>
                  
                  </tr>

              </thead>
              <tbody>
               
                <tr>
                    <td align="center" class="product-table2-td-details div-font-controls" height="30px" style="padding:10px 0px;vertical-align: top;">1.</td>
                    
                    <td align="left" class="product-table2-td-details div-font-controls" height="30px" style="padding:10px 15px;"><?= $data['payment_notes']?><!-- This amount was receipt in conjunction with multiple pending invoices and are subjected to clearance if paid by cheque --></td>
                  
                    <td align="right" class="product-table2-td-details div-font-controls" height="30px" style="padding:10px 10px;vertical-align: top;">
                      <?= number_format((float)$data['payment_amount'], 2, '.', ''); ?>
                    </td>
                </tr>
          
              </tbody>

            </table>
            
            <br/>

            <div class="div-controls div-font-controls">
               <label>Amount in words:</label><br>
               <label> <?= @$data['amount_in_words']; ?></label>
            </div>

            <br/><br/>

            <table style="border-collapse: collapse;margin-top: 10%;" width="100%" class="table_align">
              <tr>
                <td align="left" class="" height="80px" width="460px" style="vertical-align: top;font-size: 14px;" >
                  <span><b>Thanks,</b></span>
                </td>

                <td align="left" rowspan="2" class="td-border" width="340px" style="padding: 10px;">
                  <div>
                    <label style="font-size: 14px;"><b>Received By</b></label>
                    <br><br>
                    <label style="font-size: 14px;"><b>Name &nbsp;&nbsp; :</b></label>
                    <span style="font-size: 14px;"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                    <br><br>
                    <label style="font-size: 14px;"><b>Sign &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                    <span style="font-size: 14px;"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                    <br><br>
                    <label style="font-size: 14px;"><b>Contact No &nbsp; :</b></label>
                    <span style="font-size: 14px;"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                    <br><br>
                    <label style="font-size: 14px;"><b>Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                    <span style="font-size: 14px;"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                  </div>
              </tr>

              <tr>
                <td align="left" class="" height="80px" width="300px" style="vertical-align: bottom;">
                  <span style="font-size: 14px;"><b><?= @$setval['setting_company_name']?></b></span>
                </td>
              </tr>
            </table>

