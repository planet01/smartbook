<style type="text/css">
  .table_align{
        margin-left: 10%;
        margin-right: 10%;
  }
  .table1-td-h4 {
        color: #333333;
  }
  .table1-td-h4-2{
        color: #7f7f7f;
  }
  .table1-td-quotHeading {
        text-align: right;
        font-size: 20px;
        font-weight: bold;
  }
  .product-table1-txt-align {
        position: relative;
        right: 0;
  }
  .product-table2-th-details {
        /*border: 1px solid #adadad; */
        background-color:#3c3d3a;
        color: #fff;
  }
  .product-table2-td-details {
        border:1px solid #adadad;
  }
  .div-controls{
    margin-left: 8.5%;
    margin-right: 8.5%;
  }
  .div-font-controls{
    font-family: Calibri;
    font-size: 12px;
  }
  @page toc { sheet-size: A4; }
  span, strong, em, i, p{
    font-family: calibri;
  }
  .td-border{
    border: 7px double #000;
  }
  .inp-field{

  }
  
</style>   

          <table style="border-collapse: collapse;margin-bottom: 10px;" width="100%" class="table_align">
              <tr>
                <td align="left" width="400px">
                  <?php 
                    if(!empty($customer_data) ||  !empty($employee_data)){
                      $user_data = (!empty($customer_data))? @$customer_data : @$employee_data;
                      $user_name = $user_data['user_company_name'];
                      $user_address = $user_data['user_address'];
                    }else{
                      $user_name = "";
                      $user_address = "";
                    }
                  ?>
                  <h4 class="table1-td-h4"><span><!--Accounts <?= @$employee_data['department_title'] ?>Department --> <?= @$user_name ?></span><br><span>
                    
                  </span></h4>
                </td>

                <td align="right" width="400px">
                  <span class="table1-td-quotHeading">Material Issue NOTE</span><br>
                  <h4 class="product-table1-txt-align" style="text-align: right;font-weight: bold;"><?= @$setval["company_prefix"].@$setval["material_issue_prefix"] ?><?= @$material_issue_note['material_issue_note_no'] ?></h4><br>
                </td>
              </tr>

              <tr>
                <td align="left" width="400px" >
                  <h5 style="font-size: 14px;" class="table1-td-h4-2"><span><?= @$user_address ?></span><!-- <br><span>Al-Qusais Ind.Area 2,Sharjah,</span><br><span>U.A.E</span> --></h5>
                </td>
                <td align="right" width="400px" style="padding-top: 30px;">
                 
                    <span>Date : <?= @$material_issue_note['material_issue_note_date'] ?></span> 
                </td>
              </tr>
          </table>

          <table style="border-collapse: collapse;" width="100%" class="table_align">
              <thead>

                  <tr>
                    <th align="center" class="product-table2-th-details" height="30px" width="40px">#</th>

                    <th align="left" class="product-table2-th-details" height="30px" width="680px" style="padding:0px 10px;">Item</th>

                    <th class="product-table2-th-details product-table2-txt-align" height="30px" width="80px">Qty</th>
                  
                  </tr>

              </thead>
              <tbody>
                <?php
                  $subtotal = 0;
                  if (isset($material_issue_note_detail) && @$material_issue_note_detail != null) {
                    $s_no = 0;
                    foreach ($material_issue_note_detail as $k => $v) {
                      $s_no++;
                    ?>
                        <tr>
                            <td align="center" class="product-table2-td-details" height="30px"><?= $s_no;?></td>
                            
                            <td align="left" class="product-table2-td-details" height="30px" style="padding:15px 15px;"><?= $v['product_sku']?> - <?= $v['product_name'];?></td>
                          
                            <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 10px;">
                              <?= $v['material_issue_note_detail_quantity'];?>
                            </td>
                        </tr>
                    <?php 
                    }
                  }
                    ?>
                
              </tbody>

            </table>
            
            <br/>

            <div class="div-controls div-font-controls">
              <?php
                if($material_issue_note['material_issue_note_customer_note'] != ''){
              ?>
               <label><b>Notes:</b></label><br>
               <label> <?= $material_issue_note['material_issue_note_customer_note']; ?> </label>
             <?php } ?>
            </div>

            <br/><br/>

            <table style="border-collapse: collapse;margin-top: 10%;" width="100%" class="table_align">
                  <tr>
                    <td align="left" class="" height="80px" width="400px" >
                      <span><b>Thanks</b></span>
                    </td>

                    <td align="left" rowspan="2" class="td-border" width="400px" style="padding: 10px;">
                      <div>
                        <span>Received the above in good condition</span>
                        <br><br>
                        <label><b>Name &nbsp;&nbsp; :</b></label>
                        <span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Sign &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                        <span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Contact No &nbsp; :</b></label>
                        <span class="inp-field"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                        <span class="inp-field"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                      </div>
                  </tr>

                  <tr>
                    <td align="left" class="" height="80px" width="300px">
                      <span><b>For <?=/* @$customer_data['user_company_name']*/ @$setval["setting_company_name"] ?></b></span>
                    </td>
                  </tr>
            </table>




       