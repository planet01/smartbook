  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Inventory
        </li>
        <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
      </ul>
      <?php
      $material_recieve_add = false;
      $material_recieve_edit = false;
      $material_recieve_print = false;
      if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
          if ($v['module_id'] == 5) {
            if ($v['add'] == 1) {
              $material_recieve_add = true;
            }
            if ($v['edit'] == 1) {
              $material_recieve_edit = true;
            }
            if ($v['print'] == 1) {
              $material_recieve_print = true;
            }
          }
        }
      } else {
        $material_recieve_add = true;
        $material_recieve_edit = true;
        $material_recieve_print = true;
      }
      ?>
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title btn-add-custom">
              <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
              <?php if ($material_recieve_add) { ?>
                <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('inventory/add'); ?>">Add</a>
              <?php } ?>
            </div>
            <div class="grid-body view_invntry-responsive">
              <div class="row">
                <form class="validate">
                  <div class="col-md-3 col-lg-3">
                    <div class="row">
                      <div class="col-md-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label for="checkbox1"><b>From Date</b></label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-8 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3 col-lg-3">
                    <div class="row">
                      <div class="col-md-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label for="checkbox1"><b>To Date</b></label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-8 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2">
                    <div class="row">
                      <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                          <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                </form>
              </div>
              <table class="table responsve-table" id="custom_table">
                <thead>
                  <tr>
                    <th>Sr.No.</th>
                    <th>Material Receive #</th>
                    <th>Shipment Date</th>
                    <th>Shipment/Transfer</th>
                    <th>Notes</th>
                    <th class="inventory_view-dt">Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach (@$data as $v) {
                  ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo @$setval["company_prefix"] . @$setval["material_prfx"] . $v['receipt_no'] ?></td>
                        <td><?php echo date("Y-m-d", strtotime($v['shipment_date'])); ?></td>
                        <td><?php echo $v['shipment_type'] ?></td>
                        <td><?php echo $v['note'] ?></td>
                        <td class="">
                          <a href="<?php echo site_url($view_product . '/' . @$v['id']) ?>" class="btn-primary btn btn-sm" data-id="<?= @$v['id']; ?>" data-toggle="tooltip" title="View Products" data-path=""><i class="fa fa-eye"></i></a>
                          <?php if ($material_recieve_edit) { ?>
                            <a href="<?php echo site_url($edit_product . '/' . @$v['id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                          <?php }
                          if ($material_recieve_print) { ?>
                            <a href="<?php echo site_url($print_inventory . '/' . @$v['id']) ?>" target="_blank" class="btn-info btn btn-sm" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></a>
                          <?php } ?>
                          <!--  <a href="<?php echo site_url($delete_product . '/' . @$v['id']) ?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a> -->
                        </td>
                      </tr>
                  <?php
                      $count++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
  </div>
  <script>
    $(document).ready(function() {
      var custom_table = $('#custom_table').DataTable();
      $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
      $(document).on("click", "#fetch", function() {

        var from_date = $('#from_date').val();
        if (from_date == '') {
          from_date = 0;
        }

        var to_date = $('#to_date').val();
        if (to_date == '') {
          to_date = 0;
        }
        $("#custom_row").find("tr").remove();
        $.ajax({
          url: "<?php echo site_url('inventory/search_view'); ?>",
          dataType: "json",
          type: "POST",
          data: {
            from_date: from_date,
            to_date: to_date,
          },
          cache: false,
          success: function(result) {

            custom_table.clear().draw();
            var count = 0;
            for (var i = 0; i < result.data.length; i++) {
              count++;
              var html = '';
              html += '<tr>';

              html += '<td>';
              html += count;
              html += '</td>';

              html += '<td>';
              html += "<?= @$setval["company_prefix"] . @$setval["material_prfx"] ?>" + result.data[i].receipt_no;
              html += '</td>';

              html += '<td>';
              html += result.data[i].shipment_date;
              html += '</td>';

              html += '<td>';
              html += result.data[i].shipment_type;
              html += '</td>';

              html += '<td>';
              html += result.data[i].note;
              html += '</td>';

              html += '<td class="text-center">';
              html += '<a href="<?php echo site_url($view_product . "/") ?>' + result.data[i].id + '" class="btn-primary btn btn-sm mr-1" data-id="' + result.data[i].id + '" data-toggle="tooltip" title="View Products"><i class="fa fa-eye"></i></a>';
              html += '<?php if ($material_recieve_edit) { ?>';
              html += '<a href="<?php echo site_url($edit_product . "/") ?>' + result.data[i].id + '" class="btn-warning btn btn-sm mr-1" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
              html += '<?php } ?>';

              html += '<?php if ($material_recieve_print) { ?>';
              html += '<a href="<?php echo site_url($print_inventory . "/") ?>' + result.data[i].id + '" target="_blank" class="btn-info btn btn-sm mr-1" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></a>';
              html += '<?php } ?>';
              html += '</td>';

              html += '</tr>';
              custom_table.row.add($(html)).draw(false);
            }
            // $('#custom_row').append(html);
          }
        });

      });
    });
  </script>