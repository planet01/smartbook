<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Packing List</title>
    <style>
      body {
        font-weight: 600;
        font-family: sans-serif;
        font-size: 14px;
      }
      .fl-td {
        width: 60%;
        text-align: left;
        font-weight: 700;
        font-family: sans-serif;
        font-size: 14px;
      }
      .text-decoration-ul {
        text-decoration: underline;
      }
      .left-align {
        text-align: left;
      }
      /* .table-2,
      .table-2 tbody tr td,
      .table-2 th {
        border: 1px solid black;
      } */
      .border th,.border td{
        border: 1px solid black;
        
      }
      .table-2 th {
        padding: 5px 10px;
      }
      .table-2 td {
        padding: 5px;
      }
      .table-2 {
        width: 100%;
        border-collapse: collapse;
      }
      .fs-12 {
        font-size: 12px;
      }
      tfoot tr td {
        font-weight: 800;
      }
      .report_footer
      {
        display: none !important;
      }
      @page toc { size: A4; }

      /* @page{
        margin-top: -100mm;

      } */

      
    </style>
  </head>
  <body style="text-align: center">
    <table style="text-align: center; width: 700px; margin: auto;">
      <tr style="">
        <td colspan="3" style="text-align: center; padding-bottom: 40px">
          <h2
            style="
              font-size: 20px;
              text-transform: uppercase;
              text-decoration: underline;
              font-weight: 900;
              font-family: 'Times New Roman', Times, serif;
            "
            ><b>Packing List</b></h2
          >
        </td>
      </tr>
      
      <tr>
        <td class="fl-td" style="width: 500px;">
          <span><b><?= @$data['user_name']?></b></span>
        </td>
        <td class="left-align"><b>Inv. No.</b></td>
        <td class="left-align">
          : <span class="text-decoration-ul"><b><?= @$setval["company_prefix"].@$setval["packing_prfx"].@$data['ref_invoice_no']?></b></span>
        </td>
      </tr>
      <tr>
        <td class="fl-td" style="width: 500px;">
          <span><b><?= @$data['user_address']?></b></span>
        </td>
      </tr>
      <div style="position:absolute;top:220px;right:210px">
        <b>Date</b>
      </div>
      <div style="position:absolute;top:220px;right:60px">
        <b>: <span class="text-decoration-ul"><?= @$data['packing_list_date']?></span></b>
      </div>

      <tr>
        <td class="fl-td" style="width: 500px;" colspan="3">
        <b><span><?= @$data['user_city']?> / <?= @$data['country_name']?></span></b>
        </td>
      </tr>
      <tr>
        <td class="fl-td" style="width: 500px;" colspan="3">
        <b><span><?= @$data['contact_name']?></span></b>
        </td>
      </tr>
      <tr>
        <td class="fl-td" style="width: 500px;" colspan="3">
        <b><span>Tel: <?= @$data['contact_no']?> </span></b>
        </td>
      </tr>
    </table>
    
    <table 
      class="table-2 fs-12"
      style="
        text-align: center;
        width: 700px;
        margin: auto;
        font-weight: 300;
        margin-top: 20px;
      "
    >
      <thead class="border">
        <th width="50px">S.I. NO.</th>
        <th width="50px">(BOX) CARTON QUANTITY</th>
        <th >PARTICULARS</th>
        <th width="50px">ITEM QUANTITY</th>
        <th width="50px">GROSS WEIGHT PER PACKAGE / CARTON (KG)</th>
        <th width="50px">DIMENSION PER PACKAGE (H x W x L) (CM)</th>
        <th width="50px" style="">TOTAL GROSS WEIGHT (KG) - APP.</th>
      </thead>
      <!-- <tfoot class="">
        <th width="50px">S.I. NO.</th>
        <th width="50px">(BOX) CARTON QUANTITY</th>
        <th >PARTICULARS</th>
        <th width="50px">ITEM QUANTITY</th>
        <th width="50px">GROSS WEIGHT PER PACKAGE / CARTON (KG)</th>
        <th width="50px">DIMENSION PER PACKAGE (H x W x L) (CM)</th>
        <th width="50px" style="">TOTAL GROSS WEIGHT (KG) - APP.</th>
      </tfoot> -->
      <tbody>
        <!--First Row-->
        <?php
         $count = 0;
         $total_carton = 0;
         $total_gross = 0;
         foreach ($detail_data as $dt) {
             $count++;
         ?>
            <tr class="border">
            <td style="" rowspan="2"><?= $count; ?></td>
            <td style="" rowspan="2"><?= $dt['carton_qty']?></td>
            <td style="" class="left-align" style="<?= (trim($dt['description']) != '')?'border-bottom:none':''?>">
              <?= $dt['product_sku']; ?> - <?= $dt['product_name']; ?>
            </td>
            <td style="" rowspan="2"><?= $dt['delivery_qty']?> units</td>
            <td style="" rowspan="2"><?= $dt['gross_weight']?></td>
            <td style="" rowspan="2"><?= $dt['dimensions']?></td>
            <td style="" rowspan="2"><?= $dt['total_gross_weight']?></td>
            <?php
              $total_carton += $dt['carton_qty'];
              $total_gross += $dt['total_gross_weight'];
            ?>
          </tr>
        <tr class="<?= ($dt['description'] != '' || $dt['description'] != NULL )?"border":"";?>">
          <?php if(trim($dt['description']) != '' || trim($dt['description']) != NULL){?>
          <td style="" class="left-align"><?= $dt['description']?></td>
          <?php } ?>
        </tr>
        <?php if($dt['line_gap_detail'] > 0)
        {
          for($i=0;$i<$dt['line_gap_detail'];$i++)
          {
            ?>
              <tr style="border:none">
                  <td colspan='7'>
                      &nbsp;
                  </td>
              </tr>
            <?php
          }
        } ?>
        
         <?php } 
         ?>
         <tr class="border">
          <td style="font-weight: bold;">Total</td>
          <td ><?= $total_carton?></td>
          <td colspan="4"></td>
          <td style="font-weight: bold;"><?= $total_gross;?></td> 
         </tr>
      </tbody>
      

      <tfoot>
        <tr>
          <td colspan="7"></td>
        </tr>
      </tfoot>
    </table>
    <?php if($data['line_gap'] > 0)
        {
          for($i=0;$i<$data['line_gap'];$i++)
          {
            ?>
              <br>
            <?php
          }
        } ?>
    <div style="width: 700px; margin: auto; text-align: left">
      <p style="margin-top: 60px"><b>Remarks:</b></p>
    </div>
    <div style="width: 700px; margin: auto; text-align: left">
      <p style="margin-top: 60px"><b>Thanks,</b></p>
    </div>
    <div style="width: 700px; margin: auto; text-align: left">
      <p style="margin-top: 60px"><b><u>For Smart Matrix General Trading L.L.C</u></b></p>
    </div>
  </body>
</html>