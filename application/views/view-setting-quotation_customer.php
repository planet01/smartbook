<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Settings
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':''; ?>
                        <?= (isset($page_heading))?$page_heading:''; ?>
                </a>
            </li>
        </ul>
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-3">
                                                <label class="form-label">Quotation Customer Email Body</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <textarea  name="quotation_customer" id="myeditor" type="textarea" class="editor form-control" placeholder=""><?= @$data[0]['quotation_customer']; ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>                            
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                        <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                    <input name="id" type="hidden" value="<?= @$data[0]['setting_id']; ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
   
    $(document).ready(function() {

        $("form.validate").validate({
            rules: {
                quotation_customer: {
                    required: true
                }
            },
            messages: {
                setting_company_name: "This field is required.",
                setting_company_address: "This field is required.",
                setting_company_postal: "This field is required.",
                setting_company_city: "This field is required.",
                setting_company_country: "This field is required.",
                setting_company_reg: "This field is required.",
                setting_company_bank_ac_no: "This field is required.",
                setting_company_bank_branch_name: "This field is required.",
                quotation_prfx: "This field is required.",
                invoice_prfx: "This field is required.",
                delivery_prfx: "This field is required.",
                material_prfx: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit 
                error("Please input all the mandatory values marked as red");
   
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
   

</script>
