<style type="text/css">
  .table_align{
        margin-left: 10%;
        margin-right: 10%;
  }
  .table1-td-h4 {
        color: #333333;
  }
  .table1-td-h4-2{
        color: #7f7f7f;
  }
  .table1-td-quotHeading {
        text-align: right;
        font-size: 24px;
        /*font-weight: bold;*/
  }
  .product-table1-txt-align {
        position: relative;
        right: 0;
  }
  .product-table2-th-details {
        /*border: 1px solid #adadad; */
        background-color:#3c3d3a;
        color: #fff;
  }
  .product-table2-td-details {
        border:1px solid #adadad;
  }
  .div-controls{
    margin-left: 8.5%;
    margin-right: 8.5%;
  }
  .div-font-controls{
    font-family: Calibri;
    font-size: 12px;
  }
  @page toc { sheet-size: A4; }
  span, strong, em, i, p{
    font-family: calibri;
  }
  .td-border{
    border: 7px double #000;
  }
  .inp-field{

  }
  
</style>
<table style="border-collapse: collapse;margin-bottom: 10px;" width="100%" class="table_align">
  <tr>
    <td align="left" width="400px">
      <!-- <h4 class="table1-td-h4">
        <span>Warehouse</span>
        <br><span><?= @$data[0]['warehouse_name']?></span>
      </h4> -->
    </td>

    <td align="right" width="400px" >
      <span class="table1-td-quotHeading">M.R.N</span><br>
      <h4 class="product-table1-txt-align" style="text-align: right;font-weight: bold;">#<?= @$setval['company_prefix']?><?= @$setval['material_prfx']?><?= @$data[0]['receipt_no']?></h4>
      <br>
      <?php if($data[0]['shipment_type'] == 'transfer'){ ?>
        <h5 style="font-size: 14px;" class="">From Location: <span style="font-weight: normal;"><?= @$data[0]['w_from']?></span></h5>
      <?php } ?>
      <span>Date : <?= $data[0]['shipment_date']?></span> 
    </td>
  </tr>

</table>

          <table style="border-collapse: collapse;" width="100%" class="table_align">
              <thead>

                  <tr>
                    <th align="center" class="product-table2-th-details" height="30px" style="vertical-align:bottom;padding:20px 10px 3px 10px;" width="40px">#</th>

                    <th align="left" class="product-table2-th-details" height="30px" width="520px" style="padding:20px 10px 3px 10px;vertical-align:bottom;">Item</th>

                    <th align="left" class="product-table2-th-details" height="30px" width="150px" style="padding:20px 10px 3px 10px;vertical-align:bottom;">Location</th>

                    <th class="product-table2-th-details product-table2-txt-align" style="vertical-align:bottom;padding:20px 10px 3px 10px;" height="30px" width="80px">Qty</th>
                  
                  </tr>

              </thead>
              <tbody>
                <?php
                  $subtotal = 0;
                    $s_no = 0;
                    foreach($data as $dt) {
                      $s_no++;
                    ?>
                        <tr>
                            <td align="center" class="product-table2-td-details" height="30px"><?= $s_no;?></td>
                            
                            <td align="left" class="product-table2-td-details" height="30px" style="padding:15px 15px;"><?= @$dt['product_sku']?> - <?= @$dt['product_name'] ?></td>

                            <td align="left" class="product-table2-td-details" height="30px" style="padding:15px 15px;"><?= @$dt['w_to']?></td>

                            <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 10px;">
                              <?= @$dt['inventory_quantity']?>
                            </td>
                        </tr>
                    <?php 
                    }
                    ?>
                
              </tbody>

            </table>
            
            <br/>

            <div class="div-controls div-font-controls">
              <?php
                if($data[0]['note'] != ''){
              ?>
               <label><b>Notes:</b></label><br>
               <label> <?= @$data[0]['note'] ?></label>
             <?php } ?>
            </div>

            <br/><br/>

            <table style="border-collapse: collapse;margin-top: 10%;" width="100%" class="table_align">
                  <tr>
                    <td align="left" class="" height="80px" width="400px" >
                      <span><b>Thanks</b></span>
                    </td>

                    <td align="left" rowspan="2" class="td-border" width="400px" style="padding: 10px;">
                      <div>
                        <span>Received the above in good condition</span>
                        <br><br>
                        <label><b>Name &nbsp;&nbsp; :</b></label>
                        <span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Sign &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                        <span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Contact No &nbsp; :</b></label>
                        <span class="inp-field"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                        <span class="inp-field"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                      </div>
                  </tr>

                  <tr>
                    <td align="left" class="" height="80px" width="300px">
                      <span><b>For <?= @$setval['setting_company_name']?></b></span>
                    </td>
                  </tr>
            </table>

