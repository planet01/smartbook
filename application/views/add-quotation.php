<div class="page-content">
  <div class="content">
    <ul class="breadcrumb pad-btn">
      <li>
        <p>Dashboard</p>
      </li>
      <li>
        Quotation
      </li>
      <li>
        <a href="#" class="active">
          <?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?>
          <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
        </a>
      </li>

      <!-- <a href="#" class="pull-right btn btn-success btn-cons myModalBtnCustomer" >Add Customer </a> -->
      <!-- <a style="float: right;" href="#" class="btn btn-success btn-cons myModalBtnCustomer quot-addCustBtn-mg " >Add Customer </a> -->

    </ul>
    <!--<div class="page-title"> <i class="icon-custom-left"></i>-->
    <!--  <h3><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
    <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->

    <div class="col-md-12">
      <div class="grid simple">
        <div class="grid-title no-border">
          <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
        </div>
        <div class="grid-body no-border">
          <form class="ajaxForm validate" id="form" action="<?php echo site_url($save_product) ?>" method="post" enctype="multipart/form-data">
            <div id="general_fields">
              <div class="row">
                <div class="col-md-3 col-lg-2">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Quotation No</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <div class="row">
                            <div class="col-md-12 col-xs-12">

                              <input style="" readonly type="text" value="<?= ($page_title == 'add') ? '' : @$setval["company_prefix"] . @$setval["quotation_prfx"] . @$data['quotation_no']; ?>" class="form-control my-quot-no" placeholder="">


                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <input name="quotation_revised_no" type="hidden" value="<?= number_format(@$data['quotation_revised_no'] + 1); ?>">
                <input type="hidden" name="quotation_no" value="<?= ($page_title == 'add') ? 1 : @$data['quotation_no']; ?>">
                <input type="hidden" name="price_plan_id">
                <div class="col-md-2 col-lg-2">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Date</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <input type="text" class="form-control input-signin-mystyle datepicker2" id="quotation_date" name="quotation_date" placeholder="" value="<?= date("Y-m-d", strtotime(@$data["quotation_date"])); ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 col-lg-2">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Expiry Date</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <input type="text" class="form-control input-signin-mystyle datepicker" id="quotation_expiry_date" name="quotation_expiry_date" placeholder="" value="<?= date("Y-m-d", strtotime(@$data["quotation_expiry_date"])); ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Customer</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <select name="customer_id" id="customer_id" class="select2 form-control">
                            <option value="0" selected disabled>--- Select Customer ---</option>
                            <?php

                            foreach ($customerData as $k => $v) {
                            ?>
                              <option value="<?= $v['user_id']; ?>" data-city="<?= $v['user_city'] ?>" data-country="<?= $v['country_name'] ?>" data-address="<?= $v['user_address'] ?>" data-name="<?= $v['user_name']; ?>" data-user-country='<?= $v['user_country']; ?>' data-validity-days='<?= $v['validity_days']; ?>' data-support-days='<?= $v['support_days']; ?>' data-warranty-years='<?= $v['warranty_years']; ?>' data-price-plan-id='<?= $v['plan_type']; ?>' data-customer-type='<?= $v['customer_type']; ?>' data-customer-type-title='<?= $v['title']; ?>' <?= ($v['user_id'] == @$data['customer_id']) ? 'selected' : ''; ?>><?= $v['user_name']; ?></option>
                            <?php
                            }
                            ?>
                          </select>
                          <input type="hidden" name="price_plan_id">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-lg-2">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Currency</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <div class="radio radio-success add-quotation-responsve-radio">
                            <?php
                            $count = 3;
                            $required_check = 0;
                            foreach ($currencyData as $k => $v) {

                            ?>
                              <input type="radio" class="quotation_currency" id="checkbox<?= $count; ?>" data-title="<?= $v['title'] ?>" name="quotation_currency" value="<?= $v['id']; ?>" <?= (@$data['quotation_currency'] == $v['id']) ? 'checked="checked"' : (($v['id'] == @$base_currency_id) ? 'checked="checked"' : ''); ?> <?= ($required_check == 0) ? 'required' : ''; ?>>
                              <label for="checkbox<?= $count; ?>"><b><?= $v['title'] ?></b></label>
                            <?php
                              $required_check++;
                              $count++;
                            }
                            ?>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="row">
                <div class="col-md-2 col-lg-2">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Validity (No. of Days)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <input type="text" class='txtboxToFilter quotation_validity' name="quotation_validity" value="<?= @$quotation_validity; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-3 col-lg-2">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Support (No. of Days)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <input type="text" class='txtboxToFilter quotation_support' name="quotation_support" value="<?= @$quotation_support; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-lg-2">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Warranty (No. of Days)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <input type="text" class='txtboxToFilter quotation_warranty' name="quotation_warranty" value="<?= @$quotation_warranty; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <label class="form-label">Employee</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <select name="employee_id" id="employee_id" class="select2 form-control">
                            <option value="0" selected disabled>--- Select Employee ---</option>
                            <?php
                            foreach ($employeeData as $k => $v) {
                            ?>
                              <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['employee_id']) ? 'selected' : ''; ?>><?= $v['user_name']; ?></option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-lg-2">
                  <?php
                  if ($page_title != 'add' && $data['quotation_freight_type'] == NULL) {
                  ?>
                    <div class="freight_type" style='display:none;'>
                    <?php
                  } else {
                    ?>
                      <div class="freight_type">
                      <?php
                    }
                      ?>
                      <div class="row">
                        <div class="col-md-12">
                          <label class="form-label">Freight Type</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="input-with-icon right controls">
                              <i class=""></i>

                              <div class="radio radio-success add-quotation-responsve-radio">

                                <input id="checkbox1" type="radio" name="quotation_freight_type" value="0" <?= (@$data['quotation_freight_type'] == 0) ? 'checked="checked"' : ''; ?>>
                                <label for="checkbox1"><b>C.I.F.</b></label>

                                <input id="checkbox2" type="radio" name="quotation_freight_type" value="1" <?= (@$data['quotation_freight_type'] == 1) ? 'checked="checked"' : ''; ?>>
                                <label for="checkbox2"><b>F.O.B.</b></label>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-3 col-lg-2">
                      <div class="row">
                        <div class="col-md-12">
                          <label class="form-label">Line Break (after item table)</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="input-with-icon right controls">
                              <i class=""></i>
                              <input type="text" class='txtboxToFilter' name="quotation_report_line_break" value="<?= @$data['quotation_report_line_break']; ?>">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3 col-lg-2">
                      <div class="row">
                        <div class="col-md-12">
                          <label class="form-label">Line Break (after payment)</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="input-with-icon right controls">
                              <i class=""></i>
                              <input type="text" class='txtboxToFilter' name="quotation_report_line_break_payment" value="<?= @$data['quotation_report_line_break_payment']; ?>">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


                </div>




                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="my-table-controls dataTables_wrapper-1440 DTs_wrapper-quotation" style="width: 100%;">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th class="text-center" style="width:60px;">Add/Remove Row</th>
                                  <th class="text-center" style="width:600px;" style="min-width: 340px;">Product</th>

                                  <th class="text-center" style="min-width:200px;">Discount</th>
                                  <!-- <th class="text-center" width="60px">Discount Amount</th> -->
                                  <th class="text-center" style="min-width:60px;">Quantity</th>
                                  <th class="text-center" style="min-width:60px;">Rate</th>
                                  <th class="text-center" style="min-width:150px;">Amount</th>
                                </tr>
                              </thead>
                              <tr class="txtMult">
                                <td class="text-center"><a href="javascript:void(0);" class="addCF qt_list"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                <td colspan="6"></td>
                              </tr>
                              <tbody id="customFields">


                                <?php
                                $subtotal = 0;
                                $x = 0;
                                // print_b($quotation_detail_data);
                                if (isset($quotation_detail_data) && @$quotation_detail_data != null) {
                                  foreach ($quotation_detail_data as $k => $v) {
                                    $x++;
                                ?>
                                    <tr class="txtMult">
                                      <td class="text-center" style="width: 60px"><a href="<?= site_url("quotation/quotationDetailDelete/" . $v['quotation_detail_id']); ?>" class="ajaxbtn remCF" rel="delete"> <span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>
                                      <td class="label-font">
                                        <select name="product_id[]" id="pproduct_id<?= $k; ?>" class="form-control prod_name mb-3 select2" style="width: 600px;" required>
                                          <option selected disabled>--- Select Product ---</option>
                                          <?php
                                          $find_can_deliver = 0;
                                          foreach ($productData as $k => $v2) {
                                            if ($v2['product_id'] == @$v['product_id']) {
                                              $find_can_deliver = $v2['can_deliver'];
                                            }
                                          ?>

                                            <option value="<?= $v2['product_id']; ?>" <?= ($v2['product_id'] == @$v['product_id']) ? 'selected' : ''; ?>>
                                              <?= $v2['product_sku']; ?> - <?= $v2['product_name']; ?>
                                            </option>
                                          <?php } ?>
                                        </select>

                                        <div class="checkbox checkbox-inline check-success">
                                          <input type="checkbox" onchange="check_hendler('quotation_detail_optional_old<?= $x; ?>')" value="1" <?= (@$v['quotation_detail_optional'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_optional" id="quotation_detail_optional_old<?= $x; ?>" placeholder="" />
                                          <label style="padding-left:25px" for="quotation_detail_optional_old<?= $x; ?>">Optional</label>
                                          <input type="hidden" id="quotation_detail_optional_old<?= $x; ?>_hidden" class="quotation_detail_optional_old_hidden" name="quotation_detail_optional[]" value="<?= (@$v['quotation_detail_optional'] == 1) ? 1 : 0 ?>" placeholder="" />
                                        </div>
                                        <!-- <label style="display: inline;" class="control control-checkbox" for="quotation_detail_optional<?= $k; ?>"><b>Optional</b>
                                                  <input style="display: inline;" type="checkbox" value="1" <?= (@$v['quotation_detail_optional'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_optional"  id="quotation_detail_optional<?= $k; ?>" name="quotation_detail_optional[]"  placeholder="" />  
                                                  <div class="control_indicator control_indicator1 "></div>  
                                                  </label>  -->

                                        <div class="checkbox checkbox-inline check-success  ">
                                          <input type="checkbox" value="1" <?= (@$v['quotation_detail_existing'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_existing" id="quotation_detail_existing_old<?= $x; ?>" placeholder="" onchange="check_hendler('quotation_detail_existing_old<?= $x; ?>')" />
                                          <label style="padding-left:25px" for="quotation_detail_existing_old<?= $x; ?>">Existing will be used</label>
                                          <input type="hidden" value="<?= (@$v['quotation_detail_existing'] == 1) ? 1 : 0 ?>" id="quotation_detail_existing_old<?= $x; ?>_hidden" name="quotation_detail_existing[]" />

                                        </div>

                                        <!-- <label style="display: inline;" class="control control-checkbox" for="quotation_detail_existing<?= $k; ?>"><b>Existing will be used</b>
                                                  <input type="checkbox" value="1" <?= (@$v['quotation_detail_existing'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_existing"  id="quotation_detail_existing<?= $k; ?>" name="quotation_detail_existing[]"  placeholder="" />
                                                  <div class="control_indicator control_indicator1"></div>  
                                                  </label>  -->

                                        <div class="checkbox checkbox-inline check-success  ">
                                          <input type="checkbox" value="1" <?= (@$v['quotation_detail_provided'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_provided" id="quotation_detail_provided_old<?= $x; ?>" placeholder="" onchange="check_hendler('quotation_detail_provided_old<?= $x; ?>')" />
                                          <label style="padding-left:25px" for="quotation_detail_provided_old<?= $x; ?>">To be provided by client</label>
                                          <input type="hidden" value="<?= (@$v['quotation_detail_provided'] == 1) ? 1 : 0 ?>" id="quotation_detail_provided_old<?= $x; ?>_hidden" name="quotation_detail_provided[]" placeholder="" />
                                        </div>

                                        <!--  <label style="display: inline;" class="control control-checkbox" for="quotation_detail_provided<?= $k; ?>"><b>To be provided by client</b>
                                                  <input type="checkbox" value="1" <?= (@$v['quotation_detail_provided'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_provided"  id="quotation_detail_provided<?= $k; ?>" name="quotation_detail_provided[]"  placeholder="" />
                                                  <div class="control_indicator control_indicator1"></div>  
                                                  </label> -->
                                        <!-- <br> -->

                                        <!-- <input type="checkbox" value="1" <?= (@$v['quotation_detail_required'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_required"  id="quotation_detail_required<?= $k; ?>" name="quotation_detail_required[]"  placeholder="" />-->
                                        <div class="checkbox checkbox-inline check-success  ">
                                          <input type="checkbox" value="1" <?= (@$v['quotation_detail_required'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_required" id="quotation_detail_required_old<?= $x; ?>" placeholder="" onchange="check_hendler('quotation_detail_required_old<?= $x; ?>')" />
                                          <label style="padding-left:25px" for="quotation_detail_required_old<?= $x; ?>">Not Required</label>
                                          <input type="hidden" value="<?= (@$v['quotation_detail_required'] == 1) ? 1 : 0 ?>" id="quotation_detail_required_old<?= $x; ?>_hidden" name="quotation_detail_required[]" />
                                        </div>
                                        <br>
                                        <textarea name="quotation_desc[]" style="display: none;width: 70%;" id="quotation_desc<?= $x; ?>" class="form-control prod_desc_text" placeholder="" readonly="readonly"><?= @$v['quotation_detail_description']; ?></textarea>
                                        <a href="#" data-textarea_id="quotation_desc<?= $x; ?>" class="btn-warning btn btn-sm myModalBtnDesc my-txtarea-control" data-toggle="tooltip" title="Edit">Edit Description
                                          <!-- <i class="fa fa-pencil"></i> -->
                                        </a>


                                        <!-- <label style="display: inline;" class="control control-checkbox" for="quotation_detail_required<?= $k; ?>"><b>Not Required</b>
                                                  <input type="checkbox" value="1" <?= (@$v['quotation_detail_required'] == 1) ? 'checked="checked"' : '' ?> class="quotation_detail_required"  id="quotation_detail_required<?= $k; ?>" name="quotation_detail_required[]"  placeholder="" />
                                                  <div class="control_indicator control_indicator1"></div>  
                                                  </label>  -->

                                      </td>
                                      <td>
                                        <select name="quotation_detail_total_discount_type[]" style="width: 100%" class="form-control discount_type">
                                          <option value="0" <?= ($v['quotation_detail_total_discount_type'] == 0) ? 'selected' : ''; ?>>-- select discount type --</option>
                                          <option value="1" <?= ($v['quotation_detail_total_discount_type'] == 1) ? 'selected' : ''; ?>>Percentage (%)</option>
                                          <option value="2" <?= ($v['quotation_detail_total_discount_type'] == 2) ? 'selected' : ''; ?>>Amount (Number)</option>
                                        </select>
                                        <br>
                                        <input type="text" class="numeric-align code discount_amount txtboxToFilter" style="width: 100%" id="ddiscount_amount<?= $x; ?>" name="quotation_detail_total_discount_amount[]" value="<?= $v['quotation_detail_total_discount_amount']; ?>" max="<?= @$v['quotation_detail_max_discount']; ?>" placeholder="" />

                                        <input type="hidden" name="quotation_detail_max_discount[]" value="<?= @$v['quotation_detail_max_discount']; ?>" class="item_discount_max" />
                                        <br>
                                        <input type="text" class="quotation_detail_note" name="quotation_detail_note[]" style="width: 100%" id="quotation_detail_note<?= $k; ?>" value="<?= @$v['quotation_detail_note']; ?>" placeholder="Notes" />
                                      </td>
                                      <?php

                                      $total_inventory_quantity = inventory_quantity_by_product($v['product_id']);
                                      // getCustomRow("SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1");
                                      ?>
                                      <td align="center">
                                        <?php
                                        if (isset($v['quotation_detail_quantity']) && ($v['quotation_detail_quantity'] == 'lot' || $v['quotation_detail_quantity'] == 'Lot')) { ?>
                                          <input type="text" class="numeric-align code quantity txtboxToFilter quatity_num margin-control" style="width: 60px" data-optional="0" id="quantity" name="quotation_detail_quantity[]" value="<?= @$v['quotation_detail_quantity']; ?>" placeholder="" />
                                        <?php } else { ?>
                                          <!-- max="<?= $total_inventory_quantity['total_inventory_quantity'] + @$v['material_issue_note_detail_quantity']; ?>" -->
                                          <input type="text" class="code quantity txtboxToFilter quatity_num margin-control" style="width: 60px" data-optional="0" id="quantity" name="quotation_detail_quantity[]" value="<?= @$v['quotation_detail_quantity']; ?>" placeholder="" />
                                        <?php
                                        }
                                        ?>
                                      </td>
                                      <td align="center">
                                        <!-- readonly="readonly" -->
                                        <input type="text" style="width: 60px" <?= ($find_can_deliver == 1) ? '' : '' ?> class="margin-control code pro_amount rate_num numeric-align" id="pro_amount" value="<?= @$v['quotation_detail_rate']; ?>" name="quotation_detail_rate[]" placeholder="" />
                                        <input type="hidden" class="quotation_detail_id" value="<?= @$v['quotation_detail_id']; ?>" />
                                        <input type="hidden" class="base_price" name="base_price[]" value="<?= @$v['quotation_detail_rate']; ?>" />
                                        <input type="hidden" class="usd_price" name="usd_price[]" value="<?= @$v['quotation_detail_rate_usd']; ?>" />
                                      </td>

                                      <td>
                                        <?php

                                        $quotation_detail_total_discount_amount_val = 0;
                                        if ($v['quotation_detail_total_discount_type'] == 1) {
                                          if ($v['quotation_detail_total_discount_amount'] != '') {
                                            if ($v['quotation_detail_total_discount_amount'] <= 100) {
                                              $tot    = (int) is_numeric(@$v['quotation_detail_quantity']) ? @$v['quotation_detail_quantity'] : 1;
                                              $quotation_detail_total_discount_amount_val = ($v['quotation_detail_total_discount_amount'] / 100) * $tot * $v['quotation_detail_rate'];
                                            } else {

                                              $quotation_detail_total_discount_amount_val = $tot * $v['quotation_detail_rate'];
                                              // return false;
                                            }
                                          }
                                        } else if ($v['quotation_detail_total_discount_type'] == 2) {
                                          if ($v['quotation_detail_total_discount_amount'] != '') {
                                            $quotation_detail_total_discount_amount_val = $v['quotation_detail_total_discount_amount'];
                                          }
                                        } else {
                                          $quotation_detail_total_discount_amount_val = 0;
                                        }


                                        // $quotation_detail_total_discount_amount_val = $subtotal - $quotation_detail_total_discount_amount_val;


                                        $amount = 0;
                                        $tot    = (int) is_numeric(@$v['quotation_detail_quantity']) ? @$v['quotation_detail_quantity'] : 1;
                                        $amount = (@$tot * $v['quotation_detail_rate']) - $quotation_detail_total_discount_amount_val;
                                        $subtotal += $amount;
                                        ?>
                                        <input type="text" style="width: 100%" readonly="readonly" class="margin-control code amount  multTotal numeric-align " id="amount" name="amount[]" value="<?= $amount; ?>" placeholder="" />
                                      </td>
                                    </tr>
                                    <!-- <tr>
                                            <td></td>
                                            <td colspan="2">
                                              <textarea name="quotation_desc[]" id="quotation_desc<?= $k; ?>" class="form-control prod_desc_text" placeholder="" readonly="readonly"><?= @$v['quotation_detail_description']; ?></textarea>
                                              <a href="#" data-textarea_id="quotation_desc'+x+'" class="btn-warning btn btn-sm myModalBtnDesc" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-pencil"></i>
                                              </a>
                                            </td>
                                            <td colspan="5"></td>
                                          </tr> -->
                                <?php
                                  }
                                }
                                ?>

                              </tbody>
                              <!-- <tr rowspan="6">
                                          
                                        </tr> -->
                            </table>

                            <table class="table table-bordered">
                              <tr>
                                <th colspan="2" rowspan="5">
                                  <label><b>Quotation Details:</b></label>
                                  <textarea name='quotation_detail' class="quot_detail_control" rows="4" cols="50"><?= @$data['quotation_detail'] ?></textarea>
                                </th>
                                <th colspan="3" class="text-right quot-vc">Sub Total</th>
                                <th class="text-center">
                                  <input type="text" readonly="readonly" class="numeric-align" id="subtotal" name="subtotal" value="<?= @$subtotal; ?>" style="width:100%;" placeholder="0" />
                                </th>
                                <!--  <th class="text-center"><span id="subtotal">0</span></th> -->
                              </tr>

                              <tr>
                                <!-- <th colspan="1" class="text-right quot-vc">Discount Type</th> -->
                                <th class="text-center success-control" colspan="2">
                                  <span style="padding-right: 7px;display: inline;">Discount Type</span>
                                  <select name="quotation_total_discount_type" style="display: inline;width: 60%;" class="form-control total_discount_type" id="total_discount_type" aria-invalid="false">
                                    <option value="0" <?= (@$data['quotation_total_discount_type'] == 0) ? 'selected' : ''; ?>>-- select discount type --</option>
                                    <option value="1" <?= (@$data['quotation_total_discount_type'] == 1) ? 'selected' : ''; ?>>Percentage (%)</option>
                                    <option value="2" <?= (@$data['quotation_total_discount_type'] == 2) ? 'selected' : ''; ?>>Amount (Number)</option>
                                  </select>
                                </th>
                                <!-- <th colspan="1" class="text-right" >Discount Percentage</th> -->
                                <th class="text-center " style="display:flex" colspan="1">
                                <input type="text" style="width:36%" class="mr-1 numeric-align total_discount_amount" id="total_discount_amount" name="quotation_total_discount_amount" value="<?= @$data['quotation_total_discount_amount']; ?>" placeholder="0" />
                                  <input type="text" style="width:58%" class="quotation_discount_notes" id="quotation_discount_notes" name="quotation_discount_notes" value="<?= @$data['quotation_discount_notes']; ?>" placeholder="Notes" />
                                </th>
                                <th class="text-center">
                                  <input type="text" readonly class="text-right" id="total_discount_amount_val" name="total_discount_amount_val" value="" placeholder="0" />
                                </th>
                              </tr>

                              <tr>
                                <th colspan="3" class="text-right quot-vc">Buyback Discount</th>
                                <th class="text-center">
                                  <input type="text" style="width:100%" class="numeric-align quotation_buypack_discount" id="quotation_buypack_discount" name="quotation_buypack_discount" value="<?= quotation_num_format(@$data['quotation_buypack_discount']); ?>" placeholder="0" />
                                </th>
                              </tr>
                              <tr class="hide_tax_row" style="<?= (isset($user_country) && $company_country != $user_country)  ? 'display:none' : '' ?>">
                                <th colspan="3" class="text-right quot-vc">Tax Amount</th>
                                <th class="text-center">
                                  <input type="text" name="quotation_tax_amount" style="width:100%" class="numeric-align quotation_tax_val" id="quotation_tax_val" value="" placeholder="0" readonly />
                                  <input type="hidden" style="width:100%" class="numeric-align quotation_tax" id="quotation_tax" name="quotation_tax" value="<?= @$setval['tax'] ?>" placeholder="0" readonly />
                                </th>
                              </tr>

                              <?php
                              $quotation_total_discount_amount_val = 0;
                              if (@$data['quotation_total_discount_type'] == 1) {
                                if (@$data['quotation_total_discount_amount'] != '') {
                                  if (@$data['quotation_total_discount_amount'] <= 100) {
                                    $quotation_total_discount_amount_val = ($data['quotation_total_discount_amount'] / 100) * $subtotal;
                                  } else {

                                    $quotation_total_discount_amount_val = $subtotal;
                                    // return false;
                                  }
                                }
                              } else if (@$data['quotation_total_discount_type'] == 2) {
                                if (@$data['quotation_total_discount_amount'] != '') {
                                  $quotation_total_discount_amount_val = @$data['quotation_total_discount_amount'];
                                }
                              } else {
                                $quotation_total_discount_amount_val = 0;
                              }

                              $quotation_total_discount_amount_val = $subtotal - $quotation_total_discount_amount_val;
                              ?>
                              <tr>
                                <th colspan="3" class="text-right quot-vc">Net Amount</th>
                                <th class="text-center"> <input type="text" readonly="readonly" class="numeric-align" id="grandtotal" name="total" style="width:100%" value="<?= quotation_num_format(@$quotation_total_discount_amount_val); ?>" placeholder="0" /> </th>
                                <!--  <th class="text-center"><span id="subtotal">0</span></th> -->
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- <div class="form-group">
                          <div class="row form-row col-md-6">
                            <div class="col-md-4">
                              <label class="form-label">Customer Note</label>
                            </div>
                              <div class="col-md-2">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <textarea name="quotation_customer_note" class="form-control" value="" placeholder=""  ><?= @$data['quotation_customer_note']; ?></textarea>
                                </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="row form-row col-md-6">
                            <div class="col-md-4">
                              <label class="form-label">Terms & Conditions</label>
                            </div>
                            <div class="col-md-2">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <textarea id="myeditor" name="quotation_terms_conditions" class="form-control"  placeholder=""><?= @$data['quotation_terms_conditions']; ?></textarea>
                                </div>
                            </div>
                          </div>
                        </div>  -->

                    <div class="clearfix"></div>
                    <div class="form-group">
                      <h3>Payment Terms</h3>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="my-table-controls dataTables_wrapper-768">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th class="text-center quot-PayTerm-dt-th1">Add/Remove Row</th>
                                  <th class="text-center quot-PayTerm-dt-th2">Terms & Condition</th>
                                  <th class="text-center quot-PayTerm-dt-th3">Percentage</th>
                                  <th class="text-center quot-PayTerm-dt-th4">No Of Days</th>
                                  <th class="text-center quot-PayTerm-dt-th5">Amount</th>
                                </tr>
                              </thead>
                              <tr class="txtMult">
                                <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="paddCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                <td colspan="6"></td>
                              </tr>
                              <tbody id="pcustomFields">
                                <?php
                                if (@$terms != null && !empty(@$terms)) {
                                  foreach ($terms as $row) {
                                ?>

                                    <tr class="txtMult">
                                      <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="premCF test"> <span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>
                                      <td>
                                        <input type="text" class="terms" required style="width:100%" id="terms" name="terms[]" value="<?= @$row['payment_title'] ?>" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="percentage txtboxToFilter numeric-align" required style="width:100%" id="percentage'+x+'" data-optional="0" name="percentage[]" value="<?= @$row['percentage'] ?>" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="numeric-align payment_days txtboxToFilter" required style="width:100%" id="payment_days'+x+'" data-optional="0" name="payment_days[]" value="<?= @$row['payment_days'] ?>" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="numeric-align payment_amount txtboxToFilter" required readonly style="width:100%" id="payment_amount'+x+'" data-optional="0" name="payment_amount[]" value="<?= @$row['payment_amount'] ?>" placeholder="" />
                                      </td>
                                    </tr>
                                <?php }
                                } ?>

                              </tbody>
                              <tfoot>
                                <th class="text-center" colspan='4'><label class="quot-vc quot-mg-top" style="float:right">Amount</label></th>
                                <th class="text-center" width="200px"><input style="width:100%" readonly type='text' name='payment_terms_total' class="numeric-align"></th>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                      <h3>Project Duration</h3>
                      <div class="row">
                        <div class="col-md-12">
                          <div class=" dataTables_wrapper-1440 dataTables_wrapper-1441">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th class="text-center quot-ProjDur-dt-th1">Add/Remove Row</th>
                                  <th class="text-center quot-ProjDur-dt-th2">Terms & Condition</th>
                                  <th class="text-center quot-ProjDur-dt-th3">HASH COLOR CODE</th>
                                  <th class="text-center quot-ProjDur-dt-th3">Start Week</th>
                                  <th class="text-center quot-ProjDur-dt-th3">No of Week</th>
                                </tr>
                              </thead>
                              <tr class="new_txtMult">
                                <td class="text-center"><a href="javascript:void(0);" class="new_addCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                <td colspan="6"></td>
                              </tr>
                              <tbody id="new_customFields">

                                <?php
                                if (@$project_terms != null && !empty(@$project_terms)) {
                                  foreach ($project_terms as $row) {
                                ?>

                                    <tr class="new_txtMult">
                                      <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="new_remCF"> <span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>
                                      <td>
                                        <input type="text" class="project_terms" required style="width:100%" id="project_terms" name="project_terms[]" value="<?= @$row['project_title'] ?>" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="hash_code" required value="<?= @$row['hash_code'] ?>" style="width:100%" data-optional="0" name="hash_code[]" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="code numeric-align quantity txtboxToFilter start_week" required value="<?= @$row['start_week'] ?>" style="width:100%" data-optional="0" name="start_week[]" value="" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="numeric-align code quantity txtboxToFilter project_days" required value="<?= @$row['project_days'] ?>" style="width:100%" data-optional="0" name="project_days[]" value="" placeholder="" />
                                      </td>

                                    </tr>
                                <?php }
                                } ?>

                              </tbody>
                              <!-- <tfoot>
                                                      <th class="text-center" colspan='4'><label class="quot-vc quot-mg-top" style="float:right"> TOTAL NUMBER OF Weeks</label></th>
                                                      <th class="text-center"  width="200px"><input style="width:100%" type='text' readonly name='duration_total' class="numeric-align"></th>
                                                  </tfoot> -->
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- <h3>Warranty Terms</h3>
                                     <div class="form-group">
                                        <div class="row" >
                                            <div class="col-md-12">
                                              <div class="" style="width: 100%;">
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <tr>
                                                      <th class="text-center" width="60px">Add/Remove Row</th>
                                                      <th class="text-center" width="200px">Features</th>
                                                      <?php
                                                      foreach (@$warranty_type as $type) {
                                                      ?>
                                                         <th class="text-center" width="200px"><?= $type['title'] ?></th>
                                                         <th class="text-center" width="200px">Percentage</th>
                                                       <?php }  ?>
                                                    </tr>
                                                  </thead>
                                                  <tr class="warn_txtMult">
                                                      <td class="text-center"><a href="javascript:void(0);" class="warn_addCF">Add</a></td>
                                                      <td colspan="6"></td>
                                                  </tr>
                                                  <tbody id="warn_customFields">
                                                      <?php
                                                      if (@$warranty != null && !empty(@$warranty)) {
                                                        $check_data = array();
                                                        $new_data   = array();
                                                        foreach ($warranty as $warranty) {
                                                      ?>

                                                     <tr class="warn_txtMult">
                                                            <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="warn_remCF">Remove</a></td>
                                                            <td>
                                                                <input type="text" class="features" required style="width:100%" id="features" name="features[]"
                                                                value="<?= @$warranty['title'] ?>" placeholder="" />
                                                            </td>
                                                            <?php
                                                            $c = 1;

                                                            foreach (@$warranty_type as $type) {
                                                              $value = 2;
                                                              $warn_percentage = 0;
                                                              $find = true;
                                                              foreach ($warranty_setting as $setting) {
                                                                $tmp = isset($warranty['quotation_warranty_id']) ? $warranty['quotation_warranty_id'] : $warranty['warranty_id'];
                                                                if ($tmp == $setting['warn_id'] && $type['warranty_type_id'] == $setting['type_id']) {
                                                                  $value = $setting['value'];
                                                                  $warn_percentage = $setting['percentage'];
                                                                }
                                                              }
                                                            ?>
                                                             <td>
                                                                <select name="warranty_type_id<?= $c ?>[]" style="width:100%" id="warranty_type_id" class="form-control warranty_type_id" required>
                                                                    <option value="" disabled="" selected="">-- Select Option --</option>
                                                                    <option value="1" <?= (@$value == 1) ? 'selected' : '' ?>>Enabled</option>
                                                                    <option value="0" <?= (@$value == 0) ? 'selected ' : '' ?>>Disabled</option>
                                                                </select>
                                                            </td>
                                                             <td>
                                                                <input type="text" class="warranty_percentage" required style="width:100%" id="warranty_percentage" name="warranty_percentage<?= $c ?>[]"
                                                                value="<?= @$warn_percentage ?>" placeholder="" />
                                                            </td>

                                                        <?php
                                                              $c++;
                                                            } ?>
                                                        </tr>
                                                    <?php }
                                                      } ?>

                                                  </tbody>
                                                </table>
                                              </div>  
                                            </div>
                                    </div>
                                </div>
                                 <div class="clearfix"></div>   -->
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <div class="row form-row">
                        <div class="col-md-12">
                          <label class="form-label">Quotation Cover</label>
                          <button type="button" class="get_terms_data btn-warning quotation_ref_btn btn btn-sm">Refresh <i class="fa fa-refresh"></i></button>
                        </div>
                        <div class="col-md-12">
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <textarea name="quotation_cover_letter" id="myeditor" class="form-control" placeholder=""><?= ($page_title == 'add') ? @$setval['quotation_cover'] : @$data['quotation_cover_letter']; ?></textarea>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <div class="row form-row">
                        <div class="col-md-12">
                          <label class="form-label">Terms & Condition</label>
                        </div>
                        <div class="col-md-12">
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <textarea name="quotation_terms_conditions" id="myeditor2" class="form-control" placeholder=""><?= ($page_title == 'add') ? @$setval['quotation_terms_conditions'] : @$data['quotation_terms_conditions']; ?></textarea>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group text-center">
                      <!-- <a href="<?= site_url("Print_pdf"); ?>" target="_blank" class="btn btn-success btn-cons">Print Quotation</a> -->
                      <br>
                      <button class="btn btn-success btn-cons ajaxFormSubmitAlter " type="button"><?= ($page_title == 'add') ? '' : ''; ?> Submit</button>
                      <input name="id" type="hidden" value="<?= @$data['quotation_id']; ?>">
                      <input type="hidden" value="<?= @$data['quotation_status'] ?>" name="quotation_status" />
                      <input name="quotation_email_printed_status" type="hidden" value="<?= @$data['quotation_email_printed_status']; ?>">

                      <input name="quotation_no_old" type="hidden" value="<?= @$data['quotation_no']; ?>">
                    </div>
                  </div>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade " id="defaultModalDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel" class="semi-bold mymodal-customer-title">Edit Description</h4>
      </div>
      <div class="modal-body mymodal-customer-body" id="myModalDescription">
        <div class="grid-body no-border">
          <textarea id="myeditor1" name="temp_quotation_desc" class="form-control temp_quotation_desc" value="" placeholder=""></textarea>
          <input type="hidden" id="textbox">
        </div>
        <div class="modal-footer">
          <button class="btn btn-success edit_desc_button" data-dismiss="modal" type="button">
            Save & Close
          </button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</div>
<!-- Modal -->
<?php include APPPATH . 'views/include/customer_modal.php';  ?>
<!-- /.modal -->
<!-- END BASIC FORM ELEMENTS-->
<script>
  $(document).ready(function() {

    $.validator.addMethod(
      "checkPercentage",
      function(value, element) {
        var check = true;
        var total_percentage = 0;
        $("#pcustomFields tr.txtMult").each(function() {

          var row = $(this).closest('tr'); // get the row
          var percentage = row.find('.percentage').val();
          percentage = (percentage == undefined || percentage == '') ? 0 : percentage;
          total_percentage += parseInt(percentage, 10);

        });
        if (total_percentage > 100 || total_percentage < 100) {
          check = false;
        }
        return check;

      },
      "Username is Already Taken"
    );
    $("form.validate").validate({
      rules: {
        customer_id: {
          required: true
        },
        "product_id[]": {
          required: true
        },
        "quotation_detail_quantity[]": {
          required: true,
          //number: true
        },
        "quotation_detail_discount_amount[]": {
          number: true
        },
        quotation_freight_type: {
          required: true,
        },
        quotation_detail_total_discount_amount: {
          number: true
        },
        quotation_no: {
          required: true,
          // digits: true
        },
        quotation_date: {
          required: true
        },
        quotation_expiry_date: {
          required: true
        },
        quotation_customer_note: {
          required: true
        },
        quotation_terms_conditions: {
          required: true,

        },
        employee_id: {
          required: true,
        },
        "percentage[]": {
          required: true,
          checkPercentage: true
        },
        quotation_validity: {
          required: true,
        },
        quotation_support: {
          required: true,
        },
        quotation_warranty: {
          required: true,
        },
        quotation_cover_letter: {
          required: true
        },
        'start_week[]': {
          digits: true,
          required: true,
        }
      },
      messages: {
        customer_id: "This field is required.",
        "product_id[]": "This field is required.",
        "quotation_detail_quantity[]": {
          required: "This field is required.",
          number: "Please Insert Number."
        },
        "discount_amount[]": "Please Insert Number.",
        total_discount_amount: "Please Insert Number.",
        quotation_no: {
          required: "This field is required.",
          digits: "Please enter only digits."
        },
        quotation_date: "This field is required.",
        quotation_expiry_date: "This field is required.",
        quotation_customer_note: "This field is required.",
        quotation_terms_conditions: "This field is required.",
        "percentage[]": {
          required: "This field is required.",
          checkPercentage: "Percentage Sum should be equal to 100."
        },
        quotation_cover_letter: "This field is required"
      },
      invalidHandler: function(event, validator) {
        //display error alert on form submit    
        error("Please input all the mandatory values marked as red");
      },
      errorPlacement: function(label, element) { // render error placement for each input type   

        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        $('<span class="error"></span>').insertAfter(element).append(label);
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('success-control').addClass('error-control');
      },
      highlight: function(element) { // hightlight error inputs
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        var parent = $(element).parent();
        parent.removeClass('success-control').addClass('error-control');
      },
      unhighlight: function(element) { // revert the change done by hightlight
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent();
        parent.removeClass('error-control').addClass('success-control');
      },
      success: function(label, element) {
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('error-control').addClass('success-control');

      }
      // submitHandler: function (form) {
      // }
    });
    $('.select2', "form.validate").change(function() {
      $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    $('#fileUploader').fileuploader({
      changeInput: '<div class="fileuploader-input">' +
        '<div class="fileuploader-input-inner">' +
        '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
        '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
        '<p>or</p>' +
        '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
        '</div>' +
        '</div>',
      theme: 'dragdrop',
      // limit: 4,
      // extensions: ['jpg', 'jpeg', 'png', 'gif'],
      onRemove: function(item) {

      },
      captions: {
        feedback: 'Drag and drop files here',
        feedback2: 'Drag and drop files here',
        drop: 'Drag and drop files here'
      },
    });
  });
  //my code//

  var base_currency = '<?php echo $base_currency_id; ?>';
  var company_country = '<?php echo $company_country; ?>';
  var current_currency = '<?= @$data['quotation_currency'] ?>';
  var company_tax = "<?= @$setval['tax'] ?>";
  $(document).on("click", ".edit_desc_button", function(event) {
    var desc = $('#textbox').val();
    var get_data = CKEDITOR.instances['myeditor1'].getData()
    //$('#' + desc).val(/*$('.temp_quotation_desc').val()*/ get_data);
    $('#' + desc).html(get_data /*.replace(/(<([^>]+)>)/ig, "")*/ );
    // console.log($('#temp_quotation_desc').val());
  });
  $(document).on("click", ".myModalBtnDesc", function(event) {
    event.preventDefault();
    var row = $(this).closest('tr');
    var desc = row.find('.prod_desc_text').val();
    var textbox = $(this).data("textarea_id");
    $('#textbox').val(textbox);
    //$('.temp_quotation_desc').val(desc);
    CKEDITOR.instances['myeditor1'].setData(desc);
    // console.log(desc);
    $('#defaultModalDesc').modal('show');
  });

  $(document).on("click", ".myModalBtnDesc", function(event) {
    event.preventDefault();
    $('#defaultModalDesc').modal('show');
  });

  $(".disable").each(function(i) {
    $(this).click(function() {

      $(".disable").attr("disabled", "disabled");
      //$("#checkbox1").attr("disabled", "disabled"); 

    });

  });

  $(document).ready(function() {


    $('.percentage').trigger('keyup');
    $('.project_days').trigger('keyup');
    $('.payment_amount').trigger('keyup');

    var page = '<?php echo $page_title; ?>';
    var check_select = 0;
    if (page != 'add') {
      check_select = 1;
    }
    if (current_currency != '') {
      $("input[name=quotation_currency][value=" + current_currency + "]").attr('checked', 'checked');
      setTimeout(function() {
        $(".quotation_currency").trigger("change");
      }, 500);
      price_plan_change();
    }


    $(document).on('change', ".quotation_currency", function() {

      var currency_id = $('input[name="quotation_currency"]:checked').val();

      $(".prod_name").each(function() {


        var convert_to = $('input[name="quotation_currency"]:checked').data('title');
        var row = $(this).closest('tr');

        var pro_amount = row.find('.pro_amount');
        var hidden_base_price = row.find('.base_price');
        var hidden_usd_price = row.find('.usd_price');
        var quotation_detail_id = row.find('.quotation_detail_id').val();
        var prod_name = $(this).val();
        var customer = $('#customer_id').val();
        var street = row.find('.street'); // get the other select in the same row
        var quatity_num = row.find('.quatity_num');
        var discount_type = row.find('.discount_type');
        var discount_field = row.find('.discount_amount');
        var item_discount_max = row.find('.item_discount_max');
        var desc = $(this).closest('tr').find('.prod_desc_text');
        var price_plan_id = $("#customer_id").find(':selected').data("price-plan-id");
        var customer_type_title = $("#customer_id").find(':selected').data("customer-type-title");
        customer_type_title = customer_type_title.toLowerCase();
        var currency_title = $('input[name="quotation_currency"]:checked').data('title');
        var quotation_id = $('input[name="id"]').val();
        currency_title = currency_title.toLowerCase();
        if (currency_title == 'aed') {
          currency_title = 'base';
        }
        var dataString = 'pid=' + prod_name + '&plan_id=' + price_plan_id + '&quotation_id=' + quotation_id + '&quotation_detail_id=' + quotation_detail_id;

        $.ajax({
          url: "<?php echo site_url('quotation/progt'); ?>",
          dataType: "json",
          type: "POST",
          // async: true,
          data: dataString,
          cache: false,
          success: function(employeeData) {
            if (employeeData) {
              // console.log(employeeData);
              //quatity_num.attr("max", employeeData.total_inventory_quantity);

              street.val(employeeData.product_description);
              var temp = employeeData.product_description;
              if (desc.html() == null || desc.html() == '') {
                desc.html(temp /*.replace(/(<([^>]+)>)/ig, "")*/ );
              }

              //var category_type = employeeData.title.trim().toLowerCase();

              //desc.data('text',temp);
              $(this).closest('tr').next('tr').find('.myModalBtnDesc').attr('data-id', temp);
              // console.log(data['rates']);
              var amount = 0;
              var discount = 0;
              var price_name = '';

              price_name = customer_type_title + '_' + currency_title + '_price';
              amount = employeeData[price_name];
              hidden_usd_price.val(employeeData[customer_type_title + '_usd_price']);
              hidden_base_price.val(employeeData[customer_type_title + '_base_price']);
              // convrt_amount.val(amount);
              pro_amount.val(amount);
              var price_name = customer_type_title + '_discount';
              discount = employeeData[price_name];
              if (discount == null || discount == 0) {
                discount_field.val(0);
                item_discount_max.val(0);
                discount_field.attr('readonly', true);
              } else {
                discount_field.attr("max", discount);
                item_discount_max.val(discount);
              }
              if (discount_type.val() == 2) {
                var max_percentage = item_discount_max;
                var this_total = (quatity_num.val() * 1) * (amount * 1);
                discount_field.attr('max', this_total / 100 * max_percentage);
              }
              quatity_num.removeAttr("max");
              quatity_num.removeAttr("min");

              /*if(category_type == 'service') {
                        }
*/
              row.find('.amount').val(amount * quatity_num.val());
              $('.txtMult input').trigger('keyup');

            } else {
              //quatity_num.attr("max", 0);
              //quatity_num.attr("min", 0);
              $("#heading").hide();
              $("#records").hide();
              $("#no_records").show();
            }
          }
        });

      });
    });

    var max_fields = 6;
    // var wrapper         = $(".amenities_field");
    var add_button = $(" .qt_list");
    var x = 1;
    $(".qt_list").live('click', function(e) {
      e.preventDefault();
      $(".gridAddBtn").remove();
      //$(add_button).click(function(e) {
      e.preventDefault();
      var customer = $('#customer_id').valid();
      var quotation_currency = $('input[name=quotation_currency]').valid();
      // console.log(quotation_currency);
      if (!customer || !quotation_currency) {
        return false;
      }
      // if(x < max_fields){
      x++;
      var html = '';
      // $("#customFields").append('<tr class="txtMult"><td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td><td colspan="2" class="label-font" ><select name="product_id[]" style="" id="product_id'+x+'" class="form-control prod_name" required><option selected disabled>--- Select Product ---</option><?php foreach ($productData as $k => $v) { ?><option value="<?= $v['product_id']; ?>"><?= $v['product_name']; ?></option><?php } ?></select> <label style="display: inline;" for="quotation_detail_optional'+x+'" class="control control-checkbox"><b>Optional</b><input type="checkbox" value="1" class="quotation_detail_optional"  id="quotation_detail_optional'+x+'" name="quotation_detail_optional[]"  placeholder="" /><div class="control_indicator control_indicator1 "></div></label> <label style="    display: inline;" for="quotation_detail_existing'+x+'" class="control control-checkbox"><b>Existing will be used</b><input type="checkbox" value="1" class="quotation_detail_existing"  id="quotation_detail_existing'+x+'" name="quotation_detail_existing[]"  placeholder="" /><div class="control_indicator control_indicator1"></div></label> <label style="    display: inline;" for="quotation_detail_provided'+x+'" class="control control-checkbox"><b>To be provided by client</b><input type="checkbox" value="1" class="quotation_detail_provided"  id="quotation_detail_provided'+x+'" name="quotation_detail_provided[]"  placeholder="" /><div class="control_indicator control_indicator1"></div></label><br><textarea name="quotation_desc[]" id="quotation_desc'+x+'" class="form-control prod_desc_text" placeholder="" readonly="readonly" style="display: inline;width: 70%;"></textarea><a href="#" data-textarea_id="quotation_desc'+x+'" class="btn-warning btn btn-sm myModalBtnDesc my-txtarea-control" style="margin:10px;" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a><label style="display: inline;" for="quotation_detail_required'+x+'" class="control control-checkbox"><b>Not Required</b><input type="checkbox" value="1" class="quotation_detail_required"  id="quotation_detail_required'+x+'" name="quotation_detail_required[]"  placeholder="" /><div class="control_indicator control_indicator1"></div></label></td><td><select name="quotation_detail_total_discount_type[]" style="width:200px" class="form-control discount_type"><option value="0">-- select discount type --</option><option value="1">Percentage (%)</option></select><br><input type="text" class="code discount_amount txtboxToFilter" style="width:200px" id="discount_amount'+x+'" name="quotation_detail_total_discount_amount[]" style="width:60px" value="" placeholder="" /></td><td><input type="text" class="code quantity txtboxToFilter quatity_num margin-control" style="width:60px" id="quatity_num'+x+'" data-optional="0" name="quotation_detail_quantity[]" value="" placeholder="" /></td><td><input type="text" readonly="readonly" class="margin-control code pro_amount rate_num" style="width:60px;" id="quotation_detail_rate'+x+'" name="quotation_detail_rate[]" placeholder="" /></td><td><input type="text" readonly="readonly" class="margin-control code amount  multTotal" style="width:200px" id="amount'+x+'" name="amount[]" value="0" placeholder="" /></td></tr><tr><td></td><td colspan="2"></td><td colspan="5"></td></tr>');
      html += '<tr class="txtMult">';
      html += '<td class="text-center" style="width:60px;">';
      html += '<a href="javascript:void(0);" class="addCF qt_list"><span class="glyphicon glyphicon-plus-sign gridAddBtn" style="font-size:25px;"></span></a> <a href="javascript:void(0);" class="remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
      html += '</td>';
      html += '<td class="label-font" >';
      html += '<select name="product_id[]" id="product_id' + x + '" class="-control prod_name mb-3" style="width: 600px;" required>';
      html += '<option selected disabled>--- Select Product ---</option>';
      html += '<?php foreach ($productData as $k => $v) { ?><option value="<?= $v['product_id']; ?>"> <?= $v['product_sku']; ?> - <?= $v['product_name']; ?></option><?php } ?>';
      html += '</select> <br>';

      html += '<div class="checkbox checkbox-inline check-success">';
      html += '<input type="checkbox" value="1" onchange="check_hendler(\'quotation_detail_optional' + x + '\')" class="quotation_detail_optional"  id="quotation_detail_optional' + x + '"  placeholder="" />';
      html += '<label style="padding-left:25px" for="quotation_detail_optional' + x + '">Optional</label>';
      html += '<input type="hidden" value="0" class="quotation_detail_optional' + x + '_hidden"  id="quotation_detail_optional' + x + '_hidden" name="quotation_detail_optional[]"  placeholder="" />';
      html += '</div>';

      html += '<div class="checkbox checkbox-inline check-success">';
      html += '<input type="checkbox" value="1" onchange="check_hendler(\'quotation_detail_existing' + x + '\')" class="quotation_detail_existing"  id="quotation_detail_existing' + x + '" placeholder="" />';
      html += '<label style="padding-left:25px" for="quotation_detail_existing' + x + '">Existing will be used</label>';
      html += '<input type="hidden" value="0" class="quotation_detail_existing' + x + '_hidden"  id="quotation_detail_existing' + x + '_hidden" name="quotation_detail_existing[]"  placeholder="" />';
      html += '</div>';


      html += '<div class="checkbox checkbox-inline check-success">';
      html += '<input type="checkbox" value="1" onchange="check_hendler(\'quotation_detail_provided' + x + '\')" class="quotation_detail_provided"  id="quotation_detail_provided' + x + '" placeholder="" />';
      html += '<label style="padding-left:25px" for="quotation_detail_provided' + x + '">To be provided by client</label>';
      html += '<input type="hidden" value="0" class="quotation_detail_provided' + x + '_hidden"  id="quotation_detail_provided' + x + '_hidden" name="quotation_detail_provided[]"  placeholder="" />';
      html += '</div>';
      //html += '<br>';



      html += '<div class="checkbox checkbox-inline check-success">';
      html += '<input type="checkbox" value="1"  onchange="check_hendler(\'quotation_detail_required' + x + '\')" class="quotation_detail_required"  id="quotation_detail_required' + x + '" placeholder="" />';
      html += '<label style="padding-left:25px" for="quotation_detail_required' + x + '">Not Required</label>';
      html += '<input type="hidden" value="0" class="quotation_detail_required' + x + '_hidden"  id="quotation_detail_required' + x + '_hidden" name="quotation_detail_required[]"  placeholder="" />';
      html += '</div>';

      html += '<textarea name="quotation_desc[]" id="quotation_desc' + x + '" style="display:none;" class="form-control prod_desc_text" placeholder="" readonly="readonly" style="display:inline;width: 70%;"></textarea>';
      html += '<a href="#" data-textarea_id="quotation_desc' + x + '" class="btn-warning btn btn-sm myModalBtnDesc my-txtarea-control"  data-toggle="tooltip"title="Edit">Edit Description</a>';

      html += '</td>';
      html += '<td>';
      html += '<select name="quotation_detail_total_discount_type[]" style="width:100%" class="form-control discount_type">';
      html += '<option value="0">-- select discount type --</option><option value="1">Percentage (%)</option>';
      html += '<option value="2">Amount (Number)</option>';
      html += '</select>';
      html += '<br><input type="text" class="numeric-align code discount_amount txtboxToFilter" style="width:100%" id="discount_amount' + x + '" name="quotation_detail_total_discount_amount[]" style="width:60px" value="" placeholder="" />';
      html += '<br><input type="text" style="width:100%" id="quotation_detail_note' + x + '" name="quotation_detail_note[]" style="width:60px" placeholder="Notes" class="quotation_detail_note" />';
      html += '<input type="hidden" name="quotation_detail_max_discount[]" value="" class="item_discount_max"/></td>';
      html += '<td align="center"><input type="text" class="numeric-align code quantity txtboxToFilter quatity_num margin-control" style="width:60px" id="quatity_num' + x + '" data-optional="0" name="quotation_detail_quantity[]" value="" placeholder="" /></td>';
      html += '<td align="center"><input type="text" readonly="readonly" class="margin-control code pro_amount rate_num numeric-align" style="width:60px;" id="quotation_detail_rate' + x + '" name="quotation_detail_rate[]" placeholder="" />';
      html += '<input type="hidden" value="0" class="quotation_detail_id" />';
      html += '<input type="hidden" name="base_price[]" class="base_price" />';
      html += '<input type="hidden" name="usd_price[]" class="usd_price" />';
      html += '</td>';
      html += '<td><input type="text" readonly="readonly" class="numeric-align margin-control code amount  multTotal" style="width:100%" id="amount' + x + '" name="amount[]" value="0"placeholder="" /></td>';
      html += '</tr>';
      $("#customFields").append(html);
      $("#product_id" + x).select2();
      // }
    });

    var max_fields = 6;
    var add_button = $(".paddCF");
    var x = 1;
    $(add_button).click(function(e) {
      e.preventDefault();
      $('form.validate').validate();
      // if(x < max_fields){
      x++;
      var temp = '<tr class="txtMult">';
      temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="premCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>';
      temp += "<td>";
      //I removed class from below .txtboxToFilter
      temp += '<input type="text" class="code quantity  terms" required  style="width:100%" id="terms' + x + '" data-optional="0" name="terms[]" value="" placeholder="" />';
      temp += '</td>';
      temp += "<td>";
      temp += '<input type="text" class="code quantity txtboxToFilter percentage numeric-align" required  style="width:100%" id="percentage' + x + '" data-optional="0" name="percentage[]" value="" placeholder="" />';
      temp += '</td>';
      temp += "<td>";
      temp += '<input type="text" class="code quantity txtboxToFilter payment_days numeric-align" required  style="width:100%" id="payment_days' + x + '" data-optional="0" name="payment_days[]" value="" placeholder="" />';
      temp += '</td>';
      temp += '<td>';
      temp += "<input type='text' class='payment_amount code numeric-align' required style='width:100%'' id='payment_amount" + x + "' data-optional='0' name='payment_amount[]' readonly placeholder='' />";
      temp += '</td>';
      temp += '</tr>';
      $("#pcustomFields").append(temp);
      // }
    });
    $(document).on('click', '.premCF', function() {
      $(this).parent().parent().remove();
      $('.txtMult .payment_amount').trigger('keyup');
    });

    $("#customFields").on('click', '.remCF', function() {
      //$(this).parent().parent().next('tr').remove();
      $(this).parent().parent().remove();
    });

    var max_fields = 6;
    var add_button = $(".new_addCF");
    var x = 1;
    $(add_button).click(function(e) {
      e.preventDefault();
      $('form.validate').validate();
      // if(x < max_fields){
      x++;
      var temp = project_terms(x);
      $("#new_customFields").append(temp);
      // }
    });

    function project_terms(x, data = '') {
      var temp = '<tr class="new_txtMult">';
      temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="new_remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>';
      temp += "<td>";
      temp += '<input type="text" class="code quantity txtboxToFilter project_terms" required  style="width:100%" id="project_terms' + x + '" data-optional="0" name="project_terms[]" value="" placeholder="" />';
      temp += '</td>';
      temp += "<td>";
      temp += '<input type="text" class="hash_code " required  style="width:100%" id="hash_code' + x + '" data-optional="0" name="hash_code[]" value="" placeholder="" />';
      temp += '</td>';
      temp += "<td>";
      temp += '<input type="text" class="code quantity txtboxToFilter numeric-align start_week" required  style="width:100%" id="start_week' + x + '" data-optional="0" name="start_week[]" value="" placeholder="" />';
      temp += '</td>';
      temp += "<td>";
      temp += '<input type="text" class="code quantity txtboxToFilter numeric-align project_days" required  style="width:100%" id="project_days' + x + '" data-optional="0" name="project_days[]" value="" placeholder="" />';
      temp += '</td>';
      temp += '</tr>';
      return temp;
    }
    $("#new_customFields").on('click', '.new_remCF', function() {
      $(this).parent().parent().remove();
    });



    $("#customer_id").on("click", "", function(event) {
      //empty all inputs except quotation_no
      //alert("te");
      // event.preventDefault();
      if (check_select == 0) {
        check_select++;
        add_terms();
        price_plan_change();
        return true;
      }
      var form = document.getElementById("general_fields");
      // console.log(form);
      var inputs = form.getElementsByTagName("input");
      // console.log(inputs);


      swal({
          title: "Are you sure, discount will be gone?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

            for (i = 0; i < inputs.length; i++) {
              // console.log(inputs[i].name);
              if (inputs[i].name != "quotation_no") {
                if (inputs[i].name != "quotation_currency") {
                  if (inputs[i].name != "quotation_freight_type") {
                    if (inputs[i].name != "quotation_no_temp") {
                      if (inputs[i].name != "quotation_revised_no") {
                        if (inputs[i].name != "quotation_validity") {
                          if (inputs[i].name != "quotation_support") {
                            if (inputs[i].name != "quotation_warranty") {
                              if (inputs[i].name != "quotation_no_old") {
                                if (inputs[i].name != 'id') {
                                  if (inputs[i].name != 'hash_code') {
                                    if (inputs[i].name != 'start_week') {
                                      if (inputs[i].name != 'project_days') {
                                        if (inputs[i].name != 'quotation_date') {
                                          if (inputs[i].name != 'quotation_expiry_date') {
                                        inputs[i].value = "";
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }

                        }
                      }
                    }
                  }
                }

              }
            }
            add_terms();
            price_plan_change();
          } else {
            return false;
          }
        });

      // $("#customFields").empty();
      // $("#customFields").append("<tr class='txtMult'><td class='text-center'><a href='javascript:void(0);' class='addCF'>Add</a></td><td colspan='7'></td></tr>");

    });

  });


  // payment amount total
  $(document).on('keydown keypress keyup change blur', ".percentage", function() {
    cal_percentage();
  });

  //days total
  $(document).on('keydown keypress keyup change blur', ".project_days", function() {
    var total = 0;
    $("#new_customFields tr.new_txtMult").each(function() {

      var row = $(this).closest('tr'); // get the row
      var days = row.find('.project_days').val();
      days = (days == undefined || days == '') ? 0 : days;
      // console.log(days);
      total += parseInt(days, 10);
    });

    $('input[name="duration_total"]').val(total);
  });


  $(document).on('change', ".quotation_detail_optional,.quotation_detail_existing,.quotation_detail_provided,.quotation_detail_required", function() {
    var row = $(this).closest('tr'); // get the row
    var quotation_detail_optional = row.find('.quotation_detail_optional').prop('checked');
    var quotation_detail_existing = row.find('.quotation_detail_existing').prop('checked');
    var quotation_detail_provided = row.find('.quotation_detail_provided').prop('checked');
    var quotation_detail_required = row.find('.quotation_detail_required').prop('checked');
    var check = 0;

    if (quotation_detail_existing == true) {
      check = 1;
    }
    if (quotation_detail_provided == true) {
      check = 1;
    }
    if (quotation_detail_required == true) {
      check = 1;
    }

    if (check == 1) {
      console.log('chk 1');
      var rate = row.find('.pro_amount').val(0);
      $('.txtMult input').trigger('keyup');
    } else {
      var prod_name = row.find('.prod_name').find(":selected").val();
      var quotation_detail_id = $(this).closest('tr').find('.quotation_detail_id').val();
      var customer = $('#customer_id').val();
      var price_plan_id = $("#customer_id").find(':selected').data("price-plan-id");
      var dataString = 'pid=' + prod_name + '&plan_id=' + price_plan_id + '&quotation_detail_id=' + quotation_detail_id;
      var street = row.find('.street'); // get the other select in the same row
      var pro_amount = row.find('.pro_amount');
      var total_amount = row.find('.multTotal');

      var quatity_num = row.find('.quatity_num');
      var convert_to = $('input[name="quotation_currency"]:checked').data('title');
      var desc = $(this).closest('tr').find('.prod_desc_text');
      var currency_id = $('input[name="quotation_currency"]:checked').val();
      var discount_field = row.find('.discount_amount');
      var item_discount_max = row.find('.item_discount_max');
      var customer_type_title = $("#customer_id").find(':selected').data("customer-type-title");
      customer_type_title = customer_type_title.toLowerCase();
      var currency_title = $('input[name="quotation_currency"]:checked').data('title');
      currency_title = currency_title.toLowerCase();
      if (currency_title == 'aed') {
        currency_title = 'base';
      }

      $.ajax({
        url: "<?php echo site_url('quotation/progt'); ?>",
        dataType: "json",
        type: "POST",
        // async: true,
        data: dataString,
        cache: false,
        success: function(employeeData) {
          if (employeeData && employeeData != null) {

            var amount = 0;
            var discount = 0;
            var price_name = '';
            var category_type = employeeData.title.trim().toLowerCase();
            price_name = customer_type_title + '_' + currency_title + '_price';
            amount = employeeData[price_name];
            // convrt_amount.val(amount);
            pro_amount.val(amount);
            console.log('PRICE ' + amount);
            //total_amount.val(amount * quatity_num)
            discount = employeeData[price_name];
            if (discount == null || discount == 0) {
              discount_field.val(0);
              item_discount_max.val(0);
              discount_field.attr('readonly', true);
            } else {
              discount_field.attr("max", discount);
              item_discount_max.val(discount);
            }
            if (category_type == 'service') {
              quatity_num.removeAttr("max");
              quatity_num.removeAttr("min");
            }

          } else {
            quatity_num.attr("max", 0);
            quatity_num.attr("min", 0);
            $("#heading").hide();
            $("#records").hide();
            $("#no_records").show();
          }
          cal_total();
          cal_percentage();
        }
      });
      console.log('cal trig.');

    }
  });

  //discount 1 at a time start

  $(document).on('change keyup', " #total_discount_amount,#total_discount_type,.discount_amount,.discount_type", function() {


    var check = 0;
    var discount = $('#total_discount_type').val();
    var discount_amount = $('#total_discount_amount').val();
    if ((discount == 1 && discount_amount > 0) || (discount == 2 && discount_amount > 0)) {
      check = 1;
    }

    if (check == 1) {

      $("tr.txtMult").each(function() {
        var row = $(this).closest('tr'); // get the row
        var amount = row.find('.discount_amount').attr('max');
        //if (amount <= 0) {
        //  row.remove();
        //} else {
          row.find('.discount_type').val(0);
          row.find('.discount_amount').val(0);
          row.find('.discount_amount').attr('readonly', true);
        //}



      });
      cal_total();
      return false;
    } else {
      $('#total_discount_amount').attr('readonly', true);
      $("tr.txtMult").each(function() {
        var row = $(this).closest('tr'); // get the row
        row.find('.discount_amount').removeAttr('readonly', true);


      });

    }

    var check = 0;
    $("tr.txtMult").each(function() {
      var row = $(this).closest('tr'); // get the row
      var discount = row.find('.discount_type').val();
      var discount_amount = row.find('.discount_amount').val();
      if ((discount == 1 && discount_amount > 0) || discount == 2 && discount_amount > 0) {
        check = 1;
      }
    });
    // console.log(check);
    if (check == 1) {

      $('#total_discount_amount').attr('readonly', true);
      $('#total_discount_type').val(0);
      $('#total_discount_amount').val(0);

    } else {
      $("#total_discount_amount").removeAttr('readonly');
      $("tr.txtMult").each(function() {
        var row = $(this).closest('tr'); // get the row
        // row.find('.discount_amount').attr('readonly', true);
      });

    }
    cal_total();
  });



  //discount 1 at a time end


  $(document).on('keydown keypress keyup change blur', ".total_discount_amount, .total_discount_type,.quotation_buypack_discount, .quotation_tax", function() {

    cal_discount_amount();
    cal_percentage();


  });

  

  // $(document).on('keydown keypress keyup change blur', ".total_discount_amount", function() {
  //    var check = 0;
  //     $("tr.txtMult").each(function() {
  //         var row = $(this).closest('tr'); // get the row
  //         var discount = row.find('.discount_type').val();

  //         if (discount) {
  //             check = 1;
  //         }
  //     });
  //     // console.log(check);
  //     var check2 = 0;
  //     if (check != 1) {

  //       $("tr.txtMult").each(function() {


  //             var row = $(this).closest('tr');
  //             var discount_field = row.find('.discount_amount').val();

  //             if(discount_field == 0)
  //             {
  //               console.log('Abbas');
  //             }
  //             var quatity_num = row.find('.quatity_num');
  //             var rate = $(this).closest('tr').find('.rate_num');
  //             var pro_amount = row.find('.pro_amount');

  //       });
  //     }


  // });


  //$(document).on('change', ".txtMult input",function () { 
  $(document).on('keydown keypress keyup change blur', ".txtMult input,.txtMult select", function() {
    cal_total();
    cal_percentage();

  });


  $(document).on('click', ".remCF", function() {
    cal_total();
    cal_percentage();
    var mult = 0;
    // for each row:
    /* var quotation_buypack_discount = $('#quotation_buypack_discount').val();
     var quotation_tax = $('#quotation_tax').val();

     $("tr.txtMult").each(function() {
         // get the values from this row:
         var $quatity_num = $('.quatity_num', this).val();
         if ($quatity_num == undefined) {
             $quatity_num = ($quatity_num == undefined) ? 0 : $quatity_num;
         }
         var $rate_num = $('.rate_num', this).val();
         if ($rate_num == undefined) {
             $rate_num = ($rate_num == undefined) ? 0 : $rate_num;
         }else if ($quatity_num == 'lot' || $quatity_num == 'Lot') {
             $quatity_num = ($quatity_num == 'lot' || $quatity_num == 'Lot') ? 1 : $quatity_num;
         }

         var $discount_type = $('.discount_type', this).val();
         var $discount_amount = $('.discount_amount', this).val();
         if ($discount_type != '') {
             $discount_amount = ($discount_amount != '') ? $discount_amount : 0;
         }
         // console.log("discount_type "+$discount_type);
         // console.log("discount_amount "+$discount_amount);

         if ($discount_type == 1) {

             if ($discount_amount != '') {
                 if ($discount_amount <= 100) {
                     $discount_amount = ($discount_amount / 100) * ($quatity_num * 1) * ($rate_num * 1);
                 } else {
                     $discount_amount = ($quatity_num * 1) * ($rate_num * 1);
                     $('.discount_amount', this).val(100);
                     // return false;
                 }
             }
         } else if ($discount_type == 2) {
             if ($discount_amount != '') {
                 $discount_amount = $discount_amount;
             }
         } else {
             $('.discount_amount', this).val(0);
             $discount_amount = 0;
         }
         // console.log("discount_type2 "+$discount_type);
         // console.log("discount_amount2 "+$discount_amount);
         var $total = ($quatity_num * 1) * ($rate_num * 1) - $discount_amount;
         if ($total < 0) {
             $total = 0;
             $('.discount_amount', this).val(($quatity_num * 1) * ($rate_num * 1));
         }
         // else{
         //   console.log("else");
         //   $total = 0;
         // }
         if($('.quotation_detail_optional', this).prop("checked") == true){
           //console.log("optional "+$quatity_num);
         }else{
           $('.multTotal', this).val(round_value($total)).text(round_value($total));
           mult += $total;
         }
     });
     $("#subtotal").val(round_value(mult)).text(round_value(mult));

     var $subtotal = mult;
     var $total_discount_amount = $('.total_discount_amount').val();
     var $total_discount_type = $('.total_discount_type').val();
     if ($total_discount_type != '' && $('.quotation_detail_optional', this).prop("checked") == false) {
         $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
     }

     if ($total_discount_type == 1) {
         if ($total_discount_amount != '') {
             if ($total_discount_amount <= 100) {
                 $total_discount_amount = ($total_discount_amount / 100) * $subtotal;
             } else {

                 $total_discount_amount = $subtotal;
                 $('.total_discount_amount').val(100);
                 // return false;
             }
         }
     } else if ($total_discount_type == 2) {
         if ($total_discount_amount != '') {
             $total_discount_amount = $total_discount_amount;
         }
     } else {
         $('.total_discount_amount').val(0);
         $total_discount_amount = 0;
     }

     var $grandtotal = $subtotal - $total_discount_amount;

     if ($grandtotal < 0) {
         $grandtotal = 0;
         $('.total_discount_amount').val(round_value($subtotal));
         $('.quotation_buypack_discount').val($grandtotal);
         quotation_buypack_discount = $grandtotal;
     }
     //$('#grandtotal').val(round_value($grandtotal)).text(round_value($grandtotal));


     if (quotation_buypack_discount != '') {
         if (quotation_buypack_discount <= 100) {
         } else {
             quotation_buypack_discount = 0;
             $('.quotation_buypack_discount').val(0);
         }
     }

     $grandtotal = $grandtotal - quotation_buypack_discount;
     if(quotation_tax != '' && quotation_tax != 0 && quotation_tax < 100){
       var find_tax = ($grandtotal/100)*quotation_tax;
       if(!Number.isNaN(find_tax)){
         $grandtotal = $grandtotal + find_tax;
       }
     }else{
       $('#quotation_tax').val(0);
     }
     $('#grandtotal').val(round_value($grandtotal)).text(round_value($grandtotal));*/
    //$('#grandtotal').val(round_value($grandtotal));
  });

  $(document).on('change', ".prod_name", function() {
    //var prod_name = $(this).parent().parent('tr').find('.prod_name').val();
    //var quotation_detail_id = $(this).parent().parent('tr').find('.quotation_detail_id').val();
    var prod_name = $(this, ".prod_name").val();
    var quotation_detail_id = $(this).closest('tr').find('.quotation_detail_id').val();
    // console.log($(this, ".prod_name").val()+" id");
    var customer = $('#customer_id').val();
    // alert(prod_name);
    var price_plan_id = $("#customer_id").find(':selected').data("price-plan-id");
    var dataString = 'pid=' + prod_name + '&plan_id=' + price_plan_id + '&quotation_detail_id=' + quotation_detail_id;
    var row = $(this).closest('tr'); // get the row
    var street = row.find('.street'); // get the other select in the same row
    var pro_amount = row.find('.pro_amount');
    var quatity_num = row.find('.quatity_num');
    var total_amount = row.find('.multTotal');
    var rate_num = row.find('.rate_num');
    var base_price = row.find('.base_price');
    var usd_price = row.find('.usd_price');
    // var convrt_amount = row.find('.convrt_amount');
    var convert_to = $('input[name="quotation_currency"]:checked').data('title');
    var desc = $(this).closest('tr').find('.prod_desc_text');
    var currency_id = $('input[name="quotation_currency"]:checked').val();
    var discount_field = row.find('.discount_amount');
    var item_discount_max = row.find('.item_discount_max');
    var customer_type_title = $("#customer_id").find(':selected').data("customer-type-title");
    customer_type_title = customer_type_title.toLowerCase();
    var currency_title = $('input[name="quotation_currency"]:checked').data('title');
    currency_title = currency_title.toLowerCase();
    if (currency_title == 'aed') {
      currency_title = 'base';
    }
    // console.log(currency_title);
    var customer_type = $("#customer_id").find(':selected').data("customer-type");


    //street.empty();

    $.ajax({
      url: "<?php echo site_url('quotation/progt'); ?>",
      dataType: "json",
      type: "POST",
      // async: true,
      data: dataString,
      cache: false,
      success: function(employeeData) {
        if (employeeData && employeeData != null) {
          //quatity_num.attr("max", employeeData.total_inventory_quantity);

          street.val(employeeData.product_description);
          var temp = employeeData.product_description;
          desc.html(temp);
          //.replace(/(<([^>]+)>)/ig, "")
          //desc.data('text',temp);
          $(this).closest('tr').next('tr').find('.myModalBtnDesc').attr('data-id', temp);
          // console.log(data['rates']);
          var amount = 0;
          var discount = 0;
          var price_name = '';
          var category_type = employeeData.title.trim().toLowerCase();
          var can_deliver = employeeData.can_deliver;
          // console.log(category_type);

          if (category_type == 'service') {
            quatity_num.val('Lot');
            quatity_num.attr('readonly', true);
            quatity_num.removeAttr("max");
            quatity_num.removeAttr("min");

          } else {
            quatity_num.attr('readonly', false);
            quatity_num.val('');
          }
          price_name = customer_type_title + '_' + currency_title + '_price';
          amount = employeeData[price_name];
          if (can_deliver == 1) {
            rate_num.attr("readonly", false);
          } else {
            //true for hardware
            rate_num.attr("readonly", false);

          }
          // console.log(price_name);
          // console.log(amount);
          pro_amount.val(amount);
          base_price.val(employeeData[customer_type_title + '_base_price']);
          usd_price.val(employeeData[customer_type_title + '_usd_price']);
          total_amount.val(amount);
          price_name = customer_type_title + '_discount';
          discount = employeeData[price_name];
          if (discount == null || discount == 0) {
            discount_field.val(0);
            item_discount_max.val(0);
            discount_field.attr('readonly', true);
          } else {
            discount_field.attr("max", discount);
            item_discount_max.val(discount)
          }


        } else {
          quatity_num.attr("max", 0);
          quatity_num.attr("min", 0);
          $("#heading").hide();
          $("#records").hide();
          $("#no_records").show();
        }
      }
    });

  });



  $(document).ready(function() {

    $("#user_type").change(function() {
      var val = $(this).val();
      if (val == 2) {
        $('.myUserType').slideDown();
      } else {
        $('.myUserType').slideUp();
      }
    });

    $(".datepicker").datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      // startDate: new Date()

    });


    var date = new Date();
    var today = new Date(date.getYear(), date.getMonth(), date.getDate());
    console.log('Date: ' + today);
    $(".datepicker2").datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      // /*startDate: today*/
    });
    var page_title = "<?= $page_title; ?>";
    if (page_title == 'add') {
      $('.datepicker2').datepicker('setDate', new Date());
      $('.datepicker').datepicker().val('');

      var date = new Date();
      var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
      $('.datepicker').datepicker('setDate', today);
      $('.datepicker').trigger('keyup');
      $('.datepicker2').datepicker('setDate', today);
      $('.datepicker2').trigger('keyup');
      $('.datepicker').datepicker().val('');

    }


  });


  var customer_warranty = '';

  function add_terms() {
    var customer_id = $('#customer_id').find(':selected').val();
    var dataString = 'cid=' + customer_id;
    $.ajax({
      url: "<?php echo site_url('quotation/customer_terms'); ?>",
      dataType: "json",
      type: "POST",
      // async: true,
      data: dataString,
      cache: false,
      success: function(customerData) {
        if (customerData) {
          if (customerData['payment_terms'] === undefined) {} else {
            $("#pcustomFields").empty();
            $("#pcustomFields").append(customerData['payment_terms']);
          }

          if (customerData['project_duration'] === undefined) {} else {
            $("#new_customFields").empty();
            $("#new_customFields").append(customerData['project_duration']);
          }
          if (customerData['warranty'] !== undefined) {
            window.customer_warranty = customerData['warranty'];
            if (JSON.parse(window.customer_warranty).length > 0) {
              CKEDITOR.instances['myeditor2'].setData(terms_format('<?= $term_days ?>', customerData['warranty']))
            } else {
              CKEDITOR.instances['myeditor2'].setData(terms_format('<?= $term_days ?>', '<?= $warranty ?>'))

            }
          }
          if (customerData['customer']['user_country'] !== undefined) {
            if (customerData['customer']['user_country'] == window.company_country) {
              $("#quotation_tax").val(parseInt(window.company_tax));
            }
          }
          setTimeout(function() {
            $(".quotation_currency").trigger("change");
          }, 500);


          $("input[name=quotation_currency][value=" + customerData['customer']['currency_id'] + "]").attr('checked', 'checked');
          // if(customerData['warranty_terms'] === undefined){}
          // else
          // {
          //   $("#warn_customFields").empty();
          //   $("#warn_customFields").append(customerData['warranty_terms']);
          // }

          $('.percentage').trigger('keyup');
          $('.project_days').trigger('keyup');
        } else {

        }
      }
    });
  }

  function price_plan_change() {
    var customer = $('#customer_id').val();
    var price_plan_id = $("#customer_id").find(':selected').data("price-plan-id");
    var quotation_validity = $("#customer_id").find(':selected').data("validity-days");
    var quotation_support = $("#customer_id").find(':selected').data("support-days");
    var quotation_warranty = $("#customer_id").find(':selected').data("warranty-years");
    var user_country = $("#customer_id").find(':selected').data("user-country");
    if (company_country == user_country) {
      $('.freight_type').hide();
      $("input[name='quotation_freight_type']").attr("disabled", true);
      $('.hide_tax_row').show();
    } else {
      $('.freight_type').show();
      $('input[name="quotation_freight_type"]').attr("disabled", false);
      $('.quotation_tax').val(0);
      $('.hide_tax_row').hide();
    }

    // $('.quotation_validity').val(quotation_validity);
    //$('.quotation_support').val(quotation_support);
    //$('.quotation_warranty').val(quotation_warranty);


    $("input[name='price_plan_id']").val(price_plan_id);
    $("#customFields tr.txtMult").each(function() {

      var prod_name = $('.prod_name', this).val();
      var quotation_detail_id = $('.quotation_detail_id', this).val();
      var dataString = 'pid=' + prod_name + '&plan_id=' + price_plan_id + '&quotation_detail_id=' + quotation_detail_id;
      var row = $(this).closest('tr'); // get the row
      var street = row.find('.street'); // get the other select in the same row
      var pro_amount = row.find('.pro_amount');
      var quatity_num = row.find('.quatity_num');
      var base_price = row.find('.base_price');
      var usd_price = row.find('.usd_price');
      // var convrt_amount = row.find('.convrt_amount');
      var convert_to = $('input[name="quotation_currency"]:checked').data('title');
      var desc = $(this).closest('tr').find('.prod_desc_text');
      var currency_id = $('input[name="quotation_currency"]:checked').val();
      var discount_field = row.find('.discount_amount');
      var item_discount_max = row.find('.item_discount_max');
      var customer_type_title = $("#customer_id").find(':selected').data("customer-type-title");
      customer_type_title = customer_type_title.toLowerCase();
      var currency_title = $('input[name="quotation_currency"]:checked').data('title');
      currency_title = currency_title.toLowerCase();
      if (currency_title == 'aed') {
        currency_title = 'base';
      }
      $.ajax({
        url: "<?php echo site_url('quotation/progt'); ?>",
        dataType: "json",
        type: "POST",
        // async: true,
        data: dataString,
        cache: false,
        success: function(employeeData) {
          if (employeeData && employeeData != null) {
            // console.log(employeeData);
            //quatity_num.attr("max", employeeData.total_inventory_quantity);
            quatity_num.attr("min", 0);

            var category_type = employeeData.title.trim().toLowerCase();

            if (category_type == 'service') {
              quatity_num.removeAttr("max");
              quatity_num.removeAttr("min");
            }

            street.val(employeeData.product_description);
            var temp = employeeData.product_description;
            if (desc.html() == null || desc.html() == '') {
              desc.html(temp /*.replace(/(<([^>]+)>)/ig, "")*/ );
            }
            //desc.data('text',temp);
            $(this).closest('tr').next('tr').find('.myModalBtnDesc').attr('data-id', temp);
            // console.log(data['rates']);
            var amount = 0;
            var amount = 0;
            var discount = 0;
            var price_name = '';
            var category_type = employeeData.title.trim().toLowerCase();
            price_name = customer_type_title + '_' + currency_title + '_price';
            amount = employeeData[price_name];
            base_price.val(employeeData[customer_type_title + '_base_price']);
            usd_price.val(employeeData[customer_type_title + '_usd_price']);
            // convrt_amount.val(amount);
            pro_amount.val(amount);
            var price_name = customer_type_title + '_discount';
            discount = employeeData[price_name];
            if (discount == null || discount == 0) {
              discount_field.val(0);
              item_discount_max.val(0);
              discount_field.attr('readonly', true);
            } else {
              discount_field.attr("max", discount);
              item_discount_max.val(discount);
            }

            $('.txtMult input').trigger('keyup');

          } else {
            quatity_num.attr("max", 0);
            quatity_num.attr("min", 0);
            $("#heading").hide();
            $("#records").hide();
            $("#no_records").show();
          }
        }
      });
    });
  }

  function cal_discount_amount() {
    var quotation_buypack_discount = $('#quotation_buypack_discount').val();
    var quotation_tax = $('#quotation_tax').val();
    var $subtotal = $("#subtotal").val()
    var $total_discount_amount = $('.total_discount_amount').val();
    var $total_discount_type = $('.total_discount_type').val();
    if ($total_discount_type != '') {
      $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
    }

    if ($total_discount_type == 1) {
      if ($total_discount_amount != '') {
        if ($total_discount_amount <= 100) {
          $total_discount_amount = ($total_discount_amount / 100) * $subtotal;
        } else {

          $total_discount_amount = $subtotal;
          $('.total_discount_amount').val(100);
          // return false;
        }
      }
    } else if ($total_discount_type == 2) {
      if ($total_discount_amount != '') {
        $total_discount_amount = $total_discount_amount;
      }
    } else {
      $('.total_discount_amount').val(0);
      $total_discount_amount = 0;
    }
    $total_discount_amount = round_value($total_discount_amount);
    $('#total_discount_amount_val').val($total_discount_amount.toFixed(2));
    var $grandtotal = round_value($subtotal - $total_discount_amount);
    console.log("Net Amount: "+$grandtotal);
    // if ($total_discount_type == 1) {
    if (quotation_buypack_discount != '') {
      quotation_buypack_discount = quotation_buypack_discount;
      if (quotation_buypack_discount <= 100) {
        // quotation_buypack_discount = (quotation_buypack_discount / 100) * $grandtotal;
      } else {

        //quotation_buypack_discount = $grandtotal;
        // $('.quotation_buypack_discount').val(100);
        // return false;
      }
    }
    // } else if ($total_discount_type == 2) {
    //     if (quotation_buypack_discount != '') {
    //         quotation_buypack_discount = quotation_buypack_discount;
    //     }
    // } else {
    //     $('.quotation_buypack_discount').val(0);
    //     quotation_buypack_discount = 0;
    // }
    if ($grandtotal < 0) {
      $grandtotal = 0;
      console.log("buy back working " + $grandtotal);
      $('.quotation_buypack_discount').val($grandtotal.toFixed(2));
    }

    $grandtotal = round_value($grandtotal - quotation_buypack_discount);
    if (quotation_tax != '' && quotation_tax != 0 && quotation_tax < 100) {
      var find_tax = ($grandtotal / 100) * quotation_tax;
      find_tax = round_value(find_tax);
      if (!Number.isNaN(find_tax)) {
        $("#quotation_tax_val").val(find_tax.toFixed(2));
        $grandtotal = $grandtotal + find_tax;
        //$grandtotal = round_value($grandtotal);
        $grandtotal = Math.round( $grandtotal * 1e2 ) / 1e2;
      }
    } else {
      $('#quotation_tax').val(0);
    }
    //Jd
    $('#grandtotal').val($grandtotal.toFixed(2));
    console.log("Jd Total :s" + $grandtotal);
   // $('#grandtotal').val($grandtotal);

  }
  function cal_percentage() {
    var total_amount = $('#grandtotal').val();
    var total = 0;
    $("#pcustomFields tr.txtMult").each(function() {

      var row = $(this).closest('tr'); // get the row
      var percentage = row.find('.percentage').val();

      percentage = (percentage == undefined || percentage == '') ? 0 : percentage;
      percentage = percentage / 100;
      var amount = percentage * total_amount;
      row.find('.payment_amount').val(amount.toFixed(2));
      amount = (percentage * total_amount == undefined || percentage * total_amount == '') ? 0 : percentage * total_amount;
      total += amount;
    });
    // console.log('Grand Total:' + total);
    $('input[name="payment_terms_total"]').val(Number(round_value(total_amount)).toFixed(2));
  }

  function cal_total() {
    var check = $(this).attr('data-optional');
    var mult = 0;
    // alert(123);
    var total_disount_check = 0;
    var $total_discount_amount = $('.total_discount_amount').val();
    var $total_discount_type = $('.total_discount_type').val();
    $("#customFields tr.txtMult").each(function() {
      // console.log("loop");
      // get the values from this row:

      var $quatity_num = $('.quatity_num', this).val();
      if ($quatity_num == undefined) {
        $quatity_num = ($quatity_num == undefined) ? 0 : $quatity_num;
      } else if ($quatity_num == 'lot' || $quatity_num == 'Lot') {
        $quatity_num = ($quatity_num == 'lot' || $quatity_num == 'Lot') ? 1 : $quatity_num;
      }
      var $rate_num = $('.rate_num', this).val();
      if ($rate_num == undefined) {
        $rate_num = ($rate_num == undefined) ? 0 : $rate_num;
      }

      var $discount_type = $('.discount_type', this).val();
      var $discount_amount = $('.discount_amount', this).val();
      var $max_percentage = $('.item_discount_max', this).val();
      //check for total discount 
      if ($total_discount_type != '' && $total_discount_amount != '' && $('.quotation_detail_optional', this).prop("checked") == false) {
        var new_discount = 0;
        var check_this_total = ($quatity_num * 1) * ($rate_num * 1);
        new_discount = check_this_total / 100 * $max_percentage;
        total_disount_check = parseFloat(total_disount_check) + parseFloat(new_discount);
        
      }
      if ($discount_type != '') {
        $discount_amount = ($discount_amount != '') ? $discount_amount : 0;
      }
      // console.log("discount_type "+$discount_type);
      // console.log("discount_amount "+$discount_amount);

      if ($discount_type == 1) {
        $('.discount_amount', this).attr('max', $max_percentage);
        if ($discount_amount != '') {
          if ($discount_amount <= 100) {
            $discount_amount = ($discount_amount / 100) * ($quatity_num * 1) * ($rate_num * 1);
          } else {
            $discount_amount = ($quatity_num * 1) * ($rate_num * 1);
            $('.discount_amount', this).val(100);
            // return false;
          }
        }
      } else if ($discount_type == 2) {
        var this_total = ($quatity_num * 1) * ($rate_num * 1);
        var this_amount = this_total / 100 * $max_percentage;
        $('.discount_amount', this).attr('max', this_amount);
        if ($discount_amount != '') {
          if ($discount_amount > this_amount || ($discount_amount == 0 && this_amount == 0)) {
            $('.discount_amount', this).val(0);
          } else {
            $discount_amount = $discount_amount;
          }
        }
      } else {
        $('.discount_amount', this).val(0);
        $discount_amount = 0;
      }
      
      $discount_amount = ($discount_amount);
      console.log("discount_type2 "+$discount_amount);
      var $total = ($quatity_num * 1) * ($rate_num * 1) - $discount_amount;
      $total = round_value($total);
      if ($total < 0) {
        $total = 0;
        var temp = round_value(($quatity_num * 1) * ($rate_num * 1));
        $('.discount_amount', this).val(temp.toFixed(2));
      }
      // else{
      //   console.log("else");
      //   $total = 0;
      // }

      $('.multTotal', this).val($total.toFixed(2)).text($total.toFixed(2));

      /* var find_percent = ($discount_amount/this_total)*100;
       if(find_percent > $max_percentage){
         console.log("greater than");
       }else{
         console.log("less than");

       }
       console.log('max '+$max_percentage+'  find '+find_percent);
       console.log('formula =tot '+this_total+'  div/100 '+this_total/100+' dis_amount '+$discount_amount+' = '+find_percent);*/
      if ($('.quotation_detail_optional', this).prop("checked") == true) {
        //console.log("optional "+$quatity_num);
      } else {
        mult += $total;
      }

    });

    // console.log("loop");
    //$('#customer_id').valid()
    mult = round_value(mult);
    $("#subtotal").val(mult.toFixed(2)).text(mult.toFixed(2));

    var $subtotal = mult;

    if ($total_discount_type != '') {
      $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
    } else {
      $('#total_discount_amount').removeAttr('max');
    }
    if ($total_discount_amount != '') {
      var validate_total_amount = 0;
      if ($total_discount_type == 2) {
        validate_total_amount = total_disount_check;
      } else if ($total_discount_type == 1) {
        validate_total_amount = (total_disount_check / $subtotal) * 100;
      }
      //console.log(total_disount_check);
      $('.total_discount_amount').attr('max', Math.round(parseFloat(validate_total_amount)));
      $('#total_discount_amount').valid();
    }

    if ($total_discount_type == 1) {
      if ($total_discount_amount != '') {
        if ($total_discount_amount <= 100) {
          $total_discount_amount = ($total_discount_amount / 100);
        } else {

          $total_discount_amount = $subtotal;
          $('.total_discount_amount').val(100);
          // return false;
        }
      }
    } else if ($total_discount_type == 2) {
      if ($total_discount_amount != '') {
        $total_discount_amount = $total_discount_amount;
      }
    } else {
      $('.total_discount_amount').val(0);
      $total_discount_amount = 0;
    }
    
    var $grandtotal = $subtotal - $total_discount_amount;

    if ($grandtotal < 0) {
      $grandtotal = 0;
      $('.total_discount_amount').val($subtotal);
    }
    $grandtotal = Math.round( $grandtotal * 1e2 ) / 1e2;
    console.log("Jd Total : " + $grandtotal );
    //jd
    //$('#grandtotal').val(round_value($grandtotal)).text(round_value($grandtotal));
    $('#grandtotal').val($grandtotal).text($grandtotal);


    cal_discount_amount();
  }

  function round_value(num) {
    var with2Decimals = 0.00;
     if (!isNaN(num) && num != "") {
       with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
     }
     
     return parseFloat(with2Decimals);
  }

  function check_hendler(id) {
    var box = $("#" + id);
    if (box.prop("checked") == true) {
      $("#" + id + "_hidden").val(1);
    } else if (box.prop("checked") == false) {
      $("#" + id + "_hidden").val(0);
    }
  }

  $(".get_terms_data").click(function() {
    var customer = $('#customer_id').valid();
    if (!customer) {
      window.scrollTo(0, 0);
      return false;

    }
    var get_warranty = '';
    if (window.customer_warranty != '' && JSON.parse(window.customer_warranty).length > 0) {
      get_warranty = window.customer_warranty;
    } else {
      get_warranty = '<?= $warranty ?>';
    }
    var extra = "";
    extra = "<strong>Date: " + $('#quotation_date').val() + "</strong><br>";

    var customer = $('#customer_id').find(':selected').data("name");
    var customer_address = $("#customer_id").find(':selected').data("address");
    var customer_city = $("#customer_id").find(':selected').data("city");
    var customer_country = $("#customer_id").find(':selected').data("country");

    extra += "<strong>" + customer + customer_address + customer_city + ',' + customer_country + "</strong>";

    var cover = <?= json_encode($setval['quotation_cover']); ?>;
    CKEDITOR.instances['myeditor'].setData(extra + cover);
    CKEDITOR.instances['myeditor2'].setData(terms_format(1, get_warranty));
  });
  $(document).ready(function() {
    var page_title = "<?= $page_title; ?>";
    if (page_title == 'add') {
      CKEDITOR.instances['myeditor2'].setData(terms_format('<?= $term_days ?>', '<?= $warranty ?>'));
    }
  });

  function terms_format(option, data) {
    var support = 1;
    var validity = 30;
    var warranty = 1;
    if (typeof option !== 'undefined' && option == 1) {
      validity = $('.quotation_validity').val();
      support = $('.quotation_support').val();
      warranty = $('.quotation_warranty').val();
    } else if (typeof option !== 'undefined' && typeof option != '' && typeof JSON.parse(option) === 'object') {
      option = JSON.parse(option);
      validity = option.validity;
      support = option.support;
      warranty = option.warranty;
    }
    var html = '<p><strong>Validity</strong> : ' + validity + ' days from the Date of the document</p>';
    html += '<strong>Inclusions &amp; Exclusion</strong> :';
    html += '<br/><u>The cost of the project includes the following:</u>';
    html += '<ul>';
    html += '<li>Supply, Installation &amp; Testing</li>';
    html += '<li>User Training</li>';
    html += '<li>Minor Software Modifications (if any)</li>';
    html += '</ul>';
    html += '<u>The cost of the project DOES NOT include the following:</u>';
    html += '<br/>Windows OS (Wherever Applicable)';
    html += '<br/>Civil / Carpentry / Masonry / Electrical Work (if any)';
    html += '<br/>Nearest Network (Ethernet / Wi-Fi) Points - wherever required';
    html += '<p><strong>Support</strong> :';
    html += ' ' + support + ' days Online, Telephonic &amp; On-Site Support will be provided</p>';
    html += '<p><strong>Warranty</strong> :';
    html += ' ' + warranty + ' days Comprehensive Warranty for all Manufacturing Defects</p>';
    if (typeof data !== 'undefined') {
      data = JSON.parse(data);
      $.each(data, function(i) {
        var j = i;
        html += '<p><strong><u>Option-' + (j + 1) + ' : ' + data[i]['title'] + '</u></strong>';
        html += '<br/>' + data[i]['percentage'] + '% of the Project Value per annum</p>';
        //html += '<p>'+data[i]['percentage']+'% of the Project Value per annum</p>';            
      });
    }
    /*html += '<p><strong><u>Option-2 : Service</u></strong></p>';
    html += '<p>15% of the Project Value per annum</p>';
    html += '<p><strong><u>Option-3 : Hardware</u></strong></p>';
    html += '<p>10% of the Project Value per annum</p>';
    html += '<p><strong><u>Option-4 : test</u></strong></p>';
    html += '<p>20% of the Project Value per annum</p>';*/
    return html;
  }
  $(document).on('keypress', ".quatity_num, .discount_amount, .rate_num", function(e) {
    var x = e.which || e.keycode;
    if ((x >= 48 && x <= 57)) {
      //console.log(true);
      return true;
    } else {
      e.preventDefault();
      //console.log(false);
      return false;
    }
  });

  const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }
</script>