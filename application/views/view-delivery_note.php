  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
      </ul>
      <?php
      $check_add = false;
      $check_edit = false;
      $check_print = false;
      $check_status = false;
      $check_payment = false;
      $check_history = false;
      if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
          if ($v['module_id'] == 11) {
            if ($v['add'] == 1) {
              $check_add = true;
            }
            if ($v['edit'] == 1) {
              $check_edit = true;
            }
            if ($v['print'] == 1) {
              $check_print = true;
            }
            if ($v['status'] == 1) {
              $check_status = true;
            }
            if ($v['payment'] == 1) {
              $check_payment = true;
            }
            if ($v['history'] == 1) {
              $check_history = true;
            }
          }
        }
      } else {
        $check_add = true;
        $check_edit = true;
        $check_print = true;
        $check_status = true;
        $check_payment = true;
        $check_history = true;
      }
      ?>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
      <!--</div>-->
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span> <span class="fa fa-sort-desc"></spam>
              </h4>
              <?php if ($check_add) { ?>
                <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('delivery_note/add'); ?>">Add</a>
              <?php } ?>
              <!--<div class="pull-right">-->
              <!--  <a href="<?php echo site_url($add_product) ?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>-->
              <!--  <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>-->
              <!--  <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>-->
              <!--  <ul class="dropdown-menu">-->
              <!--    <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>-->
              <!--    <li class="divider"></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>-->
              <!--    <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>-->
              <!--  </ul>-->
              <!--  <span class="h-seperate"></span>-->
              <!--  <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>-->
              <!--</div>-->
            </div>
            <div class="grid-body ">
              <div class="row">
                <form class="validate">
                  <div class="col-md-3 col-lg-3">
                    <div class="row">
                      <div class="col-md-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label for="checkbox1"><b>From Date</b></label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-8 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3 col-lg-3">
                    <div class="row">
                      <div class="col-md-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label for="checkbox1"><b>To Date</b></label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-8 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3 col-lg-3">
                  <div class="row">
                    <div class="col-md-3 col-lg-3">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Customer</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                      <i class=""></i>
                      <select name="customer_id" id="customer_id" class="select2 form-control">
                        <option value="0" selected disabled >--- Select Customer ---</option>
                        <?php
                        foreach($customerData as $k => $v){
                        ?>
                        <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['customer_id'])? 'selected': ''; ?> >
                        <?= $v['user_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3 col-lg-3">
                  <div class="row">
                    <div class="col-md-4 col-lg-4">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>PI / Invoice #</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-lg-8">
                      <i class=""></i>
                      <input type="text" class="form-control" name="invoice_no" id="invoice_no"> 
                    </div>
                  </div>
                </div>

                  <div class="col-md-2 col-lg-2">
                    <div class="row">
                      <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                          <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                </form>
              </div>
              <table class="table" id="custom_table">
                <thead>
                  <tr>
                    <th>Sr.No.</th>
                    <th>Delivery Note #</th>
                    <th>Customer Name</th>
                    <th>P.I/Invoice #</th>
                    <th>Date</th>
                    <th width="200px">Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach (@$data as $v) {

                  ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo @$setting["company_prefix"] . @$setting["delivery_prfx"]; ?><?php echo $v['delivery_note_no'] ?></td>
                        <td><?php echo $v['user_name'] ?></td>
                        <td>
                          <?php if ($v['invoice_id'] != 0) { ?>
                            <?= ($v['invoice_type'] == 1) ? @$setting["company_prefix"] . @$setting["quotation_prfx"] : @$setting["company_prefix"] . @$setting["invoice_prfx"]; ?><?php echo $v['invoice_id'] . find_rev_no($v['invoice_id']); ?>
                          <?php } ?>
                        </td>
                        <td><?php echo date("Y-m-d", strtotime($v["delivery_date"])) ?></td>

                        <td class="">
                          <!-- <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['delivery_note_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a> -->
                          <a href="<?php echo site_url($detail_page . '/' . @$v['delivery_note_id']) ?>" class="btn-primary btn btn-sm" title="View Detail"><i class="fa fa-eye"></i></a>

                          <?php
                          if ($check_edit) {
                            if ($v['is_edit'] == 0) {
                              if ($v["invoice_final"] != 1) {
                          ?>
                                <a href="<?php echo site_url($edit_product . '/' . @$v['delivery_note_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                              <?php
                              }
                              if ($v["invoice_final"] != 1) { ?>
                                <a href="<?php echo site_url($delete_product . '/' . @$v['delivery_note_id']) ?>" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm"><i class="fa fa-times"></i></a>
                              <?php } ?>

                          <?php
                            }
                          } ?>
                          <?php if ($check_print) { ?>
                            <div class="btn-group mt-2">
                              <button class="btn btn-white btn-demo-space"> <i class="fa fa-print"></i></button>
                              <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                              <ul class="dropdown-menu">
                                <li><a class="page_reload" href="<?= site_url($print_delivery . '?id=' . @$v['delivery_note_id'] . '&header_check=0') ?>" target='_blank'>With Header</a></li>
                                <li class="divider"></li>
                                <li><a class="page_reload" href="<?= site_url($print_delivery . '?id=' . @$v['delivery_note_id'] . '&header_check=1') ?>" target='_blank'>Without Header</a></li>
                              </ul>
                            </div>
                          <?php } ?>
                          <!--  <a href="<?php echo site_url($print_delivery . '?id=' . @$v['delivery_note_id']) ?>" target="_blank" class="btn btn-white" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></a> -->
                        </td>
                      </tr>

                  <?php
                      $count++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
  </div>
  <script>
    $(document).ready(function() {
      var custom_table = $('#custom_table').DataTable();
      $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
      $(document).on("click", "#fetch", function() {

        var from_date = $('#from_date').val();
        if (from_date == '') {
          from_date = 0;
        }

        var to_date = $('#to_date').val();
        if (to_date == '') {
          to_date = 0;
        }

        var customer_id = $('#customer_id option:selected').val();
        var invoice_no = $('#invoice_no').val();

        $.ajax({
          url: "<?php echo site_url('delivery_note/search_view'); ?>",
          dataType: "json",
          type: "POST",
          data: {
            from_date: from_date,
            to_date: to_date,
            customer_id:customer_id,
            invoice_no:invoice_no
          },
          cache: false,
          success: function(result) {

            custom_table.clear().draw();
            var count = 0;
            for (var i = 0; i < result.data.length; i++) {
              count++;
              var html = '';
              html += '<tr>';

              html += '<td>';
              html += count;
              html += '</td>';

              html += '<td>';
              html += "<?= @$setting["company_prefix"] . @$setting["delivery_prfx"] ?>" + result.data[i].delivery_note_no;
              html += '</td>';

              html += '<td>';
              html += result.data[i].user_name;
              html += '</td>';

              html += '<td>';
              if(result.data[i].invoice_id != 0)
              {
                if(result.data[i].invoice_type == 1)
                {
                  html += "<?= @$setting["company_prefix"] . @$setting["quotation_prfx"] ?>" ;
                }else
                {
                  html += "<?= @$setting["company_prefix"] . @$setting["invoice_prfx"] ?>" ;
                }
                html += result.data[i].invoice_id+result.data[i].rev_no;              
              }
              html += '</td>';

              
              html += '<td>';
              html += result.data[i].delivery_date;
              html += '</td>';

              html += '<td >';
              html += '<a href="<?php echo site_url($detail_page . "/") ?>' + result.data[i].delivery_note_id + '" class="btn-primary btn btn-sm mr-1"  title="View Detail"><i class="fa fa-eye"></i></a>';
              html += '<?php if ($check_edit) { ?>';
                if(result.data[i].is_edit == 0)
                {
                  if(result.data[i].invoice_final != 1)
                  {
                    html += '<a href="<?php echo site_url($edit_product . "/") ?>' + result.data[i].delivery_note_id + '" class="btn-warning btn btn-sm mr-1" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                    html += '<a href="<?php echo site_url($delete_product . "/") ?>' + result.data[i].delivery_note_id + '" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm mr-1"><i class="fa fa-times"></i></a>';
                  }
                }
              html += '<?php } ?>';

              html += '<?php if ($check_print) { ?>';
              html += '<div class="btn-group mt-2">';
              html += '  <button class="btn btn-white btn-demo-space"> <i class="fa fa-print"></i></button>';
              html += '  <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>';
              html += '  <ul class="dropdown-menu">';
              html += '    <li><a class="page_reload" href="<?php echo site_url($print_delivery . "?id=") ?>' + result.data[i].delivery_note_id + '&header_check=0" target="_blank">With Header</a></li>';
              html += '    <li class="divider"></li>';
              html += '    <li><a class="page_reload" href="<?php echo site_url($print_delivery . "?id=") ?>' + result.data[i].delivery_note_id + '&header_check=1" target="_blank">Without Header</a></li>';
              html += '  </ul>';
              html += '</div>';
              html += '<?php } ?>';

              html += '</td>';

              html += '</tr>';
              custom_table.row.add($(html)).draw(false);
            }
            // $('#custom_row').append(html);
          }
        });

      });
    });
  </script>