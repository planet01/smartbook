
<style>
.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}

</style>
   
   <div class="div-controls div-font-controls" style="text-align:center;padding-top:-12%;">
      <h3 class="main-heading">Smart Matrix General Trading LLC</h3>
      <br>
      <h4 class="main-second-heading">Account Transaction Detail</h4>
      <br>
      <h4 class="main-second-heading">From Date: <?= $from_date?> To Date: <?= $to_date?></h4>
      <br>
   </div>

   

   <table style="width:100%;border-collapse: collapse;border-top:2px solid #000;border-bottom:2px solid #000;margin-bottom:15%;" class="table_align"> 
      <!-- first record -->
      <thead>
      <tr>
         <th align="left" width="60px" style="padding:5px 0 5px 10px;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
         <th align="left" width="75px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Date</b></th>
         <th align="left" width="75px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Account #</b></th>
         <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Account Name</b></th>
         <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Cheque #</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Debit</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Credit</b></th>
      </tr>
      </thead>
      <?php
         $total_debit = 0;
         $total_credit = 0;
         foreach ($voucher_data as $vd) {
            
             ?>
             <tr>
                 <td style="border-bottom:2px solid #000;border-top:2px solid #000;" colspan="7">Transaction #:<?= $vd['TransactionNo']?> </td>
             </tr>
             <?php
             $i = 0;
            foreach ($data as $d) {
             if($vd['voucher_id'] == $d['voucher_id']){
                $i++;
         ?>
            
            <tr>
                <td align="left" style="padding:5px 0 5px 10px;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $i; ?></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><b><?= $d['Transaction_Date']?></b></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $d['AccountNo']?></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $d['AccountDesc']?></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $d['Cheque_No']?></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><b><?= ($d['Amount_Dr'] > 0)?number_format((float)$d['Amount_Dr'], 2, '.', ''):''?></b></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= ($d['Amount_Cr'] > 0)?number_format((float)$d['Amount_Cr'], 2, '.', ''):''?></td>
            </tr>
            
         <?php 
            $total_debit += $d['Amount_Dr'];
            $total_credit += $d['Amount_Cr'];
             }
            }
        }
         ?>
        <tr>
            <td style="border-top:2px solid #000;" colspan="5">Grand Total</td>
            <td style="border-top:2px solid #000;"><?= number_format((float)$total_debit, 2, '.', '')?></td>
            <td style="border-top:2px solid #000;"><?= number_format((float)$total_credit, 2, '.', '')?></td>
        </tr>
      
   </table>
   
  