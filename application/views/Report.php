

          <table>

            <tr>

                <td width="40px"></td>

                <td ><h3>Ref. No.: <span style="border-bottom: 1px solid black;">SMGT/S/1903-022R</span></h3></td>

            </tr>

            <tr>

                <td width="40px"></td>

                <td><h3>Date: March 21, 2019.</h3></td>

            </tr>



           <br>



            <tr>

                <td width="40px"></td>

                <td><h3>Mr. XYZ</h3></td>

            </tr>

            <tr>

                <td width="40px"></td>

                <td><h3>Company Name</h3></td>

            </tr>

            <tr>

                <td width="40px"></td>

                <td><h3><span style="border-bottom: 1px solid black;">Abu-Dhabi, U.A.E.</span></h3></td>

            </tr>



            <br>



            <tr>

                <td width="40px"></td>

                <td><h3>Sub:  <span style="border-bottom: 1px solid black;">Annual Maintenance Contract Proposal for</span> <span style="color:red; border-bottom: 1px solid red;">(Product Name i.e., Queue management system etc..)</span> </h3></td>

            </tr>

            

            <br>

            <tr>

                <td width="40px"></td>

                <td><h3>Dear Sir, </h3></td>

            </tr>

            <br>

            <tr>

                <td width="40px"></td>

                <td width="700px" style="text-align: justify; text-justify: inter-word;">

                  With reference to your recent inquiry regarding the subject matter, 

            please find below the details as well as our best prices that we believe will suit all your requirements 

            and will provide you all the required output as per your satisfaction.

                </td>

            </tr>

            <br>

            <tr>

                <td width="40px"></td>

                <td width="700px" style="text-align: justify; text-justify: inter-word;">

                  We are submitting the attached proposal with all the details along with the cost break-up of the system to 

            give you a clear idea. We hope you will find our proposal and our services the most competitive in terms of Price, Quality & Service.

                </td>

            </tr>

            <br>

            <tr>

                <td width="40px"></td>

                <td width="700px" style="text-align: justify; text-justify: inter-word;"> We would like to thank you for considering us for the current requirements and also hope to be in contact and serve you in the future.</td>

            </tr>

            <br>

            <tr>

                <td width="40px"></td>

                <td width="700px" style="text-align: justify; text-justify: inter-word;"> Should you require any clarification and / or any other information, please do not hesitate to contact us anytime at your convenience.</td>

            </tr>

            <br><br>

            <tr>

                <td width="40px"></td>

                <td><h3>Thanks & Regards,</h3></td>

            </tr>

            <br>

            <br>

            <br>

            <tr>

              <td width="40px"></td>

              <td><h3>Sales Person Name</h3></td>

            </tr>

            <tr>

              <td width="40px"></td>

              <td><h3>Position</h3></td>

            </tr>

            <tr>

              <td width="40px"></td>

              <td><h3>primary number</h3></td>

            </tr>

            <tr>

              <td width="40px"></td>

              <td><h3>Secondary number</h3></td>

            </tr>

            <tr>

              <td width="40px"></td>

              <td><h3>email address</h3></td>

            </tr>



          </table>

      

          

          

       

    <pagebreak/>



          <table>

            <tr>

              <td width="40px" ></td>

              <td width="730px"  style="text-align:center; background-color:#a6a6a6;"><h2>Financial Details</h2></td>

            </tr>

            

            <tr>

              <td colspan="2" style="text-align:center; "><h3 style="border-bottom: 1px solid black;">EaZy-Q <span style="color:red;">(Canada)</span> – Queue Management System -product name-</h3></td>

            </tr>

            <br>

            <tr>

              <td colspan="2" style="text-align:center;color:blue;"><h3>Annual Maintenance Proposal</h3></td>

            </tr>

            <tr>

              <td colspan="2" style="text-align:center;color:blue;"><h3>(From March 25, 2019 to March 24, 2020)</h3></td>

            </tr>

          </table>

          <br><br>

          <table style="border-collapse: collapse;">

            <thead>

              <tr>

                <th width="80px" height="30px"></th>

                <th  style="border: 1px solid black; background-color:#c4bb98;" height="30px">S.N.</th>

                <th style="border: 1px solid black; background-color:#c4bb98;" height="30px" width="500px">Features</th>

                <th style="border: 1px solid black; background-color: #c3d59d;" height="30px" width="50px">Comprehensive</th>

              </tr>

            </thead>

            <tbody>

              <tr>

                <td width="80px" height="30px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">1.</td>

                <td style="border: 1px solid black; "><label style="padding-left3px;" >Warranty on Hardware</label></td>

                <td style="border: 1px solid black;  background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/tick.svg" ></center></td>

              </tr>

              <tr>

                <td   width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">2.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Warranty on software</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/tick.svg" ></center></td>

              </tr>

              <tr>

                <td  width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">3.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Telephonic & Online Support (as & when required)</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d; text-align:center; color:#187e3d; font-size:13px;">Within One working hour</td>

              </tr>

              <tr>

                <td  width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">4.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >On-Site Support (as & when necessary)</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d; text-align:center;color:#187e3d; font-size:13px;">2 to 4 working hours</td>

              </tr>

              <tr>

                <td  width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">5.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Repair of Faulty Devices</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/tick.svg" ></center></td>

              </tr>

              <tr>

                <td width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">6.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Spare Parts (needed for Repairs)</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/tick.svg" ></center></span></td>

              </tr>

              <tr>

                <td  width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">7.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Replacement of Faulty Hardware(if it is not Repairable)</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/tick.svg" ></center></td>

              </tr>

              <tr>

                <td  width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">8.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Temporary Supply of Backup Device(While Faulty Device is being Repaired)</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/tick.svg" ></center></td>

              </tr>

              <tr>

                <td  width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">9.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Regular Online System checkup each Site(Minimum once every 3 months)</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/tick.svg" ></center></td>

              </tr>

              <tr>

                <td  width="80px"></td>

                <td style="border: 1px solid black; text-align:center;" height="30px">10.</td>

                <td style="border: 1px solid black;"><label style="padding-left3px;" >Any Hardware Fault due to Identifiable Physical Damage, Misuse or Power Fluctuation</label></td>

                <td style="border: 1px solid black; background-color: #c3d59d;"><center><img style="width:15px" src="<?= base_url()?>assets/admin/pdf/cross.svg" ></center></td>

              </tr>

            </tbody>

          </table>

          <br>



          <table>

            <tr>

              <td width="40px"></td>

              <td style="background-color:#c3d59d; color:blue;" width="630px">Total Amount</td>

              <td style="background-color:#c3d59d; color:blue;" >AED 200.00</td>



            </tr>



            <tr>

              <td width="40px"></td>

              <td style="background-color:#c3d59d; color:blue;" width="630px">Comprehensive AMC + Service & Testing (For 1 Year)              </td>

              <td style="background-color:#c3d59d; color:blue;">AED 150.00</td>



            </tr>

          </table>

          <br>

          <table>

            <tr>

              <td width="40px"></td>

              <td colspan="3" width="705px" style="text-align: right;"><span style="color:red;border-bottom: 1px solid red; ">Note: 5% VAT will be applicable on Total Amount</span></td>

              



            </tr>



            <tr>

              <td width="40px"></td>

              <td colspan="3" width="705px" style="text-align: right;"><span style="color:red;border-bottom: 1px solid red; ">Note: Any hardware fault found prior to AMC period will be charged at actual spare part/hardware cost</span></td>

              

              

            </tr>

            



             </table> 

              <br>

            <table>

            <tr>

              <td width="40px" ></td>

              <td width="730px" colspan="2"  style="text-align:center; background-color:#a6a6a6;"><h2>Terms & Conditions</h2></td>

              

             

            </tr>

            <br>

            <tr>

              <td width="40px" ></td>

              <td><b style="text-align:left; ">Validity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Until March 30, 2019  </td>

              <td></td>

              

              

            </tr>



            <tr>

              <td width="40px" ></td>

              <td width="730px"  style="text-align:left; "><b>Payment &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  100% in Advance along with Order Confirmation</td>

              <td ></td>

              

            </tr>



            <tr>

              <td width="40px" ></td>

              <td width="730px"  style="text-align:left; "><b>Support &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  1 Year Online, Telephonic & On-Site Support will be provided</td>

              <td></td>

              

            </tr>

            



          </table>

        

      



    



    

  



