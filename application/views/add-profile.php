
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      <!--<div class="page-title"> <i class="icon-custom-left"></i>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border"> <br>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">
                        
                        <div class="col-md-12">
                         <div class="form-group">
                            <?php 
                            if(!empty($data['user_id']) && @$data['user_id'] != null){
                                $appendedFiles = array();
                                $file_path = dir_path().$image_upload_dir.'/'.@$data['user_image'];
                                  if (!is_dir($file_path) && file_exists($file_path)) {
                                    // $current_img = site_url($image_upload_dir."/".$categoryData['user_image']);
                                    @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['user_image']));
                                    $appendedFiles[] = array(
                                      "name" => @$data['user_image'],
                                      "type" => $file_details['mime'],
                                      "size" => filesize($file_path),
                                      "file" => site_url($image_upload_dir."/". @$data['user_image']),
                                      "data" => array(
                                        "url" => site_url($image_upload_dir."/". @$data['user_image']),
                                        "image_white_file_id" => $data['user_id']
                                      )
                                    );
                                  }
                                $appendedFiles = json_encode($appendedFiles);
                            }
                            ?>
                            <!-- <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files='<?php echo @$appendedFiles;?>'> -->
                            <img class="resize" src="<?= site_url($image_upload_dir."/". @$data['user_image']) ?>"> 
                        </div>
                      </div>

                       <div class="col-md-6">  
                        <div class="form-group">
                          <div class="row form-row">
                            <div class="col-md-4">
                              <label class="form-label">Name</label>
                              <!-- <span class="help">e.g. "Mona Lisa Portrait"</span> -->
                            </div>
                            <div class="col-md-9">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" name="user_name" class="form-control" placeholder="Enter Name" value="<?= @$data['user_name']; ?>" readonly>
                              </div>
                            </div>
                          </div>
                        </div>
                       </div>

                       <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                                <div class="col-md-4">
                                    <label class="form-label">Display Name:</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="input-with-icon right controls">
                                        <i class=""></i>
                                        <input name="user_display_name" type="text" value="<?= @$data['user_display_name']; ?>" readonly class="form-control" placeholder="Enter Display Name">
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div> 

                      <div class="clearfix"></div>          
                        <div class="col-md-6">   
                          <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Phone</label>
                          <!-- <span class="help">e.g. "+333 222 111"</span> -->
                              </div>
                              <div class="col-md-9">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <input type="text" id="phone" name="user_phone" class="form-control" placeholder="Enter Phone" value="<?= @$data['user_phone']; ?>" readonly>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="row form-row">
                            <div class="col-md-4 my-width--style">
                              <label class="form-label">Email</label>
                              <!-- <span class="help">e.g. "some@example.com"</span> -->
                            </div>
                            <div class="col-md-9">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" name="user_email" class="form-control" placeholder="Enter E-mail" value="<?= @$data['user_email']; ?>" readonly>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="row form-row">
                            <div class="col-md-4">
                              <label class="form-label">Password</label>
                            </div>
                              <div class="col-md-9">
                                <div class="controls">
                                  <div class="input-with-icon right"> <i class=""></i>
                                    <input type="password" id="password" name="user_password" class="form-control" placeholder="Enter Password" value="<?= $passwords; ?>">
                                  </div>
                                  <div class="checkbox checkbox-inline check-success">  
                                    <input type="checkbox" id="ch_pass" value="0" style="position: relative;top: 2px;"> 
                                    <label for="ch_pass" id="ch_passtxt" >Show Password</label>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                                <div class="col-md-4">
                                    <label class="form-label">Position Title:</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="input-with-icon right controls">
                                        <i class=""></i>
                                        <input name="user_pos_title" type="text" value="<?= @$data['user_pos_title']; ?>" readonly class="form-control" placeholder="Enter Position Title">
                                    </div>
                               
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                      
                      <div class="col-md-6">
                          <div class="form-group">
                              <div class="row form-row">
                                  <div class="col-md-4">
                                      <label class="form-label">Department:</label>
                                  </div>
                                  <div class="col-md-9">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <select name="department_id" id="department_id" class="select2 form-control" disabled="true">
                                        <option selected disabled>--- Select Department ---</option>
                                        <?php
                                        foreach($departmentData as $k => $v){
                                        ?>
                                        <option value="<?= $v['department_id']; ?>" readonly <?= ( $v['department_id'] == @$data['department_id'])?'selected':''; ?> ><?= $v['department_title']; ?>
                                        </option>
                                        <?php
                                        }
                                        ?>
                                      </select>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                                
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="row form-row">
                            <div class="col-md-6">
                              <label class="form-label">Include in Emails for:</label>
                            </div>
                            <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <div class="checkbox checkbox-inline check-success">
                                  <input type="checkbox" id="quotation_email" name="quotation_email" value="1" <?=( @$data['quotation_email']==1 )? 'checked': ''; ?> style="position: relative;top: 2px;" disabled="disabled"> 
                                  <label for="quotation_email" id="">Quotation</label>
                                </div>
                                <div class="checkbox checkbox-inline check-success"> 
                                  <input type="checkbox" id="invoice_email" name="invoice_email" value="1" <?=( @$data['invoice_email']==1 )? 'checked': ''; ?> style="position: relative;top: 2px;" disabled="disabled">
                                  <label for="invoice_email" id="">Invoice</label>          
                                </div>
                                <div class="checkbox checkbox-inline check-success  ">
                                   <input type="checkbox" id="proforma_invoice_email" name="proforma_invoice_email" value="1" <?=( @$data['proforma_invoice_email']==1 )? 'checked': ''; ?> style="position: relative;top: 2px;left: 5%;" disabled="disabled"> 
                                  <label for="proforma_invoice_email">Proforma Invoice</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                                
                      <div class="clearfix"></div>
                      
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="row form-row">
                            <div class="col-md-4">
                              <label class="form-label">Status:</label>
                            </div>
                            <div class="col-md-9">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <select class="form-control input-signin-mystyle select2" id="user_is_active" name="user_is_active" disabled="true">
                                  <option value="" selected="selected" disabled="">- Select Status -</option>
                                  <option value="1" <?=( @$data['user_is_active']==1 )? 'selected': ''; ?>>Enabled</option>
                                  <option value="0" <?=( @$data['user_is_active']==0 )? 'selected': ''; ?>>Disabled</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                              

                      <div class="clearfix"></div>
                       <div class="col-md-12"> 
                        <div class="form-group text-center">
                            <br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="submit">Submit</button>
                            <input type="hidden" name="id" value="<?= @$data['user_id']; ?>">
                            <input type="hidden" name="user_email_old" value="<?= @$data['user_email']; ?>">
                            <input type="hidden" name="user_image_old" value="<?= @$data['user_image']; ?>">
                            <input type="hidden" name="user_phone_old" value="<?= @$data['user_phone']; ?>">
                        </div>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script>
  
  $(document).ready(function() {
    $('#fileUploader').fileuploader({
        changeInput: '<div class="fileuploader-input fileuploader-input-profile ">' +
            '<div class="fileuploader-input-inner">' +
            '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
            '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
            '<p>or</p>' +
            '<div class="fileuploader-input-button" ><span>Browse Files</span></div>' +
            '</div>' +
            '</div>',
        theme: 'dragdrop',
        limit: 1,
        extensions: ['jpg', 'jpeg', 'png'],
         onRemove: function(item) {
           $.post('<?= site_url('profile/imageDelete'); ?>', {
             file: item.name,
             data: {
               image_file_id:"<?= @$data['user_id']; ?>",
               file:item.name,
               image_post_file_id:item.data.image_file_id
             }
           });
         },

        captions: {
            feedback: 'Drag and drop files here',
            feedback2: 'Drag and drop files here',
            drop: 'Drag and drop files here'
        },

    });
    // $('#fileUploader').attr('disabled', true);

    $('#ch_pass,ch_passtxt').on('click', function () {
      var count = $('#ch_pass').val();
      if(count == 0){
        $("#password").prop('type', 'text');
        $('#ch_passtxt').text("Hide Password");
        $('#ch_pass').val(1);
      }
      else{
        $("#password").prop('type', 'password');
        $('#ch_passtxt').text("Show Password");
        $('#ch_pass').val(0);
      }
    });
      
    $(function(){
        $(".select2").select2();
    });
    $('.datepicker').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true
    });

  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        user_name:{
          required: true
        },
        user_email:{
          required: true,
          email: true
        },
        user_password:{
          required: true
        },
        user_phone:{
          required: true
        },
      }, 
      messages: {
        user_name: "This field is required.",
        user_phone: "This field is required.",
        user_email:{
          required: "This field is required.",
          email: "Invalid E-mail Address"
        },
        user_password: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');

          
        }
        // submitHandler: function (form) {

        // }
      });
    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
  });
</script>