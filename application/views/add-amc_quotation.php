    <style>
      .border-bot
      {
        border-bottom: 2px solid #fff !important;
      }
    </style>
    <div class="page-content">
      <div class="content">
        <ul class="breadcrumb">
          <li>
            <p>Dashboard</p>
          </li>
          <li><a href="#" class="active"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
        </ul>
       
        <div class="row-fluid">
          <div class="span12">
            <div class="grid simple">
              <div class="grid-title no-border">
                <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
              </div>
              <div class="grid-body no-border">
                <form class="ajaxForm validate" id="form" action="<?php echo site_url($save_product) ?>" method="post" enctype="multipart/form-data">
                  <div id="general_fields">
                    <div class="row">


                      <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">Quotation No</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <div class="row">
                                      <div class="col-md-2 col-sm-1 col-xs-2">
                                        <label style="padding: 0 3px;" disabled type="number" value="<?php echo @$setval["quotation_prfx"]; ?>" class=""><?php echo @$setval["company_prefix"] . @$setval["quotation_prfx"]; ?></label>
                                      </div>
                                      <div class="col-md-10 col-sm-11 col-xs-10">

                                        <input name="amc_quotation_revised_no" type="hidden" value="<?= number_format(@$data['amc_quotation_revised_no'] + 1); ?>">
                                        <input name="quotation_no_temp" readonly type="text" value="<?= ($page_title == 'add') ? '' : @$data['amc_quotation_no'] . '-R' . number_format(@$data['amc_quotation_revised_no'] + 1); ?>" class="form-control" placeholder="">
                                        <input type="hidden" name="quotation_no" value="<?= ($page_title == 'add') ? 1 : @$data['amc_quotation_no']; ?>">
                                      </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class=" col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">Quotation Date</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <input type="text" class="form-control input-signin-mystyle datepicker2" id="quotation_date" name="quotation_date" placeholder="" value="<?= @$data['amc_quotation_date']; ?>">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">Validity (No. of Days)</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input type="text" class='txtboxToFilter quotation_validity' name="amc_quotation_validity" value="<?= ($page_title == 'add') ? '' : @$data['amc_quotation_validity']; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class=" col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">AMC Start Date</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <input type="text" autocomplete="off" class="form-control input-signin-mystyle datepicker2" id="quotation_start_date" name="amc_quotation_start_date" placeholder="" value="<?= @$data['amc_quotation_start_date']; ?>">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">AMC Expiry Date</label>
                              </div>
                              <div class="col-md-12">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <input type="text" autocomplete="off" class="form-control input-signin-mystyle datepicker" id="quotation_expiry_date" name="quotation_expiry_date" placeholder="" value="<?= @$data['amc_quotation_expiry_date']; ?>">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">Max No. Of Onsite Visits</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input type="text" class='txtboxToFilter max_visitor' name="amc_quotation_max_visit" value="<?= ($page_title == 'add') ? '' : @$data['amc_quotation_max_visit']; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-8">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">Customer</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select name="customer_id" id="customer_id" class="select2 form-control">
                                      <option value="0" selected disabled>--- Select Customer ---</option>
                                      <?php
                                      foreach ($customerData as $k => $v) {
                                      ?>

                                        <option value="<?= @$v['user_id']; ?>" data-city="<?= $v['user_city']?>" data-country="<?= $v['country_name']?>" data-address="<?= $v['user_address']?>" data-name="<?= $v['user_name']; ?>" data-position='<?= $v['user_pos_title'] ?>' data-user-country='<?= @$v['user_country']; ?>' data-user-country-name='<?= @$v['country_name']; ?>' data-validity-days='<?= @$v['validity_days']; ?>' data-support-days='<?= @$v['support_days']; ?>' data-warranty-years='<?= @$v['warranty_years']; ?>' data-price-plan-id='<?= @$v['plan_type']; ?>' data-customer-type='<?= @$v['customer_type']; ?>' data-customer-type-title='<?= @$v['title']; ?>' <?= ($v['user_id'] == @$data['customer_id']) ? 'selected' : ''; ?>><?= @$v['user_name']; ?></option>
                                      <?php
                                      }
                                      ?>
                                    </select>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label style="display: none;" class="form-label">Currency</label>
                              </div>
                              <div class="col-md-12">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <div class="radio radio-success" style="display: none;">

                                    <?php
                                    $count = 3;
                                    $required_check = 0;
                                    foreach ($currencyData as $k => $v) {

                                    ?>
                                      <input type="radio" class="quotation_currency" id="checkbox<?= $count; ?>" data-title="<?= $v['title'] ?>" name="quotation_currency" value="<?= $v['id']; ?>" <?= (@$data['amc_quotation_currency'] == $v['id']) ? 'checked="checked"' : (($v['id'] == @$base_currency_id) ? 'checked="checked"' : ''); ?> <?= ($required_check == 0) ? 'required' : ''; ?>>
                                      <label for="checkbox<?= $count; ?>"><?= $v['title'] ?></label>
                                    <?php
                                      $required_check++;
                                      $count++;
                                    }
                                    ?>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        
                        <div class="row">
                        <div class="col-md-8">
                      <div class="row">
                        <div class="col-md-12">
                          <label class="form-label">End User</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="input-with-icon right controls">
                              <i class=""></i>
                              <select name="end_user" id="end_user" class="select2 form-control">
                                      <?php
                                      if($page_title == 'add'){
                                      ?>
                                      <option selected disabled>--- Select End User ---</option>
                                      <?php }else{
                                        if(isset($end_user['end_user_id'])){
                                        ?>
                                        <option disabled>--- Select End User ---</option>
                                        <option value="<?= @$end_user['end_user_id']?>"selected><?= @$end_user['end_user_name']?></option>
                                        <?php
                                        }else
                                        {
                                          ?>
                                            <option selected disabled>--- Select End User ---</option>
                                          <?php
                                        }
                                      } ?>
                                        

                                      </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                        </div>
                        <div class="row">
                          <div class="col-md-8">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">Employee</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select name="employee_id" id="employee_id" class="select2 form-control">
                                      <option value="0" selected disabled>--- Select Employee ---</option>
                                      <?php
                                      foreach ($employeeData as $k => $v) {
                                      ?>

                                        <option value="<?= $v['user_id']; ?>" data-position='<?= $v['user_pos_title'] ?>' data-user-country='<?= @$v['user_country']; ?>' data-validity-days='<?= @$v['validity_days']; ?>' data-support-days='<?= @$v['support_days']; ?>' data-warranty-years='<?= @$v['warranty_years']; ?>' data-price-plan-id='<?= @$v['plan_type']; ?>' data-customer-type='<?= @$v['customer_type']; ?>' data-customer-type-title='<?= @$v['title']; ?>' <?= ($v['user_id'] == @$data['employee_id']) ? 'selected' : ''; ?>><?= $v['user_name']; ?></option>
                                      <?php
                                      }
                                      ?>
                                    </select>

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">AMC Type</label>
                              </div>
                              <div class="col-md-12">
                                <div class="input-with-icon right controls ">
                                  <i class=""></i>



                                  <div class='checkbox  check-success mt-3'>
                                    <div class='amc_type_div'>

                                      <?php

                                      if ($page_title != 'add') {
                                        $query = "SELECT * FROM `amc_type` `at` WHERE `at`.`amc_quotation_id` = '" . $data['amc_quotation_id'] . "'";
                                        $type_data = getCustomRows($query);
                                        $kk = 0;
                                        foreach ($type_data as $k => $v) {
                                      ?>
                                          <div class="disply-inline-amc_type">
                                            <input name='amc_type_percentage[]' type='hidden' value='<?= $v['amc_type_percentage']; ?>'>

                                            <input type='checkbox' data-lund='<?= $v['amc_type_subtotal'] ?>' value='<?= $v['amc_type_id'] ?>' data-percentage='<?= $v['amc_type_percentage']; ?>' <?= ($v['amc_type_subtotal'] > 0) ? "checked='checked'" : "" ?> class='amc_type' id='amc_type<?= $k; ?>' name='amc_type[]' placeholder='' />

                                            <label style='padding-left:25px' for='amc_type<?= $k; ?>'><?= $v['amc_type_title']; ?></label>
                                          </div>
                                      <?php
                                          $kk++;
                                        }
                                      }
                                      ?>

                                    </div>
                                  </div>


                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      
                      <div class="col-md-12 col-lg-6 col-sm-12">
                        <div class="col-md-12">
                          <div class="list-overflow" role="application">
                            <div class="demo-section k-content amc_quot_list-width amc_quot_list-margin" style="min-width: 330px;">
                              <div>
                                <div class="listbox-1">
                                  <label>Select Invoice(s) for AMC</label>
                                  <select id="optional" name="quotation_deselected[]" style="min-width: 180px;">
                                    <?php
                                    foreach ($quotationData as $k => $v) {
                                    ?>
                                      <!-- <option value="<?= $v['quotation_id'] ?>" ><?= @$setval["company_prfx"] . @$setval["quotation_prfx"] ?><?= ($v['quotation_revised_no'] > 0) ? @$v['quotation_no'] . '-R' . number_format(@$v['quotation_revised_no']) : @$v['quotation_no']; ?></option>  -->
                                    <?php
                                    }
                                    ?>

                                  </select>
                                </div>
                                <div class="listbox-2">
                                  <label>Invoice(s) Due for AMC</label>
                                  <select id="selected" name="quotation[]" multiple="multiple" style="min-width: 180px;">
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>
                       
                      <div class="col-md-3 col-lg-3">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="form-label mi-lbl">Line Break Quotation(after item table)</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" class='txtboxToFilter' name="amc_quotation_report_line_break" value="<?= @$data['amc_quotation_report_line_break']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-3 col-lg-3">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="form-label mi-lbl">Line Break Quotation(before validity)</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" class='txtboxToFilter' name="amc_quotation_report_line_break_payment" value="<?= @$data['amc_quotation_report_line_break_payment']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-3 col-lg-3">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="form-label mi-lbl">Line Break Quotation(after list of item)</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" class='txtboxToFilter' name="amc_quotation_report_line_break_list_item" value="<?= @$data['amc_quotation_report_line_break_list_item']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-3 col-lg-3">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="form-label mi-lbl">Line Break Quotation(before ANNEXURE)</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" class='txtboxToFilter' name="amc_quotation_report_line_break_annexure" value="<?= @$data['amc_quotation_report_line_break_annexure']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-3 col-lg-3">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="form-label mi-lbl">Line Break Invoice(after item table)</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" class='txtboxToFilter' name="amc_quotation_report_line_break_invoice" value="<?= @$data['amc_quotation_report_line_break_invoice']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-3 col-lg-3">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="form-label mi-lbl">Line Break Invoice(after payment)</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" class='txtboxToFilter' name="amc_quotation_report_line_break_payment_invoice" value="<?= @$data['amc_quotation_report_line_break_payment_invoice']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-3 col-lg-3">
                        <div class="row">
                          <div class="col-md-12">
                            <label class="form-label mi-lbl">Line Break Invoice(after bank detail)</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" class='txtboxToFilter' name="amc_quotation_report_line_break_bank_detail_invoice" value="<?= @$data['amc_quotation_report_line_break_bank_detail_invoice']; ?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                                   
                      
                    <div class="clearfix"></div>

                      

                    </div>

                    <div class="row mt-5">
                      <div class="col-md-12">
                        <div class="dataTables_wrapper-1440 dataTables_wrapper-1441">
                          <table class="table table-bordered amc-table ">
                            <thead>
                              <tr>
                                <th class="text-center" style="width:60px">S.No</th>
                                <th colspan="4" class="text-left" style="min-width:580px">Product Name</th>
                                <th class="text-center" style="min-width:120px">AMC Start Date</th>
                                <th class="text-center" style="min-width:120px">AMC End Date</th>
                                <th class="text-center" style="min-width:150px">Qty</th>
                                <th class="text-center" style="min-width:150px">Rate</th>
                                <th class="text-center amc-amount">Amount</th>
                              </tr>
                            </thead>
                            <?php
                            if ($page_title != 'add') {
                            ?>
                             
                            <?php
                            }
                            ?>
                            <tbody id="customFields" class="item_table">


                              <?php
                              $i =0;
                              $count = 0;
                              $subtotal = 0;
                              if (isset($quotation_detail_data) && @$quotation_detail_data != null) {

                                foreach ($quotation_detail_data as $k => $v) {
                                  $count++;
                              ?>
                                  <tr class="txtMult">
                                    <td class="text-center s_no" style="width: 60px"><?= $count; ?></td>
                                    <td colspan="4">
                                      <label class="inv-num"><?php echo @$setval["company_prefix"] . @$setval["invoice_prfx"]; ?><?= @$v['selected_quotation_no'] ?></label>
                                      <input type="hidden" name="selected_quotation_no[]" value="<?= $v['selected_quotation_no'] ?>" />
                                      <select readonly="readonly" name="product_id[]" id="product_id<?= $k; ?>" class="amc-quot-select-dsabled form-control prod_name amc-quot-select-disply" style="width: 300px" required>
                                        <option selected disabled>--- Select Item ---</option>
                                        <?php foreach ($productData as $k2 => $v2) { ?>
                                          <option value="<?= $v2['product_id']; ?>" <?= ($v2['product_id'] == @$v['product_id']) ? 'selected' : ''; ?>>
                                            <?= $v2['product_name']; ?>
                                          </option>
                                        <?php } ?>
                                      </select>
                                      <div class="checkbox check-success mt-3 amc-quot-chckbox-disply">
                                        <input type="hidden" name="amc_exclude_quotation[<?=$i;?>]" value="0">
                                        <input type="checkbox" value="1" <?= (@$v['amc_quotation_detail_exclude'] == '1') ? 'checked="checked"' : '' ?> class="amc_exclude_quotation" id="exclude<?= $count; ?>" name="amc_exclude_quotation[<?=$i;?>]" placeholder="" />
                                        <label style="padding-left:25px" for="exclude<?= $count; ?>">Exclude</label>
                                      </div>
                                    </td>
                                    <td>
                                      <input type='text' readonly="readonly" class='form-control amc_start_date ' value="<?= $v['amc_quotation_detail_start_date']; ?>" name='amc_start_date[]' placeholder='' />
                                    </td>
                                    <td>
                                      <input type='text' class='form-control amc_end_date' readonly="readonly" value="<?= @$v['amc_quotation_detail_end_date']; ?>" name='amc_end_date[]' placeholder='' />
                                    </td>
                                    <td>
                                      <input type="text" style="width: 100px" readonly="readonly" class="qty txtboxToFilter text-center" value="<?= @$v['amc_quotation_detail_quantity']; ?>" name="amc_quantity[]" placeholder="" />
                                    </td>
                                    <td>
                                    <input type="text" style="width: 100px" name='p_rate[]' class="txtboxToFilter p_rate text-right" value="<?= @quotation_num_format($v['amc_quotation_detail_amount'])/@$v['amc_quotation_detail_quantity']; ?>"  placeholder="" />
                                    </td>
                                    <?php
                                    $amount = $v['amc_quotation_detail_amount'];
                                    $subtotal += $amount;
                                    ?>
                                    <td>
                                      <input type="hidden" class="selected_rate" name="selected_rate[]" value="<?= $v['selected_rate']?>">
                                      <input type="hidden" style="width: 200px" value="<?= $amount; ?>" class="code amount txtboxToFilter " name="amc_amount[]" placeholder="" aria-invalid="false">
                                      <input type="hidden" name="quotation_detail_rate[]" class="quotation_detail_rate" value="<?= $v['amc_quotation_detail_rate']; ?>">
                                      <input type="hidden" name="quotation_detail_rate_usd[]" class="quotation_detail_rate_usd" value="<?= $v['amc_quotation_detail_rate_usd']; ?>">
                                      <input type="text" readonly name="rate[]" class="rate text-right" value="<?= @quotation_num_format($v['amc_quotation_detail_amount']); ?>">
                                    </td>

                                  </tr>

                              <?php
                              $i++;
                                }
                              }
                              ?>  
                            </tbody>
                            <tr>
                                <td colspan="9" class="text-center amc-inv-total-amount-lbl-td " style="border-right: none !important;">
                                  <label class="lbl-amc-inv-total-amount">Invoice Total Amount</label>
                                </td>
                                <td colspan='2' class="text-right" style="border-left: none !important;">
                                  <input readonly class="total_rate text-right" name="total_rate" type="text" value="<?= @quotation_num_format($data['total_rate']) ?>" />
                                </td>
                              </tr>
                          </table>
                          <table class="table table-bordered amc-table ">
                            <tbody class="item_table">
                              
                              <?php
                              if ($page_title != 'add') {
                                $query = "SELECT * FROM `amc_type` `at` WHERE `at`.`amc_quotation_id` = '" . $data['amc_quotation_id'] . "'";
                                $type_data = getCustomRows($query);
                                $x = 0;
                                foreach ($type_data as $k => $v) {
                                  $x++;
                              ?>
                                  <tr style="<?= ($v['amc_type_subtotal'] <= 0) ? 'display: none;' : '' ?> " class="amc_table_detail amc_total_div_<?= $v['amc_type_id'] ?>">

                                    <th colspan="4" rowspan="3" class="">
                                      <label><b><?= $v['amc_type_title']; ?>@<?= $v['amc_type_percentage']; ?>%</b></label>
                                      <input type="hidden" name='amc_type_title[]' value='<?= $v['amc_type_title'] ?>'>
                                      <p>Notes:</p>
                                      <textarea name="amc_type_detail[]" class="amc_type_detail[] amc-quot_detail_control" rows="4" cols="35"><?= $v['amc_type_note']; ?></textarea>
                                    </th>

                                    <th colspan="6" class="text-right " style="vertical-align: middle;">Total AMC Value </th>
                                    <th class="text-right " style="vertical-align: middle;">
                                      <input type="text" readonly="readonly" value='<?= $v['amc_type_subtotal'] ?>' class="subtotal text-right" id="subtotal<?= $x; ?>" name="subtotal[]" value="" style="width:140px" placeholder="0" />
                                    </th>
                                  </tr>

                                  <tr style="<?= ($v['amc_type_subtotal'] <= 0) ? 'display: none;' : '' ?> " class="amc_table_detail amc_total_div_<?= $v['amc_type_id'] ?>">
                                    <th colspan="3" style="min-width: 120px" class="text-right">Discount Type</th>

                                    <th class="text-right">
                                      <select name="quotation_total_discount_type[]" style="display:inherit;width:50%"  class="form-control total_discount_type pull-left" id="total_discount_type<?= $x; ?>">
                                        <option value="0" <?= (@$v['amc_type_total_discount_type'] == 0) ? 'selected' : ''; ?>>- select discount type -</option>
                                        <option value="1" <?= (@$v['amc_type_total_discount_type'] == 1) ? 'selected' : ''; ?>>Percentage (%)</option>
                                        <option value="2" <?= (@$v['amc_type_total_discount_type'] == 2) ? 'selected' : ''; ?>>Amount</option>
                                      </select>
                                      <input type="text" style="width:20%" class="total_discount_amount text-right" value='<?= $v['amc_type_total_discount_amount']; ?>' id="total_discount_amount<?= $x; ?>" name="quotation_total_discount_amount[]" value="" placeholder="0" />
                                    </th>
                                    <th colspan="1" class="text-left">
                                      <input type="text" style="width: 270px;" value='<?= $v['amc_type_discount_detail']; ?>' name="discount_detail[]">
                                    </th>
                                    <th colspan="1"  class="text-right">Discount Amount</th>
                                    <th class="text-right">
                                    <input type="text" style="width:140px" readonly class="text-right total_discount_amount_val" id="total_discount_amount_val' + x + '" name="total_discount_amount_val" value="" placeholder="0" />
                                    </th>
                                  </tr>

                                  <tr style="<?= ($thisCustomerData['user_country'] == $setval['setting_company_country'] && $v['amc_type_subtotal'] > 0) ? '' : 'display: none;' ?> " class="amc_table_detail tax_row amc_total_div_<?= $v['amc_type_id'] ?>">
                                    <th colspan="6"  class="text-right">VAT @ <?= $setval['tax'] ?></th>
                                    <th class="text-right  border-bot">
                                      <input type="text" readonly style="width:140px" class="tax_value text-right" value='<?= ($thisCustomerData['user_country'] == $setval['setting_company_country']) ? @$v['tax_value'] : 0; ?>' id="tax<?= $x; ?>" name="tax_value[]" placeholder="0" />
                                      <input type="hidden" class="qt_tax" name="qt_tax" value="<?= ($thisCustomerData['user_country'] == $setval['setting_company_country']) ? $setval['tax'] : 0; ?>" />
                                    </th>
                                  </tr>
                                  
                                  <tr style="<?= ($v['amc_type_subtotal'] <= 0) ? 'display: none;' : '' ?> " class="border-bot amc_table_detail amc_total_div_<?= $v['amc_type_id'] ?>">
                                    <th colspan="<?= ($thisCustomerData['user_country'] == $setval['setting_company_country']) ? 10 : 6 ?>" class="text-right">Net Amc Amount</th>
                                    <th class="text-right"> <input type="text" readonly="readonly" value='<?= $v['amc_type_total_amount']; ?>' class="grandtotal text-right" id="grandtotal<?= $x; ?>" name="total[]" style="width:140px" value="0" placeholder="0" /> </th>
                                  </tr>
                              <?php
                                }
                              }
                              ?>
                            </tbody>

                            <tfoot>
                              <input style="width: 140px;" type="hidden" name='grandtotal' value='<?= @$grandtotal; ?>'>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group">
                      <h3>Payment Terms</h3>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="dataTables_wrapper-768">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th class="text-center amc_quot-pay_term-dt-th1">Add/Remove Row</th>
                                  <th class="text-center amc_quot-pay_term-dt-th2">Terms & Condition</th>
                                  <th class="text-center amc_quot-pay_term-dt-th3">Percentage</th>
                                  <th class="text-center amc_quot-pay_term-dt-th4">No Of Days</th>
                                  <th class="text-center amc_quot-pay_term-dt-th5">Amount</th>
                                </tr>
                              </thead>
                              <tr class="txtMult">
                                <td class="text-center"><a href="javascript:void(0);" class="paddCF">Add</a></td>
                                <td colspan="6"></td>
                              </tr>
                              <tbody id="pcustomFields">
                                <?php
                                if (@$terms != null && !empty(@$terms)) {
                                  foreach ($terms as $row) {
                                ?>

                                    <tr class="txtMult">
                                      <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="premCF">Remove</a></td>
                                      <td>
                                        <input type="text" class="terms" required style="width:100%" id="terms" name="terms[]" value="<?= @$row['payment_title'] ?>" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="percentage txtboxToFilter" required style="width:100%" id="percentage'+x+'" data-optional="0" name="percentage[]" value="<?= @$row['percentage'] ?>" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="payment_days txtboxToFilter" required style="width:100%" id="payment_days'+x+'" data-optional="0" name="payment_days[]" value="<?= @$row['payment_days'] ?>" placeholder="" />
                                      </td>
                                      <td>
                                        <input type="text" class="payment_amount txtboxToFilter text-right" required readonly style="width:100%" id="payment_amount'+x+'" data-optional="0" name="payment_amount[]" value="<?= @$row['payment_amount'] ?>" placeholder="" />
                                      </td>
                                    </tr>
                                <?php }
                                } ?>

                              </tbody>
                              <tfoot>
                                <th class="text-center" colspan='4'><label style="float:right">Amount</label></th>
                                <th class="text-center" width="200px"><input style="width:100%" class="text-right" readonly type='text' name='payment_terms_total'></th>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="row form-row">
                            <div class="col-md-12">
                              <label class="form-label">AMC Quotation Cover</label>
                              <button type="button" class="get_terms_data btn-warning quotation_ref_btn btn btn-sm">Refresh <i class="fa fa-refresh"></i></button>
                            </div>
                            <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <textarea name="amc_quotation_cover_letter" id="myeditor" type="textarea" class="form-control ckeditor" placeholder=""><?= ($page_title == 'add') ? @$quotation_cover : @$data['amc_quotation_cover_letter']; ?></textarea>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="row form-row">
                            <div class="col-md-12">
                              <label class="form-label">Terms & Condition</label>
                            </div>
                            <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <textarea name="amc_quotation_terms_conditions" type="textarea" id="myeditor1" class="form-control ckeditor" placeholder=""><?= ($page_title == 'add') ? @$setval['amc_quotation_terms_conditions'] : @$data['amc_quotation_terms_conditions']; ?></textarea>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group text-center">
                          <br>
                          <input name="id" type="hidden" value="<?= @$data['amc_quotation_id']; ?>">

                          <input name="quotation_no_old" type="hidden" value="<?= @$data['amc_quotation_no']; ?>">
                          <input type="hidden" name="amc_quotation_email_printed_status" value="<?= @$data['amc_quotation_email_printed_status']; ?>" />
                          <input type="hidden" name="amc_quotation_status" value="<?= @$data['amc_quotation_status']; ?>" />
                          <button class="btn btn-success btn-cons ajaxFormSubmitAlter " type="button">Submit</button>

                        </div>
                      </div>
                    </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="defaultModalDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-customer-title">Edit Description</h4>
          </div>
          <div class="modal-body mymodal-customer-body" id="myModalDescription">
            <div class="grid-body no-border">
              <textarea id="temp_quotation_desc" name="temp_quotation_desc" class="form-control" value="" placeholder=""></textarea>
              <input type="hidden" id="textbox">
            </div>
            <div class="modal-footer">
              <button class="btn btn-success edit_desc_button" type="button">
                Edit
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        
        </div>
        
      </div>
    </div>
    
    <input type="hidden" id="customer_tax" value="<?= ($page_title != 'add' && $thisCustomerData['user_country'] == $setval['setting_company_country']) ? 1 : 0 ?>" />
    <input type="hidden" class="amount_for_payment_percentage" />

    <script>
      var base_currency = '<?php echo $base_currency_id; ?>';
      var setting = JSON.parse('<?php echo json_encode($setval); ?>');
      var company_tax = setting.tax;
      $(document).ready(function() {

        var page = '<?php echo $page_title; ?>';
        var selected_invoices = "";
        $('.quotation_currency').trigger('change');
        var check_select = 0;
        var lastSel;
        if (page != 'add') {
          selected_invoices = JSON.parse('<?php echo $selected_invoice; ?>');
          add_quotation_in_option(selected_invoices);
          check_select = 1;
          cal_percentage();
          cal_total();
          project_days_total();
        }

        $("#optional").kendoListBox({
          connectWith: "selected",
          toolbar: {
            tools: ["transferTo", "transferFrom", "transferAllTo", "transferAllFrom"]
          }
        });

        $("#selected").kendoListBox();

        $(document).on('click', "a[data-command='transferTo'],a[data-command='transferAllTo'],a[data-command='transferFrom'],a[data-command='transferAllFrom']", function(e) {
          var selected_val = $('#selected').val();
          set_selected_data(selected_val);
        });

        function set_selected_data(selected_val) {
          if (selected_val == null || selected_val === undefined) {
            $("#customFields").empty();
            cal_total();
            cal_percentage();
            return false;
          }
          var temp = '<?php echo addslashes(json_encode($quotationDetailData)); ?>';
          var quotation_detail = JSON.parse(temp);
          var customer_id = $("#customer_id option:selected").val();
          var s_no = 1;
          var html = "";
          var temp_array = [];
          for (var i = 0; i < selected_val.length; i++) {
            for (var j = 0; j < quotation_detail.length; j++) {
              if (quotation_detail[j]['invoice_id'] == selected_val[i] && customer_id == quotation_detail[j]['customer_id']) {
                var temp_object = {};
                var check = 0;
                temp_array.forEach(function(item) {
                  if (item.product_id == quotation_detail[j]['product_id']) {
                    check = 1;
                  }
                  if (!$.isNumeric(item.invoice_detail_quantity)) {
                    item.invoice_detail_quantity = 0;
                  }
                });
                
                if (quotation_detail[j]['warranty_coverage'] != 1) {

                  temp_array.push(quotation_detail[j]);
                }
              }

            }

          }
          
          $("#customFields").empty();
          var from = $("#quotation_expiry_date").val().split("-");
          var expire_date = from[2] + '-' + from[1] + '-' + from[0];
          
          var to = $("#quotation_start_date").val().split("-");
          var start_date = to[2] + '-' + to[1] + '-' + to[0];

          var date = convert_date_pattern(expire_date);
          date.setDate(date.getDate());
          date = date_pattern(date);
            
        
          var found_greater_date = false;
          for (var i = 0; i < temp_array.length; i++) {
            var d1 = convert_date_pattern(start_date);
            var t_date = temp_array[i]['invoice_expire_date'].split("-");
            t_date = t_date[2]+'-'+ t_date[1]+'-'+ t_date[0];
            var d2 = convert_date_pattern(t_date);
            var qt_expire_date = convert_date_pattern(expire_date);
            var find_start_date ;
            console.log("Date 1:"+d1);
            console.log("Date 2:"+d2);
            if(d1 > d2) { 
              find_start_date = d1; 
            } else
            {
              d2.setDate(d2.getDate()+1);
              find_start_date = d2;
            }
            
            var this_start_date = date_pattern(find_start_date); //START DATE
            var find_days = find_date_days(find_start_date, qt_expire_date);
            if (find_start_date > qt_expire_date) {
              found_greater_date = true;
            }
            
            html = "<tr class='txtMult'>";
            html += "<td class='text-center s_no'  style='width: 60px'>";
            html += "<label>" + s_no + "</label></td><td colspan='4'>";
            html += "<label class='inv-num'>" + setting["company_prefix"] + setting["invoice_prfx"] + temp_array[i]['invoice_no'] + "</label>";
            html += "<input type='hidden' name='selected_quotation_no[]' value='" + temp_array[i]['invoice_no'] + "'/>";
            html += "<select name='product_id[]'  class='amc-quot-select-dsabled form-control prod_name amc-quot-select-disply' style='width: 300px' required>";
            html += "<option value = '" + temp_array[i]['product_id'] + "' selected>" + temp_array[i]['product_name'] + "</option>";
            html += "</select>";
            html += "<div class='checkbox  check-success mt-3 amc-quot-chckbox-disply'>";
            html += "<input type='hidden' name='amc_exclude_quotation["+i+"]' value='0'>";
            html += "<input type='checkbox' value='1'  class='amc_exclude_quotation'  id='exclude" + i + "' name='amc_exclude_quotation["+i+"]' placeholder='' />";
            html += "<label style='padding-left:25px' for='exclude" + i + "'>Exclude</label>";
            html += "</div>";
            html += "</td>";
            html += "<td>";
            html += "<input type='text'  readonly='readonly' class='form-control amc_start_date ' value='" + this_start_date + "'   name='amc_start_date[]' placeholder='' />";
            html += "</td>";
            html += "<td>";
            html += "<input type='text'  readonly='readonly' class='form-control amc_end_date '  value='" + date + "'  name='amc_end_date[]'  placeholder='' /></td>";
            
            var rate = 0;
            if(temp_array[i]['invoice_currency_id'] == 1)
            {
              rate = temp_array[i]['invoice_detail_usd_rate'];
            }
            else
            {
              rate = temp_array[i]['invoice_detail_base_rate'];
            }
            
            html += "<td ><input  readonly='readonly' type='text' style='width: 100px' class='qty txtboxToFilter text-center' value = '" + temp_array[i]['invoice_detail_quantity'] + "'  name='amc_quantity[]' placeholder='' />";
            html += "</td>";
            html += "<td>";
            html += "<input type='text' class='txtboxToFilterCustom p_rate text-right' name='p_rate[]' value='"+rate+"' >";
            html += "</td>";
            html += "<td>";
            html += "<input type='hidden' class='selected_rate' name='selected_rate[]' value='"+rate+"'>"
            html += "<input type='hidden' style='width: 200px' value = '" + rate * temp_array[i]['invoice_detail_base_rate'] + "' class='code amount  txtboxToFilter ' name='amc_amount[]' readonly placeholder='' />";
            html += "<input type='hidden' name='quotation_detail_rate[]' class='quotation_detail_rate' value='" + temp_array[i]['invoice_detail_usd_rate'] + "'>"
            html += "<input type='hidden' name='quotation_detail_rate_usd[]' class='quotation_detail_rate_usd' value='" + temp_array[i]['invoice_detail_base_rate'] + "'>";
            html += "<input type='text' readonly name='rate[]' class='rate text-right' value='" + customized_round(rate*temp_array[i]['invoice_detail_quantity']) + "'>";
            html += "</td>";
            html += "</tr>";
            $("#customFields").append(html);
            s_no++;

            $(".datepicker").datepicker({
              format: "dd-mm-yyyy",
              autoclose: true
            });


          }
          cal_total();
          cal_percentage();
          $('.p_rate').trigger('keyup');
          // $('.quotation_currency').trigger('change');
          if (found_greater_date) {
            error("An Invoice Start Date is greater than End Date");
          }
        }

        var max_fields = 6;
        var add_button = $(".addCF");
        var x = 1;
        $(add_button).click(function(e) {
          e.preventDefault();
          $('form.validate').validate();
          x++;
          var temp = '';
          temp += '<tr class="txtMult">';
          temp += '<td class="text-center s_no" style="width: 60px">' + x + '</td>';
          temp += '<td colspan="2">';
          temp += '<select name="product_id[]" class="form-control prod_name" style="width: 200px" required="">';
          temp += '<option selected="" disabled="">--- Select Product ---</option>';
          temp += '<?php foreach ($productData as $k => $v) { ?><option value="<?= $v['product_id']; ?>"><?= $v['product_name']; ?></option><?php } ?>';
          temp += '</select>';
          temp += '</td>';
          temp += '<td>';
          temp += '<input type="date" class="form-control amc_start_date datepicker" name="amc_start_date[]" >';
          temp += '</td>';
          temp += '<td>';
          temp += '<input type="date" class="form-control amc_end_date datepicker" name="amc_end_date[]" >';
          temp += '</td>';
          temp += '<td>';
          temp += '<input type="text" style="width: 60px" class="qty txtboxToFilter"  name="amc_quantity[]" placeholder="">';
          temp += '</td>';
          temp += '<td>';
          temp += '<input type="hidden" style="width: 200px"  class="code amount  txtboxToFilter " name="amc_amount[]" placeholder="" aria-invalid="false">';
          temp += '<input type="hidden" name="quotation_detail_rate[]" class="quotation_detail_rate" value="1">';
          temp += '<input type="hidden" name="quotation_detail_rate_usd[]" class="quotation_detail_rate_usd" value="3">';
          temp += '<input type="text" readonly name="rate[]" class="rate text-right" value="1">';
          temp += '</td>';
          temp += '</tr>';
          $("#customFields").append(temp);
          
        });

        $("#customFields").on('click', '.remCF', function() {
          $(this).parent().parent().next('tr').remove();
          $(this).parent().parent().remove();
        });

        var max_fields = 6;
        var add_button = $(".paddCF");
        var x = 1;
        $(add_button).click(function(e) {
          e.preventDefault();
          $('form.validate').validate();
          x++;
          var temp = '<tr class="txtMult">';
          temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="premCF">Remove</a></td>';
          temp += "<td>";
          temp += '<input type="text" class="code quantity terms" required  style="width:100%" id="terms' + x + '" data-optional="0" name="terms[]" value="" placeholder="" />';
          temp += '</td>';
          temp += "<td>";
          temp += '<input type="text" class="code quantity txtboxToFilter percentage" required  style="width:100%" id="percentage' + x + '" data-optional="0" name="percentage[]" value="" placeholder="" />';
          temp += '</td>';
          temp += "<td>";
          temp += '<input type="text" class="code quantity txtboxToFilter payment_days" required  style="width:100%" id="payment_days' + x + '" data-optional="0" name="payment_days[]" value="" placeholder="" />';
          temp += '</td>';
          temp += '<td>';
          temp += "<input type='text' class='payment_amount code text-right' required style='width:100%'' id='payment_amount" + x + "' data-optional='0' name='payment_amount[]' readonly placeholder='' />";
          temp += '</td>';
          temp += '</tr>';
          $("#pcustomFields").append(temp);
          
        });
        $(document).on('click', '.premCF', function() {
          $(this).parent().parent().remove();
          $('.txtMult .payment_amount').trigger('keyup');
        });
        var max_fields = 6;
        var add_button = $("#new_customFields .new_addCF");
        var x = 1;
        $(add_button).click(function(e) {
          e.preventDefault();
          $('form.validate').validate();
          x++;
          var temp = '<tr class="new_txtMult">';
          temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="new_remCF">Remove</a></td>';
          temp += "<td>";
          temp += '<input type="text" class="code quantity txtboxToFilter project_terms" required  style="width:100%" id="project_terms' + x + '" data-optional="0" name="project_terms[]" value="" placeholder="" />';
          temp += '</td>';
          temp += "<td>";
          temp += '<input type="text" class="code quantity txtboxToFilter hash_code" required  style="width:100%" id="hash_code' + x + '" data-optional="0" name="hash_code[]" value="" placeholder="" />';
          temp += '</td>';
          temp += "<td>";
          temp += '<input type="text" class="code quantity txtboxToFilter start_week" required  style="width:100%" id="start_week' + x + '" data-optional="0" name="start_week[]" value="" placeholder="" />';
          temp += '</td>';
          temp += "<td>";
          temp += '<input type="text" class="code quantity txtboxToFilter project_days" required  style="width:100%" id="project_days' + x + '" data-optional="0" name="project_days[]" value="" placeholder="" />';
          temp += '</td>';
          temp += '</tr>';
          $("#new_customFields").append(temp);
          
        });
        $("#new_customFields").on('click', '.new_remCF', function() {
          $(this).parent().parent().remove();
        });


        $(document).on('keydown keypress keyup change blur', ".percentage", function() {

          cal_percentage();
        });

        function project_days_total() {
          var total = 0;
          $("#new_customFields tr.new_txtMult").each(function() {

            var row = $(this).closest('tr'); // get the row
            var days = row.find('.project_days').val();
            days = (days == undefined || days == '') ? 0 : days;
            total += parseInt(days, 10);
          });

          $('input[name="duration_total"]').val(total);
        }

        //days total
        $(document).on('keydown keypress keyup change blur', ".project_days", function() {
          project_days_total();
        });

        $(document).on('change', ".quotation_currency", function() {

          var currency_id = $('input[name="quotation_currency"]:checked').val();
          $(".prod_name").each(function() {
            var currency_title = $('input[name="quotation_currency"]:checked').data('title');
            currency_title = '_' + currency_title.toLowerCase();
            var price_field = 'quotation_detail_rate';
            if (currency_id == base_currency) {
              currency_title = '';
            }
            price_field += currency_title;


            var row = $(this).closest('tr');
            var temp_rate = row.find('.' + price_field).val();
            var pro_amount = row.find('.rate').val(round_value(parseFloat(temp_rate)));

            var qty = row.find('.qty').val();
            qty = (qty == 'lot' || qty == 'Lot') ? 1 : qty;
            row.find('.amount').val(qty * temp_rate);

          });
          cal_total();
          cal_percentage();
        });

        $(document).on('change', ".amc_exclude_quotation", function() {

          var row = $(this).closest('tr');

          var amount = row.find('.amount');
          if (row.find(".amc_exclude_quotation").is(':checked')) {
            amount.attr('readonly', false);
            amount.val(0);
            row.find('.p_rate').val(0);
            
          } else {
            amount.attr('readonly', true);
            
            row.find('.p_rate').val(row.find('.selected_rate').val());
             
            // $(".qty").trigger("keyup");
          }
          $(".p_rate").trigger("keyup");
          cal_total();
          cal_percentage();

        });

        function add_quotation_in_option(is_edit = 0) {
          $('#loader').show();
          var customer_id = $("#customer_id option:selected").val();
          var dataString = 'cid=' + customer_id;

          $('.demo-section').html('');

          var html = '';
          html += '<div class="listbox-1"><label>Select Invoice(s) for AMC</label>';
          html += '<select id="optional" style="min-width: 180px;" >';
          var htm1 = "";
          var htm2 = "";


          $.ajax({
            url: "<?php echo site_url('Amc_quotation/customer_quotation'); ?>",
            dataType: "json",
            type: "POST",
            data: dataString,
            cache: false,
            success: function(quotationData) {
              if (quotationData) {

                $("#optional").empty();
                for (var i = 0; i < quotationData.length; i++) {
                  var is_found = 0;
                  htm1 = "";
                  htm1 += "<option value= '" + quotationData[i]['invoice_id'] + "' >" + setting["company_prefix"] + setting["invoice_prfx"];
                  if (quotationData[i]['status_revision'] > 0) {
                    htm1 += quotationData[i]['invoice_no'] + '-R' + quotationData[i]['status_revision'];
                  } else {
                    htm1 += quotationData[i]['invoice_no'];
                  }
                  htm1 += " (Expire:" + quotationData[i]['invoice_expire_date'] + ")";
                  htm1 += "</option>";
                  if (is_edit != 0 && is_edit != '' && is_edit != null) {
                    for (var j = 0; j < is_edit.length; j++) {
                      if (parseInt(quotationData[i]['invoice_id']) == parseInt(is_edit[j])) {
                        is_found = 1;
                      }
                    }
                  }
                  if (is_found == 0) {
                    html += htm1;
                  } else {
                    htm2 += htm1;
                  }
                }
                html += '</select>';
                html += '</div>';
                html += '<div class="listbox-2"><label>Invoice(s) Due for AMC</label>';
                html += '<select id="selected" style="min-width: 180px;" name="quotation[]" multiple="multiple">' + htm2 + '</select>';
                html += '</div>';
                $('.demo-section').html(html);

                $("#optional").kendoListBox({
                  connectWith: "selected",
                  toolbar: {
                    tools: ["transferTo", "transferFrom", "transferAllTo", "transferAllFrom"]
                  }

                });
                $("#selected").kendoListBox();
              } else {
                $("#heading").hide();
                $("#records").hide();
                $("#no_records").show();
              }
            }
          });

          $('#loader').delay(400).fadeOut(400);
          setTimeout(
            function() {
              success("Listbox Updated");
            }, 400);
        }

        function add_terms() {
          var customer_id = $('#customer_id').find(':selected').val();
          var dataString = 'cid=' + customer_id;
          $.ajax({
            url: "<?php echo site_url('Amc_quotation/customer_terms'); ?>",
            dataType: "json",
            type: "POST",
            data: dataString,
            cache: false,
            success: function(customerData) {
              if (customerData) {

                if (customerData['payment_terms'] === undefined) {} else {
                  $("#pcustomFields").empty();
                  for (var i = 0; i < customerData['payment_terms'].length; i++) {

                    var html = "<tr class='txtMult'>";
                    html += "<td class='text-center' style='width:60px;''><a href='javascript:void(0);'' class='premCF'>Remove</a></td>";
                    html += "<td>";
                    html += "<input type='text' class='terms' required style='width:100%'' id='terms'  name='terms[]'' value='" + customerData['payment_terms'][i]['payment_title'] + "' placeholder='' />";
                    html += "</td>";
                    html += "<td>";
                    html += "<input type='text' class='percentage txtboxToFilter' required style='width:100%''  data-optional='0' name='percentage[]' value='" + customerData['payment_terms'][i]['percentage'] + "' placeholder='' />";
                    html += "</td>";
                    html += "<td>";
                    html += "<input type='text' class='payment_days txtboxToFilter' required style='width:100%' data-optional='0' name='payment_days[]' value='" + customerData['payment_terms'][i]['payment_days'] + "' placeholder='' />";
                    html += "</td>";
                    html += "<td>";
                    html += "<input type='text' class='payment_amount txtboxToFilter text-right' required readonly style='width:100%' data-optional='0' name='payment_amount[]' value='" + customerData['payment_terms'][i]['payment_amount'] + "' placeholder='' />";
                    html += "</td>";
                    html += "</tr>";

                  }
                  $("#pcustomFields").append(html);
                }

                if (customerData['project_duration'] === undefined) {} else {
                  $('#new_customFields tr:not(:first)').remove();
                  var html = "";
                  for (var i = 0; i < customerData['project_duration'].length; i++) {
                    var hash_code = (customerData['project_duration'][i]['hash_code'] === undefined) ? "" : customerData['project_duration'][i]['hash_code'];
                    var project_days = (customerData['project_duration'][i]['project_days'] === undefined) ? "" : customerData['project_duration'][i]['project_days'];
                    html += "<tr class='new_txtMult'>";
                    html += "<td class='text-center' style='width:60px;'><a href='javascript:void(0);' class='new_remCF'>Remove</a></td>";
                    html += "<td>";
                    html += "<input type='text' class='project_terms' required style='width:100%' id='project_terms'  name='project_terms[]' value='" + customerData['project_duration'][i]['project_title'] + "' placeholder='' />";
                    html += "</td>";
                    html += "<td>";
                    html += "<input type='text' class='code quantity txtboxToFilter hash_code' required  name='hash_code[]' value='" + hash_code + "' placeholder='' />";
                    html += "</td>";
                    html += "<td>";
                    html += "<input type='text' class='code quantity txtboxToFilter start_week' required value='" + customerData['project_duration'][i]['start_week'] + "' style='width:100%' data-optional='0' name='start_week[]' value='' placeholder='' />";
                    html += "</td>";
                    html += "<td>";
                    html += "<input type='text' class='code quantity txtboxToFilter project_days' required  name='project_days[]' value='" + project_days + "' placeholder='' />";
                    html += "</td>";
                    html += "</tr>";
                  }
                  $("#new_customFields").append(html);
                }

                if (customerData['warranty_terms'] === undefined) {} else {
                  $("#warn_customFields").empty();
                  $("#warn_customFields").append(customerData['warranty_terms']);
                }

                if (customerData['quotation_cover_letter'] === undefined) {} else {
                  CKEDITOR.instances['myeditor'].setData(customerData['quotation_cover_letter']);
                }

                var html = '';
                html += "<option value='0'>---Select End User---</option>"
                for(var i =0; i<customerData['end_user'].length; i++)
                {
                  html += "<option value='"+customerData['end_user'][i]['end_user_id']+"'>"+customerData['end_user'][i]['end_user_name']+"</option>";
                }
                $('#end_user').html(html).trigger('change');   

                // $('.quotation_validity').val(customerData['quotation_validity']);
                $('.quotation_support').val(customerData['quotation_support']);
                $('.quotation_warranty').val(customerData['quotation_warranty']);

                $('.percentage').trigger('keyup');
                $('.project_days').trigger('keyup');
              } else {

              }
            }
          });
        }

        $(document).on('change', "#customer_id", function() {
          var quotation_start_date = $('input[name="amc_quotation_start_date"]').valid();
          var quotation_end_date = $('input[name="quotation_expiry_date"]').valid();
          if (!quotation_start_date || !quotation_end_date) {
            $("#customer_id").select2("val", "0");
            return false;
          }
          if (check_select == 0) {
            check_select++;
            add_quotation_in_option();
            add_terms();
            add_amc_type();
            lastSel = $("#customer_id option:selected");
            return true;
          }
          var form = document.getElementById("general_fields");
          var inputs = form.getElementsByTagName("input");
          swal({
              title: "Are you sure, discount will be gone?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {


                for (i = 0; i < inputs.length; i++) {
                
                }
                $("#customFields").empty();
                $("a[data-command='transferAllFrom']").trigger("click");
                lastSel = $("#customer_id option:selected");
                add_quotation_in_option();
                add_terms();
                add_amc_type();
              } else {
                lastSel.attr("selected", true);
                $(".select2-chosen").text(lastSel.text());
                return false;
              }
            });
        });
        



        $(document).on('change', ".prod_name", function() {

          var prod_name = $(this).parent().parent('tr').find('.prod_name').val();
          var customer = $('#customer_id').val();
          var price_plan_id = $("#customer_id").find(':selected').data("price-plan-id");
          var dataString = 'pid=' + prod_name + '&plan_id=' + price_plan_id;
          var row = $(this).closest('tr'); // get the row
          var street = row.find('.street'); // get the other select in the same row
          var pro_amount = row.find('.pro_amount');
          var quatity_num = row.find('.qty');
          var t_amount = row.find('.amount');
          var convert_to = $('input[name="quotation_currency"]:checked').data('title');
          var desc = $(this).closest('tr').find('.prod_desc_text');
          var currency_id = $('input[name="quotation_currency"]:checked').val();
          var discount_field = row.find('.discount_amount');
          var customer_type_title = $("#customer_id").find(':selected').data("customer-type-title");
          customer_type_title = customer_type_title.toLowerCase();
          var currency_title = $('input[name="quotation_currency"]:checked').data('title');
          currency_title = currency_title.toLowerCase();
          if (currency_id == base_currency) {
            currency_title = 'base';
          }
          
          var customer_type = $("#customer_id").find(':selected').data("customer-type");
          $.ajax({
            url: "<?php echo site_url('amc_quotation/progt'); ?>",
            dataType: "json",
            type: "POST",
            data: dataString,
            cache: false,
            success: function(employeeData) {
              if (employeeData) {
                street.val(employeeData.product_description);
                var temp = employeeData.product_description;

                desc.text(temp.replace(/(<([^>]+)>)/ig, ""));
                $(this).closest('tr').next('tr').find('.myModalBtnDesc').attr('data-id', temp);
                var amount = 0;
                var discount = 0;
                var price_name = '';
                var category_type = employeeData.title.trim().toLowerCase();
                if (category_type == 'service') {
                  quatity_num.val(0);
                  quatity_num.attr('readonly', true);

                } else {
                  quatity_num.attr('readonly', false);
                }

                price_name = customer_type_title + '_' + currency_title + '_price';
                amount = employeeData[price_name];
                pro_amount.val(amount);

                price_name = customer_type_title + '_discount';
                discount = employeeData[price_name];
                discount_field.attr("max", discount);
                cal_total();
                cal_percentage();
              } else {
                $("#heading").hide();
                $("#records").hide();
                $("#no_records").show();
              }
            }
          });
        });


      });


      $(document).on('keydown keypress keyup change blur', ".total_discount_amount, .total_discount_type,.quotation_buypack_discount", function() {

        $("input[name='amc_type[]']:checked").each(function() {

          var amc_type_id = $(this).val();
          var div = $('.amc_total_div_' + amc_type_id);

          var quotation_buypack_discount = '';
          var $subtotal = div.find(".subtotal").val()
          var $total_discount_amount = div.find('.total_discount_amount').val();
          var $total_discount_type = div.find('.total_discount_type').val();
          if ($total_discount_type != '') {
            $total_discount_amount = ($total_discount_amount != '') ? customized_round(parseFloat($total_discount_amount)) : 0;
          }

          if ($total_discount_type == 1) {
            if ($total_discount_amount != '') {
              if ($total_discount_amount <= 100) {
                $total_discount_amount = ($total_discount_amount / 100) * $subtotal;
              } else {

                $total_discount_amount = $subtotal;
                div.find('.total_discount_amount').val(100);
              }
            }
          } else if ($total_discount_type == 2) {
            if ($total_discount_amount != '') {
              $total_discount_amount = $total_discount_amount;
            }
          } else {
            div.find('.total_discount_amount').val(0);
            $total_discount_amount = 0;
          }


          var $grandtotal =customized_round($subtotal) - customized_round($total_discount_amount);

          if ($total_discount_type == 1) {
            if (quotation_buypack_discount != '') {
              if (quotation_buypack_discount <= 100) {
                quotation_buypack_discount = (quotation_buypack_discount / 100) * $grandtotal;
              } else {

                quotation_buypack_discount = $grandtotal;
                div.find('.quotation_buypack_discount').val(100);
              }
            }
          } else if ($total_discount_type == 2) {
            if (quotation_buypack_discount != '') {
              quotation_buypack_discount = quotation_buypack_discount;
            }
          } else {
            div.find('.quotation_buypack_discount').val(0);
            quotation_buypack_discount = 0;
          }
          if ($grandtotal < 0) {
            $grandtotal = 0;
            div.find('.quotation_buypack_discount').val($grandtotal);
          }
          if ($("#customer_tax").val() == 1) {
            div.find('.tax_row').show();
            var quotation_tax = div.find('.tax_value').val();
            var qt_tax = div.find('.qt_tax').val();

            if (quotation_tax != '' && quotation_tax != 0 && quotation_tax < 100) {
              var find_tax = ($grandtotal / 100) * qt_tax;
              if (!Number.isNaN(find_tax)) {
                $grandtotal = $grandtotal + customized_round(find_tax);

                div.find('.tax_value').val(round_value(find_tax));
              }
            } else {
              div.find('.tax_value').val(0);
            }
          } else {
            setTimeout(function() {
              div.filter('.tax_row').hide();
              div.find('.tax_value').val(0);
              div.find('.th_net_amount').attr('colspan', '6');
            }, 100);
          }

          $grandtotal = $grandtotal - quotation_buypack_discount;
          div.find('.grandtotal').val(round_value($grandtotal));

        });

        cal_percentage();
        cal_total();
      });



      $(document).on('keydown keypress keyup change blur', ".qty", function() {
        var mult = 0;
        var row = $(this).closest('tr');
        var $quatity_num = row.find('.qty').val();
        if ($quatity_num == undefined) {
          $quatity_num = ($quatity_num == undefined) ? 0 : $quatity_num;
        }
        $quatity_num = ($quatity_num == 'lot' || $quatity_num == 'Lot') ? 1 : $quatity_num;
        var $rate_num = row.find('.rate').val();
        if ($rate_num == undefined) {
          $rate_num = ($rate_num == undefined) ? 0 : $rate_num;
        }
        
        var $total = ($quatity_num * 1) * ($rate_num * 1);
        var $am = row.find('.amount').val($total);

        row.find('.multTotal').val($total);
        mult += $total;

        cal_total();
        cal_percentage();
      });

      $(document).on('keydown keypress keyup change blur', ".amount", function() {
        sum_total_on_exclude();
        cal_percentage();
        cal_total();
      });

      $(document).on('keydown keypress keyup change blur', ".p_rate", function() {

        var row = $(this).closest('tr');
        var rate = row.find('.p_rate').val();
        var qty = row.find('.qty').val();
        row.find('.rate').val(round_value(rate*qty));
        row.find('.amount ').val(round_value(rate*qty));

        sum_total_on_exclude();
        cal_percentage();
        cal_total();
      });

      function add_amc_type() {
        var customer_id = $('#customer_id').find(':selected').val();
        var dataString = 'cid=' + customer_id;
        $.ajax({
          url: "<?php echo site_url('Amc_quotation/customer_amc_type'); ?>",
          dataType: "json",
          type: "POST",
          data: dataString,
          cache: false,
          success: function(data) {
            if (data['data'][0]['user_country'] == window.setting['setting_company_country']) {
              $("#customer_tax").val(1);
            } else {
              $("#customer_tax").val(0);
            }
            $('.amc_type_div').empty();
            $('.amc_table_detail').remove();
            var html = '';
            var html2 = '';
            var x = 0;
            for (var i = 0; i < data['data'].length; i++) {
              x++;
              html += "<div class='disply-inline-amc_type'>";
              html += "<input name='amc_type_percentage[]' type='hidden' value='" + data['data'][i]['percentage'] + "'>";
              html += "<input type='checkbox' value='" + data['data'][i]['client_warranty_type_id'] + "' data-percentage='" + data['data'][i]['percentage'] + "'  class='amc_type'  id='amc_type" + x + "' name='amc_type[]'  placeholder='' />";
              html += "<label style='padding-left:25px' for='amc_type" + x + "'>" + data['data'][i]['title'] + "</label>";
              html += "</div>";

              html2 += '';
              html2 += ' <tr style="display:none;" class="amc_table_detail amc_total_div_' + data['data'][i]['client_warranty_type_id'] + '">';
              html2 += '  <th colspan="4" rowspan="3" class="" >';
              html2 += '    <label><b>' + data['data'][i]['title'] + "@" + data['data'][i]['percentage'] + '%</b></label>';
              html2 += '<input type="hidden" name="amc_type_title[]" value="' + data['data'][i]['title'] + '">';
              html2 += '    <p>Notes:</p>';
              html2 += '    <textarea name="amc_type_detail[]" class="amc_type_detail[] amc-quot_detail_control" rows="4" cols="35"></textarea>';
              html2 += '  </th>';
              html2 += '  <th colspan="6" class="text-right " style="vertical-align: middle;" >Total AMC Value </th>';
              html2 += '  <th class="text-right  " style="vertical-align: middle;">';
              html2 += '    <input type="text" readonly="readonly" class="subtotal text-right" id="subtotal' + x + '" name="subtotal[]" value="" style="width:140px" placeholder="0"  />';
              html2 += '  </th>';
              html2 += ' </tr>';

              html2 += ' <tr style="display:none;" class="amc_table_detail amc_total_div_' + data['data'][i]['client_warranty_type_id'] + '">';
              html2 += '  <th colspan="3" style="min-width:120px;" class="text-right" >Discount Type</th>';
              html2 += '  <th colspan="1" class="text-right">';
              html2 += '  <select name="quotation_total_discount_type[]" style="display:inherit;width:50%" class="pull-left form-control total_discount_type" id="total_discount_type' + x + '">';
              html2 += '    <option value="0" >-- select discount type --</option>';
              html2 += '    <option value="1" >Percentage (%)</option>';
              html2 += '    <option value="2" >Amount</option>';
              html2 += '  </select>';
              html2 += '    <input type="text" style="width:20%" class="total_discount_amount text-right" id="total_discount_amount' + x + '" name="quotation_total_discount_amount[]" value=""  placeholder="0"  />';
              html2 += '  </th>';
              html2 += '  <th colspan="1" class="text-left" ><input type="text" style="width:270px;" name="discount_detail[]"></th>';
              html2 += '  <th colspan="1" class="text-right" style="min-width:150px;" >Discount Amount</th>';
              html2 += '  <th class="text-right">';
              html2 += '    <input type="text" style="width:140px" readonly class="text-right total_discount_amount_val" id="total_discount_amount_val' + x + '" name="total_discount_amount_val" value="" placeholder="0" />';
              html2 += '  </th>';
              html2 += ' </tr >';
              html2 += ' <tr  style="display:none;" class="amc_table_detail  tax_row amc_total_div_' + data['data'][i]['client_warranty_type_id'] + '">';
              html2 += '  <th colspan="6" class="text-right " >VAT @ ' + window.setting['tax'] + ' </th>';
              html2 += '  <th class="text-right  "> <input type="text" readonly="readonly" class="tax_value text-right" id="tax_value' + x + '" name="tax_value[]" style="width:140px" value="' + window.setting['tax'] + '"  placeholder="0"  /><input type="hidden" readonly="readonly" class="qt_tax" id="qt_tax' + x + '" name="qt_tax[]" style="width:140px" value="' + window.setting['tax'] + '"  placeholder="0"  /></th>';

              html2 += ' </tr>'
              html2 += ' <tr style="display:none;" class="border-bot amc_table_detail amc_total_div_' + data['data'][i]['client_warranty_type_id'] + '">';
              html2 += '  <th colspan="10" class="text-right th_net_amount" >Net AMC Amount</th>';
              html2 += '  <th class="text-right"> <input type="text" readonly="readonly" class="grandtotal text-right" id="grandtotal' + x + '" name="total[]" style="width:140px" value="0"  placeholder="0"  /> </th>';
              html2 += ' </tr>';


            }
            
            $('.amc_type_div').append(html);
            $('.item_table').append(html2);
          }
        });

      }

      $(document).on('change', ".amc_type", function() {

        var amc_type_id = $(this).val();
        var div = $('.amc_total_div_' + amc_type_id);

        if ($(this).prop("checked") == true) {
          cal_total();
          div.show();
        } else {
          div.find('.subtotal').val(0);
          div.find('.total_discount_type option:eq(0)').prop('selected', true);
          div.find('.total_discount_amount').val(0);
          div.find('.grandtotal').val(0);
          div.hide();
        }

        cal_percentage();


      });


      function cal_total() {


        $("input[name='amc_type[]']:checked").each(function() {
          var mult = 0;
          var total_row_amount = 0;
          var amc_type_id = $(this).val();
          var div = $('.amc_total_div_' + amc_type_id);

          var percentage = $(this).data("percentage") / 100;
          if (percentage == undefined) {
            percentage = (percentage == undefined) ? 1 : percentage;
          }
          
          $("#customFields tr.txtMult").each(function() {

            var row = $(this).closest('tr');
            var $quatity_num = row.find('.qty').val();
            var amc_start_date = row.find('.amc_start_date').val();
            var amc_end_date = row.find('.amc_end_date').val();
            var amc_exclude_quotation = row.find('.amc_exclude_quotation').is(':checked');

            if ($quatity_num == undefined) {
              $quatity_num = ($quatity_num == undefined) ? 0 : $quatity_num;
            }
            if ($quatity_num == 'lot' || $quatity_num == 'Lot') {
              $quatity_num = 1;
            }
            var $rate_num = row.find('.rate').val();
            if ($rate_num == undefined) {
              $rate_num = ($rate_num == undefined) ? 0 : $rate_num;
            }

            var $am = ($rate_num * 1);
            if (!amc_exclude_quotation) {
              total_row_amount = parseFloat(total_row_amount) + parseFloat($rate_num);
            }
            if (!amc_exclude_quotation && amc_start_date !== undefined && amc_end_date !== undefined) {
            
			      var temp_amc_start_date = new Date(amc_start_date);
            var start_date = temp_amc_start_date.toString('dd-MM-yyyy');
			  
			      var temp_end_date = new Date(amc_end_date);
            var end_date = temp_end_date.toString('dd-MM-yyyy');
			  
            var total_days = find_date_days(start_date, end_date)+1;
				    var amt_per_day = parseFloat($am / 365);
            var ttl_amount = parseFloat(amt_per_day) * total_days ;
            console.log("Total Days: "+total_days);
			      var ttl_prctg =  parseFloat(ttl_amount) * percentage;
              
            mult += ttl_prctg;
          }

          });

          $(".total_rate").val(round_value(total_row_amount));
          div.find('.subtotal').val(round_value(mult));
          var $subtotal = round_value(mult);
          var $total_discount_amount = div.find('.total_discount_amount').val();
          var $total_discount_type = div.find('.total_discount_type').val();
          if ($total_discount_type != '') {
            $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
          }


          if ($total_discount_type == 1) {
            if ($total_discount_amount != '') {
              if ($total_discount_amount <= 100) {
                $total_discount_amount = ($total_discount_amount / 100) * $subtotal;
              } else {

                $total_discount_amount = $subtotal;
                div.find('.total_discount_amount').val(100);
                
              }
            }
          } else if ($total_discount_type == 2) {
            if ($total_discount_amount != '') {
              $total_discount_amount = $total_discount_amount;
            }
          } else {
            div.find('.total_discount_amount').val(0);
            $total_discount_amount = 0;
          }

          var $grandtotal = customized_round($subtotal) - customized_round($total_discount_amount);
          $total_discount_amount =  customized_round($total_discount_amount);
          div.find('.total_discount_amount_val').val(Number($total_discount_amount).toFixed(2));
          if ($grandtotal < 0) {
            $grandtotal = 0;
            div.find('.total_discount_amount').val(round_value($subtotal));
          }
          if ($("#customer_tax").val() == 1) {
            div.find('.tax_row').show();
            var quotation_tax = div.find('.tax_value').val();
            var qt_tax = div.find('.qt_tax').val();
            if (qt_tax != '' && qt_tax != 0 && qt_tax < 100) {

              var find_tax = ($grandtotal / 100) * qt_tax;
              find_tax = customized_round(find_tax);
              if (!Number.isNaN(find_tax)) {
                $grandtotal = $grandtotal + find_tax;
                div.find('.tax_value').val(round_value(find_tax));

              }
            } else {
              div.find('.tax_value').val(0);
            }
          } else {
            setTimeout(function() {
              div.filter('.tax_row').hide();
              div.find('.tax_value').val(0);
              div.find('.th_net_amount').attr('colspan', '6');
            }, 100);

          }
          
          div.find('.grandtotal').val(round_value($grandtotal));

        });

        var total_row_amount = 0;
        $("#customFields tr.txtMult").each(function() {

          var row = $(this).closest('tr');
          var $rate_num = row.find('.rate').val();
          var amc_exclude_quotation = row.find('.amc_exclude_quotation').is(':checked');
          if ($rate_num == undefined) {
            $rate_num = ($rate_num == undefined) ? 0 : $rate_num;
          }

          var $am = ($rate_num * 1);
          if (!amc_exclude_quotation) {
            total_row_amount = parseFloat(total_row_amount) + parseFloat($rate_num);
          }
        });
        $(".total_rate").val(round_value(total_row_amount));

      }

      function sum_total_on_exclude() {
        var mult = 0;
        var total_row_amount = 0;

        var amc_exclude_quotation = $('.amc_exclude_quotation', this).is(':checked');
        if (amc_exclude_quotation) {
          $("#customFields tr.txtMult").each(function() {
            var $am = eval($('.amount', this).val());
            total_row_amount = parseFloat(total_row_amount) + parseFloat($('.rate', this).val());
            mult += $am;

          });

          $(".subtotal").val(mult).val(mult);
          $('.total_rate').val(round_value(total_row_amount));

          var $subtotal = mult;
          var $total_discount_amount = $('.total_discount_amount').val();
          var $total_discount_type = $('.total_discount_type').val();
          if ($total_discount_type != '') {
            $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
          }

          if ($total_discount_type == 1) {
            if ($total_discount_amount != '') {
              if ($total_discount_amount <= 100) {
                $total_discount_amount = ($total_discount_amount / 100) * $subtotal;
              } else {

                $total_discount_amount = customized_round(parseFloat($subtotal));
                $('.total_discount_amount').val(round_value(parseFloat(100)));
                
              }
            }
          } else if ($total_discount_type == 2) {
            if ($total_discount_amount != '') {
              $total_discount_amount = $total_discount_amount;
            }
          } else {
            $('.total_discount_amount').val(0);
            $total_discount_amount = 0;
          }

          var $grandtotal = customized_round($subtotal) - customized_round($total_discount_amount);

          if ($grandtotal < 0) {
            $grandtotal = 0;
            $('.total_discount_amount').val(round_value($subtotal));
          }
          if ($("#customer_tax").val() == 1) {
            div.find('.tax_row').show();
            var quotation_tax = div.find('.tax_value').val();
            var qt_tax = div.find('.qt_tax').val();
            if (quotation_tax != '' && quotation_tax != 0 && quotation_tax < 100) {
              var find_tax = ($grandtotal / 100) * qt_tax;
              if (!Number.isNaN(find_tax)) {
                $grandtotal = $grandtotal + find_tax;
              }
            } else {
              div.find('.tax_value').val(0);
            }
          } else {
            setTimeout(function() {
              div.filter('.tax_row').hide();
              div.find('.tax_value').val(0);
              div.find('.th_net_amount').attr('colspan', '6');
            }, 100);
          }
          $('.grandtotal').val($grandtotal).val($grandtotal);
        }

      }

      $(document).ready(function() {

        $("#user_type").change(function() {
          var val = $(this).val();
          if (val == 2) {
            $('.myUserType').slideDown();
          } else {
            $('.myUserType').slideUp();
          }
        });

        $(".datepicker").datepicker({
          format: "yyyy-mm-dd",
          autoclose: true,
          startDate: today

        });

        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $(".datepicker2").datepicker({
          format: "yyyy-mm-dd",
          autoclose: true,
        });
        
      });

      $(document).ready(function() {
        $.validator.addMethod(
          "checkPercentage",
          function(value, element) {
            var check = true;
            var total_percentage = 0;
            $("#pcustomFields tr.txtMult").each(function() {

              var row = $(this).closest('tr'); // get the row
              var percentage = row.find('.percentage').val();
              percentage = (percentage == undefined || percentage == '') ? 0 : percentage;
              total_percentage += parseInt(percentage, 10);

            });
            if (total_percentage > 100 || total_percentage < 100) {
              check = false;
            }
            return check;

          },
          "Username is Already Taken"
        );
        $("form.validate").validate({
          rules: {
            customer_id: {
              required: true
            },
            "product_id[]": {
              required: true
            },
            "quotation_detail_quantity[]": {
              required: true,
              number: true
            },
            "quotation_detail_discount_amount[]": {
              number: true
            },
            "percentage[]": {
              required: true,
              checkPercentage: true
            },
            quotation_freight_type: {
              required: true,
            },
            quotation_detail_total_discount_amount: {
              number: true
            },
            quotation_date: {
              required: true
            },
            quotation_start_date: {
              required: true
            },
            amc_quotation_start_date: {
              required: true,

            },
            quotation_expiry_date: {
              required: true
            },
            quotation_customer_note: {
              required: true
            },
            quotation_terms_conditions: {
              required: true,

            },
            amc_quotation_max_visit: {
              required: true,
            },
            employee_id: {
              required: true,
            },
            // "amc_start_date[]": {
            //   required: true,
            // },
            // "amc_end_date[]": {
            //   required: true,
            // }
          },
          messages: {
            customer_id: "This field is required.",
            "product_id[]": "This field is required.",
            "quotation_detail_quantity[]": {
              required: "This field is required.",
              number: "Please Insert Number."
            },
            "discount_amount[]": "Please Insert Number.",
            total_discount_amount: "Please Insert Number.",
            quotation_no: {
              required: "This field is required.",
              digits: "Please enter only digits."
            },
            "percentage[]": {
              required: "This field is required.",
              checkPercentage: "Percentage Sum should be equal to 100."
            },
            quotation_date: "This field is required.",
            quotation_expiry_date: "This field is required.",
            quotation_customer_note: "This field is required.",
            quotation_terms_conditions: "This field is required.",
            "amc_start_date[]": {
              required: "This field is required.",
            },
            "amc_end_date[]": {
              required: "This field is required.",
            }
          },
          invalidHandler: function(event, validator) {
            error("Please input all the mandatory values marked as red");
          },
          errorPlacement: function(label, element) { // render error placement for each input type   
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
            $('<span class="error"></span>').insertAfter(element).append(label);
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('success-control').addClass('error-control');
          },
          highlight: function(element) { // hightlight error inputs
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
            var parent = $(element).parent();
            parent.removeClass('success-control').addClass('error-control');
          },
          unhighlight: function(element) { // revert the change done by hightlight
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
            var parent = $(element).parent();
            parent.removeClass('error-control').addClass('success-control');
          },
          success: function(label, element) {
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('error-control').addClass('success-control');

          }
         
        });
        $('.select2', "form.validate").change(function() {
          $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
        $('#fileUploader').fileuploader({
          changeInput: '<div class="fileuploader-input">' +
            '<div class="fileuploader-input-inner">' +
            '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
            '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
            '<p>or</p>' +
            '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
            '</div>' +
            '</div>',
          theme: 'dragdrop',
          onRemove: function(item) {

          },
          captions: {
            feedback: 'Drag and drop files here',
            feedback2: 'Drag and drop files here',
            drop: 'Drag and drop files here'
          },
        });
      });

      function cal_percentage() {
        var total_amount = 0;
        
        $('.grandtotal').each(function() {
          if (!isNaN(this.value) && parseFloat(this.value) > 0) {
            total_amount += parseFloat(this.value);
            // return false;
          }
        });
        $(".amount_for_payment_percentage").val(parseFloat(total_amount));
        $('input[name="grandtotal"]').val(total_amount);
        var total = 0;
        $("#pcustomFields tr.txtMult").each(function() {

          var row = $(this).closest('tr'); // get the row
          var percentage = (percentage == undefined || percentage == '') ? 0 : percentage;

          percentage = row.find('.percentage').val();
          percentage = percentage / 100;
          var amount = row.find('.payment_amount').val(round_value(percentage * total_amount));
          amount = (percentage * total_amount == undefined || percentage * total_amount == '') ? 0 : percentage * total_amount;
          total += parseFloat(amount);
        });
        $('input[name="payment_terms_total"]').val(round_value(total));
      }

      $(".get_terms_data").click(function() {
        var customer = $('#customer_id').valid();
        if (!customer ) {
          window.scrollTo(0,0);
            return false;
            
        }

        CKEDITOR.instances['myeditor1'].setData(terms_format(1));

        var extra = "";
        extra = "<strong>Date: "+$('#quotation_date').val()+"</strong><br>";

        var customer = $('#customer_id').find(':selected').data("name");
        var customer_address = $("#customer_id").find(':selected').data("address");
        var customer_city = $("#customer_id").find(':selected').data("city");
        var customer_country = $("#customer_id").find(':selected').data("country");
        
        extra += "<strong>"+customer+customer_address+customer_city+','+customer_country+"</strong>";
        var page = '<?php echo $page_title; ?>';
        var cover = '';
        if(page == 'add')
        {
          cover = <?= json_encode($quotation_cover); ?>;
        }else
        {
          cover = <?= json_encode(@$data['amc_quotation_cover_letter']); ?>;
        }
        

        CKEDITOR.instances['myeditor'].setData(extra + cover);
        
      });
      $(document).ready(function() {
        var page_title = "<?= $page_title; ?>";
        if (page_title == 'add') {
          CKEDITOR.instances['myeditor1'].setData(terms_format());
        }
      });

      function terms_format(option) {
        var max_visit = 0;
        var customer_name = ($('#customer_id').val() != null && $('#customer_id').val() != '') ? $('#customer_id').select2('data').text : '';

        var setting = JSON.parse('<?php echo json_encode($setval); ?>');
        if (typeof max_visit !== 'undefined') {
          max_visit = $('.max_visitor').val();

        }
        var html = '';
        var x = 0;
        html += "From this point onwards, vendor will be used for EaZy-Q team and Client will be mentioned for " + customer_name + ". SLA term will be used for service level agreement between both parties<br>"
        html += "<b>1. Coverage Details</b> <br>";
        html += "<b>a. Invoices included and their duration of AMC </b> <br>";
        $("#selected option").each(function() {
          x++;
          html += x + ". ";
          html += $(this).text();
          var extract_date = $(this).text().split("(");
          extract_date = extract_date[1].replace(")", "");
          extract_date = extract_date.replace("Expire:", "");
          var current_start_date = $("#quotation_start_date").val();
          var current_expire_date = $("#quotation_expiry_date").val();
          var start_date = "";
          var expire_date = "";
          
          if (current_start_date != '' || current_start_date != null) {
            
          var to = $("#quotation_start_date").val().split("-");
          var start_date = to[2] + '-' + to[1] + '-' + to[0];
          var d1 = convert_date_pattern(start_date);
          var t_date = extract_date.split("-");
          console.log("DATE "+t_date);
          t_date = t_date[2]+'-'+ t_date[1]+'-'+ t_date[0];
          var d2 = convert_date_pattern(t_date);
          var qt_expire_date = convert_date_pattern(expire_date);
          var find_start_date ;
          if(d1 > d2) { 
            start_date = d1; 
          } else
          {
            d2.setDate(d2.getDate()+1);
            start_date = d2;
          }
          
            
          }
          if (current_expire_date != '' || current_expire_date != null) {
            expire_date = current_expire_date;
          }
          var tmp = start_date.toString().split('00:00:00')[0];
          console.log("Abbas"+ start_date);
          html += " will remain valid from " + tmp + " to " + expire_date + ". The AMC may be further extended with a mutual consent.<br>";
        });

        html += '<b>b. No. Of Maximum Visits During AMC contract </b><br>';
        if (max_visit == 0) {
          html += 'There is no limit to the site visits during the contract period. For more details, please refer to the section 4 for the response time. <br>';
        } else {
          html += 'There are ' + max_visit + ' visits allowed during the contract period. For more details, please refer to the section 4 for the response time. <br>';
        }

        html += '<b>2. Procedure of Rendering Services</b> <br>';

        html += 'In case of any breakdown or any malfunction in the items mentioned in section 1b, vendor must be informed to register the complaint using SLA document provided after order confirmation. It is vendor responsibility to resolve the issue with-in the time frame mentioned in section 5. <br>';

        html += '<b>3.  Schedule Preventive Maintenance </b><br>';
        html += 'It is vendor’s responsibility to commence preventive maintenance during the AMC period. Schedule will be mutually decided by both parties. In case of any change from any party, an advance notice must be provided by the vendor or the client (whichever is applicable). The number of visits must be with the limits if defined under the section 1b.<br>';

        var customer_country = $("#customer_id option:selected").data('user-country-name');
        html += '<b> 4.  Response Time </b><br>';
        html += ' Below time lines excludes public holidays and after normal business hours which is between 9am to 5pm. Please note that onsite support will only be applicable within ' + customer_country + '. <br>';
        html += '<br>';

        html += '<table border="1" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:solid windowtext 1.0pt; margin-left:18.0pt">';
        html += '<tbody>';
        html += '<tr>';
        html += '<td style="width:460.8pt">';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Criticality Level : High </span></strong></p>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><span style="">Issues which cause the entire queue operation stops working. </span></p>';
        html += '<p>&nbsp;</p>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Response Time: </span></strong></p>';
        html += '<ul>';
        html += '<li><span style="">Telephonic &amp; Online Support </span><span style=""><span style="font-family:Wingdings">-></span></span><span style=""> Within 30 minutes</span></li>';
        html += '<li><span style="">On-Site Support (if applicable) </span><span style=""><span style="font-family:Wingdings">-></span></span><span style=""> Within 3 to 4 hours</span></li>';
        html += '</ul>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Resolution Time:</span></strong></p>';
        html += '<ul>';
        html += '<li><span style="">Same Day (if applicable) = Within 5 to 6 hours (Backup and / or parts will be provided &ndash; as required)</span></li>';
        html += '</ul>';
        html += '<p>&nbsp;</p>';
        html += '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td style="width:460.8pt">';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Criticality Level : Medium</span></strong></p>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><span style="">Issues in which the queue system is partially affected.</span></p>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm">&nbsp;</p>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Response Time: </span></strong></p>';
        html += '<ul>';
        html += '<li><span style="">Telephonic &amp; Online Support </span><span style=""><span style="font-family:Wingdings">-></span></span><span style=""> Within 1 hour </span></li>';
        html += '<li><span style="">On-Site Support (if applicable) </span><span style=""><span style="font-family:Wingdings">-></span></span><span style=""> Within 5 to 6 working hours or next working day</span></li>';
        html += '</ul>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Resolution Time:</span></strong></p>';
        html += '<ul>';
        html += '<li><span style="">Same day or Next Day </span><span style=""><span style="font-family:Wingdings">-></span></span><span style=""> Within 7 to 8 working hours (Backup and / or Parts within 7 to 8 working hours)</span></li>';
        html += '</ul>';
        html += '<p>&nbsp;</p>';
        html += '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td style="width:460.8pt">';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Criticality Level : Low</span></strong></p>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><span style="">Functional/ technical changes/ Upgrades required in the system.</span></p>';
        html += '<p>&nbsp;</p>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Response Time: </span></strong></p>';
        html += '<ul>';
        html += '<li><span style="">Telephonic &amp; Online Support </span><span style=""><span style="font-family:Wingdings">-></span></span><span style=""> Within 2 to 3 working hours</span></li>';
        html += '<li><span style="">On-Site Support (if applicable) </span><span style=""><span style="font-family:Wingdings">-></span></span><span style=""> based on the mutual understanding</span></li>';
        html += '</ul>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm"><strong><span style="">Resolution Time:</span></strong></p>';
        html += '<ul>';
        html += '<li><span style="">Based on the mutual understanding</span></li>';
        html += '</ul>';
        html += '<p style="margin-left:18.0pt; margin-right:0cm">&nbsp;</p>';
        html += '</td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';


        html += '<b>5.  Coverage Hours / Emergency Visits</b> <br>';
        html += 'All visits will be done during vendor’s business hours which are between 9am to 5pm and excludes public / statutory holidays. Please note that onsite support will only be applicable within ' + customer_country + '. <br>';

        html += '<b>6.  Escalation Matrix</b> <br>';
        html += 'Proper SLA will be furnished upon order confirmation. For the basic idea / concept please refer to the below details ';
        html += '<p style="margin-left:18.0pt"><strong><span style="">Escalation Level 1 </span></strong><strong><span style=""><span style="font-family:Wingdings">-></span></span></strong><strong> </strong><span style="">Project Coordinator</span></p>';
        html += '<p style="margin-left:18.0pt"><strong><span style="">Escalation Level 2 </span></strong><strong><span style=""><span style="font-family:Wingdings">-></span></span></strong><strong> </strong><span style="">Project Manager</span></p>';
        html += '<p style="margin-left:18.0pt"><strong><span style="">Escalation Level 3 </span></strong><strong><span style=""><span style="font-family:Wingdings">-></span></span></strong><strong> </strong><span style="">General Manager</span></p>';

        html += '<b>7.  System Tempering and Physical Damages</b> <br>';
        html += 'Any Hardware Fault due to Identifiable Physical Damage, Misuse or Power Fluctuation will not be covered and will be charged separately.<br>';

        var employee_name = ($('#employee_id').val() != null && $('#employee_id').val() != '') ? $('#employee_id').select2('data').text : '';
        var quotation_date = $("#quotation_date").val();
        var employee_designition = ($("#employee_id option:selected").data('position') !== undefined && $("#employee_id option:selected").data('position') != '') ? $("#employee_id option:selected").data('position') : '';
        
        html += '<div class="pagebreak"></div>';
        html += '<table align="left" border="1" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:solid windowtext 1.0pt; margin-left:6.0pt; margin-right:6.0pt; width:459.9pt">';
        html += '<tbody>';
        html += '<tr>';
        html += '<td style="width:239.4pt">';
        html += '<span style="font-size:12.0pt">Vendor : Smart Matrix General Trading LLC</span>';
        html += '</td>';
        html += '<td style="width:220.5pt">';
        html += '<span style="font-size:12.0pt">Client : ' + customer_name + '</span>';
        html += '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td style="width:239.4pt; height:100px; vertical-align:top;">';
        html += '<span style="font-size:12.0pt">Signature :</span>';
        html += '</td>';
        html += '<td style="width:220.5pt; height:100px; vertical-align:top;" >';
        html += '<span style="font-size:12.0pt;">Signature :</span>';
        html += '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td style="width:239.4pt">';
        html += '<span style="font-size:12.0pt">Name : ' + employee_name + '</span>';
        html += '</td>';
        html += '<td style="width:220.5pt">';
        html += '<span style="font-size:12.0pt">Name : </span>';
        html += '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td style="width:239.4pt">';
        html += '<span style="font-size:12.0pt">Designation : ' + employee_designition + '</span>';
        html += '</td>';
        html += '<td style="width:220.5pt">';
        html += '<span style="font-size:12.0pt">Designation : </span>';
        html += '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td style="width:239.4pt">';
        html += '<span style="font-size:12.0pt">Date : ' + quotation_date + '</span>';
        html += '</td>';
        html += '<td style="width:220.5pt">';
        html += '<span style="font-size:12.0pt">Date : </span>';
        html += '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td style="width:239.4pt; height:120px; vertical-align:top;" >';
        html += '<span style="font-size:12.0pt">Official Stamp : </span>';
        html += '</td>';
        html += '<td style="width:220.5pt;  height:120px; vertical-align:top;">';
        html += '<span style="font-size:12.0pt">Official Stamp : </span>';
        html += '</td>';
        html += '</tr>';
        html += '</tbody>';
        html += '</table>';
        html += '<p>&nbsp;</p>';

        return html;
      }

      function date_pattern(dt) {
        var this_date = (dt.getDate() > 0 && dt.getDate() < 10) ? "0" + dt.getDate() : dt.getDate();
        var this_month = (dt.getMonth() > -1 && dt.getMonth() < 9) ? "0" + (dt.getMonth() + 1) : (dt.getMonth() + 1);
        return dt.getFullYear() + "-" + this_month + "-" + this_date;
      }

      function find_date_days(start_date, end_date) {
        var oneDay = 24 * 60 * 60 * 1000; 
        var firstDate = new Date(start_date);
        var secondDate = new Date(end_date);
        return Math.round(Math.abs((firstDate - secondDate) / oneDay));
      }

      function convert_date_pattern(dt) {
        var d1 = dt.split("-");
        return new Date(d1[2], d1[1] - 1, d1[0]);
      }

      function round_value(num) {
         return num.toFixed(2);
      }

      function customized_round(num)
      {
       
        if(num.toString().includes("."))
        {
          num =  num.toString();
          let tmp =  num.split(".");
          var firstVal = tmp[0];

          var secVal = "00";
          if(tmp[1].length>=2)
           secVal= tmp[1].substring(0,2);

          if(tmp[1].length==1)
           secVal= tmp[1] + "0";

          var final = firstVal + "." + secVal;
          num =  Number(final);
          return num;
        }
        else{
          return num;
        }

      }
    </script>