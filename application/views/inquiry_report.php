
<style>
.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}


</style>
   
   <div class="div-controls div-font-controls" style="text-align:center;padding-top:-12%;">
      <h3 class="main-heading">Smart Matrix General Trading LLC</h3>
      <br>
      <h4 class="main-second-heading">Inquiry Received History</h4>
      <h4 class="main-second-heading"><b>From:</b> <?= $from_date?> <b>To:</b> <?= $to_date?></h4>
      
   </div>

   

   <table style="width:100%;border-collapse: collapse;border-top:2px solid #000;margin-bottom:15%;" class="table_align"> 
      <!-- first record -->
      <thead>
         <tr>
            <th align="center" width="60" style="width:20px;padding:5px 0 5px 10px;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
            <th align="center" width="60" style="width:40px;padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Date</b></th>
            <th align="center" width="60" style="width:200px;padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Company </b></th>
            <th align="center" width="100" style="width:70px;padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Person</b></th>
            <th align="center" width="80" style="width:80px;padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Contact No.</b></th>
            <th align="center" width="80" style="width:120px;padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Sales Person</b></th>
            <th align="center" width="100" style="width:180px;padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Remarks</b></th>
            
         </tr>
      </thead>
      <?php
         $i = 0;
         foreach ($data as $dt) {
             $i++;
         ?>
            
            <tr style="">
                <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0 5px 10px;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $i; ?></td>
                <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= date('d-M-Y', strtotime($dt['inquiry_received_history_date']))?></td>
                <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['inquiry_received_history_company_name']?></td>
                <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['inquiry_received_history_contact_person']?></td>
                <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['inquiry_received_history_contact_no']?></td>
                <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['user_name']?></td>
                <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 10px 5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['inquiry_received_history_remarks']?></td>
            </tr>
            
         <?php }?>
     
      
   </table>
   
   