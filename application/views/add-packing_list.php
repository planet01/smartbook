    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
        <div class="content">
            <ul class="breadcrumb">
                <li>
                    <p>Dashboard</p>
                </li>
                <li>
                    Packing List
                </li>
                <li><a href="#" class="active"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?>
                        <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
                    </a> </li>
            </ul>
            <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
            <!--  <h3><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
            <!--</div>-->
            <!-- BEGIN BASIC FORM ELEMENTS-->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
                        </div>
                        <div class="grid-body no-border">
                            <form class="ajaxForm validate" action="<?php echo site_url($add_product) ?>" method="post">
                                <div class="row">

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-sm-12 col-md-12">
                                                    <label class="form-label">Date</label>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="input-with-icon right controls">
                                                        <i class=""></i>
                                                        <input type="text" class="datepicker" name="packing_list_date" value="<?= @$data['packing_list_date'] ?>" placeholder="">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-sm-12 col-md-12">
                                                    <label class="form-label">Ref Delivery Note No</label>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="input-with-icon right controls">
                                                        <i class=""></i>
                                                        <select name="packing_list_dn" id="packing_list_dn" class="delivery_note " style="width:100%">
                                                        <option value="0" selected disabled>--- Select Delivery Note No ---</option>
                                                        <?php
                                                        foreach($delivery_note as $k => $v){
                                                        ?>
                                                        <option value="<?= $v['delivery_note_id']; ?>" <?=( $v['delivery_note_id'] == @$data['packing_list_dn'])? 'selected': ''; ?> data-customer_id='<?= $v['customer_id']?>' data-customer_name='<?= $v['user_name']?>' data-invoice_id = '<?= $v['invoice_id']?>' >
                                                        <?= @$setting["company_prefix"].@$setting["delivery_prfx"]; ?><?php echo $v['delivery_note_no']; ?>
                                                        </option>
                                                        <?php
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-sm-12 col-md-12">
                                                    <label class="form-label">Customer </label>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="input-with-icon right controls">
                                                        <i class=""></i>
                                                        <select name="customer_id" id="customer_id" class="select2 form-control">
                                                        <option value="0" selected disabled>--- Select Customer ---</option>
                                                        <?php
                                                        if($page_title == 'edit'){
                                                            foreach($customerData as $k => $v){
                                                            ?>
                                                            <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['customer_id'])? 'selected': ''; ?> >
                                                            <?= $v['user_name']; ?>
                                                            </option>
                                                            <?php
                                                            }
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-sm-12 col-md-12">
                                                    <label class="form-label">Ref Invoice No</label>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="input-with-icon right controls">
                                                        <i class=""></i>
                                                        <input type="text" class="" id="ref_invoice_no" name="ref_invoice_no" value="<?= @$data['ref_invoice_no'] ?>" placeholder="">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-sm-12 col-md-12">
                                                    <label class="form-label">Line Gap Before Remarks</label>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="input-with-icon right controls">
                                                        <i class=""></i>
                                                        <input type="text" class="txtboxToFilter" id="line_gap" name="line_gap" value="<?= @$data['line_gap'] ?>" placeholder="">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group" >
                                        <div class="row" >
                                            <div class="col-md-12">
                                            <div class="dataTables_wrapper-1440 DTs_wrapper-invoice" >
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                        <th class="text-center" style="width:60px">Add/Remove Row</th>
                                                        <th colspan="1" class="text-center" style="min-width:370px">Product</th>
                                                        <th class="text-center" style="min-width:130px">Delivery QTY</th>
                                                        <th class="text-center" style="min-width:150px">Carton Quantity</th>
                                                        <th class="text-center" style="min-width:150px">Gross Weight / Carton (KG)</th>
                                                        <th class="text-center" style="min-width:200px">Dimension /Package (CM)</th>
                                                        <th class="text-center" style="min-width:100px">Total Gross Weight (KG) - APP</th>
                                                        <th class="text-center" style="min-width:50px">Line Gap</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="customFields">
                                                        <tr class="txtMult">
                                                            <td class="text-center"><a href="javascript:void(0);" class="addCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                                            <td colspan="7"></td>
                                                        </tr>

                                                        <?php
                                                        $subtotal = 0;
                                                        // print_b($detail_data);
                                                        $count = 0;
                                                        if (isset($detail_data) && @$detail_data != null) {
                                                        foreach ($detail_data as $k => $v) {
                                                            $count++;
                                                        ?>
                                                             <tr class="txtMult">
                                                                <td class="text-center">
                                                                <a href="javascript:void(0);" class="remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>
                                                                </td>
                                                                <td colspan="1">
                                                                <select name="product_id[]" style="width: 100%;" id="product_id<?= $count?>" class=" prod_name" required>
                                                                <option selected disabled></option>
                                                                <?php foreach($productData as $k2 => $v2){ ?>
                                                                <option value="<?= $v2['product_id']; ?>" <?= ($v2['product_id'] == @$v['product_id']) ? 'selected' : ''; ?>><?= $v2['product_sku']; ?> -  <?= $v2['product_name']; ?></option>
                                                                <?php } ?>
                                                                </select>
                                                                <textarea name="description[]" id="description<?= $count?>" style="width:80%;display:inline;margin-top:10px" class="form-control prod_desc_text street" placeholder="" readonly="readonly">
                                                                <?= $v['description']?>
                                                                </textarea>
                                                                <a href="#" data-textarea_id="description<?= $count?>" style="margin: 10px 9px;" class="btn-warning btn btn-sm myModalBtnDesc" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil"></i>
                                                                </a>
                                                                </td>
                                                                <td>
                                                                <input type="text" class="txtboxToFilter text-right" style="width: 100%;"  id="delivery_qty<?= $count?>" name="delivery_qty[]" value="<?= $v['delivery_qty']?>" placeholder="" />
                                                                </td>
                                                                <td>
                                                                <input type="text" class="txtboxToFilter text-right" style="width: 100%;"  id="carton_qty<?= $count?>" name="carton_qty[]" value="<?= $v['carton_qty']?>"  placeholder="" />
                                                                </td>
                                                                <td>
                                                                <input type="text" class="txtboxToFilterCustom text-right" style="width: 100%;" id="gross_weight<?= $count?>" data-optional="0" name="gross_weight[]" value="<?= $v['gross_weight']?>" placeholder="" />
                                                                </td>
                                                                <td>
                                                                <input type="text" class="text-right" style="width: 100%x;" id="dimensions<?= $count?>" name="dimensions[]" value="<?= $v['dimensions']?>" placeholder="" />
                                                                </td>
                                                                <td>
                                                                <input type="text"  class="txtboxToFilter text-right" style="width: 100%;" id="total_gross_weight<?= $count?>" name="total_gross_weight[]" value="<?= $v['total_gross_weight']?>" placeholder="" />
                                                                </td>
                                                                <td>
                                                                <input type="text"  class="txtboxToFilter text-right" style="width: 100%;" id="line_gap_detail<?= $count?>" name="line_gap_detail[]" value="<?= $v['line_gap_detail']?>" placeholder="" />   
                                                                </td>
                                                                </tr>
                                                        <?php }
                                                          }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>  
                                        </div>
                                    </div>
                            




                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <div class="form-group text-center">
                                            <!--  <button class="btn btn-success btn-cons pull-right" type="submit"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> Category</button>-->
                                            </br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"><?= ($page_title == 'add') ? '' : ''; ?> Submit</button>
                                            <input name="id" type="hidden" value="<?= @$data['packing_list_id']; ?>">

                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="defaultModalDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 id="myModalLabel" class="semi-bold mymodal-customer-title">Edit Description</h4>
            </div>
            <div class="modal-body mymodal-customer-body" id="myModalDescription">
                  <div class="grid-body no-border">
                       <textarea id="myeditor" name="temp_quotation_desc" class="form-control" value="" placeholder=""></textarea>
                       <input type="hidden" id="textbox">
                  </div>
              <div class="modal-footer">
                <button class="btn btn-success edit_desc_button" type="button">
                  Submit
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>
       <!-- Modal -->
    <!-- END BASIC FORM ELEMENTS-->
    <script type="text/javascript">
        $(document).ready(function() {
            $('.delivery_note').select2({minimumInputLength: 3});
            $(".datepicker").datepicker({
                format: "yyyy-mm-dd",
                autoclose: true
            });

            $("form.validate").validate({
                rules: {
                    packing_list_date:
                    {
                        required:true
                    },
                    packing_list_dn:
                    {
                        required:true
                    },
                    customer_id: {
                        required: true
                    },
                    ref_invoice_no: {
                        required: true
                    },
                    "product_id[]": {
                       required: true
                    },
                    "line_gap_detail[]":{
                        required: true
                    }
                
                },
                messages: {
                   
                },
                invalidHandler: function(event, validator) {
                    //display error alert on form submit 
                    error("Please input all the mandatory values marked as red");

                },
                errorPlacement: function(label, element) { // render error placement for each input type   
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                    $('<span class="error"></span>').insertAfter(element).append(label);
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('success-control').addClass('error-control');
                },
                highlight: function(element) { // hightlight error inputs
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                    var parent = $(element).parent();
                    parent.removeClass('success-control').addClass('error-control');
                },
                unhighlight: function(element) { // revert the change done by hightlight
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent();
                    parent.removeClass('error-control').addClass('success-control');
                },
                success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
            });
            

            var add_button      = $("#customFields .addCF");
    var x = 1; 
    $(add_button).click(function(e){
      e.preventDefault();
      var customer = $('#customer_id').valid();
      //var invoice_currency = $("input[name='invoice_currency']").is(':checked').valid();
      if(!customer){
            error("Select Customer");
        return false;
      }
     
      $('form.validate').validate();
      // if(x < max_fields){
        x++;
        var temp = '<tr class="txtMult">';
        temp    += '<td class="text-center">';
        temp    += '<a href="javascript:void(0);" class="remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
        temp    += '</td>';
        temp    += '<td colspan="1">';
        temp    += '<select name="product_id[]" style="width: 100%;" id="product_id'+x+'" class=" prod_name" required>';
        temp    += '<option selected disabled></option>';
        temp    += '<?php foreach($productData as $k => $v){ ?>';
        temp    += '<option value="<?= $v['product_id']; ?>"><?= $v['product_sku']; ?>'+ ' - ' +' <?= $v['product_name']; ?></option>'; 
        temp    += '<?php } ?>';
        temp    += '</select>';
        temp    += '<textarea name="description[]" id="description'+x+'" style="width:80%;display:inline;margin-top:10px" class="form-control prod_desc_text street" placeholder="" readonly="readonly">';
        temp    += '</textarea>'
        temp    += '<a href="#" data-textarea_id="description'+x+'" style="margin: 10px 9px;" class="btn-warning btn btn-sm myModalBtnDesc" data-toggle="tooltip" title="Edit">'
        temp    += '<i class="fa fa-pencil"></i>'
        temp    += '</a>';
        temp    += '</td>';
        temp    += '<td>';
        temp    += '<input type="text" class="txtboxToFilter text-right" style="width: 100%;"  id="delivery_qty'+x+'" name="delivery_qty[]" value="" placeholder="" />';
        temp    += '</td>';
        temp    += '<td>';
        temp    += '<input type="text" class="txtboxToFilter text-right" style="width: 100%;"  id="carton_qty'+x+'" name="carton_qty[]"  placeholder="" />';
        temp    += '</td>';
        temp    += '<td>';
        temp    += '<input type="text" class="txtboxToFilterCustom text-right" style="width: 100%;" id="gross_weight'+x+'" data-optional="0" name="gross_weight[]" value="" placeholder="" />';
        temp    += '</td>';
        temp    += '<td>';
        temp    += '<input type="text" class="text-right" style="width: 100%x;" id="dimensions'+x+'" name="dimensions[]" value="" placeholder="" />';
        temp    += '</td>';
        temp    += '<td>';
        temp    += '<input type="text"  class="txtboxToFilter text-right" style="width: 100%;" id="total_gross_weight'+x+'" name="total_gross_weight[]" placeholder="" />';
        temp    += '</td>';
        temp    += '<td>';
        temp    += '<input type="text"  class="txtboxToFilter text-right" style="width: 100%;" id="line_gap_detail'+x+'" name="line_gap_detail[]" value="0" placeholder="" />';   
        temp    += '</td>';
        temp    += '</tr>';
        
        $("#customFields").append(temp);
        $("#product_id"+x).select2();
        
      // }
    });

    $("#customFields").on('click', '.remCF', function() {
       $(this).parent().parent().remove();
     });

    $(document).on("click", ".myModalBtnDesc", function(event){
        event.preventDefault();
        var row = $(this).closest('tr');
        var desc = row.find('.prod_desc_text').val();
        var textbox = $(this).data("textarea_id");
        $('#textbox').val(textbox);
        CKEDITOR.instances['myeditor'].setData(desc);
        $('#defaultModalDesc').modal('show');
    });
    $(document).on("click", ".edit_desc_button", function(event){
      var desc = $('#textbox').val();
      $('#'+desc).val(CKEDITOR.instances['myeditor'].getData());
      $('#defaultModalDesc').modal('hide');
    });

    $("#packing_list_dn").change(function(e) {
        var delivery_note_id = $(this).val();
        var delivery_note_customer_id = $(this).find(':selected').data('customer_id');
        var delivery_note_customer_name = $(this).find(':selected').data('customer_name');
        var delivery_note_invoice_id = $(this).find(':selected').data('invoice_id');
        var customer_html = '';
        customer_html    += '<option disabled> Select Customer </option>';
        customer_html    += '<option selected value="'+delivery_note_customer_id+'">'+delivery_note_customer_name+'</option>'; 
        $('#customer_id').empty();
        $('#customer_id').html(customer_html);
        $("#customer_id").trigger("change");

        $('#ref_invoice_no').val(delivery_note_invoice_id);
        $("#customFields").find("tr:gt(0)").remove();
        $.ajax({
              url: "<?php echo site_url('packing_list/find_delivery_note'); ?>",
              dataType: "json",
              type: "POST",
              data: {
                id: delivery_note_id
                    },
              cache: false,
              success: function(result) {
                for (var i = 0; i < result['delivery_note'].length; i++) {
                    x++;
                    var temp = '<tr class="txtMult">';
                    temp    += '<td class="text-center">';
                    temp    += '<a href="javascript:void(0);" class="remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
                    temp    += '</td>';
                    temp    += '<td colspan="1">';
                    temp    += '<select name="product_id[]" style="width: 100%;" id="product_id'+x+'" class=" prod_name" required>';
                    temp    += '<option disabled></option>';
                    temp    += '<option selected value="'+result['delivery_note'][i]['product_id']+'">'+result['delivery_note'][i]['product_sku']+' - ' +result['delivery_note'][i]['product_name']+'</option>'; 
                    temp    += '</select>';
                    temp    += '<textarea name="description[]" id="description'+x+'" style="width:80%;display:inline;margin-top:10px" class="form-control prod_desc_text street" placeholder="" readonly="readonly">';
                    temp    += result['delivery_note'][i]['delivery_note_detail_description'];
                    temp    += '</textarea>'
                    temp    += '<a href="#" data-textarea_id="description'+x+'" style="margin: 10px 9px;" class="btn-warning btn btn-sm myModalBtnDesc" data-toggle="tooltip" title="Edit">'
                    temp    += '<i class="fa fa-pencil"></i>'
                    temp    += '</a>';
                    temp    += '</td>';
                    temp    += '<td>';
                    temp    += '<input type="text" class="txtboxToFilter text-right" style="width: 100%;"  id="delivery_qty'+x+'" name="delivery_qty[]" value="'+result['delivery_note'][i]['delivery_note_detail_quantity']+'" placeholder="" />';
                    temp    += '</td>';
                    temp    += '<td>';
                    temp    += '<input type="text" class="txtboxToFilter text-right" style="width: 100%;"  id="carton_qty'+x+'" name="carton_qty[]"  placeholder="" />';
                    temp    += '</td>';
                    temp    += '<td>';
                    temp    += '<input type="text" class="txtboxToFilterCustom text-right" style="width: 100%;" id="gross_weight'+x+'" data-optional="0" name="gross_weight[]" value="" placeholder="" />';
                    temp    += '</td>';
                    temp    += '<td>';
                    temp    += '<input type="text" class="text-right" style="width: 100%x;" id="dimensions'+x+'" name="dimensions[]" value="" placeholder="" />';
                    temp    += '</td>';
                    temp    += '<td>';
                    temp    += '<input type="text"  class="txtboxToFilter text-right" style="width: 100%;" id="total_gross_weight'+x+'" name="total_gross_weight[]" placeholder="" />';
                    temp    += '</td>';
                    temp    += '<td>';
                    temp    += '<input type="text"  class="txtboxToFilter text-right" style="width: 100%;" id="line_gap_detail'+x+'" name="line_gap_detail[]" value="0" placeholder="" />';   
                    temp    += '</td>';
                    temp    += '</tr>';
                    
                    $("#customFields").append(temp);
                    $("#product_id"+x).select2();
                }
              }
            });
    });
    

});
    
        
    </script>