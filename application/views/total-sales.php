
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">Total Sales </a> </li>
            
        </ul>
        <div class="row">
<div class="col-md-12">
    <div class="row">
    
    <div class="col-md-3 receivable-subdiv-amount">
        <label class="text-center"><b>TOTAL SALES FOR THE DATE RANGE</b></label>
        <h3 class="text-center" style="font-weight: 600;">AED <?= isset($total_sales_count['total'])? $total_sales_count['total']:'0.00' ?></h3>
        <hr style="width:100%;text-align:right;margin-right:0;border: 3px solid lightblue;">
    </div>
    <div class="col-md-3 receivable-subdiv-amount">
        <label class="text-center"><b>NET SALES FOR THE DATE RANGE</b></label>
        <h3 class="text-center" style="font-weight: 600;">AED <?= isset($net_sales_count['total'])? $net_sales_count['total']:'0.00' ?></h3>
        <hr style="width:100%;text-align:right;margin-right:0;border: 3px solid lightblue;">
    </div>
    </div>
</div>
</div>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>Total Sales</h4>
                    </div>
                    <div class="grid-body">
                        <form class="row validate" style="margin-bottom:20px" id="dashboard-form" autocomplete="off">
                        
                        <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >Customer:</label>
                                <select name="customer_id" class="select2 form-control" >
                                    <option value>Select Customer</option>
                                    <?php
                                        foreach($customerData as $k => $v){
                                        ?>
                                        <option value="<?= $v['user_id']; ?>" <?= @$customer_id == $v['user_id']?'selected':''?>>
                                        <?= $v['user_name']; ?>
                                        </option>
                                        <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >Employee:</label>
                                <select name="employee_id" class="select2 form-control" >
                                    <option value>Select Employee</option>
                                    <?php
                                        foreach($employeeData as $k => $v){
                                        ?>
                                        <option value="<?= $v['user_id']; ?>" <?= @$employee_id == $v['user_id']?'selected':''?> >
                                        <?= $v['user_name']; ?>
                                        </option>
                                        <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >From Date:</label>
                                <input type="text" name="fromdate" value="<?=@$fromdate?>" id="fromdate" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3" >
                                <label >To Date:</label>
                                <input type="text" name="todate" value="<?=@$todate?>" id="todate" class="datepicker form-control">
                            </div>
                            <div class="col-sm-12" style="margin-bottom:25px">
                                <label>
                                <input type="checkbox" name="pendingpayment" class="radiobuttons" id="inlineRadio1" value="true" <?= isset($pendingpayment)?'checked':''?>><b> Exclude Pending Payments</b>
                                </label>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            
                        </form>
                        <table class="table" id="example3" >
                            <thead>
                                <tr>
                                    <th class="prod_sr_no">Sr.No.</th>
                                    <th class="prod_name">Date</th>
                                    <th>Invoice #</th>
                                    <th>Total Amount</th>
                                    <th>Discount</th>
                                    <th>Tax Amount</th>
                                    <th>Net Amount</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach($total_sales as $key =>$item){
                                    $rev = ($item['invoice_revised_no'] > 0)?'-R'.$item['invoice_revised_no'] : '';
                                    $rev1 = ($item['invoice_status'] == 2)?'-'.$item['invoice_revised_no'] : '';
                            ?>
                                <tr>
                                    <td><?= ++$key ?></td>
                                    <td><?= $item['invoice_date'] ?></td>
                                    <td><?php echo @$setting["company_prefix"].@$setting["invoice_prfx"]; ?><?php echo $item['invoice_no'].$rev ?></td>
                                    <td><?= $item['invoice_total_amount'] ?></td>
                                    <td><?= 0.00 ?></td>
                                    <td><?= $item['invoice_tax_amount'] ?></td>
                                    <td><?= $item['invoice_net_amount'] ?></td>
                                    
                                    <td>
                                    <a class="btn btn-primary" href="<?= site_url($print.'?id='.@$item['invoice_id'].'&header_check=0')?>" target='_blank' ><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        $( ".datepicker" ).datepicker({
           format: "dd-mm-yyyy",
           autoclose: true
           
        });
    });
    $("form.validate").validate({
      rules: { 
        fromdate:{
          required: true
        }, 
        todate:{
          required: true
        }
      }, 
      messages: {
        fromdate:"This field is required.",
        todate:"This field is required."
      },
        invalidHandler: function (event, validator) {
        //display error alert on form submit    
          error("Please input all the mandatory values marked as red");
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });
      $(document).ready(function() {
          $(".btnSearch").on("click", function(event){
              event.preventDefault();
                var date_from = $("#fromdate").valid();
                var date_to   = $("#todate").valid();
                if(!date_from || !date_to){
                    return false;
                }
                else
                {
                    document.getElementById('dashboard-form').submit();
                }
              
          });
      });
</script>