 <!-- BEGIN PAGE CONTAINER-->
 <div class="page-content">
   <div class="content">
     <ul class="breadcrumb">
       <li>
         <p>Dashboard</p>
       </li>
       <li>
         Invoice
       </li>
       <li><a href="#" class="active"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?>
           <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
         </a> </li>
     </ul>
     <!-- BEGIN BASIC FORM ELEMENTS-->
     <div class="row">
       <div class="col-md-12">
         <div class="grid simple">
           <div class="grid-title no-border">
             <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
           </div>
           <div class="grid-body no-border">
             <form class="ajaxForm validate" action="<?php echo site_url($save_product) ?>" method="post">
               <div class="row">


                 <div class="col-md-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Invoice No</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-9 col-md-12 col-sm-12  md-scr-pad">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>

                           <input readonly="readonly" type="text" value="<?php echo ($page_title != 'add') ? @$setval["company_prefix"] . @$setval["invoice_prfx"] . @$receipt_no : ''; ?>" class="form-control" placeholder="">
                         </div>

                       </div>
                     </div>
                   </div>
                 </div>
                 <input name="invoice_no" type="hidden" value="<?= ($page_title == 'add') ? 1 :  @$receipt_no; ?>">


                 <div class="col-md-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Invoice Date</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-8 col-md-10 col-sm-12  md-scr-pad">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <input style="padding: 4px 5px !important;" name="invoice_date" type="text" value="<?= @$data['invoice_date']; ?>" class="datepicker form-control" placeholder="">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="col-md-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Warranty Expiry Date</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-8 col-md-10 col-sm-12  md-scr-pad">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <input style="padding: 4px 5px !important;" name="invoice_expire_date" type="text" value="<?= @$data['invoice_expire_date']; ?>" class="datepicker form-control" placeholder="">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Customer</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="form-group">
                         <!-- <div class="input-with-icon right controls"> -->
                         <!-- <i class=""></i> -->
                         <div class="row">
                           <div class="col-lg-8 col-md-10 col-sm-12  inv-pd-r md-scr-pad">
                           <?php
                          if ($page_title == 'add' ) {
                          ?>
                             <select name="customer_id" id="customer_id" class="select2 form-control">
                               <option value="0" selected <?= (@$data['invoice_status'] == 3) ? '' : 'disabled' ?>>--- Select Customer ---</option>
                               <?php
                                foreach ($customerData as $k => $v) {
                                ?>
                                 <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['customer_id']) ? 'selected' : ''; ?>>
                                   <?= $v['user_name']; ?>
                                 </option>
                               <?php
                                }
                                ?>
                             </select>
                             <?php }else{
                               foreach ($customerData as $k => $v) 
                               {
                                 if($v['user_id'] == @$data['customer_id'])
                                 {
                                   $customer_name = $v['user_name'];
                                   $customer_id = $v['user_id'];
                                 }
                               }
                               ?>
                               <input type="text" readonly value= "<?= @$customer_name?>" class="form-control">
                               <input type="hidden" value= "<?= @$customer_id?>" name="customer_id">
                               <?php
                             } ?>

                             <input type="hidden" name="plan_type" id="plan_type" value="<?= @$data['plan_type'] ?>" placeholder="" />
                             <input type="hidden" name="customer_type" id="customer_type" value="<?= @$data['customer_type'] ?>" placeholder="" />
                             <input type="hidden" name="currency_id" id="currency_id" value="<?= @$data['currency_id'] ?>" placeholder="" />
                           </div>

                           <!-- <div class="row">
                                          <div class="col-md-4 col-sm-2 inv-pd-l-searchBtn">
                                            <button type="button" id="search_customer" class="search_customer btn-warning btn btn-sm"><span class="fa fa-search"></span></button>
                                          </div>
                                        </div> -->
                         </div>
                         <!-- </div> -->
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Proforma Invoice No.</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-8 col-md-10 col-sm-12  inv-proforma-input md-scr-pad">
                       <div class="form-group" style="margin-top: -5px;">
                         <!-- <div class="input-with-icon right controls"> -->
                         <!-- <i class=""></i> -->
                         <!-- <div class="row"> -->
                         <!-- <div class="col-md-6 col-sm-12  inv-proforma-input md-scr-pad"> -->
                         <label disabled type="number" value="<?php echo @$setval["proforma_invoice_prfx"]; ?>" class="prefix">
                           <?php echo @$setval["proforma_invoice_prfx"]; ?></label>
                         <!-- <input name="invoice_proforma_no" type="text" id="invoice_proforma_no" value="<?= @$data['invoice_proforma_no']; ?>" class="invoice_proforma_no form-control" placeholder=""> -->
                         <?php
                          if ($page_title == 'add' ) {
                          ?>
                           <select name="invoice_proforma_no" id="invoice_proforma_no" class="select2 invoice_proforma_no form-control">
                           </select>
                         <?php } else { ?>
                           <input readonly type="text" id="invoice_proforma_no" value="<?= (@$data['invoice_proforma_no'] != null && @$data['invoice_proforma_no'] != 0) ? @$setting["company_prefix"] . @$setting["quotation_prfx"] . @$data['invoice_proforma_no'] . find_rev_no(@$data['invoice_proforma_no'])  : '' ?>" class="invoice_proforma_no form-control" placeholder="">
                           <input type="hidden" name="invoice_proforma_no" id="invoice_proforma_no_hidden" value="<?= @$data['invoice_proforma_no']; ?>" class="invoice_proforma_no_hidden form-control" placeholder="">
                         <?php } ?>
                         <!-- </div> -->
                         <!--  <div class="col-lg-1 col-md-2 col-sm-1 mt-1 inv-proforma-searchBtn md-scr-mg-r">
                                     <button style="" type="button" class="fetch_proforma_invoice btn-warning btn btn-sm"><span class="fa fa-search"></span></button>
                                   </div>
                                   <div class="col-lg-4 col-md-4 col-sm-1 mt-1 inv-proforma-fetchBtn">
                                     <button style="" type="button" class="fetch_proforma_invoice btn-warning btn btn-sm">Fetch Data</button>
                                   </div> -->
                         <!-- </div> -->
                         <!-- </div> -->
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="clearfix"></div>

                 <div class="col-md-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Freight Type</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <div class="radio radio-success add-invoice-responsve-radio">
                             <input id="checkbox1" type="radio" name="freight_type" value="0" <?= (@$data['freight_type'] == 0) ? 'checked="checked"' : ''; ?>>
                             <label for="checkbox1"><b>C.I.F.</b></label>

                             <input id="checkbox2" type="radio" name="freight_type" value="1" <?= (@$data['freight_type'] == 1) ? 'checked="checked"' : ''; ?>>
                             <label for="checkbox2"><b>F.O.B.</b></label>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <!-- <div class="clearfix"></div> -->

                 <div class="col-md-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Employee</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-8 col-md-10 col-sm-12  inv-pd-r md-scr-pad">
                       <!-- <div class="col-md-12"> -->
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <!-- <div class="row"> -->
                           <select name="employee_id" id="employee_id" class="select2 form-control">
                             <option selected disabled>--- Select Employee ---</option>
                             <?php
                              foreach ($employeeData as $k => $v) {
                              ?>
                               <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['employee_id']) ? 'selected' : ''; ?>>
                                 <?= $v['user_name']; ?>
                               </option>
                             <?php
                              }
                              ?>
                           </select>
                         </div>
                         <!--  <div class="col-md-4 col-sm-2 inv-pd-l-searchBtn">
                                     <button type="button" id="search_customer" class="search_customer btn-warning btn btn-sm"><span class="fa fa-search"></span></button>
                                   </div> -->
                         <!-- </div> -->
                         <!-- </div> -->
                       </div>
                     </div>
                   </div>
                 </div>


                 <!-- <div class="clearfix"></div> -->
                 <div class="col-md-2">
                   <div class="row">
                     <div class="col-md-4">
                       <label class="form-label">Currency</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-10">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <!-- <label>BASE CURRENCY: <?= $base_currency ?></label> -->
                           <div class="row">
                             <div class="col-md-12 col-sm-12 ">
                               <div class="radio radio-success add-invoice-responsve-radio">
                                 <?php
                                  $count = 3;
                                  $required_check = 0;
                                  foreach ($currencyData as $k => $v) {
                                  ?>
                                   <input type="radio" class="invoice_currency" id="checkbox<?= $count; ?>" data-title="<?= $v['title'] ?>" name="invoice_currency" value="<?= $v['id']; ?>" <?= (@$data['invoice_currency_id'] == $v['id']) ? 'checked="checked"' : (($v['id'] == @$base_currency_id) ? 'checked="checked"' : ''); ?> <?= ($required_check == 0) ? 'required' : ''; ?>>
                                   <label for="checkbox<?= $count; ?>"><?= $v['title'] ?></label>
                                 <?php
                                    $required_check++;
                                    $count++;
                                  }
                                  ?>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="col-md-3 col-lg-3s">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Installation Location </label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <input type="text" readonly name="invoice_installation_location_details" id="invoice_installation_location_details" value="<?= @$data['invoice_installation_location_details']; ?>">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">End User</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12  inv-pd-r md-scr-pad">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <select name="end_user" id="end_user" class="select2 form-control">
                             <?php
                              if ($page_title == 'add') {
                              ?>
                               <option selected disabled>--- Select End User ---</option>
                             <?php } else {
                              ?>
                               <option disabled selected>--- Select End User ---</option>
                               <?php
                                foreach ($selected_customer_end_users as $k => $v) {
                                ?>
                                 <option data-city="<?= $v['city_name'] ?>" <?= (@$end_user['end_user_id'] == $v['end_user_id']) ? 'selected' : '' ?> value="<?= $v['end_user_id'] ?>"><?= $v['end_user_name'] ?></option>
                             <?php
                                }
                              } ?>


                           </select>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="clearfix"></div>
                 <div class="col-md-3 col-lg-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Line Break (after item)</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <input type="text" class='txtboxToFilter' name="invoice_report_line_break_item" value="<?= @$data['invoice_report_line_break_item']; ?>">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="col-md-3 col-lg-2">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Line Break (after payment)</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <input type="text" class='txtboxToFilter' name="invoice_report_line_break" value="<?= @$data['invoice_report_line_break']; ?>">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="clearfix"></div>

                 <div class="form-group">
                   <div class="row">
                     <div class="col-md-12">
                       <div class="dataTables_wrapper-1440 DTs_wrapper-invoice">
                         <table class="table table-bordered">
                           <thead>
                             <tr>
                               <th class="text-center" style="width:60px">Add/Remove Row</th>
                               <th colspan="1" class="text-center" style="min-width:370px">Product</th>
                               <th class="text-center" style="min-width:130px">P.I. Quantity</th>
                               <th class="text-center" style="min-width:150px">D.N. Quantity</th>
                               <th class="text-center" style="min-width:150px">Invoiced Quantity</th>
                               <th class="text-center" style="min-width:200px">Discount</th>
                               <th class="text-center" style="min-width:100px">Rate</th>
                               <th class="text-center" style="min-width:150px">Amount</th>
                             </tr>
                           </thead>
                           <tbody id="customFields">
                             <tr class="txtMult">
                               <td class="text-center"><a href="javascript:void(0);" class="addCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                               <td colspan="8"></td>
                             </tr>

                             <?php
                              $subtotal = 0;
                              // print_b($invoice_detail_data);
                              $x = 1000;
                              if (isset($invoice_detail_data) && @$invoice_detail_data != null) {
                                foreach ($invoice_detail_data as $k => $v) {
                                  $row_product_data = [];
                              ?>
                                 <tr class="txtMult">
                                   <td class="text-center" style="width: 60px">
                                     <?php foreach ($productData as $k => $v2) {
                                        if ($v2['product_id'] == @$v['product_id']) {
                                          $deliver = $v2['can_deliver'];
                                          break;
                                        } else {
                                          $deliver = 0;
                                        }
                                      }
                                      // if($deliver != 0){
                                      ?>
                                     <a href="<?= site_url("invoice/invoiceDetailDelete/" . $v['invoice_detail_id']); ?>" class="ajaxbtn remCF" rel="delete">Remove</a>
                                     <?php
                                      // } 
                                      ?>
                                   </td>
                                   <td colspan="1">
                                     <select name="product_id[]" id="pproduct_id<?= $k; ?>" class="form-control select2 prod_name" style="width: 350px;" required>
                                       <option selected disabled></option>
                                       <?php foreach ($productData as $k => $v2) { ?>
                                         <option value="<?= $v2['product_id']; ?>" <?= ($v2['product_id'] == @$v['product_id']) ? 'selected' : ''; ?>>
                                           <?= $v2['product_sku']; ?> -
                                           <?= $v2['product_name']; ?>
                                         </option>
                                       <?php
                                          $can_deliver = ($v2['product_id'] == @$v['product_id']) ? @$v2['can_deliver'] : 0;
                                          if ($v2['product_id'] == @$v['product_id']) {
                                            $row_product_data = $v2;
                                          }
                                        } ?>
                                     </select>
                                     <input type="hidden" name="can_deliver[]" value="<?= $can_deliver; ?>" placeholder="">
                                     <textarea name="invoice_detail_description[]" id="invoice_detail_description_old<?= $x ?>" class="form-control prod_desc_text street" style="width: 80%;display: inline;margin-top:10px" placeholder="" readonly="readonly"><?= @$v['invoice_detail_description']; ?></textarea>
                                     <a href="#" data-textarea_id="invoice_detail_description_old<?= $x ?>" style="margin: 10px 9px;" class="btn-warning btn btn-sm myModalBtnDesc" data-toggle="tooltip" title="Edit">
                                       <i class="fa fa-pencil"></i>
                                     </a>
                                   </td>
                                   <td>
                                     <input type="text" class="code invoice_detail_pi_quantity text-right" style="width: 100%;" readonly id="invoice_detail_pi_quantity<?= $k; ?>" name="invoice_detail_pi_quantity[]" value="<?= @$v['invoice_detail_pi_quantity']; ?>" placeholder="" />
                                   </td>
                                   <td>

                                     <!-- @$v['invoice_detail_dn_quantity']; -->
                                     <input type="text" readonly="readonly" class="code invoice_detail_dn_quantity text-right" style="width: 100%;" id="invoice_detail_dn_quantity<?= $k; ?>" name="invoice_detail_dn_quantity[]" value="<?= (find_dn_qunatity_by_qt_no($data['invoice_id'], @$v['product_id']) == '') ? 0 :  find_dn_qunatity_by_qt_no($data['invoice_proforma_no'], @$v['product_id']) ?>" placeholder="" />
                                     <input type="hidden" readonly="readonly" class="code invoice_detail_dn_quantity_old" style="width: 100%;" id="invoice_detail_dn_quantity<?= $k; ?>" name="invoice_detail_dn_quantity_old[]" value="<?= @$v['invoice_detail_dn_quantity']; ?>" placeholder="" />
                                   </td>
                                   <?php
                                    $total_inventory_quantity = inventory_quantity_by_product($v['product_id']);
                                    ?>
                                   <!--  <?= (@$v['invoice_detail_quantity'] == 'Lot' || $v['invoice_detail_quantity'] == 'lot') ? '' :  'min="' . @$v['invoice_detail_dn_quantity'] . '" max="' . $total_inventory_quantity['total_inventory_quantity'] . '"' ?> -->
                                   <td>
                                     <input type="text" class="code quantity txtboxToFilter quatity_num text-right" style="width: 100%;" id="quatity_num'+x+'" data-optional="0" name="invoice_detail_quantity[]" value="<?= @$v['invoice_detail_quantity']; ?>" placeholder="" <?php echo (@$v['invoice_detail_quantity'] != "Lot" && @$v['invoice_detail_quantity'] != "lot") ? "min='" . find_dn_qunatity_by_qt_no($data['invoice_id'], @$v['product_id']) . "' " : 'readonly'; ?> />
                                   </td>
                                   <td>
                                     <select name="invoice_detail_total_discount_type[]" style="width: 100%;" class="form-control discount_type">
                                       <option value="0" <?= ($v['invoice_detail_total_discount_type'] == 0) ? 'selected' : ''; ?>>-- select discount type --</option>
                                       <option value="1" <?= ($v['invoice_detail_total_discount_type'] == 1) ? 'selected' : ''; ?>>Percentage (%)</option>
                                       <option value="2" <?= ($v['invoice_detail_total_discount_type'] == 2) ? 'selected' : ''; ?>>Amount (Number)</option>
                                     </select>
                                     <br>
                                     <input type="text" class="code discount_amount txtboxToFilter text-right" style="width:200px;" id="discount_amount'+x+'" name="invoice_detail_total_discount_amount[]" value="<?= @$v['invoice_detail_total_discount_amount']; ?>" placeholder="" />
                                   </td>
                                   <td>
                                     <?php $readonly = (@$row_product_data['category_type'] == 2) ? '' : ''; ?>
                                     <input type="text" <?= $readonly ?> class="code pro_amount rate_num base_price txtboxToFilter text-right" style="width: 100%;" id="invoice_detail_base_rate'+x+'" name="invoice_detail_base_rate[]" value="<?= @$v['invoice_detail_base_rate'] ?>" placeholder="" />
                                     <input type="text" <?= $readonly ?> class="code pro_amount rate_num usd_price txtboxToFilter" style="width: 100%;" id="invoice_detail_usd_rate'+x+'" name="invoice_detail_usd_rate[]" value="<?= @$v['invoice_detail_usd_rate'] ?>" placeholder="" />
                                   </td>
                                   <td>
                                     <input type="text" style="width: 100%;" readonly="readonly" class="code amount  multTotal text-right " id="amount" name="invoice_detail_total_amount[]" value="<?= number_format(@$v['invoice_detail_total_amount'],2) ?>" placeholder="" />
                                   </td>
                                 </tr>

                             <?php
                                  $x++;
                                }
                              }
                              ?>

                           </tbody>

                           <tr>
                             <th colspan="2" rowspan="5">
                               <label><b>Details:</b></label>
                               <textarea name="invoice_notes" class="inv_txtarea_control">
                                              <?= @$data['invoice_notes']; ?></textarea>
                             </th>
                             <th colspan="5" class="text-right inv-vc">Total Amount</th>
                             <th colspan="2" class="text-center">
                               <input type="text" readonly="readonly" style="width: 100%" class="text-right" id="subtotal" name="invoice_subtotal" value="<?= @quotation_num_format($data['invoice_subtotal']); ?>" placeholder="0" />
                             </th>
                             <!--  <th class="text-center"><span id="subtotal">0</span></th> -->
                           </tr>

                           <tr>

                             <th colspan="2" class="text-center">
                               <span class="inv-vc" style="display: inline;padding-right: 15px;">Discount Type</span>
                               <select name="invoice_total_discount_type" class="form-control total_discount_type" style="width: 140px;display: inline;" id="total_discount_type">
                                 <option value="0" <?= (@$data['invoice_total_discount_type'] == 0) ? 'selected' : ''; ?>>-- select discount type --</option>
                                 <option value="1" <?= (@$data['invoice_total_discount_type'] == 1) ? 'selected' : ''; ?>>Percentage (%)</option>
                                 <option value="2" <?= (@$data['invoice_total_discount_type'] == 2) ? 'selected' : ''; ?>>Amount (Number)</option>
                               </select>
                             </th>
                             <th colspan="2" class="text-right inv-vc">
                               <input type="text" style="width:37%" class="total_discount_amount text-right" id="total_discount_amount" name="total_discount_amount" value="<?= @$data['invoice_total_discount_amount']; ?>" placeholder="0" />
                               <input type="text" style="width:58%" value="<?= @$data['discount_label']; ?>" class="discount_label" name="discount_label" />
                             </th>
                             <th class="text-right inv-vc">Discount Amount</th>
                             <th colspan="2" class="text-center">
                               <input type="text" readonly class="text-right" id="total_discount_amount_val" name="total_discount_amount_val" value="" placeholder="0" />
                             </th>
                           </tr>
                           <tr class="txtMult">
                             <th colspan="5" class="text-right inv-vc">Buyback Discount</th>
                             <th colspan="2" class="text-center">
                               <input type="text" style="width: 100%" class="buy_back text-right" id="buy_back" name="invoice_buyback" value="<?= @$data['invoice_buyback']; ?>" placeholder="0" />
                             </th>
                           </tr>
                           <tr class='tax_amount_row' style="<?= (@$setting['setting_company_country'] == @$data['user_country']) ? '' : 'display:none' ?>">
                             <th colspan="5" class="text-right inv-vc">Tax Amount</th>
                             <th colspan="2" class="text-center">
                               <input type="text" readonly="readonly" style="width: 100%" class="text-right" id="tax" name="invoice_tax_amount" value="<?= @$data['invoice_tax_amount'] ?>" placeholder="0" />
                               <input type="hidden" readonly="readonly" style="width: 100%" class="" id="tax_amount" name="invoice_tax" value="<?= (@$setting['setting_company_country'] == @$data['user_country']) ? @$setval['tax'] : 0; ?>" placeholder="0" />
                             </th>
                           </tr>
                           <tr>
                             <th colspan="5" class="text-right inv-vc">Net Amount</th>
                             <th colspan="2" class="text-center">
                               <input type="text" readonly="readonly" class="net_amount text-right" style="width: 100%" id="net_amount" name="invoice_net_amount" value="<?= quotation_num_format(@$data['invoice_net_amount']); ?>" placeholder="0" />
                             </th>
                             <!--  <th class="text-center"><span id="subtotal">0</span></th> -->
                           </tr>
                         </table>
                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="clearfix"></div>
                 <div class="form-group">
                   <h3>Payment Terms</h3>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="dataTables_wrapper-1440">
                         <table class="table table-bordered">
                           <thead>
                             <tr>
                               <th class="text-center inv-PayTerm-dt-th1">Add/Remove Row</th>
                               <th class="text-center inv-PayTerm-dt-th2">Terms & Condition</th>
                               <th class="text-center inv-PayTerm-dt-th3">Percentage</th>
                               <th class="text-center inv-PayTerm-dt-th4">Days</th>
                               <th class="text-center inv-PayTerm-dt-th5">Amount</th>
                             </tr>
                           </thead>
                           <tbody id="payment_customFields">
                             <tr class="payment_txtMult">
                               <td class="text-center"><a href="javascript:void(0);" class="payment_addCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                               <td colspan="5"></td>
                             </tr>
                             <?php
                              if (@$terms != null && !empty(@$terms)) {
                                $x = 0;
                                foreach ($terms as $row) {
                              ?>

                                 <tr class="txtMult payment_tr">
                                   <td class="text-center"><a href="javascript:void(0);" class="payment_remCF">Remove</a></td>
                                   <td>
                                     <input type="text" class="terms" required style="width:100%;" id="terms" name="terms[]" value="<?= @$row['payment_title'] ?>" placeholder="" />
                                   </td>
                                   <td>
                                     <input type="text" class="percentage text-right" required style="width:100%;" id="percentage_old<?= $x; ?>" data-optional="0" name="percentage[]" value="<?= @$row['percentage'] ?>" placeholder="" />
                                   </td>
                                   <td>
                                     <input type="text" class="payment_days text-right" required style="width:100%" id="payment_days_old<?= $x; ?>" data-optional="0" name="payment_days[]" value="<?= @$row['payment_days'] ?>" placeholder="" />
                                   </td>
                                   <td>
                                     <input type="text" readonly="readonly" style="width: 100%;" class="code quantity txtboxToFilter payment_amount text-right" id="amount_old<?= $x; ?>" name="payment_amount[]" value="<?= quotation_num_format(@$row['payment_amount']) ?>" placeholder="" />
                                   </td>
                                 </tr>
                             <?php $x++;
                                }
                              } ?>
                             <tr class="txtMult">
                               <th colspan="4" class="text-right inv-vc">Total Amount</th>
                               <th class="text-center">
                                 <input type="text" readonly="readonly" class="invoice_total_amount text-right" name="invoice_total_amount" value="<?= @quotation_num_format($data['invoice_total_amount']) ?>" placeholder="0">
                               </th>
                             </tr>

                           </tbody>
                         </table>
                       </div>
                     </div>
                   </div>
                 </div>
                 <!--  <div class="form-group">
                         <div class="row form-row col-md-6">
                           <div class="col-md-4">
                             <label class="form-label">Customer Note</label>
                           </div>
                             <div class="col-md-2">
                               <div class="input-with-icon right controls">
                                 <i class=""></i>
                                 <textarea name="invoice_customer_note" class="form-control" value="" placeholder=""><?= @$data['invoice_customer_note']; ?></textarea>
                               </div>
                           </div>
                         </div>
                       </div>
                       
                       <div class="form-group">
                         <div class="row form-row col-md-6">
                           <div class="col-md-4">
                             <label class="form-label">Terms & Conditions</label>
                           </div>
                           <div class="col-md-2">
                               <div class="input-with-icon right controls">
                                 <i class=""></i>
                                 <textarea name="invoice_terms_conditions" class="form-control" value="" placeholder=""><?= @$data['invoice_terms_conditions']; ?></textarea>
                               </div>
                           </div>
                         </div>
                       </div> -->



               </div>
               <div class="row">
                 <div class="col-md-12">
                   <div class="form-group text-center">
                     <input type="hidden" id="delivery_note_found" name="delivery_note_found" value="0" />
                     <input type="hidden" name="belongs_to" value="<?= @$data['belongs_to'] ?>" />
                     <input type="hidden" name="invoice_status" value="<?= @$data['invoice_status'] ?>" />
                     <input type="hidden" name="invoice_revised_no" value="<?= number_format(@$data['invoice_revised_no'] + 1); ?>" />
                     <input type="hidden" name="invoice_email_printed_status" value="<?= @$data['invoice_email_printed_status'] ?>" />
                     <input type="hidden" name="invoice_final" value="<?= @$data['invoice_final'] ?>" />
                     <input type="hidden" id="invoice_po_no" name="invoice_po_no" value="<?= @$data['invoice_po_no'] ?>" />
                     <input type="hidden" id="invoice_trigger" value="0" />


                     <!-- <a href="<?= site_url("Print_pdf"); ?>" target="_blank" class="btn btn-success btn-cons">Print Invoice</a> -->
                     <br>
                     <button class="btn btn-success btn-cons ajaxFormSubmitAlter " type="button"><?= ($page_title == 'add') ? '' : ''; ?>Submit</button>
                     <input name="id" type="hidden" value="<?= @$data['invoice_id']; ?>">

                     <input name="invoice_no_old" type="hidden" value="<?= @$data['invoice_no']; ?>">
                   </div>
                 </div>
               </div>
             </form>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <div class="modal fade" id="defaultModalDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h4 id="myModalLabel" class="semi-bold mymodal-customer-title">Edit Description</h4>
       </div>
       <div class="modal-body mymodal-customer-body" id="myModalDescription">
         <div class="grid-body no-border">
           <textarea id="myeditor" name="temp_quotation_desc" class="form-control" value="" placeholder=""></textarea>
           <input type="hidden" id="textbox">
         </div>
         <div class="modal-footer">
           <button class="btn btn-success edit_desc_button" type="button">
             Submit
           </button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>
       <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
   </div>
 </div>
 <!-- Modal -->
 <!-- END BASIC FORM ELEMENTS-->
 <!-- <input type="hidden" value="<?= @$data['invoice_currency_id']; ?>" id="base_currency_default_id"> -->
 <input type="hidden" value="<?= @$setval['currency_id']; ?>" id="base_currency_default_id">
 <input type="hidden" id="data_exist" value="0" placeholder="" />
 <script>
   var setting = "<?= addslashes(json_encode($setting)); ?>";
   $("form.validate").validate({
     rules: {
       customer_id: {
         required: true
       },
       invoice_detail_total_discount_amount: {
         number: true
       },
       invoice_no: {
         required: true
       },
       invoice_customer_note: {
         required: true
       },
       invoice_terms_conditions: {
         required: true,
       },
       invoice_date: {
         required: true
       },
       freight_type: {
         required: true
       },
       employee_id: {
         required: true
       },
     },
     messages: {
       customer_id: "This field is required.",
       "product_id[]": "This field is required.",
       "invoice_detail_quantity[]": {
         required: "This field is required.",
         number: "Please Insert Number."
       },
       "discount_amount[]": "Please Insert Number.",
       total_discount_amount: "Please Insert Number.",
       invoice_no: {
         required: "This field is required.",
         digits: "Please enter only digits."
       },
       invoice_customer_note: "This field is required.",
       invoice_terms_conditions: "This field is required."
     },
     invalidHandler: function(event, validator) {
       //display error alert on form submit    
       error("Please input all the mandatory values marked as red");
     },
     errorPlacement: function(label, element) { // render error placement for each input type   
       var icon = $(element).parent('.input-with-icon').children('i');
       icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
       $('<span class="error"></span>').insertAfter(element).append(label);
       var parent = $(element).parent('.input-with-icon');
       parent.removeClass('success-control').addClass('error-control');
     },
     highlight: function(element) { // hightlight error inputs
       var icon = $(element).parent('.input-with-icon').children('i');
       icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
       var parent = $(element).parent();
       parent.removeClass('success-control').addClass('error-control');
     },
     unhighlight: function(element) { // revert the change done by hightlight
       var icon = $(element).parent('.input-with-icon').children('i');
       icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
       var parent = $(element).parent();
       parent.removeClass('error-control').addClass('success-control');
     },
     success: function(label, element) {
       var icon = $(element).parent('.input-with-icon').children('i');
       icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
       var parent = $(element).parent('.input-with-icon');
       parent.removeClass('error-control').addClass('success-control');

     }
     // submitHandler: function (form) {
     // }
   });
   //my code//
   base_currency_id();

   function base_currency_id(base_currency_id) {
     return $("#base_currency_default_id").val();
   }
   //my code//
   $(".disable").each(function(i) {
     $(this).click(function() {

       $(".disable").attr("disabled", "disabled");
       //$("#checkbox1").attr("disabled", "disabled"); 

     });

   });

   $('input[type=radio][name=invoice_currency]').change(function() {
     count_row_price();
   });
   var checked = $("input[name='invoice_currency']:checked").val();
   console.log('Checked:' + checked);
   if (checked != base_currency_id()) {
     $('.base_price').hide();
     $('.usd_price').show();
   } else if (checked == base_currency_id()) {
     $('.base_price').show();
     $('.usd_price').hide();
   }
   $(document).ready(function() {
     var max_fields = 6;
     // var wrapper         = $(".amenities_field");
     var add_button = $("#customFields .addCF");
     var x = 1;
     // $(add_button).click(function(e){
     $("#customFields .addCF").live('click', function(e) {
       e.preventDefault();
       $(".gridAddBtn").remove();
       var customer = $('#customer_id').valid();
       //var invoice_currency = $("input[name='invoice_currency']").is(':checked').valid();
       if (!customer || !$("input[name='invoice_currency']").is(':checked')) {
         error("Click on customer search button is required");
         return false;
       }

       $('form.validate').validate();
       // if(x < max_fields){
       x++;
       var temp = '<tr class="txtMult">';
       temp += '<td class="text-center">';
       temp += '<a href="javascript:void(0);" class="addCF"><span class="glyphicon glyphicon-plus-sign gridAddBtn" style="font-size:25px;"></span></a><a href="javascript:void(0);" class="remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
       temp += '</td>';
       temp += '<td colspan="1">';
       temp += '<select name="product_id[]" style="width: 350px;" id="product_id' + x + '" class=" prod_name" required>';
       temp += '<option selected disabled></option>';
       temp += '<?php foreach ($productData as $k => $v) { ?>';
       temp += '<option value="<?= $v['product_id']; ?>"><?= $v['product_sku']; ?>' + ' - ' + ' <?= $v['product_name']; ?></option>';
       temp += '<?php } ?>';
       temp += '</select>';
       temp += '<input type="hidden" value="" name="can_deliver[]" class="can_deliver">';
       temp += '<textarea name="invoice_detail_description[]" id="invoice_detail_description' + x + '" style="width:80%;display:inline;margin-top:10px" class="form-control prod_desc_text street" placeholder="" readonly="readonly">';
       temp += '</textarea>'
       temp += '<a href="#" data-textarea_id="invoice_detail_description' + x + '" style="margin: 10px 9px;" class="btn-warning btn btn-sm myModalBtnDesc" data-toggle="tooltip" title="Edit">'
       temp += '<i class="fa fa-pencil"></i>'
       temp += '</a>';
       temp += '</td>';
       temp += '<td>';
       temp += '<input type="text" class="code text-right" style="width: 100%;" readonly id="invoice_detail_pi_quantity' + x + '" name="invoice_detail_pi_quantity[]" value="" placeholder="" />';
       temp += '</td>';
       temp += '<td>';
       temp += '<input type="text" class="code text-right" style="width: 100%;" readonly="readonly" id="invoice_detail_dn_quantity' + x + '" name="invoice_detail_dn_quantity[]" value="0" placeholder="" />';
       temp += '<input type="hidden" class="code invoice_detail_dn_quantity_old" style="width: 100%;" readonly="readonly" id="invoice_detail_dn_quantity_old' + x + '" name="invoice_detail_dn_quantity_old[]" value="1" placeholder="" />';
       temp += '</td>';
       temp += '<td>';
       temp += '<input type="text" class="code quantity txtboxToFilter quatity_num text-right" max="0" style="width: 100%;" id="quatity_num' + x + '" data-optional="0" name="invoice_detail_quantity[]" value="" placeholder="" />';
       temp += '</td>';
       temp += '<td>';
       temp += '<select name="invoice_detail_total_discount_type[]" style="width:100%;" class="form-control discount_type">';
       temp += '<option value="0">-- select discount type --</option>';
       temp += '<option value="1">Percentage (%)</option>';
       temp += '<option value="2">Amount (Number)</option>';
       temp += '</select>';
       temp += '<br><input type="text" class="code discount_amount txtboxToFilter text-right" style="width: 100%x;" id="discount_amount' + x + '" name="invoice_detail_total_discount_amount[]" value="" placeholder="" />';
       temp += '</td>';
       temp += '<td>';
       temp += '<input type="text"  class="code pro_amount rate_num base_price text-right" style="width: 100%;" id="invoice_detail_base_rate' + x + '" name="invoice_detail_base_rate[]" placeholder="" />';
       temp += '<input type="text" class="code pro_amount rate_num usd_price" style="width: 100%;" id="invoice_detail_usd_rate' + x + '" name="invoice_detail_usd_rate[]" placeholder="" />';
       temp += '</td>';
       temp += '<td>';
       temp += '<input type="text" readonly="readonly" class="code amount  multTotal text-right" id="amount' + x + '" name="invoice_detail_total_amount[]" style="width: 100%;" value="0" placeholder="" />';
       temp += '</td>';
       temp += '</tr>';

       $("#customFields").append(temp);
       $("#product_id" + x).select2();
       var checked = $("input[name='invoice_currency']:checked").val();
       if (checked != base_currency_id()) {
         $('.base_price').hide();
         $('.usd_price').show();
       } else if (checked == base_currency_id()) {
         $('.base_price').show();
         $('.usd_price').hide();
       }
       // }
     });
     $("#customFields").on('click', '.remCF', function() {
       //$(this).parent().parent().next().remove();
       $(this).parent().parent().remove();
       $(".txtMult input").trigger("change");
     });

   });

   function check_discount() {
     var check = 0;
     var discount = $('#total_discount_type').val();
     var discount_amount = $('#total_discount_amount').val();
     if ((discount == 1 && discount_amount > 0) || (discount == 2 && discount_amount > 0)) {
       check = 1;
     }

     if (check == 1) {

       $("tr.txtMult").each(function() {
         var row = $(this).closest('tr'); // get the row
         row.find('.discount_type').val(0);
         row.find('.discount_amount').val(0);
         row.find('.discount_amount').attr('readonly', true);

       });
       return false;
     }

     var check = 0;
     $("tr.txtMult").each(function() {
       var row = $(this).closest('tr'); // get the row
       var discount = row.find('.discount_type').val();
       var discount_amount = row.find('.discount_amount').val();
       if ((discount == 1 && discount_amount > 0) || discount == 2 && discount_amount > 0) {
         check = 1;
       }
     });
     if (check == 1) {
       $('#total_discount_type').val(0);
       $('#total_discount_amount').val(0);

     }
   }

   $(document).on('keydown keypress keyup change blur', ".total_discount_amount, .total_discount_type", function() {
     check_discount();
     var $subtotal = $("#subtotal").val()
     var $total_discount_amount = $('.total_discount_amount').val();
     var $total_discount_type = $('.total_discount_type').val();
     if ($total_discount_type != '') {
       $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
     }

     if ($total_discount_type == 1) {
       if ($total_discount_amount != '') {
         if ($total_discount_amount <= 100) {
           $total_discount_amount = ($total_discount_amount / 100) * $subtotal;
         } else {

           $total_discount_amount = $subtotal;
           $('.total_discount_amount').val(100);
           
         }
       }
     } else if ($total_discount_type == 2) {
       if ($total_discount_amount != '') {
         $total_discount_amount = $total_discount_amount;
       }
     } else {
       $('.total_discount_amount').val(0);
       $total_discount_amount = 0;
     }

     $total_discount_amount = round_value($total_discount_amount);
     $('#total_discount_amount_val').val($total_discount_amount.toFixed(2));

     var $grandtotal = $subtotal - $total_discount_amount;
    //  console.log("Total Amount -  Discount: "+$grandtotal);
     if ($grandtotal < 0) {
       $grandtotal = 0;
       $('.total_discount_amount').val($subtotal);
     }

     var tax_amount = $("#tax_amount").val();
     var buyback = $("#buy_back").val();
     buyback = (buyback == undefined || buyback == null || buyback == '') ? 0 : buyback;
     $grandtotal = $grandtotal - parseFloat(buyback);
     $grandtotal = round_value($grandtotal);
    //  console.log("(Total Amount -  Discount) - Buypack: "+$grandtotal);

     var find_tax = ($grandtotal / 100) * tax_amount;
     find_tax = round_value(find_tax);
     $('#tax').val(find_tax);
    //  console.log("Tax: "+find_tax);
    //  console.log("(Total + Tax: "+$grandtotal +" + "+ find_tax + " = "+ (find_tax + $grandtotal)) ;
     $('.net_amount').val((find_tax + $grandtotal).toFixed(2));
     $('#grandtotal').val($grandtotal);
     var tot_percentage = 0;
     var payment_amount = 0;
     var net_amount = (find_tax + $grandtotal).toFixed(2);
     $('.payment_tr').each(function() {
       var percentage = $(this).find(".percentage").val();
       var price = round_value((net_amount / 100) * parseFloat(percentage));
       if (Number.isNaN(price)) {
         price = 0;
       }
       payment_amount = payment_amount + price;
       $(this).find('.payment_amount').val(price.toFixed(2))
       tot_percentage += percentage;
     });

     var difference_rate = 0;
     var final_price = 0;
     var last_row = $('.payment_amount').last().val();
     last_row = Number.isNaN(last_row) ? 0 : last_row;
     if (net_amount > payment_amount) {
       difference_rate = net_amount - parseFloat(payment_amount);
       final_price = parseFloat(last_row) + parseFloat(difference_rate);
     } else if (payment_amount > net_amount) {
       difference_rate = parseFloat(payment_amount) - net_amount;
       final_price = parseFloat(last_row) - parseFloat(difference_rate);
     } else {
       final_price = parseFloat(last_row);
     }
     $('.payment_amount').last().val(final_price.toFixed(2));
     count_percentage();

   });

   /*function round_value(num){
  return Math.round(num).toFixed(2);
}*/
   //$(document).on('change', ".txtMult input",function () { 
   $(document).on('keydown keypress keyup change blur', ".txtMult input,.txtMult select, #buyback", function() {
     count_row_price();

   });

   function count_row_price() {
     var check = $(this).attr('data-optional');
     var mult = 0;
     // alert(123);
     $("#customFields tr.txtMult").each(function() {
       
       var $quatity_num = $('.quatity_num', this).val();
       if ($quatity_num == undefined) {
         $quatity_num = ($quatity_num == undefined) ? 0 : $quatity_num;
       }

       if ($quatity_num == "Lot" || $quatity_num == "lot") {
         $quatity_num = 1;
       }

       var checked = $("input[name='invoice_currency']:checked").val();
       if (checked != base_currency_id()) {
         var $rate_num = $('.usd_price', this).val();
         $('.base_price', this).hide();
         $('.usd_price', this).show();
       } else if (checked == base_currency_id()) {
         var $rate_num = $('.base_price', this).val();
         $('.base_price', this).show();
         $('.usd_price', this).hide();
       }

       $rate_num = round_value($rate_num);
       if ($rate_num == undefined) {
         $rate_num = ($rate_num == undefined) ? 0 : $rate_num;
       }

       var $discount_type = $('.discount_type', this).val();
       var $discount_amount = round_value($('.discount_amount', this).val());
       if ($discount_type != '') {
         $discount_amount = ($discount_amount != '') ? $discount_amount : 0;
       }
       // console.log("discount_type "+$discount_type);
       // console.log("discount_amount "+$discount_amount);

       if ($discount_type == 1) {

         if ($discount_amount != '') {
           if ($discount_amount <= 100) {
             $discount_amount = ($discount_amount / 100) * ($quatity_num * 1) * ($rate_num * 1);
           } else {
             $discount_amount = ($quatity_num * 1) * ($rate_num * 1);
             $('.discount_amount', this).val(100);
             // return false;
           }
         }
       } else if ($discount_type == 2) {
         if ($discount_amount != '') {
           $discount_amount = $discount_amount;
         }
       } else {
         $('.discount_amount', this).val(0);
         $discount_amount = 0;
       }

       $discount_amount = round_value($discount_amount);
       var $total = ($quatity_num * 1) * ($rate_num * 1) - $discount_amount;
       $total = round_value($total);
       if ($total < 0) {
         $total = 0;
         $('.discount_amount', this).val(round_value(($quatity_num * 1) * ($rate_num * 1)));
       }
       // else{
       //   console.log("else");
       //   $total = 0;
       // }
       $('.multTotal', this).val($total.toFixed(2)).text($total.toFixed(2));
       
       mult += $total;
     });
     mult = round_value(mult);
    
     $("#subtotal").val(mult.toFixed(2)).text(mult.toFixed(2));

     var $subtotal = mult;
     var $total_discount_amount = round_value($('.total_discount_amount').val());
     var $total_discount_type = $('.total_discount_type').val();
     if ($total_discount_type != '') {
       $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
     }

     if ($total_discount_type == 1) {
       if ($total_discount_amount != '') {
         if ($total_discount_amount <= 100) {
           $total_discount_amount = ($total_discount_amount / 100);
           $total_discount_amount = $total_discount_amount * $subtotal;
         } else {

           $total_discount_amount = $subtotal;
           $('.total_discount_amount').val(100);
           // return false;
         }
       }
     } else if ($total_discount_type == 2) {
       if ($total_discount_amount != '') {
         $total_discount_amount = $total_discount_amount;
       }
     } else {
       $('.total_discount_amount').val(0);
       $total_discount_amount = 0;
     }
     $total_discount_amount = round_value($total_discount_amount);
     var $grandtotal = round_value($subtotal - $total_discount_amount);

     if ($grandtotal < 0) {
       $grandtotal = 0;
       $('.total_discount_amount').val($subtotal);
     }

     $('#grandtotal').val($grandtotal).text($grandtotal);
     $(".total_discount_amount").trigger("change");
     var tax = ($("#tax_amount").val() > 0) ? round_value($("#tax_amount").val()) : 0;
     var buyback = ($("#buy_back").val() > 0) ? round_value($("#buy_back").val()) : 0;
     $grandtotal = round_value($grandtotal - buyback);
     var tax_val = round_value(($grandtotal / 100) * tax);
     tax_val = (tax_val);
     $("#tax").val(tax_val);
     var net_amount = round_value($grandtotal + tax_val);
     $('.net_amount').val(net_amount.toFixed(2));
     /*$('.invoice_total_amount').val(net_amount.toFixed(2));*/



     var tot_percentage = 0;
     var payment_amount = 0;
     

     $('.payment_tr').each(function() {
       var percentage = $(this).find(".percentage").val();
       var price = round_value((net_amount / 100) * parseFloat(percentage));
       if (Number.isNaN(price)) {
         price = 0;
       }
       payment_amount = payment_amount + price;
       $(this).find('.payment_amount').val(price.toFixed(2))
       tot_percentage += percentage;
     });

     var difference_rate = 0;
     var final_price = 0;
     var last_row = $('.payment_amount').last().val();
     last_row = Number.isNaN(last_row) ? 0 : last_row;
     if (net_amount > payment_amount) {
       difference_rate = net_amount - parseFloat(payment_amount);
       final_price = parseFloat(last_row) + parseFloat(difference_rate);
     } else if (payment_amount > net_amount) {
       difference_rate = parseFloat(payment_amount) - net_amount;
       final_price = parseFloat(last_row) - parseFloat(difference_rate);
     } else {
       final_price = parseFloat(last_row);
     }
     $('.payment_amount').last().val(final_price.toFixed(2));
     count_percentage();

   }

   function round_value(num) {
     var with2Decimals = 0.00;
     if (!isNaN(num) && num != "") {
       with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
     }
     
     return parseFloat(with2Decimals);
   }
   
   const format = (num, decimals) => num.toLocaleString('en-US', {
     minimumFractionDigits: 2,
     maximumFractionDigits: 2,
   });
   $(document).on('keydown keypress keyup change blur', ".percentage", function() {
     count_percentage();
   });
   count_percentage();

   function count_percentage() {
     var subtotal = 0;
     var total_percent = 100;
     var payment_amount = 0;
     $('.payment_tr').each(function() {
       var percentage = $(this).find(".percentage").val();
       payment_amount += parseFloat($(this).find(".payment_amount").val());
       subtotal += parseFloat(percentage);
     });
     $(".percentage_error").remove();
     if (subtotal != total_percent) {
       $('.percentage').last().after('<div class="percentage_error" style="display:none">Total perecentage should be equal to 100<div>');
       $(".percentage_error").css("display", "block").css("color", "red");
       $(".ajaxFormSubmitAlter").prop('disabled', true);
     } else {
       $(".percentage_error").css("display", "none");
       $(".ajaxFormSubmitAlter").prop('disabled', false);
     }

     var net_amount = $(".net_amount").val();
     net_amount = Number.isNaN(net_amount) ? 0 : net_amount;
     var difference_rate = 0;
     var final_price = 0;
     if (net_amount > payment_amount) {
       difference_rate = parseFloat(net_amount) - parseFloat(payment_amount);
       final_price = parseFloat(payment_amount) + parseFloat(difference_rate);
     } else if (payment_amount > net_amount) {
       difference_rate = parseFloat(payment_amount) - parseFloat(net_amount);
       final_price = parseFloat(payment_amount) - parseFloat(difference_rate);
     } else {
       final_price = parseFloat(payment_amount);
     }
     $('.invoice_total_amount').val(round_value(final_price));

   }

   $(document).on('click', ".remCF", function() {
     var mult = 0;
     // for each row:

     $("tr.txtMult").each(function() {
       // get the values from this row:
       var $quatity_num = $('.quatity_num', this).val();
       if ($quatity_num == undefined) {
         $quatity_num = ($quatity_num == undefined) ? 0 : $quatity_num;
       }
       if ($quatity_num == "Lot" || $quatity_num == "lot") {
         $quatity_num = 1;
       }
       var $rate_num = $('.rate_num', this).val();
       if ($rate_num == undefined) {
         $rate_num = ($rate_num == undefined) ? 0 : $rate_num;
       }

       var $discount_type = $('.discount_type', this).val();
       var $discount_amount = $('.discount_amount', this).val();
       if ($discount_type != '') {
         $discount_amount = ($discount_amount != '') ? $discount_amount : 0;
       }
       // console.log("discount_type "+$discount_type);
       // console.log("discount_amount "+$discount_amount);

       if ($discount_type == 1) {

         if ($discount_amount != '') {
           if ($discount_amount <= 100) {
             $discount_amount = ($discount_amount / 100) * ($quatity_num * 1) * ($rate_num * 1);
           } else {
             $discount_amount = ($quatity_num * 1) * ($rate_num * 1);
             $('.discount_amount', this).val(100);
             // return false;
           }
         }
       } else if ($discount_type == 2) {
         if ($discount_amount != '') {
           $discount_amount = $discount_amount;
         }
       } else {
         $('.discount_amount', this).val(0);
         $discount_amount = 0;
       }
       // console.log("discount_type2 "+$discount_type);
       // console.log("discount_amount2 "+$discount_amount);
       var $total = ($quatity_num * 1) * ($rate_num * 1) - $discount_amount;
       $total = round_value($total);
       if ($total < 0) {
         $total = 0;
         $('.discount_amount', this).val(($quatity_num * 1) * ($rate_num * 1));
       }
       // else{
       //   console.log("else");
       //   $total = 0;
       // }
       $('.multTotal', this).val($total.toFixed(2)).text($total.toFixed(2));
       mult += $total;
     });
     $("#subtotal").val(round_value(mult)).text(round_value(mult));

     var $subtotal = mult;
     var $total_discount_amount = $('.total_discount_amount').val();
     var $total_discount_type = $('.total_discount_type').val();
     if ($total_discount_type != '') {
       $total_discount_amount = ($total_discount_amount != '') ? $total_discount_amount : 0;
     }

     if ($total_discount_type == 1) {
       if ($total_discount_amount != '') {
         if ($total_discount_amount <= 100) {
           $total_discount_amount = ($total_discount_amount / 100) * $subtotal;
         } else {

           $total_discount_amount = $subtotal;
           $('.total_discount_amount').val(100);
           // return false;
         }
       }
     } else if ($total_discount_type == 2) {
       if ($total_discount_amount != '') {
         $total_discount_amount = $total_discount_amount;
       }
     } else {
       $('.total_discount_amount').val(0);
       $total_discount_amount = 0;
     }

     $total_discount_amount = round_value($total_discount_amount);
     var $grandtotal = $subtotal - $total_discount_amount;

     if ($grandtotal < 0) {
       $grandtotal = 0;
       $('.total_discount_amount').val($subtotal);
     }
     $('#grandtotal').val($grandtotal).text($grandtotal);

   });


   var $validator = $("form.validate").validate();
   $(".invoice_proforma_no").change(function(e) {
     if ($("#invoice_trigger").val() == 1) {
       $("#invoice_trigger").val(0);
       return false;
     }
     var errors;
     if (!$("#invoice_proforma_no").val() || $("#invoice_proforma_no").val() == 0) {
       errors = {
         invoice_proforma_no: "Please enter proforma no"
       };
       $validator.showErrors(errors);
     } else {
       var inv_no = $('#invoice_proforma_no').val();

       $("#customFields").find("tr:gt(0)").remove();
       $.ajax({
         url: "<?php echo site_url('invoice/fetch_proforma_invoice'); ?>",
         dataType: "json",
         type: "POST",
         data: {
           quotation_number: inv_no
         },
         cache: false,
         success: function(invoiceData) {
           $("#payment_customFields .txtMult:not(:first)").html('');
           if (invoiceData) {
             if (invoiceData['data'] != '') {
               var invoice_Data = invoiceData['data'][0];
               // $('#customer_id').val(invoice_Data['customer_id']);
               $('#invoice_po_no').val(invoice_Data['quotation_po_no']);
               // $('#customer_id').trigger('change'); 
               if (invoice_Data['quotation_currency'] != base_currency_id()) {
                 $('.base_price', this).hide();
                 $('.usd_price', this).show();
               } else if (invoice_Data['quotation_currency'] == base_currency_id()) {
                 $('.base_price', this).show();
                 $('.usd_price', this).hide();
               }
               $("input[name=invoice_currency][value=" + invoice_Data['quotation_currency'] + "]").attr('checked', 'checked');
               $('delivery_note_found').val(invoice_Data['delivery_note_found']);
               x = 5;
               html = '';
               for (var i = 0; i < invoiceData['data'].length; i++) {
                 var data = invoiceData['data'][i];
                 //console.log(data);
                 html += '<tr class="txtMult">';
                 html += '<td class="text-center" style="min-width:60px;">';
                 // if(data['can_deliver'] != "0"){
                 html += '<a href="javascript:void(0);" class="remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
                 // }
                 html += '</td>';
                 html += '<td colspan="1">';
                 html += '<select  style="width: 350px;" name="product_id[]" class="amc-quot-select-dsabled form-control prod_name"  id="5product_id' + x + '"  required>';
                 html += '<option selected disabled>--- Select Product ---</option>';
                 var total_inventory_quantity_val = 0;
                 var can_deliver = 0;
                 for (var j = 0; j < invoiceData['productData'].length; j++) {
                   if (data['prod_id'] == invoiceData['productData'][j]['product_id']) {
                     total_inventory_quantity_val = invoiceData['productData'][j]['total_inventory_quantity'];
                     can_deliver = invoiceData['productData'][j]['can_deliver'];
                     html += '<option value="' + invoiceData['productData'][j]['product_id'] + '" selected="selected">';
                   } else {
                     html += '<option value="' + invoiceData['productData'][j]['product_id'] + '">';
                   }
                   html += invoiceData['productData'][j]['product_sku'] + ' - ' + invoiceData['productData'][j]['product_name'];
                   html += '</option>';
                 }
                 html += '</select>';
                 html += '<input type="hidden" value="' + can_deliver + '" name="can_deliver[]" class="can_deliver">';
                 html += '<textarea name="invoice_detail_description[]" id="invoice_detail_description' + x + '" class="form-control prod_desc_text street" style="width:80%;display:inline;margin-top:10px" placeholder="" readonly="readonly">';
                 // html    += data['quotation_detail_description'];
                 html += '</textarea>'
                 html += '<a href="#" data-textarea_id="invoice_detail_description' + x + '" style="margin: 10px 9px;"class="btn-warning btn btn-sm myModalBtnDesc" data-toggle="tooltip" title="Edit">'
                 html += '<i class="fa fa-pencil"></i>'
                 html += '</a>';
                 html += '</td>';
                 html += '<td>';
                 var act_val = (data['total_sum_pi_quantity'] == 0) ? "Lot" : data['total_sum_pi_quantity'];
                 html += '<input type="text" class="code text-right" readonly style="min-width: 170px;" id="invoice_detail_pi_quantity' + x + '" name="invoice_detail_pi_quantity[]"  placeholder="" value="' + act_val + '"/>';
                 html += '</td>';
                 html += '<td>';
                 var dn_quantity = (data['total_sum_dn_quantity'] == null) ? 0 : data['total_sum_dn_quantity'];
                 html += '<input type="text" class="code text-right" style="min-width: 190px;" readonly="readonly" id="invoice_detail_dn_quantity' + x + '" name="invoice_detail_dn_quantity[]" placeholder="" value="' + dn_quantity + '"/>';
                 html += '<input type="hidden" class="code invoice_detail_dn_quantity_old" style="min-width: 190px;" readonly="readonly" id="invoice_detail_dn_quantity_old' + x + '" name="invoice_detail_dn_quantity_old[]" placeholder="" value="' + dn_quantity + '"/>';
                 html += '</td>';
                 html += '<td>';
                 var actual_value = (data['total_sum_dn_quantity'] > data['total_sum_pi_quantity']) ? data['total_sum_dn_quantity'] : data['total_sum_pi_quantity'];
                 //if(can_deliver != 1){
                 //min="'+dn_quantity+'" max="'+total_inventory_quantity_val+'"
                 var actual_value1 = (actual_value == 0) ? "Lot" : actual_value;
                 var min_val = (actual_value1 != "Lot" && actual_value1 != "lot") ? "min='" + dn_quantity + "'" : 'readonly';
                 html += '<input type="text" class="code quantity txtboxToFilter quatity_num text-right" style="min-width: 100%;" id="5quatity_num' + x + '" data-optional="0" name="invoice_detail_quantity[]" ' + min_val + '  value="' + actual_value1 + '"  placeholder="" />';
                 //}else{
                 /* html += '<input type="text" class="code quantity txtboxToFilter quatity_num" style="min-width: 100%;" readonly="readonly" id="5quatity_num'+x+'" data-optional="0" name="invoice_detail_quantity[]" value="Lot"  placeholder="" />';*/
                 //}
                 html += '<td>';
                 html += '<select name="invoice_detail_total_discount_type[]" style="width: 100%;" class="form-control discount_type">';
                 selected = '';
                 if (data['quotation_detail_total_discount_type'] == 0) {
                   selected = 'selected';
                 } else {
                   selected = '';
                 }
                 html += '<option value="0"  ' + selected + '>-- select discount type --</option>';
                 if (data['quotation_detail_total_discount_type'] == 1) {
                   selected = 'selected';
                 } else {
                   selected = '';
                 }
                 html += '<option value="1" ' + selected + '>Percentage (%)</option>';
                 if (data['quotation_detail_total_discount_type'] == 2) {
                   selected = 'selected';
                 } else {
                   selected = '';
                 }
                 html += '<option value="2" ' + selected + '>Amount (Number)</option>';
                 html += '</select>';
                 html += '<input type="text" class="code discount_amount txtboxToFilter text-right" style="min-width: 60px;" id="5discount_amount' + x + '" name="invoice_detail_total_discount_amount[]" value="' + data['quotation_detail_total_discount_amount'] + '" placeholder="" />';
                 html += '</td>';
                 html += '<td>';
                 var readonly = "";
                 if (data['category_type'] == 2) {
                   // readonly = "readonly";
                   readonly = "";
                 }
                 html += '<input type="text" ' + readonly + ' class="code pro_amount rate_num base_price text-right" style="min-width: 60px;" id="5quatity_num' + x + '" data-optional="0" name="invoice_detail_base_rate[]" value="' + data['quotation_detail_rate'] + '"  placeholder="" />';
                 html += '<input type="text" ' + readonly + ' class="code usd_price pro_amount rate_num" style="min-width: 60px;" id="5quatity_num' + x + '" data-optional="0" name="invoice_detail_usd_rate[]" value="' + data['quotation_detail_rate_usd'] + '"  placeholder="" />';
                 html += '</td>';
                 html += '<td>';
                 html += '<input type="text" readonly="readonly" class="code amount multTotal text-right" id="5amount' + x + '" name="invoice_detail_total_amount[]" style="min-width: 200px;" value="0" placeholder="" />';
                 html += '</td>';
                 html += '</tr>';

                 x++;
               }

               var quotation_total_discount_amount = data['quotation_total_discount_amount'];

               var quotation_buy_pack_discount = data['quotation_buypack_discount'];

               var quotation_total_discount_type = data['quotation_total_discount_type'];

               $('.total_discount_type option[value="' + quotation_total_discount_type + '"]').prop("selected", true);
               $('.total_discount_amount').val(quotation_total_discount_amount);
               $('#buy_back').val(quotation_buy_pack_discount);

               $("#customFields").append(html);
               $(".txtMult input").trigger("change");
               $(".total_discount_amount").trigger("change");
               count_row_price();
             }
             $("#data_exist").val('1');
             $("#search_customer").attr("disabled", "disabled");
             var pg = "<?= $page_title ?>";
             var status = "<?= isset($data['invoice_status']) ? $data['invoice_status'] : 0 ?>";
             if (pg != "add" && status != 3) {
               $("#customer_id").select2("readonly", true);
             }
             append_payment_terms(invoiceData.terms);
             //get_customer_data();
           }
         }
       });
       count_percentage();
     }
   });

   function ConfirmEmptyData() {
     if ($("#data_exist").val() == 1) {

       //console.log("custom ID "+$("#customer_id").val());
       swal({
           title: "Current data on the grid will be cleared. Are you sure?",
           icon: "warning",
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             $("#customFields").find("tr:gt(0)").remove();
             $('tbody#payment_customFields > tr').not(':first').not(':last').addClass('payment_highlight');
             $(".payment_highlight").remove();
             get_customer_data();
             count_percentage();
             find_invoices();
           } else {

           }
         });
     } else {
       return true;
     }
   }

   $(document).on('change', "#customer_id", function() {
     //$(document).on('click', "#search_customer",function () {
     //if($("#data_exist").val() == 1 ){
     $('#invoice_installation_location_details').val('');
     swal({
         title: "Current data on the grid will be cleared. Are you sure?",
         icon: "warning",
         buttons: true,
         dangerMode: true,
       })
       .then((willDelete) => {
         if (willDelete) {
           $("#customFields").find("tr:gt(0)").remove();
           $('tbody#payment_customFields > tr').not(':first').not(':last').addClass('payment_highlight');
           $(".payment_highlight").remove();
           get_customer_data();
           count_percentage();
           find_invoices();
         }
       });
     //}
     $("#data_exist").val(1);

   });
   var page = "<?= $page_title ?>";
   var status = "<?= isset($data['invoice_status']) ? $data['invoice_status'] : 0 ?>";
   if (page != "add" && status == 3) {
     $("#invoice_trigger").val(1);
     find_invoices(1);
   }
   if(page != "add")
   {
    $('.total_discount_amount').trigger('keypress');
   }

   function find_invoices(is_edit = 0) {
     $.ajax({
       url: "<?php echo site_url('invoice/find_invoices'); ?>",
       dataType: "json",
       type: "POST",
       data: {
         customer_id: $("#customer_id").val(),
       },
       cache: false,
       success: function(data) {
         html = "";
         html += "<option value='0'>---Select P.I---</option>";
         data = data.proforma_invoice;
         var this_setting = JSON.parse(window.setting);
         var prefix = this_setting['company_prefix'];
         prefix = prefix + this_setting['quotation_prfx'];
         for (var i = 0; i < data.length; i++) {
           var title = (data[i].revised_no > 0) ? data[i].invoice_num + '-R' + data[i].revised_no : data[i].invoice_num;
           if (data[i].total_invoice < 1 || $("#invoice_proforma_no_hidden").val() == data[i].invoice_num) {
             if ($("#invoice_proforma_no_hidden").val() == data[i].invoice_num) {
               html += "<option selected value='" + data[i].invoice_num + "'>" + prefix + title + "</option>"
             } else {
               html += "<option value='" + data[i].invoice_num + "'>" + prefix + title + "</option>"
             }
           }
         }
         $('#invoice_proforma_no').html(html).trigger('change');
       }
     });
   }

   function get_customer_data() {
     var c_id = $('#customer_id').val();
     //$("#customer_type").val('');
     var this_setting = JSON.parse(window.setting);
     $("#plan_type").val('');
     $("#currency_id").val('');
     $.ajax({
       url: "<?php echo site_url('invoice/customer_data'); ?>",
       dataType: "json",
       type: "POST",
       data: {
         id: c_id
       },
       cache: false,
       success: function(customerData) {
         if (customerData) {
           $('#invoice_installation_location_details').val(customerData.user_city);
           if (customerData.user_country != this_setting['setting_company_country']) {
             $('.tax_amount_row').hide();
             $('#tax').val(0);
             $('#tax_amount').val(0);
           } else {
             $('.tax_amount_row').show();
             $('#tax_amount').val(this_setting['tax'])
           }
           //$("#customer_type").val(customerData.customer_type);
           $("#plan_type").val(customerData.plan_type);
           $("#currency_id").val(customerData.currency_id);
           if (customerData.currency_id != base_currency_id()) {
             $('.base_price', this).hide();
             $('.usd_price', this).show();
           } else if (customerData.currency_id == base_currency_id()) {
             $('.base_price', this).show();
             $('.usd_price', this).hide();
           }
           $("input[name=invoice_currency][value=" + customerData.currency_id + "]").attr('checked', 'checked');
           append_payment_terms(customerData.terms);
           var html = '';
           html += "<option value='0'>---Select End User---</option>"
           for (var i = 0; i < customerData.end_user.length; i++) {
             html += "<option data-city='" + customerData.end_user[i]['city_name'] + "' value='" + customerData.end_user[i]['end_user_id'] + "'>" + customerData.end_user[i]['end_user_name'] + "</option>";
           }
           $('#end_user').html(html).trigger('change');
         }
       }
     });
   }
   $(document).on('change', "#end_user", function() {
     if ($("#end_user option:selected").val() != 0) {
       $('#invoice_installation_location_details').val($("#end_user option:selected").data('city'));
     }

   });

   $(document).on('change', ".prod_name", function() {
     var prod_name = $(this).parent().parent('tr').find('.prod_name').val();
     var dataString = 'pid=' + prod_name + '&user_id=' + $('#customer_id').val() + '&plan_type=' + $('#plan_type').val() + '&customer_type=' + $('#customer_type').val();
     var row = $(this).closest('tr'); // get the row
     var street = row.find('.street'); // get the other select in the same row
     var pro_amount = row.find('.pro_amount');
     var quatity_num = row.find('.quatity_num');
     var dc = $(this).closest('tr').next('tr').find('.prod_desc_text');
     $.ajax({
       url: "<?php echo site_url('invoice/progt'); ?>",
       dataType: "json",
       type: "POST",
       data: dataString,
       cache: false,
       success: function(employeeData) {
         if (employeeData['error']) {
           error(employeeData['error']);
         }
         if (employeeData) {
           console.log(employeeData.title.trim().toLowerCase());
          var category_type = employeeData.title.trim().toLowerCase();
           if (category_type != 'service') {
             quatity_num.attr("max", employeeData.total_inventory_quantity);
             quatity_num.removeAttr("max");
             quatity_num.removeAttr("readonly");
             quatity_num.val("0");
           } else {
             quatity_num.removeAttr("max");
             quatity_num.removeAttr("min");
             quatity_num.attr("readonly", "readonly");
             quatity_num.val("Lot");
           }
           pro_amount.val(employeeData.product_price);
           var desc = employeeData.product_description;
          //  street.text($(desc).text());
          //  if (desc) {
          //    dc.text(desc.replace(/(<([^>]+)>)/ig, ""));
          //  }
           row.find('.base_price').val(employeeData.base);
           row.find('.usd_price').val(employeeData.usd);
           if (employeeData.category_type == 2) {
             /*row.find('.base_price').attr("readonly", "readonly");
             row.find('.usd_price').attr("readonly", "readonly");*/
             row.find('.base_price').removeAttr("readonly");
             row.find('.usd_price').removeAttr("readonly");
           } else {
             row.find('.base_price').removeAttr("readonly");
             row.find('.usd_price').removeAttr("readonly");
           }
           row.find('.can_deliver').val(employeeData.can_deliver);
           /* $("#invoice_currency").
            var $radios = $('input:radio[name=invoice_currency]');
            if($radios.is(':checked') === false) {
                $radios.filter('[value=Male]').prop('checked', true);
            }*/

         } else {
           $("#heading").hide();
           $("#records").hide();
           $("#no_records").show();
         }
       }
     });
   });

   $(document).on("click", ".myModalBtnDesc", function(event) {
     event.preventDefault();
     var row = $(this).closest('tr');
     var desc = row.find('.prod_desc_text').val();
     var textbox = $(this).data("textarea_id");
     $('#textbox').val(textbox);
     CKEDITOR.instances['myeditor'].setData(desc);
     $('#defaultModalDesc').modal('show');
   });
   $(document).on("click", ".edit_desc_button", function(event) {
     var desc = $('#textbox').val();
     $('#' + desc).val(CKEDITOR.instances['myeditor'].getData());
     $('#defaultModalDesc').modal('hide');
   });


   $(document).ready(function() {

     $("#user_type").change(function() {
       var val = $(this).val();
       if (val == 2) {
         $('.myUserType').slideDown();
       } else {
         $('.myUserType').slideUp();
       }
     });

     // console.log(dateToday); 


     $(".datepicker").datepicker({
       format: "yyyy-mm-dd",
       autoclose: true

     });


   });

   $(document).ready(function() {

     $('.select2', "form.validate").change(function() {
       $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
     });
     $('#fileUploader').fileuploader({
       changeInput: '<div class="fileuploader-input">' +
         '<div class="fileuploader-input-inner">' +
         '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
         '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
         '<p>or</p>' +
         '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
         '</div>' +
         '</div>',
       theme: 'dragdrop',
       // limit: 4,
       // extensions: ['jpg', 'jpeg', 'png', 'gif'],
       onRemove: function(item) {

       },
       captions: {
         feedback: 'Drag and drop files here',
         feedback2: 'Drag and drop files here',
         drop: 'Drag and drop files here'
       },
     });
   });
   var max_fields = 6;
   var add_button = $("#payment_customFields .payment_addCF");
   var x = 1;
   var box_size = "3";
   $(add_button).click(function(e) {
     e.preventDefault();
     $('form.validate').validate();
     // if(x < max_fields){
     x++;
     var temp = payment_row(x);
     //$("#payment_customFields").append(temp);
     $("#payment_customFields").closest('table').find('tr:last').prev().after(temp);
     count_percentage();

     // }
   });
   $("#payment_customFields").on('click', '.payment_remCF', function() {
     $(this).parent().parent().remove();
     count_percentage();
   });

   function append_payment_terms(term) {
     $(".payment_tr").remove();
     for (var i = 0; i < term.length; i++) {
       var temp = payment_row('dynamic_' + i, term[i]);
       //$("#payment_customFields").append(temp);
       $("#payment_customFields").closest('table').find('tr:last').prev().after(temp);
       //console.log(temp+" jhkjbjk");

     }
     count_row_price();
   }

   function payment_row(x, data) {
     //console.log(data);
     var payment_title = (data == undefined) ? "" : data.payment_title;
     var percentage = (data == undefined) ? "" : data.percentage;
     var payment_days = (data == undefined) ? "" : data.payment_days;
     var temp = '<tr class="txtMult payment_tr">';
     temp += '<td class="text-center" style="min-width:60px;"><a href="javascript:void(0);" class="payment_remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>';
     temp += "<td>";
     temp += '<input type="text" class="code quantity txtboxToFilter terms" required  style="width:100%;" id="terms' + x + '" data-optional="0" name="terms[]" value="' + payment_title + '" placeholder="" />';
     temp += '</td>';
     temp += "<td>";
     temp += '<input type="text" class="code quantity txtboxToFilter percentage text-right" required  style="width:100%;" id="percentage' + x + '" data-optional="0" name="percentage[]" value="' + percentage + '" placeholder="" />';
     temp += '</td>';
     temp += "<td>";
     var p_days = (payment_days == undefined) ? 0 : payment_days;
     temp += '<input type="text" class="code quantity txtboxToFilter payment_days text-right" required  style="width:100%" id="payment_days' + x + '" data-optional="0" name="payment_days[]" value="' + p_days + '" placeholder="" />';
     temp += '</td>';
     temp += "<td>";
     temp += '<input type="text" class="code quantity txtboxToFilter payment_amount text-right" required  style="width:100%;" id="payment_amount' + x + '" data-optional="0" name="payment_amount[]" value="" placeholder="" />';
     temp += '</td>';
     temp += '</tr>';
     return temp;
   }

   $('document').ready(function() {
     $('.inv_txtarea_control').each(function() {
       $(this).val($(this).val().trim());
     });
   });
 </script>