  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>View All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h4>
              <!--<div class="pull-right">-->
              <!--  <a href="<?php echo site_url($add_product)?>" class="btn btn-info btn-cons">Add New <?= (isset($page_heading))?$page_heading:''; ?></a>-->
              <!--</div>-->
            </div>
            <div class="grid-body ">
              <table class="table" id="example3" >
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>User Type</th>
                    <th>Status</th>
                    <th width="200px">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if(@$data != null && !empty(@$data)){
                    $count=1;
                    // print_b($data);
                    foreach( $data as $k=>$v ){
                        ?>
                  <tr class="">
                    <td>
                      <?php
                        $current_img = '';
                        $file_path = dir_path().$image_upload_dir.'/'.@$v['image'];
                        if (!is_dir($file_path) && file_exists($file_path)) {
                          $current_img = site_url($image_upload_dir.'/'.$v['image']);
                        }else{
                          $current_img = site_url("uploads/dummy.jpg");
                        }
                      ?>
                      <img src="<?= $current_img; ?>" width="100"/>
                    </td>
                    <td><?= $v['fname']." ".$v['lname']; ?></td>
                    <td><?= $v['email']; ?></td>
                    <td><?= $v['phone']; ?></td>
                    <td align="center">
                      <?php
                        if ($v['user_type'] == 3) {
                      ?>
                        <span class="label label-primary">Customer</span>
                      <?php
                        }else if ($v['user_type'] == 2) {
                      ?>
                        <span class="label label-warning">Agent</span>
                      <?php
                        }else{
                          echo "N/A";
                        }
                      ?>
                    </td>
                    <td align="">
                      <?php
                        if ($v['is_active'] == 1) {
                      ?>
                        <span class="label label-success">Enabled</span>
                      <?php
                        }else{
                      ?>
                        <span class="label label-danger">Disabled</span>
                      <?php
                        }
                      ?>
                    </td>
                    <td class="">
                      <a href="#" class="btn-primary btn btn-sm myModalBtn" data-id="<?= $v['id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                      
                      <a href="<?php echo site_url($edit_product.'/'.$v['id'])?>" class="btn-warning btn btn-sm"><i class="fa fa-pencil"></i></a>
                      <!-- <a href="<?php echo site_url($delete_product.'/'.$v['id'])?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a> -->
                    </td>
                  </tr>
                <?php
                      $count++;
                    }
                  }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
    <!-- /.modal -->
  </div>
