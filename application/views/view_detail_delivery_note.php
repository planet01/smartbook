 <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'':'View'; ?> <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4>View <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold"></span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($save_product)?>" method="post">
                    <div class="row">
                        
                        <div class="col-md-3">
                          <div class="row">
                            <div class="col-md-12">
                              <label class="form-label">Delivery Note No</label>
                            </div>
                          </div>
                          <div class="row">
                            <!-- <div class="col-md-12"> -->
                            <div class="col-lg-7 col-md-9 col-sm-8">
                              <div class="form-group">
                                <div class="right controls" id="span-pre">
                                  <i class=""></i>
                                    <!-- <div class="row"> -->
                                  <input type="hidden" name='delivery_prfx' value='<?= @$setval["delivery_prfx"]?>'>
                                  <input name="delivery_note_no_temp" type="text" readonly value="<?= ($page_title == 'add')?@$setval["company_prefix"].@$setval["delivery_prfx"].serial_no_delivery_note():@$setval["company_prefix"].@$setval["delivery_prfx"].@$data['delivery_note_no']; ?>" class="form-control" placeholder="" >
                                  <input name="delivery_note_no" type="hidden" value="<?= ($page_title == 'add')?serial_no_delivery_note():@$data['delivery_note_no']; ?>" class="form-control" placeholder="">
                                </div>
                              <!-- </div> -->
                                </div>
                              </div>
                            <!-- </div> -->
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="row">
                            <div class="col-md-6">
                              <label class="form-label">Customer</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-8">
                              <div class="form-group">
                               
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <select name="customer_id" disabled id="customer_id" class="select2 form-control">
                                    <option value="0" selected disabled>--- Select Customer ---</option>
                                    <?php
                                    foreach($customerData as $k => $v){
                                    ?>
                                    <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['customer_id'])? 'selected': ''; ?> >
                                    <?= $v['user_name']; ?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                  <!-- 1 => performa invoice, 2 => invoice -->
                                            
                                </div>
                                    
                                </div>
                            </div>
                          </div>
                        </div>

                        <!-- <div class="col-md-4">
                          <div class="row">
                            <div class="col-md-12">
                              <label class="form-label">Stock Delivered From</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-8">
                              <div class="form-group">
                                <div class="input-with-icon right controls" id="span-pre">
                                  <i class=""></i>       
                                    <select name="warehouse_id" disabled style="width:100%"  id="warehouse_id" class="form-control warehouse_id" required>
                                      <option selected disabled>--- Select Location ---</option>
                                      <?php 
                                        foreach($warehouseData as $k => $v){ 
                                        ?>
                                        <option <?=( @$data['warehouse_id'] == $v['warehouse_id'] )? 'selected ': '' ?>
                                        value="<?= $v['warehouse_id']; ?>">
                                            <?= $v['warehouse_name']; ?>
                                        </option>
                                      <?php } ?>
                                    </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> -->

                        <div class="col-md-2">
                          <div class="row">
                            <div class="col-md-6">
                              <label class="form-label">Date</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                              <div class="form-group">
                                <div class=" right controls" id="span-pre">
                                  <i class=""></i>
                                  <input type="text" disabled class="datepicker" name="delivery_date" value="<?= @$data['delivery_date'] ?>" placeholder="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-4">
                          <div class="row">
                            <div class="col-md-12">
                              <label class="form-label">Proforma Invoice / Invoice No.</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-7 col-md-8 col-sm-8">
                              <div class="form-group">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <input type="text" readonly name="invoice_no" id="invoice_id" class="" value='<?= ($data['invoice_type'] == 1)? @$setval["company_prefix"].@$setval["quotation_prfx"] : @$setval["company_prefix"].@$setval["invoice_prfx"]; ?><?= $data['invoice_id'].find_rev_no($data['invoice_id']);?>' required>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        

                            
                        <div class="clearfix"></div>

                        <div class="col-md-12">
                          <div class="dataTables_wrapper-1440" style="overflow: scroll;">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th class="text-center" style="min-width:60px;" >Add/Remove Row</th>
                                  <th class="text-left" style="min-width:520px;" >Product Name</th>
                                  <th class="text-center" style="min-width:60px;" >Available Stock</th>
                                  <th class="text-center" style="min-width:60px;" >P.I Quantity</th>
                                  <th class="text-center" style="min-width:60px;" >Already Delivered</th>
                                  <th class="text-center" style="min-width:60px;" >Pending Delivery Qty.</th>
                                  <th class="text-center" style="min-width:60px;" >Current Delivery Qty.</th>
                                  <th class="text-center" style="min-width:60px;" >Location</th>
                                </tr>
                              </thead>
                              <tr class="txtMult">
                               <!--  <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td> -->
                                <td colspan="6"></td>
                              </tr>
                              <tbody id="customFields">
                                 
                                <?php
                                 $total_quantity = 0;
                                 $count=1;
                                 // print_b($delivery_note_detail_data);
                                 if (isset($delivery_note_detail_data) && @$delivery_note_detail_data != null) {
                                  foreach ($delivery_note_detail_data as $k => $v) {
                                    $count++;
                                 ?>
                                 <tr class="txtMult" >
                                      <!-- <td class="text-center" style="width: 60px"><a href="<?= site_url("delivery_note/delivery_noteDetailDelete/".$v['delivery_note_detail_id']); ?>" class="ajaxbtn remCF" rel="delete">Remove</a></td> -->
                                      <td class="text-center" style="width:60px;">
                                        <a href="javascript:void(0);" class="">Remove (disabled)</a>
                                      </td>
                                      <td>
                                          <select disabled name="product_id[]" id="pproduct_id<?= $k; ?>" class="form-control prod_name" style="width: 100%" required>
                                              <option selected disabled>--- Select Product ---</option>
                                              <?php foreach($productData as $k => $v2){ ?>
                                                  <option value="<?= $v2['product_id']; ?>" <?=( $v2['product_id']== @$v['product_id'])? 'selected': ''; ?> >
                                                     <?= $v2['product_sku']; ?> - <?= $v2['product_name']; ?>
                                                  </option>
                                                  <?php } ?>
                                          </select>
                                      </td>
                                      <?php

                                        $total_inventory_quantity = inventory_quantity_by_warehouse_product_deileverable($v['product_id'],$data['warehouse_id']);
                                      
                                      ?>
                                         <td align="center">
                                            <input type="text" readonly class="delivery_note_detail_stock" style="width: 100px;" id="stock<?= $count?>" data-optional="0" name="delivery_note_detail_stock[]" value="<?=$total_inventory_quantity;?>"  placeholder="" />
                                          </td>
                                         
                                      <?php
                                      $in_invoice = in_array($v['product_id'], $invoice_data);
                                      if($in_invoice)
                                      {
                                        foreach($invoice_data as $k2 => $v2){

                                          if($v2['p_id'] == $v['product_id'])
                                          {
                                            $invoice_total_quantity_per_product = $v2['invoice_total_quantity_per_product'] - $v2['delivery_note_inventory_total_quantity_per_product'];
                                            
                                            ?>
                                              <td align="center">
                                                <input type="text" readonly="readonly"  class="delivery_note_detail_quantity" style="width: 100px;" id="5quatity_num<?= $count?>" data-optional="0" name="delivery_note_detail_quantity[]" value="<?= $v2['invoice_total_quantity_per_product']; ?>" placeholder="" />
                                              </td>
                                              <td align="center">
                                                <input type="text" readonly="readonly" class="delivery_note_detail_delivered" style="width: 100px;" id="delivered<?= $count?>" data-optional="0" name="delivery_note_detail_delivered[]" value="<?= $v2['delivery_note_inventory_total_quantity_per_product']; ?>"  placeholder="" />
                                              </td>
                                              <td align="center">
                                                <input type="text" readonly="readonly" class="delivery_note_detail_pending" style="width: 100px;" id="pending<?= $count?>" data-optional="0" name="delivery_note_detail_pending[]" value="<?= $invoice_total_quantity_per_product; ?>"  placeholder="" />
                                              </td>
                                            <?php

                                          }
                                        }
                                      }
                                      else
                                      {
                                         ?>
                                              <td align="center">
                                                <input type="text" readonly="readonly" class="delivery_note_detail_quantity" style="width: 100px;" id="5quatity_num<?= $count?>" data-optional="0" name="delivery_note_detail_quantity[]" value="<?= $v['delivery_note_detail_total_quantity']?>" placeholder="" />
                                              </td>
                                              <td align="center">
                                                <input type="text" readonly="readonly" class="delivery_note_detail_delivered" style="width: 100px;" id="delivered<?= $count?>" data-optional="0" name="delivery_note_detail_delivered[]" value="<?= $v['delivery_note_detail_delivered']?>"  placeholder="" />
                                              </td>
                                              <td align="center">
                                                <input type="text" readonly="readonly" class="delivery_note_detail_pending" style="width: 100px;" id="pending<?= $count?>" data-optional="0" name="delivery_note_detail_pending[]" value="<?= $v['delivery_note_detail_pending']?>"  placeholder="" />
                                              </td>
                                            <?php
                                      }
                                      
                                      ?>
                                         <td align="center">
                                            <input type="text" readonly="readonly" class="code quantity txtboxToFilter quatity_num" style="width: 100px;" id="current<?= $count?>" data-optional="0" name="delivery_note_detail_current[]" value="<?= $v['delivery_note_detail_quantity'];?>"  max="<?= @$invoice_total_quantity_per_product+$v['delivery_note_detail_quantity'];?>" placeholder="" />
                                          </td>
                                          <td>
                                            <select disabled name="warehouse_id[]" class="warehouse_id" id="warehouse_id' + x + '" >
                                              <option value="0" selected disabled>--- Select Location ---</option>
                                              <?php foreach ($warehouseData as $k2 => $v2) { ?> <option <?= ($v['warehouse_id'] == $v2['warehouse_id'])?'selected':''?> value="<?= $v2['warehouse_id']; ?>"> <?= $v2['warehouse_name']; ?> </option><?php } ?>
                                            </select>
                                          </td>
                                        <?php
                                      ?>

                                      
                                  </tr>
                                  <?php
                                        $total_quantity += $v['delivery_note_detail_quantity']; 
                                    }
                                  }
                                 ?>

                              </tbody>
                                <!-- <tr> -->
                                  <!-- <th colspan="6" class="text-right" >Total Quantity</th>
                                  <th class="text-center">
                                    <input type="text" readonly="readonly" class="" id="total_quantity" name="total_quantity" value="<?= @$total_quantity; ?>" style="width:100px" placeholder="0"  />
                                  </th> -->
                                <!--  <th class="text-center"><span id="total_quantity">0</span></th> -->
                                <!-- </tr> -->
                            </table>
                          </div>  
                        </div>

                        <div class="clearfix"></div>
                        

                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-12">
                                <label class="form-label">Notes</label>
                              </div>
                              <div class="col-md-12">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <textarea readonly="readonly" name="delivery_notes" class="form-control txtarea-control" value="" placeholder=""><?= @$data['delivery_notes']?></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      

                        <div class="clearfix"></div>  
                    
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12">
                            <div class="form-group text-center">
                              <div class="row">
                                <div class="col-md-12">
                                   <a href="<?= site_url($view_page);?>">
                                    <button class="btn btn-success btn-cons" type="button"><?= ($page_title == 'add')?'':''; ?>Go Back</button> 
                                   </a>
                                   <input name="id"  type="hidden" value="<?= @$data['delivery_note_id']; ?>"> 

                                   <input name="delivery_note_no_old"  type="hidden" value="<?= @$data['delivery_note_no']; ?>"> 
                                </div>
                              </div>
                             
                            </div>
                        </div>
                    </div>
                    
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script>

var page = '<?php echo $page_title; ?>';
var check_select = 0;
if (page != "add") {
    check_select = 1;
}

$(".disable").each(function(i){
  $(this).click(function () {
      
          $(".disable").attr("disabled", "disabled"); 
          //$("#checkbox1").attr("disabled", "disabled"); 
    
    });
});

$(document).ready(function(){


    var max_fields      = 6; 
    // var wrapper         = $(".amenities_field");
    var add_button      = $(".addCF");
    var x = 1; 
    $(add_button).click(function(e){
      if (page == "add") {
          var customer = $('#customer_id').valid();
          var invoice = $('#invoice_id').valid();
          var warehouse = $('#warehouse_id').valid();
          if (!customer || !invoice || !warehouse) {
              return false;
          }
      }
      else
      {
        var warehouse = $('#warehouse_id').valid();
        if (!warehouse) {
            return false;
        }
      }
      
      
      // if (!customer || !invoice || !warehouse) {
      //     return false;
      // }
      e.preventDefault();
      $('form.validate').validate();
      // if(x < max_fields){
        x++;
        var html = '';
        html += '<tr class="txtMult">';
        html += '<td class="text-center" style="width:60px;">';
        html += '<a href="javascript:void(0);" class="remCF">Remove</a>';
        html += '</td>';
        html += '<td>';
        html += '<select name="product_id[]" style="width:100%" id="product_id'+x+'" class="form-control prod_name" required>';
        html += '<option selected disabled>--- Select Product ---</option>';
        html += '<?php foreach($productData as $k => $v){ ?>';
        html += '<option value="<?= $v['product_id']; ?>">';
        html += '<?= $v['product_name']; ?>';
        html += '</option>';
        html += '<?php } ?>';
        html += '</select>';
        html += '</td>';
        html += '<td>';
          html += '<input type="text" readonly class="delivery_note_detail_stock" style="width: 100px;" id="stock'+x+'" data-optional="0" name="delivery_note_detail_stock[]" value=""  placeholder="" />';
          html += '</td>';

          html += '<td>';
          html += '<input type="text"  class="delivery_note_detail_quantity" style="width: 100px;" id="5quatity_num'+x+'" data-optional="0" name="delivery_note_detail_quantity[]" value="" placeholder="" />';
          html += '</td>';

          html += '<td>';
          html += '<input type="text"  class="delivery_note_detail_delivered" style="width: 100px;" id="delivered'+x+'" data-optional="0" name="delivery_note_detail_delivered[]" value=""  placeholder="" />';
          html += '</td>';

          html += '<td>';
          html += '<input type="text"  class="delivery_note_detail_pending" style="width: 100px;" id="pending'+x+'" data-optional="0" name="delivery_note_detail_pending[]" value=""  placeholder="" />';
          html += '</td>';

          html += '<td>';
          html += '<input type="text" class="code quantity txtboxToFilter quatity_num" style="width: 100px;" id="current'+x+'" data-optional="0" name="delivery_note_detail_current[]" value="0"  max="" placeholder="" />';
          html += '</td>';
        html += '</tr>';
        $("#customFields").append(html);
      // }
    });

    $("#customFields").on('click','.remCF',function(){
       $(this).parent().parent().remove();
    });

    $(document).on('click', ".remCF",function () { 
        var mult = 0;
        // for each row:
        $("tr.txtMult").each(function () {
            var $quatity_num = $('.quatity_num', this).val();
            if($quatity_num == undefined){
                $quatity_num = ($quatity_num == undefined)?0:$quatity_num;
            }

            var $total = ($quatity_num * 1);
            mult += $total;
        });
        $("#total_quantity").val(mult).text(mult);
    });
  
});


 
//$(document).on('change', ".txtMult input",function () { 
//$(document).on('change', ".txtMult input",function () { 
$(document).on('keydown keypress keyup change blur', ".txtMult input,.txtMult select",function () { 
        var check = $(this).attr('data-optional');
        var mult = 0;
        // alert(123);
        $("#customFields tr.txtMult").each(function () {

            var $quatity_num = $('.quatity_num', this).val();
            if($quatity_num == undefined){
                $quatity_num = ($quatity_num == undefined)?0:$quatity_num;
            }

            var $total = ($quatity_num * 1);
            mult += $total;
        });

        // console.log("loop");

        $("#total_quantity").val(mult).text(mult);
});
  

$(document).on('change', "#customer_id",function () {
  var invoice = $('#invoice_id').valid();
  if (!invoice) {
      $("#customer_id").select2("val", "0");
      return false;
  }
  if (check_select == 0) {
      check_select++;
      // add_customer_invoice();
      return true;
  }
  swal({
      title: "Are you sure, form will be reset.?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
  .then((willDelete) => {
      if (willDelete) {
          // add_customer_invoice();
      } else {
          return false;
      }
  });
  
});

$(document).on('click', "#invoice_id_btn",function () {
  
  if (check_select == 0) {
      check_select++;
      get_invoice();
      return true;
  }
  swal({
      title: "Are you sure, form will be reset.?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
  .then((willDelete) => {
      if (willDelete) {
          get_invoice();
      } else {
          return false;
      }
  });

  
});

// $(document).on('change', "#warehouse_id",function () {
// });

function get_invoice()
{
  //var customer = $('#customer_id').valid();
  var invoice = $('#invoice_id').valid();
  var warehouse = $('#warehouse_id').valid();
      
  if (/*!customer ||*/ !invoice || !warehouse) {
      return false;
  }

  var q_id = $('#invoice_id').val();
  var c_id = $('#customer_id').val();
  var w_id = $('#warehouse_id').val();
  $('#customFields').empty();
  $.ajax({
        url: "<?php echo site_url('delivery_note/invoice_data'); ?>",
        dataType: "json",
        type: "POST",
        data: {
                invoice_id: q_id,
                customer_id: c_id,
                warehouse_id: w_id
              },
        cache: false,
        success: function(quotationData) {
          //console.log(quotationData['invoice_type']);
          $("#customFields .txtMult").html('');
          if (quotationData) {
            if(quotationData['invoice_data'] != '' && quotationData['invoice_data'] != null) {
              $("#customer_id").select2("val", quotationData['invoice_data'][0]['customer_id']);
            
              x = 5;
              html = '';
              
                for (var i = 0; i < quotationData['invoice_data'].length; i++) {
                  var invoice_total_quantity_per_product = quotationData['invoice_data'][i]['invoice_total_quantity_per_product'] - quotationData['invoice_data'][i]['delivery_note_inventory_total_quantity_per_product'];
                  // if (invoice_total_quantity_per_product > 0){
                    // console.log(quotationData['invoice_detail_data'][i]['product_id']);
                    html += '<tr class="txtMult">';
                    html += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
                    html += '<td>';
                    // html += '<select name="product_id[]" style="width: 100%;" id="5product_id'+x+'" class="form-control prod_name" required>';
                    // html += '<option selected disabled>--- Select Product ---</option>';
                    // var total_inventory_quantity_val = 0;
                    // for (var j = 0; j < quotationData['product_data'].length; j++) {
                    //   if (quotationData['invoice_detail_data'][i]['product_id'] == quotationData['product_data'][j]['product_id']) { 
                      html += '<select name="product_id[]" style="width:100%" id="product_id'+x+'" class="form-control prod_name" required>';
                      html += '<option selected disabled>--- Select Product ---</option>';
                      html += '<option selected value="'+quotationData['invoice_data'][i]['p_id']+'">';
                      html += quotationData['invoice_data'][i]['product_name'];
                      html += '</option>';
                        // total_inventory_quantity_val = quotationData['product_data'][j]['total_inventory_quantity'];

                        // html += '<option value="'+quotationData['product_data'][j]['product_id']+'" selected>';
                      // }
                      // else{
                        // html += '<option value="'+quotationData['product_data'][j]['product_id']+'">';  
                      // }
                      // html += quotationData['product_data'][j]['product_name'];
                      // html += '</option>';
                    // }
                    // html += '</select>';
                    html += '</td>';
                    html += '<td><input type="text" readonly class="delivery_note_detail_stock" style="width: 100%;" id="stock'+x+'" data-optional="0" name="delivery_note_detail_stock[]" value="'+quotationData['invoice_data'][i]['inventory_total_quantity']+'"  placeholder="" />';
                    html += '</td>';

                    html += '</td>';
                    html += '<td><input type="text" readonly class="delivery_note_detail_quantity" style="width: 100%;" id="5quatity_num'+x+'" data-optional="0" name="delivery_note_detail_quantity[]" value="'+quotationData['invoice_data'][i]['invoice_total_quantity_per_product']+'" placeholder="" />';
                    html += '</td>';

                    html += '</td>';
                    html += '<td><input type="text" readonly class="delivery_note_detail_delivered" style="width: 100%;" id="delivered'+x+'" data-optional="0" name="delivery_note_detail_delivered[]" value="'+quotationData['invoice_data'][i]['delivery_note_inventory_total_quantity_per_product']+'"  placeholder="" />';
                    html += '</td>';

                    html += '</td>';
                    html += '<td><input type="text" readonly class="delivery_note_detail_pending" style="width: 100%;" id="pending'+x+'" data-optional="0" name="delivery_note_detail_pending[]" value="'+invoice_total_quantity_per_product+'" min="0" placeholder="" />';
                    html += '</td>';

                    html += '</td>';
                    html += '<td>';
                    if(quotationData['invoice_type'] == 2){
                      html += '<input type="text" class="code quantity txtboxToFilter quatity_num" style="width: 100%;" id="current'+x+'" data-optional="0" name="delivery_note_detail_current[]" value="'+invoice_total_quantity_per_product+'"  max="'+(quotationData['invoice_data'][i]['invoice_total_quantity_per_product'] - quotationData['invoice_data'][i]['delivery_note_inventory_total_quantity_per_product'])+'" placeholder="" />';
                    }else{
                        html += '<input type="text" class="code quantity txtboxToFilter quatity_num" style="width: 100%;" id="current'+x+'" data-optional="0" name="delivery_note_detail_current[]" value="'+invoice_total_quantity_per_product+'"  placeholder="" />';
                    }
                    html += '</td>';

                    html += '</tr>';
                  // }
                  x++;
                }
              
              $("#total_quantity").val(0);
              $("#customFields").append(html);
              $(".txtMult input").trigger("change");
              $(".total_discount_amount").trigger("change");
              

            }
          }
        }
      });
}

$(document).on('change', ".prod_name",function () {

    var q_id = $('#invoice_id').val();
    var c_id = $('#customer_id').val();
    var w_id = $('#warehouse_id').val();
    var prod_name = $(this).parent().parent('tr').find('.prod_name').val();
    var dataString  = 'pid='+prod_name+'&customer_id='+c_id+'&invoice_id='+q_id+'&warehouse_id='+w_id;
    var row = $(this).closest('tr'); // get the row
    var quatity_num = row.find('.quatity_num');
    var delivery_note_detail_stock = row.find('.delivery_note_detail_stock');
    var delivery_note_detail_quantity = row.find('.delivery_note_detail_quantity');
    var delivery_note_detail_delivered = row.find('.delivery_note_detail_delivered');
    var delivery_note_detail_pending = row.find('.delivery_note_detail_pending');
    quatity_num.attr("max", 0);
    delivery_note_detail_stock.val(0);
    //street.empty();
       
    $.ajax({
      url: "<?php echo site_url('delivery_note/progt'); ?>",
      dataType: "json",
      type: "POST",
      data: dataString ,
      cache: false,
      success: function(employeeData) {
        if(employeeData) {
          quatity_num.attr("max", employeeData.inventory_total_quantity);
          delivery_note_detail_quantity.attr("max", employeeData.inventory_total_quantity);
          delivery_note_detail_stock.val(employeeData.inventory_total_quantity);
          delivery_note_detail_quantity.val(0);
          delivery_note_detail_delivered.val(employeeData.delivery_note_inventory_total_quantity_per_product);
          delivery_note_detail_pending.val(0);
        
      } else {
        $("#heading").hide();
        $("#records").hide();
        $("#no_records").show();
      }
      }
    });
});
        
  
  
$(document).ready(function() {
  
  $("#user_type").change(function () {
    var val = $(this).val();
    if (val == 2) {
      $('.myUserType').slideDown();
    }else{
      $('.myUserType').slideUp();
    }
  });
  
  $( ".datepicker" ).datepicker({
     format: "dd-mm-yyyy",
     autoclose: true
     
  });

});

$(document).ready(function() {
  $("form.validate").validate({
    rules: {
      customer_id:{
        required: true
      },
      "product_id[]":{
        required: true
      },
      "delivery_note_detail_quantity[]":{
        required: true,
        number:true
      },
      invoice_id:
      {
        required: true,
      },
      delivery_note_no:{
        required: true,
        digits:true
      },
      delivery_note_customer_note:{
        required: true
      },
      delivery_note_terms_conditions:{
        required: true,
        
      }
    }, 
    messages: {
      customer_id: "This field is required.",
      "product_id[]":"This field is required.",
      "delivery_note_detail_quantity[]":{
        required:"This field is required.",
        number:"Please Insert Number."
      },
      delivery_note_no:{
        required: "This field is required.",
        digits: "Please enter only digits."
      },
      invoice_id:"This field is required.",
      delivery_note_customer_note: "This field is required.",
      delivery_note_terms_conditions: "This field is required."
    },
    invalidHandler: function (event, validator) {
      //display error alert on form submit    
      },
      errorPlacement: function (label, element) { // render error placement for each input type   
        var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
        $('<span class="error"></span>').insertAfter(element).append(label);
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('success-control').addClass('error-control');  
      },
      highlight: function (element) { // hightlight error inputs
        var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
        var parent = $(element).parent();
        parent.removeClass('success-control').addClass('error-control'); 
      },
      unhighlight: function (element) { // revert the change done by hightlight
        var icon = $(element).parent('.input-with-icon').children('i');
    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent();
        parent.removeClass('error-control').addClass('success-control'); 
      },
      success: function (label, element) {
        var icon = $(element).parent('.input-with-icon').children('i');
    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('error-control').addClass('success-control');
        
      }
      // submitHandler: function (form) {
      // }
    });
  $('.select2', "form.validate").change(function () {
      $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
  });
  $('#fileUploader').fileuploader({
    changeInput: '<div class="fileuploader-input">' +
                      '<div class="fileuploader-input-inner">' +
                        '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                      '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                      '<p>or</p>' +
                      '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                    '</div>' +
                  '</div>',
    theme: 'dragdrop',
    // limit: 4,
    // extensions: ['jpg', 'jpeg', 'png', 'gif'],
    onRemove: function(item) {
      
    },
    captions: {
            feedback: 'Drag and drop files here',
            feedback2: 'Drag and drop files here',
            drop: 'Drag and drop files here'
        },
  });
});
</script>