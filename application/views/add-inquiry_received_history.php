<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
            Inquiry Received History
            </li>
            <li><a href="#" class="active"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?>
                    <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
                </a> </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product) ?>" method="post">
                            <div class="row">

                            <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Inquiry No</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="for-control" readonly name="inquiry_no" value="<?= @$data['inquiry_no'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Date</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="datepicker" name="inquiry_received_history_date" value="<?= @$data['inquiry_received_history_date'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Company</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="" name="inquiry_received_history_company_name" value="<?= @$data['inquiry_received_history_company_name'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Sales Person</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select name="inquiry_received_history_sales_person" id="inquiry_received_history_sales_person" class="select2 form-control">
                                                    <option value="0" selected disabled>--- Select Sales Person ---</option>
                                                    <?php
                                                        foreach ($employee as $k => $v) {
                                                        ?>
                                                        <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['follow_up_sales_person']) ? 'selected' : ''; ?>>
                                                        <?= $v['user_name']; ?></option>
                                                    <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Contact Person</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="" name="inquiry_received_history_contact_person" value="<?= @$data['inquiry_received_history_contact_person'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Contact No</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="" name="inquiry_received_history_contact_no" value="<?= @$data['inquiry_received_history_contact_no'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Remarks</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <textarea name="inquiry_received_history_remarks" rows="4" cols="50"><?= @$data['inquiry_received_history_remarks'] ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <!--  <button class="btn btn-success btn-cons pull-right" type="submit"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> Category</button>-->
                                        </br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"><?= ($page_title == 'add') ? '' : ''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['follow_up_id']; ?>">

                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script type="text/javascript">
    $(document).ready(function() {
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });

        $("form.validate").validate({
            rules: {
                follow_up_date: {
                    required: true
                },
                follow_up_company_name: {
                    required: true
                },
                follow_up_sales_person: {
                    required: true
                },
                follow_up_contact_person: {
                    required: true
                },
                follow_up_contact_no: {
                    required: true
                },
                follow_up_remarks: {
                    required: true
                },

            },
            messages: {
                payment_date: "This field is required.",
                payment_amount: "This field is required.",
                invoice_no: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit 
                error("Please input all the mandatory values marked as red");

            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('error-control').addClass('success-control');

            }
            // submitHandler: function (form) {
            // }
        });
        // $('.select2', "form.validate").change(function () {
        //     $('fosrm.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        // });
    });
    
</script>