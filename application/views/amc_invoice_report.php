<style type="text/css">
    .table1-td-h4 {
        color: #333333;
    }

    .table1-td-h4-2 {
        color: #7f7f7f;
    }

    .table1-td-quotHeading {
        text-align: right;
        font-size: 22px;
        font-weight: bold;
    }

    .product-table2-th-details {
        border: 1px solid #adadad;
        background-color: #3c3d3a;
        color: #fff;
    }

    .product-table2-txt-align {
        text-align: center;
    }

    .product-table1-txt-align {
        position: relative;
        right: 0;
    }

    .product-table2-td-details {
        border: 1px solid #adadad;
    }

    .bank-table2-th-details {
        border: 1px solid #adadad;
        background-color: #3c3d3a;
        color: #fff;
        /*font-family: Calibri;
        font-size: 16px;*/
        font-family: Calibri;
        font-size: 12px;
    }

    .bank-table2-td-details {
        background-color: #3c3d3a;
        color: #fff;
        border: 1px solid #adadad;
        /*font-family: Calibri;
        font-size: 16px;*/
        font-family: Calibri;
        font-size: 12px;
    }

    .sub-total-heading {
        font-weight: bold;
        border-left: 1px solid #adadad;
        text-align: right;
    }

    .sub-total-detail {
        border-right: 1px solid #adadad;
        text-align: right;
        padding-right: 7px;
    }

    .less-special-discount-heading {
        font-weight: bold;
        border-left: 1px solid #adadad;
        text-align: right;
    }

    .less-special-discount-detail {
        border-right: 1px solid #adadad;
        text-align: right;
        padding-right: 7px;
    }

    .buyback-discount-heading {
        font-weight: bold;
        border-left: 1px solid #adadad;
        text-align: right;
    }

    .buyback-discount-detail {
        border-right: 1px solid #adadad;
        text-align: right;
        padding-right: 7px;
    }

    .netAmount-heading {
        font-weight: bold;
        text-align: right;
        border-top: 1px solid #adadad;
        border-left: 1px solid #adadad;
    }

    .netAmount-detail {
        font-weight: bold;
        text-align: right;
        border-top: 1px solid #adadad;
        border-right: 1px solid #adadad;
        padding-right: 7px;
    }

    .vat-heading {
        font-weight: bold;
        text-align: right;
        border-left: 1px solid #adadad;
        border-bottom: 1px solid #adadad;
    }

    .vat-details {
        text-align: right;
        border-right: 1px solid #adadad;
        border-bottom: 1px solid #adadad;
        padding-right: 7px;
    }

    .netAmount-inc_VAT-heading {
        font-weight: bold;
        text-align: right;
        border-left: 1px solid #adadad;
        border-bottom: 1px solid #adadad;
    }

    .netAmount-inc_VAT-detail {
        font-weight: bold;
        text-align: right;
        border-right: 1px solid #adadad;
        border-bottom: 1px solid #adadad;
        padding-right: 7px;
    }

    .border-bot {
        border-bottom: 1px solid #adadad;
    }

    .div-controls {
        margin-left: 7%;
        margin-right: 7%;
    }

    .table_align {
        margin-left: 10%;
        margin-right: 10%;
    }

    .font-controls {
        font-family: Calibri;
        font-size: 16px;
    }

    .notes-font-controls {
        font-family: Calibri;
        font-size: 12px;
    }

    .item-dtable-font-controls {
        font-family: Calibri;
        font-size: 16px;
    }

    .payment-font-controls,
    .bankDetails-font-controls {
        font-family: Calibri;
        font-size: 12px;
    }

    .Thanks-font-controls {
        font-family: Calibri;
        font-size: 16px;
    }

    .optional-table-th {
        border: 1px solid #adadad;
        background-color: #c4bb98;
    }

    .optional-table-td {
        border: 1px solid #adadad;
    }

    .tr1-th-first {
        border-top: 1px solid black;
        text-align: right;
        border-left: 1px solid black;
    }

    .tr1-th-sec {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr1-th {
        border-top: 1px solid black;
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr1-th-sec {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr2-th-first {
        text-align: right;
        border-left: 1px solid black;
        border-bottom: none;
    }

    .tr2-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr3-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr3-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr4-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr4-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr5-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr5-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr6-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr6-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr7-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr7-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr8-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr8-th {
        border-right: 1px solid black;
        border-left: 1px solid black
    }

    .tr9-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr9-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr10-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr10-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr11-th-first {
        text-align: right;
        border-left: 1px solid black;
    }

    .tr11-th {
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .tr12-th-first {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        text-align: right;
        border-left: 1px solid black;
        background-color: #d9d9d9;
    }

    .tr12-th {
        border-bottom: 1px solid black;
        border-top: 1px solid black;
        border-right: 1px solid black;
        border-left: 1px solid black;
        background-color: #d9d9d9;
    }

    tr:nth-child(3) .tr1-th-first,
    tr:nth-child(3) .tr1-th {
        border-top: none;
    }

    tr:nth-child(5) .tr1-th-first,
    tr:nth-child(5) .tr1-th {
        border-top: none;
    }

    tr:nth-child(7) .tr1-th-first,
    tr:nth-child(7) .tr1-th {
        border-top: none;
    }

    tr:nth-child(9) .tr1-th-first,
    tr:nth-child(9) .tr1-th {
        border-top: none;
    }

    tr:nth-child(11) .tr1-th-first,
    tr:nth-child(11) .tr1-th {
        border-top: none;
    }

    .covr_letter {
        margin-right: 8%;
        margin-left: 8%;
        font-family: calibri;
    }

    .img_margin {
        padding: 10px 10px;
    }

    @page toc {
        size: A4;
    }

    span,
    strong,
    em,
    i,
    p {
        font-family: calibri;
    }

    .pagebreak {
        page-break-after: always;
    }

    .bl {
        border-left: 1px solid #adadad;
    }

    .br {
        border-right: 1px solid #adadad;
    }

    .bt {
        border-top: 1px solid #adadad;
    }

    .bb {
        border-bottom: 1px solid #adadad;
    }

    .bt-bold {
        border-top: 1px solid black;
    }

    .font_size {
        font-size: 12px;
    }
    .font_size_2 {
        font-size: 15px;
    }
    .font_size_3 {
        font-size: 11px;
    }
    .l-height
    {
        line-height: 0.1px;
    }
</style>
<table style="border-collapse: collapse;" width="100%" class="table_align">

    <tr>
        <td width="500px" align="left" style="padding-top: 12px;">
            <h4 style="font-family:calibri;font-size: 18px;" class="table1-td-h4"><span>Accounts
                    <!-- <?= @$employee_data['department_title'] ?> --> Department
                </span><br><span><?= @$customer_data['user_company_name'] ?></span></h4>
            <h4 class="" style="font-family:calibri;font-size: 18px;text-align: right;font-weight: bold;">T.R.N # <?= @$customer_data['tax_reg']; ?>
            </h4>
            <h5 style="font-size: 18px;" class="table1-td-h4-2 "><span>
                    <!-- <?= @$setval['setting_company_address']; ?> --><?= @$customer_data['user_address']; ?>
                </span></h5>
        </td>

        <td align="right" width="500px" style="">
            <span class="table1-td-quotHeading" style="font-family:calibri;"> <?= ($setval['setting_company_country'] == $customer_data['user_country']) ? 'TAX' : '' ?> INVOICE</span><br>
            <!-- <?= @$setval["company_prefix"] . @$setval["quotation_prfx"] ?> -->
            <h4 class="" style="font-family:calibri;font-size: 20px;text-align: right;font-weight: bold;"># <?= @$setval["company_prefix"] ?>AMCINV-<?= (@$amc_quotation['amc_quotation_revised_no'] > 0) ? @$amc_quotation['amc_quotation_no'] . '-R' . number_format(@$amc_quotation['amc_quotation_revised_no']) : @$amc_quotation['amc_quotation_no']; ?>
            </h4>

            <!--    <?= ($amc_quotation['quotation_po_no'] != '') ? ' <h4 class="" style="font-size: 20px;text-align: right;font-weight: bold;font-family:calibri;">P.O. # ' . $amc_quotation['quotation_po_no'] . '</h4>' : '' ?> -->
            <h4 class="" style="font-family:calibri;font-size: 20px;text-align: right;font-weight: bold;">T.R.N # <?= @$setval['setting_company_reg']; ?>
            </h4>
            <span style="font-size: 20px; font-weight:bold;" class="">Date : <?= @$amc_quotation['amc_quotation_date']; ?></span>
        </td>
    </tr>

</table>
<br>
<table style="border-collapse: collapse;margin-left:50px;margin-right:50px;" width="100%" class=" bb">
    <tr>
        <th colspan="1" align="center" style="padding: 10px 10px;" class="product-table2-th-details" width='50px' height="40px">
            <h5>#</h5>
        </th>
        <th colspan="3" align="left" style="padding: 10px 10px;" class="product-table2-th-details" width='450px' height="40px">
            <h5>Item & Description</h5>
        </th>
        <th colspan="2" align="center" style="padding: 10px 10px;" class="product-table2-th-details" width='100px' height="40px">
            <h5>Amount <br> <?= @$currency_name ?></h5>
        </th>
    </tr>
    <?php
    $subtotal_total = 0;
    $discount_amount_total = 0;
    $net_amount_total = 0;
    $toal_amount_total =0;
    $toal_vat =0;
    if (@$amc_warranty_types != null && !empty(@$amc_warranty_types)) {
        $j = 1;
        foreach ($amc_warranty_types as $warranty) {


            $discount_amount = 0;
            $subtotal = @$warranty['amc_type_subtotal'];
            if ($warranty['amc_type_total_discount_type'] == '1') {
                $discount_amount = ($subtotal / 100) * $warranty['amc_type_total_discount_amount'];
            } else if ($warranty['amc_type_total_discount_type'] == '2') {
                $discount_amount = $warranty['amc_type_total_discount_amount'];
            }
            $discount_amount = customized_round($discount_amount);
            $find_vat = @$warranty['tax_value'];
            $toal_amount = @$warranty['amc_type_total_amount'];
            $notes = @$warranty['amc_type_note'];
            $net_amount = ($subtotal - number_format((float)$discount_amount, 2, '.', ''));

            $subtotal_total += $subtotal;
            $discount_amount_total += $discount_amount;
            $net_amount_total += $net_amount;
            $toal_amount_total += $toal_amount;
            if($warranty['amc_type_subtotal'] >0){
                $toal_vat += number_format((float) @$warranty['tax_value'], 2, '.', '');
                }

            if ($toal_amount > 0) {
    ?>

                <tr>
                    <td class="bl font_size" align="center"><?= $j; ?></td>
                    <td colspan="3" align="left" style="padding: 10px 10px;" class="br bl" height="40px">
                        <h5><?= @$warranty['amc_type_title'] ?></h5>
                    </td>
                    <td align="center" colspan="2" class="br font_size"><?= number_format((float) $subtotal, 2, '.', ''); ?></td>
                </tr>

                <?php foreach ($selected_invoice as $invoice) { ?>
                    <tr>
                        <td class="bl "></td>
                        <td colspan="3" align="left" style="padding: 10px 10px;" class="bl br bb bt-bold font_size" height="40px">
                            <?= $invoice; ?> will remain valid from <?= @$amc_quotation['amc_quotation_start_date']; ?> to <?= @$amc_quotation['amc_quotation_expiry_date']; ?>.
                            The AMC may be further extended with a mutual consent.
                        </td>
                        <td colspan="2" class="bl br"></td>
                    </tr>
                <?php } ?>



    <?php
                $j++;
            }
        }
    }
    ?>
</table>

<?php
if ($amc_quotation['amc_quotation_report_line_break_invoice'] > 0) {
    for ($i = 0; $i < $amc_quotation['amc_quotation_report_line_break_invoice']; $i++) {
?>
        <br>
<?php
    }
}
?>
 <?php 
 $style="margin-left:50px;margin-right:50px;";
 $check = 0;
 if ($setval['setting_company_country'] == $customer_data['user_country']) { 
    $style="margin-left:67px;margin-right:67px;";
    $check =1;
     } ?>
<table style="border-collapse: collapse;<?=$style?>" width="100%" class=" bb bt">

    <?php
    $class = "bb";
    if ($discount_amount_total > 0) {
        $class = "";
    }
    ?>
    <tr>
        <td class="bl <?= $class ?> <?= ($check == 1)?'font_size_2':'font_size_3'?> l-height" width='50px' align="center"></td>
        <td colspan="3"  align="right" width='480px' style="padding: 10px 10px;font-weight: bold" class="<?= $class ?> l-height <?= ($check == 1)?'font_size_2':'font_size_3'?>" >
            Sub Total
        </td>
        <td align="right" style="padding-right:10px;" colspan="2" width='100px' class="br l-height <?= $class ?> <?= ($check == 1)?'font_size_2':'font_size_3'?>"><?= number_format((float) $subtotal_total, 2, '.', ''); ?></td>
    </tr>

    <?php if ($discount_amount_total > 0) { ?>
        <tr>
            <td class="bl bb <?= ($check == 1)?'font_size_2':'font_size_3'?> l-height" width='50px' align="center"></td>
            <td colspan="3"  align="right" width='480px' style="padding: 10px 10px;font-weight: bold" class="bb l-height <?= ($check == 1)?'font_size_2':'font_size_3'?>" height="40px">
                <?php
                if (strlen(str_replace(' ', '', $warranty['amc_type_discount_detail'])) > 0) {
                    echo $warranty['amc_type_discount_detail'];
                } else {
                    echo "Less Special Discount";
                }
                ?>
            </td>
            <td align="right"  style="padding-right:10px;" colspan="2" width='100px' class="br l-height bb <?= ($check == 1)?'font_size_2':'font_size_3'?>"><?= number_format((float) $discount_amount_total, 2, '.', ''); ?></td>
        </tr>
    <?php } ?>

    <tr>
        <td class="bl font_size" width='50px' align="center"></td>
        <td colspan="3"  align="right" width='480px' style="padding: 10px 10px; font-weight: bold" class="l-height <?= ($check == 1)?'font_size_2':'font_size_3'?>" height="40px">
            Net Amount
        </td>
        <td align="right" style="font-weight: bold;padding-right:10px;" colspan="2" width='100px' class="br l-height <?= ($check == 1)?'font_size_2':'font_size_3'?>"><?= number_format((float) $net_amount_total, 2, '.', ''); ?></td>
    </tr>

    <?php if ($setval['setting_company_country'] == $customer_data['user_country']) { ?>
                  <tr>
                    <td class="bl bb <?= ($check == 1)?'font_size_2':'font_size'?> l-height" width='50px' align="center"></td>
                     <td colspan="3" style="font-weight: bold" align="right" height="25px" class="bb l-height <?= ($check == 1)?'font_size_2':'font_size'?>" width="480px"><?= $setval['tax'] ?>% VAT</td>

                     <td colspan="2"  align="right" style="padding-right:10px;" height="25px" width='100px' class="bb l-height <?= ($check == 1)?'font_size_2':'font_size'?> br">
                        <?= $toal_vat; ?>
                     </td>

                  </tr>

                  <tr>

                    <td class="bl " colspan="3" class="l-height bl" width="350px" style="padding-left:10px;line-height: 15px;"> 
                        <b><?= numberTowords2(number_format((float) $toal_amount_total, 2, '.', ''),$currency['base_name'], $currency['Deci_name'])?></b>
                        <!-- <b>Twenty Thousand Two Hundread and Twenty Five Dhiram and Ninty Fils</b> -->
                    </td>
                     <td colspan="1" style="font-weight: bold" align="right" height="40px" class="l-height <?= ($check == 1)?'font_size_2':'font_size'?>" width="180px">Net Amount (Incl. VAT)</td>

                     <td colspan="2" style="font-weight: bold;padding-right:10px;" align="right" height="40px" width="100px" class="l-height <?= ($check == 1)?'font_size_2':'font_size'?> br"><b>
                           <?= number_format((float) $toal_amount_total, 2, '.', ''); ?>
                        </b></td>
                  </tr>
      <?php
               } ?>
</table>


<br />
<?php
if (isset($notes) && strlen(str_replace(' ', '', $notes)) > 0) {
?>
    <div class="div-controls">
        <span class="notes-font-controls"><b>Notes :</b></span><br>
        <span class="notes-font-controls"><?= @$notes; ?></span>
    </div>

    <br /><br />
<?php } ?>

<div class="div-controls">
    <span class="payment-font-controls"><b>Payment :</b></span><br>
    <span class="payment-font-controls">
        <?php
        foreach ($terms as $term) {
        ?>
            <?= @$term['percentage'] ?>% <?= @$term['payment_title'] ?>
            (<?= (isset($term['payment_days']) && $term['payment_days'] > 0) ? 'After ' . $term['payment_days'] . ' Day' : ((isset($term['payment_days']) && $term['payment_days'] == 0) ? 'Immediate' : '') ?>)<br />
        <?php } ?>
    </span>
</div>

<br />

<?php
if ($amc_quotation['amc_quotation_report_line_break_payment_invoice'] > 0) {
    for ($i = 0; $i < $amc_quotation['amc_quotation_report_line_break_payment_invoice']; $i++) {
?>
        <br>
<?php
    }
}
?>

<div class="div-controls">
    <span class="bankDetails-font-controls"><b>Bank Details :</b></span>
</div>
<table style="border-collapse: collapse;margin-top: 10px;" class="table_align " width="100%">

    <thead>

        <tr>

            <th align="center" class="bank-table2-th-details font-controls" height="40px" width="250px">Beneficiary</th>

            <th colspan="<?= sizeof($bank) ?>" class="bank-table2-th-details font-controls" height="40px" width="750px" style="padding:0 0 0 10px;"><?= @$setval['setting_company_name']; ?></th>

        </tr>

    </thead>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Bank</td>
        <?php
        foreach ($bank as $bnk1) {
        ?>
            <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
                <?= @$bnk1['bank']; ?>
            </td>
        <?php
        }
        ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Account No.</td>
        <?php
        foreach ($bank as $bnk2) {
        ?>
            <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
                <?= @$bnk2['account_no']; ?>
            </td>
        <?php
        }
        ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">IBAN</td>

        <?php
        foreach ($bank as $bnk3) {
        ?>
            <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
                <?= @$bnk3['iban']; ?>
            </td>
        <?php
        }
        ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Branch</td>

        <?php
        foreach ($bank as $bnk4) {
        ?>
            <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
                <?= @$bnk4['branch']; ?>
            </td>
        <?php
        }
        ?>
    </tr>

    <tr>
        <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Swift Code</td>

        <?php
        foreach ($bank as $bnk5) {
        ?>
            <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
                <?= @$bnk5['swift_code']; ?>
            </td>
        <?php
        }
        ?>
    </tr>
    <br />

</table>

<br /><br />
<?php
if ($amc_quotation['amc_quotation_report_line_break_bank_detail_invoice'] > 0) {
    for ($i = 0; $i < $amc_quotation['amc_quotation_report_line_break_bank_detail_invoice']; $i++) {
?>
        <span style="color:white">.</span>
        <br />
<?php
    }
}
?>

<div class="div-controls">
    <span class="Thanks-font-controls"><b>Thanks,</b></span>
</div>

<br /><br /><br />

<div class="div-controls">
    <span class="Thanks-font-controls"><b>For <?= @$setval['setting_company_name'] ?></b></span>
</div>