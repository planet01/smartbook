
<style>
.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}

</style>
   
   <div class="div-controls div-font-controls" style="text-align:center;padding-top:-12%;">
      <h3 class="main-heading">Smart Matrix General Trading LLC</h3>
      <br>
      <h4 class="main-second-heading">VOUCHER</h4>
      <br>
   </div>

   <table style="width:100%;border-collapse: collapse;" class="table_align">
      <tr>
      <?php 
      if($history == 1)
      {
         $rev = '-R'.$data['voucher_revised_no'];
      }else
      {
         $rev = ($data['voucher_revised_no'] > 0)?'-R'.$data['voucher_revised_no'] : '';   
      }
      ?>
         <th align="left" class="font-controls" style="padding-left:15px;padding-bottom:10px;"><b>Transfer No. : <?= $data['TransactionNo'].$rev?></b></th>
         <th align="right" class="font-controls" style="padding-right:15px;padding-bottom:10px;"><b>Effective Date : <?= $data['Transaction_Date']?></b></th>
      </tr>
   </table>

   <table style="width:100%;border-collapse: collapse;border-top:2px solid #000;border-bottom:2px solid #000;margin-bottom:15%;" class="table_align"> 
      <!-- first record -->
      <tr>
         <th align="left" width="60px" style="padding:5px 0 5px 10px;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
         <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>A/c Code</b></th>
         <th align="center" width="250px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Account Name</b></th>
         <th align="center" width="100px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Cheque #</b></th>
         <th align="center" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Debit</b></th>
         <th align="center" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Credit</b></th>
         <th align="center" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Tax</b></th>
      </tr>
      <?php
         $i = 0;
         $t_debit = 0;
         $t_credit = 0;
         $t_tax = 0;
         foreach ($detail_data as $dt) {
             $i++;
         ?>
            
            <tr>
                <td align="left" style="padding:5px 0 5px 10px;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $i; ?></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><b><?= $dt['AccountNo']?></b></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['AccountDesc']?></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['Cheque_No']?></td>
                <td align="right" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= ($dt['Amount_Dr'] > 0)?number_format($dt['Amount_Dr'],2):''?></td>
                <td align="right" style="padding:5px 10px 5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= ($dt['Amount_Cr'] > 0)?number_format($dt['Amount_Cr'],2):''?></td>
                <td align="right" style="padding:5px 10px 5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= ($dt['tax'] > 0)?number_format($dt['tax'],2):''?></td>
            </tr>
            <tr>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;border-bottom:1px solid #000;"></td>
                <td align="left" colspan="3" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;border-bottom:1px solid #000;"><b>Details</b>: <?= $dt['Transaction_Detail']?></td>
                <td align="right" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;border-bottom:1px solid #000;"></td>
                <td align="right" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;border-bottom:1px solid #000;"></td>
                <td align="right" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;border-bottom:1px solid #000;"></td>
            </tr>
         <?php 
            $t_debit += $dt['Amount_Dr'];
            $t_credit += $dt['Amount_Cr'];
            $t_tax += $dt['tax'];
            }
         ?>
         <tr>
            <th colspan="4" align="right" width="60px" style="padding:5px 0 5px 10px;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Total</b></th>
            <th align="right" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b><?= number_format($t_debit,2)?></b></th>
            <th align="right" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b><?= number_format($t_credit,2)?></b></th>
            <th align="right" width="80px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b><?= ($t_tax <= 0)?'': number_format($t_tax,2)?></b></th>
         </tr>
     
      
   </table>
   
   <table style="width:100%;border-collapse: collapse;" class="bottom-table-div">
      <tr>
         <td align="left" width="300px" style="border-bottom:1px solid #000;"></td>
         <td width="33px"></td>
         <td align="left" width="300px" style="border-bottom:1px solid #000;"></td>
         <td width="33px"></td>
         <td align="left" width="300px" style=""></td>
         <td width="33px"></td>
      </tr>
      <tr>
         <td align="left" style="font-family: TimesNewRoman;font-size: 16px;padding-left:10px;" colspan="2"><b>Authorized Official</b></td>
         <td align="left" style="font-family: TimesNewRoman;font-size: 16px;padding-left:10px;" colspan="2"><b>Receiver Signature</b></td>
         <td align="left" colspan="2"><table><tr><td style="font-family: TimesNewRoman;font-size: 16px;" align="left" colspan="2"><b>Date :</b></td><td style="border-bottom:1px solid #000;width:250px;font-family: TimesNewRoman;font-size: 16px;"></td></tr></table></td>
      </tr>
   </table>