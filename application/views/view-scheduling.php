<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <?php
    $check_add = false;
    $check_edit = false;
    if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
            if ($v['module_id'] == 34) {
                if ($v['add'] == 1) {
                    $check_add = true;
                }
                if ($v['edit'] == 1) {
                    $check_edit = true;
                }
            }
        }
    } else {
        $check_add = true;
        $check_edit = true;
    }
    ?>
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
        </ul>

        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
                <?php if ($check_add) { ?>
                            <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('scheduling/add'); ?>">Add</a>
                        <?php } ?>
                </div>
                    <div class="grid-body amc_quot-dt-pad">
                        <div class="row">
                            <form class="validate">
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>From Date</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>To Date</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 col-lg-3 responsve-mt-10">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </form>
                        </div>

                        <table class="table dataTables_wrapper amc_quot-dtable-disp" id="custom_table">
                            <thead>
                                <tr>
                                    <th width="10%">SR. No</th>
                                    <th width="40%">Schedule Date</th>
                                    <th width="30%">Standby Employee</th>
                                    <th width="20%" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="custom_row">
                            <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach(@$data as $k => $v) {
                                ?>
                                    <tr>
                                        <td><?= $count;?></td>
                                        <td><?= $v['schedule_date']?></td>
                                        <td><?= $v['user_name']?></td>
                                        <td>
                                        <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['schedule_id']; ?>" data-path="scheduling/detail"><i class="fa fa-eye"></i></a>
                                        <?php if ($check_edit) {
                                        ?>
                                            <a href="<?php echo site_url('scheduling/edit/' . @$v['schedule_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <?php }
                                        ?>
                                        </td>
                                    </tr>    
                                <?php
                                $count++; 
                                }
                            } 
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <?php include APPPATH . 'views/include/add_quarterly_visit_modal.php'; ?>
    <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $("form.validate").validate({
            rules: {
                to_date: {
                    required: true
                },
                from_date: {
                    required: true
                },

            },
            messages: {
                customer_id: "This field is required.",
            },
            invalidHandler: function(event, validator) {
                error("Please input all the mandatory values marked as red");
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('error-control').addClass('success-control');

            }
        });


        $(document).on("click", "#fetch", function() {
            
            var from_date = $('#from_date').val();
            if (from_date == '') {
                from_date = 0;
            }

            var to_date = $('#to_date').val();
            if (to_date == '') {
                to_date = 0;
            }

            $("#custom_row").find("tr").remove();
            $.ajax({
                url: "<?php echo site_url('Scheduling/search_view'); ?>",
                dataType: "json",
                type: "POST",
                data: {
                    from_date: from_date,
                    to_date: to_date,
                },
                cache: false,
                success: function(result) {
                    custom_table.clear().draw();
                    var count = 0;
                    for (var i = 0; i < result.data.length; i++) {
                        count ++;
                        var html = '';
                        html += '<tr>';

                        html += '<td>';
                        html += count;
                        html += '</td>';

                        html += '<td>';
                        html += result.data[i].schedule_date;
                        html += '</td>';

                        html += '<td>';
                        html += result.data[i].standby_employee;
                        html += '</td>';

                        html += '<td class="text-center">';
                        var schedule_id = result.data[i].schedule_id ;
                        html += '<a href="#" class="btn-primary btn btn-sm myModalBtn" data-id="' + schedule_id + '" data-path="scheduling/detail/"><i class="fa fa-eye"></i></a>';
                        html += '<a href="scheduling/edit/'+ schedule_id+ '" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                        html += '</td>';

                        html += '</tr>';
                        custom_table.row.add($(html)).draw(false);
                    }
                }
            });

        });


    });
</script>