
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Chart Of Accounts
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border"> 
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">
                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Account No</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="AccountNo"  type="text" value="<?= @$data['AccountNo']; ?>"  class="form-control" placeholder="Enter Account No">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Account Description</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="AccountDesc"  type="text" value="<?= @$data['AccountDesc']; ?>"  class="form-control" placeholder="Enter Account Description">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Account Group</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="AccountGroupID" name="AccountGroupID">
                                      <option value="" selected="selected" disabled="">- Select Account Group -</option>
                                      <?php
                                       foreach($account_group as $v){
                                        $selected = (isset($data['AccountGroupID']) &&  $data['AccountGroupID'] == $v['AccountGroupID'])? 'selected="selected"':'';
                                        ?> 
                                          <option <?= $selected ?> value="<?= $v['AccountGroupID'] ?>"> <?= $v['AccountGroupName'] ?></option>
                                      <?php
                                        }
                                      ?>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Account Level</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="AccountLevelID" name="AccountLevelID">
                                      <option value="" selected="selected" disabled="">- Select Account Level -</option>
                                      <?php
                                       foreach($account_level as $v){
                                        $selected = (isset($data['AccountLevelID']) &&  $data['AccountLevelID'] == $v['AccountLevelID'])? 'selected="selected"':'';
                                        ?> 
                                          <option <?= $selected ?> value="<?= $v['AccountLevelID'] ?>"> <?= $v['AccountLevelDesc'] ?></option>
                                      <?php
                                        }
                                      ?>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Account Type</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="AccountTypeID" name="AccountTypeID">
                                      <option value="" selected="selected" disabled="">- Select Account Type -</option>
                                      <?php
                                       foreach($account_type as $v){
                                        $selected = (isset($data['AccountTypeID']) &&  $data['AccountTypeID'] == $v['AccountTypeID'])? 'selected="selected"':'';
                                        ?> 
                                          <option <?= $selected ?> value="<?= $v['AccountTypeID'] ?>"> <?= $v['AccountType_Desc'] ?></option>
                                      <?php
                                        }
                                      ?>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">BL/PL</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="BL_PL" name="BL_PL">
                                      <option value="" selected="selected" disabled="">- Select BL/PL -</option>
                                      <option value="0" <?= ($page_title == 'add')?'selected':''?> <?=( @$data['BL_PL']==0 )? 'selected': ''; ?> >Balance Sheet A/c</option>
                                      <option value="1" <?=( @$data['BL_PL']==1 && $page_title != 'add')? 'selected': ''; ?>>Profit Loss A/c
                                      </option>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Status</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="InActive_Status" name="InActive_Status">
                                      <option value="" selected="selected" disabled="">- Select Status -</option>
                                      <option value="0" <?= ($page_title == 'add')?'selected':''?> <?=( @$data['InActive_Status']==0 )? 'selected': ''; ?> >Enabled</option>
                                      <option value="1" <?=( @$data['InActive_Status']==1 && $page_title != 'add')? 'selected': ''; ?>>Disabled
                                      </option>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-12">
                        <div class="form-group text-center">
                          </br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"><?= ($page_title == 'add')?'':''; ?> Submit</button>
                          <input name="id"  type="hidden" value="<?= @$data['AccountNo']; ?>">
                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        AccountNo:{
          required: true
        },
        AccountDesc:{
          required: true
        },
        AccountGroupID:{
          required: true
        },
        AccountLevelID:{
          required: true
        },
        AccountTypeID:{
          required: true
        },
        CompanyID:{
          required: true
        },
        InActive_Status:
        {
          required: true
        }
      }, 
      messages: {
        AccountNo: "This field is required.",
        AccountDesc: "This field is required.",
        AccountGroupID: "This field is required.",
        AccountLevelID: "This field is required.",
        AccountTypeID: "This field is required.",
        CompanyID: "This field is required.",
        InActive_Status: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });
 
  });
</script>