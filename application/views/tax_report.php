
<style>
.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}

</style>
   
   <div class="div-controls div-font-controls" style="text-align:center;padding-top:-12%;">
      <h3 class="main-heading">Smart Matrix General Trading LLC</h3>
      <br>
      <h4 class="main-second-heading">Tax Calculation</h4>
      <br>
   </div>

   <table style="width:100%;border-collapse: collapse;" class="table_align">
      <tr>
      <?php 
      if($history == 1)
      {
         $rev = '-R'.$data['tax_revised_no'];
      }else
      {
         $rev = ($data['tax_revised_no'] > 0)?'-R'.$data['tax_revised_no'] : '';   
      }
      ?>
         <th align="left" class="font-controls" style="padding-left:15px;padding-bottom:10px;"><b>Tax No. : <?= @$setval["company_prefix"].'TX-'.$data['tax_no'].$rev?></b></th>
         <th align="right" class="font-controls" style="padding-right:15px;padding-bottom:10px;"><b>Tax Date : <?= $data['tax_date']?></b></th>
      </tr>
   </table>

   <table style="width:100%;border-collapse: collapse;border-top:2px solid #000;border-bottom:2px solid #000;margin-bottom:15%;" class="table_align"> 
      <!-- first record -->
      <tr>
         <th align="left" width="60px" style="padding:5px 0 5px 10px;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
         <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Invoice No</b></th>
         <th align="right" width="250px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Invoice Date</b></th>
         <th align="right" width="100px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Tax</b></th>
         <th align="right" width="80px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Paid Amount</b></th>
      </tr>
      <?php
         $i = 0;
         foreach ($detail_data as $dt) {
             $i++;
         ?>
            
            <tr>
                <td align="left" style="padding:5px 0 5px 10px;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $i; ?></td>
                <td align="left" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><b><?= $dt['invoice_no']?></b></td>
                <td align="right" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['invoice_date']?></td>
                <td align="right" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['tax']?></td>
                <td align="right" style="padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= number_format($dt['paid_amount'],2)?></td>
            </tr>
         <?php }?>
     
      
   </table>
   
   <table style="width:100%;border-collapse: collapse;" class="bottom-table-div">
      <tr>
         <td align="left" width="300px" style="border-bottom:1px solid #000;"></td>
         <td width="33px"></td>
         <td align="left" width="300px" style="border-bottom:1px solid #000;"></td>
         <td width="33px"></td>
         <td align="left" width="300px" style=""></td>
         <td width="33px"></td>
      </tr>
      <tr>
         <td align="left" style="font-family: TimesNewRoman;font-size: 16px;padding-left:10px;" colspan="2"><b>Authorized Official</b></td>
         <td align="left" style="font-family: TimesNewRoman;font-size: 16px;padding-left:10px;" colspan="2"><b>Receiver Signature</b></td>
         <td align="left" colspan="2"><table><tr><td style="font-family: TimesNewRoman;font-size: 16px;" align="left" colspan="2"><b>Date :</b></td><td style="border-bottom:1px solid #000;width:250px;font-family: TimesNewRoman;font-size: 16px;"></td></tr></table></td>
      </tr>
   </table>