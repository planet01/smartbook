 <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Inventory
        </li>
         <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
      </ul>
    
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">             
                    
                    <div class="row">
                      <div class="col-md-3">
                              <div class="row">
                                  <div class="col-md-8">
                                       <label class="form-label">M.R.N.#</label>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-9">
                                     <div class="form-group">
                                          <div class="input-with-icon right controls my-pad-control">
                                              <i class=""></i>
                                              <input style="width:100%" type="text" readonly="readonly" value="<?php echo ($page_title != 'add') ? @$setval["company_prefix"].@$setval["material_prfx"].@$receipt_no : ''; ?>"  placeholder="">
                                               <input style="width:100%" type="hidden" readonly="readonly" name="receipt_no" value="<?= ($page_title == 'add')? 1 : @$receipt_no; ?>"  placeholder="">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      </div>

                      <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-8">
                                 <label class="form-label">Date</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                               <div class="form-group">
                                    <div class="input-with-icon right controls" id="span-pre">
                                        <i class=""></i>
                                        <input name="shipment_date"  type="text" value="<?= @$data[0]['shipment_date']; ?>"  class="form-control datepicker" placeholder="">
                                    </div>   
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-3">
                          <div class="row">
                              <div class="col-md-8">
                                   <label class="form-label">M.R.N. Type</label>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-9">
                                 <div class="form-group">
                                      <div class="input-with-icon right controls" id="span-pre">
                                          <i class=""></i>
                                          <input type="hidden" name="shipment_type_old" value="<?= @$data[0]['shipment_type'] ?>"/>
                                          <div class="radio radio-success">
                                            <input name="shipment_type" <?= (@$data[0]['shipment_type'] == 'shipment') ? 'checked' : ''; ?>  type="radio" value="shipment"  id="shipment" />
                                            <label for="shipment">Shipment</label>
                                            
                                            <input name="shipment_type" <?= (@$data[0]['shipment_type'] == 'transfer') ? 'checked' : ''; ?>   type="radio" value="transfer"  id="transfer" />
                                            <label for="transfer">Transfer</label>
                                          </div>
                                          <!-- <label for="shipment" >Shipment</label> -->
                                          
                                      
                                          <!-- <label for="transfer" class="prefix">Transfer</label> -->
                                          
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3 transfer_from_list" <?= (@$data[0]['shipment_type'] != 'transfer') ? 'style="display: none;"' : ''; ?>>
                                <div class="row">
                                    <div class="col-md-8">
                                         <label class="form-label">Transfor from(Warehouse)</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                       <div class="form-group">
                                            <div class="input-with-icon right controls my-pad-control">
                                                <i class=""></i>
                                                <select class="form-control input-signin-mystyle select2 my-padding-style" id="warehouse_from" name="warehouse_from">
                                                <option value="" selected="selected" disabled="">- Select Type -</option>
                                                <?php
                                                foreach($warehouseData as $type){
                                                $selected = (isset($data[0]['warehouse_from']) &&  $data[0]['warehouse_from'] == $type['warehouse_id'])? 'selected="selected"':'';
                                                ?> 
                                                    <option <?= $selected ?> value="<?= $type['warehouse_id'] ?>"> <?= $type['warehouse_name'] ?></option>
                                                <?php
                                                }
                                                ?> 
                                            </select>
                                            <input type="hidden" name="warehouse_from_old" value="<?= @$data[0]['warehouse_from'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                          </div>
                    </div>
                     
                        <input type="hidden" id="warehouse_from_selected" />
                       <!--Jd <input type="hidden" name="belongs_to" value="<?= @$data[0]['belongs_to']?>" /> -->
                        <div class="form-group" >
                            <div class="row" >
                                <div class="col-md-12">
                                  <div class="dataTables_wrapper-1024">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th class="text-center" style="width: 60px;" >Add/Remove Row</th>
                                          <th  class="text-center inventory-dt-th2" >Product</th>
                                          <th class="text-center inventory-dt-th3" >Location</th>
                                          <th class="text-center inventory-dt-th4" >Quantity</th>
                                        </tr>
                                      </thead>
                                      <tbody id="customFields">
                                        <tr class="txtMult">
                                          <td class="text-center" style="width: 60px;"><a href="javascript:void(0);" class="addCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                          <td colspan="3"></td>
                                        </tr>

                                         <?php
                                         $total_quantity = 0;
                                         $c= 0;
                                         // print_b($material_receive_note_detail_data);
                                         ?>
                                        
                                          <?php
                                                /*$total_quantity += $v['material_receive_note_detail_quantity']; 
                                            }*/
                                          if(@$data != null && !empty(@$data)){
                                          foreach(@$data as $row){
                                            $c++;
                                         ?>

                                         <tr class="txtMult">
                                                <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                <td style="border-right: 1px solid #fff !important;">
                                                    <select name="product_id[]" style="width:100%;" id="product_id_<?= $c; ?>" onChange="handle_description('_<?= $c ?>')" class="select2  prod_name" required>
                                                        <option selected disabled value="">--- Select Product ---</option>
                                                        <?php
                                                          $select_des = "";
                                                          foreach($productData as $k => $v){ 
                                                            (@$row['product_id'] == $v['product_id'] )? $select_des = $v['product_description'] : '';
                                                        ?>
                                                            //Jd did, match from backup file.
                                                            <option  <?=( @$row['product_id'] == $v['product_id'] )? 'selected ': '' ?>
                                                            value="<?= $v['product_id'].','.""; ?>" data-id="<?= $v['product_id'] ?>">
                                                               <?= $v['product_sku'] ?> -  <?= $v['product_name']; ?>
                                                            </option>
                                                            <?php } ?>
                                                    </select>
                                                    <!-- </br> -->
                                                </td>
                                                <input type="hidden" value="<?= $row['inventory_id']; ?>" name="row_id[]">
                                                <!-- <td width="30%">
                                                    <div  id="product_desc_<?= $c ?>" class="form-control"><?= $select_des ?></div>
                                                </td> -->
                                                <td>
                                                    <select name="warehouse_id[]" style="width:100%" onchange="handle_data('+x+')" id="warehouse_id'+x+'" class="form-control warehouse_id" required>
                                                        <option selected disabled>--- Select Location ---</option>
                                                        <?php 
                                                            $floor   = 0;
                                                            $section = 0;
                                                            foreach($warehouseData as $k => $v){ 
                                                               if(@$row['warehouse_id'] == $v['warehouse_id'] ){
                                                                 $floor = $v['warehouse_no_of_shelf'];
                                                                 $section = $v['warehouse_no_of_section'];
                                                               }
                                                            ?>
                                                            <option <?=( @$row['warehouse_id'] == $v['warehouse_id'] )? 'selected ': '' ?>
                                                            value="<?= $v['warehouse_id']; ?>">
                                                                <?= $v['warehouse_name']; ?>
                                                            </option>
                                                            <?php } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <!-- <?=  @inventory_quantity_by_warehouse($row['product_id'], $row['warehouse_id']) ?> -->
                                                    <input type="text" class="code quantity txtboxToFilter inventory_quantity" required style="width:100%" id="inventory_quantity'+x+'" data-optional="0" name="inventory_quantity[]" 
                                                    value=" <?= @$row['inventory_quantity'] ?>" placeholder="" />
                                                    <input type="hidden" class="code quantity txtboxToFilter inventory_quantity_old" required style="width:100%" id="inventory_quantity_old'+x+'" data-optional="0" name="inventory_quantity_old[]" 
                                                    value="<?= @$row['inventory_quantity'] ?>" placeholder="" />
                                                </td>
                                            </tr>
                                        <?php } } ?>

                                      </tbody>
                                    </table>
                                  </div>  
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-12">
                                  <label class="form-label">Note</label>
                                </div>
                                <div class="col-md-12">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <textarea name="note" class="form-control txtarea-control" value="" placeholder=""><?= @$data[0]['note']; ?></textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-center">
                             <button class="btn btn-success btn-cons ajaxFormSubmitAlter my-bttn" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data[0]['id']; ?>">
                            </div>
                        </div>
                    </div>    
                  </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  




<script type="text/javascript">
    $(document).ready(function() {
        $("form.validate").validate({
            rules: {
                product_id: {
                    required: true
                },
                inventory_quantity: {
                    required: true,
                    digits: true
                },
                warehouse_id: {
                    required: true
                },
                /*receipt_no: {
                    required: true
                },*/
                shipment_date:{
                  required: true
                },
                shipment_type:{
                  required:true
                },
                warehouse_from:{
                required:{
                  depends: function(element){
                   var status = false;
                   if($("input[name='shipment_type']:checked").val() == 'transfer'){
                       var status = true;
                   }
                   return status;
                 }
                },
                digits: true
                },
            },
            messages: {
                product_id: "This field is required.",
                inventory_quantity:{
                    required: "This field is required.",
                    digits: "Please enter only digits."
                },
                warehouse_id: "This field is required.",
                //Jd inventory_no_of_shelf: "This field is required.",
                //Jd inventory_no_of_section: "This field is required.",
                status: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit  
                error("Please input all the mandatory values marked as red");
  
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    
    });

function handle_data(id){
        $.ajax({
          url: "<?php echo site_url('inventory/warehouse_data'); ?>",
          dataType: "json",
          type: "POST",
          data: {'id': $("#warehouse_id"+id).find('option:selected').val()} ,
          cache: false,
          success: function(data) {
            if(data) {
              var total_shelf   = data[0].warehouse_no_of_shelf;
              var total_section = data[0].warehouse_no_of_section;
              var shelf = "";
              var section = "";
              for(var i=1; i<=total_shelf; i++){
                shelf += '<option value="'+i+'">'+i+'</option>';
              }
              for(var j=1; j<=total_section; j++){
                section += '<option value="'+j+'">'+j+'</option>';
              }
              $("#inventory_no_of_shelf_list"+id).siblings().remove();
              $("#inventory_no_of_shelf"+id).append(shelf);
              $("#inventory_no_of_section_list"+id).siblings().remove();
              $("#inventory_no_of_section"+id).append(section);
              
            } else {
              /*$("#heading").hide();
              $("#records").hide();
              $("#no_records").show();*/
            }
          }
        });
     
    }
$(document).ready(function(){

  $(".addCF").live('click', function(e){
    e.preventDefault();
    $(".gridAddBtn").remove();
    $('form.validate').validate();
      // if(x < max_fields){
        x++;
        var temp = '<tr class="txtMult">';
        temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="addCF gridAddBtn"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a><a href="javascript:void(0);" class="remCF"> <span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>';
        temp    += '<td style="border-right: 1px solid #fff !important;">';
        temp    += '<select name="product_id[]" onChange="handle_description('+x+')" style="width:100%;" id="product_id'+x+'" class="select2 prod_name"  required>';
        temp    += '<option selected disabled value="">--- Select Product ---</option>';
        temp    += '<?php foreach($productData as $k => $v){ ?>';
        //Jdtemp    += '<option value="<?= $v['product_id'].','.$v['product_sku']; ?>" data-id="<?= $v['product_id'] ?>" >';
        temp    += '<option value="<?= $v['product_id'].','.""; ?>" data-id="<?= $v['product_id'] ?>" >';
        //Jdtemp    += '<?= $v['product_sku']; ?> - <?= $v['product_name']; ?>';
        temp    += '<?= $v['product_sku']; ?> - <?= $v['product_name']; ?>';
        temp    += '</option><?php } ?>';
        temp    += '</select>';
        temp    += '</td>';
       /* temp    += '<td width="30%">';
        temp    += '<div  id="product_desc'+x+'" class="form-control"></div>';
        temp    += '</td>';*/
        temp    += '<td>';
        temp    += '<select name="warehouse_id[]" style="width:100%" onchange="handle_data('+x+')" id="warehouse_id'+x+'" class="warehouse_id" required>';
        temp    += '<option selected disabled>--- Select Location ---</option>';
        temp    += '<?php foreach($warehouseData as $k => $v){ ?>';
                    var find_id = "<?= $v['warehouse_id'] ?>";
                    if(parseInt($("#warehouse_from_selected").val()) != parseInt(find_id)){
          temp    += '<option value="<?= $v['warehouse_id']; ?>">';
          temp    += '<?= $v['warehouse_name']; ?>';
          temp    += '</option>';
        }else{
          temp    += '<option disabled="disabled" value="<?= $v['warehouse_id']; ?>">';
          temp    += '<?= $v['warehouse_name']; ?>';
          temp    += '</option>';

        }
        temp    += '<?php } ?>';
        temp    += '</select>';
        temp    += '</td>';
        temp    += '<td>';
        temp    += '<input type="number" class="code quantity txtboxToFilter inventory_quantity" min="1" required  style="width:100%" id="inventory_quantity'+x+'" data-optional="0" name="inventory_quantity[]" value="" placeholder="" />';
        temp    += '<input type="hidden" class="code quantity txtboxToFilter inventory_quantity_old" min="1" required  style="width:100%" id="inventory_quantity_old'+x+'" data-optional="0" name="inventory_quantity_old[]" value="" placeholder="" />';
        temp    += '</td></tr>'
        $("#customFields").append(temp);
        setTimeout(function(){
          $('select.select2').select2();
        }, 100);

        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
});

    var max_fields      = 6; 
    var x = 1; 
    
    $("#customFields").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
  
});

function handle_description(id) {
   var pro_id = $("#product_id"+id+" option:selected").attr('data-id');
   $.ajax({
      url: "<?php echo site_url('inventory/product_desc'); ?>",
      dataType: "json",
      type: "POST",
      data:  {'id': pro_id},
      cache: false,
      success: function(rec_data) {
        if(rec_data) {
          var desc = rec_data.product_description;
          $("#product_desc"+id).html(desc);
        } 
      }
     });
}
$(document).on('keydown keypress keyup change blur', ".txtMult input,.txtMult select",function () { 
        var check = $(this).attr('data-optional');
        var mult = 0;
        $("#customFields tr.txtMult").each(function () {
            var $quatity_num = $('.quatity_num', this).val();
            if($quatity_num == undefined){
                $quatity_num = ($quatity_num == undefined)?0:$quatity_num;
            }
            var $total = ($quatity_num * 1);
            mult += $total;
        });

        $("#total_quantity").val(mult).text(mult);
           
  });
  

$('input[type=radio][name=shipment_type]').change(function() {
  if (this.value == 'shipment') {
     $(".transfer_from_list").hide();
  }
  else if (this.value == 'transfer') {
     $(".transfer_from_list").show(); 
  }
});
$('#warehouse_from').change(function() {
  var id = this.value;
  $("#warehouse_from_selected").val(id);
  $('.warehouse_id').children().prop("disabled", false);
  $(".warehouse_id option[value*='"+id+"']").prop('disabled',true);
  $(".warehouse_id option[value=0]").attr('selected','selected');


  //$(".warehouse_id option[value*='"+id+"']").removeAttr("selected");
});


$(document).ready(function() {
    $("#user_type").change(function () {
      var val = $(this).val();
      if (val == 2) {
        $('.myUserType').slideDown();
      }else{
        $('.myUserType').slideUp();
      }
    });
    $( ".datepicker" ).datepicker({
       format: "yyyy-mm-dd",
       autoclose: true
       
    });
 });
 setTimeout(function(){
$('.select2').select2();
}, 1000);

$(document).on('change', ".prod_name", function() {
   var selected_val = $(this).val();
   var chk  = 0;
   $("tr.txtMult").each(function() {
      var row = $(this).closest('tr'); 
      var prod  = row.find('.prod_name :selected').val();
      if(prod !== undefined){
        var find_prod = prod.split(',');
        var this_find_prod = selected_val.split(',');
        if(this_find_prod[0] == find_prod[0]){
          chk++;
        }
      }           
  });
  if(chk > 1){
    $(this).select2().select2('val', '');

  }    
});

</script>