<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
        <!--</div>-->
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
                        <?php
                        $location_add = false;
                        $location_print = false;
                        if ($this->user_type == 2) {
                            foreach ($this->user_role as $k => $v) {
                                if ($v['module_id'] == 24) {
                                    if ($v['add'] == 1) {
                                        $location_add = true;
                                    }
                                    if ($v['print'] == 1) {
                                        $location_print = true;
                                    }
                                }
                            }
                        } else {
                            $location_add = true;
                            $location_print = true;
                        }
                        ?>
                        <?php if ($location_add) { ?>
                            <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('Follow_Up/add'); ?>">Add</a>
                        <?php } ?>
                        <!--<div class="pull-right">-->
                        <!--  <a href="<?php echo site_url($add_product) ?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>-->
                        <!--  <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>-->
                        <!--  <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>-->
                        <!--  <ul class="dropdown-menu">-->
                        <!--    <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>-->
                        <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>-->
                        <!--    <li class="divider"></li>-->
                        <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>-->
                        <!--    <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>-->
                        <!--  </ul>-->
                        <!--  <span class="h-seperate"></span>-->
                        <!--  <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>-->
                        <!--</div>-->
                    </div>
                    <div class="grid-body ">
                        <?php if ($location_print) { ?>

                            <form id="report_form" class="validate" target="_blank" action="<?= site_url('follow_up/print_report') ?>" method="post">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-4 date_input">
                                                <div class="form-group mg-10">
                                                    <div class="input-with-icon right controls">
                                                        <label for="checkbox1"><b>From Date</b></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 date_input">
                                                <i class=""></i>
                                                <input class="datepicker form-control  " type="text" name="from_date" id="from_date" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4 date_input">
                                                <div class="form-group mg-10">
                                                    <div class="input-with-icon right controls">
                                                        <label for="checkbox1"><b>To Date</b></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 date_input">
                                                <i class=""></i>
                                                <input class="datepicker form-control " type="text" id="to_date" name="to_date" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 responsve-mt-10">
                                        <div class="row">
                                            <div class="col-md-9 col-lg-10">
                                                <div class="form-group">
                                                    <input class="btn btn-success btn-cons  soa_btn" type="submit" value="Print">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-4 date_input">
                                                <div class="form-group mg-10">
                                                    <div class="input-with-icon right controls">
                                                        <label for="checkbox1"><b>Customer</b></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 date_input">
                                                <i class=""></i>
                                                <select name="customer" id="customer" class="custom_select" style="width: 100%;" required>
                                                    <option selected  value='0'>--- Select Customer ---</option>
                                                    <?php
                                                    foreach (@$customer as $k => $v) {
                                                    ?>
                                                        <option><?= $v['follow_up_company_name'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        <?php } ?>

                        <table class="table" id="custom_table">
                            <thead>
                                <tr>
                                    <th width="200px">Sr.No.</th>
                                    <th width="200px">Date</th>
                                    <th width="200px">Customer</th>
                                    <th width="200px">Type</th>
                                    <th width="200px">Doc No</th>
                                    <th width="200px">Remarks</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach (@$data as $k => $v) {
                                ?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <td>
                                                <?= date("Y-m-d", strtotime($v["follow_up_date"])) ?>
                                            </td>
                                            <td><?= $v['follow_up_company_name'] ?></td>
                                            <td>

                                                <?php
                                                if ($v['follow_up_doc_type'] == 1) {
                                                ?>
                                                    Quotation
                                                <?php
                                                } else if ($v['follow_up_doc_type'] == 2) {
                                                ?>
                                                    Perfoma Invoice
                                                <?php
                                                } else if ($v['follow_up_doc_type'] == 3) {
                                                ?>
                                                    Invoice
                                                <?php
                                                } else if ($v['follow_up_doc_type'] == 4) {
                                                ?>
                                                    AMC Invoice
                                                <?php
                                                } else if ($v['follow_up_doc_type'] == 5) {
                                                ?>
                                                    AMC Quotation
                                                <?php
                                                }  else if ($v['follow_up_doc_type'] == 6) {
                                                
                                                    ?>
                                                        Inquiry Recieved History
                                                    <?php 
                                                    }
                                                    ?>
                                                

                                            </td>
                                            <td>

                                                <?php
                                                if ($v['follow_up_doc_type'] == 1) {
                                                    $quotation = getCustomRow("Select * From quotation where quotation_id = '" . $v['follow_up_doc_id'] . "'");
                                                    echo $setting['company_prefix'] . $setting['quotation_prfx'] . (($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no']);
                                                } else if ($v['follow_up_doc_type'] == 2) {
                                                    $quotation = getCustomRow("Select * From quotation where quotation_id = '" . $v['follow_up_doc_id'] . "'");
                                                    echo $setting['company_prefix'] . $setting['quotation_prfx'] . (($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no']);
                                                } else if ($v['follow_up_doc_type'] == 3) {
                                                    $invoice = getCustomRow("Select * From invoice where invoice_id = '" . $v['follow_up_doc_id'] . "'");
                                                    $rev = ($invoice['invoice_revised_no'] > 0) ? '-R' . $invoice['invoice_revised_no'] : '';
                                                    echo @$setting["company_prefix"] . @$setting["invoice_prfx"]; ?><?php echo $invoice['invoice_no'] . $rev;
                                                } else if ($v['follow_up_doc_type'] == 4) {
                                                    $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '" . $v['follow_up_doc_id'] . "'");
                                                    echo @$setting["company_prefix"]; ?><?= ($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7) ? 'AMCINV-' : @$setting["quotation_prfx"] . (($amc_invoice['amc_quotation_revised_no'] > 0) ? @$amc_invoice['amc_quotation_no'] . '-R' . number_format(@$amc_invoice['amc_quotation_revised_no']) : @$amc_invoice['amc_quotation_no']);
                                                } else if ($v['follow_up_doc_type'] == 5) {
                                                    $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '" . $v['follow_up_doc_id'] . "'");
                                                    echo @$setting["company_prefix"]; ?><?= ($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7) ? 'AMCINV-' : @$setting["quotation_prfx"] . (($amc_invoice['amc_quotation_revised_no'] > 0) ? @$amc_invoice['amc_quotation_no'] . '-R' . number_format(@$amc_invoice['amc_quotation_revised_no']) : @$amc_invoice['amc_quotation_no']);
                                                }  else if ($v['follow_up_doc_type'] == 6) {
                                                    $inquiry = getCustomRow("Select * From inquiry_received_history where inquiry_received_history_id  = '" . $v['follow_up_doc_id'] . "'");
                                                    echo 'SM-INQ-'.$inquiry['inquiry_no'];
                                                }
                                                ?>

                                            </td>
                                            <td><?= $v['follow_up_remarks'] ?></td>
                                            <td><a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['follow_up_id']; ?>" data-path="Follow_up/detail"><i class="fa fa-eye"></i></a></td>

                                        </tr>
                                <?php
                                        $count++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $('.custom_select').select2({
            minimumInputLength: 4
        });
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true

        });

        $(document).on("click", "#fetch", function() {

var from_date = $('#from_date').val();
if (from_date == '') {
  from_date = 0;
}

var to_date = $('#to_date').val();
if (to_date == '') {
  to_date = 0;
}

var customer = $('#customer option:selected').val();

$.ajax({
  url: "<?php echo site_url('follow_up/search_view'); ?>",
  dataType: "json",
  type: "POST",
  data: {
    from_date: from_date,
    to_date: to_date,
    customer:customer,
  },
  cache: false,
  success: function(result) {
    custom_table.clear().draw();
    var count = 0;
    for (var i = 0; i < result.data.length; i++) {
      count++;
      var html = '';
      html += '<tr>';

      html += '<td >';
      html += count;
      html += '</td>';

      html += '<td >';
      html += result.data[i].follow_up_date;
      html += '</td>';

      html += '<td >';
      html += result.data[i].follow_up_company_name;
      html += '</td>';

      html += '<td >';
      html += result.data[i].follow_up_doc_type;
      html += '</td>';

      html += '<td >';
      html += result.data[i].follow_up_doc_no;
      html += '</td>';

      html += '<td>';
      html += result.data[i].follow_up_remarks;
      html += '</td>';

      html += '<td>';

            html += '<a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="' + result.data[i].follow_up_id + '" data-path="Follow_up/detail"><i class="fa fa-eye"></i></a>';
             
      html += '</td>';
      html += '</tr>';
      custom_table.row.add($(html)).draw(false);
    }
    
  }
});

});
    });
</script>