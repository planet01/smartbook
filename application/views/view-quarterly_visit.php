<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <?php
      $check_add =false;
      if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
          if ($v['module_id'] == 29) {
            if ($v['add'] == 1) {
              $check_add = true;
            }
          }
        }
      } else {
        $check_add = true;
      }
      ?>
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            
            <div class="grid-body amc_quot-dt-pad">
              <div class="row">
                  <form class="validate">
                <div class="col-md-3 col-lg-3">
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <div class="radio radio-success responsve-receivable-radio">
                            <input id="checkbox1" type="radio" name="invoice_type" value="0">
                            <label class="" for="checkbox1"><b>Under Warrenty</b></label>
                                
                            <input id="checkbox2" checked="true" type="radio" name="invoice_type" value="1">
                            <label class="" for="checkbox2"><b>All</b></label>      
                          </div> 
                        </div>
                      </div>
                </div>
                <div class="col-md-4 col-lg-4" id="due_since_div">
                    <div class="row">
                      <div class="col-md-6 col-lg-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                              <label for="checkbox1"><b>Due Since</b></label>     
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="due_since" id="due_since" value=""  >
                      </div>
                    </div>
                </div>
                <div id='print_history_div'>
                  <div class="col-md-3 col-lg-3">
                      <div class="row">
                        <div class="col-md-6 col-lg-4 date_input">
                          <div class="form-group mg-10">
                            <div class="input-with-icon right controls">
                                <label for="checkbox1"><b>From Date</b></label>     
                            </div>
                          </div>
                        </div>
                        <div class="col-md-8 date_input">
                          <i class=""></i>
                          <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>"  >
                        </div>
                      </div>
                  </div>
                  <div class="col-md-3 col-lg-3">
                      <div class="row">
                        <div class="col-md-6 col-lg-4 date_input">
                          <div class="form-group mg-10">
                            <div class="input-with-icon right controls">
                                <label for="checkbox1"><b>To Date</b></label>     
                            </div>
                          </div>
                        </div>
                        <div class="col-md-8 date_input">
                          <i class=""></i>
                          <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>"  >
                        </div>
                      </div>
                  </div>

                  <div class="col-md-2 col-lg-2 responsve-mt-10 ">
                    <div class="row">
                      <div class="col-md-9 col-lg-10">
                        <div class="form-group">
                          <a class="btn btn-warning" id="print_history_button" >Print Service History</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4 col-lg-4">
                  <div class="row">
                    <div class="col-md-3 col-lg-3">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Customer</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                      <i class=""></i>
                      <select name="customer_id" id="customer_id" class="select2 form-control">
                        <option value="0" selected <?= (@$data['invoice_status'] == 3)? '' : 'disabled'?>>--- Select Customer ---</option>
                        <?php
                        foreach($customerData as $k => $v){
                        ?>
                        <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['customer_id'])? 'selected': ''; ?> >
                        <?= $v['user_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div>

                <div class="col-md-3 col-lg-3">
                  <div class="row">
                    <div class="col-md-4 col-lg-4">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>End User</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-lg-8">
                      <i class=""></i>
                      <select name="end_user" id="end_user" class="select2 w-100">
                        <option value="0" selected >--- Select End User ---</option>
                      </select>  
                    </div>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2">
                  <div class="row">
                    <div class="col-md-3 col-lg-3">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>City</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                      <i class=""></i>
                      <select name="city_name" id="city_name" class="select2 form-control">
                        <option value="0" selected >--- Select City ---</option>
                        <?php
                        foreach($cityData as $k => $v){
                        ?>
                        <option value="<?= $v['city_name']; ?>" >
                        <?= $v['city_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div>

                <div class="col-md-3 col-lg-3 responsve-mt-10">
                  <div class="row">
                    <div class="col-md-4 col-lg-4">
                      <div class="form-group">
                         <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch"> 
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="clearfix"></div>
                </form>
              </div>
              
              <table class="table dataTables_wrapper amc_quot-dtable-disp" id="custom_table"  >
                <thead>
                  <tr>
                    <th width="15%">Invoice No</th>
                    <th width="40%">Customer<br>End User</th>
                    <th width="10%">Warranty Expire</th>
                    <th width="10%">Last Service</th>
                    <th width="25%" class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody id="custom_row">
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <?php include APPPATH.'views/include/add_quarterly_visit_modal.php'; ?>
    <!-- /.modal -->
    </div>
    <script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $('#due_since_div').hide();
        $( ".datepicker" ).datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $("form.validate").validate({
            rules: {
                customer_id:{
                  required: true
                },  
                to_date:{
                  required: true
                },  
                from_date:{
                  required: true
                },  
                
            }, 
            messages: {
                customer_id: "This field is required.",
            },
            invalidHandler: function (event, validator) {
                error("Please input all the mandatory values marked as red");
                },
                errorPlacement: function (label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');  
                },
                highlight: function (element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control'); 
                },
                unhighlight: function (element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control'); 
                },
                success: function (label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('error-control').addClass('success-control');
                
                }
        });

        $('[name="invoice_type"]').change(function () {
          var invoice_type = $('[name="invoice_type"]:checked').val();
          if(invoice_type == 1)
          {
            $('#print_history_div').show();
            $('#due_since_div').hide();
          }
          else
          {
            $('#print_history_div').hide();
            $('#due_since_div').show();
          }
          
        });

        $(document).on("click", "#fetch", function(){
            var customer = $('#customer_id').valid();
            var invoice = $('[name="invoice_type"]').valid();
            if(!customer || !invoice)
            {
                return false;
            }
            var from_date = $('#from_date').val();
            if(from_date == '')
            {
              from_date= 0;
            }
            var due_since = $('#due_since').val();
            var invoice_type = $('[name="invoice_type"]:checked').val();
            var customer_id = $('#customer_id option:selected').val();
            var end_user = $('#end_user option:selected').val();
            var city_name = $('#city_name option:selected').val();
            $("#custom_row").find("tr").remove();
            $.ajax({
              url: "<?php echo site_url('Quarterly_visit/fetch_invoice'); ?>",
              dataType: "json",
              type: "POST",
              data: {
                    invoice_type:invoice_type,
                    due_since: due_since,
                    customer_id:customer_id,
                    end_user:end_user,
                    city_name:city_name
                    },
              cache: false,
              success: function(invoiceData) {
                custom_table.clear().draw();
                for (var i = 0; i < invoiceData.data.length; i++) {
                  var html = '';
                    html += '<tr>';
                        html += '<td>';
                        var $rev; 
                        if(invoiceData.data[i].invoice_revised_no > 0){
                            $rev = '-R'+invoiceData.data[i].invoice_revised_no
                        }
                        else{
                            $rev = '';
                        }
                        var $rev1;
                        if(invoiceData.data[i].invoice_status == 2)
                        {
                            $rev1 = '-'+invoiceData.data[i].invoice_revised_no; 
                        } 
                        else{$rev1 =''};
                        html += invoiceData.setting.company_prefix + invoiceData.setting.invoice_prfx + invoiceData.data[i].invoice_no + $rev;
                        html += '</td>';

                        var user_name = invoiceData.data[i].user_name;
                        html += '<td>';
                        html += user_name+'<br>';
                        if(invoiceData.data[i].end_user_name != '' && invoiceData.data[i].end_user_name !== null)
                        {
                            user_name = invoiceData.data[i].end_user_name;
                            html += user_name;
                        }
                        html += '</td>';

                        html += '<td>';
                        html += invoiceData.data[i].invoice_expire_date;
                        html += '</td>';

                        var last_visit = invoiceData.data[i].last_service_date;
                        if(last_visit === null)
                        {
                          last_visit = '';
                        }
                        var ticket_no = invoiceData.data[i].ticket_no;
                        if(ticket_no === null)
                        {
                          ticket_no = '';
                        }

                        html += '<td>';
                        html += ticket_no+'<br>'+last_visit;
                        html += '</td>';
                        
                        html += '<td class="text-center">';
                        var invoice_id = invoiceData.data[i].invoice_id;
                        html += '<a class="btn-primary btn btn-sm mr-1" href="'+invoiceData.base_url+'Ticket_management/add_invoice_ticket/'+invoice_id+'" >Create Ticket</a>';
                        html += '<a href="#" class="btn-primary btn btn-sm myModalBtn" data-id="'+invoice_id+'" data-path="Quarterly_visit/ticket_detail/"><i class="fa fa-history"></i></a>';
                        // html += '<a href="" data-id="'+invoiceData.data[i].invoice_id+'" class="mr-2 btn-warning btn btn-sm quarterly_visit" data-toggle="tooltip" title="Add">Serviced</a>';
                        html += '</td>';                        

                    html += '</tr>';
                    custom_table.row.add($(html)).draw(false);
                }
                // $('#custom_row').append(html);
                // custom_table.row.add($(html)).draw(false);
              }
            });
            
        });

        $(document).on("click", "#print_history_button", function(e){
          e.preventDefault();
          var from_date = $('#from_date').valid();
          var to_date = $('#to_date').valid();
          if(!from_date || !to_date)
          {
              return false;
          }
          
          from_date = $('#from_date').val();
          to_date = $('#to_date').val();

          window.open('print?from_date='+from_date+'&to_date='+to_date);  
          return false;
        });
        
        $('#customer_id').change(function () {
          var customer_id = $("#customer_id").find(':selected').val();
          $.ajax({
              url: "<?php echo site_url('Ticket_management/get_customer_end_users'); ?>",
              dataType: "json",
              type: "GET",
              data: {
                customer_id: customer_id,
              },
              cache: false,
              success: function(invoiceData) {
                var html = '';
                html += '<option value="0" selected >--- Select End User ---</option>';
                if(invoiceData.data != undefined){
                  for (var i = 0; i < invoiceData.data.length; i++) {
                    html += '<option value="'+invoiceData.data[i].end_user_id+'" >'+invoiceData.data[i].end_user_name+'</option>';
                  }
                }
                
                $('#end_user').html(html).trigger('change');
              }
            });
        });

    });
    </script>
    
  
