<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">Account Ledger</a> </li>
            
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-body">
                        <h2>Account Ledger</h2>
                        <form class="row validate" autocomplete="off" id="dashboard-form" style="margin-bottom:20px">
                            <div class="form-group col-sm-12 col-md-6 col-lg-2 date_input">
                                <label >From Date:</label>
                                <input type="text" name="fromdate" value="<?=@$fromdate?>" id="fromdate" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-2">
                                <label >To Date:</label>
                                <input type="text" name="todate" id="todate" value="<?=@$todate?>" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-2">
                                <label >Accounts:</label>
                                <select name="AccountNo" id="AccountNo" class="form-control select2" >
                                    <option value='0'>Select Account</option>
                                    <?php
                                        foreach($accounts as $k => $v){
                                        ?>
                                        <option value="<?= $v['AccountNo']; ?>" <?= @$account_id == $v['AccountNo']?'selected':''?> >
                                        <?= $v['AccountNo'].'-'.$v['AccountDesc'] ?>
                                        </option>
                                        <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-2">
                                <label >Type:</label>
                                <div class="input-with-icon right controls">
                                <i class=""></i>
                                <div class="radio radio-success responsve-receivable-radio">
                                    <input id="report_type1" checked="true" type="radio" name="report_type" value="0">
                                    <label class="" for="report_type1"><b>Detail</b></label>
                                        
                                    <input id="report_type2" type="radio" name="report_type" value="1">
                                    <label class="" for="report_type2"><b>Summary</b></label>      
                                </div> 
                              </div>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                              <a href="" id="print" class="btn btn-success" style="margin-top:25px">Print</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        
        $( ".datepicker" ).datepicker({
           format: "yyyy-mm-dd",
           autoclose: true
           
        });
    });
    $("form.validate").validate({
      rules: { 
        fromdate:{
          required: true
        }, 
        todate:{
          required: true
        }, 
        
      }, 
      messages: {
        fromdate:"This field is required.",
        todate:"This field is required."
      },
        invalidHandler: function (event, validator) {
        //display error alert on form submit    
          error("Please input all the mandatory values marked as red");
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });

      
      $(document).ready(function() {
          $('#print').on("click", function(event){
            event.preventDefault();
              var AccountNo;
              var date_from = $("#fromdate").valid();
              var date_to   = $("#todate").valid();
              if(!date_from || !date_to ){
                return false;
              }
              else
              {
                from_date = $('#fromdate').val();
                to_date = $('#todate').val();

                var report_type = $('[name="report_type"]:checked').val();
                if(report_type == 0) //Detail
                {
                  window.open('account_ledger_print?from_date='+from_date+'&to_date='+to_date);  
                }
                else //Summary
                {
                  window.open('account_ledger_summary_print?from_date='+from_date+'&to_date='+to_date+'&AccountNo='+AccountNo);  
                }
                
                return false;
              }
          });

      });
</script>