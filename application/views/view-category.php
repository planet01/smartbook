  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Category
        </li>
        <li><a href="#" class="active">View All <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span> <span class="fa fa-sort-desc"></spam></h4>
              <?php
                $categor_add = false;
                $categor_edit = false;
                if ($this->user_type == 2) {
                    foreach ($this->user_role as $k => $v) {
                    if($v['module_id'] == 1)
                    {
                        if($v['add'] == 1)
                        {
                            $categor_add = true;
                        }
                        if($v['edit'] == 1)
                        {
                            $categor_edit = true;
                        }
                    }
                    }
                }else
                {
                    $categor_add = true;
                    $categor_edit = true;
                }
              ?>
              <?php if($categor_add){?>
              <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('category/add'); ?>">Add</a>
              <?php } ?>
              <!--<div class="pull-right">-->
              <!--  <a href="<?php echo site_url($add_product)?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>-->
              <!--  <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>-->
              <!--  <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>-->
              <!--  <ul class="dropdown-menu">-->
              <!--    <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>-->
              <!--    <li class="divider"></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>-->
              <!--    <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>-->
              <!--  </ul>-->
              <!--  <span class="h-seperate"></span>-->
              <!--  <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>-->
              <!--</div>-->
            </div>
            <div class="grid-body ">
              <table class="table" id="example3" >
                <thead>
                  <tr>
                    <th class="cat_view-width">Sr.No.</th>
                    <th class="cat_view-width2">Name</th>
                    <!-- <th>Type</th> -->
                    <th>Status</th>
                    <th width="200px">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                if (isset($data) && @$data != null) {
                  $count = 1;
                  foreach(@$data as $k => $v) {
                ?>
                <tr class="">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $v['category_name']; ?></td>
                    <!-- <td><?= $v['title']?></td> -->
                    <td align="">
                      <?php  
                        if ( $v['category_is_active'] == 1) {
                      ?>
                        <span class="label label-success">Enabled</span>
                      <?php
                        }else{
                      ?>
                        <span class="label label-danger">Disabled</span>
                      <?php
                        }
                      ?>
                    </td>
                    <td class="">
                      <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['category_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                      <?php 
                        if($categor_edit)
                        {
                          ?>
                          <a href="<?php echo site_url($edit_product.'/'.@$v['category_id'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></a>
                          <?php
                        }
                      ?>
                     <!-- <a href="<?php echo site_url($delete_product.'/'.@$v['category_id'])?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a> -->
                   </td>
                  </tr>
                  <?php
                    $count++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
    <!-- /.modal -->
  </div>
