<style type="text/css">
   .table1-td-h4 {
      color: #333333;
   }

   .table1-td-h4-2 {
      color: #7f7f7f;
   }

   .table1-td-quotHeading {
      text-align: right;
      font-size: 22px;
      font-weight: bold;
   }

   .product-table2-th-details {
      border: 1px solid #adadad;
      background-color: #3c3d3a;
      color: #fff;
   }

   .product-table2-txt-align {
      text-align: center;
   }

   .product-table1-txt-align {
      position: relative;
      right: 0;
   }

   .product-table2-td-details {
      border: 1px solid #adadad;
   }

   .bank-table2-th-details {
      border: 1px solid #adadad;
      background-color: #3c3d3a;
      color: #fff;
      /*font-family: Calibri;
        font-size: 16px;*/
      font-family: Calibri;
      font-size: 12px;
   }

   .bank-table2-td-details {
      background-color: #3c3d3a;
      color: #fff;
      border: 1px solid #adadad;
      /*font-family: Calibri;
        font-size: 16px;*/
      font-family: Calibri;
      font-size: 12px;
   }

   .sub-total-heading {
      font-weight: bold;
      border-left: 1px solid #adadad;
      text-align: right;
   }

   .sub-total-detail {
      border-right: 1px solid #adadad;
      text-align: right;
      padding-right: 7px;
   }

   .less-special-discount-heading {
      font-weight: bold;
      border-left: 1px solid #adadad;
      text-align: right;
   }

   .less-special-discount-detail {
      border-right: 1px solid #adadad;
      text-align: right;
      padding-right: 7px;
   }

   .buyback-discount-heading {
      font-weight: bold;
      border-left: 1px solid #adadad;
      text-align: right;
   }

   .buyback-discount-detail {
      border-right: 1px solid #adadad;
      text-align: right;
      padding-right: 7px;
   }

   .netAmount-heading {
      font-weight: bold;
      text-align: right;
      border-top: 1px solid #adadad;
      border-left: 1px solid #adadad;
   }

   .netAmount-detail {
      font-weight: bold;
      text-align: right;
      border-top: 1px solid #adadad;
      border-right: 1px solid #adadad;
      padding-right: 7px;
   }

   .vat-heading {
      font-weight: bold;
      text-align: right;
      border-left: 1px solid #adadad;
      border-bottom: 1px solid #adadad;
   }

   .vat-details {
      text-align: right;
      border-right: 1px solid #adadad;
      border-bottom: 1px solid #adadad;
      padding-right: 7px;
   }

   .netAmount-inc_VAT-heading {
      font-weight: bold;
      text-align: right;
      border-left: 1px solid #adadad;
      border-bottom: 1px solid #adadad;
   }

   .netAmount-inc_VAT-detail {
      font-weight: bold;
      text-align: right;
      border-right: 1px solid #adadad;
      border-bottom: 1px solid #adadad;
      padding-right: 7px;
   }

   .border-bot {
      border-bottom: 1px solid #adadad;
   }

   .div-controls {
      margin-left: 7%;
      margin-right: 7%;
   }

   .table_align {
      margin-left: 10%;
      margin-right: 10%;
   }

   .font-controls {
      font-family: Calibri;
      font-size: 16px;
   }

   .notes-font-controls {
      font-family: Calibri;
      font-size: 12px;
   }

   .item-dtable-font-controls {
      font-family: Calibri;
      font-size: 16px;
   }

   .payment-font-controls,
   .bankDetails-font-controls {
      font-family: Calibri;
      font-size: 12px;
   }

   .Thanks-font-controls {
      font-family: Calibri;
      font-size: 16px;
   }

   .optional-table-th {
      border: 1px solid #adadad;
      background-color: #c4bb98;
   }

   .optional-table-td {
      border: 1px solid #adadad;
   }

   .tr1-th-first {
      border-top: 1px solid black;
      text-align: right;
      border-left: 1px solid black;
   }

   .tr1-th-sec {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr1-th {
      border-top: 1px solid black;
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr1-th-sec {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr2-th-first {
      text-align: right;
      border-left: 1px solid black;
      border-bottom: none;
   }

   .tr2-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr3-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr3-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr4-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr4-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr5-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr5-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr6-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr6-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr7-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr7-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr8-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr8-th {
      border-right: 1px solid black;
      border-left: 1px solid black
   }

   .tr9-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr9-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr10-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr10-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr11-th-first {
      text-align: right;
      border-left: 1px solid black;
   }

   .tr11-th {
      border-right: 1px solid black;
      border-left: 1px solid black;
   }

   .tr12-th-first {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      text-align: right;
      border-left: 1px solid black;
      background-color: #d9d9d9;
   }

   .tr12-th {
      border-bottom: 1px solid black;
      border-top: 1px solid black;
      border-right: 1px solid black;
      border-left: 1px solid black;
      background-color: #d9d9d9;
   }

   tr:nth-child(3) .tr1-th-first,
   tr:nth-child(3) .tr1-th {
      border-top: none;
   }

   tr:nth-child(5) .tr1-th-first,
   tr:nth-child(5) .tr1-th {
      border-top: none;
   }

   tr:nth-child(7) .tr1-th-first,
   tr:nth-child(7) .tr1-th {
      border-top: none;
   }

   tr:nth-child(9) .tr1-th-first,
   tr:nth-child(9) .tr1-th {
      border-top: none;
   }

   tr:nth-child(11) .tr1-th-first,
   tr:nth-child(11) .tr1-th {
      border-top: none;
   }

   .covr_letter {
      margin-right: 8%;
      margin-left: 8%;
      font-family: calibri;
   }

   .img_margin {
      padding: 10px 10px;
   }

   @page toc {
      size: A4;
   }

   span,
   strong,
   em,
   i,
   p {
      font-family: calibri;
   }
</style>


<table style="border-collapse: collapse;" width="100%" class="table_align">

   <tr>
      <td width="500px" align="left" style="padding-top: 12px;">
         <h4 style="font-family:calibri;font-size: 18px;" class="table1-td-h4"><span>Accounts
               <!-- <?= @$employee_data['department_title'] ?> --> Department
            </span><br><span><?= @$customer_data['user_company_name'] ?></span></h4>
         <?php if ($customer_data['user_country'] == @$setval['setting_company_country']) { ?>
            <h4 class="" style="font-family:calibri;font-size: 18px;text-align: right;font-weight: bold;">T.R.N # <?= @$customer_data['tax_reg']; ?>
            </h4>
         <?php } ?>
         <h5 style="font-size: 18px;" class="table1-td-h4-2 "><span>
               <!-- <?= @$setval['setting_company_address']; ?> --><?= @$customer_data['user_address']; ?>
            </span></h5>
      </td>

      <td align="right" width="500px" style="">
         <span class="table1-td-quotHeading" style="font-family:calibri;">PROFORMA INVOICE</span><br>
         <h4 class="" style="font-family:calibri;font-size: 20px;text-align: right;font-weight: bold;"># <?= @$setval["company_prefix"] . @$setval["quotation_prfx"] ?><?= (@$quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no']; ?>
         </h4>

         <?= ($quotation['quotation_po_no'] != '') ? ' <h4 class="" style="font-size: 20px;text-align: right;font-weight: bold;font-family:calibri;">P.O. # ' . $quotation['quotation_po_no'] . '</h4>' : '' ?>
         <h4 class="" style="font-family:calibri;font-size: 20px;text-align: right;font-weight: bold;">T.R.N # <?= @$setval['setting_company_reg']; ?>
         </h4>
         <span style="font-size: 20px; font-weight:bold;" class="">Date : <?= @$quotation['quotation_date']; ?></span>
      </td>
   </tr>

   <!-- <tr>
                <td  align="left" style="padding-top: -7px;" >
                  
                </td>
                <td colspan="3" width="500px" align="right" style="">
                  
                </td>
              </tr> -->

</table>



<?php if ($inline_discount) { ?>
   <table style="border-collapse: collapse;margin-top: 10px;" class="table_align" width="100%">

      <thead>

         <tr>

            <th align="center" class="product-table2-th-details font-controls" height="30px" width="40px">#</th>

            <th class="product-table2-th-details font-controls" align="left" height="30px" width="500px" style="padding:0 0 0 10px;">Item & Description</th>

            <th class="product-table2-th-details font-controls" height="30px" width="60px">Qty</th>

            <th class="product-table2-th-details font-controls" height="50px" width="140px">Unit Price <br> <?= $quotation_currency_name['title'] ?></th>

            <th class="product-table2-th-details font-controls" height="50px" width="120px">Discount</th>

            <th class="product-table2-th-details font-controls" height="50px" width="120px">Amount <br> <?= $quotation_currency_name['title'] ?></th>

         </tr>

      </thead>

      <tbody>
         <?php
         $i = 1;
         $total_discoount_price = 0;
         foreach ($quotation_detail_data as $dt) {
         ?>
            <tr>

               <td class="product-table2-td-details" style="text-align:center;" height="30px"><?= $i; ?></td>

               <td class="product-table2-td-details font-controls" style="padding:15px 25px;">
                  <span style="font-weight: bold;"><?= @$dt['product_sku'] ?> - <?= @$dt['product_name'] ?></span>
               </td>

               <td align="center" class="product-table2-td-details font-controls" style="vertical-align: top;padding-top: 1%;"><?= $dt['quotation_detail_quantity'] ?>
               </td>

               <td align="center" class="product-table2-td-details font-controls" style="vertical-align: top;padding-top: 1%;padding-right:10px;padding-left:10px;">
                  <?php
                  if (strtolower($quotation_currency_name['title']) != 'usd') {
                     $price = $dt['quotation_detail_rate'];
                  } else {
                     $price = $dt['quotation_detail_rate_usd'];
                  }
				  $price = sprintf("%.4f",$price);
				  $price = substr_replace($price, "", -2);
                  echo number_format((float)$price, 2, '.', '');
				  
                  ?>
               </td>

               <td align="center" class="product-table2-td-details font-controls" style="vertical-align: top;padding-top: 1%;">
                  <?php
                  $discount_amount = 0.00;
                  $discount_percentage = '';
                  $qt = is_numeric($dt['quotation_detail_quantity']) ? $dt['quotation_detail_quantity'] : 1;
                  if ($dt['quotation_detail_total_discount_type'] == 1) {
                     $row_discounted_amount =  (($price*$qt) / 100) * $dt['quotation_detail_total_discount_amount'];
                     $row_discount_amount = quotation_num_format($row_discounted_amount);
                     // $row_discounted_amount =  (($price*$qt) / 100) * $dt['quotation_detail_total_discount_amount'];
                     // $row_discount_amount = quotation_num_format($row_discounted_amount);
                     $discount_amount = $row_discount_amount;
                     $discount_percentage = $dt['quotation_detail_total_discount_amount'];

                  } else if ($dt['quotation_detail_total_discount_type'] == 2) {
                     $discount_amount = $dt['quotation_detail_total_discount_amount'];
                     $discount_percentage = '';
                  }
				  $discount_percentage = sprintf("%.4f",$discount_percentage);
				  $discount_percentage = substr_replace($discount_percentage, "", -2);
				  
                  $total_discoount_price += $discount_amount;
                  ?>

                  <?= $discount_amount; ?>
                  <br> <?= @$discount_percentage; ?>
               </td>

               <td class="product-table2-td-details font-controls" style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 1%;">
                  <?= number_format((float)$dt['quotation_detail_total_amount'], 2, '.', ''); ?>

               </td>

            </tr>

         <?php
            $i++;
         }
         ?>

      </tbody>

   </table>
   
   <?php
      if ($quotation['quotation_report_line_break'] > 0) {
         for ($i = 0; $i < $quotation['quotation_report_line_break']; $i++) {
      ?>
            <br>
      <?php
         }
      }
      ?>

   <table style="border-collapse: collapse;border-top: 1px solid #adadad; " width="100%" class="table_align">
      <tr bgcolor="">

         <!-- <td colspan="3" height="15px" style="font-size: 16px;border-right: none;" align="left" class="netAmount-heading font-controls" ></td> -->
         <td colspan="5" width="900px" bgcolor="" height="20px" style="font-size: 16px;border-right: none;" class="sub-total-heading font-controls">Sub Total (Including <?= @$quotation_currency_name['title'].' '.number_format((float) round($total_discoount_price,2), 2, '.', '') ?>/- Discount)</td>

         <td colspan="6" width="80px" height="20px" style="font-size: 16px;border-left: none;" class="sub-total-detail font-controls">

            <?= number_format((float)$quotation['subtotal'], 2, '.', ''); ?>

         </td>
      </tr>
      <?php if(number_format((float)$quotation['quotation_buypack_discount'], 2, '.', '') > 0){?>
      <tr>

         <td colspan="5" height="20px" style="font-size: 16px;" class="buyback-discount-heading font-controls" width="630px">Buyback Discount</td>

         <td colspan="6" height="20px" style="font-size: 16px;" class="buyback-discount-detail font-controls">
            <?= number_format((float)$quotation['quotation_buypack_discount'], 2, '.', ''); ?>
         </td>

      </tr>
      <?php } ?>
      <?php $bottom_line = ($setval['setting_company_country'] != $customer_data['user_country']) ? " netAmount-inc_VAT-detail " : ""; ?>
     
      <?php if ($setval['setting_company_country'] == $customer_data['user_country']) { ?>
         <tr>
         <?php $nt_amount = $quotation['net_amount'] - (($setval['setting_company_country'] == $customer_data['user_country'])?$quotation['quotation_tax_amount']:0); ?>
            <td colspan="3" height="15px" style="font-size: 16px;border-right:none" align="left" class="netAmount-heading font-controls <?= $bottom_line; ?>" width="315px"></td>
            <td colspan="2" height="15px" style="font-size: 16px;border-right:none;border-left:none;" class="netAmount-heading font-controls <?= $bottom_line; ?>" width="315px">Net Amount</td>
            <td colspan="6" height="15px" style="font-size: 16px;border-left:none;" class="netAmount-detail font-controls <?= $bottom_line; ?>">
               <b>
                  
                  <?= number_format((float) $nt_amount, 2, '.', ''); ?>
               </b>
            </td>

         </tr>
         <tr>

            <td colspan="5" height="15px" style="font-size: 16px;" class="vat-heading font-controls" width="630px"><?= $setval['tax'] ?>% VAT</td>

            <td colspan="6" height="15px" style="font-size: 16px;" class="vat-details font-controls">
               <?php $tax = sprintf("%.2f",($quotation['subtotal'] / 100)) * $quotation['quotation_tax'] ?>
               <?=sprintf("%.2f",$quotation['quotation_tax_amount']); ?>

            </td>

         </tr>

         <tr>

         <td colspan="3" height="15px" style="font-size: 16px;border-right:none;border-bottom:1px solid #adadad;" align="left;" class="netAmount-heading font-controls <?= $bottom_line; ?>" width="315px"><?= numberTowords2(number_format((float) $quotation['net_amount'], 2, '.', ''),$quotation_currency_name['base_name'],$quotation_currency_name['Deci_name']) ?></td>
         <td colspan="2" height="30px" style="font-size: 16px;border-right:none;border-left:none;" class="netAmount-inc_VAT-heading font-controls" width="315px">Net Amount (Incl. VAT)</td>

         <td colspan="6" height="30px" style="font-size: 16px;border-left:0px;" class="netAmount-inc_VAT-detail font-controls"><b>
               <?= number_format((float) $quotation['net_amount'], 2, '.', ''); ?>
            </b>
         </td>

         </tr>
      <?php }else
      {
         ?>
            <tr>
      <?php $nt_amount = $quotation['net_amount'] - (($setval['setting_company_country'] == $customer_data['user_country'])?$quotation['quotation_tax_amount']:0); ?>
         <td colspan="3" height="15px" style="font-size: 16px;border-right:none" align="left" class="netAmount-heading font-controls <?= $bottom_line; ?>" width="315px"><?= numberTowords2(number_format((float) $nt_amount, 2, '.', ''),$quotation_currency_name['base_name'],$quotation_currency_name['Deci_name']) ?></td>
         <td colspan="2" height="15px" style="font-size: 16px;border-right:none;border-left:none;" class="netAmount-heading font-controls <?= $bottom_line; ?>" width="315px">Net Amount</td>
         <td colspan="6" height="15px" style="font-size: 16px;border-left:none;" class="netAmount-detail font-controls <?= $bottom_line; ?>">
            <b>
               
               <?= number_format((float) $nt_amount, 2, '.', ''); ?>
            </b>
         </td>

      </tr>
         <?php
      } ?>
   </table>
<?php } else { ?>
   <table style="border-collapse: collapse;margin-top: 10px;" class="table_align" width="100%">
      <thead>
         <tr>

            <th align="center" class="product-table2-th-details font-controls" height="30px" width="40px">#</th>

            <th class="product-table2-th-details font-controls" align="left" height="30px" width="650px" style="padding:0 0 0 10px;">Item & Description</th>

            <th class="product-table2-th-details font-controls" height="30px" width="60px">Qty</th>

            <th class="product-table2-th-details font-controls" height="50px" width="140px">Unit Price <br> <?= $quotation_currency_name['title'] ?></th>

            <th class="product-table2-th-details font-controls" height="50px" width="120px">Amount <br> <?= $quotation_currency_name['title'] ?></th>

         </tr>

      </thead>

      <tbody>
         <?php
         $i = 1;
         foreach ($quotation_detail_data as $dt) {
         ?>
            <tr>
               <td class="product-table2-td-details font-controls" style="text-align:center;" height="30px"><?= $i; ?></td>

               <td class="product-table2-td-details font-controls" style="padding:15px 25px;">
                  <span style="font-weight: bold;font-size: 16px;font-family: Calibri;"><?= @$dt['product_sku'] ?> - <?= @$dt['product_name'] ?></span>
               </td>

               <td align="center" class="product-table2-td-details font-controls" style="vertical-align: top;padding-top: 1%;"><?= $dt['quotation_detail_quantity'] ?>
               </td>
               <td align="center" class="product-table2-td-details font-controls" style="vertical-align: top;padding-top: 1%;padding-right:10px;padding-left:10px;">
                  <?php
                  if (strtolower($quotation_currency_name['title']) != 'usd') {
                     $price = $dt['quotation_detail_rate'];
                  } else {
                     $price = $dt['quotation_detail_rate_usd'];
                  }
                  echo number_format((float)$price, 2, '.', '');
                  ?>
               </td>
               <td class="product-table2-td-details font-controls" style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 1%;">
                  <?= number_format((float)$dt['quotation_detail_total_amount'], 2, '.', ''); ?>
               </td>

            </tr>
         <?php $i++;
         } ?>


      </tbody>
   </table>
   <p style="margin:0px">
   <?php
   if ($quotation['quotation_report_line_break'] > 0) {
      for ($i = 0; $i < $quotation['quotation_report_line_break']; $i++) {
   ?>
         <br>
   <?php
      }
   }
   ?>
   </p>
   

   <table style="border-collapse: collapse;border-top: 1px solid #adadad;" class="table_align" width="100%">
   <tbody>
   <tr>
         <td  class="sub-total-heading" width="600px"></td>
         <td  style="text-align:right;font-weight:bold" colspan="3"  height="30px" width="310px" class=" font-controls">Sub Total</td>
         <td colspan="1"  height="30px"  width="100px" class="sub-total-detail font-controls"><?= number_format((float)$quotation['subtotal'], 2, '.', ''); ?></td>
      </tr>
      <?php if ($quotation['quotation_total_discount_amount'] != '' &&  $quotation['quotation_total_discount_amount'] > 0) { ?>
         <tr>

            <td colspan="4" height="30px" class="sub-total-heading font-controls">
               <?php
               if (strlen(str_replace(' ', '', $quotation['quotation_discount_notes'])) > 0) {
                  echo $quotation['quotation_discount_notes'];
               } else {
                  echo "<p style='font-size:16px;font-family:Calibri;'>Less Special Discount</p>";
               }
               ?>
            </td>

            <td colspan="1" height="30px" class="sub-total-detail font-controls">
               <?php
               $dicounted_amount = $quotation['quotation_total_discount_amount'];
               if ($quotation['quotation_total_discount_type'] == 1) {
                  $dicounted_amount = ($quotation['subtotal'] / 100) * $quotation['quotation_total_discount_amount'];
				  $dicounted_amount = sprintf("%.4f",$dicounted_amount);
				  $dicounted_amount = substr_replace($dicounted_amount, "", -2);
               }
               ?>
               <p style='font-size:16px;font-family:Calibri;'><?= $dicounted_amount; ?></p>
            </td>

         </tr>

      <?php }
      if ($quotation['quotation_buypack_discount'] != '' &&  $quotation['quotation_buypack_discount'] > 0) { ?>
         <tr>

            <td colspan="4" height="30px" class="buyback-discount-heading font-controls">Buyback Dsicount</td>

            <td colspan="1" height="30px" class="buyback-discount-detail font-controls">
               <p style='font-size:16px;font-family:Calibri;'><?= number_format((float)$quotation['quotation_buypack_discount'], 2, '.', ''); ?></p>
            </td>

         </tr>
      <?php }
      $bottom_line = ($setval['setting_company_country'] != $customer_data['user_country']) ? " netAmount-inc_VAT-detail " : "";

      ?>
      
      <?php if ($setval['setting_company_country'] == $customer_data['user_country']) { ?>

         <tr>
      <?php $nt_amount = ((float) $quotation['subtotal'] - ((float)$quotation['quotation_buypack_discount']) - (float)$dicounted_amount); ?>
         <td colspan="4" height="30px" style="border-right:none;" class="netAmount-heading <?= $bottom_line; ?>  font-controls">Net Amount</td>
         <td colspan="1" height="30px" style="border-left:none;" class="netAmount-detail <?= $bottom_line; ?>  font-controls"><p style='font-size:16px;font-family:Calibri;'><b>
               <?= number_format((float) $nt_amount, 2, '.', ''); ?>
            </b></p>
         </td>

      </tr>

         <tr>

            <td colspan="4" height="30px" style="" class="vat-heading font-controls" ><?= $setval['tax'] ?>% VAT</td>

            <td colspan="1" height="30px" style="" class="vat-details font-controls">
               <?php $tax = ($quotation['subtotal'] / 100) * $quotation['quotation_tax'] ?>
               <?= number_format($quotation['quotation_tax_amount'], 2); ?>
            </td>

         </tr>

         <tr >

            <td colspan="1"  height="30px" style="border-right:none;text-align:left; border-bottom:1px solid #adadad;" class="netAmount-heading font-controls" ><?= numberTowords2(number_format((float) $quotation['net_amount'], 2, '.', ''),$quotation_currency_name['base_name'],$quotation_currency_name['Deci_name']) ?></td>
            <td colspan="3"  height="30px" style="font-size: 16px;font-family:Calibri;border-right:none; border-left:none;" class="netAmount-inc_VAT-heading font-controls" >Net Amount (Incl. VAT)</td>
            <td colspan="1" height="30px" class="netAmount-inc_VAT-detail font-controls"><b>
                  <p style='font-size:16px;font-family:Calibri;'><?= number_format((float) $quotation['net_amount'], 2, '.', ''); ?></p>
               </b></td>
         </tr>
      <?php
      } else {
         $tax = 0;
         ?>
            <tr>
      <?php $nt_amount = ((float) $quotation['subtotal'] - ((float)$quotation['quotation_buypack_discount']) - (float)$dicounted_amount); ?>
         <td colspan="1"  height="30px" style="font-size: 16px;border-right:none;text-align:left" class="netAmount-heading <?= $bottom_line; ?>  font-controls" ><?= numberTowords2(number_format((float) $nt_amount, 2, '.', ''),$quotation_currency_name['base_name'],$quotation_currency_name['Deci_name']) ?></td>
         <td colspan="3" height="30px" style="font-size: 16px;font-family:Calibri;border-right:none; border-left:none;" class="netAmount-heading <?= $bottom_line; ?>  font-controls">Net Amount</td>
         <td colspan="1" height="30px" style="font-size: 16px;font-family:Calibri;border-left:none;" class="netAmount-detail <?= $bottom_line; ?>  font-controls"><b>
               <?= number_format((float) $nt_amount, 2, '.', ''); ?>
            </b></td>

      </tr>
         <?php
      }
      ?>
   </tbody>
      
   </table>

<?php } ?>

<?php
if (strlen(str_replace(' ', '', $quotation['quotation_detail'])) > 0) {
?>
   <div class="div-controls">
      <span class="notes-font-controls"><b>Notes :</b></span><br>
      <span class="notes-font-controls"><?= $quotation['quotation_detail']; ?></span>
   </div>

   <br />
<?php } ?>
<br> 
<div class="div-controls">
   <span class="payment-font-controls"><b>Payments :</b></span><br>
   <span class="payment-font-controls">
      <?php
      foreach ($terms as $term) {
      ?>
         <?= @$term['percentage'] ?>% <?= @$term['payment_title'] ?>
         (<?= (isset($term['payment_days']) && $term['payment_days'] > 0) ? 'After ' . $term['payment_days'] . ' Day' : ((isset($term['payment_days']) && $term['payment_days'] == 0) ? 'Immediate' : '') ?>)<br />
      <?php } ?>
   </span>
</div>

<br />
<?php
if ($quotation['quotation_report_line_break_payment'] > 0) {
   for ($i = 0; $i < $quotation['quotation_report_line_break_payment']; $i++) {
?>
      <br>
<?php
   }
}
?>

<div class="div-controls">
   <span class="bankDetails-font-controls"><b>Bank Details :</b></span>
</div>
<table style="border-collapse: collapse;margin-top: 10px;" class="table_align " width="100%">

   <thead>

      <tr>

         <th align="center" class="bank-table2-th-details font-controls" height="40px" width="250px">Beneficiary</th>

         <th colspan="<?= sizeof($bank) ?>" class="bank-table2-th-details font-controls" height="40px" width="750px" style="padding:0 0 0 10px;"><?= @$setval['setting_company_name']; ?></th>

      </tr>

   </thead>

   <tr>
      <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Bank</td>
      <?php
      foreach ($bank as $bnk1) {
      ?>
         <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk1['bank']; ?>
         </td>
      <?php
      }
      ?>
   </tr>

   <tr>
      <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Account No.</td>
      <?php
      foreach ($bank as $bnk2) {
      ?>
         <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk2['account_no']; ?>
         </td>
      <?php
      }
      ?>
   </tr>

   <tr>
      <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">IBAN</td>

      <?php
      foreach ($bank as $bnk3) {
      ?>
         <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk3['iban']; ?>
         </td>
      <?php
      }
      ?>
   </tr>

   <tr>
      <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Branch</td>

      <?php
      foreach ($bank as $bnk4) {
      ?>
         <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk4['branch']; ?>
         </td>
      <?php
      }
      ?>
   </tr>

   <tr>
      <td class="bank-table2-td-details font-controls" height="30px" style="padding-left: 20px;">Swift Code</td>

      <?php
      foreach ($bank as $bnk5) {
      ?>
         <td width="375px" style="padding-left: 10px;" class="product-table2-td-details font-controls">
            <?= @$bnk5['swift_code']; ?>
         </td>
      <?php
      }
      ?>
   </tr>
   <br />

</table>

<br /><br />

<div class="div-controls">
   <span class="Thanks-font-controls"><b>Thanks,</b></span>
</div>

<br /><br /><br />

<div class="div-controls">
   <span class="Thanks-font-controls"><b>For <?= @$setval['setting_company_name'] ?></b></span>
</div>