
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      <!--<div class="page-title"> <i class="icon-custom-left"></i>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border"> <br>
                  <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                      <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">

                        <div class="form-group">
                          <label class="form-label">Firt Name</label>
                          <span class="help">e.g. "Mona Lisa Portrait"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="text" name="fname" class="form-control" placeholder="Enter Firt Name" value="<?= @$data['fname']; ?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="form-label">Last Name</label>
                          <span class="help">e.g. "Mona Lisa Portrait"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="text" name="lname" class="form-control" placeholder="Enter Last Name" value="<?= @$data['lname']; ?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label for="date_of_birth" class="label-signin-mystyle">
                            D.O.B
                          </label>
                          <span class="help">e.g. "1994-09-24"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="text" class="form-control input-signin-mystyle datepicker" id="date_of_birth" name="date_of_birth" placeholder="Enter D.O.B" value="<?= @$data['date_of_birth']; ?>" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="nationality" class="label-signin-mystyle">
                            Nationality
                          </label>
                          <span class="help">e.g. "Afghanistan"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <select class="form-control input-signin-mystyle select2" id="nationality" name="nationality">
                              <option value="" selected="" disabled="">-- Select Nationality --</option>
                              <?php
                                foreach ($countries as $k => $v) {
                              ?>
                              <option value="<?= $k; ?>" <?= ($k == @$data['nationality'])?'selected':''; ?> ><?= $v; ?></option>
                              <?php
                                }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="form-label">Phone</label>
                          <span class="help">e.g. "+333 222 111"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="Enter Phone" value="<?= @$data['phone']; ?>">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="form-label">About Us</label>
                          <span class="help">e.g. "Mona Lisa Portrait"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <textarea name="about" class="form-control" placeholder="Enter About Us" ><?= @$data['about']; ?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <?php 
                            if(!empty($data['id']) && @$data['id'] != null){
                                $current_img = '';
                                $file_path = dir_path().$image_upload_dir.'/'.@$data['image'];
                              if (!is_dir($file_path) && file_exists($file_path)) {
                                $current_img = site_url($image_upload_dir.'/'.$data['image']);
                              }else{
                                $current_img = site_url("uploads/dummy.jpg");
                              }
                              ?>
                              <div class="clearfix"></div>
                              <label class="form-label">Current Image:</label><br/>
                              <div class="controls">  
                                <img src="<?= $current_img; ?>" width="200"/>
                              </div>
                              <br>
                              <?php
                            }
                          ?>
                          <label class="form-label">Profile Picture</label>
                          <div id="preview"></div>
                          <div class="controls">
                            <input type="file" id="img_src" name="image" class="form-control" accept="image/*">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="form-label">Email</label>
                          <span class="help">e.g. "some@example.com"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="text" name="email" class="form-control" placeholder="Enter E-mail" value="<?= @$data['email']; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="form-label">Password</label>
                          <span class="help">e.g. "Mona Lisa Portrait"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password" value="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="form-label">Re-type Password</label>
                          <span class="help">e.g. "Mona Lisa Portrait"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="password" id="password2" name="re_password" class="form-control" placeholder="Enter Re-type Password" value="">
                          </div>
                        </div>

                        <div class="form-group">
                          <h3><b>Location</b></h3>
                          <div class="amenities_field">
                            <div class="row form-row">
                              <div class="col-md-12">
                                <button type="button" class="btn btn-primary pull-right amenities_field_button">Add More</button>
                              </div>
                            </div>
                            <?php
                            if (isset($user_address) && @$user_address != null) {
                              foreach ($user_address as $k => $v) {
                            ?>
                              <div class="row form-row">
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <label class="loc_name">Location Name</label>
                                        <div class="input-with-icon right controls"> <i class=""></i>
                                            <input type="text" id="loc_name" name="loc_name[]" class="form-control" placeholder="Enter Location Name" value="<?= $v['name']; ?>"> </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <label class="location">Location</label>
                                        <div class="input-with-icon right controls"> <i class=""></i>
                                            <input type="text" id="location" name="location[]" class="form-control" placeholder="Enter Location" value="<?= $v['location']; ?>"> </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-row">
                                        <label class="ad_phone">Phone</label>
                                        <div class="input-with-icon right controls"> <i class=""></i>
                                            <input type="text" id="ad_phone" name="ad_phone[]" class="form-control" placeholder="Enter Phone" value="<?= $v['phone']; ?>"> </div>
                                    </div>
                                </div>
                                <div class=col-md-12><span class=pull-right><a class="btn btn-danger ajaxbtn" rel="delete" href="<?= site_url('all_user/deleteDetails/'.$v['id'].''); ?>" title=remove_field>Remove <i class="fa fa-times ml-10 "></i></a></span></div>
                            </div>
                            <?php
                              }
                            }
                            ?>
                          </div>
                        </div>


                        <div class="form-group">
                          <label for="nationality" class="label-signin-mystyle">
                            User Type
                          </label>
                          <span class="help">e.g. "Agent, Customer"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <select class="form-control input-signin-mystyle select2" id="user_type" name="user_type">
                              <option value="" selected="selected" disabled="">- Select User Type -</option>
                              <option value="2" <?= (@$data['user_type'] == 2)?'selected':''; ?>>Agent</option>
                              <option value="3" <?= (@$data['user_type'] == 3)?'selected':''; ?>>Customer</option>
                            </select>
                          </div>
                        </div>

                        <div class="myUserType" style="display:<?= (@$data['user_type'] == 2)?'block':'none'; ?>">
                          <div class="form-group">
                            <label for="category" class="label-signin-mystyle">
                              Category
                            </label>
                            <div class="input-with-icon right controls">
                              <i class=""></i>
                              <select class="form-control input-signin-mystyle select2" multiple="" id="category" name="category[]">
                                <?php foreach ($categoryData as $k => $v){ 
                                  if (in_array($v['id'], @$categorySelect)){
                                ?>
                                <option value="<?= $v['id']; ?>" selected="selected"><?= $v['name']; ?></option>
                                <?php
                                  }else{
                                ?>
                                <option value="<?= $v['id']; ?>"><?= $v['name']; ?></option>
                                <?php
                                  }
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="form-label">Price</label>
                            <div class="input-with-icon right controls">
                              <i class=""></i>
                              <input type="text" id="price" name="price" class="form-control" placeholder="Enter Price" value="<?= @$data['price']; ?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="nationality" class="label-signin-mystyle">
                            Status
                          </label>
                          <span class="help">e.g. "Enabled, Disabled"</span>
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <select class="form-control input-signin-mystyle select2" id="status" name="status">
                              <option value="" selected="selected" disabled="">- Select Status -</option>
                              <option value="1" <?= (@$data['is_active'] == 1)?'selected':''; ?>>Enabled</option>
                              <option value="0" <?= (@$data['is_active'] == 0)?'selected':''; ?>>Disabled</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary btn-cons pull-right" type="submit">Submit</button>
                            <input type="hidden" name="id" value="<?= @$data['id']; ?>">
                            <input type="hidden" name="email_old" value="<?= @$data['email']; ?>">
                            <input type="hidden" name="phone_old" value="<?= @$data['phone']; ?>">
                            <input type="hidden" name="image_old" value="<?= @$data['image']; ?>">
                            <input type="hidden" name="pass_old" value="<?= @$data['password']; ?>">
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script>
  
  $(document).ready(function() {

    $("#user_type").change(function () {
      var val = $(this).val();
      if (val == 2) {
        $('.myUserType').slideDown();
      }else{
        $('.myUserType').slideUp();
      }
    });

    $(function(){
        $(".select2").select2();
    });
    $('.datepicker').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true
    });

    var max_fields      = 6; 
    var wrapper         = $(".amenities_field");
    var add_button      = $(".amenities_field_button");
    var x = 1; 
    $(add_button).click(function(e){
        e.preventDefault();
        $('form.validate').validate();
        if(x < max_fields){
            x++;
            $('.amenities_field').append('<div class="row form-row"> <div class="col-md-6"> <div class="form-row"> <label class="loc_name">Location Name</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="loc_name" name="loc_name[]" class="form-control" placeholder="Enter Location Name" > </div></div></div><div class="col-md-6"> <div class="form-row"> <label class="location">Location</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="location" name="location[]" class="form-control" placeholder="Enter Location" > </div></div></div><div class="col-md-6"> <div class="form-row"> <label class="ad_phone">Phone</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="ad_phone" name="ad_phone[]" class="form-control" placeholder="Enter Phone" > </div></div></div><div class=col-md-12><span class=pull-right><a class="btn btn-danger remove_field "href=# title=remove_field>Remove <i class="fa fa-times ml-10 "></i></a></span></div></div>');
        }else{
            alert("Extra Max Limit Are 6");
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent().parent().parent().remove(); x--;
    });
  });
</script>

<?php 
if (isset($data['id']) && @$data['id'] != null) {
?>
<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        fname:{
          required: true
        },
        lname:{
          required: true
        },
        email:{
          required: true,
          email: true
        },
        date_of_birth:{
          required: true
        },
        nationality:{
          required: true
        },
        phone:{
          required: true
        },
        price:{
          required: true,
          number:true
        },
        about:{
          required: true
        },
        user_type:{
          required: true
        },
        'category[]':{
          required: true
        },
        'loc_name[]':{
          required: true
        },
        'location[]':{
          required: true
        },
        'ad_phone[]':{
          required: true
        },
        status:{
          required: true
        }
      }, 
      messages: {
        fname: "This field is required.",
        lname: "This field is required.",
        email:{
          required: "This field is required.",
          email: "Invalid E-mail Address"
        },
        date_of_birth: "This field is required.",
        nationality: "This field is required.",
        phone: "This field is required.",
        price:{
          required: "This field is required.",
          number: "Please Insert Number"
        },
        about: "This field is required.",
        user_type: "This field is required.",
        'category[]': "This field is required.",
        'loc_name[]': "This field is required.",
        'location[]': "This field is required.",
        'ad_phone[]': "This field is required.",
        status: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');

          
        }
        // submitHandler: function (form) {

        // }
      });

    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    
  });
</script>
<?php }else{ ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        fname:{
          required: true
        },
        lname:{
          required: true
        },
        email:{
          required: true,
          email: true
        },
        password:{
          required: true
        },
        re_password:{
          required: true,
          equalTo: '#password'
        },
        date_of_birth:{
          required: true
        },
        nationality:{
          required: true
        },
        phone:{
          required: true
        },
        price:{
          required: true,
          number:true
        },
        about:{
          required: true
        },
        'category[]':{
          required: true
        },
        'loc_name[]':{
          required: true
        },
        'location[]':{
          required: true
        },
        'ad_phone[]':{
          required: true
        },
        user_type:{
          required: true
        },
        status:{
          required: true
        }
      }, 
      messages: {
        fname: "This field is required.",
        lname: "This field is required.",
        email:{
          required: "This field is required.",
          email: "Invalid E-mail Address"
        },
        password: "This field is required.",
        re_password:{
          required: "This field is required.",
          equalTo: "Password Not Match To Confirm Password"
        },
        date_of_birth: "This field is required.",
        nationality: "This field is required.",
        phone: "This field is required.",
        price:{
          required: "This field is required.",
          number: "Please Insert Number"
        },
        about: "This field is required.", 
        user_type: "This field is required.",
        'category[]': "This field is required.",
        'loc_name[]': "This field is required.",
        'location[]': "This field is required.",
        'ad_phone[]': "This field is required.",
        status: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');

          
        }
        // submitHandler: function (form) {

        // }
      });
    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
  });
</script>
<?php } ?>