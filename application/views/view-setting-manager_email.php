<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Settings
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':''; ?>
                        <?= (isset($page_heading))?$page_heading:''; ?>
                </a>
            </li>
        </ul>
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            
                                <div class="form-group">
                                        <div class="row" >
                                            <div class="col-md-12">
                                              <div class="" style="width: 100%;">
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <tr>
                                                      <th class="text-center" width="60px">Add/Remove Row</th>
                                                      <th class="text-center" width="200px">Email</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="customFields">
                                                    <tr class="txtMult">
                                                      <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                                      <td colspan="6"></td>
                                                    </tr>

                                                     <?php
                                                     $total_quantity = 0;
                                                     // print_b($material_receive_note_detail_data);
                                                     ?>
                                                    
                                                      <?php
                                                            /*$total_quantity += $v['material_receive_note_detail_quantity']; 
                                                        }*/
                                                      if(@$emails != null && !empty(@$emails)){
                                                      $k = 1;
                                                      foreach(@$emails as $row){
                                                        
                                                     ?>

                                                     <tr class="txtMult">
                                                            <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                            <td>
                                                                <input type="text" class="email" required style="width:100%" id="email_old<?= $k ?>"  name="email[]" 
                                                                value="<?= @$row['email'] ?>" placeholder="Email" />
                                                            </td>
                                                        </tr>
                                                    <?php $k++; } } ?>

                                                  </tbody>
                                                </table>
                                              </div>  
                                            </div>
                                    </div>
                                </div>
                            

                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <!--    <button class="btn btn-success btn-cons pull-right" type="submit"><?php // ($page_title == 'add')?'Add New':'Edit'; ?> Product</button> -->
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['setting_id']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>

    $(document).ready(function() {
        jQuery.validator.addMethod("emailValidation", function( value, element ) {
            var regex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
            var key = value;

            if (!regex.test(key)) {
               return false;
            }
            return true;
        }, "Please enter a valid email address.");

        $("form.validate").validate({
            rules: {
                "email[]": {
                    required: true,
                    emailValidation : true,
                }
            },
            messages: {
                
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit
                error("Please input all the mandatory values marked as red");
    
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    $(document).ready(function(){
        var max_fields      = 6; 
        var add_button      = $("#customFields .addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
            x++;
            var temp = '<tr class="txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="email" required  style="width:100%" id="email'+x+'" data-optional="0" name="email[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#customFields").append(temp);
        });
        $("#customFields").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });
      
    });

</script>
