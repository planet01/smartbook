  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Invoice
        </li>
        <li>
          <a href="#" class="active">View All</a>
        </li>
      </ul>
      <?php
      $check_visible = false;
      $check_add = false;
      $check_edit = false;
      $check_print = false;
      $check_status = false;
      $check_payment = false;
      $check_history = false;
      if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
          if ($v['module_id'] == 12) {
            if ($v['visible'] == 1) {
              $check_visible = true;
            }
            if ($v['add'] == 1) {
              $check_add = true;
            }
            if ($v['edit'] == 1) {
              $check_edit = true;
            }
            if ($v['print'] == 1) {
              $check_print = true;
            }
            if ($v['status'] == 1) {
              $check_status = true;
            }
            if ($v['payment'] == 1) {
              $check_payment = true;
            }
            if ($v['history'] == 1) {
              $check_history = true;
            }
          }
        }
      } else {
        $check_add = true;
        $check_edit = true;
        $check_print = true;
        $check_status = true;
        $check_payment = true;
        $check_history = true;
        $check_visible = true;
      }
      ?>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
      <!--</div>-->
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span> <span class="fa fa-sort-desc"></spam>
              </h4>
              <?php if ($check_add) { ?>
                <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('invoice/add'); ?>">Add</a>
              <?php } ?>
              <!--<div class="pull-right">-->
              <!--  <a href="<?php echo site_url($add_product) ?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>-->
              <!--  <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>-->
              <!--  <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>-->
              <!--  <ul class="dropdown-menu">-->
              <!--    <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>-->
              <!--    <li class="divider"></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>-->
              <!--    <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>-->
              <!--  </ul>-->
              <!--  <span class="h-seperate"></span>-->
              <!--  <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>-->
              <!--</div>-->
            </div>
            <div class="grid-body inv-dt-pad">
              <div class="row">
                <form class="validate">
                  <div class="col-md-4 col-lg-4">
                    <div class="row">
                      <div class="col-md-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label for="checkbox1">From Date</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-8 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="row">
                      <div class="col-md-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label for="checkbox1">To Date</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-8 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2">
                  <div class="row">
                    <div class="col-md-5 col-lg-5">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Invoice No</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-7 col-lg-7">
                      <i class=""></i>
                      <input type="text" class="form-control" name="invoice_no" id="invoice_no"> 
                    </div>
                  </div>
                </div>

                  <div class="col-md-4 col-lg-4">
                  <div class="row">
                    <div class="col-md-4 col-lg-4">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Customer</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-lg-8">
                      <i class=""></i>
                      <select name="customer_id" id="customer_id" class="select2 form-control">
                        <option value="0" selected disabled >--- Select Customer ---</option>
                        <?php
                        foreach($customerData as $k => $v){
                        ?>
                        <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['customer_id'])? 'selected': ''; ?> >
                        <?= $v['user_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div>

                <div class="col-md-4 col-lg-4">
                  <div class="row">
                    <div class="col-md-4 col-lg-4">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>End User</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-lg-8">
                      <i class=""></i>
                      <select name="end_user" id="end_user" class="select2 w-100">
                        <option value="0" selected >--- Select End User ---</option>
                      </select>  
                    </div>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2">
                  <div class="row">
                    <div class="col-md-5 col-lg-5">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>City</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-7 col-lg-7">
                      <i class=""></i>
                      <select name="city_name" id="city_name" class="select2 form-control">
                        <option value="0" selected >--- Select City ---</option>
                        <?php
                        foreach($cityData as $k => $v){
                        ?>
                        <option value="<?= $v['city_name']; ?>" >
                        <?= $v['city_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div>

                  <div class="col-md-2 col-lg-2">
                    <div class="row">
                      <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                          <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                </form>
              </div>
              <table class="table dataTables_wrapper inv_dtable-disp" id="custom_table">
                <thead>
                  <tr>
                    <th>Sr.No.</th>
                    <th class="text-center" width='150px'>Invoice Date / <br>Expire Date</th>
                    <th class="text-center" width='200px'>Invoice # / <br> P.I #.</th>
                    <th class="text-center" width='200px'>Customer Name <br> End User / City </th>
                    <th class="text-center" width='400px'>Total Value <br> Current Status</th>
                    <th class="inv-view-dt-th8 text-center">Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach (@$data as $v) {
                      $rev = ($v['invoice_revised_no'] > 0) ? '-R' . $v['invoice_revised_no'] : '';
                      $rev1 = ($v['invoice_status'] == 2) ? '-' . $v['invoice_revised_no'] : '';
                  ?>
                      <tr>
                        <td class="text-center"><?php echo $count; ?></td>
                        <td class="text-center"><?php echo date("Y-m-d", strtotime($v['invoice_date'])) ?> <br><?= date("Y-m-d", strtotime($v['invoice_expire_date'])) ?></td>
                        <td class="text-center"><?php echo @$setting["company_prefix"] . @$setting["invoice_prfx"]; ?><?php echo $v['invoice_no'] . $rev ?> <br> <?= ($v['invoice_proforma_no'] != 0) ? @$setting["company_prefix"] . @$setting["quotation_prfx"] . $v['invoice_proforma_no'] . find_rev_no($v['invoice_proforma_no']) : '' ?></td>
                        <td class="text-center">
                          <?php echo $v['user_name'] ?> <br>
                          <?= ($v['end_user_name'] != '') ? $v['end_user_name'] . ' / ' . $v['city_name'] : '' ?>
                        </td>
                        <td class="text-center"><?php echo quotation_num_format($v['invoice_net_amount']) ?><br>
                          <input type="hidden" class="quotation_stat" value="<?= $v['invoice_status'] ?>" />
                          <span class="label label-success">
                            <?php
                            echo InvoiceStatusData()[$v['invoice_status']] . $rev1 ?>
                          </span>
                        </td>

                        <td class="">
                          <!--  <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['invoice_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a> -->
                          <?php if ($check_print || $check_visible) { ?>
                            <a href="<?= site_url($print . '?id=' . @$v['invoice_id'] . '&header_check=0&view=0') ?>" target='_blank' title="View Detail" data-id="<?= @$v['invoice_id']; ?>" class="btn-primary btn btn-sm"><i class="fa fa-eye"></i></a>
                          <?php } ?>
                          <?php if (@$v['invoice_status'] != 4) { ?>
                            <?php
                            if (@$v['has_payment'] != 1) {
                              if ($check_edit) {
                            ?>
                                <a href="<?php echo site_url($edit_product . '/' . @$v['invoice_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                            <?php
                              }
                            } ?>
                            <?php if ($check_status) { ?>
                              <div class="btn-group mt-2">
                                <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                  <!-- <li><a class="status_comment" data-id="<?= $v['quotation_id']; ?>" status-id="4" href="javascript:void(0)">Final</a></li>
                              <li class="divider"></li> -->
                                  <li><a class="status_comment" data-id="<?= $v['invoice_id']; ?>" status-id="4" href="javascript:void(0)">Payment Received</a></li>
                                  <li class="divider"></li>
                                </ul>
                              </div>
                            <?php } ?>
                          <?php } ?>
                          <?php if ($check_print || $check_visible) { ?>
                            <div class="btn-group mt-2">
                              <button class="btn btn-white btn-demo-space"> <i class="fa fa-print"></i></button>
                              <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                              <ul class="dropdown-menu">
                                <li><a class="page_reload" href="<?= site_url($print . '?id=' . @$v['invoice_id'] . '&header_check=0') ?>" target='_blank'>With Header</a></li>
                                <li class="divider"></li>
                                <li><a class="page_reload" href="<?= site_url($print . '?id=' . @$v['invoice_id'] . '&header_check=1') ?>" target='_blank'>Without Header</a></li>
                              </ul>
                            </div>
                          <?php } ?>
                          <a href="#" class="btn-warning btn btn-sm send_mail" data-id="<?= @$v['invoice_id']; ?>" data-toggle="tooltip" title="Mail"><i class="fa fa-envelope"></i></a>
                          <?php if ($check_history) { ?>
                            <a href="#" class="btn-primary btn btn-sm find_history history_modal" data-id="<?= @$v['invoice_id']; ?>" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>
                            <a href="#" class="btn-primary btn btn-sm find_follow_up" data-id="<?= @$v['invoice_id']; ?>" data-toggle="tooltip" title="Follow Up"><i class="fa fa-arrow-up"></i></a>
                          <?php } ?>

                          <?php if ($check_payment) { ?>
                            <a href="javascript:void(0)" class="btn-primary btn btn-sm find_payment" data-id="<?= @$v['invoice_id']; ?>" id="<?= @$v['invoice_id']; ?>"><i class="fa fa-credit-card"></i></a>
                          <?php } ?>
                          <!-- <a href="<?php echo site_url($delete_product . '/' . @$v['invoice_id']) ?>" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm"><i class="fa fa-times"></i></a> -->
                        </td>
                      </tr>

                  <?php
                      $count++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      var print_url = "<?= site_url($print) ?>?id=";
      var invoice_status_data = '<?php echo json_encode(InvoiceStatusData()); ?>';
      $(".page_reload").click(function(event) {
        //window.location.reload(true);
        var quotation_final = $(this).closest('tr')
          .children('td.status_desc').find('.quotation_stat').val();
        if (parseInt(quotation_final) < 4) {
          $(this).closest('tr')
            .children('td.status_desc').find('span')
            .text('Final');
        }

      });
      $(".find_history").click(function(event) {
        var values = "id=" + $(this).data('id');
        $.ajax({
          url: "<?= site_url($history) ?>",
          type: "get",
          data: values,
          success: function(data) {
            if (data) {
              data = $.parseJSON(data);
              var html = "";
              for (var i = 0; i < data.length; i++) {
                var count = i;
                data[i]['s_no'] = count + 1;
                html += history_template(data[i]);
                //console.log(data[i]['quotation_no']);
              }
              $('#history_body').html(html);
              $('#history_modal').modal('show');
            };
          },
          error: function() {
            alert('There is error while submit');
          }
        });
      });
      var invoice_prefix = '<?php echo @$setting["company_prefix"] . @$setting["invoice_prfx"]; ?>';

      function history_template(data) {
        var dt = new Date(data['invoice_date']);
        var subtotal = (isNaN(data['invoice_net_amount']) || data['invoice_net_amount'] == null) ? 0 : data['invoice_net_amount'];
        var inv_no = (data['invoice_revised_no'] > 0) ? data['invoice_no'] + '-R' + data['invoice_revised_no'] : data['invoice_no'] + '-R0';
        var status = window.invoice_status_data;
        status = JSON.parse(status);
        var current_status = (data['invoice_status'] == 2) ? status[data['invoice_status']] + '-R' + data['invoice_revised_no'] : status[data['invoice_status']];
        var html = '<tr>';
        html += '<td class="history-dt-td1">' + data['s_no'] + '</td>';
        html += '<td class="history-dt-td2">';
        html += '<a target="_blank" href="' + window.print_url + data['invoice_id'] + '&header_check=0&view=0&history=1">';
        html += window.invoice_prefix + inv_no;
        html += '</a>';
        html += '</td>';
        html += '<td class="history-dt-td3">';
        // html += ("0" + dt.getDate()).slice(-2) + "-" + ("0"+(dt.getMonth()+1)).slice(-2) + "-" + dt.getFullYear();
        html += data['create_date'];
        html += '</td>';
        html += '<td class="history-dt-td4">';
        html += current_status;
        html += '</td>';
        html += '<td class="history-dt-td5">';
        if (data['quotation_status'] == 4) {
          html += data['invoice_status_comment'];
        }
        html += '</td>';
        html += '<td class="history-dt-td6 text-right">' + parseFloat(subtotal).toFixed(2) + '</td>';
        html += '</tr>';
        return html;
      }

      $(".find_follow_up").click(function(event) {
        var values = "id=" + $(this).data('id');
        $.ajax({
          url: "<?= site_url('invoice/follow_up') ?>",
          type: "get",
          data: values,
          success: function(data) {
            if (data) {
              data = $.parseJSON(data);
              var html = "";
              for (var i = 0; i < data.length; i++) {
                var count = i;
                data[i]['s_no'] = count + 1;
                html += follow_up_template(data[i]);
                //console.log(data[i]['quotation_no']);
              }
              $('#follow_up_body').html(html);
              $('#follow_up_modal').modal('show');
            };
          },
          error: function() {
            alert('There is error while submit');
          }
        });
      });

      function follow_up_template(data) {

        var html = '<tr>';
        html += '<td class="history-dt-td1">' + data['s_no'] + '</td>';
        html += '<td class="history-dt-td2" style="min-width:120px">';
        html += data['follow_up_date'];
        html += '</td>';
        html += '<td class="history-dt-td3" >';
        html += data['user_name'];
        html += '</td>';
        html += '<td class="history-dt-td4">';
        html += data['follow_up_contact_person'];
        html += '</td>';
        html += '<td class="history-dt-td5" style="min-width:150px">';
        html += data['follow_up_contact_no'];
        html += '</td>';
        html += '<td class="history-dt-td6">';
        html += data['follow_up_remarks'];
        html += '</td>';
        html += '</tr>';
        return html;
      }
      //payment
      $(".find_payment").click(function(event) {
        var id = $(this).attr("id");
        var total_amount = 0;
        var total_received = 0;
        var overdue_since = 0;
        var overdue_amount = 0;
        var is_invoice = 1;

        $.ajax({
          url: "<?= site_url('receivables/invoice_payment') ?>",
          type: "POST",
          data: {
            'id': id,
            'is_invoice': is_invoice
          },
          success: function(data) {
            if (data) {
              var html = "";
              data = $.parseJSON(data);
              var row = data['payment'];
              console.log(data['data']);
              for (var i = 0; i < row.length; i++) {
                var count = i;
                row[i]['s_no'] = count + 1;
                row[i]['total_amount'] = data['data'].total_amount_base;
                row[i]['total_received'] = data['data'].paid_amount_base;
                row[i]['overdue_since'] = data['data'].expiry_date_by_pyment_term;
                row[i]['overdue_amount'] = data['data'].expiry_amount_by_pyment_term;
                html += payment_template(row[i]);
              }
              $('#payment_body').html(html);
              $('#payment_modal').modal('show');

            };
          },
          error: function() {
            alert('There is error while submit');
          }
        });
      });
      var invoice_prefix1 = '<?php echo @$setting["company_prefix"] . @$setting["invoice_prfx"]; ?>';
      // var print_url         = '<?php echo @$setting["company_prefix"] . @$setting["invoice_prfx"]; ?>';
      function payment_template(data) {
        //var dt = new Date(data.payment_date);
        var dt = data.payment_date;
        var payment_amount = (isNaN(data.payment_amount) || data.payment_amount == null) ? 0.00 : parseFloat(data.payment_amount).toFixed(2);
        var payment_adjustment = (isNaN(data.payment_adjustment) || data.payment_adjustment == null) ? 0.00 : parseFloat(data.payment_adjustment).toFixed(2);
        /*var qot_no   = (data['quotation_revised_no'] > 0)? data['quotation_no']+'-R'+data['quotation_revised_no'] : data['quotation_no']+'-R0';*/

        var html = '<tr>';
        html += '<td class="receivable-dt-td1">' + data.s_no + '</td>';
        html += '<td class="receivable-dt-td2">' + data.receipt_no + '</td>';
        html += '<td class="receivable-dt-td3">' + dt + '</td>';
        html += '<td class="receivable-dt-td4">';
        html += window.invoice_prefix1 + data.invoice_no;
        html += '</td>';
        html += '<td class="receivable-dt-td5">' + payment_amount + '</td>';
        html += '<td class="receivable-dt-td5">' + payment_adjustment + '</td>';
        html += '<td class="receivable-dt-td6">';
        html += '<a href="<?php echo site_url($edit_payment . '/') ?>' + data.payment_id + '?total_amount=' + data.total_amount + '&total_received=' + data.total_received + '&overdue_since=' + data.overdue_since + '&overdue_amount=' + data.overdue_amount + '&is_invoice=' + data.is_invoice + '&from_invoice=1" class="mb-2 mr-2 disableClick btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
        html += '<a class="page_reload btn btn-white mb-2 mr-2" href="<?= site_url(@$print_payment) ?>?id=' + data.payment_id + '&header_check=0" target="_blank"><i class="fa fa-print"></i></a>';
        html += '<a href="<?php echo site_url(@$delete_payment . '/') ?>' + data.payment_id + '" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm mb-2"><i class="fa fa-times"></i></a>';
        html += '</td>';

        html += '</tr>';
        return html;
      }

      $(document).ready(function() {

        $('#customer_id').change(function () {
          var customer_id = $("#customer_id").find(':selected').val();
          $.ajax({
              url: "<?php echo site_url('Ticket_management/get_customer_end_users'); ?>",
              dataType: "json",
              type: "GET",
              data: {
                customer_id: customer_id,
              },
              cache: false,
              success: function(invoiceData) {
                var html = '';
                html += '<option value="0" selected >--- Select End User ---</option>';
                if(invoiceData.data != undefined){
                  for (var i = 0; i < invoiceData.data.length; i++) {
                    html += '<option value="'+invoiceData.data[i].end_user_id+'" >'+invoiceData.data[i].end_user_name+'</option>';
                  }
                }
                
                $('#end_user').html(html).trigger('change');
              }
            });
        });

      var custom_table = $('#custom_table').DataTable();
      $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
      $(document).on("click", "#fetch", function() {

        var from_date = $('#from_date').val();
        if (from_date == '') {
          from_date = 0;
        }

        var to_date = $('#to_date').val();
        if (to_date == '') {
          to_date = 0;
        }
        var end_user = $('#end_user option:selected').val();
        var customer_id = $('#customer_id option:selected').val();
        var invoice_no = $('#invoice_no').val(); 
        var city_name = $('#city_name option:selected').val();

        $.ajax({
          url: "<?php echo site_url('invoice/search_view'); ?>",
          dataType: "json",
          type: "POST",
          data: {
            from_date: from_date,
            to_date: to_date,
            end_user:end_user,
            customer_id:customer_id,
            invoice_no:invoice_no,
            city_name:city_name
          },
          cache: false,
          success: function(result) {

            custom_table.clear().draw();
            var count = 0;
            for (var i = 0; i < result.data.length; i++) {
              count++;
              var html = '';
              html += '<tr>';

              html += '<td class="text-center">';
              html += count;
              html += '</td>';

              html += '<td class="text-center">';
              html += result.data[i].invoice_date+'<br>'+result.data[i].invoice_expire_date;
              html += '</td>';

              $rev = '';
              if(result.data[i].invoice_revised_no > 0)  
              {
                $rev = '-R' + result.data[i].invoice_revised_no;
              } 

              $rev1 = '';
              if(result.data[i].invoice_status == 2)  
              {
                $rev1 = '-' + result.data[i].invoice_revised_no;
              } 

              html += '<td class="text-center">';
              html += "<?= @$setting["company_prefix"] . @$setting["invoice_prfx"] ?>" + result.data[i].invoice_no + $rev +'<br>';
              if(result.data[i].invoice_proforma_no != 0)
              {
                html += "<?= @$setting["company_prefix"] . @$setting["quotation_prfx"] ?>" + result.data[i].invoice_proforma_no + result.data[i].perfoma_rev_no;
              }
              else
              {
                html += "";
              }
              html += '</td>';

              html += '<td class="text-center">';
              html += result.data[i].user_name+'<br>';
              if(result.data[i].end_user_name != null)
              {
                html += result.data[i].end_user_name+' / '+ result.data[i].city_name;
              }
              html += '</td>';

              html += '<td class="text-center">'+parseFloat(result.data[i].invoice_net_amount).toFixed(2)+'<br>';
              html += '<input type="hidden" class="quotation_stat" value="'+result.data[i].invoice_status+'" />';
              html += '<span class="label label-success">';
              if(result.data[i].invoice_status == 0)
              {
                html += 'Expire';
              }else if(result.data[i].invoice_status == 1)
              {
                html += 'Final';
              }else if(result.data[i].invoice_status == 2)
              {
                html += 'Revision';
              }else if(result.data[i].invoice_status == 3)
              {
                html += 'Draft';
              }else if(result.data[i].invoice_status == 4)
              {
                html += 'Payment Received';
              }
              html += $rev1;
              html += '</span>';
              html += '</td>';


              html += '<td>';

              html += '<?php if ($check_print || $check_visible) { ?>';
                html += '<a href="<?php echo site_url($print . "?id=") ?>' + result.data[i].invoice_id + '&header_check=0&view=0" target="_blank" title="View Detail" data-id="'+ result.data[i].invoice_id + '" class="mr-1 btn-primary btn btn-sm"><i class="fa fa-eye"></i></a>';
              html += '<?php } ?>';
              if (result.data[i].invoice_status != 4) { 
                
                if (result.data[i].has_payment != 1) {
                
                  html += '<?php  if ($check_edit) { ?>';
                    html += '<a href="<?php echo site_url($edit_product . "/") ?>' + result.data[i].invoice_id + '" class="btn-warning btn btn-sm mr-1" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                  html += '<?php } ?>';
                }
                html += '<?php if ($check_status) { ?>';
                  html += '<div class="btn-group mt-2 mr-1">';
                  html += '<button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>';
                  html += '<button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>';
                    html += '<ul class="dropdown-menu">';
                    html += '<li><a class="status_comment" data-id="'+ result.data[i].invoice_id + '" status-id="4" href="javascript:void(0)">Payment Received</a></li>';
                    html += '<li class="divider"></li>';
                    html += '</ul>';
                    html += '</div>';
                    html += '<?php } ?>';
              }


              html += '<?php if ($check_print || $check_visible) { ?>';
                html += '<div class="btn-group mt-2 mr-1">';
                html += '<button class="btn btn-white btn-demo-space"> <i class="fa fa-print"></i></button>';
                html += '<button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>';
                html += '<ul class="dropdown-menu">';
                html += '    <li><a class="page_reload" href="<?php echo site_url($print . "?id=") ?>' + result.data[i].invoice_id + '&header_check=0" target="_blank">With Header</a></li>';
                html += '    <li class="divider"></li>';
                html += '    <li><a class="page_reload" href="<?php echo site_url($print . "?id=") ?>' + result.data[i].invoice_id + '&header_check=1" target="_blank">Without Header</a></li>';
                html += '</ul>';
                html += '</div>';
                html += '<?php } ?>';

              html += '<a href="#" class="btn-warning btn btn-sm send_mail mr-1" data-id="'+ result.data[i].invoice_id +'" data-toggle="tooltip" title="Mail"><i class="fa fa-envelope"></i></a>';
              html += '<?php if ($check_history) { ?>';
                html += '<a href="#" class="btn-primary btn btn-sm mr-1 find_history history_modal" data-id="'+ result.data[i].invoice_id +'" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>';
                html += '<a href="#" class="btn-primary btn btn-sm mr-1 find_follow_up" data-id="'+ result.data[i].invoice_id +'" data-toggle="tooltip" title="Follow Up"><i class="fa fa-arrow-up"></i></a>';
              html += '<?php } ?>';

              html += '<?php if ($check_payment) { ?>';
                html += '<a href="javascript:void(0)" class="btn-primary mr-1 btn btn-sm find_payment" data-id="'+ result.data[i].invoice_id +'" id="'+ result.data[i].invoice_id +'"><i class="fa fa-credit-card"></i></a>';
                html += '<?php } ?>';
              html += '</td>';
              html += '</tr>';
              custom_table.row.add($(html)).draw(false);
            }
            // $('#custom_row').append(html);
          }
        });

      });
    });
    </script>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <?php include APPPATH . 'views/include/send_mail_modal.php'; ?>
    <?php include APPPATH . 'views/include/status_comment_modal.php'; ?>
    <?php include APPPATH . 'views/include/receivable_payments_modal.php'; ?>
    <?php include APPPATH . 'views/include/follow_up_modal.php'; ?>
    <?php
    $module_prefix = "Invoice #";
    include APPPATH . 'views/include/history_modal.php'; ?>

    <!-- /.modal -->
  </div>