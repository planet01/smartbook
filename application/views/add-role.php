<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Role
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?>
                    <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
                </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product) ?>" method="post" enctype="multipart/form-data">
                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Role Name:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="role_name" type="text" value="<?= @$data['role_name']; ?>" class="form-control" placeholder="Enter Role Name">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Role Description:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="role_description" type="text" value="<?= @$data['role_description']; ?>" class="form-control" placeholder="Enter Role Description">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <h5 class="form-label">Visible Dashboard For:</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-with-icon right controls">
                                        <i class=""></i>
                                        <?php
                                        $count = 0;
                                        foreach (@$departmentData as $v) {
                                            $count++;
                                            $checked = false;
                                            if($edit == true)
                                            {
                                                foreach (@$dashboard_detail as $v1) 
                                                {
                                                    if($v['department_id'] == $v1['department_id'])
                                                    {
                                                        $checked = true;
                                                    }
                                                }
                                            }
                                        ?>


                                            <div class="checkbox checkbox-inline check-success">
                                                <input type="checkbox" id="department<?= $count ?>" name="department[]" <?=( @$checked== true )? 'checked': ''; ?> value="<?= $v['department_id']?>" style="position: relative;top: 2px;">
                                                <label for="department<?= $count ?>"><?= $v['department_title'] ?></label>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12 mt-5">
                                    <div class="form-group">
                                                <div class="checkbox checkbox-inline check-success">
                                                <input type="checkbox" id="checkAll" style="position: relative;top: 2px;">
                                                <label for="checkAll"><b>Check/Uncheck All</b></label>
                                        </div>
                                    </div>
                                </div>
                                
                                    <div class="col-12">
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                            <div class="my-table-controls dataTables_wrapper-1440 dataTables_wrapper-1441" style="width: 100%;">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Features</th>
                                                            <th colspan="9" class="text-center">Functions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $count = 0;
                                                    foreach (@$moduleData as $v) {
                                                        
                                                        $checked_visible = false;
                                                        $checked_add = false;
                                                        $checked_edit = false;
                                                        $checked_delete = false;
                                                        $checked_print = false;
                                                        $checked_status = false;
                                                        $checked_payment = false;
                                                        $checked_history = false;
                                                        $checked_clone = false;

                                                        if($edit == true)
                                                        {
                                                            foreach (@$detail as $v1) 
                                                            {
                                                                if($v['module_id'] == $v1['module_id'] && $data['role_id'] == $v1['role_id'])
                                                                {
                                                                    if($v1['visible'] == 1)
                                                                    {
                                                                        $checked_visible = true;
                                                                    }
                                                                    if($v1['add'] == 1)
                                                                    {
                                                                        $checked_add = true;
                                                                    }
                                                                    if($v1['edit'] == 1)
                                                                    {
                                                                        $checked_edit = true;
                                                                    }
                                                                    if($v1['delete'] == 1)
                                                                    {
                                                                        $checked_delete = true;
                                                                    }
                                                                    if($v1['print'] == 1)
                                                                    {
                                                                        $checked_print = true;
                                                                    }
                                                                    if($v1['status'] == 1)
                                                                    {
                                                                        $checked_status = true;
                                                                    }
                                                                    if($v1['payment'] == 1)
                                                                    {
                                                                        $checked_payment = true;
                                                                    }
                                                                    if($v1['history'] == 1)
                                                                    {
                                                                        $checked_history = true;
                                                                    }
                                                                    if($v1['clone'] == 1)
                                                                    {
                                                                        $checked_clone = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                        <tr>
                                                            <td>
                                                                <?= $v['module_name']?>
                                                                <input type="hidden" name="module[]" value="<?= $v['module_id']?>">
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_visible'] == 1){?>
                                                                <div class="input-with-icon right controls">
                                                                    <div class="checkbox checkbox-inline check-success">
                                                                        <input type="hidden" name="visible[<?=$count;?>]" value="0" />
                                                                        <input type="checkbox" class="checkAllBox" id="visible<?= $count ?>" <?=( @$checked_visible== true )? 'checked': ''; ?> name="visible[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                        <label style="padding-left: 25px;" for="visible<?= $count ?>">Visible</label>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_add'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="add[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="add<?= $count ?>" <?=( @$checked_add == true )? 'checked': ''; ?> name="add[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="add<?= $count ?>">Add</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_edit'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="edit[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="edit<?= $count ?>" <?=( @$checked_edit == true )? 'checked': ''; ?> name="edit[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="edit<?= $count ?>">Edit</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_delete'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="delete[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="delete<?= $count ?>" <?=( @$checked_delete == true )? 'checked': ''; ?> name="delete[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="delete<?= $count ?>">Delete</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_print'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="print[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="print<?= $count ?>" <?=( @$checked_print == true )? 'checked': ''; ?> name="print[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="print<?= $count ?>">Print</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_status'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="status[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="status<?= $count ?>" <?=( @$checked_status == true )? 'checked': ''; ?> name="status[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="status<?= $count ?>">Status</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_payment'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="payment[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="payment<?= $count ?>" <?=( @$checked_payment == true )? 'checked': ''; ?> name="payment[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="payment<?= $count ?>">Payment</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_history'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="history[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="history<?= $count ?>" <?=( @$checked_history == true )? 'checked': ''; ?> name="history[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="history<?= $count ?>">History</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                            <?php if($v['module_clone'] == 1){?>
                                                                <div class="checkbox checkbox-inline check-success">
                                                                    <input type="hidden" name="clone[<?=$count;?>]" value="0" />
                                                                    <input type="checkbox" class="checkAllBox" id="clone<?= $count ?>" <?=( @$checked_clone == true )? 'checked': ''; ?> name="clone[<?=$count;?>]" value="1" style="position: relative;top: 2px;">
                                                                    <label style="padding-left: 25px;" for="clone<?= $count ?>">Clone</label>
                                                                </div>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    $count++;
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                
                                <div class="col-md-12 mt-4">
                                    <div class="form-group text-center">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['role_id']; ?>">
                                        <input name="role_name_old" type="hidden" value="<?= @$data['role_name']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {

    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $("form.validate").validate({

            rules: {
                role_name: {
                    required: true
                },
                
            },
            messages: {
                role_name: "This field is required.",
                
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit   
                error("Please input all the mandatory values marked as red");

            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('error-control').addClass('success-control');

            }
            // submitHandler: function (form) {
            // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
        $(document).on('change','#checkAll',function(){
            if ($(this).prop("checked")) {
                $('.checkAllBox').prop('checked',true);
            }else
            {
                $('.checkAllBox').prop('checked',false);
            }
            
        })
    });
</script>