<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Customer
            </li>
            <li><a href="#" class="active">View All
                    <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
                </a> </li>
        </ul>
        <?php
        $check_add = false;
        $check_edit = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 8) {
                    if ($v['add'] == 1) {
                        $check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $check_edit = true;
                    }
                }
            }
        } else {
            $check_add = true;
            $check_edit = true;
        }
        ?>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
        <!--</div>-->
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span>
                        </h4>
                        <?php if ($check_add) { ?>
                        <a style="float: right;" class="btn btn-success btn-cons"
                            href="<?= site_url('customer/add'); ?>">Add</a>
                        <?php } ?>
                        <!--<div class="pull-right">-->
                        <!--  <a href="<?php echo site_url($add_product) ?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>-->
                        <!--  <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>-->
                        <!--  <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>-->
                        <!--  <ul class="dropdown-menu">-->
                        <!--    <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>-->
                        <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>-->
                        <!--    <li class="divider"></li>-->
                        <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>-->
                        <!--    <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>-->
                        <!--  </ul>-->
                        <!--  <span class="h-seperate"></span>-->
                        <!--  <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>-->
                        <!--</div>-->
                    </div>
                    <div class="grid-body ">
                        <div class="row">
                            <form class="validate">
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-5 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>Customer Name</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 date_input">
                                            <i class=""></i>
                                            <input class="form-control" type="text" name="customer_name"
                                                id="customer_name" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-5 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>Customer Type</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 date_input">
                                            <i class=""></i>
                                            <select class="form-control select2" id="type"
                                                name="type">
                                                <option value="0" selected="">-- Customer Type --</option>
                                                <?php
                                                        foreach ($customer_type as $k => $v) {
                                                        ?>
                                                <option value="<?= $v['id'] ?>"
                                                    <?=( $v['id']== @$data['customer_type']) ? 'selected' : ''; ?>>
                                                    <?= $v['title']; ?>
                                                </option>
                                                <?php
                                                        }
                                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input class="btn btn-success btn-cons" id="fetch" type="button"
                                                    value="Fetch">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-5 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>Country</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 date_input">
                                            <i class=""></i>
                                            <select class="form-control select2" id="country" name="country">
                                                <option value="0" selected="" >-- Select Country --</option>
                                                <?php
                                                        foreach ($countries as $k => $v) {
                                                        ?>
                                                <option value="<?= $v['country_id'] ?>" >
                                                    <?= $v['country_name']; ?>
                                                </option>
                                                <?php
                                                        }
                                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-5 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>City</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 date_input">
                                            <i class=""></i>
                                            <select class="form-control select2" id="city" name="city">
                                            <option selected value="0">--- Select City ---</option>
                                            <?php foreach($city as $k => $v){ ?>
                                                <option  value="<?= $v['city_name']; ?>"> <?= $v['city_name']; ?></option>';
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                

                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <table class="table" id="custom_table">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <!-- <th>Name</th> -->
                                    <th>Company Name</th>
                                    <!--  <th>Display Name</th> -->
                                    <th>Customer Type</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th>Status</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach (@$data as $k => $v) {
                                ?>
                                <tr class="view-cust_dt">
                                    <td>
                                        <?php echo $count; ?>
                                    </td>
                                    <!-- <td>
                                            <?php echo $v['user_name'] ?>
                                        </td> -->
                                    <td>
                                        <?php echo $v['user_company_name'] ?>
                                    </td>
                                    <!-- <td>
                                            <?php echo $v['user_display_name'] ?>
                                        </td> -->
                                    <td>
                                        <?php echo ($v['title'] != Null) ? $v['title'] : 'NA'; ?>
                                    </td>
                                    <td>
                                        <?php echo $v['user_city'] ?>
                                    </td>
                                    <td>
                                        <?php echo $v['country_name'] ?>
                                    </td>
                                    <td>
                                        <?php
                                                if ($v['user_is_active'] == 1) {
                                                ?>
                                        <span class="label label-success">Enabled</span>
                                        <?php
                                                } else {
                                                ?>
                                        <span class="label label-danger">Disabled</span>
                                        <?php
                                                }
                                                ?>
                                    </td>
                                    <td class="">
                                        <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip"
                                            title="View Detail" data-id="<?= @$v['user_id']; ?>"
                                            data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                                        <?php if ($check_edit) { ?>
                                        <a href="<?php echo site_url($edit_product . '/' . @$v['user_id']) ?>"
                                            class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i
                                                class="fa fa-pencil"></i></a>
                                        <?php } ?>

                                        <!--       <a href="<?php // echo site_url($delete_product.'/'.@$v['user_id'])
                                                                    ?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a> -->
                                    </td>
                                </tr>
                                <?php
                                        $count++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
</div>
<script>
$(document).ready(function() {
    var custom_table = $('#custom_table').DataTable();
    $(document).on("click", "#fetch", function() {

        var customer_name = $('#customer_name').val();
        if (customer_name == '') {
            customer_name = 0;
        }

        var type = $('#type option:selected').val();
        var country = $('#country option:selected').val();
        var city = $('#city option:selected').val();
        

        $.ajax({
            url: "<?php echo site_url('Customer/search_view'); ?>",
            dataType: "json",
            type: "POST",
            data: {
                customer_name: customer_name,
                type: type,
                city: city,
                country: country,
            },
            cache: false,
            success: function(result) {

                custom_table.clear().draw();
                var count = 0;
                for (var i = 0; i < result.data.length; i++) {
                    count++;
                    var html = '';
                    html += '<tr>';

                    html += '<td >';
                    html += count;
                    html += '</td>';

                    html += '<td >';
                    html += result.data[i].user_company_name;
                    html += '</td>';

                    

                    html += '<td >';
                    if (result.data[i].title != null) {
                        html += result.data[i].title;
                    }else{
                        html += 'NA';
                    }
                    html += '</td>';

                    html += '<td >';
                    html += result.data[i].user_city;
                    html += '</td>';

                    html += '<td>';
                    html += result.data[i].country_name;
                    html += '</td>';


                    html += '<td >';
                    
                    if (result.data[i].user_is_active == 1) {
                        html += '<span class="label label-success">';
                        html += 'Enabled';
                    }else{
                        html += '<span class="label label-danger">';
                        html += 'Disabled';
                    }
                    html += '</span>';
                    html += '</td>';


                    html += '<td>';

                                        
                    html += '<a href="#" class="btn-primary btn btn-sm myModalBtn mr-1" data-toggle="tooltip" title="View Detail" data-id="' + result.data[i].user_id +'" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>';
                    html += '<?php  if ($check_edit) { ?>';
                    html += '<a href="<?php echo site_url($edit_product . "/") ?>' +
                        result.data[i].user_id +
                        '" class="btn-warning btn btn-sm mr-1" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                    html += '<?php } ?>';

                    html += '</td>';
                    html += '</tr>';
                    custom_table.row.add($(html)).draw(false);
                }
                // $('#custom_row').append(html);
            }
        });

    });
});
</script>