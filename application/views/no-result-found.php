<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                <a href="#" class="active">
                    No Results Found
                </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>No Results Found</h4>
                    </div>
                    <div class="grid-body no-border">
                        <h2 style="font-weight: 600;">No Results Found.</h2>
                        <p style="font-weight: 500;">The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
