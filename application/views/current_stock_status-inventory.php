  <style>
    .dot {
  height: 5px;
  width: 5px;
  background-color: #d81f27;
  border-radius: 50%;
  display: inline-block;
}
</style>
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Inventory
        </li>
        <li><a href="#" class="active">Stock Transaction View
          <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
        </a> </li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4> <span class="semi-bold">Stock Transaction View
                <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
              </span></h4>
            </div>
            <div class="grid-body ">


              <!-- FIRST ROW -->
              <div class="row">
               <div class="col-md-12">
                <div class="col-md-4">
                  <label><span class="dot"></span> Product Name : </label>
                  <select class="form-control input-signin-mystyle select2 ddlProduct responsive_scr_navhead-wdth1" id=""  name="product_id" style="width:100% !important;">
                    <option value="" selected="" disabled="">Search By Product</option>

                    <?php
                    $getProduct = getCustomRows('SELECT * FROM product WHERE product_is_active = 1 AND category_type  = 2 ORDER BY product_name ASC');
                    foreach($getProduct as $k => $v){
                     ?>
                     <option value="<?= $v['product_id']; ?>" <?= ( $v['product_id'] == @$product_id)?'selected':''; ?>>
                      <?= $v['product_sku']; ?> - <?= $v['product_name']; ?>
                    </option>
                    <?php
                  }
                  ?>
                </select>

              </div>
              <div class="col-md-4" >
                <label><span class="dot"></span> Location Address : </label>
                <input type="text" class="address" name="address" value="<?= ($address == "" || $address == "-1")? "" : $address; ?>" placeholder="Enter address here">
              </div>
              <div class="col-md-4">
                <label style="color: white !important;">Empty</label>
                <button style="width:200px; color: #ffffff !important; background-color: #0090d9; padding: 7px 18px;" class="btn btn-success  btnFilter" type="button"> Filter Data</button>
              </div>
            </div>
          </div>
          <br>

          <!-- SECOND ROW -->
          <div class="row">
           <div class="col-md-12">
            <div class="col-md-4">
              <label>From Date : </label>
              <div class=" right controls" id="span-pre">
               <i class=""></i>
               <input type="text" class="datepicker fromDate" name="delivery_date" value="<?= ($from_date == "2000-01-01")? "yyyy-mm-dd" : $from_date; ?>" placeholder="From Date">
             </div>
           </div>
           <div class="col-md-4">
            <label>To Date : </label>
            <div class=" right controls" id="span-pre">
             <i class=""></i>
             <input type="text" class="datepicker toDate" name="delivery_date" value="<?= ($to_date == "2999-01-31")? "yyyy-mm-dd" : $to_date; ?>" placeholder="To Date">
           </div>
         </div>
         <div class="col-md-4">
           <label style="color: white !important;">Empty</label>
          <button style="width:200px; color: #ffffff !important; background-color: green; padding: 7px 18px;" class="btn btn-success btnExportToExcel " type="button"> Export To Excel</button>
        </div>
      </div>
    </div>


    <br>
    <br>

    <table class="table" id="example3">
      <thead>
        <tr>
          <th>Sr.No.</th>
          <th>Date</th>
          <th class="prod-control">Doc No</th>
          <th>Location</th>
          <th>In</th>
          <th>Out</th>
          <th>Balance</th>
          <th width="200px">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><?= $previous_balance; ?></td>
          <td></td>
        </tr>
        <?php
        $balance = $previous_balance;
        $count = 1;
        foreach (@$data as $k => $v) {
          ?>
          <tr>
            <td><?= $count?></td>
            <td><?= date("Y-m-d", strtotime($v['doc_create_date'])) ?></td>
            <td>
              <?php
                $setval = getCustomRow('Select * From setting where setting_id = 1');
                 
                if($v['receipt_id'] != NULL)
                {
                  $data = getCustomRow('SELECT *  FROM inventory_receive WHERE id  = "'.$v['receipt_id'].'" ');
                  echo $setval["company_prefix"].@$setval["material_prfx"].$data['receipt_no'];
                }
                else if ($v['dn_id'] != NULL)
                {
                  $data = getCustomRow('SELECT *  FROM delivery_note WHERE delivery_note_id  = "'.$v['dn_id'].'" ');
                  echo $setval["company_prefix"] . @$setval["delivery_prfx"] .$data['delivery_note_no'];
                }
                else if ($v['min_id'] != NULL)
                {
                  $data = getCustomRow('SELECT *  FROM material_issue_note WHERE material_issue_note_id  = "'.$v['min_id'].'" ');
                  echo $setval["company_prefix"] . @$setval["material_issue_prefix"] .$data['material_issue_note_no'];
                }

              ?>
            </td>
            <td><?= $v['warehouse_name']?></td>
            <td><?= ($v['inventory_type'] == 1) ? $v['inventory_quantity'] : '-' ?></td>
            <td><?= ($v['inventory_type'] == 1) ? '-' : $v['inventory_quantity'] ?></td>
            <td>
              <?php

							//Jd commented, for more detail view in previous backup file.
              //Jd$query = "SELECT 
              //( SELECT COALESCE(SUM(`inv`.`inventory_quantity`),0) FROM inventory inv WHERE `inv`.`inventory_id` <= '" . $v['inventory_id'] . "' AND `inv`.`inventory_type` = 1 AND `inv`.`product_id` = i.product_id AND `inv`.`warehouse_id` = '" . $v['warehouse_id'] . "'    ) 
              //- 
              //( SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_id` <= '" . $v['inventory_id'] . "' AND `inventory_type` = 0 AND `warehouse_id` = '" . $v['warehouse_id'] . "' AND `product_id` = i.product_id  ) 
              //as total_inventory_quantity 
              //FROM product i WHERE i.product_id = '" . $v['product_id'] . "'  ";
              //$result = getCustomRow($query);
              //Jd echo $result['total_inventory_quantity'];


              if($v['inventory_type'] == 1)
              {
                $balance = $balance + $v['inventory_quantity'];
              }
              else{
                $balance = $balance - $v['inventory_quantity'];
              }
              echo $balance;
              ?>
            </td>
            <td>
              <a href="#" class="btn-primary btn btn-sm myModalBtn" data-id="<?= @$v['inventory_id']; ?>" data-toggle="tooltip" title="View Detail" data-path="inventory/current_stock_detail"><i class="fa fa-eye"></i></a>
            </td>
          </tr>
          <?php
          $count++;
        }
        ?>
                  <!-- <?php
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach ($warehouse as $warehouse) {
                      foreach ($product as $products) {
                        foreach (@$data as $v) {
                          if ($warehouse['warehouse_id'] == $v['warehouse_id'] && $products['product_id'] == $v['product_id']) {
                            if ($v['total_inventory_quantity'] > 0) {
                  ?>
                              <tr>
                                <td><?php echo $count; ?></td>
                                <td><?php echo $products['product_sku'] . '-' . $v['product_name'] ?></td>
                                <td><?php echo $v['total_inventory_quantity'] ?></td>
                                <td><?php echo $v['warehouse_name'] ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="">
                                  <a href="#" class="btn-primary btn btn-sm myModalBtn" data-id="<?= @$v['inventory_id']; ?>" data-toggle="tooltip" title="View Detail" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                                </td>
                              </tr>
                  <?php
                              $count++;
                              break;
                            }
                          }
                        }
                      }
                    }
                  }
                  ?> -->
                </tbody>
                <tfoot>
                  <!--  <tr>
                   <th></th>
                   <th>Total Quantity</th>
                   <th class="Int"></th>
                 </tr> -->
               </tfoot>
             </table>
           </div>
         </div>
       </div>
     </div>
   </div>
   <!-- Modal -->
   <?php include APPPATH . 'views/include/modal.php'; ?>
   <!-- /.modal -->
 </div>
 <script>
  var table = $("#example5").DataTable({
    "initComplete": function(settings, json) {
      var api = this.api();
      CalculateTableSummary(this);
    },
    "footerCallback": function(row, data, start, end, display) {
      console.log('aaya');
      var api = this.api(),
      data;
      CalculateTableSummary(this);
      return;
    }
  });



  function CalculateTableSummary(table) {
    try {

      var intVal = function(i) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '') * 1 :
        typeof i === 'number' ?
        i : 0;
      };

      var api = table.api();
      api.columns(".sum").eq(0).each(function(index) {
        var column = api.column(index, {
          page: 'current'
        });

        var sum = column
        .data()
        .reduce(function(a, b) {
              //return parseInt(a, 10) + parseInt(b, 10);
              return intVal(a) + intVal(b);
            }, 0);


          // console.log(sum);

          $(".Int").html(sum.toFixed(0));


        });
    } catch (e) {
      console.log('Error in CalculateTableSummary');
      console.log(e)
    }
  }

  $(document).ready(function() {


   $(".datepicker").datepicker({
     format: "yyyy-mm-dd",
     autoclose: true
   });

   $(".btnFilter, .btnExportToExcel").click(function(){
        
       
        var productId = $('.ddlProduct').find(":selected").val();
        var address = $('.address').val();
        var fromDate = $(".fromDate").val();
        var toDate = $(".toDate").val();
        if(typeof productId == "undefined" || productId == "" || productId == null || productId.length < 1)
        {

          $('.ddlProduct .select2-choice').css("border","1px solid red");
          return;
        }
        
        if(typeof address == "undefined" || address == "" || address == null || address.length < 1)
        {

          $('.address').css("border","1px solid red");
          return;
        }

        if(fromDate == "" || fromDate =="yyyy-mm-dd")
          fromDate = "2000-01-01";

        if(toDate == "" || toDate =="yyyy-mm-dd")
          toDate = "2999-01-31";
        var clickedClass = $(this).attr("class");
        if(clickedClass.includes("btnExportToExcel"))
        {
           var url = "<?= site_url('inventory/export_to_excel_stock_transaction/'); ?>"+ productId+"/"+ address + "/" + fromDate + "/"+toDate;
        }
        else{
           var url = "<?= site_url('inventory/filter_current_stock_status/'); ?>"+ productId+"/"+ address + "/" + fromDate + "/"+toDate;
        }
       
        window.open(url,"_self");
  });

 });

</script>