
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Category
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border"> 
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">
                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Name</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="category_name"  type="text" value="<?= @$data['category_name']; ?>"  class="form-control" placeholder="Enter Name">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <!-- <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Type</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="category_type" name="category_type">
                                      <option value="" selected="selected" disabled="">- Select Type -</option>
                                      <?php
                                       foreach($categoryTypes as $type){
                                        $selected = (isset($data['category_type']) &&  $data['category_type'] == $type['id'])? 'selected="selected"':'';
                                        ?> 
                                          <option <?= $selected ?> value="<?= $type['id'] ?>"> <?= $type['title'] ?></option>
                                      <?php
                                        }
                                      ?>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div> -->

                      <!-- <div class="clearfix"></div> -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Status</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="category_is_active" name="category_is_active">
                                      <option value="" selected="selected" disabled="">- Select Status -</option>
                                      <option value="1" <?= ($page_title == 'add')?'selected':''?> <?=( @$data['category_is_active']==1 )? 'selected': ''; ?> >Enabled</option>
                                      <option value="0" <?=( @$data['category_is_active']==0 && $page_title != 'add')? 'selected': ''; ?>>Disabled
                                      </option>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="col-md-12">
                        <div class="form-group text-center">
                          <!--  <button class="btn btn-success btn-cons pull-right" type="submit"><?= ($page_title == 'add')?'Add New':'Edit'; ?> Category</button>-->
                          </br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"><?= ($page_title == 'add')?'':''; ?> Submit</button>
                          <input name="id"  type="hidden" value="<?= @$data['category_id']; ?>">
                          <input name="category_name_old"  type="hidden" value="<?= @$data['category_name']; ?>">
                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        category_name:{
          required: true
        },
        category_is_active:{
          required: true
        },
        category_type:{
          required: true
        }
      }, 
      messages: {
        category_name: "This field is required.",
        category_is_active: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
    // $('.select2', "form.validate").change(function () {
    //     $('fosrm.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    // });
  });
</script>