
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Ticket Management
        </li>
        <li><a href="#" class="active">
          <?php
            if($page_title == 'add')
            {
              echo 'Add New';
            }else if($page_title == 'Edit')
            {
              echo 'Edit';
            }else if($page_title == 'add_invoice_ticket')
            {
              echo 'Invoice Ticket';
            }
          ?>
       </a> </li>
      </ul>
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border"> 
                  <h4>
                  <?php
                    if($page_title == 'add')
                    {
                      echo 'Add New';
                    }else if($page_title == 'Edit')
                    {
                      echo 'Edit';
                    }else if($page_title == 'add_invoice_ticket')
                    {
                      echo 'Invoice';
                    }
                  ?>
                  <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Date</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="ticket_date"  type="text" value="<?= @$data['ticket_date']; ?>"  class="form-control datepicker" placeholder="Select Date">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>


                     <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Type</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <div class="radio radio-success responsve-receivable-radio">
                                    <input id="checkbox1" <?= ($page_title == 'add_invoice_ticket')?'onclick="return false;"':'' ?>
                                    <?php
                                      if($page_title == 'add')
                                      {
                                        echo 'checked="true"';
                                      }else if($page_title == 'Edit')
                                      {
                                        if(@$data['type'] == 0)
                                        {
                                          echo 'checked="true"';
                                        }
                                      }else if($page_title == 'add_invoice_ticket')
                                      {
                                        
                                      }
                                    ?>
                                    type="radio" name="type" value="0">
                                    <label class="" for="checkbox1"><b>Project</b></label>
                                        
                                    <input id="checkbox2" <?= ($page_title == 'add_invoice_ticket')?'onclick="return false;"':'' ?>
                                    <?php
                                      if($page_title == 'add')
                                      {
                                        echo '';
                                      }else if($page_title == 'Edit')
                                      {
                                        if(@$data['type'] != 0)
                                        {
                                          echo 'checked="true"';
                                        }
                                      }else if($page_title == 'add_invoice_ticket')
                                      {
                                        echo 'checked="true"';
                                      }
                                    ?>
                                    type="radio" name="type" value="1">
                                    <label class="" for="checkbox2"><b>Others</b></label>      
                                </div> 
                                </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div id="project_div">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row ">
                                <div class="col-sm-12 col-md-12">
                                    <label class="form-label">Project</label>
                                </div>
                                <div class="col-sm-12 col-md-9">
                                    <div class="input-with-icon right controls">
                                        <i class=""></i>
                                        <select name="project_id" id="project_id" class="custom_select" style="width:100% !important">
                                            <option value="0" selected >--- Select Project ---</option>
                                            <?php
                                            foreach($project_data as $k => $v){
                                            ?>
                                                <option value="<?= $v['project_id']; ?>" <?=( $v['project_id'] == @$data['doc_id'])? 'selected': ''; ?> >
                                                <?= $v['project_name']; ?>
                                                </option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    
                                </div>
                                </div>
                            </div>
                        </div>
                        
                      </div>

                      <div id="others_div">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row ">
                                <div class="col-sm-12 col-md-12">
                                    <label class="form-label">Invoice Type</label>
                                </div>
                                <div class="col-sm-12 col-md-9">
                                <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <div class="radio radio-success responsve-receivable-radio">
                                        <input id="checkbox3" <?= ($page_title == 'add_invoice_ticket')?'onclick="return false;"':'' ?>
                                        <?php
                                          if($page_title == 'add')
                                          {
                                            echo 'checked="true"';
                                          }else if($page_title == 'Edit')
                                          {
                                            if(@$data['type'] == 1)
                                            {
                                              echo 'checked="true"';
                                            }
                                          }else if($page_title == 'add_invoice_ticket')
                                          {
                                            echo 'checked="true"';
                                          }
                                        ?>
                                        type="radio" name="invoice_type" value="0">
                                        <label class="" for="checkbox3"><b>Invoice</b></label>
                                            
                                        <input id="checkbox4" <?= ($page_title == 'add_invoice_ticket')?'onclick="return false;"':'' ?>
                                        <?php
                                          if($page_title == 'add')
                                          {
                                            echo 'checked="true"';
                                          }else if($page_title == 'Edit')
                                          {
                                            if(@$data['type'] == 2)
                                            {
                                              echo 'checked="true"';
                                            }
                                          }else if($page_title == 'add_invoice_ticket')
                                          {
                                            echo '';
                                          }
                                        ?>
                                        type="radio" name="invoice_type" value="1">
                                        <label class="" for="checkbox4"><b>Perfoma Invoice</b></label>      
                                    </div> 
                                    </div>
                                    
                                </div>
                                </div>
                            </div>
                        </div>

                      </div>

                      <div class="col-md-6">
                            <div class="form-group">
                                <div class="row ">
                                <div class="col-sm-12 col-md-12">
                                    <label class="form-label">Reference No</label>
                                </div>
                                <div class="col-sm-12 col-md-9">
                                <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select name="invoice_id" id="invoice_id" class="custom_select" style="width:100% !important">
                                        <option value="0" selected>--Select P.I/Invoice--</option>
                                        <?php
                                          if($page_title == 'Edit')
                                          {
                                            if(@$data['type'] == 0){
                                            ?>
                                              <option value='<?= @$data['doc_id'];?>' selected><?= @$data['invoice_no'];?></option>
                                            <?php
                                            }
                                            else{
                                              if(@$data['type'] == 1) //Invoice
                                              {
                                                foreach($invoice  as $k => $v)
                                                {
                                                  $rev = ($v['revised_no'] > 0) ? '-R'. $v['revised_no'] : '';
                                                  ?>
                                                  <option value='<?= @$v['ID'];?>' <?=( $v['ID'] == @$data['doc_id'])? 'selected': ''; ?> data-expiry = '<?= @$v['inv_expiry_date']?>'><?= $setting['company_prefix'].$setting['invoice_prfx'].@$v['invoice_no'].$rev;?></option>
                                                  <?php
                                                }
                                              }else if(@$data['type'] == 2) //PI
                                              {
                                                foreach($proforma_invoice  as $k => $v)
                                                {
                                                  $rev = ($v['revised_no'] > 0) ? '-R'. $v['revised_no'] : '';
                                                  ?>
                                                  <option value='<?= @$v['ID'];?>' <?=( $v['ID'] == @$data['doc_id'])? 'selected': ''; ?> data-expiry = '<?= @$v['inv_expiry_date']?>'><?= $setting['company_prefix'].$setting['quotation_prfx'].@$v['invoice_no'].$rev;?></option>
                                                  <?php
                                                }
                                              }
                                            }
                                          }else if($page_title == 'add_invoice_ticket')
                                          {
                                            foreach($invoice  as $k => $v)
                                                {
                                                  if($ticket_invoice_id == $v['ID']){
                                                  $rev = ($v['revised_no'] > 0) ? '-R'. $v['revised_no'] : '';
                                                  ?>
                                                  <option value='<?= @$v['ID'];?>' data-expiry = '<?= @$v['inv_expiry_date']?>'  ><?= $setting['company_prefix'].$setting['invoice_prfx'].@$v['invoice_no'].@$rev;?></option>
                                                  <?php
                                                  }
                                                }
                                          }
                                        ?>
                                    </select> 
                                    <input type="hidden" name="invoice_no" id="invoice_no" value="<?= @$data['invoice_no'];?>">
                                    <input type="hidden" name="invoice_expiry_date" id="invoice_expiry_date" value="<?= @$data['invoice_expiry_date'];?>">
                                    </div>
                                    
                                </div>
                                </div>
                            </div>
                        </div>

                      <div class="clearfix"></div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Customer</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input type="text" disabled name="customer_name" id="customer_name" value='<?= @$data['user_name']?>'>
                                    <input type="hidden" name="customer_id" id="customer_id" value="<?= @$data['customer_id'];?>">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>
                      

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">End User</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input type="text" disabled name="end_user" id="end_user" value='<?= @$data['end_user_name']?>'>
                                    <input type="hidden" name="end_user_id" id="end_user_id" value='<?= @$data['end_user']?>'>
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Assigned To</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select name="assigned_to" id="assigned_to" class="select2 form-control">
                                        <option disabled selected >--- Select Employee ---</option>
                                        <?php
                                        foreach($employeeData as $k => $v){
                                        ?>
                                            <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['assigned_to'])? 'selected': ''; ?> >
                                            <?= $v['user_name']; ?>
                                            </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Ticket Type</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select name="ticket_type" id="ticket_type" class="select2 form-control">
                                        <option value="0" disabled selected >--- Select Ticket Type ---</option>
                                        <?php
                                        foreach($ticket_type as $k => $v){
                                        ?>
                                            <option value="<?= $v['ticket_type_id']; ?>" <?=( $v['ticket_type_id'] == @$data['ticket_type'])? 'selected': ''; ?> >
                                            <?= $v['ticket_type_name']; ?>
                                            </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width:60px;"></th>
                            <th>Task Description</th>
                            <th>No Of Hours</th>
                            <th>Task Type</th>
                            <th>From Time</th>
                            <th>To Time</th>
                        </tr>
                        </thead>
                        <tr class="txtMult">
                            <td class="text-center"><a href="javascript:void(0);" class="addCF"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                            <td colspan="3"></td>
                        </tr>
                        <tbody id="custom_row">
                          <?php
                            if (isset($ticket_task) && @$ticket_task != null) {
                              foreach ($ticket_task as $k => $v) {
                                ?>
                                  <tr class="txtMult">
                        
                                    <td>
                                        <a href="javascript:void(0);" class="remCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>
                                    </td>

                                    <td>
                                        <input type="text" required name="task_description[]" value="<?= $v['task_description']?>">
                                    </td>

                                    <td>
                                        <input type="text" required class="txtboxToFilter" name="no_of_hours[]" value="<?= $v['no_of_hours']?>">
                                    </td>

                                  <td>
                                      <select name="type_of_task[]" required>
                                      <option selected disabled> --- Select Type ---</option>
                                        <?php
                                        
                                          foreach ($projectCateogryData as $k2 => $v2) {
                                          ?>
                                            <option <?= ($v['type_of_task'] == $v2['pc_id'])?'selected':''?> value="<?= $v2['pc_id']?>"><?= $v2['pc_name']?></option>    
                                          <?php
                                        } 
                                        ?>
                                        </select>
                                    </td>

                                    <td>
                                        <input type="time" required class="form-control" name="from_time[]" value="<?= $v['from_time']?>">
                                    </td>

                                    <td>
                                        <input type="time" required class="form-control" name="to_time[]" value="<?= $v['to_time']?>">
                                    </td>
                                </tr>
                                <?php
                              }
                            }
                          ?>
                               
                        </tbody>
                    </table>

                      <div class="clearfix"></div>

                      <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-label">Notes</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-with-icon right controls">
                                            <i class=""></i>
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <textarea name="notes" id="myeditor" type="textarea" class="form-control ckeditor" placeholder=""><?=@$data['notes']; ?></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                      
                    <div class="clearfix"></div>
                      <div class="col-md-12">
                        <div class="form-group text-center">
                          </br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"> Submit</button>
                          <input name="id"  type="hidden" value="<?= @$data['ticket_id']; ?>">
                          <input name="t_no"  type="hidden" value="<?= @$data['ticket_no']; ?>">
                          <input name="ticket_for_invoice"  type="hidden" value="<?= ($page_title == 'add_invoice_ticket')?'1':'0' ?>">
                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script type="text/javascript">
  $(document).ready(function() {
    $('.custom_select').select2({
            minimumInputLength: 4
    });
    var page = '<?php echo $page_title; ?>';
    
    
    if (page != "add") {
      var type = $('[name="type"]:checked').val();
      
      if(type == 0) //Project
      {
          $('#project_div').show();
          $('#others_div').hide();
      }
      else
      {
          $('#project_div').hide();
          $('#others_div').show();

          
      }        
    }else{
      $('#project_div').show();
      $('#others_div').hide();
    }


    

    $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
    });
    $("form.validate").validate({
      rules: {
        ticket_date:{
          required: true
        },
        assigned_to:{
          required: true
        },
        ticket_type:{
          required :true
        },
        "task_description[]": {
            required: true
        },
        "no_of_hours": {
            required: true
        },
        "ticket_type": {
            required: true
        },
        "from_time": {
            required: true
        },
        "to_time": {
            required: true
        },
      }, 
      messages: {
        ticket_date: "This field is required.",
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });

        

        
        window.setting = "<?= addslashes(json_encode($setting)); ?>";
       
          find_invoices();
            

        function find_invoices() {
            // $.ajax({
            // url: "<?php echo site_url('Ticket_management/find_invoices'); ?>",
            // dataType: "json",
            // type: "GET",
            // cache: false,
            // success: function(data) {
                window.invoices = JSON.parse('<?php echo json_encode($invoice); ?>');
                window.proforma_invoices = JSON.parse('<?php echo json_encode($proforma_invoice); ?>');
                window.project_data = JSON.parse('<?php echo json_encode($project_data); ?>');
                window.customerData = JSON.parse('<?php echo json_encode($customerData); ?>');
                window.endUserData = JSON.parse('<?php echo json_encode($endUserData); ?>');
                window.project_task = JSON.parse('<?php echo json_encode($project_task); ?>');
                window.projectCateogryData = JSON.parse('<?php echo json_encode($projectCateogryData); ?>');
            // }
            // });
        }

        $('[name="type"]').change(function (e) {
            
            $('#end_user').val('');
            $('#end_user_id').val('');
            $('#customer_name').val('');
            $('#customer_id').val('');
            var html = "<option value='0'>--Select P.I/Invoice--</option>";
            $('#invoice_id').html(html).trigger('change');
            $('#project_id').val(0).trigger('change');

            var type = $('[name="type"]:checked').val();
            if(type == 0) //Project
            {
                $('#project_div').show();
                $('#others_div').hide();
            }
            else
            {
                $('[name="invoice_type"]').trigger('change');
                $('#project_div').hide();
                $('#others_div').show();
            }
            
        });

        

        $('[name="invoice_type"]').change(function () {
            
            $('#end_user').val('');
            $('#end_user_id').val('');
            $('#customer_name').val('');
            $('#customer_id').val('');
            var html = "<option value='0'>--Select P.I/Invoice--</option>";
            $('#invoice_id').html(html).trigger('change');
            $('#project_id').val(0).trigger('change');

            var invoice_type = $('[name="invoice_type"]:checked').val();
        
            data = (invoice_type == 1) ? window.proforma_invoices : window.invoices;
            html = "";
            var this_setting = JSON.parse(window.setting);
            var prefix = this_setting['company_prefix'];
            prefix = (invoice_type == 1) ? prefix + this_setting['quotation_prfx'] : prefix + this_setting['invoice_prfx'];
            html += "<option value='0'>--Select P.I/Invoice--</option>"
            for (var i = 0; i < data.length; i++) {
                var title = (data[i].revised_no > 0) ? data[i].invoice_no + '-R' + data[i].revised_no : data[i].invoice_no;
                if (data[i].invoice_found !== undefined && data[i].invoice_found > 0) {} else {
                    html += "<option value='" + data[i].ID + "' data-expiry = '"+data[i].inv_expiry_date+"'>" + prefix + title + "</option>"
                }
            }
            $('#invoice_id').html(html).trigger('change');
            
        });

        $('#project_id').change(function () {
            var data = window.project_data;
            var project_id = $("#project_id").find(':selected').val();
            for (var i = 0; i < data.length; i++) {
                if(data[i].project_id == project_id)
                {
                    for (var j = 0; j < window.customerData.length; j++) 
                    {
                      if(data[i].doc_type == 2)
                      {
                        if(window.customerData[j].user_id == data[i].invoice_customer_id)
                        {
                            $('#customer_name').val(window.customerData[j].user_name);
                            $('#customer_id').val(window.customerData[j].user_id);
                        }
                      }else
                      {
                        if(window.customerData[j].user_id == data[i].quotation_customer_id)
                        {
                            $('#customer_name').val(window.customerData[j].user_name);
                            $('#customer_id').val(data[i].user_id);
                        }
                      }
                        
                    }
                    var data2 = (data[i].doc_type == 1) ? window.proforma_invoices : window.invoices;
                    var this_setting = JSON.parse(window.setting);
                    var prefix = this_setting['company_prefix'];
                    prefix = (data[i].doc_type == 1) ? prefix + this_setting['quotation_prfx'] : prefix + this_setting['invoice_prfx'];
                    var html = '';
                    for (var l = 0; l < data2.length; l++) {
                        var title = (data2[l].revised_no > 0) ? data2[l].invoice_no + '-R' + data2[l].revised_no : data2[l].invoice_no;
                        if (data2[l].ID == data[i].doc_id) {
                            html += "<option selected value='" + data2[l].ID + "' data-expiry = '"+data[i].inv_expiry_date+"'>" + prefix + title + "</option>"
                        }
                    }
                    $('#invoice_id').html(html).trigger('change');
                    
                    if(data[i].doc_type == 2) //Invoice
                    {
                        for (var k = 0; k < window.endUserData.length; k++) 
                        {
                            if(window.endUserData[k].end_user_id  == data[i].end_user)
                            {
                                $('#end_user').val(window.endUserData[k].end_user_name);
                                $('#end_user_id').val(data[i].end_user);
                            }
                        }
                    }else //PI
                    {
                        $('#end_user').val('');
                        $('#end_user_id').val('');
                    }
                    
                }
            }

            data = window.project_task;
            var html2 = '';
            var count = 0;
            for (var i = 0; i < data.length; i++) 
            {
                if(data[i].project_id == project_id)
                {
                    count++;
                    html2 += '<tr class="txtMult">';
                        
                        html2 += '<td>';
                            html2 += '<a href="javascript:void(0);"  class="remCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
                        html2 += '</td>';

                        html2 += '<td>';
                            html2 += '<input type="text" id="task_description'+count+'" required name="task_description[]" value="'+data[i].pt_description+'">';
                        html2 += '</td>';

                        html2 += '<td>';
                            html2 += '<input type="text" id="no_of_hours'+count+'" required class="txtboxToFilter" name="no_of_hours[]" value="'+data[i].no_of_hours+'">';
                        html2 += '</td>';

                        html2 += '<td>';
                            html2 += '<select required id="type_of_task'+count+'" name="type_of_task[]" required>';
                            html2 += '<option selected disabled> --- Select Type ---</option>';
                            for (var j = 0; j < window.projectCateogryData.length; j++)
                            {
                                html2 += '<option value="'+window.projectCateogryData[j].pc_id +'"';
                                if(window.projectCateogryData[j].pc_id == data[i].type_of_task)
                                {
                                  html2 += 'selected';
                                }
                                html2 +='>';
                                html2 += window.projectCateogryData[j].pc_name;
                                html2 += '</option>';    
                            } 
                            html2 += '</select>';
                        html2 += '</td>';
                      html2 +='<td>';
                      html2 += '<input type="time" required class="form-control" name="from_time[]" value="">';
                      html2 += '</td>';

                      html2 +='<td>';
                      html2 += '<input type="time" required class="form-control" name="to_time[]" value="">';
                      html2 += '</td>';
                    html2 += '</tr>';
                }
            }
            $("#custom_row .txtMult").html('');
            $('#custom_row').append(html2);
        });

        $(document).on('change', '#invoice_id', function(e) {
            console.log('Invoice Changed');
            $('#invoice_no').val($( "#invoice_id option:selected" ).text());
            $('#invoice_expiry_date').val($( "#invoice_id option:selected" ).data('expiry'));
            var type = $('[name="type"]:checked').val();
            var invoice_type = $('[name="invoice_type"]:checked').val();
            var invoice_id = $("#invoice_id").find(':selected').val();
            if(type == 1) //Others
            {
                $('#end_user').val('');
                $('#end_user_id').val('');
                $('#customer_name').val('');
                $('#customer_id').val('');
                var data = (invoice_type == 1) ? window.proforma_invoices : window.invoices;

                for (var i = 0; i < data.length; i++) {
                    if(invoice_id == data[i].ID)
                    {
                       if(invoice_type == 0) //Invoice
                        {
                            for (var j = 0; j < window.endUserData.length; j++) 
                            {
                                if(data[i].end_user  == window.endUserData[j].end_user_id)
                                {
                                    $('#end_user').val(window.endUserData[j].end_user_name);
                                    $('#end_user_id').val(data[i].end_user);
                                }
                            }
                        }
                        for (var k = 0; k < window.customerData.length; k++) 
                        {
                            if(window.customerData[k].user_id == data[i].customer_id)
                            {
                                $('#customer_name').val(window.customerData[k].user_name);
                                $('#customer_id').val(data[i].customer_id);
                            }
                        }
                    }
                }
                
            }
        });

        $(".addCF").live('click', function(e){
            e.preventDefault();
            var html2 = '';
            html2 += '<tr class="txtMult">';
                        
                html2 += '<td>';
                    html2 += '<a href="javascript:void(0);" class="remCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
                html2 += '</td>';

                html2 += '<td>';
                    html2 += '<input type="text" name="task_description[]">';
                html2 += '</td>';

                html2 += '<td>';
                    html2 += '<input type="text" class="txtboxToFilter" name="no_of_hours[]">';
                html2 += '</td>';

                html2 += '<td>';
                    html2 += '<select name="type_of_task[]" >';
                    html2 += '<option selected value="0">Select Ticket Type</option>';
                    for (var j = 0; j < window.projectCateogryData.length; j++)
                    {
                        html2 += '<option value="'+window.projectCateogryData[j].pc_id +'"';
                        html2 +='>';
                        html2 += window.projectCateogryData[j].pc_name;
                        html2 += '</option>';    
                    } 
                    html2 += '</select>';
                html2 += '</td>';
            html2 += '</tr>';
            $('#custom_row').append(html2);
        });
        $(document).on('click', '.remCF', function() {
            $(this).parent().parent().remove();
        });
        if (page == "add_invoice_ticket") {
          console.log('Trigger Called');
          $('#invoice_id').val('<?= @$ticket_invoice_id?>').prop('selected',true).trigger('change');
        }
        
  });
</script>