<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<br><br>
<br><br>
<table border="0" width="100%">
	<tr>
		<td><p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Ref. No.: SMGT/1806-05AR3</p></td>
	</tr>
	<tr>
		<td><p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Date: September 15, 2018</p></td>
	</tr>
</table>
<br><br>
<table border="0" width="100%">
	<tr>
		<td><p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Mr. ABC</p></td>
	</tr>
	<tr>
		<td><p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Manager</p></td>
	</tr>
	<tr>
		<td><p style="font-family: Calibri; font-size: 12px; font-weight: bold;">ABC LLC</p></td>
	</tr>
	<tr>
		<td><p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Dubai, U.A.E</p></td>
	</tr>
</table>
<br>
<table border="0" width="100%">
	<tr>
		<td><p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Sub:	………dynamic subject text will come here…………. </p></td>
	</tr>
</table>
<br><br>
<table border="0" width="100%">
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Dear Sir,</p>
			<br><br><br>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px;">With reference to your recent inquiry regarding the subject matter, please find below the details as well as our best prices that we believe will suit all your requirements and will provide you all the required output as per your satisfaction.</p>
			<br><br>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px;">We are submitting the attached proposal with all the details along with the cost break-up of the system to give you a clear idea. We hope you will find our proposal and our services the most competitive in terms of Price, Quality & Service.</p>
			<br><br>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px;">We are submitting the attached proposal with all the details along with the cost break-up of the system to give you a clear idea. We hope you will find our proposal and our services the most competitive in terms of Price, Quality & Service.</p>
			<br><br>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px;">We would like to thank you for considering us for the current requirements and also hope to be in contact and serve you in the future.</p>
			<br><br>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px;">Should you require any clarification and / or any other information, please do not hesitate to contact us anytime at your convenience.</p>
			<br><br><br>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px; font-weight: bold;">Thanks & Regards,</p>
			<br><br><br><br>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px; font-weight: bold;">&lt;Sales Person Name&gt;</p>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px; font-weight: bold;">&lt;Sales person Position in the Company&gt;</p>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px; font-weight: bold;">&lt;Mobile No.&gt;</p>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-family: Calibri; font-size: 12px; font-weight: bold;"><span style="color:#0000ff;">&lt;Email</span> Address&gt;</p>
		</td>
	</tr>
</table>
<pagebreak/>
<br><br><br><br>
<table border="0" width="100%" align="center" bgcolor="#a6a6a6">
	<tr>
		<td align="center">
			<h2 style="font-family: Verdana; font-size: 16px;">Financial Details</h2>
		</td>
	</tr>
</table>
<br>
<table border="0" width="100%" align="center">
	<tr>
		<td align="center">
			<p style="font-family: Verdana; font-size: 12px; font-weight: bold; border-bottom: 1px solid #000;">EaZy-Q – Queue Management System –<span style="border-bottom: 1px solid #0000ff;color: #0000ff;">ADVANCE</p>
		</td>
	</tr>
	<tr>
		<td align="center">
			<p style="font-family: Verdana; font-size: 12px; font-weight: bold; border-bottom: 1px solid #ff0000; color: #ff0000">
				(Product of Canada)
			</p>
		</td>
	</tr>
</table>
<br>
<table border="1" width="100%">
	<tr bgcolor="#cccccc">
		<th align="center" width="6%"><p style="font-family: Calibri;font-size: 11px;">S. No.</th>
		<th align="left" width="54%"><p style="font-family: Calibri;font-size: 11px;">Product Description</th>
		<th align="center" width="6%"><p style="font-family: Calibri;font-size: 11px;">Qty.</th>
		<th align="center" width="15%"><p style="font-family: Calibri;font-size: 11px;">Reseller Price/Unit</th>
		<th align="center" width="19%"><p style="font-family: Calibri;font-size: 11px;">Amount (AED) F.O.B. UAE</th>
	</tr>
	<tr>
		<td width="" valign="top"><p style="font-family: Calibri;font-size: 11px;">1</p></td>
		<td width="">
			<p style="font-family: Calibri;font-size: 11px;"><strong>Main Display &ndash; 32&rdquo; LED TV&nbsp; (Samsung/LG Brand) </strong></p>
			<p style="font-family: Calibri;font-size: 11px;">(4 / 8 / 10 Lines - User&rsquo;s Choice. Can display Scrolling Messages / Promotions / Videos / Live-TV on part of the Display) <em>Including Extended Cables &amp; Accessories</em></p>
			<p style="font-family: Calibri;font-size: 11px; color: #0000ff;"><strong><em>Note: Recorded Videos, Pictures &amp; Text-Scrolling-Messages are available as Built-in feature in the system.</em></strong></p>
		</td>
		<td width="" valign="top" align="right"><p style="font-family: Calibri;font-size: 11px;">1</p></td>
		<td width="" valign="top" align="right">
			<p style="font-family: Calibri;font-size: 11px;">120</p>
			<p style="font-family: Calibri;font-size: 11px;font-weight: bold; color: #ff0000">
				<em>120</em>
			</p>
			<br>
			<p>
				<img src="<?= base_url(); ?>/assets/admin/pdf/Untitled-2.png" width="80px" style="position:absolute; right: 0px; bottom:0px;">
			</p>
		</td>
		<td width="" valign="top" align="right">
			<p style="font-family: Calibri;font-size: 11px;">120</p>
			<br>
			<p>
				<img src="<?= base_url(); ?>/assets/admin/pdf/Untitled-2.png" width="80px" style="position:absolute; right: 0px; bottom:0px;">
			</p>
		</td>
	</tr>
	<tr bgcolor="#fbd4b4">
		<td colspan="4" align="right">
			<p style="font-family:Calibri; font-size: 11px; font-weight: bold;">Total Amount</p>
		</td>
		<td align="right">
			<p style="font-family:Calibri; font-size: 11px; font-weight: bold;">690.00</p>
		</td>
	</tr>
</table>
<pagebreak/>
<br><br><br><br>
<table border="0" width="100%" align="center" bgcolor="#ccffcc">
	<tr>
		<td align="left">
			<h2 style="font-family: Verdana; font-size: 16px; border-bottom: 1px solid #0000ff; color: #0000ff; font-style: italic;">Optional (If Required)</h2>
		</td>
	</tr>
</table>
<br>
<table border="1" width="100%">
	<tr bgcolor="#cccccc">
		<th align="center" width="6%"><p style="font-family: Calibri;font-size: 11px;">S. No.</th>
		<th align="left" width="54%"><p style="font-family: Calibri;font-size: 11px;">Product Description</th>
		<th align="center" width="6%"><p style="font-family: Calibri;font-size: 11px;">Qty.</th>
		<th align="center" width="15%"><p style="font-family: Calibri;font-size: 11px;">Reseller Price/Unit</th>
		<th align="center" width="19%"><p style="font-family: Calibri;font-size: 11px;">Amount (AED) F.O.B. UAE</th>
	</tr>
	<tr>
		<td width="" valign="top"><p style="font-family: Calibri;font-size: 11px;">1</p></td>
		<td width="">
			<p style="font-family: Calibri;font-size: 11px;"><strong>Main Display &ndash; 32&rdquo; LED TV&nbsp; (Samsung/LG Brand) </strong></p>
			<p style="font-family: Calibri;font-size: 11px;">(4 / 8 / 10 Lines - User&rsquo;s Choice. Can display Scrolling Messages / Promotions / Videos / Live-TV on part of the Display) <em>Including Extended Cables &amp; Accessories</em></p>
			<p style="font-family: Calibri;font-size: 11px; color: #0000ff;"><strong><em>Note: Recorded Videos, Pictures &amp; Text-Scrolling-Messages are available as Built-in feature in the system.</em></strong></p>
		</td>
		<td width="" valign="top" align="right"><p style="font-family: Calibri;font-size: 11px;">1</p></td>
		<td width="" valign="top" align="right">
			<p style="font-family: Calibri;font-size: 11px;">120</p>
			<p style="font-family: Calibri;font-size: 11px;font-weight: bold; color: #ff0000">
				<em>120</em>
			</p>
			<br>
			<p>
				<img src="<?= base_url(); ?>/assets/admin/pdf/Untitled-2.png" width="80px" style="position:absolute; right: 0px; bottom:0px;">
			</p>
		</td>
		<td width="" valign="top" align="right">
			<p style="font-family: Calibri;font-size: 11px;">120</p>
			<br>
			<p>
				<img src="<?= base_url(); ?>/assets/admin/pdf/Untitled-2.png" width="80px" style="position:absolute; right: 0px; bottom:0px;">
			</p>
		</td>
	</tr>
	<tr bgcolor="#fbd4b4">
		<td colspan="4" align="right">
			<p style="font-family:Calibri; font-size: 11px; font-weight: bold;">Total Amount</p>
		</td>
		<td align="right">
			<p style="font-family:Calibri; font-size: 11px; font-weight: bold;">690.00</p>
		</td>
	</tr>
</table>
<pagebreak/>
<br><br><br><br>
<table border="0" width="100%" align="center" bgcolor="#a6a6a6">
	<tr>
		<td align="center">
			<h2 style="font-family: Verdana; font-size: 16px;">Terms & Conditions</h2>
		</td>
	</tr>
</table>
<br>
<table border="0" width="100%" align="" bgcolor="">
	<tr>
		<td width="15%">
			<p style="font-family: Verdana; font-size: 12px;"><strong>Validity</strong></p>
		</td>
		<td width="10%"><p style="font-family: Verdana; font-size: 12px;"><strong>:</strong></p></td>
		<td width="75%">
			<p style="font-family: Verdana; font-size: 12px;">30 days from the Date of Quotation</p>
		</td>
	</tr>	
	<tr>
		<td colspan="3"><br></td>
	</tr>	
	<tr>
		<td valign="top">
			<p style=""><strong>Inclusions </strong></p>
		</td>
		<td valign="top"><strong>:</strong></td>
		<td>
			<p style="font-family: Verdana; font-size: 12px;"><u>The cost of the project includes the following:</u></p>
			<ul>
				<li style="font-family: Verdana; font-size: 12px;">Supply of Material in UAE</li>
				<li style="font-family: Verdana; font-size: 12px;">Technical & Operational Training</li>
				<li style="font-family: Verdana; font-size: 12px;">Assistance during Installation</li>
				<li style="font-family: Verdana; font-size: 12px;">Minor Software Modifications (if any)</li>
			</ul>
			<p style="font-family: Verdana; font-size: 12px;"><u>The cost of the project DOES NOT include the following:</u></p>
			<ul>
				<li style="font-family: Verdana; font-size: 12px;">Complete Physical Installation</li>
				<li style="font-family: Verdana; font-size: 12px;">Civil / Carpentry / Masonry / Electrical Work (if any)</li>
				<li style="font-family: Verdana; font-size: 12px;">Nearest Network (Ethernet / Wi-Fi) Points – wherever required</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>	
	<tr>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;"><strong>Payment </strong></p>
		</td>
		<td valign="top"><strong>:</strong></td>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;">100% by T/T in advance</p>
		</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>	
	<tr>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;"><strong>Project Duration </strong></p>
		</td>
		<td valign="top"><strong>:</strong></td>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;">As per following plan: (After Order Confirmation & Receipt of Payment)</p>
		</td>
	</tr>
	<!-- image -->
	<tr>
		<td colspan="3"><br></td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%" border="1">
				<tr>
					<td width="35%" align="right">
						<p style="font-weight:bold; font-family: Verdana; font-size: 12px;">User Training  & Hand Over</p>
					</td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%" bgcolor="#00b050"></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%"></td>
					<td width="5%"></td>
				</tr>
				<tr>
					<td align="right">
						<p style="font-weight:bold; font-family: Verdana; font-size: 12px;">System Testing & Adjustments</p>
					</td>
					<td></td>
					<td></td>
					<td bgcolor="#943634"></td>
					<td bgcolor="#943634"></td>
					<td bgcolor="#943634"></td>
					<td bgcolor="#943634"></td>
					<td bgcolor="#943634"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align="right">
					<p style="font-weight:bold; font-family: Verdana; font-size: 12px;">Software Customization & Integration</p>
					</td>
					<td bgcolor="#ffc000"></td>
					<td bgcolor="#ffc000"></td>
					<td bgcolor="#ffc000"></td>
					<td bgcolor="#ffc000"></td>
					<td bgcolor="#ffc000"></td>
					<td bgcolor="#ffc000"></td>
					<td bgcolor="#ffc000"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align="right">
					<p style="font-weight:bold; font-family: Verdana; font-size: 12px;">Installation, Configuration & Testing</p>
					</td>
					<td bgcolor="#ff0000"></td>
					<td bgcolor="#ff0000"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align="right">
					<p style="font-weight:bold; font-family: Verdana; font-size: 12px;">Delivery of Material (in Dubai)</p>
					</td>
					<td bgcolor="#0000ff"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr bgcolor="#d9d9d9">
					<td align="right">
					<p style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">Duration (<span style="color:#0000ff;">Weeks)</p>
					</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">1</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">2</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">3</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">4</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">5</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">6</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">7</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">8</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">9</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">10</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">11</td>
					<td align="center" style="font-weight:bold;font-style: italic; font-family: Verdana; font-size: 12px;">12</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>	
	<tr>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;"><strong>Support </strong></p>
		</td>
		<td valign="top"><strong>:</strong></td>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;">1 Year Telephonic and Online support will be provided</p>
		</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>	
	<tr>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;"><strong>Warranty </strong></p>
		</td>
		<td valign="top"><strong>:</strong></td>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;">1 Year Comprehensive Back-to-Base Warranty for all Manufacturing Defects</p>
		</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>
	<tr>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;"><strong>Extended Warranty </strong></p>
		</td>
		<td valign="top"><strong>:</strong></td>
		<td valign="top">
			<p style="font-family: Verdana; font-size: 12px;">From 2nd Year On-wards Warranty		Available at 12% of the Project Value per annum</p>
		</td>
	</tr>
	
</table>
	


</body>
</html>