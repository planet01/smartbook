 <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active"><?= $title ?> </a> </li>
      </ul>
      <!--<div class="page-title"> <i class="icon-custom-left"></i>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4><?=  $title  ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($save_product)?>" method="post">
                    <div class="row " >

                        <div class="col-md-3">
                          <div class="row">
                            <div class="col-md-12">
                              <label class="form-label">Material Issue Note No</label>
                            </div>
                          </div>
                          <div class="row">
                            <!-- <div class="col-md-12"> -->
                              <div class="col-lg-10 col-md-12 col-sm-12">
                                <div class="form-group">
                                  <div class="right controls" id="span-pre">
                                    <i class=""></i>
                                    <input  type="text" readonly value="<?= ($page_title != 'add')?@$setval["company_prefix"].@$setval["material_issue_prefix"].@$data['material_issue_note_no']: ''; ?>" class="form-control" placeholder="" >
                                  </div>
                                </div>
                              </div>
                            <!-- </div> -->
                          </div> 
                        </div>
                         <input type="hidden" name='delivery_prfx' value='<?= @$setval["material_issue_prefix"]?>'>
                        <input name="material_issue_note_no" type="hidden" value="<?= ($page_title == 'add')? 1 :@$data['material_issue_note_no']; ?>" class="form-control" placeholder="">
                        <div class="col-md-3">
                          <div class="row">
                            <div class="col-md-6 col-sm-6">
                              <label class="form-label">Customer</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-12">
                              <div class="form-group">
                                    <i class=""></i>
                                       <select <?= @$disable ?> name="customer_id" id="customer_id" class="select2 form-control">
                                          <option value="0" selected >--- Select customer ---</option>
                                          <?php
                                          foreach($customerData as $k => $v){
                                          ?>
                                          <option value="<?= $v['user_id']; ?>" <?= ( $v['user_id'] == @$data['customer_id'])?'selected':''; ?> ><?= $v['user_name']; ?></option>
                                          <?php
                                          }
                                          ?>
                                    </select>
                              </div>
                            </div>
                           </div>

                        </div>
                         <div class="col-md-3">
                          <div class="row">
                            <div class="col-md-6 col-sm-6">
                              <label class="form-label">Employee</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-12">
                              <div class="form-group">
                                    <i class=""></i>
                                       <select <?= @$disable ?> name="employee_id" id="employee_id" class="select2 form-control">
                                          <option value="0" selected >--- Select employee ---</option>
                                          <?php
                                          foreach($employeeData as $k => $v){
                                          ?>
                                          <option value="<?= $v['user_id']; ?>" <?= ( $v['user_id'] == @$data['employee_id'])?'selected':''; ?> ><?= $v['user_name']; ?></option>
                                          <?php
                                          }
                                          ?>
                                    </select>
                              </div>
                            </div>
                           </div>
                           
                        </div>

                        <!-- <div class="col-md-3">
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <label class="form-label">Stock Delivered From</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-12">
                              <div class="form-group">
                                <div class="input-with-icon right controls" id="span-pre">
                                  <i class=""></i>       
                                    <select <?= @$disable ?> name="material_issue_note_warehouse_id" style="width:100%"  id="warehouse_id" class="form-control warehouse_id" required>
                                      <option value="0" selected disabled>--- Select Location ---</option>
                                      <?php 
                                        foreach($warehouseData as $k => $v){ 
                                        ?>
                                        <option <?=( @$data['material_issue_note_warehouse_id'] == $v['warehouse_id'] )? 'selected ': '' ?>
                                        value="<?= $v['warehouse_id']; ?>">
                                            <?= $v['warehouse_name']; ?>
                                        </option>
                                      <?php } ?>
                                    </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> -->

                        <!-- <div class="clearfix"></div> -->

                        <div class="col-md-3">
                          <div class="row">
                            <div class="col-md-6 col-sm-6">
                              <label class="form-label">Date</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-12 success-control">
                              <div class="form-group">
                                <div class=" right controls" id="span-pre">
                                  <i class=""></i>
                                  <input <?= @$disable ?> type="text" class="datepicker" name="material_issue_note_date" value="<?= @$data['material_issue_note_date'] ?>" placeholder="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="clearfix"></div> 

                          <div class="col-md-12">
                            <div class="dataTables_wrapper" style="width: 100%;">
                              <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th class="text-center" style="width:60px">Add/Remove Row</th>
                                    <th class="text-center" style="min-width:300px;">Product Name</th>
                                    <!-- <th class="text-center" width="200px">Product Description</th> -->
                                    <th class="text-center" style="min-width:100px";>Quantity</th>
                                    <th class="text-center" style="min-width:100px";>Warehouse</th>
                                  </tr>
                                </thead>
                                <tbody id="customFields">
                                  <?php if(!isset($disable)){ ?>
                                  <tr class="txtMult">
                                    <td class="text-center"><a href="javascript:void(0);" class="addCF"><a href="javascript:void(0);" class="addCF "><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></a></td>
                                    <td colspan="3"></td>
                                  </tr>
                                   <?php
                                  }
                                   $total_quantity = 0;
                                   // print_b($material_issue_note_detail_data);
                                   if (isset($material_issue_note_detail_data) && @$material_issue_note_detail_data != null) {
                                    foreach ($material_issue_note_detail_data as $k => $v) {
                                   ?>
                                   <tr class="txtMult" >
                                        <td class="text-center" style="width: 60px">
                                          <?php
                                          if(isset($disable)){
                                          ?>
                                           <a href="javascript:void(0);">
                                              Remove (disabled)
                                           </a>
                                          <?php
                                          }else{
                                          ?>
                                          <a href="<?= site_url("material_issue_note/material_issue_noteDetailDelete/".$v['material_issue_note_detail_id']); ?>" class="ajaxbtn remCF" rel="delete">Remove</a>
                                          <?php
                                           }
                                          ?>
                                        </td>
                                        <td>
                                            <select <?= @$disable ?> name="product_id[]" id="pproduct_id<?= $k; ?>" class="form-control prod_name select2" style="width: 100%" required>
                                                <option selected disabled>--- Select Product ---</option>
                                                <?php foreach($productData as $k => $v2){ ?>
                                                    <option value="<?= $v2['product_id']; ?>" <?=( $v2['product_id']== @$v['product_id'])? 'selected': ''; ?> >
                                                        <?= $v2['product_sku']; ?> - 
                                                        <?= $v2['product_name']; ?>
                                                    </option>
                                                    <?php } ?>
                                            </select>
                                        </td>
                                        <!-- <td>
                                            <input type="text" class="code street" style="width: 100%" id="ddiscount_type<?= $k; ?>" name="material_issue_note_detail_description[]" value="<?= @$v['material_issue_note_detail_description']; ?>" placeholder="" />
                                        </td> -->
                                        <?php

                                          $total_inventory_quantity = getCustomRow("SELECT *,(COALESCE(SUM(`inventory_quantity`),0) - (SELECT COALESCE(SUM(`inventory_quantity`),0) FROM `inventory` WHERE `inventory_type` = 0 AND `product_id` = '".$v['product_id']."')) as total_inventory_quantity FROM `product` JOIN `inventory` ON `inventory`.`product_id` = `product`.`product_id` WHERE `inventory`.`inventory_type` = 1 AND `product`.`product_id` = '".$v['product_id']."' AND product_is_active = 1");
                                        ?>
                                        <!-- max="<?= $total_inventory_quantity['total_inventory_quantity']+@$v['material_issue_note_detail_quantity']; ?>" -->
                                        <td>
                                            <input type="text" <?= @$disable ?> class="code quantity txtboxToFilter quatity_num" style="width: 100%" data-optional="0"  id="quantity" name="material_issue_note_detail_quantity[]" value="<?= @$v['material_issue_note_detail_quantity']; ?>" placeholder="" />
                                        </td>
                                        <td>
                                        <select <?= @$disable ?> name="material_issue_note_detail_warehouse_id[]" style="width:100%"  id="warehouse_id<?= $k; ?>" class="form-control warehouse_id" required>
                                          <option value="0" selected disabled>--- Select Location ---</option>
                                          <?php 
                                            foreach($warehouseData as $k2 => $v2){ 
                                            ?>
                                            <option <?=( @$v['material_issue_note_detail_warehouse_id'] == $v2['warehouse_id'] )? 'selected ': '' ?>
                                            value="<?= $v2['warehouse_id']; ?>">
                                                <?= $v2['warehouse_name']; ?>
                                            </option>
                                          <?php } ?>
                                        </select>
                                        </td>
                                    </tr>
                                    <?php
                                          $total_quantity += $v['material_issue_note_detail_quantity']; 
                                      }
                                    }
                                   ?>

                                </tbody>
                                 <!--  <tr>
                                    <th colspan="2" class="text-right" >Total Quantity</th>
                                    <th class="text-center">
                                      <input type="text" readonly="readonly" class="" id="total_quantity" name="total_quantity" value="<?= @$total_quantity; ?>" style="width:100%" placeholder="0"  />
                                    </th>
                                  </tr> -->
                                  <!--  <th class="text-center"><span id="total_quantity">0</span></th> -->
                              </table>
                            </div>  
                          </div>
                            
                          <div class="col-md-12 ">
                             <div class="row">
                              <div class="col-md-12">
                                <label class="form-label">Notes</label>
                              </div>
                             </div>
                             <div class="row">
                              <div class="col-md-12">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <textarea <?= @$disable ?> name="material_issue_note_customer_note" class="form-control" value="" placeholder=""><?= @$data['material_issue_note_customer_note']; ?></textarea>
                                </div>
                              </div>
                             </div>
                          </div>
                        
                       
                        
                        
                        
                          <!-- <div class="col-md-6 mb-4">
                            <div class="col-md-12">
                              <label class="form-label">Terms & Conditions</label>
                            </div>
                            <div class="col-md-12">
                                <div class="input-with-icon right controls">
                                  <i class=""></i>
                                  <textarea name="material_issue_note_terms_conditions" class="form-control" value="" placeholder=""><?= @$data['material_issue_note_terms_conditions']; ?></textarea>
                                </div>
                            </div>
                          </div> -->
                        
                        
                        
                      
                    
                  </div>
                   <br/>
                   <div class="col-md-12">
                       <div class="form-group text-center">
                         <?php 
                          if(isset($is_detail)){
                         ?>
                         <a href="<?= site_url($view_page);?>">
                          <button class="btn btn-success btn-cons" type="button"><?= ($page_title == 'add')?'':''; ?>Go Back</button> 
                         </a>
                         <?php }else{?>
                          <button class="btn btn-success btn-cons ajaxFormSubmitAlter " type="button"><?= ($page_title == 'add')?'':''; ?> Submit</button> 
                         <?php }?>
                         <input name="id"  type="hidden" value="<?= @$data['material_issue_note_id']; ?>"> 
                         <input name="material_issue_note_no_old"  type="hidden" value="<?= @$data['material_issue_note_no']; ?>"> 

                         <input name="material_issue_note_no_old"  type="hidden" value="<?= @$data['material_issue_note_no']; ?>"> 
                        </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script>
//my code//

//my code//
  $(".disable").each(function(i){
    $(this).click(function () {
        
            $(".disable").attr("disabled", "disabled"); 
            //$("#checkbox1").attr("disabled", "disabled"); 
      
      });

  });

var page = '<?php echo $page_title; ?>';
$(document).ready(function(){
  var max_fields      = 6; 
    // var wrapper         = $(".amenities_field");
    var add_button      = $("#customFields .addCF");
    var x = 1; 

    $(".addCF").live('click', function(e){
       e.preventDefault();
       $(".gridAddBtn").remove();
       //$(add_button).click(function(e){
      if (page == "add") {
          //var employee = $('#employee_id').valid();
          // var warehouse = $('#warehouse_id').valid();
          //if (!employee  || !warehouse) {
          // if (!warehouse) {
          //     return false;
          // }
      }
      else
      {
        // var warehouse = $('#warehouse_id').valid();
        // if (!warehouse) {
        //     return false;
        // }
      }
      e.preventDefault();
      $('form.validate').validate();
      // if(x < max_fields){
        //<td><input type="text" class="code street" style="width:100%" id="discount_type'+x+'" name="material_issue_note_detail_description[]" value="" placeholder="" /> </td>
        x++;
        var temp = '<tr class="txtMult">';
        temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="addCF gridAddBtn"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a> <a href="javascript:void(0);" class="remCF"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a></td>';
        temp    += '<td><select name="product_id[]" style="width:100%" id="product_id'+x+'" class="form-control prod_name select2" required>';
        temp    += '<option selected disabled>--- Select Product ---</option>';
        temp    += '<?php foreach($productData as $k => $v){ ?><option value="<?= $v['product_id']; ?>"><?= $v['product_sku']; ?> - <?= $v['product_name']; ?></option><?php } ?>';
        temp    += '</select></td>';
        temp    += '<td><input type="text" class="code quantity txtboxToFilter quatity_num"  style="width:100%" id="quatity_num'+x+'" data-optional="0" name="material_issue_note_detail_quantity[]" value="" placeholder="" /></td>';
        
        temp    +='<td><select <?= @$disable ?> name="material_issue_note_detail_warehouse_id[]" style="width:100%"  id="warehouse_id'+x+'" class="form-control warehouse_id" required>';
        temp    +='<option value="0" selected disabled>--- Select Location ---</option>';
        temp    += '<?php foreach($warehouseData as $k => $v){ ?><option value="<?= $v['warehouse_id']; ?>"> <?= $v['warehouse_name']; ?></option><?php } ?>';
        temp    += '</select></td>';
        temp    += '</tr>';
        $("#customFields").append(temp);
        $("#product_id"+x).select2();
      // }
       $("html, body").animate({ scrollTop: $(document).height() }, 1000);
    });
    $("#customFields").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
  
});

 
//$(document).on('change', ".txtMult input",function () { 
$(document).on('keydown keypress keyup change blur', ".txtMult input,.txtMult select",function () { 
        var check = $(this).attr('data-optional');
        var mult = 0;
        // alert(123);
        $("#customFields tr.txtMult").each(function () {

            var $quatity_num = $('.quatity_num', this).val();
            if($quatity_num == undefined){
                $quatity_num = ($quatity_num == undefined)?0:$quatity_num;
            }

            var $total = ($quatity_num * 1);
            mult += $total;
        });

        // console.log("loop");

        $("#total_quantity").val(mult).text(mult);
           
  });
  
$(document).on('click', ".remCF",function () { 
    var mult = 0;
    // for each row:
    $("tr.txtMult").each(function () {
        var $quatity_num = $('.quatity_num', this).val();
        if($quatity_num == undefined){
            $quatity_num = ($quatity_num == undefined)?0:$quatity_num;
        }

        var $total = ($quatity_num * 1);
        mult += $total;
    });
    $("#total_quantity").val(mult).text(mult);
    
});

  

    $(document).on('change', ".prod_name",function () {
    
        var prod_name = $(this).parent().parent('tr').find('.prod_name').val();
        var dataString  = 'pid='+prod_name;
        var row = $(this).closest('tr'); // get the row
        var street = row.find('.street'); // get the other select in the same row
        var quatity_num = row.find('.quatity_num');
        //street.empty();
        $.ajax({
          url: "<?php echo site_url('material_issue_note/progt'); ?>",
          dataType: "json",
          type: "POST",
          data: dataString ,
          cache: false,
          success: function(employeeData) {
            if(employeeData) {
              //quatity_num.attr("max", employeeData.total_inventory_quantity);
              var desc = employeeData.product_description;
              street.val($(desc).text());
              
            } else {
              $("#heading").hide();
              $("#records").hide();
              $("#no_records").show();
            }
          }
        });
    });
        
  
  
  $(document).ready(function() {
    
    $("#user_type").change(function () {
      var val = $(this).val();
      if (val == 2) {
        $('.myUserType').slideDown();
      }else{
        $('.myUserType').slideUp();
      }
    });
    
    

    $( ".datepicker" ).datepicker({
       format: "yyyy-mm-dd",
       autoclose: true
       
    });

  });

  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
       /* employee_id:{
          required: true
        },*/
        "product_id[]":{
          required: true
        },
        "material_issue_note_warehouse_id[]":{
          required: true
        },
        "material_issue_note_detail_quantity[]":{
          required: true,
          number:true
        },
        material_issue_note_no:{
          required: true,
          /*digits: true*/
        },
        
        /*material_issue_note_customer_note:{
          required: true
        },*/
       
      }, 
      messages: {
        employee_id: "This field is required.",
        "product_id[]":"This field is required.",
        "product_id[]":"This field is required.",
        "material_issue_note_warehouse_id[]":{
          required:"This field is required.",
          number:"Please Insert Number."
        },
        material_issue_note_no:{
          required: "This field is required.",
          digits: "Please enter only digits."
        },
        material_issue_note_customer_note: "This field is required.",
        material_issue_note_terms_conditions: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    $('#fileUploader').fileuploader({
      changeInput: '<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                          '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                        '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                        '<p>or</p>' +
                        '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                      '</div>' +
                    '</div>',
      theme: 'dragdrop',
      // limit: 4,
      // extensions: ['jpg', 'jpeg', 'png', 'gif'],
      onRemove: function(item) {
        
      },
      captions: {
              feedback: 'Drag and drop files here',
              feedback2: 'Drag and drop files here',
              drop: 'Drag and drop files here'
          },
    });
  });
  $("body").on('keypress keyup blur','.quatity_num',function() {
      $(this).val($(this).val().replace(/[^0-9\.]/g,''));
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
          event.preventDefault();
      }
});
</script>