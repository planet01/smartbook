<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Settings
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':''; ?>
                        <?= (isset($page_heading))?$page_heading:''; ?>
                </a>
            </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                    <h3>Terms & Condition For Sells Document</h3>
                        <form class="ajaxForm validate " action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-2 col-sm-2">
                                                <label class="form-label">Validity</label>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="validity_days" type="text" value="<?= @$data['validity_days']; ?>" class="form-control txtboxToFilter" placeholder="Validity">
                                                </div>
                                            </div>
                                            <div class="col-md-4  mt-2">
                                                <label><b>(No. of Days)</b></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row form-row">
                                                <div class="col-md-2 col-sm-2">
                                                    <label class="form-label">Support</label>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                    <div class="input-with-icon right controls">
                                                        <i class=""></i>
                                                        <input name="support_days" type="text" value="<?= @$data['support_days']; ?>" class="form-control txtboxToFilter" placeholder="Support">
                                                    </div>

                                                </div>
                                                <div class="col-md-4  mt-2">
                                                    <label><b>(No. of Days)</b></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                               
                                <div class="clearfix"></div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-2 col-sm-2">
                                                <label class="form-label">Warranty</label>

                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="warranty_years" type="text" value="<?= @$data['warranty_years']; ?>" class="form-control txtboxToFilter" placeholder="Warranty">
                                                
                                                </div>

                                            </div>
                                            <div class="col-md-4 mt-2">
                                                <label><b>(No. of Days)</b></label>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                              
                                <div class="clearfix"></div>
                                     
                                
                                
                                <div class="col-md-12 col-sm-12">
                                    <h3>Payment Terms</h3>
                                  <div class="dataTables_wrapper-425" style="width: 100%;">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th class="text-center" width="60px">Add/Remove Row</th>
                                          <th class="text-center" >Terms & Condition</th>
                                          <th class="text-center" width="100px">Percentage</th>
                                        </tr>
                                      </thead>
                                      <tbody id="customFields">
                                        <tr class="txtMult">
                                          <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                          <td colspan="6"></td>
                                        </tr>

                                         <?php
                                         $total_quantity = 0;
                                         // print_b($material_receive_note_detail_data);
                                         ?>
                                        
                                          <?php
                                                /*$total_quantity += $v['material_receive_note_detail_quantity']; 
                                            }*/
                                          if(@$terms != null && !empty(@$terms)){
                                          foreach($terms as $row){
                                         ?>

                                         <tr class="txtMult">
                                                <td class="text-center pay_terms-dt-td1"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                <td>
                                                    <input type="text" class="terms pay_terms-dt-td2" required id="terms"  name="terms[]" 
                                                    value=" <?= @$row['payment_title'] ?>" placeholder="" />
                                                </td>
                                                <td>
                                                    <input type="text" class="percentage pay_terms-dt-td3 txtboxToFilter" required id="percentage'+x+'" data-optional="0" name="percentage[]" 
                                                    value="<?= @$row['percentage'] ?>" placeholder="" />
                                                </td>
                                            </tr>
                                        <?php } } ?>

                                      </tbody>
                                    </table>
                                  </div>  
                                </div>
                                    
                                
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-sm-12">
                                    <h3>Project Duration</h3>
                                  <div class="dataTables_wrapper-425" style="width: 100%;">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th class="text-center" width="60px">Add/Remove Row</th>
                                          <th class="text-center" width="350px">Terms & Condition</th>
                                          <th class="text-center" width="50px">Hash Color Code</th>
                                          <th class="text-center" width="50px">Start Week</th>
                                        </tr>
                                      </thead>
                                      <tbody id="new_customFields">
                                        <tr class="new_txtMult">
                                          <td class="text-center"><a href="javascript:void(0);" class="new_addCF">Add</a></td>
                                          <td colspan="6"></td>
                                        </tr>
                                        
                                          <?php
                                          if(@$project_terms != null && !empty(@$project_terms)){
                                          foreach($project_terms as $row){
                                         ?>

                                         <tr class="new_txtMult">
                                                <td class="text-center proj_dur-dt-td1" ><a href="javascript:void(0);" class="new_remCF">Remove</a></td>
                                                <td>
                                                    <input type="text" class="project_terms proj_dur-dt-td2" required id="project_terms"  name="project_terms[]" 
                                                    value="<?= @$row['project_title'] ?>" placeholder="" />
                                                </td>
                                                <td>
                                                    <input type="text" class="hash_code proj_dur-dt-td2" required id="hash_code"  name="hash_code[]" 
                                                    value="<?= @$row['hash_code'] ?>" placeholder="" />
                                                </td>
                                                <td>
                                                    <input type="text" class="start_week proj_dur-dt-td2 txtboxToFilter" required id="start_week"  name="start_week[]" 
                                                    value="<?= @$row['start_week'] ?>" placeholder="" />
                                                </td>
                                            </tr>
                                        <?php } } ?>

                                      </tbody>
                                    </table>
                                  </div>  
                                </div>
                                <div class="clearfix"></div>
                            </div>

                                <div class="col-md-12 mt-4">
                                    <div class="form-group text-center">
                                        <input type="hidden" name="setting_id" value="<?= @$data['setting_id']?>" placeholder="">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['setting_id']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {

        
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });

    });
    $(document).ready(function() {

        $("form.validate").validate({
            rules: {
                'start_week[]':{
                    required: true,
                    digits: true
                },
                'percentage[]':{
                    required: true,
                    digits: true
                }
            },
            messages: {
                
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit    
                error("Please input all the mandatory values marked as red");

            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    $(document).ready(function(){
        var max_fields      = 6; 
        var add_button      = $("#customFields .addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity terms" required  style="width:100%" id="terms'+x+'" data-optional="0" name="terms[]" value="" placeholder="" />';
            temp    += '</td>';
             temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter percentage" required  style="width:100%" id="percentage'+x+'" data-optional="0" name="percentage[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#customFields").append(temp);
          // }
        });
        $("#customFields").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });

        var max_fields      = 6; 
        var add_button      = $("#new_customFields .new_addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="new_txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="new_remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity  project_terms" required  style="width:100%" id="project_terms'+x+'" data-optional="0" name="project_terms[]" value="" placeholder="" />';
            temp    += '</td>';
             temp    += "<td>";
            temp    += '<input type="text" class="code quantity  hash_code" required  style="width:100%" id="project_terms'+x+'" data-optional="0" name="hash_code[]" value="" placeholder="" />';
            temp    += '</td>';
             temp    += "<td>";
            temp    += '<input type="text" class="start_week proj_dur-dt-td2 txtboxToFilter" required  style="width:100%" id="start_week'+x+'" data-optional="0" name="start_week[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#new_customFields").append(temp);
          // }
        });
        $("#new_customFields").on('click','.new_remCF',function(){
            $(this).parent().parent().remove();
        });
      
    });

</script>
