
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Schedule
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
      </ul>
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border"> 
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                    <div class="row">

                      <div class="col-md-4">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Schedule Date</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="schedule_date" id="schedule_date"  type="text" value="<?= @$data['schedule_date']; ?>"  class="form-control datepicker" placeholder="Enter Date">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Stand By Employee</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="standby_employee" name="standby_employee">
                                      <option value="" selected="selected" disabled="">- Select Employee -</option>
                                      <?php
                                       foreach($employeeData as $v){
                                        $selected = (isset($data['standby_employee']) &&  $data['standby_employee'] == $v['user_id'])? 'selected="selected"':'';
                                        ?> 
                                          <option <?= $selected ?> value="<?= $v['user_id'] ?>"> <?= $v['user_name'] ?></option>
                                      <?php
                                        }
                                      ?>
                                    </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>
                      
                      <div class="col-md-2">
                        <div class="form-group ">
                          <a class="btn btn-success btn-cons" id="fetch" href="#">Fetch</a>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <div class="form-group ">
                          <button class="btn btn-success btn-cons ajaxFormSubmitAlter" style="background-color:#19915fd9" type="button"><?= ($page_title == 'add')?'Save':'Update'; ?></button>
                          <input name="id"  type="hidden" value="<?= @$data['schedule_id']; ?>">
                          <input name="schedule_date_old"  type="hidden" value="<?= @$data['schedule_date']; ?>">
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="col-md-12">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <textarea id="myeditor" name="schedule_note"><?= @$data['schedule_note']?></textarea>
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script type="text/javascript">
  $(document).ready(function() {

    $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });
    $("form.validate").validate({
      rules: {
        schedule_date:{
          required: true
        },
        standby_employee:{
          required: true
        },
        schedule_note:{
          required: true
        }
      }, 
      messages: {
        schedule_date: "This field is required.",
        standby_employee: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });

      $(document).on("click", "#fetch", function(event){
        event.preventDefault();
        var schedule_date_valid = $('#schedule_date').valid();
        if(!schedule_date_valid)
        {
            return false;
        }

        var schedule_date = $('#schedule_date').val();
        var employee_id = $('#standby_employee option:selected').val();
            

        CKEDITOR.instances['myeditor'].setData('');
        $.ajax({
              url: "<?php echo site_url('Scheduling/fetch_tickets'); ?>",
              dataType: "json",
              type: "POST",
              data: {
                schedule_date : schedule_date,
                employee_id : employee_id,
                },
              cache: false,
              success: function(result) {
                var html = "";
                // html += "<table>";
                //     html += "<thead>";
                //         html += "<tr>";
                //             html += "<th>Date</th>";
                //             html += "<th>Day</th>";
                //             html += "<th>Engineer</th>";
                //             html += "<th>Project / Client</th>";
                //             html += "<th>Task Description</th>";
                //         html += "</tr>";
                //     html += "</thead>";
                //     html += "<tbody>";
                //         for (var i = 0; i < result.data.length; i++) 
                //         {
                                
                //             html += "<tr>";
                //                 html += "<td>"+result.data[i].ticket_date+"</td>";
                //                 html += "<td></td>";
                //                 html += "<td></td>";
                //                 html += "<td></td>";
                //                 html += "<td></td>";
                //             html += "</tr>";
                            
                //             for (var j = 0; j < result.data[i].ticket_detail.length; j++) 
                //             {
                //                 html += "<tr>";
                //                     html += "<td></td>";
                //                     html += "<td></td>";
                //                     html += "<td></td>";
                //                     html += "<td></td>";
                //                     html += "<td>"+result.data[i].ticket_detail[j].task_description+"</td>";
                //                 html += "</tr>";
                //             }
                //         }
                //     html += "</tbody>";
                // html += "</table>";
                for (var i = 0; i < result.data.length; i++) 
                {
                  html += "<b>Date:</b> "+result.data[i].ticket_date+"<br>";
                  // html += "<b>Project / Client:</b> "+result.data[i].doc+" / "+result.data[i].customer_name+"<br>";
                  html += "<b>Client:</b> "+result.data[i].customer_name+"<br>";
                  html += "<b>Technician Name:</b> "+result.data[i].employee_name+"<br>";
                  // html += "<b>Time:</b> "+result.data[i].ticket_date+"<br>";
                  html += "<b>Task:</b><br>";
                  for (var j = 0; j < result.data[i].ticket_detail.length; j++) 
                  {
                    html += "&nbsp;&nbsp;&nbsp;&nbsp;<b>"+result.data[i].ticket_detail[j].task_description+"</b> -> From: "+result.data[i].ticket_detail[j].from_time+", To: "+result.data[i].ticket_detail[j].to_time+"<br>";
                  }
                  html +="<br>";
                }
                CKEDITOR.instances['myeditor'].setData(html);
              }
        });
        
    });
   
  });
</script>