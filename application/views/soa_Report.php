<style type="text/css">
  .table_align{
        margin-left: 3%;
        margin-right: 3%;
  }
  .table1-td-h4 {
        color: #333333;
  }
  .table1-td-h4-2{
        color: #7f7f7f;
  }
  .table1-td-quotHeading {
        /*text-align: right;*/
        font-size: 14px;
  }
  /*.product-table1-txt-align {
        position: relative;
        right: 0;
  }*/
  .product-table2-th-details {
        /*border: 1px solid #adadad; */
        background-color:#eaeae9;
        color: #000;
  }
  .product-table2-td-details {
        border:1px solid #adadad;
  }
  .div-controls{
    margin-left: 8.5%;
    margin-right: 8.5%;
  }
  .div-font-controls{
    font-family: Calibri;
    font-size: 12px;
  }
  @page toc { sheet-size: A4; }
  span, strong, em, i, p{
    font-family: calibri;
  }
  .td-border{
    border: 7px double #000;
  }
  .td1-tr1-trn-no{
    text-align: right;
    font-size: 18px;
  }
  .td1-tr1-receipt-no{
    text-align: right;
    font-size: 14px;
  }
  
</style>

            <table style="border-collapse: collapse;" width="100%" class="table_align">

              <tr>
                <td width="400px" align="left" >
                  <img src="<?= base_url().'uploads/settings/'.basic_setting()['logo'] ?>" style="
                  width: 350px;height: 100px;margin-left: -2%;" >
                </td>

                <td align="right" width="400px" style="padding-top:15px;">
                    
                    <span class="td1-tr1-trn-no">TRN <?= @$setval['setting_company_reg'] ?></span>
                    <h1 class="">Statement Of Accounts</h1>
                    <hr style="width:57%;text-align:right;margin:7px 0;border: 1px solid #000;">
                    <?php
                     if($invoice_type == 1){
                    ?>
                    <span class="td1-tr1-receipt-no"><?= @$date_from; ?> To <?= @$date_to; ?></span>
                    <hr style="width:57%;text-align:right;margin:7px 0;border: 1px solid #000;">
                    <?php } ?>
                    
                   
                </td>
              </tr>
              
              <tr>
                <td  align="left" width="400px">
                    <h4 class="table1-td-h4 table1-td-quotHeading">
                      <span style="font-weight: bold;">To</span>
                      <br>
                      <span style="font-weight: bold;"><?= @$customer_data['user_company_name']; ?></span>
                      <br>
                      <span style="font-weight: normal;"><?= @$customer_data['user_address']; ?></span>
                      <br>
                      <?php if($customer_data['user_country'] == @$setval['setting_company_country']){ ?>
                      <span style="font-weight: normal;">TRN <?= @$customer_data['tax_reg']?></span>
                      <?php } ?>
                    </h4>
                </td>
                
                <td align="right" width="400px">
                  <table>
                    <tr>
                      <th colspan="2" bgcolor="#eaeae9" style="padding: 5px 8px;" align="left" width="300px"><span class="table1-td-quotHeading">Account Summary</span></th>
                    </tr>

                    <tr >
                      <td align="left" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;">Opening Balance</h4></td>
                      <td align="right" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;"><?= @$currencyData['title'] ?> <?= @$total_opening_balance + @$amc_total_opening_balance ?></h4></td>
                    </tr>
                    <tr>
                      <td align="left" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;">Invoiced Amount</h4></td>
                      <td align="right" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;"><?= @$currencyData['title'] ?> <?= @$total_invoiced_amount + @$amc_total_invoiced_amount ?></h4></td>
                    </tr>
                    <tr>
                      <td align="left" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;">Amount Paid</h4></td>
                      <td align="right" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;"><?= @$currencyData['title'] ?> <?= @$total_paid_amount + @$amc_total_paid_amount ?> </h4></td>
                    </tr>
                    <tr>
                      <td colspan="2" width="300px"><hr width="100%" style="border: 1px solid #000;margin: 3px 0;"></td>
                    </tr>
                    <tr>
                      <td align="left" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;">Balance Due</h4></td>
                      <td align="right" width="150px" style="padding: 2px 8px;" ><h4 style="font-weight: normal;font-size: 14px;"><?= @$currencyData['title'] ?> <?= number_format((float)((@$total_opening_balance + @$amc_total_opening_balance + @$total_invoiced_amount + @$amc_total_invoiced_amount) - (@$total_paid_amount + @$amc_total_paid_amount)), 2, '.', ''); ?> </h4></td>
                    </tr>
                    <tr>
                      <td colspan="2" width="300px"><hr width="100%" style="border: 1px solid #000;margin: 3px 0;"></td>
                    </tr>
                  </table>
                </td>
              </tr>

            </table>


            


            <table style="border-collapse: collapse;margin-top: 30px;" class="table_align" width="100%">

              <thead>

                <tr>
                  <th align="left" class="product-table2-th-details" style="font-size: 14px;padding: 5px 10px;" height="30px" width="110">Date</th>
                  <th align="center" class="product-table2-th-details" align="left" height="30px" width="110" style="font-size: 14px;">P.I./ Invoice #</th>
                  <th align="center" class="product-table2-th-details" style="font-size: 14px;" height="30px" width="110">Total Amount</th>
                  <th align="center" class="product-table2-th-details" style="font-size: 14px;" height="30px" width="110">Total Received</th>
                  <th align="center" class="product-table2-th-details" style="font-size: 14px;" height="30px" width="120">Overdue Since</th>
                  <th align="center" class="product-table2-th-details" style="font-size: 14px;" height="30px" width="120">Overdue Amount</th>
                  <th align="center" class="product-table2-th-details" style="font-size: 14px;" height="30px" width="110">Balance</th>

                </tr>

              </thead>
                 
              <tbody>
                
                <tr>
                  <th align="left" colspan="6" style="padding: 5px 10px;font-size: 14px;font-weight: normal;">
                    Opening Balance
                  </th>
                  <th align="right">
                    <span style="font-size: 14px;"><?= @$total_opening_balance ?></span>
                  </th>  
                </tr>
                <?php 
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach($data as $v){ ?>
                      <tr <?= ($count== 1)?'bgcolor="#eaeae9"':''?>>
                        <td align="left" class="" style="padding: 5px 10px;" >
                          <span style="font-size: 14px;"><?php echo $v['invoice_date']?> </span>
                        </td>
                        <td align="center" class="">
                          <span style="font-size: 14px;"><?php echo $v['invoice_no']?> </span>
                        </td>
                        <td align="center" class="">
                          <span style="font-size: 14px;"><?= $v['total_amount_base'];?> </span>
                        </td>
                        <td align="center" class="">
                          <span style="font-size: 14px;"><?= number_format((float)$v['paid_amount_base'], 2, '.', ''); ?></span>
                        </td>
                        <td align="left" class="" style="padding-left: 20px;" >
                          <span style="font-size: 14px;"><?= ($v['expiry_amount_by_pyment_term'] > 0)?@$v['expiry_date_by_pyment_term']:'-'; ?> </span>
                        </td>
                        <td align="right" class="" style="padding-right: 15px;">
                          <span style="font-size: 14px;"><?=  number_format((float)$v['expiry_amount_by_pyment_term'], 2, '.', ''); ?> </span>
                        </td>
                        <td align="right" class="">
                          <span style="font-size: 14px;"><?= number_format((float)$v['amount_left_base'], 2, '.', ''); ?> </span>
                        </td>
                      </tr> 
                  <?php 
                    $count++;
                    if($count == 3)
                    {
                      $count =1;
                    }
                    }
                  } 
                  ?>          
                  
                  <?php 
                  if (isset($amc_data) && @$amc_data != null) {
                    foreach($amc_data as $v){ ?>
                      <tr <?= ($count== 1)?'bgcolor="#eaeae9"':''?>>
                        <td align="left" class="" style="padding: 5px 10px;" >
                          <span style="font-size: 14px;"><?php echo $v['invoice_date']?> </span>
                        </td>
                        <td align="center" class="">
                          <span style="font-size: 14px;"><?php echo $v['invoice_no']?> </span>
                        </td>
                        <td align="center" class="">
                          <span style="font-size: 14px;"><?= $v['total_amount_base'];?> </span>
                        </td>
                        <td align="center" class="">
                          <span style="font-size: 14px;"><?= number_format((float)$v['paid_amount_base'], 2, '.', ''); ?></span>
                        </td>
                        <td align="left" class="" style="padding-left: 20px;" >
                          <span style="font-size: 14px;"><?= ($v['expiry_amount_by_pyment_term'] > 0)?date('M-d-Y', strtotime($v['expiry_date_by_pyment_term'])):'-'; ?> </span>
                        </td>
                        <td align="right" class="" style="padding-right: 15px;">
                          <span style="font-size: 14px;"><?=  number_format((float)$v['expiry_amount_by_pyment_term'], 2, '.', ''); ?> </span>
                        </td>
                        <td align="right" class="">
                          <span style="font-size: 14px;"><?= number_format((float)$v['amount_left_base'], 2, '.', ''); ?> </span>
                        </td>
                      </tr> 
                  <?php 
                    $count++;
                    if($count == 3)
                    {
                      $count =1;
                    }
                    }
                  } 
                  ?>     
              </tbody>

            </table>
           
            <br/>

          
            
        