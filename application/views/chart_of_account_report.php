
<style>

.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}

</style>

<table style="width:100%;border-collapse: collapse;border-top:2px solid #000;border-bottom:2px solid #000;margin-bottom:15%;" class="table_align"> 
<thead>
      <tr>
         <!-- <th width="10px">S.No</th> -->
         <th  width="10px" align="left"  style="padding-left:20px;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>A/c ID</b></th>
         <th  align="left" width="250px" style="padding:5px 0;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Account Name</b></th>
      </tr>
</thead>
      <?php
         $num = 0;
         foreach ($group as $g) 
         {
            ?>
               <tr>
                  <td colspan="2" align="center" width="10px"  style="<?=($num != 0)?'border-top:1px solid #000;':''?>padding-left:20px;"><b><?= $g['AccountGroupName'] ?></b></td>
               </tr>
            <?php
            foreach ($data as $d) 
            {
               if($d['AccountGroupID'] == $g['AccountGroupID']){
                  if($d['AccountLevelID'] == 1){
                     $style = "font-size:16px; font-weight:bold;";
                     $padding = "padding-left:0px;";
                     ?>
                        <tr>
                           <td style="<?= $style?>padding-left:20px"><?= $d['AccountNo']?></td>
                           <td style="<?= $style.$padding?>"><?= $d['AccountDesc']?></td>
                        </tr>
                     <?php
                  }elseif($d['AccountLevelID'] == 2)
                  {
                     $style = "font-size:14px; font-weight:bold;";
                     $padding = "padding-left:10px;";
                     $border= "border-top:1px solid #000;border-bottom:1px solid #000;";
                     ?>
                        <tr>
                           <td style="<?= $style.$border?>padding-left:20px;"><?= $d['AccountNo']?></td>
                           <td style="<?= $style.$padding.$border?>"><?= $d['AccountDesc']?></td>
                        </tr>
                     <?php
                  }
                  elseif($d['AccountLevelID'] == 3)
                  {
                     
                        
                     $style = "font-size:10px;";
                     $padding = "padding-left:20px;";
                     
                     ?>
                        <tr>
                           <td style="<?= $style?>padding-left:20px;"><?= $d['AccountNo']?></td>
                           <td style="<?= $style.$padding?>"><?= $d['AccountDesc']?></td>
                        </tr>
                     <?php
                  }
               }
            }
         }
          
      ?>

      
      
</table>