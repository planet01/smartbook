
<style>
.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}

</style>
   
   <div class="div-controls div-font-controls" style="text-align:center;padding-top:-12%;">
      <h3 class="main-heading">Smart Matrix General Trading LLC</h3>
      <br>
      <h4 class="main-second-heading">Account Ledger</h4>
      <br>
      <h4 class="main-second-heading">From Date: <?= $from_date?> To Date: <?= $to_date?></h4>
      <br>
   </div>

   

   <table style="width:100%;border-collapse: collapse;border-top:2px solid #000;border-bottom:2px solid #000;margin-bottom:15%;" class="table_align"> 
      <!-- first record -->
      <thead>
      <tr>
         <th align="left" width="60px" style="padding:5px 0 5px 10px;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
         <th align="left" width="75px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Docu.#</b></th>
         <th align="left" width="60px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Date</b></th>
         <th align="center" width="100px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Transaction Detail</b></th>
         <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Cheque #</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Debit</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Credit</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Balance</b></th>
      </tr>
      </thead>
      <?php
         $total_debit = 0;
         $total_credit = 0;
         
         foreach ($account_data as $vd) {
            $balance = 0;
            $credit = 0;
            $debit = 0;
            $opt_balance = 0;
             ?>
             <tr>
                 <td style="border-bottom:2px solid #000;border-top:2px solid #000;" colspan="8"><b>Account No #:<?= $vd['AccountNo'].'-'.$vd['AccountDesc']?> </b></td>
             </tr>
             <?php
             $i = 0;
            foreach ($data as $d) {
             if($vd['AccountNo'] == $d['AccountNo']){
                if($i == 0)
                {
                  $opt_balance = $d['previous_balance'];
                }
                $i++;
         ?>
            
            <tr>
                <td align="left" style="padding:5px 0 5px 10px;font-family: TimesNewRoman;font-size: 12px;"><?= $i; ?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $d['TransactionNo']?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $d['Transaction_Date']?></td>
                <td align="center" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $d['Transaction_Detail']?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $d['Cheque_No']?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><b><?= ($d['Amount_Dr'] > 0 )? number_format((float)$d['Amount_Dr'], 2, '.', ''):''?></b></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= ($d['Amount_Cr'] > 0) ?number_format((float)$d['Amount_Cr'], 2, '.', ''):''?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= number_format((float)$d['balance'], 2, '.', '')?></td>
            </tr>
            
         <?php 
            $total_debit += $d['Amount_Dr'];
            $total_credit += $d['Amount_Cr'];

            $credit += $d['Amount_Cr'];
            $debit += $d['Amount_Dr'];
            $balance = $d['balance'];
             }
            }
            ?>
                <tr>
                     <td style="border-top:2px solid #000;" colspan="3"><b>Opt.Bal: <?= number_format((float)$opt_balance, 2, '.', '')?></b></td>
                     <td style="border-top:2px solid #000;" colspan="2" ><b>Debit: <?= number_format((float)$debit, 2, '.', '')?></b></td>
                     <td style="border-top:2px solid #000;" colspan="2" ><b>Credit: <?= number_format((float)$credit, 2, '.', '')?></b></td>
                     <td style="border-top:2px solid #000;"><b>Cl.Bal: <?= number_format((float)$balance, 2, '.', '')?></b></td>
               </tr>
            <?php
        }
         ?>
        <tr>
            <td style="border-top:2px solid #000;" colspan="2"><b>Grand Total</b></td>
            <td style="border-top:2px solid #000;" colspan="2"><b>Opening Bal.: <?= $previous_balance?></b></td>
            <td style="border-top:2px solid #000;" colspan="2"><b>Closing Bal.: <?= $balance?></b></td>
            <td style="border-top:2px solid #000;"><b><?= $total_debit?></b></td>
            <td style="border-top:2px solid #000;"><b><?= $total_credit?></b></td>
        </tr>
      
   </table>
   
  