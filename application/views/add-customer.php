<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Customer
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> -->
                </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Name:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_name" type="text" value="<?= @$data['user_name']; ?>" class="form-control" placeholder="Enter Name">
                                                </div>
                                
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Company Name:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_name" type="text" value="<?= @$data['user_name']; ?>" class="form-control" placeholder="Enter Company Name">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                               <!--  <div class="col-md-6">
                                   <div class="form-group">
                                       <div class="row form-row">
                                           <div class="col-md-12">
                                               <label class="form-label">Display Name:</label>
                                           </div>
                                           <div class="col-md-12">
                                               <div class="input-with-icon right controls">
                                                   <i class=""></i>
                                                   <input name="user_display_name" type="text" value="<?= @$data['user_display_name']; ?>" class="form-control" placeholder="Enter Display Name">
                                               </div>
                               
                                           </div>
                                       </div>
                                   </div>
                               </div> -->

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Company Email Address:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_email" type="text" value="<?= @$data['user_email']; ?>" class="form-control" placeholder="Enter E-mail">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">City:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select name="user_city" id="user_city" class="select2" style="width: 100%;">
                                                        <option selected disabled>--- Select City ---</option>
                                                        <?php foreach($city as $k => $v){ ?>
                                                            <option <?=( $v['city_name']==@ $data['user_city']) ? 'selected' : ''; ?> value="<?= @$v['city_name']; ?>"> <?= $v['city_name']; ?></option>';
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Country:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control select2" id="user_country" name="user_country">
                                                        <option value="" selected="" disabled="">-- Select Country --</option>
                                                        <?php
                                                        foreach ($countries as $k => $v) {
                                                        ?>
                                                            <option value="<?= $v['country_id'] ?>" <?=( $v['country_id']==@ $data['user_country']) ? 'selected' : ''; ?> >
                                                                <?= $v['country_name']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Phone:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_phone" type="text" value="<?= @$data['user_phone']; ?>" class="form-control" placeholder="Enter Phone">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Website:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_website" type="text" value="<?= @$data['user_website']; ?>" class="form-control" placeholder="Enter Website">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Status:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control input-signin-mystyle select2" id="user_is_active" name="user_is_active">
                                                        <option value="" selected="selected" disabled="">- Select Status -</option>
                                                    <option value="1" <?= ($page_title == 'add')?'selected':''?> <?=( @$data[ 'user_is_active']==1 )? 'selected': ''; ?> >Enabled</option>
                                                    <option value="0" <?=( @$data[ 'user_is_active']==0 && $page_title != 'add')? 'selected': ''; ?>>Disabled</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Customer Type:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control select2" id="customer_type" name="customer_type">
                                                        <option value="" selected="" disabled="">-- Customer Type --</option>
                                                        <?php
                                                        foreach ($customer_type as $k => $v) {
                                                        ?>
                                                            <option value="<?= $v['id'] ?>" <?=( $v['id']== @$data['customer_type']) ? 'selected' : ''; ?> ><?= $v['title']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Price List:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control select2" id="plan_type" name="plan_type">
                                                        <option value="" selected="" disabled="">-- Select Price --</option>
                                                        <?php
                                                        foreach ($price_list as $k => $v) {
                                                        ?>
                                                            <option value="<?= $v['price_plan_id'] ?>" <?=( $v['price_plan_id']==@ $data['plan_type']) ? 'selected' : ''; ?> ><?= $v['plan_slog']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">T.R.No. :</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="tax_reg" type="text" value="<?= @$data['tax_reg']; ?>" class="form-control" placeholder="Enter T.R.N.">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>

                                <div class="col-md-6 ">
                                  <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Currency:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select class="form-control select2" id="currency_id" name="currency_id">
                                                    <option value="" selected="" disabled="">-- Select Currency --</option>
                                                    <?php
                                                    foreach ($currency as $k => $v) {
                                                    ?>
                                                        <option value="<?= $v['id'] ?>" <?=( $v['id']==@ $data['currency_id']) ? 'selected' : ''; ?> ><?= $v['title']; ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div >
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                            <h3>End Users</h3>
                                            <div class="row" >
                                                <div class="col-md-12">
                                                <div class="" style="width: 100%;">
                                                    <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                        <th class="text-center" width="60px">Add/Remove Row</th>
                                                        <th class="text-center" width="200px">Name</th>
                                                        <th class="text-center" width="200px">Location</th>
                                                        <th class="text-center" width="200px">Address</th>
                                                        <th class="text-center" width="200px">Contact</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="end_user_customFields">
                                                        <tr class="end_user_txtMult">
                                                            <td class="text-center"><a href="javascript:void(0);" class="end_user_addCF">Add</a></td>
                                                            <td colspan="4"></td>
                                                        </tr>
                                                        
                                                        <?php
                                                         if(@$end_user != null && !empty(@$end_user)){
                                                         foreach($end_user as $row){
                                                        ?>

                                                            <tr class="end_user_txtMult">
                                                                <td class="text-center" style="width:60px;">
                                                                    <!-- <a href="javascript:void(0);" class="end_user_remCF">Remove</a> -->
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" name="end_user_id[]" value="<?= $row['end_user_id']?>">
                                                                    <input type="text" class="" style="width:100%" id="end_user_name'+x+'" name="end_user_name[]" value="<?= $row['end_user_name']?>" placeholder="" />
                                                                </td>
                                                                <td>
                                                                <select name="end_user_location[]" id="end_user_location'+x+'" class="end_user_location" style="width: 100%;">
                                                                    <option selected disabled>--- Select City ---</option>
                                                                    <?php foreach($city as $k => $v){ ?>
                                                                        <option <?=( $v['city_id']==@ $row['end_user_location']) ? 'selected' : ''; ?> value="<?= $v['city_id']; ?>"> <?= $v['city_name']; ?></option>';
                                                                    <?php } ?>
                                                                </select>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="" style="width:100%" id="end_user_address'+x+'" name="end_user_address[]" value="<?= $row['end_user_address']?>" placeholder="" />
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="" style="width:100%" id="end_user_contact'+x+'" name="end_user_contact[]" value="<?= $row['end_user_contact']?>" placeholder="" />
                                                                </td>
                                                            </tr>

                                                        <?php } } ?>

                                                    </tbody>
                                                    </table>
                                                </div>  
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Address:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <textarea name="user_address" type="textarea" id="myeditor"  class="form-control" placeholder="Enter Address"><?= @$data['user_address']; ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-md-offset-3 col-md-6 ">
                                    <div class="form-group">
                                        <?php 
                                        if(!empty($data['user_id']) && @$data['user_id'] != null){
                                            $appendedFiles = array();
                                            $file_path = dir_path().$image_upload_dir.'/'.@$data['user_image'];
                                              if (!is_dir($file_path) && file_exists($file_path)) {
                                                @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['user_image']));
                                                $appendedFiles[] = array(
                                                  "name" => @$data['user_image'],
                                                  "type" => $file_details['mime'],
                                                  "size" => filesize($file_path),
                                                  "file" => site_url($image_upload_dir."/". @$data['user_image']),
                                                  "data" => array(
                                                    "url" => site_url($image_upload_dir."/". @$data['user_image']),
                                                    "image_white_file_id" => $data['user_id']
                                                  )
                                                );
                                              }
                                            $appendedFiles = json_encode($appendedFiles);
                                        }
                                        ?>
                                        <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files='<?php echo @$appendedFiles;?>'>
                                    </div>
                                </div> -->
                                <!-- <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-3">
                                                <label class="form-label">Validity</label><b>(No. of Days)</b>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="validity_days" type="text" value="<?= @$data['validity_days']; ?>" class="form-control" placeholder="Validity">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-3">
                                                <label class="form-label">Support</label><b>(No. of Days)</b>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="support_days" type="text" value="<?= @$data['support_days']; ?>" class="form-control" placeholder="Support">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                               
                                <div class="clearfix"></div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-3">
                                                <label class="form-label">Warranty</label><b>(No. of Years)</b>

                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="warranty_years" type="text" value="<?= @$data['warranty_years']; ?>" class="form-control" placeholder="Warranty">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                  </div> -->
                                <div class="clearfix"></div>
                                     
                                <div class="col-md-12 ">
                                    <h3>Contact Details</h3>
                                      <div class="dataTables_wrapper-1024">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th class="text-center cust_contact_det-dt-th1">Add/Remove Row</th>
                                              <th class="text-center cust_contact_det-dt-th2">Contact Person Name</th>
                                              <th class="text-center cust_contact_det-dt-th3">Contact Number</th>
                                              <th class="text-center cust_contact_det-dt-th4">Position</th>
                                              <th class="text-center cust_contact_det-dt-th5">Email Address</th>
                                            </tr>
                                          </thead>
                                          <tbody id="contact_field">
                                            <tr class="txtMult">
                                              <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                              <td colspan="6"></td>
                                            </tr>
                                              <?php
                                              if(@$contact != null && !empty(@$contact)){
                                              foreach($contact as $row){
                                             ?>

                                             <tr class="txtMult contact_tr">
                                                    <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                    <td>
                                                        <input type="text" class="contact_name" required style="width:100%" id="contact_name"  name="contact_name[]" 
                                                        value="<?= @$row['contact_name'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="contact_no" required style="width:100%" id="contact_no'+x+'" data-optional="0" name="contact_no[]" 
                                                        value="<?= @$row['contact_no'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="contact_position" required style="width:100%" id="contact_position'+x+'" data-optional="0" name="contact_position[]" 
                                                        value="<?= @$row['contact_position'] ?>" placeholder="" />
                                                    </td>
                                                     <td>
                                                        <input type="text" class="contact_email" required style="width:100%" id="contact_email'+x+'" data-optional="0" name="contact_email[]" 
                                                        value="<?= @$row['contact_email'] ?>" placeholder="" />
                                                    </td>
                                                </tr>
                                            <?php } } ?>

                                          </tbody>
                                        </table>
                                      </div>  
                                </div> 
                              
                                <div class="clearfix"></div>
                                     
                                <div class="col-md-12 ">
                                    <h3>Payment Terms</h3>
                                      <div class="dataTables_wrapper-768">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th class="text-center cust_pay_term-dt-th1" >Add/Remove Row</th>
                                              <th class="text-center cust_pay_term-dt-th2" >Terms & Condition</th>
                                              <th class="text-center cust_pay_term-dt-th3" >Percentage</th>
                                              <th class="text-center cust_pay_term-dt-th4" >No Of Days</th>
                                            </tr>
                                          </thead>
                                          <tbody id="customFields">
                                            <tr class="txtMult">
                                              <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                              <td colspan="6"></td>
                                            </tr>
                                              <?php
                                              if(@$terms != null && !empty(@$terms)){
                                              foreach($terms as $row){
                                             ?>

                                             <tr class="txtMult payment_percent_tr">
                                                    <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                    <td>
                                                        <input type="text" class="terms cust_pay_term-dt-td1" required id="terms"  name="terms[]" 
                                                        value="<?= @$row['payment_title'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="percentage cust_pay_term-dt-td2" required id="percentage'+x+'" data-optional="0" name="percentage[]" 
                                                        value="<?= @$row['percentage'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="payment_days cust_pay_term-dt-td3" required id="payment_days'+x+'" data-optional="0" name="payment_days[]" 
                                                        value="<?= @$row['payment_days'] ?>" placeholder="" />
                                                    </td>
                                                </tr>
                                            <?php } } ?>

                                          </tbody>
                                        </table>
                                      </div>  
                                </div>
                                
                                <!-- <div class="clearfix"></div>
                                     <div class="form-group">
                                        <h3>Project Duration</h3>
                                        <div class="row" >
                                            <div class="col-md-12">
                                              <div class="" style="width: 100%;">
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <tr>
                                                      <th class="text-center" width="60px">Add/Remove Row</th>
                                                      <th class="text-center" width="200px">Terms & Condition</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="new_customFields">
                                                    <tr class="new_txtMult">
                                                      <td class="text-center"><a href="javascript:void(0);" class="new_addCF">Add</a></td>
                                                      <td colspan="6"></td>
                                                    </tr>
                                                    
                                                      <?php
                                                      // if(@$project_terms != null && !empty(@$project_terms)){
                                                      // foreach($project_terms as $row){
                                                     ?>

                                                     <tr class="new_txtMult">
                                                            <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="new_remCF">Remove</a></td>
                                                            <td>
                                                                <input type="text" class="project_terms" required style="width:100%" id="project_terms"  name="project_terms[]" 
                                                                value="<?= @$row['project_title'] ?>" placeholder="" />
                                                            </td>
                                                        </tr>
                                                    <?php //} } ?>

                                                  </tbody>
                                                </table>
                                              </div>  
                                            </div>
                                    </div>
                                </div> -->
                                <div class="clearfix"></div>
                                
                                <div class="container-fluid">
                                    <h3>Warranty Terms</h3>
                                  <div class="dataTables_wrapper-768">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th class="text-center cust_warnty_term-dt-th1" >Add/Remove Row</th>
                                          <th class="text-center cust_warnty_term-dt-th2" >Warranty Type</th>
                                          <th class="text-center cust_warnty_term-dt-th3" >Percentage</th>
                                        </tr>
                                      </thead>
                                      <tbody id="warn_customFields">
                                        <tr class="warn_txtMult">
                                          <td class="text-center"><a href="javascript:void(0);" class="warn_addCF">Add</a></td>
                                          <td colspan="6"></td>
                                        </tr>
                                          <?php
                                          if(@$warranty_type != null && !empty(@$warranty_type)){
                                            $check_data = Array();
                                            $new_data   = Array();
                                            $x =1;
                                            foreach($warranty_type as $warranty_type){
                                         ?>

                                            <tr class="warn_txtMult">
                                                <td class="text-center cust_warnty_term-dt-td1"><a href="javascript:void(0);" 
                                                    class="warn_remCF">Remove</a></td>
                                                <td>
                                                    <input type="text" class="title cust_warnty_term-dt-td2" required id="title<?=$x;?>" name="warranty_title[]"
                                                    value="<?= @$warranty_type['title'] ?>" placeholder="" />
                                                </td>
                                                <td>
                                                    <input type="text" class="percentage txtboxToFilter cust_warnty_term-dt-td3" required id="percentage<?=$x;?>" name="warranty_percentage[]"
                                                    value="<?= @$warranty_type['percentage'] ?>" placeholder="" />
                                                </td>
                                                
                                            </tr>
                                        <?php 
                                            }
                                         } 
                                        ?>

                                      </tbody>
                                    </table>
                                  </div>  
                                </div>
                                    
                                
                                <div class="clearfix"></div>    

                                <div class="col-md-12 mt-4">
                                    <div class="form-group text-center">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['user_id']; ?>">
                                        <input type="hidden" name="user_email_old" value="<?= @$data['user_email']; ?>">
                                        <input type="hidden" name="user_phone_old" value="<?= @$data['user_phone']; ?>">
                                        <input type="hidden" name="user_image_old" value="<?= @$data['user_image']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {
        $('#fileUploader').fileuploader({
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-inner">' +
                '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                '<p>or</p>' +
                '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
             onRemove: function(item) {
               $.post('<?= site_url('customer/imageDelete'); ?>', {
                 file: item.name,
                 data: {
                   image_file_id:"<?= @$data['user_id']; ?>",
                   file:item.name,
                   image_post_file_id:item.data.image_file_id
                 }
               });
             },

            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            },
        });
        $("#fileUploader").on("change", function() {
            // console.log('Selected file: ');
        });
        
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {            
        $.validator.addMethod('validUrl', function(value, element) {
            var url = $.validator.methods.url.bind(this);
            return url(value, element) || url('http://' + value, element);
        }, 'Please enter a valid URL');
        $("form.validate").validate({

            rules: {
                user_name: {
                    required: true
                },
                user_company_name: {
                    required: true
                },
                user_display_name: {
                    required: true
                },
                /*user_address: {
                    required: true
                },*/
                user_email:{
                    required: true,
                    email: true
                },
                user_city: {
                    required: true
                },
                user_country: {
                    required: true
                },
                user_phone: {
                    required: true
                },
                /*user_website: {
                    required: true,
                    validUrl: true
                },*/
                user_is_active: {
                    required: true
                },
                customer_type:{
                    required: true
                },
                plan_type:{
                    required: true
                },
                "end_user_name[]": { 
                    required: function(element){
                            return $("#customer_type option:selected").val() == 1;
                    }
                },
                "end_user_location[]": { 
                    required: function(element){
                            return $("#customer_type option:selected").val() == 1;
                    }
                },
                "end_user_address[]": { 
                    required: function(element){
                            return $("#customer_type option:selected").val() == 1;
                    }
                },
                "end_user_contact[]": { 
                    required: function(element){
                            return $("#customer_type option:selected").val() == 1;
                    }
                }
            },
            messages: {
                user_name: "This field is required.",
                user_company_name: "This field is required.",
                user_display_name: "This field is required.",
                user_address: "This field is required.",
                user_email:{
                    required: "This field is required.",
                    email: "Invalid E-mail Address"
                },
                user_city: "This field is required.",
                user_country: "This field is required.",
                user_phone: "This field is required.",
                user_website:{
                    required: "This field is required.",
                    url: "This field required correct format."
                },
                user_is_active: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit   
                error("Please input all the mandatory values marked as red");
 
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
     $(document).ready(function(){
        var page_title = "<?= $page_title; ?>";  
        if(page_title != 'add')
        {
            $('.end_user_location').select2();
        }
        $(document).on('change', "#customer_type", function() {
            // var customer_type = $("#customer_type").find(':selected').val();
            // if(customer_type == 1)
            // {
            //     $('#end_user_div').show();
            // }else
            // {
            //     $('#end_user_div').hide();
            // }
        });
        //end users
        var max_fields      = 6; 
        var add_button      = $("#end_user_customFields .end_user_addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
            x++;
            var temp = '<tr class="end_user_txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="end_user_remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="hidden" name="end_user_id[]" value="">';
            temp    += '<input type="text" class="" style="width:100%" id="end_user_name'+x+'" name="end_user_name[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<select name="end_user_location[]" id="end_user_location'+x+'" class="" style="width: 100%;">';
            temp    += '<option selected disabled>--- Select City ---</option>';
            temp    += '<?php foreach($city as $k => $v){ ?>';
            var option_text = "<?= $v['city_name']; ?>";
            temp    += '<option value="<?= $v['city_id']; ?>"> '+option_text.replace(/'/g,"")+'</option>';
            temp    += '<?php } ?>';
            temp    += '</select>';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="" style="width:100%" id="end_user_address'+x+'" name="end_user_address[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="" style="width:100%" id="end_user_contact'+x+'" name="end_user_contact[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#end_user_customFields").append(temp);
            $("#end_user_location"+x).select2();
        });
        $("#end_user_customFields").on('click','.end_user_remCF',function(){
            $(this).parent().parent().remove();
        });
        //Payment Terms
        var max_fields      = 6; 
        var add_button      = $("#customFields .addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="txtMult payment_percent_tr">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter terms" required  style="width:100%" id="terms'+x+'" data-optional="0" name="terms[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter percentage" required  style="width:100%" id="percentage'+x+'" data-optional="0" name="percentage[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter payment_days" required  style="width:100%" id="payment_days'+x+'" data-optional="0" name="payment_days[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#customFields").append(temp);
          // }
        });
        $("#customFields").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });
        //contact
        var max_fields      = 6; 
        var add_button      = $("#contact_field .addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="txtMult contact_tr">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code txtboxToFilter contact_name" required  style="width:100%" id="contact_name'+x+'" data-optional="0" name="contact_name[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code  txtboxToFilter contact_no" required  style="width:100%" id="contact_no'+x+'" data-optional="0" name="contact_no[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter contact_position" required  style="width:100%" id="contact_position'+x+'" data-optional="0" name="contact_position[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter contact_email" required  style="width:100%" id="contact_email'+x+'" data-optional="0" name="contact_email[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#contact_field").append(temp);
          // }
        });
        $("#contact_field").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });

        var max_fields      = 6; 
        var add_button      = $("#new_customFields .new_addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="new_txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="new_remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter project_terms" required  style="width:100%" id="project_terms'+x+'" data-optional="0" name="project_terms[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#new_customFields").append(temp);
          // }
        });
        $("#new_customFields").on('click','.new_remCF',function(){
            $(this).parent().parent().remove();
        });

        var max_fields      = 6; 
        var add_button      = $("#warn_customFields .warn_addCF");
        var x = 1; 
        var box_size = "<?php echo sizeof($warranty_type); ?>";
        console.log(box_size);
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="warn_txtMult">';
            temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="warn_remCF">Remove</a></td>';
            temp += '<td><input type="text" class="title" required style="width:100%" id="title'+x+'" name="warranty_title[]" placeholder="" /></td>';
            temp += '<td><input type="text" class="percentage txtboxToFilter" required style="width:100%" id="percentage'+x+'" name="warranty_percentage[]"  placeholder="" /></td>';
            temp += '</tr>';
            $("#warn_customFields").append(temp);
          // }
        });
        $("#warn_customFields").on('click','.warn_remCF',function(){
            $(this).parent().parent().remove();
        });
        $(document).on('keydown keypress keyup change blur', ".percentage",function () {
            // count_percentage();            
        });
        // count_percentage();
        function count_percentage(){
            var subtotal = 0;
            var total_percent = 100;
            $('.payment_percent_tr').each(function() {
                var percentage = $(this).find(".percentage").val();
                subtotal += parseInt(percentage);
            });
            $(".percentage_error").remove();
            if(subtotal != total_percent) {
                $('.percentage').last().after('<div class="percentage_error" style="display:none">Total perecentage should be equal to 100<div>');
                $(".percentage_error").css("display","block").css("color","red");
                $(".ajaxFormSubmitAlter").prop('disabled',true);
            }
            else {
                $(".percentage_error").css("display","none");
                $(".ajaxFormSubmitAlter").prop('disabled',false);        
            }
        }

        
      
    });
</script>