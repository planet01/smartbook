<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Product
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> -->
                </a>
            </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <input name="product_name" type="text" value="<?= @htmlspecialchars($data['product_name'], ENT_QUOTES, 'UTF-8'); ?>" class="form-control" placeholder="Enter Name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                               <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Status</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select class="form-control input-signin-mystyle select2" id="product_is_active" name="product_is_active">
                                                    <option value="" selected="selected" disabled="">- Select Status -</option>
                                                    <option value="1" <?= ($page_title == 'add')?'selected':''?> <?=( @$data[ 'product_is_active']==1 )? 'selected': ''; ?> >Enabled</option>
                                                    <option value="0" <?=( @$data[ 'product_is_active']==0 && $page_title != 'add')? 'selected': ''; ?>>Disabled</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                              
                              <div class="clearfix"></div>

                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Model No</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <input name="product_sku" type="text" value="<?= @$data['product_sku']; ?>" class="form-control" placeholder="Enter Model No">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>       

                              <div class="col-md-6 chargeable" >
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Chargeable</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select class="form-control input-signin-mystyle select2 chargeable_list" id="chargeable_list" name="chargeable">
                                                    <option value="" selected="selected" disabled="">- Chargeable -</option>
                                                    <option value="0" <?=( @$data['chargeable']== 0)? 'selected': ''; ?>> Yes</option>
                                                    <option value="1" <?=( @$data['chargeable']== 1 )? 'selected': ''; ?>> No</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                            
                              <div class="clearfix"></div>

                               <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Category </label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select name="category_id" class="select2 category form-control">
                                                    <option selected disabled> --- Select Category ---</option>
                                                    <?php
                                                      foreach($categoryData as $k => $v){
                                                    ?>
                                                        <option value="<?= $v['category_id']; ?>" <?=( $v['category_id']== @$data[ 'category_id'])? 'selected': ''; ?> >
                                                            <?= $v['category_name']; ?>
                                                        </option>
                                                      <?php
                                                        }
                                                      ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                      <div class="col-md-6">
                                        <label class="form-label">Category Type</label>
                                      </div>
                                      <div class="col-sm-12 col-md-9">
                                          <div class="input-with-icon right controls">
                                            <i class=""></i>
                                            <select class="form-control input-signin-mystyle category_type select2" id="category_type" name="category_type">
                                              <option value="" selected="selected" disabled="">- Select Type -</option>
                                              <?php
                                               foreach($categoryTypes as $type){
                                                $selected = (isset($data['category_type']) &&  $data['category_type'] == $type['id'])? 'selected="selected"':'';
                                                ?> 
                                                  <option <?= $selected ?> value="<?= $type['id'] ?>"> <?= $type['title'] ?></option>
                                              <?php
                                                }
                                              ?>
                                            </select>
                                          </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                                <div class="clearfix"></div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Min Stock Quantity</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <input name="min_quantity" id="min_quantity" type="text" value="<?= @$data['min_quantity']; ?>" class="form-control" placeholder="Enter Min Stock Quantity">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                             
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Include in Invoice</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select class="form-control input-signin-mystyle select2 can_invoice" id="can_invoice" name="can_invoice">
                                                    <option value="" selected="selected" disabled="">- Can Invoice -</option>
                                                    <option value="0" <?=( @$data['can_invoice']== 0)? 'selected': ''; ?>> Yes</option>
                                                    <option value="1" <?=( @$data['can_invoice']== 1 )? 'selected': ''; ?>> No</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                             
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Can Deliver</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select class="form-control input-signin-mystyle select2 can_deiliver" id="can_deliver" name="can_deliver">
                                                    <option value="" selected="selected" disabled="">- Can Deliver -</option>
                                                    <option value="0" <?=( @$data['can_deliver']== 0)? 'selected': ''; ?>> Yes</option>
                                                    <option value="1" <?=( @$data['can_deliver']== 1 )? 'selected': ''; ?>> No</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Stock Item</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select class="form-control input-signin-mystyle select2 stock_item" id="stock_item" name="stock_item">
                                                    <option value="" selected="selected" disabled="">- Stock Item -</option>
                                                    <option value="0" <?=( @$data['stock_item']== 0)? 'selected': ''; ?>> Yes</option>
                                                    <option value="1" <?=( @$data['stock_item']== 1 )? 'selected': ''; ?>> No</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              
                              <div class="col-md-6 warranty_coverage">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Warranty Coverage</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <select class="form-control input-signin-mystyle select2 warranty_coverage" id="warranty_coverage" name="warranty_coverage">
                                                    <option selected disabled>- Warranty Coverage -</option>
                                                    <option value="0" <?=( @$data['warranty_coverage']== 0)? 'selected': ''; ?>> Yes</option>
                                                    <option value="1" <?=( @$data['warranty_coverage']== 1 )? 'selected': ''; ?>> No</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>

                              
                              <div class="col-md-6 warranty_days">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Warranty Coverage Days</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <input name="warranty_days" type="text" value="<?= isset($data['warranty_days'])? $data['warranty_days'] : 365 ; ?>" class="form-control" placeholder="Enter warranty coverage days" id="warranty_days">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              
                              <div class="col-md-6 ">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <label class="form-label">Image Printing</label>
                                        </div>
                                        <div class="col-md-9">
                                            <!-- <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <input name="print_width" type="number" min="1" value="<?= @$data['print_width']; ?>" class="form-control" placeholder="" id="print_width"><span class="input-group-ratio">:</span>
                                                <input name="print_height" type="number" min="1" value="<?= @$data['print_height']; ?>" class="form-control" placeholder="" id="print_height">
                                            </div> -->
                                            <div class="my-input-control input-group input-with-icon right controls">
                                                <input type="text" name="print_width" min="1" value="<?= @$data['print_width']; ?>" class="form-control only-numeric" placeholder="Width"/>
                                                <span class="input-group-addon"><b>:</b></span>
                                                <input type="text" name="print_height"  min="1" value="<?= @$data['print_height']; ?>"  class="form-control only-numeric" placeholder="Height"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                             
                              
                              <div class="clearfix"></div>
                              <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-12">
                                            <label class="form-label">Description</label>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-with-icon right controls main_editor">
                                                <i class=""></i>
                                                <textarea name="product_description"  id="myeditor" class="editor form-control" placeholder="Enter Description"><?= @$data['product_description']; ?></textarea>
                                                <span id="description_error" class="custom_error error" style="display: none;">
                                                  This field is required.
                                                </span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                              </div>
                              
                              <div class="clearfix"></div>
                               <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row form-row">
                                        <div class="col-md-3">
                                            <label class="form-label">Product Note</label>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-with-icon right controls">
                                                <i class=""></i>
                                                <textarea name="product_note" type="textarea" id="myeditor1" class="editor form-control" placeholder="Enter Note"><?= @$data['product_note']; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>

                               

                              <div class="clearfix"></div>                              
                              <div class="col-md-offset-3 col-md-6 ">
                                <div class="form-group">
                                    <?php 
                                      if(!empty($data['product_id']) && @$data['product_id'] != null){
                                        $appendedFiles = array();
                                        foreach($images as $img){
                                            //$data['product_image'];
                                            $file_path = dir_path().$image_upload_dir.'/'.@$img['image'];
                                              /*if (!is_dir($file_path) && file_exists($file_path)) {*/
                                              // $current_img = site_url($image_upload_dir."/".$categoryData['image']);
                                              @$file_details = getimagesize(site_url($image_upload_dir."/".@$img['image']));
                                              $appendedFiles[] = array(
                                                "name" => @$img['image'],
                                                "type" => $file_details['mime'],
                                                "size" => @filesize($file_path),
                                                "file" => site_url($image_upload_dir."/". @$img['image']),
                                                "data" => array(
                                                "url" => site_url($image_upload_dir."/". @$img['image']),
                                                "image_file_id" => $data['product_id']
                                                )
                                              );
                                              //}

                                        }
                                        $appendedFiles = json_encode($appendedFiles);
                                      }
                                    ?>
                                  <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files='<?php echo @$appendedFiles;?>'>
                                </div>
                              </div>

                              <div class="clearfix"></div>
                        <?php  if (isset($price_plan) && @$price_plan != null) { ?>
                        <div class="form-group" >
                            <div class="row" >
                                <div class="col-md-12">
                                    <h3>Price List</h3>
                                  <div class="" >
                                   <div class="grid-body dataTables_wrapper-1440 " >
                                    <table class="table" >
                                        <thead>
                                            <tr class="priceplan_dt-controls">
                                                <th class="price_plan-dt-th1">Price Id</th>
                                                <th class="price_plan-dt-th2">Description</th>
                                                <th class="price_plan-dt-th3" colspan="3">Retail Price</th>
                                                <th class="price_plan-dt-th4" colspan="3">Reseller Price</th>
                                                <th class="price_plan-dt-th5" colspan="3">Partner Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="priceplan_dt-controls">
                                                <td class="price_plan-dt-td1" colspan="2"></td>
                                                <td class="price_plan-dt-td2">AED</td>
                                                <td class="price_plan-dt-td3">USD</td>
                                                <td class="price_plan-dt-td4">Max Discount in %</td>
                                                <td class="price_plan-dt-td5">AED</td>
                                                <td class="price_plan-dt-td6">USD</td>
                                                <td class="price_plan-dt-td7">Max Discount in %</td>
                                                <td class="price_plan-dt-td8">AED</td>
                                                <td class="price_plan-dt-td9">USD</td>
                                                <td class="price_plan-dt-td10">Max Discount in %</td>
                                            </tr>
                                            <?php
                                                $count = 1;
                                                foreach($price_plan as $k => $v) {
                                            ?>
                                                <tr class="txt-align-control">
                                                    <td>
                                                       <?= $v['plan_slog']; ?>
                                                       <input type="hidden" name="plan_id[]" value="<?= $v['price_plan_id']; ?>" />
                                                       <input type="hidden" name="price_id[]" value="<?= @$v['id']; ?>" />
                                                    </td>
                                                    <td>
                                                       <?= $v['plan_description']; ?>
                                                    </td>
                                                    <td>
                                                        <input name="retail_base_price[]" id="amount_deci1" type="text" value="<?= empty($v['retail_base_price'])? int_to_float(0): int_to_float($v['retail_base_price']); ?>" class="form-control" placeholder="AED Price"/>
                                                    </td>
                                                    <td>
                                                        <input name="retail_usd_price[]" id="amount_deci2" type="text" value="<?= empty($v['retail_usd_price'])? int_to_float(0) : int_to_float($v['retail_usd_price']); ?>" class="form-control" placeholder="USD Price"/>
                                                    </td>
                                                    <td>
                                                         <input name="retail_discount[]" type="text" value="<?= empty($v['retail_discount'])? 0 : $v['retail_discount']; ?>" class="form-control" placeholder="Discount"/>
                                                    </td>
                                                    <td>
                                                        <input name="reseller_base_price[]" id="amount_deci1" type="text" value="<?= empty($v['reseller_base_price'])? int_to_float(0) : int_to_float($v['reseller_base_price']); ?>" class="form-control" placeholder="AED Price"/>
                                                    </td>
                                                    <td>
                                                         <input name="reseller_usd_price[]" id="amount_deci2" type="text" value="<?= empty($v['reseller_usd_price'])? int_to_float(0) : int_to_float($v['reseller_usd_price']); ?>" class="form-control" placeholder="USD Price"/>
                                                    </td>
                                                    <td>
                                                         <input name="reseller_discount[]" type="text" value="<?= empty($v['reseller_discount'])? int_to_float(0) : int_to_float($v['reseller_discount']); ?>" class="form-control" placeholder="Discount"/>
                                                    </td>
                                                     <td>
                                                        <input name="partner_base_price[]" id="amount_deci1" type="text" value="<?= empty($v['partner_base_price']) ? int_to_float(0) : int_to_float($v['partner_base_price']); ?>" class="form-control" placeholder="AED Price"/>
                                                    </td>
                                                    <td>
                                                        <input name="partner_usd_price[]" id="amount_deci2" type="text" value="<?= empty($v['partner_usd_price'])? int_to_float(0) : int_to_float($v['partner_usd_price']); ?>" class="form-control" placeholder="USD Price"/>
                                                    </td>
                                                    <td>
                                                        <input name="partner_discount[]" type="text" value="<?= empty($v['partner_discount'])? int_to_float(0) : int_to_float($v['partner_discount']); ?>" class="form-control" placeholder="Discount"/>
                                                    </td>
                                                </tr>
                                            <?php
                                                $count++; 
                                                }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>

                                  </div>  
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                              <div class="col-md-12 ">
                                <div class="form-group text-center">
                                  <!-- <button class="btn btn-success btn-cons pull-right" type="submit"><?php // ($page_title == 'add')?'Add New':'Edit'; ?> Product</button> -->
                                  <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                      <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                  <input name="id" type="hidden" value="<?= @$data['product_id']; ?>">
                                  <input type="hidden" id="cat_type" value="">
                                  <input type="hidden" name="image_old" value="<?= @$data['product_image']; ?>">
                                </div>
                              </div>
                            </div>

                        </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {
        $('#fileUploader').fileuploader({
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-inner">' +
                '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                '<p>or</p>' +
                '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            //limit: 100,
            addMore: true,
            extensions: ['jpg', 'jpeg', 'png'],
               onRemove: function(item) {
                 $.post('<?= site_url('product/imageDelete'); ?>', {
                 file: item.name,
                 data: {
                   image_file_id:"<?= @$data['product_id']; ?>",
                   file:item.name,
                   image_post_file_id:item.data.image_file_id
                 }
                 });
               },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            },
        });


        $("#discount_status").change(function () {
          var val = $(this).val();
          if (val == 1) {
            $('.discount_status_type').slideDown();
          }else{
            $('.discount_status_type').slideUp();
          }
        });

       
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });

    });
</script>
<script type="text/javascript">

    $(document).ready(function() { 
        setTimeout(function(){
            //$('.category_type').trigger('change');     
            //$('.warranty_coverage').trigger('change');     
          }, 500);  
        $("form.validate").validate({
           focusInvalid: true,
            rules: {
                product_name: {
                    required: true
                },
                product_sku: {
                    required: true
                },
                category_id: {
                    required: true
                },
                product_is_active: {
                    required: true,
                },
                min_quantity:{
                   required: true,
                  /* digits: true*/
                },
                can_deliver:{
                   required: true,
                },
                can_invoice:{
                   required: true,
                },
                warranty_coverage:{
                   required: true,
                },
                chargeable:{
                    required: true,
                    digits: true
                },
                warranty_days:{
                    required: true
                },
                warranty_coverage:{
                    required: true
                },
                description:{
                         required:true
                    }
            },
            messages: {
                product_name: "This field is required.",
                product_sku: "This field is required.",
                product_price:{
                    required: "This field is required.",
                    number: "Please Insert Number"
                },
                product_discount_amount:{
                    required: "This field is required.",
                    number: "Please Insert Number"
                },
                product_discount_type: "This field is required.",
                category_id: "This field is required.",
                product_is_active: "This field is required."
            },
            invalidHandler: function(event, validator) {
                error("Please input all the mandatory values marked as red");
                //display error alert on form submit  
                /* if(ckEditor.getData() == ""){
                   $("#description_error").show();
                    //setTimeout(function() { $("#description_error").hide(); }, 5000);
                  return false;
                }  */
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
        $("#amount_deci1,#amount_deci2").change(function() {
            var $this = $(this);
            $this.val(parseFloat($this.val()).toFixed(2));        
        });
        $("#amount_deci1,#amount_deci2").change(function() {
            var $this = $(this);
            if($.isNumeric($this.val())){
                $this.val(parseFloat($this.val()).toFixed(2));        
            }else{
               $this.val(parseFloat(0).toFixed(2)); 
            }
        });
    });

    $(".only-numeric").bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode
        if (!(keyCode >= 48 && keyCode <= 57)) {
          return false;
        }
    });
   
    //warranty
    $('.warranty_coverage').on('change', function() {
      $('#warranty_days').val('0');
      if(this.value == '0'){
        $('#warranty_days').val('365');
        $('#warranty_days').prop('readonly', false);
      }else if(this.value == '1'){
        $('#warranty_days').val('0');
        $('#warranty_days').prop('readonly', true);
        //$('.warranty_days').hide();
      }
    });

    /*$('.category').on('change', function() {
        $.ajax({
          url: "<?php echo site_url('category/category_type'); ?>",
          dataType: "json",
          type: "POST",
          data: {id: this.value} ,
          cache: false,
          success: function(data) {
            var type = data.data[0].title;
            if(type == 'Service'){
              $('#min_quantity').val('lot');
            }
            else{
              $('#min_quantity').val('0');
            }
          }
        });
    });*/

    $('.category_type').on('change', function() {
        var type = $(this).val();
        if(type == '1'){
        console.log("type "+type);
          $('#min_quantity').val('lot');
        }
        else{
          $('#min_quantity').val('0');
        }
        if(type == '1' || type == '3'){
          $('#min_quantity').prop('readonly', true);
          $("#can_deliver").select2("readonly", true);
          $("#can_deliver").val(1);
          $("#can_deliver").trigger("change");
        }
        else{
          $('#min_quantity').prop('readonly', false);
          $("#can_deliver").select2("readonly", false);
          $("#can_deliver").val(0);
          $("#can_deliver").trigger("change");

        }
    });
    
    
</script>