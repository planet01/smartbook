<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Settings
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':''; ?>
                        <?= (isset($page_heading))?$page_heading:''; ?>
                </a>
            </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Name</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="setting_company_name" type="text" value="<?= @$data['setting_company_name']; ?>" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 my-float-style">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Company Prefix</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="company_prefix" type="text" value="<?= @$data['company_prefix']; ?>" class="form-control" placeholder="Company Prefix">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">T.R.No</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="setting_company_reg" type="text" value="<?= @$data['setting_company_reg']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        
                                      <div class="col-md-4">
                                          <div class="form-group">
                                              <div class="row form-row">
                                                  <div class="col-md-12">
                                                      <label class="form-label">Tax %</label>
                                                  </div>
                                                  <div class="col-md-12">
                                                      <div class="input-with-icon right controls">
                                                          <i class=""></i>
                                                          <input name="tax" type="text" value="<?= @$data['tax']; ?>" class="only-numeric form-control" placeholder="">
                                                          <span id="error"></span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                                  
                               
                                <div class="clearfix"></div>
                                
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Delivery Prefix</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="delivery_prfx" type="text" value="<?= @$data['delivery_prfx']; ?>" class="form-control" placeholder="">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Quotation Prefix</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="quotation_prfx" type="text" value="<?= @$data['quotation_prfx']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!--<div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Proforma Invoice Prefix </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="proforma_inv_prfx" type="text" value="<?= @$data['proforma_inv_prfx']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                         <input name="proforma_inv_prfx" type="hidden" value="PI" class="form-control" placeholder=""/>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Receivable Prefix </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="receivable_prefix" type="text" value="<?= @$data['receivable_prefix']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Invoice Prefix</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="invoice_prfx" type="text" value="<?= @$data['invoice_prfx']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        
                                    </div>
                                </div>

                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <div class="row form-row my-col-width">
                                            <div class="col-md-12">
                                                <label class="form-label">Address</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <!-- <input name="setting_company_address" type="textarea" value="<?= @$data['setting_company_address']; ?>" class="form-control" placeholder=""> -->
                                                    <textarea name="setting_company_address" type="textarea" class="form-control txtarea-width" placeholder=""><?= @$data['setting_company_address']; ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>        
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">M.R.N. Prefix</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="material_prfx" type="text" value="<?= @$data['material_prfx']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">M.I.N. Prefix</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="material_issue_prefix" type="text" value="<?= @$data['material_issue_prefix']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                  

                               

                                <!-- <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Currency</label>
                                                </div>
                                                <div class="col-md-12">
                                                    <select name="currency_id" style="width:100%" id="currency_id" class="form-control currency_id" required>
                                                        <option selected disabled>--- Select Currency ---</option>
                                                        <?php foreach($currency as $k => $v){ ?>
                                                        <option  <?=( @$data['currency_id'] == $v['id'] )? 'selected ': '' ?>
                                                        value="<?= $v['id']; ?>">
                                                            <?= $v['title']; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --> 
                                     <!-- <div class="clearfix"></div> -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row form-row">
                                                <div class="col-md-12">
                                                    <label class="form-label">Currency</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="currency_id" style="width:100%" id="currency_id" class="form-control currency_id" required>
                                                        <option selected disabled>--- Select Currency ---</option>
                                                        <?php foreach($currency as $k => $v){ ?>
                                                        <option  <?=( @$data['currency_id'] == $v['id'] )? 'selected ': '' ?>
                                                        value="<?= $v['id']; ?>">
                                                            <?= $v['title']; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-6">
                                                        <label class="form-label">Country:</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="setting_company_country" name="setting_company_country">
                                                                <option value="" selected="" disabled="">-- Select Country --</option>
                                                                <?php
                                                                foreach ($countries as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['country_id'] ?>" <?=( $v['country_id']==@ $data['setting_company_country']) ? 'selected' : ''; ?> >
                                                                        <?= $v['country_name']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Packing Prefix</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="packing_prfx" type="text" value="<?= @$data['packing_prfx']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">P.O.#</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="po_no" type="text" value="<?= @$data['po_no']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Receivable A/c</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="receivable_account" name="receivable_account">
                                                                <option value="" selected="" disabled="">-- Select Receivable A/c --</option>
                                                                <?php
                                                                foreach ($accounts as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['AccountNo'] ?>" <?=( $v['AccountNo']==@ $data['receivable_account']) ? 'selected' : ''; ?> >
                                                                        <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Tax Receivable A/c</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="tax_receivable_account" name="tax_receivable_account">
                                                                <option value="" selected="" disabled="">-- Select Tax Receivable A/c --</option>
                                                                <?php
                                                                foreach ($accounts as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['AccountNo'] ?>" <?=( $v['AccountNo']==@ $data['tax_receivable_account']) ? 'selected' : ''; ?> >
                                                                        <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Tax Payable A/c</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="tax_payable_account" name="tax_payable_account">
                                                                <option value="" selected="" disabled="">-- Select Tax Payable A/c --</option>
                                                                <?php
                                                                foreach ($accounts as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['AccountNo'] ?>" <?=( $v['AccountNo']==@ $data['tax_payable_account']) ? 'selected' : ''; ?> >
                                                                        <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Revenue A/c</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="revenue_account" name="revenue_account">
                                                                <option value="" selected="" disabled="">-- Select Revenue A/c --</option>
                                                                <?php
                                                                foreach ($accounts as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['AccountNo'] ?>" <?=( $v['AccountNo']==@ $data['revenue_account']) ? 'selected' : ''; ?> >
                                                                        <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="clearfix"></div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Tax Provision A/c</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="tax_provision_account" name="tax_provision_account">
                                                                <option value="" selected="" disabled="">-- Select Tax Provision A/c --</option>
                                                                <?php
                                                                foreach ($accounts as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['AccountNo'] ?>" <?=( $v['AccountNo']==@ $data['tax_provision_account']) ? 'selected' : ''; ?> >
                                                                        <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Bank A/c</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="bank_account" name="bank_account">
                                                                <option value="" selected="" disabled="">-- Select Bank A/c --</option>
                                                                <?php
                                                                foreach ($accounts as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['AccountNo'] ?>" <?=( $v['AccountNo']==@ $data['bank_account']) ? 'selected' : ''; ?> >
                                                                        <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Adjustment A/c</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <select class="form-control select2" id="adjustment_account" name="adjustment_account">
                                                                <option value="" selected="" disabled="">-- Select Adjustment A/c --</option>
                                                                <?php
                                                                foreach ($accounts as $k => $v) {
                                                                ?>
                                                                    <option value="<?= $v['AccountNo'] ?>" <?=( $v['AccountNo']==@ $data['adjustment_account']) ? 'selected' : ''; ?> >
                                                                        <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Ticket Prefix</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input type="text" class="form-control" name="ticket_prfx" value="<?= $data['ticket_prfx']?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                         <!--<div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row form-row">
                                                    <div class="col-md-12">
                                                        <label class="form-label">Receivable Prefix</label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="input-with-icon right controls">
                                                            <i class=""></i>
                                                            <input name="receivable_prefix" type="text" value="<?= @$data['receivable_prefix']; ?>" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->

                                <div class="clearfix"></div>
                                
                                <div class="col-md-12">
                                  <h3>Bank Account Details</h3>
                                  <div class="dataTables_wrapper-768">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th class="text-center setting_bank_acc_details-dt-th1" >Add/Remove Row</th>
                                          <th class="text-center setting_bank_acc_details-dt-th2" >Bank</th>
                                          <th class="text-center setting_bank_acc_details-dt-th3" >Account No</th>
                                          <th class="text-center setting_bank_acc_details-dt-th4" >IBAN</th>
                                          <th class="text-center setting_bank_acc_details-dt-th5" >Branch</th>
                                          <th class="text-center setting_bank_acc_details-dt-th6" >Swift Code</th>
                                        </tr>
                                      </thead>
                                      <tbody id="customFields">
                                        <tr class="txtMult">
                                          <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                          <td colspan="6"></td>
                                        </tr>

                                         <?php
                                         $total_quantity = 0;
                                         // print_b($material_receive_note_detail_data);
                                         ?>
                                        
                                          <?php
                                                /*$total_quantity += $v['material_receive_note_detail_quantity']; 
                                            }*/
                                          if(@$data != null && !empty(@$data)){
                                          foreach(@$bank as $row){
                                         ?>

                                         <tr class="txtMult">
                                                <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                <td width="150px">
                                                    <input type="text" class="bank" required style="width:100%" id="bank"  name="bank[]" 
                                                    value="<?= @$row['bank'] ?>" placeholder="Bank" />
                                                </td>
                                                <td width="100px">
                                                    <input type="text" class="account_no" required style="width:100%" id="account_no'+x+'" data-optional="0" name="account_no[]" 
                                                    value="<?= @$row['account_no'] ?>" placeholder="Account Mo" />
                                                </td >
                                                <td width="150px">
                                                    <input type="text" class="iban" required style="width:100%" id="iban'+x+'" data-optional="0" name="iban[]" 
                                                    value="<?= @$row['iban'] ?>" placeholder="IBAN" />
                                                </td>
                                                <td width="150px">
                                                    <input type="text" class="branch" required style="width:100%" id="branch'+x+'" data-optional="0" name="branch[]" 
                                                    value="<?= @$row['branch'] ?>" placeholder="Branch" />
                                                </td>
                                                <td width="130px">
                                                    <input type="text" class="swift" required style="width:100%" id="swift'+x+'" data-optional="0" name="swift_code[]" 
                                                    value="<?= @$row['swift_code'] ?>" placeholder="Swift Code" />
                                                </td>
                                            </tr>
                                        <?php } } ?>

                                      </tbody>
                                    </table>
                                  </div>  
                                </div>
                            
                                <div class="clearfix"></div>
                                
                                <div class="col-md-4 onerow">
                                    <h4>Header Image (Letter Head)</h4>
                                    <div class="form-group">
                                        <?php 
                                          if(!empty($data['header_image']) && @$data['header_image'] != null){
                                            $appendedFiles = array();
                                            $file_path = dir_path().$image_upload_dir.'/'.@$data['header_image'];
                                              if (!is_dir($file_path) && file_exists($file_path)) {
                                              // $current_img = site_url($image_upload_dir."/".$categoryData['image']);
                                              @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['header_image']));
                                              $appendedFiles[] = array(
                                                "name" => @$data['header_image'],
                                                "type" => $file_details['mime'],
                                                "size" => filesize($file_path),
                                                "file" => site_url($image_upload_dir."/". @$data['header_image']),
                                                "data" => array(
                                                "url" => site_url($image_upload_dir."/". @$data['header_image']),
                                                "image_file_id" => 'header_image'
                                                )
                                              );
                                              }
                                            $appendedHeaderFiles = json_encode($appendedFiles);
                                          }
                                        ?>
                                      <input type="file" class="form-control" name="header_image" id="header_image" multiple="" data-fileuploader-files='<?php echo @$appendedHeaderFiles;?>'>
                                    </div>
                                </div>
                                <div class="col-md-4 onerow">
                                    <h4>Footer Image (Letter Head)</h4>
                                    <div class="form-group">
                                        <?php 
                                          if(!empty($data['footer_image']) && @$data['footer_image'] != null){
                                            $appendedFiles = array();
                                            $file_path = dir_path().$image_upload_dir.'/'.@$data['footer_image'];
                                              if (!is_dir($file_path) && file_exists($file_path)) {
                                              // $current_img = site_url($image_upload_dir."/".$categoryData['image']);
                                              @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['footer_image']));
                                              $appendedFiles[] = array(
                                                "name" => @$data['footer_image'],
                                                "type" => $file_details['mime'],
                                                "size" => filesize($file_path),
                                                "file" => site_url($image_upload_dir."/". @$data['footer_image']),
                                                "data" => array(
                                                "url" => site_url($image_upload_dir."/". @$data['footer_image']),
                                                "image_file_id" =>"footer_image"
                                                )
                                              );
                                              }
                                            $appendedFooterFiles = json_encode($appendedFiles);
                                          }
                                        ?>
                                      <input type="file" class="form-control" name="footer_image" id="footer_image" multiple="" data-fileuploader-files='<?php echo @$appendedFooterFiles;?>'>
                                    </div>
                                </div>
                                <div class="col-md-4 onerow">
                                    <h4>Company Logo</h4>
                                    <div class="form-group">
                                        <?php 
                                          if(!empty($data['logo']) && @$data['logo'] != null){
                                            $appendedFiles = array();
                                            $file_path = dir_path().$image_upload_dir.'/'.@$data['logo'];
                                              if (!is_dir($file_path) && file_exists($file_path)) {
                                              // $current_img = site_url($image_upload_dir."/".$categoryData['image']);
                                              @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['logo']));
                                              $appendedFiles[] = array(
                                                "name" => @$data['logo'],
                                                "type" => $file_details['mime'],
                                                "size" => filesize($file_path),
                                                "file" => site_url($image_upload_dir."/". @$data['logo']),
                                                "data" => array(
                                                "url" => site_url($image_upload_dir."/". @$data['logo']),
                                                "image_file_id" => "logo"
                                                )
                                              );
                                              }
                                            $appendedLogoFiles = json_encode($appendedFiles);
                                          }
                                        ?>
                                      <input type="file" class="form-control" name="logo" id="logo" multiple="" data-fileuploader-files='<?php echo @$appendedLogoFiles;?>'>
                                    </div>
                                </div>
                                <div class="col-md-12 onerow">
                                    <h4>Email Footer image</h4>
                                    <textarea name="email_footer" id="myeditor" class="editor form-control" placeholder="Enter Description" ><?= @$data['email_footer'];?></textarea>
                                    <!-- <div class="form-group">
                                        <?php 
                                          if(!empty($data['email_footer']) && @$data['email_footer'] != null){
                                            $appendedFiles = array();
                                            $file_path = dir_path().$image_upload_dir.'/'.@$data['email_footer'];
                                              if (!is_dir($file_path) && file_exists($file_path)) {
                                              // $current_img = site_url($image_upload_dir."/".$categoryData['image']);
                                              @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['email_footer']));
                                              $appendedFiles[] = array(
                                                "name" => @$data['email_footer'],
                                                "type" => $file_details['mime'],
                                                "size" => filesize($file_path),
                                                "file" => site_url($image_upload_dir."/". @$data['email_footer']),
                                                "data" => array(
                                                "url" => site_url($image_upload_dir."/". @$data['email_footer']),
                                                "image_file_id" => "email_footer"
                                                )
                                              );
                                              }
                                            $appendedEmailFiles = json_encode($appendedFiles);
                                          }
                                        ?>
                                      <input type="file" class="form-control" name="email_footer" id="email_footer" multiple="" data-fileuploader-files='<?php echo @$appendedEmailFiles;?>'>
                                    </div> -->
                                </div>
                            

                                <div class="col-md-12 mt-3">
                                    <div class="form-group text-center">
                                        <!--    <button class="btn btn-success btn-cons pull-right" type="submit"><?php // ($page_title == 'add')?'Add New':'Edit'; ?> Product</button> -->
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['setting_id']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {
        $('#header_image, #footer_image, #logo, #email_footer').fileuploader({
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-inner">' +
                '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                '<p>or</p>' +
                '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
               onRemove: function(item) {
                 $.post('<?= site_url('setting/delete'); ?>', {
                 file: item.name,
                 data: {
                   image_file_id:"<?= @$data['product_id']; ?>",
                   file:item.name,
                   image_post_file_id:item.data.image_file_id
                 }
                 });
               },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            },
        });
   
    });
    $(document).ready(function() {

        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });

    });
    $(document).ready(function() {
        jQuery.validator.addMethod("specialChars", function( value, element ) {
            var regex = new RegExp("^[a-zA-Z0-9-. ]+$");
            var key = value;

            if (!regex.test(key)) {
               return false;
            }
            return true;
        }, "Only alphanumeric & (-) OR (.) value allowed.");

        $("form.validate").validate({
            rules: {
                setting_company_name: {
                    required: true
                },
                setting_company_address: {
                    required: true
                },
                setting_company_postal: {
                    required: true
                },
                setting_company_city: {
                    required: true
                },
                setting_company_country: {
                    required: true
                },
                setting_company_reg: {
                    specialChars: true,
                    required: true,
                },
                setting_company_bank_ac_no: {
                    required: true
                },
                setting_company_bank_branch_name: {
                    required: true
                },
                quotation_prfx: {
                    specialChars: true,
                    required: true
                },
                invoice_prfx: {
                    specialChars: true,
                    required: true
                },
                delivery_prfx: {
                    specialChars: true,
                    required: true
                },
                material_prfx: {
                    specialChars: true,
                    required: true
                },
                po_no: {
                    specialChars: true,
                    required: true
                },
                receivable_account: {
                    required: true
                },
                tax_receivable_account: {
                    required: true
                },
                tax_payable_account: {
                    required: true
                },
                revenue_account: {
                    required: true
                },
                tax_provision_account: {
                    required: true
                },
                bank_account: {
                    required: true
                },
                adjustment_account:{
                    required: true
                },
                company_prefix: {
                    specialChars: true,
                    required: true
                },
                ticket_prfx: {
                    required: true
                },
                "bank[]":{
                    specialChars: true,
                    required:true
                },
                "iban[]":{
                    specialChars: true,
                    required:true
                },
                "swift_code[]":{
                    specialChars: true,
                    required:true
                },
                "branch[]":{
                    specialChars: true,
                    required:true
                },
                "account_no[]":{
                    specialChars: true,
                    required:true
                },
            },
            messages: {
                setting_company_name: "This field is required.",
                setting_company_address: "This field is required.",
                setting_company_postal: "This field is required.",
                setting_company_city: "This field is required.",
                setting_company_country: "This field is required.",
                //setting_company_reg: "This field is required.",
                //setting_company_bank_ac_no: "This field is required.",
                //setting_company_bank_branch_name: "This field is required.",
                //quotation_prfx: "This field is required.",
                //invoice_prfx: "This field is required.",
                //delivery_prfx: "This field is required.",
                //material_prfx: "This field is required."
            },
            invalidHandler: function(event, validator) {
                error("Please input all the mandatory values marked as red");
                //display error alert on form submit    
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    $(document).ready(function(){
        var max_fields      = 6; 
        var add_button      = $("#customFields .addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td width='150px'>";
            temp    += '<input type="text" class="bank" required  style="width:100%" id="bank'+x+'" data-optional="0" name="bank[]" value="" placeholder="" />';
            temp    += '</td>';
             temp    += "<td width='100px'>";
            temp    += '<input type="text" class="code quantity txtboxToFilter account_no" required  style="width:100%" id="account_no'+x+'" data-optional="0" name="account_no[]" value="" placeholder="" />';
            temp    += '</td>';
             temp    += "<td width='150px'>";
            temp    += '<input type="text" class="code quantity txtboxToFilter iban" required  style="width:100%" id="iban'+x+'" data-optional="0" name="iban[]" value="" placeholder="" />';
            temp    += '</td>';
             temp    += "<td width='150px'>";
            temp    += '<input type="text" class="code quantity txtboxToFilter branch" required  style="width:100%" id="branch'+x+'" data-optional="0" name="branch[]" value="" placeholder="" />';
            temp    += '</td>';
             temp    += "<td width='130px'>";
            temp    += '<input type="text" class="code quantity txtboxToFilter swift_code" required  style="width:100%" id="swift_code'+x+'" data-optional="0" name="swift_code[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#customFields").append(temp);
          // }
        });
        $("#customFields").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });


        $(".only-numeric").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#error").html("Digits Only").css("color","red").show().fadeOut("slow");
               return false;
        }
        });
        $('.only-numeric').on('input', function () {
        var value = $(this).val();
        if ((value !== '') && (value.indexOf('.') === -1)) {
            $("#error").html("Not More Than 100%").show().fadeOut("slow");
            $(this).val(Math.max(Math.min(value, 100), -100));
        }
        });
      
    });



</script>
