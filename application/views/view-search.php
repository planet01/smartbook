<!--
  <script src="https://code.jquery.com/jquery-2.0.3.min.js" data-semver="2.0.3" data-require="jquery@2.0.3"></script>
-->

<div class="page-content">
  <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  <div class="content">
    <ul class="breadcrumb">
      <li>
        <p>Dashboard</p>
      </li>
      <li><a href="<?php $_SERVER['HTTP_REFERER']; ?>" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
    </ul>
    <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
    <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
    <!--</div>-->
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple ">
          <div class="grid-title">
            <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
            <div class="pull-right">
              <!--    <a href="<?php echo site_url($add_product) ?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>
                <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>
                <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>
                -->
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>
                <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>
                <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>
              </ul>
              <span class="h-seperate"></span>
              <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>
            </div>
          </div>
          <div class="grid-body ">
            <!--              <table class="table" id="example3" >
    -->
            <table class="table" id="dt" width="100%">
              <thead>

                <th>Product Image</th>
                <th>Product Name</th>
                <th>Product SKU</th>
                <th>Location Name</th>
                <th>Location Address</th>
                <th>Quantity</th>

              </thead>
              <tbody>

                <?php if (isset($data) && @$data != null) {

                  foreach (@$data as $v) {
                    
                      $quantity = inventory_quantity_by_warehouse($v['product_id'], $v['warehouse_id']);
                      
                     
                      if (!empty($quantity)) {
                      if (isset($warefilter)) {
                          if ($warefilter == $v['warehouse_id']) {
                              ?>
                        <tr>
                          <td>
                            <?php
                            $current_img = '';
                              $file_path = dir_path() . $image_upload_dir . '/' . @$v['product_image'];
                              if (!is_dir($file_path) && file_exists($file_path)) {
                                  $current_img = site_url($image_upload_dir . '/' . $v['product_image']);
                              } else {
                                  $current_img = site_url("uploads/dummy.jpg");
                              } ?>
                            <div class="clearfix"></div>

                            <div class="controls">
                              <img src="<?= $current_img; ?>" width="50" />
                            </div>

                          </td>

                          <td>
                            <?php if (empty($v['product_name'])) {
                                  echo "No Name Found";
                              } else {
                                  echo $v['product_name'];
                              } ?>
                          </td>

                          <td>
                            <?php if (empty($v['p_model'])) {
                                  echo "No SKU Found";
                              } else {
                                  echo $v['p_model'];
                              } ?>
                          </td>

                          <td>
                            <?php if (empty($v['warehouse_name'])) {
                                  echo "No Ware House Found";
                              } else {
                                  echo $v['warehouse_name'];
                              } ?>
                          </td>

                          <td>
                            <?php if (empty($v['warehouse_address'])) {
                                  echo "No Address Found";
                              } else {
                                  echo $v['warehouse_address'];
                              } ?>
                          </td>

                          <td>
                            <?php
                            if (empty($quantity)) {
                                echo "No Quantity Found";
                            } else {
                                echo $quantity;
                            } ?>
                          </td>

                          <!-- <td class="">
                          <a href="#" class="btn-primary btn btn-sm myModalBtn" data-id="<?= @$v['id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                          <a href="<?php echo site_url($edit_product . '/' . @$v['id']) ?>" class="btn-warning btn btn-sm"><i class="fa fa-pencil"></i></a>
                          <a href="<?php echo site_url($delete_product . '/' . @$v['id']) ?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a>
                        </td> -->
                        </tr>
                      <?php
                          } else {
                          }
                      } else {  ?>
                      <tr>
                        <td>
                          <?php
                          $current_img = '';
                          $file_path = dir_path() . $image_upload_dir . '/' . @$v['product_image'];
                          if (!is_dir($file_path) && file_exists($file_path)) {
                              $current_img = site_url($image_upload_dir . '/' . $v['product_image']);
                          } else {
                              $current_img = site_url("uploads/dummy.jpg");
                          }
                          ?>
                          <div class="clearfix"></div>

                          <div class="controls">
                            <img src="<?= $current_img; ?>" width="50" />
                          </div>

                        </td>
                        <td>
                          <?php if (empty($v['product_name'])) {
                              echo "No Name Found";
                          } else {
                              echo $v['product_name'];
                          }
                          ?>
                        </td>


                        <td>
                          <?php if (empty($v['p_model'])) {
                              echo "No SKU Found";
                          } else {
                              echo $v['p_model'];
                          }
                          ?>
                        </td>
                        <td>
                          <?php if (empty($v['warehouse_name'])) {
                              echo "No Ware House Found";
                          } else {
                              echo $v['warehouse_name'];
                          }
                          ?>
                        </td>
                        <td>
                          <?php if (empty($v['warehouse_address'])) {
                              echo "No Address Found";
                          } else {
                              echo $v['warehouse_address'];
                          }
                          ?>
                        </td>

                        <td>
                          <?php
                          if (empty($quantity)) {
                              echo "No Quantity Found";
                          } else {
                              echo $quantity;
                          }
                          ?>
                        </td>
                        <!-- <td class="">
                          <a href="#" class="btn-primary btn btn-sm myModalBtn" data-id="<?= @$v['id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                          <a href="<?php echo site_url($edit_product . '/' . @$v['id']) ?>" class="btn-warning btn btn-sm"><i class="fa fa-pencil"></i></a>
                          <a href="<?php echo site_url($delete_product . '/' . @$v['id']) ?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a>
                        </td> -->
                      </tr>
                <?php  }
                  }
                  
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div>
        <!--		
  <input onchange="filterme()" type="checkbox" name="type" value="Tes">Tes
  <input onchange="filterme()" type="checkbox" name="type" value="MX">MX
  <input onchange="filterme()" type="checkbox" name="type" value="A">A
  <input onchange="filterme()" type="checkbox" name="type" value="CNAME">CNAME
  <hr>
  <input onchange="filterme()" type="radio" name="free" value="0">0
  <input onchange="filterme()" type="radio" name="free" value="1">1
    <input onchange="filterme()" type="radio" name="free" value="">All -->
      </div>

    </div>
  </div>
  <!-- Modal -->
  <?php include APPPATH . 'views/include/modal.php'; ?>
  <!-- /.modal -->
</div>




<script>
  $(document).ready(function() {
    $('#dt').DataTable();
  });
  // Code goes here
</script>