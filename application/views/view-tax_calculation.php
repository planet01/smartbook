<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Tax Calculation
            </li>
            <li><a href="#" class="active">View All
                    <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
                </a> </li>
        </ul>
        <?php
        $check_add = false;
        $check_edit = false;
        $check_status = false;
        $check_print = false;
        $check_history = false;
        if ($this->user_type == 2) {
            foreach ($this->user_role as $k => $v) {
                if ($v['module_id'] == 28) {
                    if ($v['add'] == 1) {
                        $check_add = true;
                    }
                    if ($v['edit'] == 1) {
                        $check_edit = true;
                    }
                    if ($v['status'] == 1) {
                        $check_status = true;
                    }
                    if ($v['print'] == 1) {
                        $check_print = true;
                    }
                    if ($v['history'] == 1) {
                        $check_history = true;
                    }
                }
            }
        } else {
            $check_add = true;
            $check_edit = true;
            $check_status = true;
            $check_print = true;
            $check_history = true;
        }
        ?>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
                        <?php if ($check_add) { ?>
                            <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('Tax_calculation/add'); ?>">Add</a>
                        <?php } ?>
                    </div>
                    <div class="grid-body ">
                        <div class="row">
                            <form class="validate">
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>From Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="from_date" id="from_date">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>To Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="to_date" id="to_date" >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>Remarks</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control" type="text" name="detail" id="detail" >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <table class="table" id="custom_table">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Tax No</th>
                                    <th>Date</th>
                                    <th>Remarks</th>
                                    <th>Period</th>
                                    <th>Tax Payable</th>
                                    <th>Payment Recieved</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach (@$data as $k => $v) {
                                ?>
                                        <tr class="view-cust_dt">
                                            <td class="align-middle">
                                                <?php echo $count; ?>
                                            </td>

                                            <td class="align-middle">
                                                <?php echo @$setval["company_prefix"] . 'TX-' . $v['tax_no'] ?>
                                            </td>

                                            <td class="align-middle">
                                                <?php echo $v['tax_date'] ?>
                                            </td>

                                            <td class="align-middle">
                                                <?php echo $v['remarks'] ?>
                                            </td>

                                            <td class="align-middle">
                                                <?php echo $v['start_date'] ?> - <?= $v['end_date'] ?>
                                            </td>
                                            <td class="align-middle">
                                                <?php echo $v['all_tax'] ?>
                                            </td>
                                            <td class="align-middle">
                                                <?= $v['paid_amount'] ?>
                                            </td>
                                            <td class="align-middle">
                                                <?php if ($v['status'] == 0) {
                                                ?>
                                                    <span class="label label-success">Draft</span>
                                                <?php } else if ($v['status'] == 1) {
                                                ?>
                                                    <span class="label label-success">Final</span>
                                                <?php
                                                } else  if ($v['status'] == 2) {
                                                ?>
                                                    <span class="label label-success">Revised</span>
                                                <?php
                                                } else {
                                                ?>
                                                    <span class="label label-success">Posted</span>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                            <td class="align-middle">
                                                <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['tax_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                                                <?php if ($check_status) {
                                                ?>
                                                <?php if ($v['status'] != 3) { ?>
                                                    <div class="btn-group mt-2">
                                                        <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                                        <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="  btn-sm ajaxBtnAlter" href="<?php echo site_url($status_product . '?tax_id=' . @$v['tax_id'] . '&status=1') ?>">Final</a></li>
                                                            <li class="divider"></li>
                                                            <li><a class="  btn-sm ajaxBtnAlter" href="<?php echo site_url($status_product . '?tax_id=' . @$v['tax_id'] . '&status=2') ?>">Revision</a></li>
                                                        </ul>
                                                    </div>
                                                    
                                                        <a href="<?php echo site_url($post_product . '/' . @$v['tax_id']) ?>" class=" btn btn-sm ajaxBtnAlter" style="background-color: #673ab7;" data-toggle="tooltip" title="Post"><b style="color: white;">Post</b></a>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                <?php if ($check_edit) {
                                                    if ($v['status'] != 3) { ?>
                                                        <a href="<?php echo site_url($edit_product . '/' . @$v['tax_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <?php }
                                                } ?>

                                                <?php if ($check_history) { ?>
                                                    <a href="#" class="btn-primary btn btn-sm myModalBtn" data-path="tax_calculation/history" data-id="<?= @$v['tax_id']; ?>" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>
                                                <?php } ?>

                                                <?php if ($check_print) { ?>
                                                    <a href="<?= site_url('Tax_calculation/print_report?id=' . @$v['tax_id'] . '&header_check=1&view=0&history=0') ?>" target='_blank' title="View Detail" data-id="<?= @$v['invoice_id']; ?>" class="btn-primary btn btn-sm"><i class="fa fa-print"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                <?php
                                        $count++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $(document).on("click", "#fetch", function() {

            var from_date = $('#from_date').val();
            if (from_date == '') {
                from_date = 0;
            }

            var to_date = $('#to_date').val();
            if (to_date == '') {
                to_date = 0;
            }

            var detail = $('#detail').val();
            $.ajax({
                url: "<?php echo site_url('Tax_calculation/search_view'); ?>",
                dataType: "json",
                type: "POST",
                data: {
                    from_date: from_date,
                    to_date: to_date,
                },
                cache: false,
                success: function(result) {
                    custom_table.clear().draw();
                    var count = 0;
                    for (var i = 0; i < result.data.length; i++) {
                        count++;
                        var html = '';
                        html += '<tr>';

                        html += '<td class="align-middle">';
                        html += count;
                        html += '</td>';

                        html += '<td class="align-middle">';
                        html += "<?= @$setval["company_prefix"] . 'TX-' ?>" + result.data[i].tax_no;
                        html += '</td>';

                        html += '<td class="align-middle">';
                        html += result.data[i].tax_date;
                        html += '</td>';

                        html += '<td class="align-middle">';
                        html +=  result.data[i].remarks;
                        html += '</td>';

                        html += '<td class="align-middle">';
                        html += result.data[i].start_date + ' - '+result.data[i].end_date;
                        html += '</td>';

                        html += '<td class="align-middle">';
                        html += result.data[i].all_tax;
                        html += '</td>';

                        html += '<td class="align-middle">';
                        html += result.data[i].paid_amount;
                        html += '</td>';

                        html += '<td class="align-middle">';
                        if (result.data[i].status == 0) {
                            html += '<span class="label label-success">Draft</span>';
                        } else if (result.data[i].status == 1) {
                            html += '<span class="label label-success">Final</span>';
                        } else  if (result.data[i].status == 2) {
                            html += '<span class="label label-success">Revised</span>';
                        } else {
                            html += '<span class="label label-success">Posted</span>';
                        }
                        html += '</td>';

                        html += '<td class="align-middle">';
                        html += '<a href="#" class="mr-1 btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="'+result.data[i].tax_id+'" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>';
                        html += '<?php if ($check_status) {?>';
                        if (result.data[i].status != 3) { 
                        html += '<div class="btn-group mt-2 mr-1">';
                        html += '<button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>';
                        html += '<button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>';
                        html += '<ul class="dropdown-menu">';
                        html += '<li><a class="  btn-sm ajaxBtnAlter" href="<?php echo site_url($status_product."?tax_id=") ?> ' +result.data[i].tax_id + '&status=1">Final</a></li>';
                        html += '<li class="divider"></li>';
                        html += '<li><a class="  btn-sm ajaxBtnAlter" href="<?php echo site_url($status_product."?tax_id=") ?> ' +result.data[i].tax_id + '&status=2">Revision</a></li>';
                        html += '</ul>';
                        html += '</div>';
                        
                            html += '<a href="<?php echo site_url($post_product . "/") ?>' +result.data[i].tax_id + '" class="mr-1 btn btn-sm ajaxBtnAlter" style="background-color: #673ab7;" data-toggle="tooltip" title="Post"><b style="color: white;">Post</b></a>';
                        }
                        html += '<?php }?>';
                        html += '<?php if ($check_edit) { ?>';
                            if (result.data[i].status != 3) {
                                html += '<a href="<?php echo site_url($edit_product . "/" ) ?>' +result.data[i].tax_id + '" class="mr-1 btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                            }
                        html += '<?php } ?>';

                        html += '<?php if ($check_history) { ?>';
                        html += '<a href="#" class="mr-1 btn-primary btn btn-sm myModalBtn" data-path="tax_calculation/history" data-id="' +result.data[i].tax_id + '" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>';
                        html += '<?php } ?>';

                        html += '<?php if ($check_print) { ?>';
                        html += '<a href="<?= site_url("Tax_calculation/print_report?id=")?>' +result.data[i].tax_id + '&header_check=1&view=0&history=0" target="_blank" title="View Detail" class="mr-1 btn-primary btn btn-sm"><i class="fa fa-print"></i></a>';
                        html += '<?php } ?>';
                        
                        html += '</td>';
                        html += '</tr>';
                        custom_table.row.add($(html)).draw(false);
                    }

                }
            });

        });
    });
</script>