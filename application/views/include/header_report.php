<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />

<link rel="icon" href="<?= base_url()?>assets/admin/img/favicon.png" type="image/x-icon" />
<link href="<?= base_url()?>assets/admin/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>

</head>
<body>

    <div class="container-fluid ">
      <div class="header clearfix">
        <!-- assets/admin/pdf/report_header.png -->
          <img src="<?= base_url().'uploads/settings/'.basic_setting()['header_image'] ?>" style="
          width: 120%;padding-right: -20px;" >
       
        
      </div>
    </div>