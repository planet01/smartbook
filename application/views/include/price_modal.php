<div class="modal_display">
<div class="modal fade" id="defaultModalPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content my-modal-controls">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel" class="semi-bold mymodal-price-title">Clone Price List</h4>
      </div>
      <div class="modal-body mymodal-price-body price_modal" id="myModalDescription">
            <div class="grid-body no-border">
                        <form class="ajaxForm2 validate2" action="<?php echo site_url($add_price_clone); ?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Price Id:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                   <input name="plan_slog" type="text" value="" class="form-control" placeholder="Enter price id">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>                              
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-6">
                                                <label class="form-label">Description:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                     <input name="plan_description" type="text" id="" class="form-control" placeholder="Enter Description" value="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <!--           <button class="btn btn-success btn-cons" type="submit"><?php // ($page_title == 'add')?'Add New':'Edit'; ?> Customer</button> -->
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter3 my-bttn" type="button">
                                           Submit</button>
                                        <input type="hidden" id="price_id_modal" name="price_id_modal" value="<?= @$price_plan_id; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".defaultModalPrice", function(event){
        event.preventDefault();
        $("#price_id_modal").val($(this).data("id"));
        $('#defaultModalPrice').modal('show');
    });

        $("form.validate2").validate({
            rules: {
                plan_slog: {
                    required: true
                },
                plan_description: {
                    required: true
                }
            },
            messages: {
                user_name: "This field is required.",
                user_company_name: "This field is required.",
                user_display_name: "This field is required.",
                user_address: "This field is required.",
                user_email:{
                    required: "This field is required.",
                    email: "Invalid E-mail Address"
                },
                user_city: "This field is required.",
                user_country: "This field is required.",
                user_phone: "This field is required.",
                user_website:{
                    required: "This field is required.",
                    url: "This field required correct format."
                },
                user_is_active: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit    
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate2").change(function() {
            $('form.validate2').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
</script>