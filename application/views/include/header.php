<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Admin Panel <?= (@$title != '' )?'| '.$title:''; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />

<link rel="icon" href="<?= base_url()?>assets/admin/img/favicon-sb.png" type="image/x-icon" />

<!-- Disable previous dates before current dates -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<!-- End Disable previous dates before current dates -->

<!-- BEGIN Form element PLUGIN CSS -->
<!-- <link href="<?= base_url()?>assets/admin/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/> -->
<link href="<?= base_url()?>assets/admin/plugins/bootstrap-tag/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url()?>assets/admin/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url()?>assets/admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url()?>assets/admin/plugins/ios-switch/ios7-switch.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?= base_url()?>assets/admin/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?= base_url()?>assets/admin/plugins/boostrap-clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css" media="screen"/>
 
<!-- END Form element PLUGIN CSS -->
<!-- BEGIN datatable PLUGIN CSS -->
  <link href="<?= base_url()?>assets/admin/plugins/jquery-datatable/css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
  <link href="<?= base_url()?>assets/admin/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- END datatable PLUGIN CSS -->



<link href="<?= base_url()?>assets/admin/plugins/jquery-metrojs/MetroJs.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/admin/plugins/shape-hover/css/demo.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/admin/plugins/shape-hover/css/component.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/admin/plugins/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/admin/plugins/owl-carousel/owl.theme.css" />
<!-- <link href="<?= base_url()?>assets/admin/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/> -->
<link href="<?= base_url()?>assets/admin/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="<?= base_url()?>assets/admin/plugins/jquery-ricksaw-chart/css/rickshaw.css" type="text/css" media="screen" >
<link rel="stylesheet" href="<?= base_url()?>assets/admin/plugins/Mapplic/mapplic/mapplic.css" type="text/css" media="screen" >
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="<?= base_url()?>assets/admin/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->

<!-- BEGIN CSS TEMPLATE -->
<link href="<?= base_url()?>assets/admin/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url()?>assets/admin/css/magic_space.css" rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->


<!-- Start My plugin validation -->
<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/toastr.css">
<!-- <link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/jquery_validation_engine/template.css">
<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/jquery_validation_engine/validationEngine.jquery.css"> -->

<!-- end My plugin validation -->

<!-- start My plugin loader -->
<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/loader/css/main.css">
<!-- end My plugin loader -->
<!-- <script type="text/javascript" src="<?= base_url()?>assets/admin/js/jquery.min.js"></script> -->
<script src="<?php echo base_url()?>assets/admin/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/prettyPhoto/prettyPhoto.css">
<script src="<?= base_url()?>assets/admin/myplugin/prettyPhoto/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/jquery-validate/css/jquery.validate.css">

<script src="<?= base_url()?>assets/admin/myplugin/validation/jquery-validate/js/jquery.validate.js"></script>


<!-- sweetalert -->
<link href="<?= base_url(); ?>assets/admin/myplugin/sweetalert/npm/sweetalert.css" rel="stylesheet" type="text/css" media="screen"/>
<script src="<?= base_url(); ?>assets/admin/myplugin/sweetalert/npm/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/myplugin/sweetalert/dist/sweetalert.min.js"></script>

<!-- fileuploader -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/admin/myplugin/fileuploader/css/jquery.fileuploader.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/admin/myplugin/fileuploader/css/jquery.fileuploader-theme-dragdrop.css">
<script src="<?= base_url(); ?>assets/admin/myplugin/fileuploader/js/jquery.fileuploader.js"></script>
<script src="<?= base_url(); ?>assets/admin/js/jquery.dataTables.js"></script>
    
<!-- user-timezone-javascript -->
    <!-- <script type="text/javascript" src="<?= base_url(); ?>assets/admin/myplugin/user-timezone-javascript/js/jstz-1.0.4.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/admin/myplugin/user-timezone-javascript/js/moment.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/admin/myplugin/user-timezone-javascript/js/moment-timezone.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>assets/admin/myplugin/user-timezone-javascript/js/moment-timezone-data.js"></script> -->
    
<?php
if (@$uri == "dashboard") {
?>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/series-label.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
<?php
}
?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/listbox/css/kendo.common.min.css" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/listbox/css/kendo.default.min.css" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/listbox/css/kendo.default.mobile.min.css" />
<script src="<?= base_url(); ?>assets/listbox/js/kendo.all.min.js"></script>


<link href="<?= base_url()?>assets/admin/css/custom.css" rel="stylesheet" type="text/css"/> 
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- BEGIN BODY -->
<body class="error-body no-top">
<div id="loader" class="loading-wrapper">
    <div class="loading"></div>
</div>
<div id="loader-form" class="form-loading-wrapper">
    <div class="loading"></div>
</div>
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse ">
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
    <div class="header-seperation">
      <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">
        <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" >
          <div class="iconset top-menu-toggle-white"></div>
          </a> 
          
          </li>
      </ul>
      <!-- BEGIN LOGO -->
      <a href="<?= site_url('dashboard'); ?>"><img src="<?php echo base_url(); ?>assets/admin/img/logo.png" class="logo" alt=""  data-src="<?php echo base_url(); ?>assets/admin/img/logo.png" data-src-retina="<?php echo base_url(); ?>assets/admin/img/logo.png" width="170" height=""/></a>
      
      <!-- END LOGO -->
      
      <ul class="nav pull-right notifcation-center" style="display: none;">
        <li class="dropdown" id="header_task_bar"> <a href="<?= site_url('dashboard'); ?>" class="dropdown-toggle active" data-toggle="">
          <div class="iconset top-home"></div>
          </a> </li>
        <li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle">
          <div class="iconset top-chat-white "></div>
          </a> </li>
      </ul> 
    </div>
    <!-- END RESPONSIVE MENU TOGGLER -->
    <div class="header-quick-nav" >
      <!-- BEGIN TOP NAVIGATION MENU -->
      <div class="pull-left">
          <form class="" action="<?php echo site_url()."search/view"; ?>" method="get">
              <ul class="nav quick-section">
                  <li class="quicklinks">
                      <a href="#" class="" id="layout-condensed-toggle">
                          <div class="iconset top-menu-toggle-dark"></div>
                      </a>
                  </li>
                  <li class="m-r-10 input-prepend inside search-form">

                      <!--   <input name="" type="text" class="no-boarder " placeholder="Search" style="width:250px;"> -->
                      <select class="form-control input-signin-mystyle select2 responsive_scr_navhead-wdth1" id="header_product_id"  name="product_id">
                          <option value="" selected="" disabled="">Search By Product</option>
                          <?php
                 $getProduct = getCustomRows('SELECT * FROM product WHERE product_is_active = 1 AND category_type= 2 ORDER BY product_name ASC');
                 foreach($getProduct as $k => $v){
                 ?>
                              <option value="<?= $v['product_id']; ?>" <?= ( $v['product_id'] == @$_GET['product_id'])?'selected':''; ?>>
                                  <?= $v['product_sku']; ?> - <?= $v['product_name']; ?>
                              </option>
                              <?php
                 }
                 ?>
                      </select>
                  </li>
                 <!--  <li class="m-r-10 input-prepend inside search-form">
                     <select class="form-control input-signin-mystyle select2 responsive_scr_navhead-wdth2" id="" name="product_sku" >
                         <option value="" selected="" disabled="">Search By Model No</option>
                         <?php
                               $getProduct = getCustomRows('SELECT * FROM product WHERE product_is_active = 1 ORDER BY product_name ASC');
                               foreach($getProduct as $k => $v){
                               ?>
                             <option value="<?= $v['product_sku']; ?>" <?= ( $v['product_sku'] == @$_GET['product_sku'])?'selected':''; ?>>
                                 <?= $v['product_sku']; ?>
                             </option>
                             <?php
                               }
                               ?>
                     </select>
                 </li> -->
                  <li class="m-r-10 input-prepend inside search-form">
                      <select class="form-control input-signin-mystyle select2 responsive_scr_navhead-wdth3" id="header_location_id" name="warehouse_id" >
                          <option value="" selected="" disabled="" >Search By Location</option>
                          <?php
              $getProduct = getCustomRows('SELECT * FROM warehouse ORDER BY warehouse_name ASC');
              foreach($getProduct as $k => $v){
              ?>
                              <option value="<?= $v['warehouse_id']; ?>" <?= ( $v['warehouse_id'] == @$_GET['warehouse_id'])?'selected':''; ?>>
                                  <?= $v['warehouse_name']; ?>
                              </option>
                              <?php
              }
              ?>
                      </select>
                  </li>
                  <li class="m-r-10 input-prepend inside search-form">
                      <!--    <input name="" type="text" class="no-boarder " placeholder="Search" ">
           <input class="btn btn-success no-boarder" type="submit" value="Search"/> -->
           
                  <button class="btn btn-success no-boarder" type="submit"><span class="iconset top-search"></span></button>

                  </li>
                  <li class="m-r-10 input-prepend inside search-form">
                    <button style="color: #ffffff !important; background-color: #0090d9; padding: 7px 18px;" class="btn btn-success refresh_search " type="button"> Clear</button>
                    <!-- <span class="fa fa-refresh refresh_search" style="color:black"></span> -->
                  </li>

                  <li class="m-r-10 inside search-form">
                  <a id="inventory_excel_export" style="background-color: #0aa699;padding: 7px 18px;" class="btn-primary btn btn-sm" >
                  Stock Transaction View (Export to Excel)
                  </a> 
                  </li>
                  

              </ul>
          </form>
      </div>
      <!-- END TOP NAVIGATION MENU -->
      <div class="pull-right my-cog-log-style">
        <ul class="nav quick-section">
          <li class="quicklinks"> <a data-toggle="dropdown" class="dropdown-toggle  pull-right" href="#" id="user-options">
              <?php
              $id = $this->session->userdata('admin_id');
              $header_user_data = getRow("user", "user_id", $id, 'user_id' ,"ASC");
              ?>
               <div class="username"><span class="bold"><?= $header_user_data['user_name']; ?></span> 
              <div class="chat-toggler" style="min-width: auto;">
              <div class="profile-pic"> 
                <?php 
                $current_image = base_url()."uploads/dummy.jpg";
                if(isset($header_user_data) && @$header_user_data != null){
                    $file_path = dir_path().'uploads/user/'.@$header_user_data['user_image'];
                    if (file_exists($file_path)) {
                      $current_image = base_url()."uploads/user/".@$header_user_data['user_image'];
                    }else{
                      $current_image = base_url()."uploads/dummy.jpg";
                    }
                }
                ?>
                <img src="<?= $current_image; ?>"  alt="" data-src="<?= $current_image; ?>" data-src-retina="<?= $current_image; ?>" width="35" height="35" /> 
              </div>
              </div>
          </div>
            </a>
            <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
              <li><a href="<?= site_url("profile/edit"); ?>"> Edit profile</a> </li>
              <li class="divider"></li>
              <li><a href="<?= site_url("user/logout"); ?>"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- BEGIN CHAT TOGGLER -->
     
      <!-- END CHAT TOGGLER -->
    </div>
    <!-- END TOP NAVIGATION MENU -->
  </div>
  <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <script>

$(document).ready(function () {
  $(".refresh_search").click(function(){
    $('select[name=warehouse_id]').removeAttr('selected').find('option:first').attr('selected', 'selected');
    $('select[name=warehouse_id]').trigger('change')

    $('select[name=product_id]').removeAttr('selected').find('option:first').attr('selected', 'selected');
    $('select[name=product_id]').trigger('change')

    $('select[name=product_sku]').removeAttr('selected').find('option:first').attr('selected', 'selected');
    $('select[name=product_sku]').trigger('change')
  });

  $('#inventory_excel_export').click(function(e){
    
    var product_id = $('#header_product_id option:selected').val();
    var warehouse_id = $('#header_location_id option:selected').val();
    if(product_id =='')
    {
      product_id =0;
    }
    if(warehouse_id =='')
    {
      warehouse_id =0;
    }
    $(this).attr("href", "<?php echo site_url('inventory/test_excel'); ?>?product_id="+product_id+"&warehouse_id="+warehouse_id).attr( 'target','_blank' );
    return true
  });
});
  </script>