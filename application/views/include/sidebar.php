<!--BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
      <ul>
        
        <?php
        $categor_visible = false;
        $product_visible = false;
        $price_visible = false;
        $location_visible = false;
        $material_rcv_note_visible = false;
        $current_stock_status_visible = false;
        $material_issue_note_visible = false;
        $terms_n_condition_visible = false;
        $warranty_detail_visible = false;
        $warranty_type_visible = false;
        $perfoma_invoice_mail_visible = false;
        $customer_visible = false;
        $quotation_visible = false;
        $amc_quotation_visible = false;
        $delivery_note_visible = false;
        $invoice_visible = false;
        $receivables_visible = false;
        $amc_receivables_visible = false;
        $manage_email_visible = false;
        $quoatation_cover_visible = false;
        $quoatation_customer_email_visible = false;
        $invoice_customer_email_visible = false;
        $invoicedelivery = false;
        $warrantystatus = false;
        $minimumlevel = false;
        $pendingqotations = false;
        $taxcalculation = false;
        $totalsales = false;
        $dashboard_account_ledger = false;
        $dashboard_account_transaction = false;
        $voucher = false;
        $follow_up = false;
        $packing_list = false;
        $inquiry_received_history = false;
        $chart_of_accounts = false;
        $tax_calculation = false;
        $quarterly_visit = false;
        $project_summary = false;
        $ticket_managment = false;
        $scheduling = false;
        if ($this->user_type == 2) {
         //   print_r($this->user_role);
         foreach ($this->user_role_dashboard as $d => $item) {
            
            if(isset($item['department_id']) && $item['department_id'] ==1)
            {
               $invoicedelivery = true;
               $warrantystatus = true;
               $minimumlevel = true;
               $pendingqotations = true;
            }
            if(isset($item['department_id']) && $item['department_id'] ==3)
            {
               $invoicedelivery = true;
               $warrantystatus = true;
               $minimumlevel = true;
               $pendingqotations = true;
               $taxcalculation = true;
               $totalsales = true;
            }
            if(isset($item['department_id']) && $item['department_id'] ==2)
            {
               $warrantystatus = true;
               $minimumlevel = true;
            }
         }
            foreach ($this->user_role as $k => $v) {
               if($v['module_id'] == 1)
               {
                  if($v['visible'] == 1)
                  {
                     $categor_visible = true;
                  }
               }
               if($v['module_id'] == 2)
               {
                  if($v['visible'] == 1)
                  {
                     $product_visible = true;
                  }
               }
               if($v['module_id'] == 3)
               {
                  if($v['visible'] == 1)
                  {
                     $price_visible = true;
                  }
               }
               if($v['module_id'] == 4)
               {
                  if($v['visible'] == 1)
                  {
                     $location_visible = true;
                  }
               }
               if($v['module_id'] == 5)
               {
                  if($v['visible'] == 1)
                  {
                     $material_rcv_note_visible = true;
                  }
               }
               if($v['module_id'] == 6)
               {
                  if($v['visible'] == 1)
                  {
                     $current_stock_status_visible = true;
                  }
               }
               if($v['module_id'] == 7)
               {
                  if($v['visible'] == 1)
                  {
                     $material_issue_note_visible = true;
                  }
               }
               if($v['module_id'] == 8)
               {
                  if($v['visible'] == 1)
                  {
                     $customer_visible = true;
                  }
               }
               if($v['module_id'] == 9)
               {
                  if($v['visible'] == 1)
                  {
                     $quotation_visible = true;
                  }
               }
               if($v['module_id'] == 10)
               {
                  if($v['visible'] == 1)
                  {
                     $amc_quotation_visible = true;
                  }
               }
               if($v['module_id'] == 11)
               {
                  if($v['visible'] == 1)
                  {
                     $delivery_note_visible = true;
                  }
               }
               if($v['module_id'] == 12)
               {
                  if($v['visible'] == 1)
                  {
                     $invoice_visible = true;
                  }
               }
               if($v['module_id'] == 13)
               {
                  if($v['visible'] == 1)
                  {
                     $receivables_visible = true;
                  }
               }
               if($v['module_id'] == 14)
               {
                  if($v['visible'] == 1)
                  {
                     $amc_receivables_visible = true;
                  }
               }
               if($v['module_id'] == 15)
               {
                  if($v['visible'] == 1)
                  {
                     $manage_email_visible = true;
                  }
               }
               if($v['module_id'] == 16)
               {
                  if($v['visible'] == 1)
                  {
                     $quoatation_cover_visible = true;
                  }
               }
               if($v['module_id'] == 17)
               {
                  if($v['visible'] == 1)
                  {
                     $quoatation_customer_email_visible = true;
                  }
               }
               if($v['module_id'] == 18)
               {
                  if($v['visible'] == 1)
                  {
                     $invoice_customer_email_visible = true;
                  }
               }
               if($v['module_id'] == 19)
               {
                  if($v['visible'] == 1)
                  {
                     $perfoma_invoice_mail_visible = true;
                  }
               }
               if($v['module_id'] == 20)
               {
                  if($v['visible'] == 1)
                  {
                     $warranty_type_visible = true;
                  }
               }
               if($v['module_id'] == 21)
               {
                  if($v['visible'] == 1)
                  {
                     $warranty_detail_visible = true;
                  }
               }
               if($v['module_id'] == 22)
               {
                  if($v['visible'] == 1)
                  {
                     $terms_n_condition_visible = true;
                  }
               }
               if($v['module_id'] == 23)
               {
                  if($v['visible'] == 1)
                  {
                     $voucher = true;
                  }
               }
               if($v['module_id'] == 24)
               {
                  if($v['visible'] == 1)
                  {
                     $follow_up = true;
                  }
               }
               if($v['module_id'] == 25)
               {
                  if($v['visible'] == 1)
                  {
                     $packing_list = true;
                  }
               }
               if($v['module_id'] == 26)
               {
                  if($v['visible'] == 1)
                  {
                     $inquiry_received_history = true;
                  }
               }
               if($v['module_id'] == 27)
               {
                  if($v['visible'] == 1)
                  {
                     $chart_of_accounts = true;
                  }

               }
               if($v['module_id'] == 28)
               {
                  if($v['visible'] == 1)
                  {
                     $tax_calculation =  true;
                  }
               }

               if($v['module_id'] == 29)
               {
                  if($v['visible'] == 1)
                  {
                     $quarterly_visit =  true;
                  }
               }

               if($v['module_id'] == 30)
               {
                  if($v['visible'] == 1)
                  {
                     $dashboard_account_transaction = true;
                  }
               }

               if($v['module_id'] == 31)
               {
                  if($v['visible'] == 1)
                  {
                     $dashboard_account_ledger = true;
                  }
               }

               if($v['module_id'] == 32)
               {
                  if($v['visible'] == 1)
                  {
                     $project_summary = true;
                  }
               }

               if($v['module_id'] == 33)
               {
                  if($v['visible'] == 1)
                  {
                     $ticket_managment = true;
                  }
               }
               if($v['module_id'] == 34)
               {
                  if($v['visible'] == 1)
                  {
                     $scheduling = true;
                  }
               }
               
            }
        }else
        {
         $categor_visible = true;
         $product_visible = true;
         $price_visible = true;
         $location_visible = true;
         $material_rcv_note_visible = true;
         $current_stock_status_visible = true;
         $material_issue_note_visible = true;
         $terms_n_condition_visible = true;
         $warranty_detail_visible = true;
         $warranty_type_visible = true;
         $perfoma_invoice_mail_visible = true;
         $customer_visible = true;
         $quotation_visible = true;
         $amc_quotation_visible = true;
         $delivery_note_visible = true;
         $invoice_visible = true;
         $receivables_visible = true;
         $amc_receivables_visible = true;
         $manage_email_visible = true;
         $quoatation_cover_visible = true;
         $quoatation_customer_email_visible = true;
         $invoice_customer_email_visible = true;
         $invoicedelivery = true;
         $warrantystatus = true;
         $minimumlevel = true;
         $pendingqotations = true;
         $taxcalculation = true;
         $totalsales = true;
         $dashboard_account_ledger = true;
         $dashboard_account_transaction = true;
         $voucher = true;
         $follow_up = true;
         $packing_list = true;
         $inquiry_received_history = true;
         $chart_of_accounts = true;
         $tax_calculation =  true;
         $quarterly_visit = true;
         $project_summary = true;
         $ticket_managment = true;
         $scheduling = true;
        }
        ?>
        <li class="<?php echo (@$uri == 'dashboard') ? 'active open' : ''; ?>">
        <a href="#"> <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span class="title">Dashboards</span>
            <span class="selected"></span>
            <span class="arrow <?php echo ( @$active == 'invoice_warranty_status' || @$active == 'invoice_delivery_note_matching' || @$active == 'pending_quotations' || @$active == 'tax_calculation' || @$active == 'total_sales' || @$active == 'stock_below_minimum_level') ? 'open' : ''; ?>"></span>
          </a> 
          <ul class="sub-menu">
            <?php if($invoicedelivery){?>
              <li class="<?php echo (@$active == 'invoice_delivery_note_matching') ? 'active' : ''; ?>"> <a href="<?= site_url('dashboard/invoice_delivery_note_matching'); ?>">Invoice & Delivery Not Matching  </a></li>
              <?php } ?>
              <?php if($warrantystatus){?>
               <li class="<?php echo (@$active == 'invoice_warranty_status') ? 'active' : ''; ?>"> <a href="<?= site_url('dashboard/invoice_warranty_status'); ?>">Invoice Warranty Status </a></li>
               <?php } ?>

               <?php if($minimumlevel){?>
              <li class="<?php echo (@$active == 'stock_below_minimum_level') ? 'active open' : ''; ?>">
                 <a href="<?= site_url('dashboard/stock_below_minimum_level'); ?>"> Stock Below Minimum Level 
                  </a> 
              </li>
              <?php } ?>
              <?php if($pendingqotations){?>
              <li class="<?php echo (@$active == 'pending_quotations' ) ? 'active open' : ''; ?>">
                 <a href="<?= site_url('dashboard/pending_quotations'); ?>"> Pending Quotations  
                  </a> 
              </li>
              <?php } ?>
              <?php if($taxcalculation){?>
              <li class="<?php echo (@$active == 'tax_calculation') ? 'active open' : ''; ?>">
                 <a href="<?= site_url('dashboard/tax_calculation'); ?>"> Tax Calculation  
                  </a> 
              </li>
              <?php } ?>
              <?php if($totalsales){?>
              <li class="<?php echo (@$active == 'total_sales') ? 'active open' : ''; ?>">
                 <a href="<?= site_url('dashboard/total_sales'); ?>"> Total Sales   
                  </a> 
              </li>
              <?php } ?>
              
              

            </ul>
        </li>
        
        <!-- <li class="<?php echo (@$active == 'add-category' || @$active == 'view-category') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-tags" aria-hidden="true"></i>
            <span class="title">Categories</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-category' || @$active == 'view-category') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'add-category') ? 'active' : ''; ?>"> <a href="<?= site_url('category/add'); ?>"> Add </a> </li>
              <li class="<?php echo (@$active == 'view-category') ? 'active' : ''; ?>"> <a href="<?= site_url('category/view'); ?>"> View </a></li>
           </ul>
        </li> -->
        
        <?php if($product_visible){?>
        <li class="<?php echo (@$active == 'add-product' || @$active == 'view-product') ? 'active open' : ''; ?>">
           <a href="<?= site_url('product/view'); ?>"> 
            <i class="fa fa-print" aria-hidden="true"></i>
            <span class="title">Products</span>
          </a> 
        </li>
        <?php }?>

        <!-- <li class="<?php echo (@$active == 'add-product' || @$active == 'view-product') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-print" aria-hidden="true"></i>
            <span class="title">Products</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-product' || @$active == 'view-product') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'add-product') ? 'active' : ''; ?>"> <a href="<?= site_url('product/add'); ?>"> Add </a> </li>
              <li class="<?php echo (@$active == 'view-product') ? 'active' : ''; ?>"> <a href="<?= site_url('product/view'); ?>"> View / Change Info </a></li>
           </ul>
        </li> -->
         <!-- <li class="<?php echo (@$active == 'add-price_plan' || @$active == 'view-price_plan') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-print" aria-hidden="true"></i>
            <span class="title">Price List</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-price_plan' || @$active == 'view-price_plan') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">    
              <li class="<?php echo (@$active == 'add-price_plan') ? 'active' : ''; ?>"> <a href="<?= site_url('price_plan/add'); ?>"> Add </a> </li>
              <li class="<?php echo (@$active == 'view-price_plan') ? 'active' : ''; ?>"> <a href="<?= site_url('price_plan/view'); ?>"> View</a></li>
           </ul>
                 </li> -->

        <li class="<?php echo (@$active == 'add-inventory' || @$active == 'view-inventory' || @$active == 'quantity_remaining-inventory'  || @$active == 'current_stock_status-inventory' || @$active == 'add-material_issue_note' || @$active == 'view-material_issue_note') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-calculator" aria-hidden="true"></i>
            <span class="title">Inventory</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-inventory' || @$active == 'view-inventory' || @$active == 'quantity_remaining-inventory' || @$active == 'current_stock_status-inventory') ? 'open' : ''; ?>"></span>
            </a> 
            <ul class="sub-menu">
            <?php if($material_rcv_note_visible){?>
              <li class="<?php echo (@$active == 'view-inventory') ? 'active' : ''; ?>"> <a href="<?= site_url('inventory/summary'); ?>">Material Receive Note  </a></li>
              <?php } ?>
              <!-- <li class="<?php echo (@$active == 'view-issue-note') ? 'active' : ''; ?>"> <a href="#">Material Issue Note  </a></li> -->
              <?php if($current_stock_status_visible){?>
               <li class="<?php echo (@$active == 'current_stock_status-inventory') ? 'active' : ''; ?>"> <a href="<?= site_url('inventory/current_stock_status'); ?>">Stock Transaction View</a></li>
               <?php } ?>

               <?php if($material_issue_note_visible){?>
              <li class="<?php echo (@$active == 'add-material_issue_note' || @$active == 'view-material_issue_note') ? 'active open' : ''; ?>">
                 <a href="<?= site_url('material_issue_note/view'); ?>"> Material Issue Note
                  </a> 
              </li>
              <?php } ?>
            </ul>
        </li>
        <!-- <li class="<?php echo (@$active == 'add-inventory' || @$active == 'view-inventory' || @$active == 'quantity_remaining-inventory'  || @$active == 'current_stock_status-inventory') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-calculator" aria-hidden="true"></i>
            <span class="title">Inventory</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-inventory' || @$active == 'view-inventory' || @$active == 'quantity_remaining-inventory' || @$active == 'current_stock_status-inventory') ? 'open' : ''; ?>"></span>
            </a> 
            <ul class="sub-menu">
              <li class="<?php echo (@$active == 'add-inventory') ? 'active' : ''; ?>"> <a href="<?= site_url('inventory/add'); ?>">Material Receive Note </a> </li>
              <li class="<?php echo (@$active == 'view-inventory') ? 'active' : ''; ?>"> <a href="<?= site_url('inventory/summary'); ?>">Material Receive Note Summary </a></li>
              <li class="<?php echo (@$active == 'quantity_remaining-inventory') ? 'active' : ''; ?>"> <a href="<?= site_url('inventory/quantity_remaining'); ?>"> Quantity Remaining </a></li>
               <li class="<?php echo (@$active == 'current_stock_status-inventory') ? 'active' : ''; ?>"> <a href="<?= site_url('inventory/current_stock_status'); ?>">Current Stock Status</a></li>
              
            </ul>
        </li> -->
        <li class="<?php echo (@$active == 'view-follow_up' || @$active == 'view-receivables' || @$active == 'add-receivables' || @$active == 'add-inquiry_received_history' || @$active == 'view-inquiry_received_history' ||@$active == 'view-packing_list' ||  @$active == 'add-packing_list' || @$active == 'add-customer' || @$active == 'view-customer' || @$active == 'add-quotation' || @$active == 'view-quotation' ||  @$active == 'add-delivery_note' || @$active == 'view-delivery_note' || @$active == 'add-invoice' || @$active == 'view-invoice') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-users" aria-hidden="true"></i>
            <span class="title">Sales Management</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-customer' || @$active == 'view-customer') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
           <?php if($customer_visible){?>
              <li class="<?php echo (@$active == 'view-customer') ? 'active' : ''; ?>"> <a href="<?= site_url('customer/view'); ?>"> Customers </a></li>
            <?php } ?>
            <?php if($quotation_visible){?>
               <li class="<?php echo (@$active == 'view-quotation') ? 'active' : ''; ?>"> <a href="<?= site_url('quotation/view'); ?>"> Quotations & P.I </a></li>
               <?php } ?>   
               <?php if($delivery_note_visible) {?>
               <li class="<?php echo (@$active == 'view-delivery_note') ? 'active' : ''; ?>"> <a href="<?= site_url('delivery_note/view'); ?>"> Delivery Notes </a></li>
               <?php }?>
               <?php if($invoice_visible) {?>
               <li class="<?php echo (@$active == 'view-invoice') ? 'active' : ''; ?>"> <a href="<?= site_url('invoice/view'); ?>"> Invoices </a></li>
               <?php }?>
               <?php if($receivables_visible) {?>
               <li class="<?php echo (@$active == 'view-receivables') ? 'active' : ''; ?>"> <a href="<?= site_url('receivables/view'); ?>"> Receivables </a></li>
               <?php }?>
               <?php if($inquiry_received_history) {?>
               <li class="<?php echo (@$active == 'view-inquiry_received_history' || @$active == 'add-inquiry_received_history') ? 'active' : ''; ?>"> <a href="<?= site_url('Inquiry_received_history/view'); ?>"> Inquiry Received History </a></li>
               <?php } ?>
               <?php if($follow_up) {?>
               <li class="<?php echo (@$active == 'view-follow_up') ? 'active' : ''; ?>"> <a href="<?= site_url('Follow_Up/view'); ?>"> Follow Ups </a></li>
               <?php } ?>
               <?php if($packing_list) {?>
               <li class="<?php echo (@$active == 'view-packing_list' || @$active == 'add-packing_list') ? 'active' : ''; ?>"> <a href="<?= site_url('Packing_list/view'); ?>"> Packing List </a></li>
               <?php } ?>
           </ul>
         
         
        </li>
         
        <?php if($amc_quotation_visible || $amc_receivables_visible){?>
        <li class="<?php echo (@$active == 'add-amc_quotation' || @$active == 'view-amc_quotation' || @$active == 'view-amc-receivables' || @$active == 'add-amc-receivables'  ) ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-users" aria-hidden="true"></i>
            <span class="title">Annual Maintenance</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-amc_quotation' || @$active == 'view-amc_quotation' || @$active == 'add-receivables' || @$active == 'view-receivables') ? 'open' : ''; ?>"></span>
          </a> 
          <ul class="sub-menu">
            <?php if($amc_quotation_visible){?>
            <li class="<?php echo (@$active == 'view-amc_quotation') ? 'active' : ''; ?>"> <a href="<?= site_url('Amc_quotation/view'); ?>">Annual Maintenance Contract (AMC) </a></li>
            <?php } ?>   
            <?php if($amc_receivables_visible) {?>
            <li class="<?php echo (@$active == 'view-amc-receivables') ? 'active' : ''; ?>"> <a href="<?= site_url('AMC_Receivables/view'); ?>"> AMC Receivables </a></li>
            <?php }?>
            </ul>
         </li>
         <?php } ?>
         
        <?php if( $dashboard_account_ledger || $dashboard_account_transaction || $voucher || $chart_of_accounts || $tax_calculation) {?>
        <li class="<?php echo (@$active== 'dashboard_account_ledger' || @$active== 'dashboard_account_transaction' || @$active == 'view-tax_calculation' || @$active == 'add-tax_calculation' || @$active == 'edit-tax_calculation' || @$active == 'edit-chart_of_accounts' || @$active == 'add-chart_of_accounts' || @$active == 'view-chart_of_accounts' || @$active == 'view-voucher' || @$active == 'add-voucher' || @$active == 'edit-voucher') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-users" aria-hidden="true"></i>
            <span class="title">Accounts</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-voucher' || @$active == 'view-voucher') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
               <?php if($voucher) {?>
               <li class="<?php echo (@$active == 'view-voucher') ? 'active' : ''; ?>"> <a href="<?= site_url('voucher/view'); ?>">Vouchers </a></li>
               <?php } ?>

               <?php if($chart_of_accounts){
                  ?>
                     <li class="<?php echo (@$active == 'view-chart_of_accounts' || @$active == 'add-chart_of_accounts') ? 'active' : ''; ?>"> <a href="<?= site_url('Chart_Of_Accounts/view'); ?>"> Chart Of Accounts </a></li>
                  <?php
               }?>

               <?php if($tax_calculation) {?>
               <li class="<?php echo (@$active == 'view-tax_calculation' || @$active == 'add-tax_calculation' || @$active == 'edit-tax_calculation') ? 'active' : ''; ?>"> <a href="<?= site_url('tax_calculation/view'); ?>">Tax Calculation & Payment Screen </a></li>
               <?php } ?>

               <?php if($dashboard_account_transaction){?>
               <li class="<?php echo (@$active == 'dashboard_account_transaction') ? 'active open' : ''; ?>">
                  <a href="<?= site_url('dashboard/dashboard_account_transaction'); ?>"> Accounts Transaction Report
                     </a> 
               </li>
              <?php } ?>
               <?php if($dashboard_account_ledger){?>
               <li class="<?php echo (@$active == 'dashboard_account_ledger') ? 'active open' : ''; ?>">
                  <a href="<?= site_url('dashboard/dashboard_account_ledger'); ?>"> Accounts Ledger Report
                     </a> 
               </li>
              <?php } ?>
               
           </ul>
         </li>
         <?php }?>

         <?php if($quarterly_visit || $project_summary || $ticket_managment || $scheduling) {?>
        <li class="<?php echo ( @$active == 'add-scheduling' || @$active == 'view-scheduling' || @$active == 'add-ticket_management' || @$active == 'view-ticket_management' || @$active == 'view-quarterly_visit' || @$active == 'view-project_summary' || @$active == 'edit-project_summary') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-users" aria-hidden="true"></i>
            <span class="title">Project Management</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-scheduling' ||@$active == 'view-scheduling' || @$active == 'view-quarterly_visit' || @$active == 'view-project_summary' || @$active == 'edit-project_summary') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
               <?php if($project_summary) {?>
               <li class="<?php echo (@$active == 'view-project_summary' || @$active == 'edit-project_summary') ? 'active' : ''; ?>"> <a href="<?= site_url('project_summary/view'); ?>">Project Summary</a></li>
               <?php } ?>
               
               <?php if($ticket_managment) {?>
               <li class="<?php echo (@$active == 'add-ticket_management' || @$active == 'view-ticket_management') ? 'active' : ''; ?>"> <a href="<?= site_url('Ticket_management/view'); ?>">Ticket Management</a></li>
               <?php } ?>
               
               <?php if($quarterly_visit) {?>
               <li class="<?php echo (@$active == 'view-quarterly_visit') ? 'active' : ''; ?>"> <a href="<?= site_url('quarterly_visit/view'); ?>">Quarterly Visit</a></li>
               <?php } ?>

               <?php if($scheduling) {?>
               <li class="<?php echo (@$active == 'view-scheduling'|| @$active == 'add-scheduling') ? 'active' : ''; ?>"> <a href="<?= site_url('scheduling/view'); ?>">Scheduling</a></li>
               <?php } ?>
               


            </ul>
         </li>
         <?php }?>
         
        <li class="<?php echo (@$active == 'add-approvals' || @$active == 'view-approvals') ? 'active open' : ''; ?>">
          <!-- <a href="#"> 
            <i class="fa fa-file-text" aria-hidden="true"></i>
            <span class="title">Approvals</span>
          </a> --> 
        </li>

      
        <!-- <li class="<?php echo (@$active == 'add-receivables' || @$active == 'view-receivables' ) ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-file-text-o" aria-hidden="true"></i>
            <span class="title">Receivables</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-receivables' || @$active == 'view-receivables') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'view-delivery_note') ? 'active' : ''; ?>"> <a href="<?= site_url('receivables/view'); ?>"> View </a></li>
           </ul>
        </li> -->

        <!--  <li class="<?php echo (@$active == 'add-invoice' || @$active == 'view-invoice') ? 'active open' : ''; ?>">
          <a href="#"> <i class="fa fa-file-text" aria-hidden="true"></i>
           <span class="title">Invoice</span>
           <span class="selected"></span>
           <span class="arrow <?php echo (@$active == 'add-invoice' || @$active == 'view-invoice') ? 'open' : ''; ?>"></span>
         </a> 
          <ul class="sub-menu">
             <li class="<?php echo (@$active == 'add-invoice') ? 'active' : ''; ?>"> <a href="<?= site_url('invoice/add'); ?>"> Add </a> </li>
             <li class="<?php echo (@$active == 'view-invoice') ? 'active' : ''; ?>"> <a href="<?= site_url('invoice/view'); ?>"> View </a></li>
          </ul>
                </li> -->

        <!-- <li class="<?php echo (@$active == 'add-delivery_note' || @$active == 'view-delivery_note') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-file-text-o" aria-hidden="true"></i>
            <span class="title">Delivery Note</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-delivery_note' || @$active == 'view-delivery_note') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'add-delivery_note') ? 'active' : ''; ?>"> <a href="<?= site_url('delivery_note/add'); ?>"> Add </a> </li>
              <li class="<?php echo (@$active == 'view-delivery_note') ? 'active' : ''; ?>"> <a href="<?= site_url('delivery_note/view'); ?>"> View </a></li>
           </ul>
        </li> -->

        

         

       

        <!--  <li class="<?php echo (@$active == 'add-warehouse' || @$active == 'view-warehouse') ? 'active open' : ''; ?>">
          <a href="#"> <i class="fa fa-university" aria-hidden="true"></i>
           <span class="title">Location</span>
           <span class="selected"></span>
           <span class="arrow <?php echo (@$active == 'add-warehouse' || @$active == 'view-warehouse') ? 'open' : ''; ?>"></span>
         </a> 
          <ul class="sub-menu">
             <li class="<?php echo (@$active == 'add-warehouse') ? 'active' : ''; ?>"> <a href="<?= site_url('warehouse/add'); ?>"> Add </a> </li>
             <li class="<?php echo (@$active == 'view-warehouse') ? 'active' : ''; ?>"> <a href="<?= site_url('warehouse/view'); ?>"> View </a></li>
          </ul>
                </li>
         -->
        

        <!-- <li class="<?php echo (@$active == 'add-customer' || @$active == 'view-customer') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-users" aria-hidden="true"></i>
            <span class="title">Customer</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-customer' || @$active == 'view-customer') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'add-customer') ? 'active' : ''; ?>"> <a href="<?= site_url('customer/add'); ?>"> Add </a> </li>
              <li class="<?php echo (@$active == 'view-customer') ? 'active' : ''; ?>"> <a href="<?= site_url('customer/view'); ?>"> View </a></li>
           </ul>
        </li>
         -->
       <!--  <li class="<?php echo (@$active == 'add-employee' || @$active == 'view-employee') ? 'active open' : ''; ?>">
          <a href="#"> <i class="fa fa-users" aria-hidden="true"></i>
           <span class="title">Employee</span>
           <span class="selected"></span>
           <span class="arrow <?php echo (@$active == 'add-employee' || @$active == 'view-employee') ? 'open' : ''; ?>"></span>
         </a> 
          <ul class="sub-menu">
             <li class="<?php echo (@$active == 'add-employee') ? 'active' : ''; ?>"> <a href="<?= site_url('employee/add'); ?>"> Add </a> </li>
             <li class="<?php echo (@$active == 'view-employee') ? 'active' : ''; ?>"> <a href="<?= site_url('employee/view'); ?>"> View </a></li>
          </ul>
       </li> -->
        
        <!-- <li class="<?php echo (@$active == 'add-quotation' || @$active == 'view-quotation') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
            <span class="title">Quotations</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-quotation' || @$active == 'view-quotation') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'add-quotation') ? 'active' : ''; ?>"> <a href="<?= site_url('quotation/add'); ?>"> Add </a> </li>
              <li class="<?php echo (@$active == 'view-quotation') ? 'active' : ''; ?>"> <a href="<?= site_url('quotation/view'); ?>"> View </a></li>
           </ul>
        </li> -->

       <!--  <li class="<?php echo (@$active == 'add-amc_quotation' || @$active == 'view-amc_quotation') ? 'active open' : ''; ?>">
          <a href="#"> <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
           <span class="title">AMC Quotations</span>
           <span class="selected"></span>
           <span class="arrow <?php echo (@$active == 'add-amc_quotation' || @$active == 'view-amc_quotation') ? 'open' : ''; ?>"></span>
         </a> 
          <ul class="sub-menu">
             <li class="<?php echo (@$active == 'add-amc_quotation') ? 'active' : ''; ?>"> <a href="<?= site_url('Amc_quotation/add'); ?>"> Add </a> </li>
             <li class="<?php echo (@$active == 'view-amc_quotation') ? 'active' : ''; ?>"> <a href="<?= site_url('Amc_quotation/view'); ?>"> View </a></li>
          </ul>
       </li> -->
        
       
        
        

        <!-- <li class="<?php echo (@$active == 'add-discrepancy' || @$active == 'view-discrepancy') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-file-text-o" aria-hidden="true"></i>
            <span class="title">Discrepancy</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-discrepancy' || @$active == 'view-discrepancy') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'view-discrepancy') ? 'active' : ''; ?>"> <a href="<?= site_url('discrepancy/view'); ?>"> View </a></li>
           </ul>
        </li> -->

         
        <!-- <li class="<?php echo (@$active == 'add-material_issue_note' || @$active == 'view-material_issue_note') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-file" aria-hidden="true"></i>
            <span class="title">Material Issue Note</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-material_issue_note' || @$active == 'view-material_issue_note') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
              <li class="<?php echo (@$active == 'add-material_issue_note') ? 'active' : ''; ?>"> <a href="<?= site_url('material_issue_note/add'); ?>"> Add </a> </li>
              <li class="<?php echo (@$active == 'view-material_issue_note') ? 'active' : ''; ?>"> <a href="<?= site_url('material_issue_note/view'); ?>"> View </a></li>
           </ul>
        </li> -->

         <li class="<?php echo (@$active == 'add-category' || @$active == 'view-category' || @$active == 'add-price_plan' || @$active == 'view-price_plan' || @$active == 'edit-setting' || @$active == 'manager_email'  || @$active == 'quotation_cover' || @$active == 'quotation_customer' || @$active == 'invoice_customer' || @$active == 'proforma_invoice' || @$active =='warranty_type' || @$active =='warranty_detail' || @$active == 'add-warehouse' || @$active == 'view-warehouse' || @$active == 'add-employee' || @$active == 'view-employee') ? 'active open' : ''; ?>">
           <a href="#"> <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="title">Settings</span>
            <span class="selected"></span>
            <span class="arrow <?php echo (@$active == 'add-category' || @$active == 'view-category' || @$active == 'add-price_plan' || @$active == 'view-price_plan' ||@$active == 'edit-setting'  || @$active == 'manager_email'  || @$active == 'quotation_cover'|| @$active == 'quotation_customer' || @$active == 'invoice_customer' || @$active == 'proforma_invoice' || @$active =='warranty_type' || @$active =='warranty_detail') ? 'open' : ''; ?>"></span>
          </a> 
           <ul class="sub-menu">
               <?php if ($this->user_type == 1) { ?>
               <li class="<?php echo (@$active == 'view-role') ? 'active' : ''; ?>">  <a href="<?= site_url('role/view'); ?>"> Role </a> </li>
               <?php } ?>

              <?php if ($this->user_type == 1) { ?>
              <li class="<?php echo (@$active == 'edit-setting') ? 'active' : ''; ?>">  <a href="<?= site_url('setting/edit'); ?>"> General Details </a> </li>
              <?php } ?>

              <?php if($categor_visible){?>
               <li class="<?php echo (@$active == 'add-category' || @$active == 'view-category') ? 'active' : ''; ?>"><a href="<?= site_url('category/view'); ?>">Categories</a> </li>
               <?php }?>
              <?php if($manage_email_visible){?>
              <li class="<?php echo (@$active == 'manager_email') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/manager_email'); ?>"> Managers Email IDs </a></li>
              <?php } ?>
              <?php if($price_visible){?>
               <li class="<?php echo (@$active == 'add-price_plan' || @$active == 'view-price_plan') ? 'active ' : ''; ?>"><a href="<?= site_url('price_plan/view'); ?>"> Price List</a> </li>
               <?php }?>
              <?php if($quoatation_cover_visible){?>
              <li class="<?php echo (@$active == 'quotation_cover') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/quotation_cover'); ?>"> Quotation Cover Letter </a></li>
              <?php } ?>
              <?php if($quoatation_customer_email_visible){?>
              <li class="<?php echo (@$active == 'quotation_customer') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/quotation_customer'); ?>"> Quotation Customer Email </a></li>
              <?php } ?>
              <?php if($invoice_customer_email_visible){?>
              <li class="<?php echo (@$active == 'invoice_customer') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/invoice_customer'); ?>"> Invoice Customer Email </a></li>
              <?php } ?>
              <?php if($perfoma_invoice_mail_visible){?>
              <li class="<?php echo (@$active == 'proforma_invoice') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/proforma_invoice'); ?>"> Proforma Invoice Email</a></li>
              <?php } ?>
              <?php if($warranty_type_visible){?>
              <li class="<?php echo (@$active == 'warranty_type') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/warranty_type'); ?>"> Warranty Types</a></li>
              <?php } ?>
              <?php if($warranty_detail_visible){?>
              <li class="<?php echo (@$active == 'warranty_detail') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/warranty_detail'); ?>"> Warranty Details</a></li>
              <?php } ?>
              <?php if($terms_n_condition_visible){?>
              <li class="<?php echo (@$active == 'edit_terms') ? 'active' : ''; ?>"> <a href="<?= site_url('setting/edit_terms'); ?>">Terms & Condition</a></li>
               <?php } ?>
        
               
               <?php if($location_visible){?>
                  <li class="<?php echo (@$active == 'view-warehouse') ? 'active' : ''; ?>"> <a href="<?= site_url('warehouse/view'); ?>"> Location </a></li>  
               <?php }?>     
               <?php if ($this->user_type == 1) { ?>
                <li class="<?php echo (@$active == 'view-employee') ? 'active' : ''; ?>"> <a href="<?= site_url('employee/view'); ?>"> Employee </a></li>
                <?php } ?>
           </ul>
        </li>

          
        <li class="responsive-nav <?php echo (@$active == 'edit-profile') ? 'active open' : ''; ?>">
           <a href="<?= site_url("profile/edit"); ?>"> <i class="fa  fa-user" aria-hidden="true"></i>
            <span class="title">Edit Profile</span>
          </a> 
        </li>
        <li class="responsive-nav <?php echo (@$active == 'dashboard') ? 'active open' : ''; ?>">
           <a href="<?php echo site_url('user/logout'); ?>"> <i class="fa fa-power-off" aria-hidden="true"></i>
            <span class="title">Logout</span>
          </a> 
        </li>
        <!-- <li onclick="delete_all_data()">
               <a class="delete_all_data" href="javascript:void(0);"> <i class="fa fa-file-text" aria-hidden="true"></i>
                <span class="title">Delete</span>
              </a> 
            </li>    -->    
      </ul>
      <div class="clearfix"></div>
      <!-- END SIDEBAR MENU -->
    </div>
  </div>
  <a href="#" class="scrollup">Scroll</a>
  <!--<div class="footer-widget">
    <div class="progress transparent progress-small no-radius no-margin">
      <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar" ></div>
    </div>
    <div class="pull-right">
      <div class="details-status"> <span data-animation-duration="560" data-value="86" class="animate-number"></span>% </div>
      <a href="lockscreen.html"><i class="fa fa-power-off"></i></a></div>
  </div>-->