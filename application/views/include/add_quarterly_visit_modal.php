<div class="modal_display">
<div class="modal fade" id="add_quaterly_visit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content customer_modal_height">
        <form class='ajaxForm validate2' action="<?=site_url('Quarterly_visit/add') ?>" method='post' enctype='multipart/form-data'>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Add Quarterly Visit</h4>
          </div>
          <div class="modal-body add_quaterly_visit-body" id="myModalDescription">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-3">
                            <label class="form-label">Service Date</label>
                          </div>
                          <div class="col-md-6">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input value="" name='service_date' type='text' class='datepicker form-control' placeholder=''>
                              </div>
                            
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-3 ">
                            <label class="form-label">Sercive Type</label>
                          </div>
                          <div class="col-md-6">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <div class="radio radio-success responsve-receivable-radio">
                                    <input id="service_type1" type="radio" name="service_type" value="0">
                                    <label class="" for="service_type1"><b>Online</b></label>
                                        
                                    <input id="service_type2" checked="true" type="radio" name="service_type" value="1">
                                    <label class="" for="service_type2"><b>Onsite</b></label>      
                                </div> 
                              </div>
                            
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                            <label class="form-label">Service Detail</label>
                          </div>
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <textarea name="service_detail" class="form-control"></textarea>
                              </div>
                            
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            
          </div>
          <div class="modal-footer">
            <input type="hidden" value="" name="invoice_id" id="invoice_id" >
            <input type="hidden" value="" name="due_since_search" id="due_since_search" >
            <input type="hidden" value="" name="customer_id_search" id="customer_id_search" >
            <input type="hidden" value="" name="invoice_type_search" id="invoice_type_search" >
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter my-bttn mt-2' type='button'>OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
 </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".quarterly_visit", function(event){
        event.preventDefault();
        
        var id = $(this).data('id');
        $("#invoice_id").val(id);

        var due_since = $('#due_since').val();
        var invoice_type = $('[name="invoice_type"]:checked').val();
        var customer_id = $('#customer_id').val();
        
        $("#due_since_search").val(due_since);
        $("#customer_id_search").val(invoice_type);
        $("#invoice_type_search").val(customer_id);
        $('#add_quaterly_visit').modal('show');
    });

    $("form.validate2").validate({
          rules: {
            service_date:{
              required: true
            },
            service_type:{
              required: true
            },
            service_detail:{
                required: true
            }
          }, 
          messages: {
            service_date: "This field is required.",
            service_type: "This field is required.",
            service_detail: "This field is required."
          },
          invalidHandler: function (event, validator) {
            //display error alert on form submit    
            },
            errorPlacement: function (label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');  
            },
            highlight: function (element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control'); 
            },
            unhighlight: function (element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control'); 
            },
            success: function (label, element) {
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('error-control').addClass('success-control');
              
            }
            // submitHandler: function (form) {
            // }
        });
});
</script>