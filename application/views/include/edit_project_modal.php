<div class="modal_display">
<div class="modal fade" id="edit_technician_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content customer_modal_height">
        <form class='ajaxForm validate2' action="<?=site_url('Project_summary/edit_tech') ?>" method='post' enctype='multipart/form-data'>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Edit Project Technician</h4>
          </div>
          <div class="modal-body edit_technician_modal-body" id="myModalDescription">
            <div class="row">

            <div class="col-md-6">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Primary Technincian</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <select name="primary_techinician" id="primary_techinician" class="select2 form-control">
                               <option value="0" selected disabled>--- Select Primary Technincian ---</option>
                               <?php
                                foreach ($employeeData as $k => $v) {
                                ?>
                                 <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['primary_techinician']) ? 'selected' : ''; ?>>
                                   <?= $v['user_name']; ?></option>
                               <?php
                                }
                                ?>
                             </select>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-6">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Secondary Technincian</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <select name="secondary_techinician" id="secondary_techinician" class="select2 form-control">
                               <option value="0" selected disabled>--- Select Primary Technincian ---</option>
                               <?php
                                foreach ($employeeData as $k => $v) {
                                ?>
                                 <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['secondary_techinician']) ? 'selected' : ''; ?>>
                                   <?= $v['user_name']; ?></option>
                               <?php
                                }
                                ?>
                             </select>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                  
              </div>
            
          </div>
          <div class="modal-footer">
            <input type="hidden" value="" name="project_id" id="project_id" >
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter my-bttn mt-2' type='button'>OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
 </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".edit_technician", function(event){
        event.preventDefault();
        
        var id = $(this).data('id');
        var primary = $(this).data('primary');
        var secondary = $(this).data('secondary');
        console.log('Abbas'+primary);
        $('#primary_techinician option[value="'+primary+'"]').attr('selected','selected').trigger('change');
        $('#secondary_techinician option[value="'+secondary+'"]').attr('selected','selected').trigger('change');

        $("#project_id").val(id);
        $('#edit_technician_modal').modal('show');
    });

    $("form.validate2").validate({
          rules: {
            secondary_techinician:{
              required: true
            },
            primary_techinician:{
              required: true
            }
          }, 
          messages: {
            primary_techinician: "This field is required.",
            secondary_techinician: "This field is required.",
            
          },
          invalidHandler: function (event, validator) {
            //display error alert on form submit    
            },
            errorPlacement: function (label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');  
            },
            highlight: function (element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control'); 
            },
            unhighlight: function (element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control'); 
            },
            success: function (label, element) {
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('error-control').addClass('success-control');
              
            }
            // submitHandler: function (form) {
            // }
        });
});
</script>