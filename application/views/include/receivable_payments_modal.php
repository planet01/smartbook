<div class="modal_display">
<div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog responsive-scr-width">
    <div class="modal-content history_modal_height">
      <!-- action="<?=site_url($edit_status) ?>"  -->
          <div class="modal-header header-fixed">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Payments</h4>
          </div>
          <div class="modal-body send_mail_mymodal-body receivable-dt-pad" id="myModalDescription" style="overflow-x: auto;">
            <div class="row receivalble-row">
                  <div class="col-md-12">
                    <div class="receivable-overflowX">
                      <table class="table receivable-dt" id="example4" >
                        <thead class="receivable-dt-thead" >
                          <tr>
                            <th class="receivable-dt-th1">#</th>
                            <th class="receivable-dt-th2">Receipt No</th>
                            <th class="receivable-dt-th3">Date</th>
                            <th class="receivable-dt-th4">P.I. / Invoice #</th>
                            <th class="receivable-dt-th5">Amount Received</th>
                            <th class="receivable-dt-th6">Adjustment Amount</th>
                            <th class="receivable-dt-th7">Action</th>
                          </tr>
                        </thead>
                        <tbody id="payment_body" class="receivable-dt-tbody">

                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
            
          </div>
          <!-- <div class="modal-footer">
            <input type="hidden" value="" name="id" id="quotation_id1" >
            <input type="hidden" value="" name="status" id="status" >
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter2 my-bttn' type='button'>Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div> -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
   /* $(document).on("click", ".status_comment", function(event){
        event.preventDefault();
        
        var id = $(this).data('id');
        var status = $(this).attr('status-id');
        $("#quotation_id1").val(id);
        $("#status").val(status);
        $("#status_comment").val('');
        $('#history_modal').modal('show');
    });*/

    $("form.validate").validate({
          rules: {
            /*email_subject:{
              required: true
            },
            email_body:{
              required: true
            }*/
          }, 
          messages: {
            email_subject: "This field is required.",
            email_body: "This field is required."
          },
          invalidHandler: function (event, validator) {
            //display error alert on form submit    
            },
            errorPlacement: function (label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');  
            },
            highlight: function (element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control'); 
            },
            unhighlight: function (element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control'); 
            },
            success: function (label, element) {
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('error-control').addClass('success-control');
              
            }
            // submitHandler: function (form) {
            // }
        });
});

// $('table.saa').on('scroll', function() {
//   $("table.saa > *").width($("table.saa").width() + $("table.saa").scrollLeft());
// });

</script>