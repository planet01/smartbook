<div class="modal fade" id="project_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class='ajaxForm4 validate' action="<?=site_url('Project_summary/change_status') ?>" method='post' enctype='multipart/form-data'>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Change Status</h4>
          </div>
          <div class="modal-body send_mail_mymodal-body" id="myModalDescription">
            <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                            <label class="form-label">Status</label>
                             
                          </div>
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <select name="status_id" id="status_id" class="select2 form-control">
                                    <option value="0" selected >--- Select Status ---</option>
                                    <?php
                                    $status = ProjectStatusData();
                                    foreach($status as $k => $v){
                                    ?>
                                        <option value="<?= $v['status_id']; ?>" <?=( $v['status_id'] == @$data['status_id'])? 'selected': ''; ?> >
                                        <?= $v['status_name']; ?>
                                        </option>
                                    <?php
                                    }
                                    ?>
                                </select>
                              </div>
                          </div>
                          
                        </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                            <label class="form-label">Comments</label>
                             
                          </div>
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <textarea row="2" col="8" name="comments"></textarea>
                              </div>
                          </div>
                          
                        </div>
                    </div>
                  </div>
              </div>
            
          </div>
          <div class="modal-footer">
            <input type="hidden" value="" name="id" id="project_id_for_status" >
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter4 my-bttn mt-2' type='button'>Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".project_status", function(event){
        event.preventDefault();
        var id = $(this).data('id');
        var status_id = $(this).data('status-id');
        $('#status_id option[value="'+status_id+'"]').prop('selected', true);
        $('#status_id').trigger('change');
        $("#project_id_for_status").val(id);
        $('#project_status_modal').modal('show');
    });

    $("form.validate").validate({
          rules: {
            status_id:{
              required: true
            },
          }, 
          messages: {
            status_id: "This field is required.",
          },
          invalidHandler: function (event, validator) {
            //display error alert on form submit    
            },
            errorPlacement: function (label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');  
            },
            highlight: function (element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control'); 
            },
            unhighlight: function (element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control'); 
            },
            success: function (label, element) {
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('error-control').addClass('success-control');
              
            }
            // submitHandler: function (form) {
            // }
        });
});
</script>