<style>
    .rate {
    margin-top: 8px;
    float: left;
    height: 46px;
    padding: 0 10px;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
</style>
<div class="modal_display">
<div class="modal fade" id="ticket_approval" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content customer_modal_height">
        <form class='ajaxForm2 validate3' action="<?=site_url('Ticket_management/status_approval') ?>" method='post' enctype='multipart/form-data'>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Approval</h4>
          </div>
          <div class="modal-body ticket_approval-body" id="myModalDescription">
            <div class="row">
            <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-3">
                            <label class="form-label">Rating</label>
                          </div>
                          <div class="col-md-6">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <div class="rate">
                                    <input type="radio" id="star5" name="approval_rating" value="5" />
                                    <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="approval_rating" value="4" />
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="approval_rating" value="3" />
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="approval_rating" value="2" />
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="approval_rating" value="1" />
                                    <label for="star1" title="text">1 star</label>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-3">
                            <label class="form-label">Status</label>
                          </div>
                          <div class="col-md-6">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <select name="approval_status" class="select2 form-control">
                                    <option value='0' selected disabled>Selet Status</option>
                                    <option value='2'>Accept</option>
                                    <option value='3'>Reject</option>
                                </select>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <div class="radio radio-success responsve-receivable-radio">
                                    <input id="checkbox3" checked="true" type="radio" name="approval_type" value="0">
                                    <label class="" for="checkbox3"><b>Complains</b></label>
                                        
                                    <input id="checkbox4" type="radio" name="approval_type" value="1">
                                    <label class="" for="checkbox4"><b>Concerns</b></label>    
                                    
                                    <input id="checkbox5" type="radio" name="approval_type" value="2">
                                    <label class="" for="checkbox5"><b>Appreciation</b></label>    

                                </div> 
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-3">
                            <label class="form-label">Notes</label>
                          </div>
                          <div class="col-md-6">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <textarea name="approval_notes" class="form-control"></textarea>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
            
          </div>
          <div class="modal-footer">
            <input type="hidden"  name="ticket_id_status" id="ticket_id_status" >
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter2 my-bttn mt-2' type='button'>OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
 </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".ticket_approval", function(event){
        event.preventDefault();
        $('texarea[name="approval_notes"]').val('');
        $('select[name="approval_status"] option[value="0"]').attr('selected', 'selected').trigger('change');
        $('input[name="approval_rating"]:checkbox').removeAttr('checked');
        var id = $(this).data('id');
        $("#ticket_id_status").val(id);

        $('#ticket_approval').modal('show');
    });

    $("form.validate3").validate({
          rules: {
            status:{
              required: true
            },
            approval_rating:{
              required : true
            },
            approval_type:{
              required : true
            },
            approval_status:{
              required : true
            }
          }, 
          messages: {
            service_date: "This field is required.",
          },
          invalidHandler: function (event, validator) {
            //display error alert on form submit    
            },
            errorPlacement: function (label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');  
            },
            highlight: function (element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control'); 
            },
            unhighlight: function (element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control'); 
            },
            success: function (label, element) {
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('error-control').addClass('success-control');
              
            }
            // submitHandler: function (form) {
            // }
        });
});
</script>