<div class="modal_display">
<div class="modal fade" id="defaultModalCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content customer_modal_height">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel" class="semi-bold mymodal-customer-title">Add Customer</h4>
      </div>
      <div class="modal-body mymodal-customer-body customer_modal" id="myModalDescription">
            <div class="grid-body no-border">
                        <form class="ajaxForm2 validate2" action="<?php echo site_url($add_customer)?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Name:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_name" type="text" value="<?= @$data['user_name']; ?>" class="form-control" placeholder="Enter Name">
                                                </div>
                                
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Company Name:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_name" type="text" value="<?= @$data['user_name']; ?>" class="form-control" placeholder="Enter Company Name">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Display Name:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_display_name" type="text" value="<?= @$data['user_display_name']; ?>" class="form-control" placeholder="Enter Display Name">
                                                </div>
                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Company Email Address:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_email" type="text" value="<?= @$data['user_email']; ?>" class="form-control" placeholder="Enter E-mail">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">City:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_city" type="text" value="<?= @$data['user_city']; ?>" class="form-control" placeholder="Enter City">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Country:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <!--     <select class="form-control input-signin-mystyle select2" id="country" name="country">-->
                                                    <select class="form-control select2" id="user_country" name="user_country">
                                                        <option value="" selected="" disabled="">-- Select Country --</option>
                                                        <?php
                                                        foreach ($countries as $k => $v) {
                                                      ?>
                                                            <option value="<?= $v['country_id'] ?>" <?=( $v['country_id']==@ $data[ 'user_country']) ? 'selected' : ''; ?> >
                                                                <?= $v['country_name']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Phone:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_phone" type="text" value="<?= @$data['user_phone']; ?>" class="form-control" placeholder="Enter Phone">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Website:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="user_website" type="text" value="<?= @$data['user_website']; ?>" class="form-control" placeholder="Enter Website">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Status:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control input-signin-mystyle select2" id="user_is_active" name="user_is_active">
                                                        <option value="" selected="selected" disabled="">- Select Status -</option>
                                                        <option value="1" selected>Enabled</option>
                                                        <option value="0">Disabled</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Customer Type:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control select2" id="customer_type" name="customer_type">
                                                        <option value="" selected="" disabled="">-- Customer Type --</option>
                                                        <?php
                                                        foreach ($customer_type as $k => $v) {
                                                        ?>
                                                            <option value="<?= $v['id'] ?>" <?=( $v['id']==@ $data['customer_type']) ? 'selected' : ''; ?> ><?= $v['title']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Price List:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control select2" id="plan_type" name="plan_type">
                                                        <option value="" selected="" disabled="">-- Select Price --</option>
                                                        <?php
                                                        foreach ($price_list as $k => $v) {
                                                        ?>
                                                            <option value="<?= $v['price_plan_id'] ?>" <?=( $v['price_plan_id']==@ $data['plan_type']) ? 'selected' : ''; ?> ><?= $v['plan_slog']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                

                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">T.R.No. :</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="tax_reg" type="text" value="<?= @$data['tax_reg']; ?>" class="form-control" placeholder="Enter T.R.N.">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Currency:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control select2" id="currency_id" name="currency_id">
                                                        <option value="" selected="" disabled="">-- Select Currency --</option>
                                                        <?php
                                                        foreach ($currency as $k => $v) {
                                                        ?>
                                                            <option value="<?= $v['id'] ?>" <?=( $v['id']==@ $data['currency_id']) ? 'selected' : ''; ?> ><?= $v['title']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Address:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <!-- <input name="user_address" type="text" value="<?= @$data['user_address']; ?>" class="form-control" placeholder="Enter Address"> -->
                                                     <textarea id="myeditor3" name="user_address" class="form-control" value="" placeholder=""><?= @$data['user_address']; ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                     
                                <div class="col-md-12 ">
                                    <h3>Contact Details</h3>
                                      <div class="dataTables_wrapper-1440 dataTables_wrapper-1441">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th class="text-center cust_contact_det-dt-th1">Add/Remove Row</th>
                                              <th class="text-center cust_contact_det-dt-th2">Contact Person Name</th>
                                              <th class="text-center cust_contact_det-dt-th3">Contact Number</th>
                                              <th class="text-center cust_contact_det-dt-th4">Position</th>
                                              <th class="text-center cust_contact_det-dt-th5">Email Address</th>
                                            </tr>
                                          </thead>
                                          <tbody id="contact_field">
                                            <tr class="txtMult">
                                              <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                              <td colspan="6"></td>
                                            </tr>
                                              <?php
                                              if(@$contact != null && !empty(@$contact)){
                                              foreach($contact as $row){
                                             ?>

                                             <tr class="txtMult contact_tr">
                                                    <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                    <td>
                                                        <input type="text" class="contact_name" required style="width:100%" id="contact_name"  name="contact_name[]" 
                                                        value="<?= @$row['contact_name'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="contact_no" required style="width:100%" id="contact_no'+x+'" data-optional="0" name="contact_no[]" 
                                                        value="<?= @$row['contact_no'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="contact_position" required style="width:100%" id="contact_position'+x+'" data-optional="0" name="contact_position[]" 
                                                        value="<?= @$row['contact_position'] ?>" placeholder="" />
                                                    </td>
                                                     <td>
                                                        <input type="text" class="contact_email" required style="width:100%" id="contact_email'+x+'" data-optional="0" name="contact_email[]" 
                                                        value="<?= @$row['contact_email'] ?>" placeholder="" />
                                                    </td>
                                                </tr>
                                            <?php } } ?>

                                          </tbody>
                                        </table>
                                      </div>  
                                </div> 
                              
                                <div class="clearfix"></div>
                                     
                                <div class="col-md-12 ">
                                    <h3>Payment Terms</h3>
                                      <div class="dataTables_wrapper-1440 dataTables_wrapper-1441">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th class="text-center cust_pay_term-dt-th1" >Add/Remove Row</th>
                                              <th class="text-center cust_pay_term-dt-th2" >Terms & Condition</th>
                                              <th class="text-center cust_pay_term-dt-th3" >Percentage</th>
                                              <th class="text-center cust_pay_term-dt-th4" >No Of Days</th>
                                            </tr>
                                          </thead>
                                          <tbody id="terms_customFields">
                                            <tr class="txtMult">
                                              <td class="text-center"><a href="javascript:void(0);" class="terms_addCF">Add</a></td>
                                              <td colspan="6"></td>
                                            </tr>
                                              <?php
                                              if(@$terms != null && !empty(@$terms)){
                                              foreach($terms as $row){
                                             ?>

                                             <tr class="txtMult payment_percent_tr">
                                                    <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                    <td>
                                                        <input type="text" class="terms cust_pay_term-dt-td1" required id="terms"  name="terms[]" 
                                                        value="<?= @$row['payment_title'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="percentage cust_pay_term-dt-td2" required id="percentage'+x+'" data-optional="0" name="percentage[]" 
                                                        value="<?= @$row['percentage'] ?>" placeholder="" />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="payment_days cust_pay_term-dt-td3" required id="payment_days'+x+'" data-optional="0" name="payment_days[]" 
                                                        value="<?= @$row['payment_days'] ?>" placeholder="" />
                                                    </td>
                                                </tr>
                                            <?php } } ?>

                                          </tbody>
                                        </table>
                                      </div>  
                                </div>
                                  <div class="clearfix"></div>
                                
                                <div class="container-fluid">
                                    <h3>Warranty Terms</h3>
                                  <div class="dataTables_wrapper-1440 dataTables_wrapper-1441">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th class="text-center cust_warnty_term-dt-th1" >Add/Remove Row</th>
                                          <th class="text-center cust_warnty_term-dt-th2" >Warranty Type</th>
                                          <th class="text-center cust_warnty_term-dt-th3" >Percentage</th>
                                        </tr>
                                      </thead>
                                      <tbody id="warn_customFields">
                                        <tr class="warn_txtMult">
                                          <td class="text-center"><a href="javascript:void(0);" class="warn_addCF">Add</a></td>
                                          <td colspan="6"></td>
                                        </tr>
                                          <?php
                                          $warranty1 = json_decode($warranty, true);
                                          if(@$warranty1 != null && !empty(@$warranty1)){
                                            $check_data = Array();
                                            $new_data   = Array();
                                            $x =1;
                                            foreach($warranty1 as $warranty_type){
                                         ?>

                                            <tr class="warn_txtMult">
                                                <td class="text-center cust_warnty_term-dt-td1"><a href="javascript:void(0);" 
                                                    class="warn_remCF">Remove</a></td>
                                                <td>
                                                    <input type="text" class="title cust_warnty_term-dt-td2" required id="title<?=$x;?>" name="warranty_title[]"
                                                    value="<?= @$warranty_type['title'] ?>" placeholder="" />
                                                </td>
                                                <td>
                                                    <input type="text" class="percentage txtboxToFilter cust_warnty_term-dt-td3" required id="percentage<?=$x;?>" name="warranty_percentage[]"
                                                    value="<?= @$warranty_type['warranty_percentage'] ?>" placeholder="" />
                                                </td>
                                                
                                            </tr>
                                        <?php 
                                            }
                                         } 
                                        ?>

                                      </tbody>
                                    </table>
                                  </div>  
                                </div>
                                    
                                
                                <div class="clearfix"></div>    

                                
                               <!--  <div class="col-md-offset-3 col-md-6">
                                   <div class="form-group">
                                       <?php 
                                       if(!empty($data['user_id']) && @$data['user_id'] != null){
                                           $appendedFiles = array();
                                           $file_path = dir_path().$image_upload_dir.'/'.@$data['user_image'];
                                             if (!is_dir($file_path) && file_exists($file_path)) {
                                               // $current_img = site_url($image_upload_dir."/".$categoryData['user_image']);
                                               @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['user_image']));
                                               $appendedFiles[] = array(
                                                 "name" => @$data['user_image'],
                                                 "type" => $file_details['mime'],
                                                 "size" => filesize($file_path),
                                                 "file" => site_url($image_upload_dir."/". @$data['user_image']),
                                                 "data" => array(
                                                   "url" => site_url($image_upload_dir."/". @$data['user_image']),
                                                   "image_white_file_id" => $data['user_id']
                                                 )
                                               );
                                             }
                                           $appendedFiles = json_encode($appendedFiles);
                                       }
                                       ?>
                                       <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files='<?php echo @$appendedFiles;?>'>
                                   </div>
                               </div> -->
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <!--           <button class="btn btn-success btn-cons" type="submit"><?php // ($page_title == 'add')?'Add New':'Edit'; ?> Customer</button> -->
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter2 my-bttn mt-5" type="button">
                                            <?= ($page_title == 'add')?'':''; ?>Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['user_id']; ?>">
                                        <input type="hidden" name="user_email_old" value="<?= @$data['user_email']; ?>">
                                        <input type="hidden" name="user_phone_old" value="<?= @$data['user_phone']; ?>">
                                        <input type="hidden" name="user_image_old" value="<?= @$data['user_image']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
            </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".myModalBtnCustomer", function(event){
        event.preventDefault();
        $('#defaultModalCustomer').modal('show');
    });

        $("form.validate2").validate({
            rules: {
                user_name: {
                    required: true
                },
                user_company_name: {
                    required: true
                },
                user_display_name: {
                    required: true
                },
                /*user_address: {
                    required: true
                },*/
                user_email:{
                    required: true,
                    email: true
                },
                user_city: {
                    required: true
                },
                user_country: {
                    required: true
                },
                user_phone: {
                    required: true
                },
               /* user_website: {
                    required: true,
                    url: true
                },*/
                user_is_active: {
                    required: true
                },
                 customer_type:{
                    required: true
                },
                plan_type:{
                    required: true
                }
            },
            messages: {
                user_name: "This field is required.",
                user_company_name: "This field is required.",
                user_display_name: "This field is required.",
                user_address: "This field is required.",
                user_email:{
                    required: "This field is required.",
                    email: "Invalid E-mail Address"
                },
                user_city: "This field is required.",
                user_country: "This field is required.",
                user_phone: "This field is required.",
                user_website:{
                    required: "This field is required.",
                    url: "This field required correct format."
                },
                user_is_active: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit    
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate2").change(function() {
            $('form.validate2').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    

        var max_fields      = 6; 
        var add_button      = $("#terms_customFields .terms_addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="txtMult payment_percent_tr">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter terms" required  style="width:100%" id="terms'+x+'" data-optional="0" name="terms[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter percentage" required  style="width:100%" id="percentage'+x+'" data-optional="0" name="percentage[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter payment_days" required  style="width:100%" id="payment_days'+x+'" data-optional="0" name="payment_days[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#terms_customFields").append(temp);
          // }
        });
        $("#terms_customFields").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });
        //contact
        var max_fields      = 6; 
        var add_button      = $("#contact_field .addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="txtMult contact_tr">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code txtboxToFilter contact_name" required  style="width:100%" id="contact_name'+x+'" data-optional="0" name="contact_name[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code  txtboxToFilter contact_no" required  style="width:100%" id="contact_no'+x+'" data-optional="0" name="contact_no[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter contact_position" required  style="width:100%" id="contact_position'+x+'" data-optional="0" name="contact_position[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter contact_email" required  style="width:100%" id="contact_email'+x+'" data-optional="0" name="contact_email[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#contact_field").append(temp);
          // }
        });
        $("#contact_field").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });

        var max_fields      = 6; 
        var add_button      = $("#new_customFields .new_addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="new_txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="new_remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="code quantity txtboxToFilter project_terms" required  style="width:100%" id="project_terms'+x+'" data-optional="0" name="project_terms[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#new_customFields").append(temp);
          // }
        });
        $("#new_customFields").on('click','.new_remCF',function(){
            $(this).parent().parent().remove();
        });

        var max_fields      = 6; 
        var add_button      = $("#warn_customFields .warn_addCF");
        var x = 1; 
        var box_size = "<?php echo sizeof(json_decode($warranty)); ?>";
        console.log(box_size);
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
          // if(x < max_fields){
            x++;
            var temp = '<tr class="warn_txtMult">';
            temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="warn_remCF">Remove</a></td>';
            temp += '<td><input type="text" class="title" required style="width:100%" id="title'+x+'" name="warranty_title[]" placeholder="" /></td>';
            temp += '<td><input type="text" class="percentage txtboxToFilter" required style="width:100%" id="percentage'+x+'" name="warranty_percentage[]"  placeholder="" /></td>';
            temp += '</tr>';
            $("#warn_customFields").append(temp);
          // }
        });
        $("#warn_customFields").on('click','.warn_remCF',function(){
            $(this).parent().parent().remove();
        });
</script>
