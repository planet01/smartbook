<div class="modal_display">
<div class="modal fade" id="send_mail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content customer_modal_height">
        <form class='ajaxForm validate' action="<?=site_url($mail) ?>" method='post' enctype='multipart/form-data'>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Send Mail</h4>
          </div>
          <div class="modal-body send_mail_mymodal-body" id="myModalDescription">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-2">
                            <label class="form-label">From</label>
                          </div>
                          <div class="col-md-10">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input value="<?= company_email_string()?>" name='email_from' type='text' class='form-control' placeholder='Enter Text'>
                              </div>
                            
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-2">
                            <label class="form-label">To</label>
                          </div>
                          <div class="col-md-10">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input value="<?= predefined_email_string()?>" name='email_to' type='text' class='form-control' placeholder='Enter Text'>
                              </div>
                            
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                            <label class="form-label">Subject</label>
                          </div>
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input name='email_subject' type='text' class='form-control' placeholder='Enter Text'>
                              </div>
                            
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                            <label class="form-label">Body</label>
                             
                          </div>
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>

                                <textarea name="email_body" class="form-control"></textarea>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            
          </div>
          <div class="modal-footer">
            <input type="hidden" value="" name="id" id="quotation_id" >
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter my-bttn mt-2' type='button'>Send</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
 </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".send_mail", function(event){
        event.preventDefault();
        
        var id = $(this).data('id');
        $("#quotation_id").val(id);
        $('#send_mail_modal').modal('show');
    });

    $("form.validate").validate({
          rules: {
            email_subject:{
              required: true
            },
            email_body:{
              required: true
            }
          }, 
          messages: {
            email_subject: "This field is required.",
            email_body: "This field is required."
          },
          invalidHandler: function (event, validator) {
            //display error alert on form submit    
            },
            errorPlacement: function (label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');  
            },
            highlight: function (element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control'); 
            },
            unhighlight: function (element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control'); 
            },
            success: function (label, element) {
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('error-control').addClass('success-control');
              
            }
            // submitHandler: function (form) {
            // }
        });
});
</script>