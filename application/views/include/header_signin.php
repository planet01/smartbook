<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Admin Panel <?= ($title != '' )?'| '.$title:''; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<link rel="icon" href="<?= base_url()?>assets/admin/img/favicon-sb.png" type="image/x-icon" />
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="<?php echo base_url()?>assets/admin/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url()?>assets/admin/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>assets/admin/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>assets/admin/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>assets/admin/css/animate.min.css" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link href="<?php echo base_url()?>assets/admin/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>assets/admin/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>assets/admin/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->

<!-- Start My plugin validation -->
<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/toastr.css">
<!-- <link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/jquery_validation_engine/template.css">
<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/jquery_validation_engine/validationEngine.jquery.css"> -->

<!-- end My plugin validation -->

<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/validation/jquery-validate/css/jquery.validate.css">
<script type="text/javascript" src="<?= base_url()?>assets/admin/js/jquery.min.js"></script>

<script src="<?= base_url()?>assets/admin/myplugin/validation/jquery-validate/js/jquery.validate.js"></script>

<!-- start My plugin loader -->
<link rel="stylesheet" href="<?= base_url()?>assets/admin/myplugin/loader/css/main.css">
<!-- end My plugin loader -->
<!-- sweetalert -->
<link href="<?= base_url(); ?>assets/admin/myplugin/sweetalert/npm/sweetalert.css" rel="stylesheet" type="text/css" media="screen"/>
<script src="<?= base_url(); ?>assets/admin/myplugin/sweetalert/npm/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/myplugin/sweetalert/dist/sweetalert.min.js"></script>
<style>
 
.swal-footer {
    text-align: center;
}

.swal-button--danger:not([disabled]):hover {
    background-color: #000 !important;
}

.swal-button--danger {
    padding: 10px 33px;
    background-color: #2e8fda;
}

.swal-icon {
    border-color: #000 !important;
}

.swal-icon--warning__body, .swal-icon--warning__dot {
    background-color: red;
}
 
.swal-button--danger:focus {
    box-shadow: none;
}
.swal-button--cancel:focus{
	box-shadow: none;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="error-body no-top">
<div class="loading-wrapper">
    <div class="loading"></div>
</div>