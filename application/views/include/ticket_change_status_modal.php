<div class="modal_display">
  <div class="modal fade" id="ticket_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="min-width: 800px;">
      <div class="modal-content customer_modal_height">
        <form class='ajaxForm validate2' action="<?= site_url('Ticket_management/change_status') ?>" method='post' enctype='multipart/form-data'>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Update Status</h4>
          </div>
          <div class="modal-body ticket_status-body" id="myModalDescription">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-4">
                      <label class="form-label">Service Date</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <input value="" name='service_date' type='text' class='datepicker form-control' placeholder=''>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-5">
                      <label class="form-label">Next Follow Up</label>
                    </div>
                    <div class="col-md-7">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <input value="" name='next_follow_up' type='text' class='form-control datepicker' placeholder=''>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-4">
                      <label class="form-label">Start Time</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <input value="" name='start_time' type='time' class='form-control' placeholder=''>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-5">
                      <label class="form-label">End Time</label>
                    </div>
                    <div class="col-md-7">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <input value="" name='end_time' type='time' class='form-control' placeholder=''>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-4">
                      <label class="form-label">No Of Hours</label>
                    </div>
                    <div class="col-md-8">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <input value="" name='no_of_hours' type='text' class='form-control txtboxToFilterCustom' placeholder=''>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-5">
                      <label class="form-label">Status </label>
                    </div>
                    <div class="col-md-7">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <select name='status' id="change_status_status" class="select2 w-100">
                          <option disabled selected>Select status</option>
                          <option value='0'>Pending </option>
                          <option value='1'>Partially </option>
                          <option value='3'>Pending By Client </option>
                          <option value='2'>Completed </option>
                        </select>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="col-md-12">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-2">
                      <label class="form-label">Service </label>
                    </div>
                    <div class="col-md-10">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <select name='service_id' id='status_service_id' class="custom_select w-100">
                          <option disabled selected>-- Select Service --</option>
                          <?php
                            foreach($product as $k => $v){
                              ?>
                              <option value='<?= $v['product_id']?>'><?= $v['product_sku'].'-'.$v['product_name']?></option>
                              <?php
                            } 
                          ?>
                        </select>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              

              

              <div class="col-md-12">
                <div class="form-group">
                  <div class="row form-row">
                    <div class="col-md-2">
                      <label class="form-label">Detail</label>
                    </div>
                    <div class="col-md-10">
                      <div class="input-with-icon right controls">
                        <i class=""></i>
                        <textarea name="service_detail" class="form-control" style="height:100px"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              

            </div>

          </div>
          <div class="modal-footer">
            <input type="hidden" value="" name="ticket_id_status_change" id="ticket_id_status_change">
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter my-bttn mt-2' type='button'>OK</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    
    $(document).on("click", ".ticket_status_change", function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      // $.ajax({
      //   url: "<?php echo site_url('Ticket_management/fetch_ticket'); ?>",
      //   dataType: "json",
      //   type: "GET",
      //   data: {
      //     id: id,

      //   },
      //   cache: false,
      //   success: function(ticketData) {
      //     if(ticketData.statusData != undefined)
      //     {
            $('input[name="service_date"]').val('');
            $('input[name="start_time"]').val('');
            $('input[name="end_time"]').val('');
            $('input[name="no_of_hours"]').val('');
            $('#status_service_id option[value="0"]').attr('selected', 'selected').trigger('change');
            $('#change_status_status option[value=0]').prop('selected', 'selected').trigger('change');
            $('[name="service_detail"]').val('');
            $('input[name="next_follow_up"]').val('');
      //     }
          

          $("#ticket_id_status_change").val(id);
          $('#ticket_status').modal('show');
      //   }
      // });

    });


    $("form.validate2").validate({
      rules: {
        service_date: {
          required: true
        },
        start_time: {
          required: true
        },
        end_time: {
          required: true
        },
        no_of_hours: {
          required: true
        },
        service_id:{
          required:true
        },
        status: {
          required: true
        },
        // next_follow_up: {
        //   required: true
        // }
      },
      messages: {
        service_date: "This field is required.",
        service_type: "This field is required.",
        service_detail: "This field is required."
      },
      invalidHandler: function(event, validator) {
        //display error alert on form submit    
      },
      errorPlacement: function(label, element) { // render error placement for each input type   
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        $('<span class="error"></span>').insertAfter(element).append(label);
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('success-control').addClass('error-control');
      },
      highlight: function(element) { // hightlight error inputs
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        var parent = $(element).parent();
        parent.removeClass('success-control').addClass('error-control');
      },
      unhighlight: function(element) { // revert the change done by hightlight
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent();
        parent.removeClass('error-control').addClass('success-control');
      },
      success: function(label, element) {
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('error-control').addClass('success-control');

      }
      // submitHandler: function (form) {
      // }
    });


  });
</script>