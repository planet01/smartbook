<div class="modal fade" id="status_comment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class='ajaxForm4 validate' action="<?=site_url($edit_status) ?>" method='post' enctype='multipart/form-data'>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myModalLabel" class="semi-bold mymodal-title">Status Comment</h4>
          </div>
          <div class="modal-body send_mail_mymodal-body" id="myModalDescription">
            <div class="row">
                  <?php if(isset($module) && ($module == 'quotation' || $module == 'delivery')){?>
                  <div class="col-md-12 po_box">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                            <label class="form-label">P.O.#</label>
                          </div>
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <input type="text" name="po_no" id="po_no" class="form-control"/>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <?php } ?>
                  <div class="col-md-12">
                    <div class="form-group">
                        <div class="row form-row">
                          <div class="col-md-12">
                            <label class="form-label">Comments</label>
                             
                          </div>
                          <div class="col-md-12">
                              <div class="input-with-icon right controls">
                                <i class=""></i>
                                <textarea name="status_comment" id="myeditor" class="form-control"></textarea>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            
          </div>
          <div class="modal-footer">
            <input type="hidden" value="" name="id" id="quotation_id1" >
            <input type="hidden" value="" name="status" id="status" >
            <button class='btn btn-success btn-cons ajaxFormSubmitAlter4 my-bttn mt-2' type='button'>Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", ".status_comment", function(event){
        event.preventDefault();
        
        var id = $(this).data('id');
        var status = $(this).attr('status-id');
        $("#quotation_id1").val(id);
        $("#status").val(status);
        if(status != 4){
          $(".po_box").hide();
        }else{
          $(".po_box").show();
        }
        /*$("#status_comment").val('');*/

        $('#status_comment_modal').modal('show');
    });

    $("form.validate").validate({
          rules: {
            status_comment:{
              required: true
            },
            /*email_body:{
              required: true
            }*/
          }, 
          messages: {
            email_subject: "This field is required.",
            email_body: "This field is required."
          },
          invalidHandler: function (event, validator) {
            //display error alert on form submit    
            },
            errorPlacement: function (label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');  
            },
            highlight: function (element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control'); 
            },
            unhighlight: function (element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control'); 
            },
            success: function (label, element) {
              var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('error-control').addClass('success-control');
              
            }
            // submitHandler: function (form) {
            // }
        });
});
</script>