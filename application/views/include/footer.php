</div>

<!-- <script src="<?= base_url()?>assets/admin/myplugin/validation/jquery-validate/js/jquery-3.1.1.js"></script> -->
<script src="<?= base_url()?>assets/admin/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/breakpoints.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-lazyload/jquery.lazyload.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="<?= base_url()?>assets/admin/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- <script src="<?= base_url()?>assets/admin/plugins/pace/pace.min.js" type="text/javascript"></script> -->
<script src="<?= base_url()?>assets/admin/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-ricksaw-chart/js/raphael-min.js"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-ricksaw-chart/js/d3.v2.js"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-ricksaw-chart/js/rickshaw.min.js"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-sparkline/jquery-sparkline.js"></script>
<script src="<?= base_url()?>assets/admin/plugins/skycons/skycons.js"></script>
<script src="<?= base_url()?>assets/admin/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
<!-- <script src="<?= base_url()?>assets/admin/plugins/jquery-gmap/gmaps.js" type="text/javascript"></script> -->
<script src="<?= base_url()?>assets/admin/plugins/Mapplic/js/jquery.easing.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/Mapplic/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/Mapplic/js/hammer.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/Mapplic/mapplic/mapplic.js" type="text/javascript"></script>
    
<script src="<?= base_url()?>assets/admin/plugins/jquery-flot/jquery.flot.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-metrojs/MetroJs.min.js" type="text/javascript" ></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN Form element PAGE LEVEL PLUGINS -->
<!-- <script src="<?= base_url()?>assets/admin/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>  -->
<script src="<?= base_url()?>assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-autonumeric/autoNumeric.js" type="text/javascript"></script>
<!-- <script src="<?= base_url()?>assets/admin/plugins/ios-switch/ios7-switch.js" type="text/javascript"></script> -->
<!-- <script src="<?= base_url()?>assets/admin/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script> -->
<!-- <script src="<?= base_url()?>assets/admin/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script> -->
<!-- <script src="<?= base_url()?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script> -->
<script src="<?= base_url()?>assets/admin/plugins/bootstrap-tag/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/boostrap-clockpicker/bootstrap-clockpicker.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<!-- <script src="<?= base_url()?>assets/admin/js/form_elements.js" type="text/javascript"></script> -->
<!-- END Form element PAGE LEVEL PLUGINS -->

<!-- BEGIN datatable PAGE LEVEL JS -->
<script src="<?= base_url()?>assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>    
<script src="<?= base_url()?>assets/admin/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-datatable/js/jquery.dataTables.min.js" type="text/javascript" ></script>
<script src="<?= base_url()?>assets/admin/plugins/jquery-datatable/extra/js/dataTables.tableTools.min.js" type="text/javascript" ></script>
<script type="text/javascript" src="<?= base_url()?>assets/admin/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="<?= base_url()?>assets/admin/plugins/datatables-responsive/js/lodash.min.js"></script>
<script src="<?= base_url()?>assets/admin/js/datatables.js" type="text/javascript"></script>

<!-- END datatable PAGE LEVEL PLUGINS -->



<!-- BEGIN CORE TEMPLATE JS -->
<script src="<?= base_url()?>assets/admin/js/core.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/js/chat.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin/js/demo.js" type="text/javascript"></script>
<!-- <script src="<?= base_url()?>assets/admin/js/dashboard_v2.js" type="text/javascript"></script> -->



<!-- Start My plugin validation -->
<!-- <script src="<?= base_url()?>assets/admin/myplugin/validation/jquery_validation_engine/jquery.validationEngine.js"></script>
<script src="<?= base_url()?>assets/admin/myplugin/validation/jquery_validation_engine/jquery.validationEngine-en.js"></script>-->
<script src="<?= base_url()?>assets/admin/myplugin/validation/custom.js"></script>
<script src="<?= base_url()?>assets/admin/myplugin/validation/jquery.browser.js"></script>
<script src="<?= base_url()?>assets/admin/myplugin/validation/jquery.form.js"></script>
<script src="<?= base_url()?>assets/admin/myplugin/validation/toastr.js"></script> 

<!-- <script src="<?= base_url()?>assets/admin/myplugin/jquery-validate/js/jquery-3.1.1.js"></script>
<script src="<?= base_url()?>assets/admin/myplugin/jquery-validate/js/jquery.validate.js"></script> -->


<!-- end My plugin validation -->

<!-- start My plugin loader -->
<script src="<?= base_url()?>assets/admin/myplugin/loader/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="<?= base_url()?>assets/admin/myplugin/loader/js/loader.js"></script>
 <script src="<?php echo base_url()?>assets/ckeditor/ckeditor.js"></script>
<?php 
if (@$ckeditor == "yes") {
?>
<script type="text/javascript">
  CKEDITOR.replace('myeditor');
</script>
<?php
}
?>


<?php 
if (@$ckeditor1 == "yes") {
?>
<script type="text/javascript">
  CKEDITOR.replace('myeditor1');
</script>
<?php
}
?>

<?php 
if (@$ckeditor2 == "yes") {
?>
<script type="text/javascript">
  CKEDITOR.replace('myeditor2');
</script>
<?php
}
?>
<?php 
if (@$ckeditor3 == "yes") {
?>
<script type="text/javascript">
  CKEDITOR.replace('myeditor3');
</script>
<?php
}
?>
 <script src="<?php echo base_url()?>assets/ckeditor/sample.js"></script> 

<!-- end My plugin loader -->

<script type="text/javascript">

$(document).ready(function () {

    

    $('[data-toggle="tooltip"]').tooltip(); 
	$('body').on('keyup keydown keypress', '.txtboxToFilter', function (event) {
		
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
	        // let it happen, don't do anything
	        // console.log("if---"+event.keyCode);
	    }
	    else {
	    	// console.log("else---"+event.keyCode);
	        // Ensure that it is a number and stop the keypress
	        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) {
	        	// console.log("else---if---"+event.keyCode);
	            event.preventDefault();
	        }
	    }
	});

	$('body').on('keyup keydown keypress', '.txtboxToFilterCustom', function (e) {
		
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 110 || event.keyCode == 190) {
	        // let it happen, don't do anything
	        // console.log("if---"+event.keyCode);
	    }
	    else {
	    	// console.log("else---"+event.keyCode);
	        // Ensure that it is a number and stop the keypress
	        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) {
	        	// console.log("else---if---"+event.keyCode);
	            event.preventDefault();
	        }
	    }
	});

    $(".live-tile,.flip-list").liveTile();

    // $('#example3').dataTable();
    function readURL(input) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#preview').html("<img src='"+e.target.result+"' width='200px'/>"); 
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#img_src").change(function(){
		if($(this).val() != ""){
	    	readURL(this);
	    }else{
	    	$('#preview').html(" ");
	    }
	});

	function readURL2(input) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#preview2').html("<img src='"+e.target.result+"' width='200px'/>"); 
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#img_src2").change(function(){
		if($(this).val() != ""){
	    	readURL2(this);
	    }else{
	    	$('#preview2').html(" ");
	    }
	});

    // $('#example3').dataTable();
});
</script>
<style type="text/css">
.image-detail-bg-pattern{
	/*background-size:  cover;*/
	background-color: #444;
	/*background-image: url(<?= site_url(); ?>uploads/background-pattern.jpg); 	*/
}

</style>
<script type="text/javascript">
	// jQuery(document).ready(function() {
	// 	var tz = jstz.determine();
	//     var timezone = tz.name();
	//     <?php
	//     if (empty($_SESSION['timezone_set']) && @$_SESSION['timezone_set'] == null) {
	//     ?>
	//     $.ajax({
 //                url: "<?= site_url(); ?>user/date_default_timezone_set/?timezone="+timezone,
 //                dataType: "json",
 //                error: function(jqXHR, textStatus, errorThrown) {
 //                    console.log("Request not completed.Please try Again");
 //                },
 //                success: function(data) {
 //                	console.log(data);
 //                }
 //            });
	//     <?php
	// 	}else{
	// 	?>
	// 	return false;
	// 	<?php
	// 	}
	// 	?>;
	// });

function delete_all_data(){
      var x = confirm("Do you really want to delete all the data?");
      if (x){
      	   $.ajax({
            url: "<?php echo site_url('Clear_all_data/delete'); ?>",
            dataType: "json",
            type: "POST",
            data: {
                    delete: true
                  },
            cache: false,
            success: function(data) {
               success(data['success'])
            }
          });
      }
  }
</script>
</body>
</html>