<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">My Account</a> </li>
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>My - <span class="semi-bold">Account</span></h3> </div>
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>My Acount <span class="semi-bold">Form</span></h4> </div>
                    <div class="grid-body no-border">
                        <br>
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <form class="ajaxForm validate" action="<?php echo site_url('my_account/update')?>" method="post">
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <span class="help">e.g. "some@example.com"</span>
                                        <div class="controls">
                                            <div class="input-with-icon right">
                                            	<i class=""></i>
                                                <input type="text" name="email" class="form-control" placeholder="Enter E-mail" value="<?= $data['email']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Password</label> <span class="help">e.g. "Mona Lisa Portrait"</span>
                                        <div class="controls">
                                            <div class="input-with-icon right"> <i class=""></i>
                                                <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password" value="<?= $passwords; ?>"> </div>
                                            <input type="checkbox" id="ch_pass" value="0" style="position: relative;top: 2px;" /> <span id="ch_passtxt">Show Password</span> </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-cons pull-right" type="submit">Submit</button>
                                        <input type="hidden" name="id" value="<?= $data['id']; ?>">
                                        <input type="hidden" name="email_old" value="<?= $data['email']; ?>"> </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->

<script>
    $(document).ready(function() {
		$("form.validate").validate({
		    rules: {
		        email: {
		            required: true,
		            email: true
		        },
		        password: {
		            required: true
		        }
		    },
		    messages: {
		        email: {
		            required: "This field is required.",
		            email: "Invalid E-mail Address"
		        },
		        password: "This field is required."
		    },
		    invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');

          
        }
        // submitHandler: function (form) {

        // }
      });
    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
</script>