
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">Pending Quotations </a> </li>
            
        </ul>
        <div class="row">
<div class="col-md-12">
    <div class="row">
    
    <div class="col-md-3 receivable-subdiv-amount">
        <label class="text-center"><b>Quotation not yet converted into Sales</b></label>
        <h3 class="text-center" style="font-weight: 600;">AED <?=number_format((float)$pendingquotationcount, 2, '.', '') ?></h3>
        <hr style="width:100%;text-align:right;margin-right:0;border: 3px solid lightblue;">
    </div>
    </div>
</div>
</div>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>Pending Quotations</h4>
                    </div>
                    <div class="grid-body">
                        <form class="row validate" style="margin-bottom:20px" autocomplete="off" id="dashboard-form">
                        <div class="col-sm-12" style="margin-bottom:25px">
                        <label class="radio-inline">
                        <input type="checkbox" name="types[]" class="radiobuttons" id="inlineRadio1" value="1" <?= isset($types) && in_array(1,$types)?'checked':''?> ><b> Invoiced but advanced not yet recieved</b>
                        </label>
                        <label class="radio-inline">
                        <input type="checkbox" name="types[]" class="radiobuttons" id="inlineRadio2" value="5" <?= isset($types) && in_array(5,$types)?'checked':''?> > <b>Include Client Not Interested</b>
                        </label>
                        <label class="radio-inline">
                        <input type="checkbox" name="types[]" class="radiobuttons" id="inlineRadio3" value="6" <?= isset($types) && in_array(6,$types)?'checked':''?> > <b>Include Decision Postponed</b>
                        </label>
                        <label class="radio-inline">
                        <input type="checkbox" name="types[]" class="radiobuttons" id="inlineRadio3" value="3" <?= isset($types) && in_array(3,$types)?'checked':''?> > <b>Not converted to invoice</b>
                        </label>
                        </div>
                        <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >Customer:</label>
                                <select name="customer_id" id="customer_id" class="select2 form-control" >
                                    <option value>Select Customer</option>
                                    <?php
                                        foreach($customerData as $k => $v){
                                        ?>
                                        <option value="<?= $v['user_id']; ?>" <?= @$customer_id == $v['user_id']?'selected':''?> >
                                        <?= $v['user_name']; ?>
                                        </option>
                                        <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >From Date:</label>
                                <input type="text" value="<?=@$fromdate?>" name="fromdate" id="fromdate" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3" >
                                <label >To Date:</label>
                                <input type="text" value="<?=@$todate?>" name="todate" id="todate" class="datepicker form-control">
                            </div>
                            
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                            <button type="submit" class="btn btn-primary" style="margin-top:25px">Submit</button>
                            </div>
                            
                        </form>
                        <table class="table" id="example3" >
                            <thead>
                                <tr>
                                    <th class="prod_sr_no">Sr.No.</th>
                                    <th class="prod_name">Date</th>
                                    <th>Document #</th>
                                    <th>Customer Name</th>
                                    <th>Status</th>
                                    <th>Net Amount</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                              foreach($pendingquotation as $key =>$item){
                            ?>
                                <tr>
                                    <td><?= ++$key ?></td>
                                    <td><?= date("d-m-Y", strtotime($item["quotation_date"])) ?></td>
                                    <td><?php echo @$setting["company_prefix"] . @$setting["quotation_prfx"]; ?><?= ($item['quotation_revised_no'] > 0) ? @$item['quotation_no'] . '-R' . number_format(@$item['quotation_revised_no']) : @$item['quotation_no']; ?></td>
                                    <td><?= @$item['user_name']  ?></td>
                                    <td>
                                      <input type="hidden" class="quotation_stat" value="<?= $item['quotation_status'] ?>" />
                                      <?php
                                      $QuotationStatusData = QuotationStatusData();
                                      foreach ($QuotationStatusData as $k2 => $item2) {
                                        if ($k2 == $item['quotation_status']) {
                                          if ($k2 == 2) { ?>
                                            <span class="label label-success"><?= $item2 . '-' . $item['quotation_revised_no']; ?></span>
                                          <?php
                                          } else {

                                          ?>
                                            <span class="label label-success"><?= $item2; ?></span>
                                      <?php
                                          }
                                        }
                                      }
                                      ?>
                                    </td>
                                    <td><?= @$item['net_amount'] ?></td>
                                    <td>
                                    <a href="<?= site_url($print . '?id=' . @$item['quotation_id'] . '&header_check=0&view=0') ?>" target='_blank' title="View Detail" data-id="<?= @$item['quotation_id']; ?>" class="btn-primary btn btn-sm"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <?php
                              }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        
        $( ".datepicker" ).datepicker({
           format: "dd-mm-yyyy",
           autoclose: true
           
        });
    });
    $("form.validate").validate({
      rules: { 
        fromdate:{
          required: true
        }, 
        todate:{
          required: true
        }
      }, 
      messages: {
        fromdate:"This field is required.",
        todate:"This field is required."
      },
        invalidHandler: function (event, validator) {
        //display error alert on form submit    
          error("Please input all the mandatory values marked as red");
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });
      $(document).ready(function() {
          $("#dashboard-form").on("submit", function(event){
              event.preventDefault();
              var date_from = $("#fromdate").valid();
              var date_to   = $("#todate").valid();
              if(!date_from || !date_to){
                return false;
              }
              else
              {
                document.getElementById('dashboard-form').submit();
              }
          });
      });
</script>