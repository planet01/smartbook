<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <?php
      $check_add =false;
      if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
          if ($v['module_id'] == 29) {
            if ($v['add'] == 1) {
              $check_add = true;
            }
          }
        }
      } else {
        $check_add = true;
      }
      ?>
    <div class="content">
        <div class="d-flex">
            <ul class="breadcrumb pull-left">
                <li>
                <p>Dashboard</p>
                </li>
                <li><a href="#" class="active">View All <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
            </ul>
            <a class="btn btn-primary pull-right" href="<?= site_url('Ticket_management/add'); ?>">Create Ticket</a>
        </div>
      
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            
            <div class="grid-body" style="margin-top: 50px">
              <div class="row">
                  <form class="validate">
                <div class="col-md-2 col-lg-2" style="display:none">
                      <div class="form-group">
                        <div class="input-with-iconloat right controls">
                          <i class=""></i>
                          <div class="radio radio-success responsve-receivable-radio">
                            <input id="checkbox1" type="radio" name="invoice_type" value="0">
                            <label class="" for="checkbox1"><b>Pending</b></label>
                                
                            <input id="checkbox2" checked="true" type="radio" name="invoice_type" value="1">
                            <label class="" for="checkbox2"><b>All</b></label>      
                          </div> 
                        </div>
                      </div>
                </div>
                <div class="col-md-4 col-lg-4" id="due_since_div">
                    
                </div>
                <div id='print_history_div'>
                  <div class="col-md-3 col-lg-3">
                      <div class="row">
                        <div class="col-md-3 col-lg-3 date_input">
                          <div class="form-group mg-10">
                            <div class="input-with-icon right controls">
                                <label for="checkbox1"><b>From Date</b></label>     
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9 date_input">
                          <i class=""></i>
                          <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>"  >
                        </div>
                      </div>
                  </div>
                  <div class="col-md-3 col-lg-3">
                      <div class="row">
                        <div class="col-md-3 col-lg-3 date_input">
                          <div class="form-group mg-10">
                            <div class="input-with-icon right controls">
                                <label for="checkbox1"><b>To Date</b></label>     
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9 date_input">
                          <i class=""></i>
                          <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>"  >
                        </div>
                      </div>
                  </div>

                  <div class="col-md-2 col-lg-2 responsve-mt-10 ">
                    <div class="row">
                      <div class="col-md-9 col-lg-10">
                        <div class="form-group">
                          <a class="btn btn-warning" id="print_history_button" >Print Service History</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3 col-lg-3">
                  <div class="row">
                    <div class="col-md-3 col-lg-3">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Customer</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                      <i class=""></i>
                      <select name="customer_id" id="customer_id" class="select2 form-control">
                        <option value="0" selected disabled >--- Select Customer ---</option>
                        <?php
                        foreach($customerData as $k => $v){
                        ?>
                        <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['customer_id'])? 'selected': ''; ?> >
                        <?= $v['user_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div>

                <div class="col-md-3 col-lg-3">
                  <div class="row">
                    <div class="col-md-3 col-lg-3">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>End User</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                      <i class=""></i>
                      <select name="end_user" id="end_user" class="select2 w-100">
                        <option value="0" selected >--- Select End User ---</option>
                      </select>  
                    </div>
                  </div>
                </div>

                <div class="col-md-3 col-lg-3">
                  <div class="row">
                    <div class="col-md-4 col-lg-4">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Ticket Type</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8 col-lg-8">
                      <i class=""></i>
                      <select name="ticket_type" id="ticket_type" class="select2 w-100">
                        <option selected value="00" disbaled >--- Select Ticket Type ---</option>
                        <?php
                                        foreach($ticket_type as $k => $v){
                                        ?>
                                            <option value="<?= $v['ticket_type_id']; ?>" <?=( $v['ticket_type_id'] == @$data['ticket_type'])? 'selected': ''; ?> >
                                            <?= $v['ticket_type_name']; ?>
                                            </option>
                                        <?php
                                        }
                                        ?></select>  
                    </div>
                  </div>
                </div>

                <!-- <div class="col-md-3 col-lg-3">
                  <div class="row">
                    <div class="col-md-3 col-lg-3">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Location</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                      <i class=""></i>
                      <select name="location" id="location" class="custom_select w-100">
                        <option value="0" selected >--- Select Location ---</option>
                        <?php
                        foreach($cityData as $k => $v){
                        ?>
                        <option value="<?= $v['city_id']; ?>" >
                        <?= $v['city_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div> -->

                <div class="col-md-3 col-lg-3 responsve-mt-10">
                  <div class="row">
                    <div class="col-md-4 col-lg-4">
                      <div class="form-group">
                         <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch"> 
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="clearfix"></div>
                </form>
              </div>
              
              <table class="table dataTables_wrapper amc_quot-dtable-disp" id="custom_table"  >
                <thead>
                  <tr>
                    <th width='150px'>Ticket No</th>
                    <th width='150px'>Ticket Type</th>
                    <th width='120px'>Ticket Date</th>
                    <th width='170px'>Invoice No<br>Expire Date</th>
                    <th width='200px'>Customer /<br>End User</th>
                    <th width='120px'>Status</th>
                    <th >Action</th>
                  </tr>
                </thead>
                <tbody id="custom_row">
                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach(@$data as $k => $v) {
                                ?>
                                <tr>
                                    <td><?= $setting['company_prefix'].$setting['ticket_prfx'].$v['ticket_no']?></td>
                                    <td>
                                      <?=
                                        $v['ticket_type_name']
                                      ?>
                                    </td>
                                    <td><?= $v['ticket_date']?></td>
                                    <td> <?= $v['invoice_no']?><br><?= $v['invoice_expiry_date']?></td>
                                    <td><?= $v['user_name']?> /<br><?= ($v['end_user_name'] != null )?$v['end_user_name']:''?></td>
                                    <td>
                                      <?php 
                                        
                                        
                                          if($v['status'] == 1)
                                          {
                                            if($v['TicketStatus'] == 0)
                                            {
                                              echo 'Pending';
                                            }else if($v['TicketStatus'] == 1)
                                            {
                                              echo 'Partially';
                                            }else if($v['TicketStatus'] == 2)
                                            {
                                              echo 'Completed';
                                            }else if($v['TicketStatus'] == 3)
                                            {
                                              echo 'Pending By Client';
                                            }else 
                                            {
                                              echo 'Not Initiated';
                                            }
                                          }else if($v['status'] == 2)
                                          {
                                            echo 'Accepted';
                                          }else if($v['status'] == 3)
                                          {
                                            echo 'Rejected';
                                          }
                                        
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?php echo site_url('Ticket_management/detail/'.@$v['ticket_id'])?>" data-id="<?= @$v['ticket_id']?>" class="btn-primary btn btn-sm" data-toggle="tooltip" title="Detail">View</a>
                                    
                                    
                                    <?php if($v['status'] != 2){?>
                                    <a href="<?php echo site_url('Ticket_management/edit/'.@$v['ticket_id'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="" data-id="<?= @$v['ticket_id']?>" class="btn-warning btn btn-sm ticket_status_change" data-toggle="tooltip" title="Add">Status Update</a>
                                    <?php } ?>

                                    <?php if($v['TicketStatus'] == 2 && $v['status'] != 2 && $v['status'] != 3){?>
                                    <a href="" data-id="<?= @$v['ticket_id']?>" class="btn-primary btn btn-sm ticket_approval" data-toggle="tooltip" title="Add">Approvals</a>
                                    <?php } ?>
                                    <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Status History" data-id="<?= @$v['ticket_id']; ?>" data-path="Ticket_management/status_history">Status History</a>
                                    <?php
                                      if($v['status'] == 2)
                                      {
                                        ?>
                                        <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Approval History" data-id="<?= @$v['ticket_id']; ?>" data-path="Ticket_management/approval_history">Approval History</a>
                                        <?php
                                      }
                                    ?>
                                    </td>
                                      </tr>
                                <?php
                                    }
                                }
                                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <input type="hidden" value="0" id="searched">

    
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <?php include APPPATH.'views/include/ticket_change_status_modal.php'; ?>
    <?php include APPPATH.'views/include/ticket_approval_modal.php'; ?>
    <!-- /.modal -->
    </div>
    <script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $('.custom_select').select2({
              minimumInputLength: 4
        });
        $('#due_since_div').hide();
        $( ".datepicker" ).datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $("form.validate").validate({
            rules: {
                customer_id:{
                  required: true
                },
                to_date:{
                  required: true
                },  
                from_date:{
                  required: true
                },  
                ticket_type:{
                  required: true
                },  
                
            }, 
            messages: {
                customer_id: "This field is required.",
            },
            invalidHandler: function (event, validator) {
                error("Please input all the mandatory values marked as red");
                },
                errorPlacement: function (label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');  
                },
                highlight: function (element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control'); 
                },
                unhighlight: function (element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control'); 
                },
                success: function (label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('error-control').addClass('success-control');
                
                }
        });

        $('[name="invoice_type"]').change(function () {
          var invoice_type = $('[name="invoice_type"]:checked').val();
          if(invoice_type == 1)
          {
            $('#print_history_div').show();
            $('#due_since_div').hide();
          }
          else
          {
            $('#print_history_div').hide();
            $('#due_since_div').show();
          }
          
        });

        $(document).on("click", "#fetch", function(){

            // var invoice = $('[name="invoice_type"]').valid();
            var from_date_valid = $('#from_date').valid();
            var to_date_valid = $('#to_date').valid();
            // var ticket_type_valid = $('#ticket_type').valid();

            if(!from_date_valid || !to_date_valid )
            {
                return false;
            }
            var ticket_type = $('#ticket_type option:selected').val();
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if(from_date == '')
            {
              from_date= 0;
            }
            if(to_date == '')
            {
              to_date= 0;
            }
            var end_user = $('#end_user').val();
            // var location = $('#location').val();
            // var invoice_type = $('[name="invoice_type"]:checked').val();
            var customer_id = $('#customer_id option:selected').val();
            $('#searched').val(1);
            $.ajax({
              url: "<?php echo site_url('Ticket_management/search_view'); ?>",
              dataType: "json",
              type: "GET",
              data: {
                    ticket_type: ticket_type,
                    // invoice_type: invoice_type,
                    customer_id:  customer_id,
                    // location:     location,
                    end_user:     end_user,
                    from_date:     from_date,
                    to_date:     to_date
              },
              cache: false,
              success: function(invoiceData) {
                
                custom_table.clear().draw();
                for (var i = 0; i < invoiceData.data.length; i++) {
                  var html = '';
                    html += '<tr>';

                        html += '<td>';
                        html += invoiceData.setting.company_prefix + invoiceData.setting.ticket_prfx + invoiceData.data[i].ticket_no;
                        html += '</td>';


                        html += '<td>';
                        html += invoiceData.data[i].ticket_type_name
                        html += '</td>';


                        html += '<td>';
                        html += invoiceData.data[i].ticket_date;
                        html += '</td>';

                        
                        html += '<td>';
                        html += invoiceData.data[i].invoice_no+'<br>'+invoiceData.data[i].invoice_expiry_date;
                        html += '</td>';

                        html += '<td>';
                        html += invoiceData.data[i].user_name+' / <br>'+ invoiceData.data[i].end_user_name;
                        html += '</td>';

                        html += '<td>';
                        if(invoiceData.data[i].status == '1')
                          {
                             console.log(invoiceData.data[i].TicketStatus);
                             if(invoiceData.data[i].TicketStatus == 0)
                             {
                              html += 'Pending';
                             }else if(invoiceData.data[i].TicketStatus == 1)
                             {
                              html += 'Partially';
                             }else if(invoiceData.data[i].TicketStatus == 2)
                             {
                              html += 'Completed';
                             }else if(invoiceData.data[i].TicketStatus == 3)
                             {
                              html += 'Pending By Client';
                             }else 
                             {
                              html += 'Not Initiated';
                             }
                          }else if(invoiceData.data[i].status == '2')
                          {
                            html += 'Accepted';
                          }else if(invoiceData.data[i].status == '3')
                          {
                            html += 'Rejected';
                          }
                          
                        html += '</td>';

                        html += '<td>';
                        html += '<a href="detail/'+invoiceData.data[i].ticket_id+'" data-id="'+invoiceData.data[i].ticket_id+'" class="btn-primary btn btn-sm mr-1" data-toggle="tooltip" title="Detail">View</a>';
                        if(invoiceData.data[i].status != 2){
                          html += '<a href="edit/'+invoiceData.data[i].ticket_id+'" class="btn-warning btn btn-sm mr-1" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                          html += '<a href="" data-id="'+invoiceData.data[i].ticket_id+'" class="mr-2 btn-warning btn btn-sm mr-1 ticket_status_change" data-toggle="tooltip" title="Add">Status Update</a>';
                        }
                        if(invoiceData.data[i].TicketStatus == 2 && invoiceData.data[i].status != 3 && invoiceData.data[i].status != 2){
                          html += '<a href="" data-id="'+invoiceData.data[i].ticket_id+'" class="btn-primary btn btn-sm ticket_approval mr-1" data-toggle="tooltip" title="Add">Approvals</a>';
                        }
                        html += '<a href="#" class="btn-primary btn btn-sm myModalBtn mr-1" data-toggle="tooltip" title="View Status History" data-id="'+invoiceData.data[i].ticket_id+'" data-path="Ticket_management/status_history">Status History</a>';
                        
                        if(invoiceData.data[i].status == 2){
                          html += '<a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Approval History" data-id="'+invoiceData.data[i].ticket_id+'" data-path="Ticket_management/approval_history">Approval History</a>';
                        }
                        
                        html += '</td>';

                                        

                    html += '</tr>';
                    custom_table.row.add($(html)).draw(false);
                }
                // $("#custom_row").find("tr").remove();
                // $('#custom_row').append(html);
                
                
              }
            });
            
        });

        $(document).on("click", "#print_history_button", function(e){
          e.preventDefault();
          var from_date = $('#from_date').valid();
          var to_date = $('#to_date').valid();
          var ticket_type_valid = $('#ticket_type').valid();
          if(!from_date || !to_date || !ticket_type_valid)
          {
              return false;
          }
          
          from_date = $('#from_date').val();
          to_date = $('#to_date').val();
          var ticket_type = $('#ticket_type option:selected').val();
          window.open('print_history?from_date='+from_date+'&to_date='+to_date+'&ticket_type='+ticket_type);  
          return false;
        });

        $('#customer_id').change(function () {
          var customer_id = $("#customer_id").find(':selected').val();
          $.ajax({
              url: "<?php echo site_url('Ticket_management/get_customer_end_users'); ?>",
              dataType: "json",
              type: "GET",
              data: {
                customer_id: customer_id,
              },
              cache: false,
              success: function(invoiceData) {
                var html = '';
                html += '<option value="0" selected >--- Select End User ---</option>';
                if(invoiceData.data != undefined){
                  for (var i = 0; i < invoiceData.data.length; i++) {
                    html += '<option value="'+invoiceData.data[i].end_user_id+'" >'+invoiceData.data[i].end_user_name+'</option>';
                  }
                }
                
                $('#end_user').html(html).trigger('change');
              }
            });
        });

        // $('#end_user').change(function () {
        //   $('#location option[value="0"]').prop('selected', true);
        // });

        // $('#location').change(function () {
        //   $('#end_user option[value="0"]').prop('selected', true);
        // });
        
    });
    </script>
    
  
