<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Settings
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':''; ?>
                        <?= (isset($page_heading))?$page_heading:''; ?>
                </a>
            </li>
        </ul>
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                                                    
                                 <div class="clearfix"></div>
                                   <h3>Warranty Types</h3>
                                     <div class="form-group">
                                        <div class="row" >
                                            <div class="col-md-12">
                                              <div class="dataTables_wrapper-768">
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <tr>
                                                      <th class="text-center warranty_type-dt-th1" >Add/Remove Row</th>
                                                      <th class="text-center warranty_type-dt-th2" >Warranty Type</th>
                                                      <th class="text-center warranty_type-dt-th3" >Percentage</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="customFields">
                                                    <tr class="txtMult">
                                                      <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                                      <td colspan="6"></td>
                                                    </tr>
                                                    
                                                      <?php
                                                      if(@$data != null && !empty(@$data)){
                                                      $k =0;
                                                      foreach(@$data as $type){
                                                     ?>
                                                     <tr class="txtMult">
                                                            <td class="text-center" style="width:60px;"><a href="javascript:void(0);" data-id="<?= $type['warranty_type_id']?>"  class="remCF">Remove</a></td>
                                                            <td>
                                                                <input type="text" class="warranty_type" required 
                                                                style="width:100%" id="warranty_type"  name="warranty_type[]" 
                                                                value="<?= @$type['title'] ?>" placeholder="Warranty Type" />
                                                                <input type="hidden" name="warranty_type_id[]" value="<?= @$type['warranty_type_id'] ?>" />
                                                            </td>
                                                             <td>
                                                                <input type="text" class="percentage txtboxToFilter" required style="width:100%" id="percentage_old<?=$k?>"  name="percentage[]" 
                                                                value="<?= @$type['percentage'] ?>" placeholder="Warranty Type" />
                                                            </td>
                                                        </tr>
                                                    <?php $k++; } } ?>

                                                  </tbody>
                                                </table>
                                              </div>  
                                            </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                            

                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['setting_id']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {

        $("form.validate").validate({
            rules: {
                warranty_type: {
                    required: true
                },
                'percentage[]':{
                    required: true,
                    digits: true
                }
            },
            messages: {
                setting_company_name: "This field is required.",
                setting_company_address: "This field is required.",
                setting_company_postal: "This field is required.",
                setting_company_city: "This field is required.",
                setting_company_country: "This field is required.",
                setting_company_reg: "This field is required.",
                setting_company_bank_ac_no: "This field is required.",
                setting_company_bank_branch_name: "This field is required.",
                quotation_prfx: "This field is required.",
                invoice_prfx: "This field is required.",
                delivery_prfx: "This field is required.",
                material_prfx: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit  
                error("Please input all the mandatory values marked as red");
  
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    $(document).ready(function(){
        var max_fields      = 6; 
        var add_button      = $("#customFields .addCF");
        var x = 1; 
        var box_size = "<?php echo sizeof($data); ?>";
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
            x++;
            var temp = '<tr class="txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="warranty_type" required  style="width:100%" id="warranty_type'+x+'" data-optional="0" name="warranty_type[]" value="" placeholder="" />';
            temp    += '<input type="hidden" class="warranty_type_id" id="warranty_type'+x+'"  name="warranty_type_id[]" value="0"  />';
            temp    += '</td>';
             temp    += "<td>";
            temp    += '<input type="text" class="percentage txtboxToFilter" required  style="width:100%" id="percentage'+x+'" data-optional="0" name="percentage[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#customFields").append(temp);
        });
        $("#customFields").on('click','.remCF',function(){
            var id = $(this).attr('data-id');
            console.log(id);
            if(id){
                var r = confirm("Are you sure you want to delete it?");
                if (r == true){
                console.log("true");
                   $.ajax({
                      url: "<?php echo site_url('setting/warranty_type_delete'); ?>",
                      dataType: "json",
                      type: "POST",
                      data: {id: id} ,
                      cache: false,
                      success: function(data) {
                       
                      }
                    });
                    $(this).parent().parent().remove();
                }
            }else{
                $(this).parent().parent().remove();
            }
        });
      
    });

</script>
function myFunction() {
  var txt;
  var r = confirm("Press a button!");
  if (r == true) {
    txt = "You pressed OK!";
  } else {
    txt = "You pressed Cancel!";
  }
  document.getElementById("demo").innerHTML = txt;
}