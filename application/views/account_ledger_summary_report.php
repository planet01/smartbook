
<style>
.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}

</style>
   
   <div class="div-controls div-font-controls" style="text-align:center;padding-top:-12%;">
      <h3 class="main-heading">Smart Matrix General Trading LLC</h3>
      <br>
      <h4 class="main-second-heading">Account Ledger Summary</h4>
      <br>
      <h4 class="main-second-heading">From Date: <?= $from_date?> To Date: <?= $to_date?></h4>
      <br>
   </div>

   

   <table style="width:100%;border-collapse: collapse;border-top:2px solid #000;border-bottom:2px solid #000;margin-bottom:15%;" class="table_align"> 
      <!-- first record -->
      <tr>
         <th align="left" width="60px" style="padding:5px 0 5px 10px;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
         <th align="left" width="75px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Account #</b></th>
         <th align="left" width="60px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Account Name</b></th>
         <th align="center" width="100px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Opening Balance</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Debit</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Credit</b></th>
         <th align="left" width="80px" style="padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Closing Balance</b></th>
      </tr>

      
      <?php
         $i = 0;
               foreach ($data as $d) {
                
            $i++;
      ?>
            
            <tr>
                <td align="left" style="padding:5px 0 5px 10px;font-family: TimesNewRoman;font-size: 12px;"><?= $i; ?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $d['AccountNo']?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $d['AccountDesc']?></td>
                <td align="center" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= number_format((float)$d['previous_balance'], 2, '.', '')?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= number_format((float)$d['debit'], 2, '.', '')?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= number_format((float)$d['credit'], 2, '.', '')?></td>
                <td align="left" style="padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= number_format((float)$d['previous_balance'] + $d['balance'], 2, '.', '')?></td>
            </tr>
            
      <?php 
             
         }
           
      ?>
        
   </table>
   
  