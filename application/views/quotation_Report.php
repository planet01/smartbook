   <style type="text/css">
     .table1-td-h4 {
        color: #333333;
     }
     .table1-td-h4-2{
        color: #7f7f7f;
     }
     .table1-td-quotHeading {
        text-align: right;
        font-size: 22px;
        font-weight: bold;
     }
     .product-table2-th-details {
        border: 1px solid #adadad; 
        background-color:#3c3d3a;
        color: #fff;
     }
     .product-table2-txt-align {
        text-align: center;
     }
     .product-table1-txt-align {
        position: relative;
        right: 0;
     }
     .product-table2-td-details {
        border:1px solid #adadad;
     }
     .sub-total-heading {
        font-weight:bold;
        border-left: 1px solid #adadad;
        text-align: right;
     }
     .sub-total-detail{
        border-right: 1px solid #adadad;
        text-align: right;
        padding-right: 7px;
     }
     .less-special-discount-heading {
        font-weight:bold;
        border-left:1px solid #adadad;
        text-align: right;
     }
     .less-special-discount-detail {
        border-right:1px solid #adadad;
        text-align: right;
        padding-right: 7px;
     }
     .buyback-discount-heading{
        font-weight:bold;
        border-left: 1px solid #adadad;
        text-align:right;
     }
     .buyback-discount-detail{
        border-right: 1px solid #adadad;
        text-align:right;
        padding-right: 7px;
     }
     .netAmount-heading{
        font-weight:bold;
        text-align:right;
        border-top: 1px solid #adadad;
        border-left: 1px solid #adadad;
     }
     .netAmount-detail{
        font-weight:bold;
        text-align:right;
        border-top: 1px solid #adadad;
        border-right: 1px solid #adadad;
        padding-right: 7px;
     }
     .vat-heading{
        font-weight:bold;
        text-align:right;
        border-left: 1px solid #adadad;
        border-bottom: 1px solid #adadad;
     }
     .vat-details{
        text-align:right;
        border-right: 1px solid #adadad;
        border-bottom: 1px solid #adadad;
        padding-right: 7px;
     }
     .netAmount-inc_VAT-heading{
        font-weight:bold;
        text-align:right;
        border-left:1px solid #adadad;
        border-bottom: 1px solid #adadad;
     }
     .netAmount-inc_VAT-detail{
        font-weight:bold;
        text-align:right;
        border-right:1px solid #adadad;
        border-bottom: 1px solid #adadad;
        padding-right: 7px;
     }
     .border-bot{border-bottom: 1px solid #adadad;}
     .div-controls{
        margin-left: 6%;
        margin-right: 6%;
     }
     .table_align{
        margin-left:8%;
        margin-right: 8%;
     }
	 .table_align_item{
        margin-left:8%;
        margin-right: 8%;
     }
	 .table_align_customer
     {
      margin-left:6%;
        margin-right: 6%;
     }
    .font_xl 
    {
      font-size: medium;
    }
    .font_l 
    {
      font-size: small;
    }
     .div-font-controls{
        font-family: Calibri;
        font-size: 12px;
     }
     .font-controls{
        font-family: Calibri;
        font-size: 16px !important;
     }
     .font-control-custom{
        font-family: Calibri;
        font-size: 14px !important;
     }
     td.font-controls{
       font-size: 16px !important;
     }
     .optional-table-th{
        border: 1px solid #adadad; 
        background-color:#c4bb98;
     }
     .optional-table-td{
        border: 1px solid #adadad; 
     }

     .tr1-th-first{border-top: 1px solid black;text-align: right;border-left: 1px solid black;}
     .tr1-th-sec{text-align: right;border-left: 1px solid black;}
     .tr1-th{border-top: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;}
     .tr1-th-sec{border-right: 1px solid black;border-left: 1px solid black;}
     .tr2-th-first{text-align: right;border-left: 1px solid black; border-bottom: none;}
     .tr2-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr3-th-first{text-align: right;border-left: 1px solid black;}
     .tr3-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr4-th-first{text-align: right;border-left: 1px solid black;}
     .tr4-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr5-th-first{text-align: right;border-left: 1px solid black;}
     .tr5-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr6-th-first{text-align: right;border-left: 1px solid black;}
     .tr6-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr7-th-first{text-align: right;border-left: 1px solid black;}
     .tr7-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr8-th-first{text-align: right;border-left: 1px solid black;}
     .tr8-th{border-right: 1px solid black;border-left: 1px solid black}
     .tr9-th-first{text-align: right;border-left: 1px solid black;}
     .tr9-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr10-th-first{text-align: right;border-left: 1px solid black;}
     .tr10-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr11-th-first{text-align: right;border-left: 1px solid black;}
     .tr11-th{border-right: 1px solid black;border-left: 1px solid black;}
     .tr12-th-first{border-top:1px solid black;border-bottom:1px solid black;text-align: right;border-left: 1px solid black;background-color: #d9d9d9;}
     .tr12-th{border-bottom:1px solid black;border-top: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;background-color: #d9d9d9;}
     tr:nth-child(3) .tr1-th-first ,tr:nth-child(3) .tr1-th {
        border-top: none;
     }
     tr:nth-child(5) .tr1-th-first ,tr:nth-child(5) .tr1-th {
        border-top: none;
     }
     tr:nth-child(7) .tr1-th-first ,tr:nth-child(7) .tr1-th {
        border-top: none;
     }
     tr:nth-child(9) .tr1-th-first ,tr:nth-child(9) .tr1-th {
        border-top: none;
     }
     tr:nth-child(11) .tr1-th-first ,tr:nth-child(11) .tr1-th {
        border-top: none;
     }
     
     .covr_letter{
      margin-right:8%;
      margin-left:8%;
      font-family:calibri;
     }
     .img_margin{
      padding:10px 10px;
     }
     @page toc { sheet-size: A4; }
     span, strong, em, i, p{
        font-family: calibri;
     }

     table.table_size_font tr td, table.table_size_font tr th{
       font-size: large !important;
     }

     table.table_size_font_2 tr td, table.table_size_font_2 tr th{
       font-family: calibri !important;
       font-size: 20px !important;
     }
   </style>  
          <div class="covr_letter">        
            <?= $setting['quotation_cover']; ?>
            <?php
              $currency = currency_data($quotation['quotation_currency']);
             // print_b($quotation);
            ?>
          </div>
          

      <pagebreak/>


            <?php if($non_optional_found){?>
            <table style="border-collapse: collapse;" width="100%" class="table_align_customer ">
             <tr>
                <td align="left"style="padding-top: -10px;" >
                  <h3 style="font-family:calibri;color: #333333;" class="font_xl">
                    <?= @$customer_data['user_company_name'] ?>
                  </h3>
                  <h4 style="font-family:calibri;color: #7f7f7f;" class=" font_l">
                    <?= @$customer_data['user_address'] ?> 
                  </h4>
                  <?php
                    if(strlen( str_replace(' ', '',@$customer_data['tax_reg'])) > 0){
                  ?>
                  <h4 style="font-family:calibri;color: #7f7f7f;" class="">
                    TRN # <?= @$customer_data['tax_reg'] ?> 
                  </h4>
                <?php } ?>
                </td>

                <td align="right" style="padding-top: 0;">
                  <span style="float: right;font-weight: bold;" class="font_xl">QUOTATION</span><br>
                  <h4 class="font_l" style="font-family:calibri;text-align: right;">  # <?= @$setval["company_prefix"].@$setval["quotation_prfx"] ?><?= (@$quotation['quotation_revised_no'] > 0)?@$quotation['quotation_no'].'-R'.number_format(@$quotation['quotation_revised_no']):@$quotation['quotation_no']; ?>
                  </h4>
                  <h4 class="font_l" style="font-family:calibri;text-align: right;font-weight: bold;">
                    <?php
                    if(strlen( str_replace(' ', '',@$setval['setting_company_reg'])) > 0){
                    ?>
                      T.R.N # <?= @$setval['setting_company_reg']; ?>
                    <?php } ?>
                    <span>
                    <br> 
                    <?php 
                      if(!isset($_GET['view'])){
                    ?>
                    <?= ($quotation['quotation_po_no'] != '')? '<span style="float: right;font-weight:bold;" class="">P.O.# : </span>'.$quotation['quotation_po_no'].'' : ''?><br>
                  <?php } ?>
                    <span style="float: right;font-weight:bold;" class="font_l">Date</span> : <?= str_replace(' ','',@$quotation['quotation_date']); ?>
                    </span>
                  </h4>
                  
                </td>
              </tr>
            </table>
            


            <table style="border-collapse: collapse;" class="table_align_item table_size_font" width="100%" >

              <thead>

                <tr>

                  <th align="center" class="product-table2-th-details" height="30px" width="40px">#</th>

                  <th class="product-table2-th-details" height="30px" width="120px">Model</th>

                  <th class="product-table2-th-details product-table2-txt-align" height="30px" width="<?= ($inline_discount) ? '400px': '500px' ?>">Item & Description</th>

                  <th class="product-table2-th-details" height="30px" width="<?= ($inline_discount) ? '60px': '90px' ?>">Qty</th>

                  <th class="product-table2-th-details" height="50px" width="<?= ($inline_discount) ? '140px': '90px' ?>"><?= ($inline_discount) ? 'Unit Price': 'Unit Price' ?> <br> <?= $quotation_currency_name['title'];?></th>
                  <?php 
                   if($inline_discount){
                  ?>
                  <th class="product-table2-th-details" height="50px" width="120px">Discount</th>
                  <?php 
                   }
                  ?>
                  <th class="product-table2-th-details" height="50px" width="<?= ($inline_discount) ? '120px': '160px' ?>">Amount <br> <?= $quotation_currency_name['title'];?></th>

                </tr>

              </thead>
              <?php if (isset($quotation_detail_data) && @$quotation_detail_data != null) {?>
                 
              <tbody>
                <?php
                 $subtotal = 0;
                 quotation_num_format($subtotal);
                 if (isset($quotation_detail_data) && @$quotation_detail_data != null) {
                   $current_img = '';
                   $price_name = '';

                   if($quotation['quotation_currency'] == $base_currency_id)
                   {
                      $price_name ='quotation_detail_rate';
                   }
                   else
                   {
                      
                      $price_name ='quotation_detail_rate_'.strtolower($currency['title']);
                   }
                  $count1 = 1;
                  $total_discoount_price = 0;
                  foreach ($quotation_detail_data as $k => $v) {

                    if($v['quotation_detail_optional'] != 1)
                    {
                      $price = $v[$price_name];
                      $get_retail_price = (strtolower($currency['title']) == 'aed')? 'retail_base_price' : "retail_".strtolower($currency['title'])."_price";
                      $retail_price = ($retail_id == $customer_data['customer_type']) ? $price : @$v[$get_retail_price];
                      $image = explode(",",$v['images']);
                      $all_images = '';
                      foreach($image as $img){
                        $file_path = dir_path().$this->image_upload_dir.'/'.@$img;
                        if (!is_dir($file_path) && file_exists($file_path)) {
                          $current_img = site_url($this->image_upload_dir.'/'.$img);
                          $all_images .= "<img src='".$current_img."' width='".($v['print_width']*50)."' height='".($v['print_height']*50)."' /><br><br>";
                        }else{
                          /*$current_img = site_url("uploads/dummy.jpg");
                          $all_images .= "<img src='".$current_img."' width='".($v['print_width']*50)."' height='".($v['print_height']*50)."' /><br><br>";*/
                          $all_images .= "<br><br>";
                        }

                      }
                   ?>
                      <tr>

                        <td  class="product-table2-td-details" style="text-align:center;" height="30px"><?= $count1 ?></td>

                        <td class="product-table2-td-details img_margin" >
                          <center>
                            <!-- <img src='<?=  $current_img; ?>' width='<?= $v['print_width']*50; ?>' height='<?= $v['print_height']*50; ?>' /><br><br>
                            <img src='<?=  $current_img; ?>' width='<?= $v['print_width']*50; ?>' height='<?= $v['print_height']*50; ?>' /> -->
                            <b><?= $v['product_sku'] ?></b>
                            <br/>
                            <?= $all_images; ?>
                          </center>
                          <?php
                            /*if($v['quotation_detail_required'] == '1'){
                              echo '<b style="font-size:12px;color: #3a0000;">Not Required</b>'.'<br/><br/>';
                            }*/
                            // if($v['quotation_detail_required'] == '1'){
                            //   echo '<b style="font-size:12px;color: #3a0000;">Not Required</b>'.'<br/><br/>';
                            // }
                          ?>
                        </td>

                        <td class="product-table2-td-details" style="padding:15px 25px;">
                          <span style="font-size:large;font-weight: bold;font-family: calibri;"><?= $v['product_name'] ?></span>
                          <br/>
                          <span style=""><?=$v['quotation_detail_description'];?></span>
                        </td>

                        <td class="product-table2-td-details" style="vertical-align: top;padding-top: 2%;" ><center><?= $v['quotation_detail_quantity'];?></center></td>
                      
                        <td class="product-table2-td-details" style="vertical-align: top;padding-top: 2%;padding-right:10px;padding-left:10px;" ><center>
                          <?= quotation_num_format($price);  ?>
                          <br/><!-- <br><span><u>Retail Price</u><br>2500.00</span><br><span>To Be Provided By Client</span><br><span>Existing Will Be Used</span> -->
                          <?php 
                          $count1++;
                          if($retail_id != $customer_data['customer_type'] && @$retail_price > 0){
                          ?>
                            <br/>
                            <span style="font-weight: bold;text-decoration: underline;color: #3a0000;">Retail Price</span>
                            <br/>
                            <?=  @$retail_price; ?>
                            <br/>
                            <br/>
                          <?php
                          }
                            
                            if($v['quotation_detail_provided'] == '1'){
                              echo '<br>'.'<span style="color: #3a0000;font-weight:bold;font-family:calibri;">To Be Provided By The Client</span>'.'<br><br>';
                            }
                            if($v['quotation_detail_existing'] == '1'){
                              echo '<span style="color: #3a0000;font-weight:bold;font-family:calibri;">Existing Will Be Used</span>'.'<br/><br/>';
                            }
                            // if($v['quotation_detail_required'] == '1'){
                            //   echo '<b>Not Required</b>'.'<br/><br/>';
                            // }
                            if($v['quotation_detail_required'] == '1'){
                              echo '<span style="color: #3a0000;font-weight:bold;">Not Required</span>'.'<br/><br/>';
                            }
                          ?>
                        </center></td>
                        <?php 
                         $row_discount_amount = 0; 
                         if($inline_discount){
                        ?>
                          <td class="product-table2-td-details" style="vertical-align: top;padding-top: 2%;" >
                            <center>
                              <?php 
                                  if($v['quotation_detail_total_discount_amount'] > 0){
                                    if($v['quotation_detail_total_discount_type'] == 1){
                                      $qt = is_numeric($v['quotation_detail_quantity']) ? $v['quotation_detail_quantity'] : 1;
                                      $row_discounted_amount =  (($price*$qt) / 100) * $v['quotation_detail_total_discount_amount'];
                                      $row_discount_amount = quotation_num_format($row_discounted_amount);
                                      echo  quotation_num_format($row_discount_amount);
                                      echo "<br><br>". quotation_num_format($v['quotation_detail_total_discount_amount']).'%';
                                    }else if($v['quotation_detail_total_discount_type'] == 2){
                                      $row_discount_amount = $v['quotation_detail_total_discount_amount'];
                                      echo  quotation_num_format($row_discount_amount);
                                    }
                                    $total_discoount_price += $row_discount_amount;
                                  }
                              ?>
                              <br/>
                              <br/>
                              <?= $v['quotation_detail_note']; ?>
                            </center>
                          </td>

                        <?php
                         }
                        ?>

                        <td class="product-table2-td-details" style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 2%;">
             
                              <?php 
                                /*if($v['quotation_detail_existing'] == '1')
                                {
                                  echo 'Existing can be used';
                                }
                                else
                                {*/
                                  if(is_numeric($v['quotation_detail_quantity'])){

                                    $prc =  $v['quotation_detail_quantity'] * quotation_num_format($price) - $row_discount_amount;
                                    echo quotation_num_format($prc);
                                  }else{
                                     $prc = quotation_num_format($price) - $row_discount_amount;
                                     echo quotation_num_format($prc);
                                  }
                                //}
                              ?>
                          
                        </td>

                      </tr>
                   <?php

                        if(is_numeric($v['quotation_detail_quantity'])){
                          $subtotal += $v['quotation_detail_quantity'] * quotation_num_format($price) - $row_discount_amount;
                        }else{
                          $subtotal += quotation_num_format($price) - $row_discount_amount;
                        }

                      }
                    }
                  }
                 ?>
                 
            <?php
              } ?>
            <?php
      if($quotation['quotation_report_line_break'] > 0)
      {
        for($i=0;$i<$quotation['quotation_report_line_break'];$i++)
        {
          ?>
          <!-- line gap after items -->
          <tr style="border: none;">
             <td colspan="<?= ($inline_discount) ? 6 : 5 ?>" class="sub-total-heading font-control-custom">
             </td>
              <td class="sub-total-detail font-control-custom" style='color:white;'> <?= quotation_num_format($quotation['subtotal']); ?></td>
        </tr>
          <?php
        }
      }
     ?>
       <tr style=" border: 1px solid #adadad; border-bottom: none;">
         <td colspan="<?= ($inline_discount) ? 6 : 5 ?>" class="sub-total-heading font-control-custom">
          <?= ($inline_discount) ? 'Sub Total (Including '.$quotation_currency_name['title'].' '.number_format((float) $total_discoount_price, 2, '.', '').'/- Discount)' : 'Sub Total' ?></td>
          <td class="sub-total-detail font-control-custom"> <?= quotation_num_format($quotation['subtotal']); ?></td>
       </tr>
       <?php
        $discount_amount = 0;
        if ($quotation['quotation_total_discount_type'] == 1) {
          $discount_amount = sprintf("%.4f",($subtotal / 100) * $quotation['quotation_total_discount_amount']);
          $discount_amount = substr_replace($discount_amount, "", -2);
        } else if ($quotation['quotation_total_discount_type'] == 2) {
          $discount_amount = $quotation['quotation_total_discount_amount'];
        }
        $buy_bck = ($quotation['quotation_buypack_discount'] != '') ? $quotation['quotation_buypack_discount'] : 0;
        
        $find_net = sprintf("%.4f",$subtotal - ($discount_amount + $buy_bck));
		    $find_net = substr_replace($find_net, "", -2);

        $find_vat = sprintf("%.4f",($find_net / 100) * @$setval['tax']);
		    $find_vat = substr_replace($find_vat, "", -2);
		
        $toal_net = $find_net + $find_vat;
        if (!$inline_discount && $discount_amount > 0) {
        ?>
         <tr>
           <td colspan="<?= ($inline_discount) ? 6 : 5 ?>" class="less-special-discount-heading font-control-custom">
             <?php
              if (strlen(str_replace(' ', '', $quotation['quotation_discount_notes'])) > 0) {
                echo $quotation['quotation_discount_notes'];
              } else {
                echo "Less Special Discount";
              }
              ?>
           </td>

           <td class="less-special-discount-detail font-control-custom"> <?= quotation_num_format($discount_amount); ?></td>

         </tr>
       <?php }
        if ($quotation['quotation_buypack_discount'] != 0  && $quotation['quotation_buypack_discount'] != '') {
        ?>
         <tr>

           <td colspan="<?= ($inline_discount) ? 6 : 5 ?>" class="buyback-discount-heading font-control-custom">Buyback Discount</td>

           <td class="buyback-discount-detail font-control-custom"><?= quotation_num_format(@$quotation['quotation_buypack_discount']); ?></td>

         </tr>
       <?php
        }
        if ($setval['setting_company_country'] == $customer_data['user_country']) {
        ?>
         <tr>

           <td colspan="<?= ($inline_discount) ? 6 : 5 ?>" style="" class="netAmount-heading font-control-custom">Net Amount</td>

           <td class="netAmount-detail font-control-custom"><?= quotation_num_format($find_net); ?></td>

         </tr>
         <tr>

           <td colspan="<?= ($inline_discount) ? 6 : 5 ?>" class="vat-heading font-control-custom"><?= @$setval['tax'] ?>% VAT</td>

           <td class="vat-details font-control-custom"><?=  $quotation['quotation_tax_amount'];  ?></td>

         </tr>

         <tr>

           <!-- <td width="80px" height="30px"></td> -->
           <td colspan="<?= ($inline_discount) ? 3 : 3 ?>" style="text-align:left; border-right:none; border-bottom:1px solid #adadad;" class="netAmount-heading font-control-custom"><?= numberTowords2(number_format((float) $toal_net, 2, '.', ''),$quotation_currency_name['base_name'],$quotation_currency_name['Deci_name']) ?></td>
           <td colspan="<?= ($inline_discount) ? 3 : 2 ?>" style=" border-left:none" class="netAmount-inc_VAT-heading font-control-custom">Net Amount (Incl. VAT)</td>

           <!-- <td colspan="<?= ($inline_discount) ? 7 : 6 ?>" class="netAmount-inc_VAT-detail" ><?= number_format((float)ceil($toal_net), 2, '.', ''); ?></td> -->
           <td class="netAmount-inc_VAT-detail font-control-custom"><?= quotation_num_format($quotation['net_amount']); ?></td>

         </tr>
       <?php
        } else {
        ?>
         <tr>

           <!-- <td width="80px" height="30px"></td> -->
           <td colspan="<?= ($inline_discount) ? 3 : 3 ?>" style="text-align:left;border-right:none" class="netAmount-heading  border-bot font-control-custom"><?= numberTowords2(number_format((float) $find_net, 2, '.', ''),$quotation_currency_name['base_name'],$quotation_currency_name['Deci_name']) ?></td>
           <td colspan="<?= ($inline_discount) ? 3 : 2 ?>" style="border-left:none" class="netAmount-heading border-bot font-control-custom">Net Amount</td>

           <td class="netAmount-detail border-bot font-control-custom"><?= quotation_num_format($quotation['net_amount']); ?></td>

         </tr>
       <?php } ?>
       </tbody>
     </table>
     <br>
            <!-- <pagebreak/> -->
            <?php } ?>
            <!-- <br> -->
            <div style="page-break-after: auto;page-break-before: auto;">
              <?php
              if(isset($quotation['quotation_detail']) && $quotation['quotation_detail'] != ''){
              ?>
              <div class="div-controls div-font-controls" >
                <label style=""><b>QUOTATION NOTES :</b></label><br>
                <label><?= @$quotation['quotation_detail']; ?></label>
              </div>

              <br>
              <?php 
              }
              ?>

              <div class="div-controls div-font-controls">
                <label><b>Payment :</b></label><br>
                <?php
                foreach($terms as $term){
                ?>
                  <label><?= @$term['percentage']?>% <?= @$term['payment_title']?> 
                  <b>
                       (<?=  (isset($term['payment_days']) && $term['payment_days'] > 0) ? 'After '.$term['payment_days'].' Day' : ((isset($term['payment_days']) && $term['payment_days'] == 0) ? 'Immediate' : '') ?>)
                  </b>
                  </label>
                  <br/>
                <?php
                }
                ?>
              </div>

              <br>

              <?php
      if($quotation['quotation_report_line_break_payment'] > 0)
      {
        for($i=0;$i<$quotation['quotation_report_line_break_payment'];$i++)
        {
          ?>
            <br>
          <?php
        }
      }
     ?>

              <div class="div-controls div-font-controls">
                <label><b>As per following plan:<i>(After Order Confirmation & Receipt of Payment)</i></label><br>
              </div>
            </div>

            <br>

            <table class=' table_align' style="border-collapse: collapse;border:1px solid black;border-left: none !important;border-top: none !important;border-bottom: none !important;">

              <tbody>

                <?php 
                $j = 0;
                foreach($project_terms as $term){
                ?>
                <tr style="border-bottom: none; border-top: none;">

                  <!-- <th width="80px" height="30px"></th> -->

                  <th width="300px" height="15px" class="tr1-th-first" ></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                  <th width="40px" height="15px" class="tr1-th"></th>

                </tr>
                
                <tr style="border-bottom: none; border-top: none;">

                  <!-- <th width="80px" height="30px"></th> -->

                  <th  width="350px" height="30px" class="tr2-th-first"><?= $term['project_title'] ?> &nbsp;</th>
                  <?php
                  for($i=0; $i< 12; $i++){
                    if($i >= $term['start_week'] && $i<= $term['project_days']){
                  ?>
                  <th width="40px" height="30px" class="tr2-th" style="background-color: <?= $term['hash_code']?>" ></th>
                  <?php 
                    }else{
                  ?>
                  <th width="40px" height="30px" class="tr2-th"></th>
                  <?php
                    }
                  }
                  ?>
                </tr>
              <?php $j++; } ?>
                <!-- 
               empty row for spacing -->
               <tr>
                 <!-- <th width="80px" height="30px"></th> -->
               
                 <th width="300px" height="15px" class="tr11-th-first" style="" ></th>
               
                 <th width="40px" height="15px" class="tr11-th" style="" ></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               
                 <th width="40px" height="15px" class="tr11-th"></th>
               </tr>
               
               <tr>
               
                 <!-- <th width="80px" height="30px"></th> -->
                 
                 <th width="300px" height="30px" class="tr12-th-first" ><i>Duration<span style="color: #20f;">(Weeks)</span></i></th>
               
                 <th width="40px" height="30px" class="tr12-th">1</th>
               
                 <th width="40px" height="30px" class="tr12-th">2</th>
               
                 <th width="40px" height="30px" class="tr12-th">3</th>
               
                 <th width="40px" height="30px" class="tr12-th">4</th>
               
                 <th width="40px" height="30px" class="tr12-th">5</th>
               
                 <th width="40px" height="30px" class="tr12-th">6</th>
               
                 <th width="40px" height="30px" class="tr12-th">7</th>
               
                 <th width="40px" height="30px" class="tr12-th">8</th>
               
                 <th width="40px" height="30px" class="tr12-th">9</th>
               
                 <th width="40px" height="30px" class="tr12-th">10</th>
               
                 <th width="40px" height="30px" class="tr12-th">11</th>
               
                 <th width="40px" height="30px" class="tr12-th">12</th>
               
               </tr>

              </tbody>
            </table>


            <br>

            <div style="page-break-after: auto;page-break-before: auto;">
              <div class="div-controls div-font-controls">
               <?= @$quotation['quotation_terms_conditions']; ?>
              </div>
             <!--  <div class="div-controls">
               <label><b>Validity :</b></label><br>
               <label><?= @$customer_data['validity_days']; ?> days from the Date of the document</label>
             </div>
             <br>
             <div class="div-controls">
               <label><b>Inclusions & Exclusion :</b></label><br>
               </label><u>The cost of the project includes the following:</u></label>
               <ul>
                 <li>Supply, Installation & Testing</li>
                 <li>User Training </li>
                 <li>Minor Software Modifications (if any)</li>
               </ul>
               </label><u>The cost of the project <b style="color: red;">DOES NOT</b> include the following:</u></label>
               <ul>
                 <li>Windows OS (Wherever Applicable)</li>
                 <li>Civil / Carpentry / Masonry / Electrical Work (if any)</li>
                 <li>Nearest Network (Ethernet / Wi-Fi) Points - wherever required</li>
               </ul>
             </div>
             
             <div class="div-controls">
               <label><b>Support :</b></label><br>
               <label><?= @$customer_data['support_days']; ?> Year Online, Telephonic & On-Site Support will be provided </label>
             </div>
             <br>
             
             <div class="div-controls">
               <label><b>Warranty :</b></label><br>
               <label><?= @$customer_data['warranty_years']; ?> Year Comprehensive Warranty for all Manufacturing Defects</label>
               <br><br>
               <?php 
               $i = 1;
               foreach($warranty_type as $type){ ?>
               <label><u><b>Option-<?= $i ?> : <?= $type['title']?></b></u></label><br>
               <label>
                Complete warranty of the system (Hardware, Software and Accessories), including <i style="color: red;"><b>REPLACEMENT</b></i> of faulty <b><u><?= $type['percentage']?>%
               </u></b> of the Project Value per annum
               </label>
               <br><br>
               <?php 
               $i++;
               }
               ?> -->
               <!--  <label><u><b>Option-2: Service Warranty Only</b></u></label><br>
               <label>Service and Support for the system (Hardware, Software and Accessories), <i style="color: red;"><b>EXCLUDING PARTS.</b></i> Available Value per annum</label> -->
              <!-- </div> -->
            </div> 


            <!-- <table>

              <tr>

                <td width="40px"></td>

                <td colspan="3" width="705px" style="text-align: right;"><span style="color:red;border-bottom: 1px solid red;">Note: 5% VAT will be applicable on Total Amount</span></td>

            </table>  -->
            <?php
            if ($optional_found) {
            ?>
            <pagebreak/>
              
              <h2 style="text-align:center; background-color:#a6a6a6;">Optional (If Required)</h2>
              <table style="border-collapse: separate /collapse/;" class="table_align">

               <thead>

                <tr>

                  <th  class="product-table2-th-details" height="30px" width="40px">#</th>

                  <th class="product-table2-th-details" height="30px" width="120px">Model</th>

                  <th class="product-table2-th-details product-table2-txt-align" height="30px" width="400px">Item & Description</th>

                  <th class="product-table2-th-details" height="30px" width="60px">Qty</th>

                  <th class="product-table2-th-details" height="50px" width="<?= ($inline_discount) ? '140px': '200px' ?>"><?= ($inline_discount) ? 'Unit Price': 'Unit Price' ?> <br> <?= $quotation_currency_name['title'];?></th>
                  <?php 
                   if($inline_discount){
                  ?>
                  <th class="product-table2-th-details" height="50px" width="120px">Discount</th>
                  <?php 
                   }
                  ?>
                  <th class="product-table2-th-details" height="50px" width="<?= ($inline_discount) ? '120px': '160px' ?>">Amount <br> <?= $quotation_currency_name['title'];?></th>

                </tr>

              </thead>
                <tbody>
                <?php
                 $subtotal = 0;
                 quotation_num_format($subtotal);
                 if (isset($quotation_detail_data) && @$quotation_detail_data != null) {
                   $current_img = '';
                   $price_name = '';

                   if($quotation['quotation_currency'] == $base_currency_id)
                   {
                      $price_name ='quotation_detail_rate';
                   }
                   else
                   {
                      
                      $price_name ='quotation_detail_rate_'.strtolower($currency['title']);
                   }
                  $count2 =1;
                  foreach ($quotation_detail_data as $k => $v) {

                    if($v['quotation_detail_optional'] == 1)
                    {
                      $price = $v[$price_name];
                      $get_retail_price = (strtolower($currency['title']) == 'aed')? 'retail_base_price' : "retail_".strtolower($currency['title'])."_price";
                      $retail_price = ($retail_id == $customer_data['customer_type']) ? $price : @$v[$get_retail_price];
                      $image = explode(",",$v['images']);
                      $all_images = '';
                      foreach($image as $img){
                        $file_path = dir_path().$this->image_upload_dir.'/'.@$img;
                        if (!is_dir($file_path) && file_exists($file_path)) {
                          $current_img = site_url($this->image_upload_dir.'/'.$img);
                          $all_images .= "<img src='".$current_img."' width='".($v['print_width']*50)."' height='".($v['print_height']*50)."' /><br><br>";
                        }else{
                          // $current_img = site_url("uploads/dummy.jpg");
                          // $all_images .= "<img src='".$current_img."' width='".($v['print_width']*50)."' height='".($v['print_height']*50)."' /><br><br>";
                          $all_images .= "<br><br>";
                        }

                      }
                   ?>
                      <tr>

                        <td class="product-table2-td-details" style="text-align:center;" height="30px"><?= $count2; ?>.</td>

                        <td class="product-table2-td-details img_margin" >
                          <center>
                            <!-- <img src='<?=  $current_img; ?>' width='<?= $v['print_width']*50; ?>' height='<?= $v['print_height']*50; ?>' /><br><br>
                            <img src='<?=  $current_img; ?>' width='<?= $v['print_width']*50; ?>' height='<?= $v['print_height']*50; ?>' /> -->
                            <b><?= $v['product_sku'] ?></b>
                            <br/>
                            <?= $all_images; ?>
                          </center>
                          <?php
                            /*if($v['quotation_detail_required'] == '1'){
                              echo '<b style="font-size:12px;color: #3a0000;">Not Required</b>'.'<br/><br/>';
                            }*/
                          ?>
                        </td>

                        <td class="product-table2-td-details" style="padding: 15px 25px;">
                          <span style="font-size: 16px;font-weight: bold;font-family: calibri;"><?= $v['product_name']?></span>
                          <br/>
                          <span style=""><?= $v['quotation_detail_description'];?></span>
                        </td>

                        <td  class="product-table2-td-details" style="vertical-align: top;padding-top: 2%;"><center><?= $v['quotation_detail_quantity'];?></center></td>
                      
                        <td class="product-table2-td-details" style="vertical-align: top;padding-top: 2%;padding-right:10px;padding-left:10px;" >
                          <center>
                          <?= quotation_num_format($price);  ?>
                          <br/><!-- <br><span><u>Retail Price</u><br>2500.00</span><br><span>To Be Provided By Client</span><br><span>Existing Will Be Used</span> -->
                          <?php 
                          if($retail_id != $customer_data['customer_type'] && @$retail_price > 0){
                          ?>
                            <span style="font-weight: bold;text-decoration: underline;">Retail Price</span>
                            <br/>
                            <?=  @$retail_price; ?>
                            <br/>
                            <br/>
                          <?php
                          }  
                            
                            if($v['quotation_detail_provided'] == '1'){
                              echo '<span style="color: #3a0000;font-weight:bold;">To Be Provided By The Client</span>'.'<br/><br/>';
                            }
                            if($v['quotation_detail_existing'] == '1'){
                              echo '<span style="color: #3a0000;font-weight:bold;">Existing Will Be Used</span>'.'<br/><br/>';
                            }
                            if($v['quotation_detail_required'] == '1'){
                              echo '<span style="color: #3a0000;font-weight:bold;">Not Required</span>'.'<br/><br/>';
                            }
                            
                          ?>
                          </center>
                        </td>
                        <?php 
                         $count2++;
                         $row_discount_amount = 0; 
                         if($inline_discount){
                        ?>
                          <td class="product-table2-td-details" style="vertical-align: top;padding-top: 2%;" >
                            <center>
                              <?php 
                                if($v['quotation_detail_total_discount_amount'] > 0){
                                    if($v['quotation_detail_total_discount_type'] == 1){
                                      $qt = is_numeric($v['quotation_detail_quantity']) ? $v['quotation_detail_quantity'] : 1;
                                      $row_discounted_amount =  (($price*$qt) / 100) * $v['quotation_detail_total_discount_amount'];
                                      $row_discount_amount = quotation_num_format($row_discounted_amount);
                                      echo quotation_num_format($row_discount_amount);
                                      echo "<br/>". quotation_num_format($v['quotation_detail_total_discount_amount']).'%';
                                    }else if($v['quotation_detail_total_discount_type'] == 2){
                                      $row_discount_amount = $v['quotation_detail_total_discount_amount'];
                                      echo quotation_num_format($row_discount_amount);
                                    }
                                }
                              ?>
                              <br/>
                              <br/>
                              <?= $v['quotation_detail_note']; ?>
                            </center>
                          </td>

                        <?php
                         }
                        ?>

                        <td class="product-table2-td-details" style="text-align: right;padding-right: 7px;vertical-align: top;padding-top: 2%;" >
                          
                              <?php 

                                /*if($v['quotation_detail_existing'] == '1')
                                {
                                  echo 'Existing can be used';
                                }
                                else
                                {*/
                                  if(is_numeric($v['quotation_detail_quantity'])){
                                    $prc = $v['quotation_detail_quantity'] * quotation_num_format($price) - $row_discount_amount;
                                    echo quotation_num_format($prc);
                                  }else{
                                    $prc =  quotation_num_format($price) - $row_discount_amount;
                                    echo quotation_num_format($prc);
                                  }
                                //}
                              ?>
                           
                        </td>

                      </tr>
                   <?php
                        if(is_numeric($v['quotation_detail_quantity'])){
                          $subtotal += $v['quotation_detail_quantity'] * quotation_num_format($price) - $row_discount_amount;
                        }else{
                          $subtotal += quotation_num_format($price) - $row_discount_amount;
                        }

                      }
                    }
                  }

                 ?>
                 <!-- <tr>
                   <td colspan="<?= ($inline_discount)? 6 : 5?>" class="sub-total-heading">Sub Total</td>

                   <td colspan="<?= ($inline_discount)? 7 : 6?>" class="sub-total-detail"> <?= number_format((float)$subtotal, 2, '.', ''); ?></td>
                                   
                 </tr> -->
                 <!-- <?php
                  $discount_amount = 0;
                  if($quotation['quotation_total_discount_type'] == 1){
                    $discount_amount = ($subtotal/100)*$quotation['quotation_total_discount_amount']; 

                  }else if($quotation['quotation_total_discount_type'] == 2){
                    $discount_amount = $quotation['quotation_total_discount_amount'];
                  }
                  $buy_bck = ($quotation['quotation_buypack_discount'] != '') ? $quotation['quotation_buypack_discount'] : 0;
                  $find_net = $subtotal - ($discount_amount + $buy_bck);
                  $find_vat = ($find_net/100)*@$setval['tax'];
                  $toal_net = $subtotal + $find_vat;
                  if(!$inline_discount && $discount_amount > 0){
                 ?> -->
                 <!-- <tr>
                   <td width="80px" height="30px"></td> 
                 
                   <td colspan="<?= ($inline_discount)? 6 : 5?>" class="less-special-discount-heading">
                    <?php 
                      if(strlen( str_replace(' ', '',$quotation['quotation_discount_notes'])) > 0){
                        echo $quotation['quotation_discount_notes'];
                      }else{
                        echo "Less Special Discount";
                      }
                    ?>         
                   </td>
                 
                   <td colspan="<?= ($inline_discount)? 7 : 6?>" class="less-special-discount-detail" > <?= number_format((float)$discount_amount, 2, '.', ''); ?></td>
                                   
                 </tr> -->
                <!-- <?php }
                 if($quotation['quotation_buypack_discount'] != 0  && $quotation['quotation_buypack_discount'] != ''){
                ?> -->
                 <!-- <tr>
                                 
                                 <td width="80px" height="30px"></td>
                 
                                 <td colspan="<?= ($inline_discount)? 6 : 5?>" class="buyback-discount-heading" width="630px">Buyback Discount</td>
                 
                                 <td colspan="<?= ($inline_discount)? 7 : 6?>" class="buyback-discount-detail" ><?= number_format((float)@$quotation['quotation_buypack_discount'], 2, '.', ''); ?></td>
                               
                                 </tr> -->

               <!-- <?php
                }
                 if($setval['setting_company_country'] == $customer_data['user_country']){
                ?> -->
                <!-- <tr>
                  
                  <td width="80px" height="30px"></td>

                  <td  colspan="<?= ($inline_discount)? 6 : 5?>" class="netAmount-heading" width="630px">Net Amount</td>

                  <td  colspan="<?= ($inline_discount)? 7 : 6?>" class="netAmount-detail" ><?= number_format((float)$subtotal, 2, '.', ''); ?></td>
                
                </tr>
                <tr>
                  
                  <td width="80px" height="30px"></td>

                  <td colspan="<?= ($inline_discount)? 6 : 5?>" class="vat-heading" width="630px"><?= @$setval['tax']?>% VAT</td>

                  <td colspan="<?= ($inline_discount)? 7 : 6?>" class="vat-details" ><?= number_format((float)$find_vat, 2, '.', ''); ?></td>
                
                </tr> -->

                <!-- <tr>
                  
                  <td width="80px" height="30px"></td>

                  <td colspan="<?= ($inline_discount)? 6 : 5?>" class="netAmount-inc_VAT-heading" width="630px">Net Amount (Incl. VAT)</td>

                  <td colspan="<?= ($inline_discount)? 7 : 6?>" class="netAmount-inc_VAT-detail" ><?= number_format((float)$toal_net, 2, '.', ''); ?></td>
                
                </tr>
                <?php 
                }else{
                ?>
                 <tr>
                  
                  <td width="80px" height="30px"></td>

                  <td style="border-bottom:1px solid #adadad;" colspan="<?= ($inline_discount)? 6 : 5?>" class="netAmount-heading" width="630px">Net Amount</td>

                  <td style="border-bottom:1px solid #adadad;" colspan="<?= ($inline_discount)? 7 : 6?>" class="netAmount-detail" ><?= number_format((float)$find_net, 2, '.', ''); ?></td>
                
                </tr> -->
            <?php } ?>
              </tbody>

                <!-- <tbody>
                  <?php
                   $subtotal = 0;
                     // print_b($quotation_detail_data);
                   
                    $current_img = '';
                    $price_name = '';
                
                     if($quotation['quotation_currency'] == $base_currency_id)
                     {
                        $price_name ='quotation_detail_rate';
                     }
                     else
                     {
                        
                        $price_name ='quotation_detail_rate_'.strtolower($currency['title']);
                     }
                    foreach ($quotation_detail_data as $k => $v) {
                      $price = $v[$price_name];
                      if($v['quotation_detail_optional'] == 1)
                      {
                        $file_path = dir_path().$this->image_upload_dir.'/'.@$v['product_image'];
                        if (!is_dir($file_path) && file_exists($file_path)) {
                          $current_img = site_url($this->image_upload_dir.'/'.$v['product_image']);
                        }else{
                          $current_img = site_url("uploads/dummy.jpg");
                        }
                     ?>
                        <tr>
                
                          <td width="80px" height="30px"></td>
                
                          <td  class="optional-table-td" style="text-align:center;" height="30px">1.</td>
                          
                          <td class="optional-table-td" ><center><img src='<?=  $current_img; ?>' width='100px' /></center></td>
                
                          <td class="optional-table-td" ><label><?= $v['quotation_detail_description'];?></label></td>
                
                          <td class="optional-table-td" ><center><?= $v['quotation_detail_quantity'];?></center></td>
                
                          <td class="optional-table-td" ><center><?= $v['quotation_detail_rate']; ?></center></td>
                
                          <td >
                            <center>
                              <?php 
                                if($v['quotation_detail_existing'] == '1')
                                {
                                  echo 'Existing can be used';
                                }
                                else
                                {
                                  echo $v['quotation_detail_quantity'] * $price;
                                }
                              ?>
                            </center>
                          </td>
                        </tr>
                     <?php
                          $subtotal += $v['quotation_detail_quantity'] * $price;
                        }
                      }
                    
                   ?>
                </tbody> -->

              </table>
              
              <?php
                }
                if(isset($_GET['view']) &&  ($quotation['quotation_status_comment'] != '' || $quotation['quotation_status_comment'] != null)){
              ?>
              <br>
              <!-- <div style="page-break-after: auto;page-break-before: auto;">
              <div class="div-controls div-font-controls" style="margin-left: 9%;margin-right: 4%;">
                <label style=""><b>Comments :</b></label><br>
                <label><?= $quotation['quotation_status_comment'] ?></label>
              </div> -->
              <?php
                }
              ?>
          