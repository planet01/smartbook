<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Price Plan
            </li>
            <li><a href="#" class="active">View All <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span> <span class="fa fa-sort-desc"></spam></h4>
                        <?php
                            $price_add = false;
                            $price_edit = false;
                            $price_clone = false;
                            if ($this->user_type == 2) {
                                foreach ($this->user_role as $k => $v) {
                                if($v['module_id'] == 3)
                                {
                                    if($v['add'] == 1)
                                    {
                                        $price_add = true;
                                    }
                                    if($v['edit'] == 1)
                                    {
                                        $price_edit = true;
                                    }
                                    if($v['clone'] == 1)
                                    {
                                        $price_clone = true;
                                    }
                                }
                                }
                            }else
                            {
                                $price_add = true;
                                $price_edit = true;
                                $price_clone = true;
                            }
                        ?>
                        <?php if($price_add){?>
                        <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('price_plan/add'); ?>">Add</a>
                        <?php } ?>
                    </div>
                    <div class="grid-body ">
                        <table class="table" id="example3">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Price Id</th>
                                    <th class="priceplan-resp-scr">Description</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach(@$data as $k => $v) {
                                ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td>
                                            <?php echo $v['plan_slog']; ?>
                                        </td>
                                        <td>
                                            <?php echo $v['plan_description']; ?>
                                        </td>
                                        <td class="">
                                        <?php if($price_edit) {?>
                                           <!--  <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['product_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a> -->
                                            <a href="<?php echo site_url($edit_product.'/'.@$v['price_plan_id'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <!-- <a href="<?php echo site_url($delete_product.'/'.@$v['price_plan_id'])?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a> -->
                                            <?php }?>
                                            <?php if($price_clone) {?>
                                           <a href="#" class="defaultModalPrice btn-info btn btn-sm" data-toggle="tooltip" title="Clone" data-id="<?= @$v['price_plan_id'] ?>">
                                                <i class="fa fa-copy" aria-hidden="true"></i>
                                            </a>
                                            <?php }?>
                                        </td>
                                    </tr>
                                <?php
                                    $count++; 
                                    }
                                } 
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; 
    include APPPATH.'views/include/price_modal.php';?>

        <!-- /.modal -->
</div>