 <!-- BEGIN PAGE CONTAINER-->
 <div class="page-content">
   <div class="content">
     <ul class="breadcrumb">
       <li>
         <p>Dashboard</p>
       </li>
       <li><a href="#" class="active"> Detail <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
     </ul>
     
     <div class="row">
       <div class="col-md-12">
         <div class="grid simple">
           <div class="grid-title no-border">
             <h4>Detail <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold"></span></h4>
           </div>
           <div class="grid-body no-border">
            <div class="row">

                
               <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-6">
                       <label class="form-label">Project Name</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <input readonly type="text" class="" name="project_name" value="<?= @$data['project_name'] ?>" placeholder="">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-6">
                       <label class="form-label">Date</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <input type="text" readonly disabled class="datepicker" name="project_date" value="<?= @$data['project_date'] ?>" placeholder="">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Primary Technincian</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <select name="primary_techinician" readonly disabled id="primary_techinician" class="select2 form-control">
                               <option value="0" selected disabled>--- Select Primary Technincian ---</option>
                               <?php
                                foreach ($employeeData as $k => $v) {
                                ?>
                                 <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['primary_techinician']) ? 'selected' : ''; ?>>
                                   <?= $v['user_name']; ?></option>
                               <?php
                                }
                                ?>
                             </select>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Secondary Technincian</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <select name="secondary_techinician" readonly disabled id="secondary_techinician" class="select2 form-control">
                               <option value="0" selected disabled>--- Select Primary Technincian ---</option>
                               <?php
                                foreach ($employeeData as $k => $v) {
                                ?>
                                 <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['secondary_techinician']) ? 'selected' : ''; ?>>
                                   <?= $v['user_name']; ?></option>
                               <?php
                                }
                                ?>
                             </select>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
                 
                 <div class="clearfix"></div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Invoice type</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <div class="radio radio-success responsve-radio">
                             <div style="display:inline;">
                               <input type="radio" readonly disabled class="doc_type" id="checkbox3" name="doc_type" value="1" <?= (@$data['doc_type'] == 1) ? 'checked="checked"' : '' ?> required="" aria-required="true">
                               <label for="checkbox3"><b>Proforma Invoice</b></label>
                             </div>
                             <div style="display:inline;">
                               <input type="radio" readonly disabled class="doc_type" id="checkbox4" name="doc_type" value="2" <?= (@$data['doc_type'] == 2) ? 'checked="checked"' : '' ?>>
                               <label for="checkbox4"><b>Invoice No.</b></label>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                   <div class="col-md-3">
                     <div class="row">
                       <div class="col-md-12">
                         <label class="form-label">Proforma Invoice / Invoice No</label>
                       </div>
                     </div>
                     <div class="row">
                       <div class="col-md-10">
                         <div class="form-group">
                           <div class="input-with-icon right controls">
                             <i class=""></i>
                                 <select name="doc_id" id="doc_id" readonly disabled class="custom_select " style="width:100% !important">
                                  <?php
                                    if ($page_title != 'add') {
                                      ?>
                                        <option selected value="<?= $data['doc_id']?>">
                                          <?php
                                            if($data['doc_type'] == 1) //PI
                                            {
                                              echo @$setting["company_prefix"] . @$setting["quotation_prfx"]; ?><?= ($data['quotation_revised_no'] > 0) ? @$data['quotation_no'] . '-R' . number_format(@$data['quotation_revised_no']) : @$data['quotation_no'];
                                            }else //Inovice
                                            {
                                              $rev = ($data['invoice_revised_no'] > 0)?'-R'.$data['invoice_revised_no'] : '';
                                              $rev1 = ($data['invoice_status'] == 2)?'-'.$data['invoice_revised_no'] : '';
                                              echo @$setting["company_prefix"].@$setting["invoice_prfx"].$data['invoice_no'].$rev;
                                              ?>
                                                <!-- <br> -->
                                              <?php
                                              // ($data['invoice_proforma_no'] != 0)?@$setting["company_prefix"].@$setting["quotation_prfx"].$data['invoice_proforma_no'].find_rev_no($data['invoice_proforma_no']):'';
                                            }
                                          ?>
                                        </option>
                                      <?php
                                    }
                                  ?>
                                 </select>
                               
                           </div>
                         </div>
                       </div>
                       
                     </div>
                   </div>

                 
                 <div class="clearfix"></div>


                 <div class="col-md-12">
                   <div class="form-group">
                     <div class="row form-row">
                       <div class="col-md-12">
                         <label class="form-label">Project Notes</label>
                       </div>
                       <div class="col-md-12">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <textarea readonly disabled name="project_note" id="myeditor" type="textarea" class="form-control ckeditor" placeholder=""><?=@$data['project_note']; ?></textarea>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>


                 <div class="clearfix"></div>
                <h2>Milestones: </h2>
                <?php
                  if ($page_title == 'add') {
                    foreach ($milestoneData as $k => $v) {
                ?>  
                        <div class="col-md-12">
                            <div class="dataTables_wrapper-1440">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan='5'><?= $v['milestone_name']?>
                                            <input type="hidden" name='milestone[<?= $k ?>][milestone_name]' value="<?= $v['milestone_name']?>">
                                            <input type="hidden" name='milestone[<?= $k ?>][milestone_id]' value="<?= $v['milestone_id']?>">
                                        </th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th>Task Description</th>
                                            <th>Type Of Task</th>
                                            <th>No Of Hours</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tr class="txtMult">
                                        <td class="text-center" ><a href="javascript:void(0);" data-tbody="milestoneTask<?= $v['milestone_id']?>" data-key="<?= $k ?>" class="addTask"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                        <td colspan='4'></td>
                                    </tr>
                                    <tbody id="milestoneTask<?= $v['milestone_id']?>">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                <?php
                    }
                  } else {
                    $count = 0;
                    
                    foreach ($milestoneData as $k => $v) {
                      ?>  
                              <div class="col-md-12">
                                  <div class="dataTables_wrapper-1440">
                                      <table class="table table-bordered">
                                          <thead>
                                              <tr>
                                                  <th colspan='6'><?= $v['milestone_name']?>
                                                  <input type="hidden" name='milestone[<?= $k ?>][milestone_name]' value="<?= $v['milestone_name']?>">
                                                  <input type="hidden" name='milestone[<?= $k ?>][milestone_id]' value="<?= $v['milestone_id']?>">
                                              </th>
                                              </tr>
                                              <tr>
                                                  <th></th>
                                                  <th>Task Description</th>
                                                  <th>Type Of Task</th>
                                                  <th>No Of Hours</th>
                                                  <th>Remarks</th>
                                              </tr>
                                          </thead>
                                          <tr class="txtMult">
                                              <td class="text-center" ></td>
                                              <td colspan='5'></td>
                                          </tr>
                                          <tbody id="milestoneTask<?= $v['milestone_id']?>">
                                            <?php
                                              if(isset($project_task_data) && !empty($project_task_data))
                                              {
                                                foreach ($project_task_data as $k2 => $v2) {
                                                  if(@$v2['milestone_id'] == @$v['milestone_id'])
                                                  {
                                                    $count++;
                                                    ?>
                                                    <tr class="txtMult">
                                                      <td class="text-center">
                                                      </td>
                                                      <td >
                                                        <input id='pt_description<?= $count ?>' type='text' value='<?= $v2['pt_description']?>' name='milestone[<?= $k ?>][pt_description][]' required>
                                                      </td>
                                                      
                                                      <td>
                                                        <select id='type_of_task<?= $count ?>' name='milestone[<?= $k ?>][type_of_task][]' style='width:100%' required>
                                                          <option selected disabled >--- Select Task Type ---</option>
                                                          <?php foreach($projectCateogryData as $k3 => $v3){ ?>
                                                            <option <?= ($v2['type_of_task'] == $v3['pc_id'])?'selected':''?> value="<?= $v3['pc_id']; ?>"> <?= $v3['pc_name']; ?></option>
                                                          <?php } ?>
                                                        </select>
                                                      </td>

                                                      <td>
                                                        <input id='no_of_hours<?= $count ?>' value='<?= $v2['no_of_hours']?>' type='text' class='txtboxToFilter' name='milestone[<?= $k ?>][no_of_hours][]' required>
                                                      </td>

                                                      <td>
                                                        <input id='remarks<?= $count ?>' value='<?= $v2['remarks']?>' type='text' name='milestone[<?= $k ?>][remarks][]' required>
                                                      </td>

                                                    </tr>
                                                    <?php
                                                  }
                                                }
                                              }
                                            ?>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                      <?php
                          }
                  } ?>
               </div>
               <br>
               <div class="row">
                 <div class="col-md-12">
                   <div class="form-group text-center">
                     <div class="row">
                       <div class="col-md-12">
                         <a class="btn btn-success btn-cons " href="<?= base_url('Project_summary/view')?>"> Back</a>
                         <input name="id" type="hidden" value="<?= @$data['project_id']; ?>">
                       </div>
                     </div>

                   </div>
                 </div>
               </div>

           </div>
         </div>
       </div>
     </div>
   </div>
 </div>

 <!-- END BASIC FORM ELEMENTS-->
<script>
   
$(document).ready(function() {
    $('.custom_select').select2({
            minimumInputLength: 4
    });
    var page = '<?php echo $page_title; ?>';
    var check_select = 0;
    if (page != "add") {
        check_select = 1;
    }
    var invoices = "";
    var proforma_invoices = "";
    window.setting = "<?= addslashes(json_encode($setting)); ?>";
    find_invoices();

    function find_invoices() {
        $.ajax({
        url: "<?php echo site_url('Project_summary/find_invoices'); ?>",
        dataType: "json",
        type: "GET",
        cache: false,
        success: function(data) {
            window.invoices = data.invoice;
            window.proforma_invoices = data.proforma_invoice;
        }
        });
    }
    $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true

    });
  

});
</script>