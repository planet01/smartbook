  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
      </ul>
      <?php
      $check_visible = false;
      $check_add = false;
      $check_edit = false;
      $check_print = false;
      $check_status = false;
      $check_payment = false;
      $check_history = false;
      if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
          if ($v['module_id'] == 10) {
            if ($v['visible'] == 1) {
              $check_visible = true;
            }
            if ($v['add'] == 1) {
              $check_add = true;
            }
            if ($v['edit'] == 1) {
              $check_edit = true;
            }
            if ($v['print'] == 1) {
              $check_print = true;
            }
            if ($v['status'] == 1) {
              $check_status = true;
            }
            if ($v['payment'] == 1) {
              $check_payment = true;
            }
            if ($v['history'] == 1) {
              $check_history = true;
            }
          }
        }
      } else {
        $check_add = true;
        $check_edit = true;
        $check_print = true;
        $check_status = true;
        $check_payment = true;
        $check_history = true;
        $check_visible = true;
      }
      ?>
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span>
                <span class="fa fa-sort-desc"></spam>
              </h4>
              <?php if ($check_add) { ?>
                <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('Amc_quotation/add'); ?>">Add</a>
              <?php } ?>
            </div>
            <div class="grid-body amc_quot-dt-pad">
              <div class="row">
                <form class="validate">
                  <div class="col-md-4 col-lg-4">
                    <div class="row">
                      <div class="col-md-5 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label>From Date</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="row">
                      <div class="col-md-5 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label>To Date</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7 date_input">
                        <i class=""></i>
                        <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>">
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-md-4 col-lg-4">
                    <div class="row">
                      <div class="col-md-5 col-lg-5">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label>Customer</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7 col-lg-7">
                        <i class=""></i>
                        <select name="customer_id" id="customer_id" class="select2 form-control">
                          <option value="0" selected disabled>--- Select Customer ---</option>
                          <?php
                          foreach ($customerData as $k => $v) {
                          ?>
                            <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['customer_id']) ? 'selected' : ''; ?>>
                              <?= $v['user_name']; ?>
                            </option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="row">
                      <div class="col-md-5 col-lg-5">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                            <label>AMC Contract No.</label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7 col-lg-7">
                        <i class=""></i>
                        <input type="text" class="form-control" name="quotation_no" id="quotation_no">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2">
                    <div class="row">
                      <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                          <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                </form>
              </div>
              <table class="table dataTables_wrapper amc_quot-dtable-disp" id="custom_table">
                <thead>
                  <tr>
                    <th>Sr.No.</th>
                    <th class="text-center" style="min-width:200px">AMC Contract # <br>Expire Date
                    </th>
                    <th class="text-center" style="min-width:150px">Invoice</th>
                    <th class="text-center" style="min-width:200px">Customer Name <br>End User / City
                    </th>
                    <th class="text-center" style="min-width:300px">AMC Total <br> Current Status</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach (@$data as $v) {
                  ?>
                      <tr>
                        <td><?php echo $count; ?></td>

                        <td class="text-center">
                          <?php echo @$setting["company_prefix"]; ?><?= ($v['amc_quotation_status'] == 4 ||  $v['amc_quotation_status'] == 7) ? 'AMCINV-' : 'AMC' . @$setting["quotation_prfx"]; ?><?= ($v['amc_quotation_revised_no'] > 0) ? @$v['amc_quotation_no'] . '-R' . number_format(@$v['amc_quotation_revised_no']) : @$v['amc_quotation_no']; ?>
                          <br>
                          <?php echo date("Y-m-d", strtotime($v["amc_quotation_expiry_date"])) ?>
                        </td>
                        <td class="text-center"><?php
                                                $invoices = explode(", ", $v['amc_selected_invoice_no']);
                                                foreach (@$invoices as $iv) {
                                                  echo $iv . '<br>';
                                                }

                                                ?></td>

                        <td class="text-center"><?php echo $v['user_name'] ?> <br>
                          <?= ($v['end_user_name'] != '') ? $v['end_user_name'] . ' / ' . $v['city_name'] : '' ?>
                        </td>

                        <td class="text-center">
                          <?php echo quotation_num_format($v['amc_subtotal']); ?>
                          <br>
                          <input type="hidden" class="quotation_stat" value="<?= $v['amc_quotation_status'] ?>" />
                          <?php
                          $QuotationStatusData = AmcQuotationStatusData();
                          foreach ($QuotationStatusData as $k2 => $v2) {
                            if ($k2 == $v['amc_quotation_status']) {
                              if ($k2 == 2) {
                          ?>
                                <span class="label label-success"><?= $v2 . '-' . $v['amc_quotation_revised_no']; ?></span>
                              <?php
                              } else {
                              ?>
                                <span class="label label-success"><?= $v2; ?></span>
                          <?php
                              }
                            }
                          }
                          ?>
                        </td>
                        <!-- <td>
                      <?php
                      $total = 0;
                      foreach (@$detail_data as $v2) {
                        if ($v['amc_quotation_id']  == $v2['amc_quotation_id']) {
                          if ($v['amc_quotation_currency'] == $base_currency_id) {
                            $price_name = 'amc_quotation_detail_rate';
                          } else {
                            $currency = currency_data($v['amc_quotation_currency']);
                            $price_name = 'amc_quotation_detail_rate_' . strtolower($currency['title']);
                          }
                          $total += (int)$v2[$price_name] * $v2['amc_quotation_detail_quantity'];
                        }
                      }

                      echo $total;
                      ?>

                    </td> -->

                        <td class="amc_quot_dt-align-items">
                          <?php
                          if ($v['amc_quotation_status'] != '0') {
                            if ($v['has_payment'] == 0) {
                              if ($check_edit) {
                          ?>
                                <a href="<?php echo site_url($edit_product . '/' . @$v['amc_quotation_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                              <?php
                              }
                            }
                            if ($check_status) {
                              if ($v['amc_quotation_status'] != '4' && $v['amc_quotation_status'] != '7') {
                              ?>
                                <div class="btn-group ">
                                  <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                  <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                  <ul class="dropdown-menu">
                                    <li><a class="status_comment" data-id="<?= $v['amc_quotation_id']; ?>" status-id="4" href="javascript:void(0)">Convert to Invoice</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a class="status_comment" data-id="<?= $v['amc_quotation_id']; ?>" status-id="5" href="javascript:void(0)">Client Not
                                        Interested</a></li>
                                    <li class="divider"></li>
                                    <li><a class="status_comment" data-id="<?= $v['amc_quotation_id']; ?>" status-id="6" href="javascript:void(0)">Decision Postponed</a>
                                    </li>
                                    <li><a class="status_comment" data-id="<?= $v['amc_quotation_id']; ?>" status-id="7" href="javascript:void(0)">Payment Received</a>
                                    </li>
                                  </ul>
                                </div>
                                <!--  <a href="<?php echo site_url($edit_product . '/' . @$v['amc_quotation_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a> -->
                              <?php
                              } elseif ($v['amc_quotation_status'] != '7') {
                              ?>
                                <div class="btn-group ">
                                  <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                  <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                  <ul class="dropdown-menu">
                                    <li><a class="status_comment" data-id="<?= $v['amc_quotation_id']; ?>" status-id="7" href="javascript:void(0)">Payment Received</a>
                                    </li>
                                  </ul>
                                </div>
                            <?php
                              }
                            }
                            ?>
                            <?php if ($check_print || $check_visible) { ?>

                              <div class="btn-group ">
                                <button class="btn btn-white btn-demo-space"> <i class="fa fa-print"></i></button>
                                <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                  <li><a class="page_reload" href="<?= site_url($print . '?id=' . @$v['amc_quotation_id'] . '&header_check=0') ?>" target='_blank'>Quotation With Header</a></li>
                                  <li class="divider"></li>
                                  <li><a class="page_reload" href="<?= site_url($print . '?id=' . @$v['amc_quotation_id'] . '&header_check=1') ?>" target='_blank'>Quotation Without Header</a></li>
                                  <li class="divider"></li>
                                  <?php if ($v['amc_quotation_status'] == '7' || $v['amc_quotation_status'] == '4') { ?>
                                    <li><a class="page_reload" href="<?= site_url($print . '?id=' . @$v['amc_quotation_id'] . '&header_check=0&is_invoice=1') ?>" target='_blank'>Invoice With Header</a></li>
                                    <li class="divider"></li>
                                    <li><a class="page_reload" href="<?= site_url($print . '?id=' . @$v['amc_quotation_id'] . '&header_check=1&is_invoice=1') ?>" target='_blank'>Invoice Without Header</a></li>
                                  <?php } ?>
                                </ul>
                              </div>

                              <a href="<?= site_url($print . '?id=' . @$v['amc_quotation_id'] . '&header_check=0&view=0') ?>" target='_blank' title="View Detail" data-id="<?= @$v['amc_quotation_id']; ?>" class="btn-primary btn btn-sm"><i class="fa fa-eye"></i></a>
                            <?php } ?>
                            <a href="#" class="btn-warning btn btn-sm send_mail" data-id="<?= @$v['amc_quotation_id']; ?>" data-toggle="tooltip" title="Mail"><i class="fa fa-envelope"></i></a>
                            <?php if ($check_history) { ?>
                              <a href="#" class="btn-primary btn btn-sm find_history history_modal" data-id="<?= @$v['amc_quotation_id']; ?>" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>
                              <a href="#" class="btn-primary btn btn-sm find_follow_up" data-id="<?= @$v['amc_quotation_id']; ?>" data-toggle="tooltip" title="Follow Up"><i class="fa fa-arrow-up"></i></a>
                            <?php } ?>
                            <?php
                          } else {
                            if ($check_print) {
                            ?>
                              <a href="<?= site_url($print . '?id=' . @$v['amc_quotation_id'] . '&header_check=0&view=0') ?>" target='_blank' title="View Detail" data-id="<?= @$v['amc_quotation_id']; ?>" class="btn-primary btn btn-sm"><i class="fa fa-eye"></i></a>

                          <?php
                            }
                          } ?>
                          <?php if ($check_payment) { ?>
                            <a href="javascript:void(0)" class="btn-primary btn btn-sm find_payment" data-id="<?= @$v['amc_quotation_id']; ?>" id="<?= @$v['amc_quotation_id']; ?>"><i class="fa fa-credit-card"></i></a>
                          <?php } ?>
                        </td>
                      </tr>

                  <?php
                      $count++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $(".status").change(function() {
          $("form").submit();
        });
        $(".datepicker").datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });

        $(document).on("click", "#fetch", function() {

          var from_date = $('#from_date').val();
          if (from_date == '') {
            from_date = 0;
          }

          var to_date = $('#to_date').val();
          if (to_date == '') {
            to_date = 0;
          }

          var customer_id = $('#customer_id option:selected').val();
          var quotation_no = $('#quotation_no').val();

          $.ajax({
            url: "<?php echo site_url('amc_quotation/search_view'); ?>",
            dataType: "json",
            type: "POST",
            data: {
              from_date: from_date,
              to_date: to_date,
              customer_id: customer_id,
              quotation_no: quotation_no
            },
            cache: false,
            success: function(result) {
              console.log("Abbas");
              custom_table.clear().draw();
              var count = 0;
              for (var i = 0; i < result.data.length; i++) {
                count++;
                var html = '';
                html += '<tr>';

                html += '<td >';
                html += count;
                html += '</td>';

                html += '<td class="text-center">';
                html += result.data[i].amc_no + '<br>' + result.data[i].amc_quotation_expiry_date;
                html += '</td>';

                html += '<td class="text-center">';
                html += result.data[i].amc_selected_invoice_no;
                html += '</td>';

                html += '<td class="text-center">';
                html += result.data[i].user_name;
                html += '</td>';


                html += '<td class="text-center">';
                html += result.data[i].amc_total + '<br>';
                html += '<input type="hidden" class="quotation_stat" value="' + result.data[i].amc_quotation_status + '" />';
                html += '<span class="label label-success">';
                html += result.data[i].status_name;
                html += '</span>';
                html += '</td>';

                html += '<td class="amc_quot_dt-align-items">';

                if (result.data[i].amc_quotation_status != '0') {
                  if (result.data[i].has_payment == 0) {
                    html += '<?php if ($check_edit) { ?>';
                    html += '<a href="<?php echo site_url($edit_product . "/") ?>' + result.data[i].amc_quotation_id + '" class="mr-1 btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                    html += '<?php } ?>';
                  }
                  html += '<?php if ($check_status) { ?>';
                  if (result.data[i].amc_quotation_status != '4' && result.data[i].amc_quotation_status != '7') {

                    html += '<div class="btn-group mr-1">';
                    html += '    <button class="btn btn-white btn-demo-space"> <i';
                    html += '            class="fa fa-calendar"></i> Change Status</button>';
                    html += '    <button class="btn btn-white dropdown-toggle btn-demo-space"';
                    html += '        data-toggle="dropdown"> <span class="caret"></span> </button>';
                    html += '    <ul class="dropdown-menu">';
                    html += '        <li><a class="status_comment" data-id="' + result.data[i].amc_quotation_id + '"';
                    html += '                status-id="4" href="javascript:void(0)">Convert to Invoice</a>';
                    html += '        </li>';
                    html += '        <li class="divider"></li>';
                    html += '        <li><a class="status_comment" data-id="' + result.data[i].amc_quotation_id + '';
                    html += '                status-id="5" href="javascript:void(0)">Client Not';
                    html += '                Interested</a></li>';
                    html += '        <li class="divider"></li>';
                    html += '        <li><a class="status_comment" data-id="' + result.data[i].amc_quotation_id + '"';
                    html += '                status-id="6" href="javascript:void(0)">Decision Postponed</a>';
                    html += '        </li>';
                    html += '        <li><a class="status_comment" data-id="' + result.data[i].amc_quotation_id + '"';
                    html += '                status-id="7" href="javascript:void(0)">Payment Received</a>';
                    html += '        </li>';
                    html += '    </ul>';
                    html += '</div>';

                  } else if (result.data[i].amc_quotation_status != '7') {

                    html += '<div class="btn-group mr-1">';
                    html += '    <button class="btn btn-white btn-demo-space"> <i';
                    html += '            class="fa fa-calendar"></i> Change Status</button>';
                    html += '    <button class="btn btn-white dropdown-toggle btn-demo-space"';
                    html += '        data-toggle="dropdown"> <span class="caret"></span> </button>';
                    html += '    <ul class="dropdown-menu">';
                    html += '        <li><a class="status_comment" data-id="' + result.data[i].amc_quotation_id + '"';
                    html += '                status-id="7" href="javascript:void(0)">Payment Received</a>';
                    html += '        </li>';
                    html += '    </ul>';
                    html += '</div>';
                  }
                  html += '<?php } ?>';
                html += '<?php if ($check_print || $check_visible) { ?>';

                  html += '<div class="btn-group mr-1">';
                  html += '    <button class="btn btn-white btn-demo-space"> <i';
                  html += '            class="fa fa-print"></i></button>';
                  html += '    <button class="btn btn-white dropdown-toggle btn-demo-space"';
                  html += '        data-toggle="dropdown"> <span class="caret"></span> </button>';
                  html += '    <ul class="dropdown-menu">';
                  html += '        <li><a class="page_reload"';
                  html += '              href="<?= site_url($print . "?id=") ?>' + result.data[i].amc_quotation_id + '&header_check=0"';
                  html += '                target="_blank">Quotation With Header</a></li>';
                  html += '        <li class="divider"></li>';
                  html += '        <li><a class="page_reload"';
                  html += '        href="<?= site_url($print . "?id=") ?>' + result.data[i].amc_quotation_id + '&header_check=1"';
                  html += '                target="_blank">Quotation Without Header</a></li>';
                  html += '        <li class="divider"></li>';
                  html += '        <?php if ($v['amc_quotation_status'] == '7' || $v['amc_quotation_status'] == '4') { ?>';
                  html += '        <li><a class="page_reload"';
                  html += '        href="<?= site_url($print . "?id=") ?>' + result.data[i].amc_quotation_id + '&header_check=0&is_invoice=1"';
                  html += '                target="_blank">Invoice With Header</a></li>';
                  html += '        <li class="divider"></li>';
                  html += '        <li><a class="page_reload"';
                  html += '        href="<?= site_url($print . "?id=") ?>' + result.data[i].amc_quotation_id + '&header_check=1&is_invoice=1"';
                  html += '                target="_blank">Invoice Without Header</a></li>';
                  html += '        <?php } ?>';
                  html += '    </ul>';
                  html += '</div>';

                  html += '<a href="<?= site_url($print . "?id=") ?>' + result.data[i].amc_quotation_id + '&header_check=0&view=0"';
                  html += '    target="_blank" title="View Detail"';
                  html += '    data-id="' + result.data[i].amc_quotation_id + '"';
                  html += '    class="btn-primary btn btn-sm mr-1"><i class="fa fa-eye"></i></a>';
                html += '<?php } ?>';
                html += '<a href="#" class="btn-warning btn btn-sm send_mail mr-1"';
                html += '    data-id="' + result.data[i].amc_quotation_id + '" data-toggle="tooltip"';
                html += '    title="Mail"><i class=" fa fa-envelope"></i></a>';
                html += '<?php if ($check_history) { ?>';
                html += '<a href="#" class="mr-1 btn-primary btn btn-sm find_history history_modal"';
                html += '    data-id="' + result.data[i].amc_quotation_id + '" data-toggle="tooltip"';
                html += '    title="History"><i class="fa fa-history"></i></a>';
                html += '<a href="#" class="mr-1 btn-primary btn btn-sm find_follow_up"';
                html += '    data-id="' + result.data[i].amc_quotation_id + '" data-toggle="tooltip"';
                html += '    title="Follow Up"><i class="fa fa-arrow-up"></i></a>';
                html += '<?php } ?>';

              } else {
                html += '<?php if ($check_print) { ?>';
                  html += '<a href="<?= site_url($print . "?id=") ?>' + result.data[i].amc_quotation_id + '&header_check=0&view=0"';
                  html += '    target="_blank" title="View Detail"';
                  html += '    data-id="' + result.data[i].amc_quotation_id + '"';
                  html += '    class="mr-1 btn-primary btn btn-sm"><i class="fa fa-eye"></i></a>';
                html += '<?php } ?>';
              } 
              html += '<?php if ($check_payment) { ?>';
              html += '<a href="javascript:void(0)" class="mr-1 btn-primary btn btn-sm find_payment" ';
              html += 'data-id="' + result.data[i].amc_quotation_id + '" id="' + result.data[i].amc_quotation_id + '"> <i class="fa fa-credit-card"></i></a >';
              html += '<?php } ?>';

              html += '</td>';
              html += '</tr>';
              custom_table.row.add($(html)).draw(false);
              }

            }
          });

        });
        //update status in grid
        $(".page_reload").click(function(event) {
          var quotation_final = $(this).closest('tr')
            .children('td.status_desc').find('.quotation_stat').val();
          if (parseInt(quotation_final) < 4) {
            $(this).closest('tr')
              .children('td.status_desc').find('span')
              .text('Final');
          }
        });
        //find history
        $(".find_history").click(function(event) {
          var values = "id=" + $(this).data('id');
          $.ajax({
            url: "<?= site_url($history) ?>",
            type: "get",
            data: values,
            success: function(data) {
              if (data) {
                data = $.parseJSON(data);
                var html = "";
                for (var i = 0; i < data.length; i++) {
                  var count = i;
                  data[i]['s_no'] = count + 1;
                  html += history_template(data[i]);
                  //console.log(data[i]['quotation_no']);
                }
                $('#history_body').html(html);
                $('#history_modal').modal('show');
                /* setTimeout(
                   function() 
                   {
                     location. reload(true);
                   }, 1000); */
              };
            },
            error: function() {
              alert('There is error while submit');
            }
          });
        });

        $(".find_follow_up").click(function(event) {
          var values = "id=" + $(this).data('id');
          $.ajax({
            url: "<?= site_url('amc_quotation/follow_up') ?>",
            type: "get",
            data: values,
            success: function(data) {
              if (data) {
                data = $.parseJSON(data);
                var html = "";
                for (var i = 0; i < data.length; i++) {
                  var count = i;
                  data[i]['s_no'] = count + 1;
                  html += follow_up_template(data[i]);
                  //console.log(data[i]['quotation_no']);
                }
                $('#follow_up_body').html(html);
                $('#follow_up_modal').modal('show');
              };
            },
            error: function() {
              alert('There is error while submit');
            }
          });
        });

        function follow_up_template(data) {

          var html = '<tr>';
          html += '<td class="history-dt-td1">' + data['s_no'] + '</td>';
          html += '<td class="history-dt-td2" style="min-width:120px">';
          html += data['follow_up_date'];
          html += '</td>';
          html += '<td class="history-dt-td3" >';
          html += data['user_name'];
          html += '</td>';
          html += '<td class="history-dt-td4">';
          html += data['follow_up_contact_person'];
          html += '</td>';
          html += '<td class="history-dt-td5" style="min-width:150px">';
          html += data['follow_up_contact_no'];
          html += '</td>';
          html += '<td class="history-dt-td6">';
          html += data['follow_up_remarks'];
          html += '</td>';
          html += '</tr>';
          return html;
        }

      });
      var print_url = "<?= site_url($print) ?>?id=";
      var quotation_status_data = '<?php echo json_encode(QuotationStatusData()); ?>';
      var quotation_prefix = '<?php echo @$setting["company_prefix"] . @$setting["quotation_prfx"]; ?>';

      function history_template(data) {
        var dt = new Date(data['amc_quotation_created_at']);
        var subtotal = (isNaN(data['amc_subtotal']) || data['amc_subtotal'] == null) ? 0 : data['amc_subtotal'];
        var qot_no = (data['amc_quotation_revised_no'] > 0) ? data['amc_quotation_no'] + '-R' + data[
          'amc_quotation_revised_no'] : data['amc_quotation_no'] + '-R0';
        var status = window.quotation_status_data;
        status = JSON.parse(status);
        var current_status = (data['amc_quotation_status'] == 2) ? status[data['amc_quotation_status']] + '-R' + data[
          'amc_quotation_revised_no'] : status[data['amc_quotation_status']];
        var html = '<tr>';
        html += '<td class="amc-history-dt-td1">' + data['s_no'] + '</td>';
        html += '<td class="amc-history-dt-td2">';
        html += '<a target="_blank" href="' + window.print_url + data['amc_quotation_id'] +
          '&header_check=0&view=0&history=1">';
        html += window.quotation_prefix + qot_no;
        html += '</a>';
        html += '</td>';
        html += '<td class="amc-history-dt-td3">';
        // html += ("0" + dt.getDate()).slice(-2) + "-" + ("0"+(dt.getMonth()+1)).slice(-2) + "-" + dt.getFullYear();
        html += data['amc_quotation_created_at'];
        html += '</td>';
        html += '<td class="amc-history-dt-td4 text">';
        html += current_status;
        html += '</td>';
        html += '<td class="amc-history-dt-td5">';
        if (data['amc_quotation_status'] == 4) {
          html += data['amc_quotation_status_comment'];
        }
        html += '</td>';
        html += '<td class="amc-history-dt-td6 text-right">' + parseFloat(subtotal).toFixed(2) + '</td>';
        html += '</tr>';
        return html;
      }

      $(".find_payment").click(function(event) {
        var id = $(this).attr("id");
        var total_amount = 0;
        var total_received = 0;
        var overdue_since = 0;
        var overdue_amount = 0;
        var is_invoice = 0;

        $.ajax({
          url: "<?= site_url('AMC_Receivables/quotation_payment') ?>",
          type: "POST",
          data: {
            'id': id
          },
          success: function(data) {
            if (data) {
              var html = "";
              data = $.parseJSON(data);
              var row = data['payment'];
              console.log(data['data']);
              for (var i = 0; i < row.length; i++) {
                var count = i;
                row[i]['s_no'] = count + 1;
                row[i]['total_amount'] = data['data'].total_amount_base;
                row[i]['total_received'] = data['data'].paid_amount_base;
                row[i]['overdue_since'] = data['data'].expiry_date_by_pyment_term;
                row[i]['overdue_amount'] = data['data'].expiry_amount_by_pyment_term;
                html += payment_template(row[i]);
              }
              $('#payment_body').html(html);
              $('#payment_modal').modal('show');

            };
          },
          error: function() {
            alert('There is error while submit');
          }
        });
      });
      var invoice_prefix1 = '<?php echo @$setting["company_prefix"] . @$setting["quotation_prfx"]; ?>';

      function payment_template(data) {
        var dt = data.payment_date;
        var payment_amount = (isNaN(data.payment_amount) || data.payment_amount == null) ? 0.00 : parseFloat(data
          .payment_amount).toFixed(2);
        var payment_adjustment = (isNaN(data.payment_adjustment) || data.payment_adjustment == null) ? 0.00 :
          parseFloat(data.payment_adjustment).toFixed(2);

        var html = '<tr>';
        html += '<td class="receivable-dt-td1">' + data.s_no + '</td>';
        html += '<td class="receivable-dt-td2">' + data.receipt_no + '</td>';
        html += '<td class="receivable-dt-td3">' + dt + '</td>';
        html += '<td class="receivable-dt-td4">';
        html += window.invoice_prefix1 + data.invoice_no;
        html += '</td>';
        html += '<td class="receivable-dt-td5">' + payment_amount + '</td>';
        html += '<td class="receivable-dt-td6">' + payment_adjustment + '</td>';
        html += '<td class="receivable-dt-td7">';
        html += '<a href="<?php echo site_url($edit_payment . '/') ?>' + data.amc_payment_id + '?total_amount=' + data
          .total_amount + '&total_received=' + data.total_received + '&overdue_since=' + data.overdue_since +
          '&overdue_amount=' + data.overdue_amount + '&is_invoice=' + data.is_invoice +
          '" class="mb-2 mr-2 disableClick btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
        html += '<a class="page_reload btn btn-white mb-2 mr-2" href="<?= site_url(@$print_payment) ?>?id=' + data
          .amc_payment_id + '&header_check=0" target="_blank"><i class="fa fa-print"></i></a>';
        html += '<a href="<?php echo site_url(@$delete_payment . '/') ?>' + data.amc_payment_id +
          '" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm mb-2"><i class="fa fa-times"></i></a>';
        html += '</td>';

        html += '</tr>';
        return html;
      }
    </script>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <?php include APPPATH . 'views/include/send_mail_modal.php'; ?>
    <?php include APPPATH . 'views/include/status_comment_modal.php'; ?>
    <?php include APPPATH . 'views/include/follow_up_modal.php'; ?>
    <?php
    $module_prefix = "AMC Quotation #";
    include APPPATH . 'views/include/history_modal.php';
    ?>
    <?php include APPPATH . 'views/include/receivable_payments_modal.php'; ?>
    <!-- /.modal -->
  </div>