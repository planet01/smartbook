<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
        <!--</div>-->
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
                        <?php
                        $location_add = false;
                        $location_print = false;
                        if ($this->user_type == 2) {
                            foreach ($this->user_role as $k => $v) {
                                if ($v['module_id'] == 26) {
                                    if ($v['add'] == 1) {
                                        $location_add = true;
                                    }
                                    if ($v['print'] == 1) {
                                        $location_print = true;
                                    }
                                }
                            }
                        } else {
                            $location_add = true;
                            $location_print = true;
                        }
                        ?>
                        <?php if ($location_add) { ?>
                            <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('Inquiry_received_history/add'); ?>">Add</a>
                        <?php } ?>
                    </div>
                    <div class="grid-body ">


                        <form id="report_form" class="validate" target="_blank" action="<?= site_url('Inquiry_received_history/print_report') ?>" method="post">
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>From Date</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 date_input">
                                            <i class=""></i>
                                            <input class="datepicker form-control  " type="text" name="date_from" id="date_from" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>To Date</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 date_input">
                                            <i class=""></i>
                                            <input class="datepicker form-control " type="text" id="date_to" name="date_to" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <div class="row">
                                        <div class="col-md-9 col-lg-10">
                                            <div class="form-group">
                                                <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($location_print) { ?>
                                    <div class="col-md-2 col-lg-2 responsve-mt-10">
                                        <div class="row">
                                            <div class="col-md-9 col-lg-10">
                                                <div class="form-group">
                                                    <input class="btn btn-success btn-cons  soa_btn" type="submit" value="Print">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                
                                <div class="clearfix"></div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>Company Name</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <i class=""></i>
                                            <input class="form-control " type="text" id="company_name" name="company_name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>


                        <table class="table" id="custom_table">
                            <thead>
                                <tr>
                                    <th width="50px">Sr.No.</th>
                                    <th width="200px">Inquiry No</th>
                                    <th width="200px">Date</th>
                                    <th width="200px">Company</th>
                                    <th width="200px">Sales Person</th>
                                    <th width="200px">Contact Person</th>
                                    <th width="200px">Contact</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach (@$data as $k => $v) {
                                ?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <td><?= 'SM-INQ-'.$v['inquiry_no']?></td>
                                            <td>
                                                <?= date("Y-m-d", strtotime($v["inquiry_received_history_date"])) ?>
                                            </td>
                                            <td><?= $v['inquiry_received_history_company_name'] ?></td>
                                            <td><?= $v['user_name'] ?></td>
                                            <td><?= $v['inquiry_received_history_contact_person'] ?></td>
                                            <td><?= $v['inquiry_received_history_contact_no'] ?></td>
                                            <td>
                                                <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['inquiry_received_history_id']; ?>" data-path="Inquiry_received_history/detail"><i class="fa fa-eye"></i></a>
                                            </td>

                                        </tr>
                                <?php
                                        $count++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
    <script>
        $(document).ready(function() {
            var custom_table = $('#custom_table').DataTable();
            $(".datepicker").datepicker({
                format: "yyyy-mm-dd",
                autoclose: true

            });

            $(document).on("click", "#fetch", function() {

                var from_date = $('#date_from').val();
                if (from_date == '') {
                    from_date = 0;
                }

                var to_date = $('#date_to').val();
                if (to_date == '') {
                    to_date = 0;
                }

                var company_name = $('#company_name').val();

                $.ajax({
                    url: "<?php echo site_url('Inquiry_received_history/search_view'); ?>",
                    dataType: "json",
                    type: "POST",
                    data: {
                        from_date: from_date,
                        to_date: to_date,
                        company_name: company_name
                    },
                    cache: false,
                    success: function(result) {
                        custom_table.clear().draw();
                        var count = 0;
                        for (var i = 0; i < result.data.length; i++) {
                            count++;
                            var html = '';
                            html += '<tr>';

                            html += '<td >';
                            html += count;
                            html += '</td>';

                            html += '<td >';
                            html += result.data[i].inquiry_received_history_date;
                            html += '</td>';

                            html += '<td>';
                            html += result.data[i].inquiry_received_history_company_name;
                            html += '</td>';

                            html += '<td >';
                            html += result.data[i].user_name;
                            html += '</td>';

                            html += '<td >';
                            html += result.data[i].inquiry_received_history_contact_person;
                            html += '</td>';

                            html += '<td >';
                            html += result.data[i].inquiry_received_history_contact_no;
                            html += '</td>';

                            html += '<td>';
                            html += '<a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="'+result.data[i].inquiry_received_history_id+'" data-path="Inquiry_received_history/detail"><i class="fa fa-eye"></i></a>';
                            html += '</td>';
                            html += '</tr>';
                            custom_table.row.add($(html)).draw(false);
                        }

                    }
                });

            });
        });
    </script>
</div>