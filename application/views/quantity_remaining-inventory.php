  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>View All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h4>
              <!--<div class="pull-right">-->
              <!--  <a href="<?php echo site_url($add_product)?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>-->
              <!--  <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>-->
              <!--  <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>-->
              <!--  <ul class="dropdown-menu">-->
              <!--    <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>-->
              <!--    <li class="divider"></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>-->
              <!--    <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>-->
              <!--  </ul>-->
              <!--  <span class="h-seperate"></span>-->
              <!--  <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>-->
              <!--</div>-->
            </div>
            <div class="grid-body ">
              <table class="table" id="example3" >
                <thead>
                  <tr>
                    <th>Sr.No.</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                  </tr>
                </thead>
                <tbody>

                 <?php
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach(@$data as $v) {
                 ?>
                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $v['product_name']?></td>
                        <td><?php echo $v['total_inventory_quantity']?></td>
                    </tr>
          				<?php 
                    $count++;
                    }
          				} 
          				?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
    <!-- /.modal -->
  </div>
