
<style>
.div-controls{
   margin-left: 3%;
   margin-right: 3%;
}
.font-controls{
   font-size:14px;
   font-family: TimesNewRoman;
   font-weight:none;
}
.main-heading{
   font-weight:none;
   font-family: TimesNewRoman;
   font-size: 20px;
}
.main-second-heading{
   font-size:16px;
   letter-spacing:3px;
   font-family: TimesNewRoman;
   font-weight:none;
   margin-top:-15px;
}
.table_align{
   margin-left: 2%;
   margin-right: 2%;
}
.bottom-table-div{
   margin-left: 3%;
   margin-right: 3%;
}


</style>
   
   <div class="div-controls div-font-controls" style="text-align:center;padding-top:-12%;">
      <h3 class="main-heading">Smart Matrix General Trading LLC</h3>
      <br>
      <h4 class="main-second-heading"><b>Follow Ups History</b></h4>
      <h4 class="main-second-heading"><b>From:</b> <?= $from_date?> <b>To:</b> <?= $to_date?></h4>
      
   </div>

   

   <table style="width:100%;border-collapse: collapse;border-top:2px solid #000;margin-bottom:15%;" class="table_align"> 
      <!-- first record -->
      <thead>
        <tr>
            <th align="left" style="width:40px;padding:5px 0 5px 10px;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
            <th align="center" style="width:40px;padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Date</b></th>
            <th align="CENTER" style="width:60px;padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Person</b></th>
            <th align="left" style="width:60px;padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Contact No.</b></th>
            <th align="left" style="width:60px;padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Sales Person</b></th>
            <th align="CENTER" style="width:200px;padding:5px 0;border-bottom:2px solid #000;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Remarks</b></th>
            
        </tr>
      </thead>
      <?php
         foreach ($unique_data as $ud) 
         {
            $i = 0;
            ?>
                <tr style="border-style: dotted;border-bottom:1px solid #000;">
                   <td colspan='6' style="border-top:2px solid #000;">
                    
                        <span >
                        <b>Company: </b> <?= $ud['follow_up_company_name'];?>
                        </span>
                        <span>
                        <b>Document No: </b>
                            <?php
                            $type = '';
                            if($ud['follow_up_doc_type'] == 1)
                            {
                                $type = 'Quotation';
                                $quotation = getCustomRow("Select * From quotation where quotation_id = '".$ud['follow_up_doc_id']."'");
                                echo $setting['company_prefix'].$setting['quotation_prfx'].(($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no']); 
                                
                            }else if($ud['follow_up_doc_type'] == 2)
                            {
                                $type = 'Perfoma Invoice';
                                $quotation = getCustomRow("Select * From quotation where quotation_id = '".$ud['follow_up_doc_id']."'");
                                echo $setting['company_prefix'].$setting['quotation_prfx'].(($quotation['quotation_revised_no'] > 0) ? @$quotation['quotation_no'] . '-R' . number_format(@$quotation['quotation_revised_no']) : @$quotation['quotation_no']); 
                            }else if($ud['follow_up_doc_type'] == 3)
                            {
                                $type = 'Invoice';
                                $invoice = getCustomRow("Select * From invoice where invoice_id = '".$ud['follow_up_doc_id']."'");
                                $rev = ($invoice['invoice_revised_no'] > 0)?'-R'.$invoice['invoice_revised_no'] : '';
                                echo @$setting["company_prefix"].@$setting["invoice_prfx"]; ?><?php echo $invoice['invoice_no'].$rev;
                                
                            }else if($ud['follow_up_doc_type'] == 4)
                            {
                                $type = 'AMC Invoice';
                                $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '".$ud['follow_up_doc_id']."'");
                                echo @$setting["company_prefix"]; ?><?= ($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7)? 'AMCINV-': @$setting["quotation_prfx"]. (($amc_invoice['amc_quotation_revised_no'] > 0)?@$amc_invoice['amc_quotation_no'].'-R'.number_format(@$amc_invoice['amc_quotation_revised_no']):@$amc_invoice['amc_quotation_no']);
                                
                            }else if($ud['follow_up_doc_type'] == 5)
                            {
                                $type = 'AMC Quotation';
                                $amc_invoice = getCustomRow("Select * From amc_quotation where amc_quotation_id = '".$ud['follow_up_doc_id']."'");
                                echo @$setting["company_prefix"]; ?><?= ($amc_invoice['amc_quotation_status'] == 4 ||  $amc_invoice['amc_quotation_status'] == 7)? 'AMCINV-': @$setting["quotation_prfx"]. (($amc_invoice['amc_quotation_revised_no'] > 0)?@$amc_invoice['amc_quotation_no'].'-R'.number_format(@$amc_invoice['amc_quotation_revised_no']):@$amc_invoice['amc_quotation_no']);
                            }  else if ($ud['follow_up_doc_type'] == 6) {
                                    $type = 'Inquiry Recieved History';
                                    $inquiry = getCustomRow("Select * From inquiry_received_history where inquiry_received_history_id  = '" . $ud['follow_up_doc_id'] . "'");
                                    echo 'SM-INQ-'.$inquiry['inquiry_no'];
                                }
                            ?>
                            </span>
                            <span >
                            <b>Type:</b> <?= $type?>
                            </span>
                       
                   </td>
               </tr>
               
            <?php
            foreach ($data as $dt) {
                if($ud['follow_up_doc_type'] == $dt['follow_up_doc_type'] && $ud['follow_up_doc_id'] == $dt['follow_up_doc_id']){
                    $i++;
                ?>
                <tr style="">
                    <td align="left" style="border-style: dotted;border-bottom:1px solid #000;padding:10px 0 5px 10px;font-family: TimesNewRoman;font-size: 12px;"><?= $i?></td>
                    <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= date('d-M-Y', strtotime($dt['follow_up_date']))?></td>
                    <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['follow_up_contact_person']?></td>
                    <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['follow_up_contact_no']?></td>
                    <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['user_name']?></td>
                    <td align="center" style="border-style: dotted;border-bottom:1px solid #000;padding:5px 10px 5px 0;font-family: TimesNewRoman;font-size: 12px;"><?= $dt['follow_up_remarks']?></td>
                </tr>
                
                <?php 
                }
            }
           
         }
         ?>
     
      
   </table>
   
   