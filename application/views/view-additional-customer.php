<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Customer
            </li>
             <li>
                Additional Details
            </li>
            <li><a href="#" class="active">View All<!--  <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h4>
                         <a style="float: right;" class="btn btn-success btn-cons" href="<?php echo site_url($view_product)?>">Back</a>
                    </div>
                    <div class="grid-body ">
                        <table class="table" id="example3">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Position</th>
                                    <th>Notes Type</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach(@$data as $k => $v) {
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $count; ?>
                                        </td>
                                        <td>
                                            <?php echo $v['name']?>
                                        </td>
                                        <td>
                                            <?php echo $v['number']?>
                                        </td>
                                        <td>
                                            <?php echo $v['position']?>
                                        </td>
                                        <td>
                                            <?php echo $v['notes']?>
                                        </td>
                                        <td class="">
                                            <a href="<?php echo site_url($edit_product.'/'.@$v['id'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo site_url($delete_product.'/'.@$v['id'].'?user_id='.@$v['user_id'])?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a> 
                                        </td>
                                    </tr>
                                    <?php
                                $count++; 
                                }
                            } 
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>