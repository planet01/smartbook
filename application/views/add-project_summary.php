 <!-- BEGIN PAGE CONTAINER-->
 <div class="page-content">
   <div class="content">
     <ul class="breadcrumb">
       <li>
         <p>Dashboard</p>
       </li>
       <li><a href="#" class="active"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
     </ul>
     <!--<div class="page-title"> <i class="icon-custom-left"></i>-->
     <!--  <h3><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
     <!--</div>-->
     <!-- BEGIN BASIC FORM ELEMENTS-->
     <div class="row">
       <div class="col-md-12">
         <div class="grid simple">
           <div class="grid-title no-border">
             <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
           </div>
           <div class="grid-body no-border">
             <form class="ajaxForm validate" action="<?php echo site_url($add_product) ?>" method="post" enctype="multipart/form-data">
               <div class="row">

                
               <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-6">
                       <label class="form-label">Project Name</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <input type="text" class="" name="project_name" value="<?= @$data['project_name'] ?>" placeholder="">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-6">
                       <label class="form-label">Date</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <input type="text" class="datepicker" name="project_date" value="<?= @$data['project_date'] ?>" placeholder="">
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Primary Technincian</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <select name="primary_techinician" id="primary_techinician" class="select2 form-control">
                               <option value="0" selected disabled>--- Select Primary Technincian ---</option>
                               <?php
                                foreach ($employeeData as $k => $v) {
                                ?>
                                 <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['primary_techinician']) ? 'selected' : ''; ?>>
                                   <?= $v['user_name']; ?></option>
                               <?php
                                }
                                ?>
                             </select>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Secondary Technincian</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-lg-10 col-md-12 col-sm-8 add-dlvry_note-md-scr-pad success-control">
                       <div class="form-group">
                         <div class=" right controls" id="span-pre">
                           <i class=""></i>
                           <select name="secondary_techinician" id="secondary_techinician" class="select2 form-control">
                               <option value="0" selected disabled>--- Select Primary Technincian ---</option>
                               <?php
                                foreach ($employeeData as $k => $v) {
                                ?>
                                 <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['secondary_techinician']) ? 'selected' : ''; ?>>
                                   <?= $v['user_name']; ?></option>
                               <?php
                                }
                                ?>
                             </select>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
                 
                 <div class="clearfix"></div>

                 <div class="col-md-3">
                   <div class="row">
                     <div class="col-md-12">
                       <label class="form-label">Invoice type</label>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-12">
                       <div class="form-group">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <div class="radio radio-success responsve-radio">
                             <div style="display:inline;">
                               <input type="radio" class="doc_type" id="checkbox3" name="doc_type" value="1" <?= (@$data['doc_type'] == 1) ? 'checked="checked"' : '' ?> required="" aria-required="true">
                               <label for="checkbox3"><b>Proforma Invoice</b></label>
                             </div>
                             <div style="display:inline;">
                               <input type="radio" class="doc_type" id="checkbox4" name="doc_type" value="2" <?= (@$data['doc_type'] == 2) ? 'checked="checked"' : '' ?>>
                               <label for="checkbox4"><b>Invoice No.</b></label>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

                   <div class="col-md-3">
                     <div class="row">
                       <div class="col-md-12">
                         <label class="form-label">Proforma Invoice / Invoice No</label>
                       </div>
                     </div>
                     <div class="row">
                       <div class="col-md-10">
                         <div class="form-group">
                           <div class="input-with-icon right controls">
                             <i class=""></i>
                                 <select name="doc_id" id="doc_id" class="custom_select " style="width:100% !important">
                                  <?php
                                    if ($page_title != 'add') {
                                      ?>
                                        <option selected value="<?= $data['doc_id']?>">
                                          <?php
                                            if($data['doc_type'] == 1) //PI
                                            {
                                              echo @$setting["company_prefix"] . @$setting["quotation_prfx"]; ?><?= ($data['quotation_revised_no'] > 0) ? @$data['quotation_no'] . '-R' . number_format(@$data['quotation_revised_no']) : @$data['quotation_no'];
                                            }else //Inovice
                                            {
                                              $rev = ($data['invoice_revised_no'] > 0)?'-R'.$data['invoice_revised_no'] : '';
                                              $rev1 = ($data['invoice_status'] == 2)?'-'.$data['invoice_revised_no'] : '';
                                              echo @$setting["company_prefix"].@$setting["invoice_prfx"].$data['invoice_no'].$rev;
                                              ?>
                                                <!-- <br> -->
                                              <?php
                                              // ($data['invoice_proforma_no'] != 0)?@$setting["company_prefix"].@$setting["quotation_prfx"].$data['invoice_proforma_no'].find_rev_no($data['invoice_proforma_no']):'';
                                            }
                                          ?>
                                        </option>
                                      <?php
                                    }else
                                    {
                                      ?>
                                      <option value="0" disabled>--Select P.I/Invoice--</option>
                                      <?php
                                    }
                                  ?>
                                 </select>
                               
                           </div>
                         </div>
                       </div>
                       <div class="col-md-2">
                            <div class="input-with-icon right controls">
                                <button type="button" id="fetch_data" class="btn-warning btn btn-sm">
                                    Fetch Data
                                </button>
                            </div>
                        </div>
                     </div>
                   </div>

                  
                 
                 <div class="clearfix"></div>


                 <div class="col-md-12">
                   <div class="form-group">
                     <div class="row form-row">
                       <div class="col-md-12">
                         <label class="form-label">Project Notes</label>
                       </div>
                       <div class="col-md-12">
                         <div class="input-with-icon right controls">
                           <i class=""></i>
                           <textarea name="project_note" id="myeditor" type="textarea" class="form-control ckeditor" placeholder=""><?=@$data['project_note']; ?></textarea>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>


                 <div class="clearfix"></div>
                <h2>Milestones: </h2>
                <?php
                  if ($page_title == 'add') {
                    foreach ($milestoneData as $k => $v) {
                ?>  
                        <div class="col-md-12">
                            <div class="dataTables_wrapper-1440">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan='5'><?= $v['milestone_name']?>
                                            <input type="hidden" name='milestone[<?= $k ?>][milestone_name]' value="<?= $v['milestone_name']?>">
                                            <input type="hidden" name='milestone[<?= $k ?>][milestone_id]' value="<?= $v['milestone_id']?>">
                                        </th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th>Task Description</th>
                                            <th>Type Of Task</th>
                                            <th>No Of Hours</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tr class="txtMult">
                                        <td class="text-center" ><a href="javascript:void(0);" data-tbody="milestoneTask<?= $v['milestone_id']?>" data-key="<?= $k ?>" class="addTask"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                        <td colspan='4'></td>
                                    </tr>
                                    <tbody id="milestoneTask<?= $v['milestone_id']?>">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                <?php
                    }
                  } else {
                    $count = 0;
                    
                    foreach ($milestoneData as $k => $v) {
                      ?>  
                              <div class="col-md-12">
                                  <div class="dataTables_wrapper-1440">
                                      <table class="table table-bordered">
                                          <thead>
                                              <tr>
                                                  <th colspan='6'><?= $v['milestone_name']?>
                                                  <input type="hidden" name='milestone[<?= $k ?>][milestone_name]' value="<?= $v['milestone_name']?>">
                                                  <input type="hidden" name='milestone[<?= $k ?>][milestone_id]' value="<?= $v['milestone_id']?>">
                                              </th>
                                              </tr>
                                              <tr>
                                                  <th></th>
                                                  <th>Task Description</th>
                                                  <th>Type Of Task</th>
                                                  <th>No Of Hours</th>
                                                  <th>Remarks</th>
                                              </tr>
                                          </thead>
                                          <tr class="txtMult">
                                              <td class="text-center" ><a href="javascript:void(0);" data-tbody="milestoneTask<?= $v['milestone_id']?>" data-key="<?= $k ?>" class="addTask"><span class="glyphicon glyphicon-plus-sign" style="font-size:25px;"></span></a></td>
                                              <td colspan='5'></td>
                                          </tr>
                                          <tbody id="milestoneTask<?= $v['milestone_id']?>">
                                            <?php
                                              if(isset($project_task_data) && !empty($project_task_data))
                                              {
                                                foreach ($project_task_data as $k2 => $v2) {
                                                  if(@$v2['milestone_id'] == @$v['milestone_id'])
                                                  {
                                                    $count++;
                                                    ?>
                                                    <tr class="txtMult">
                                                      <td class="text-center">
                                                      <a href="javascript:void(0);" class="remCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>
                                                      </td>
                                                      <td >
                                                        <input id='pt_description<?= $count ?>' type='text' value='<?= $v2['pt_description']?>' name='milestone[<?= $k ?>][pt_description][]' required>
                                                      </td>
                                                      
                                                      <td>
                                                        <select id='type_of_task<?= $count ?>' name='milestone[<?= $k ?>][type_of_task][]' style='width:100%' required>
                                                          <option selected disabled >--- Select Task Type ---</option>
                                                          <?php foreach($projectCateogryData as $k3 => $v3){ ?>
                                                            <option <?= ($v2['type_of_task'] == $v3['pc_id'])?'selected':''?> value="<?= $v3['pc_id']; ?>"> <?= $v3['pc_name']; ?></option>
                                                          <?php } ?>
                                                        </select>
                                                      </td>

                                                      <td>
                                                        <input id='no_of_hours<?= $count ?>' value='<?= $v2['no_of_hours']?>' type='text' class='txtboxToFilter' name='milestone[<?= $k ?>][no_of_hours][]' required>
                                                      </td>

                                                      <td>
                                                        <input id='remarks<?= $count ?>' value='<?= $v2['remarks']?>' type='text' name='milestone[<?= $k ?>][remarks][]' >
                                                      </td>

                                                    </tr>
                                                    <?php
                                                  }
                                                }
                                              }
                                            ?>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                      <?php
                          }
                  } ?>
               </div>
               <br>
               <div class="row">
                 <div class="col-md-12">
                   <div class="form-group text-center">
                     <div class="row">
                       <div class="col-md-12">
                         <button class="btn btn-success btn-cons ajaxFormSubmitAlter " type="button"><?= ($page_title == 'add') ? '' : ''; ?> Submit</button>
                         <input name="id" type="hidden" value="<?= @$data['project_id']; ?>">
                       </div>
                     </div>

                   </div>
                 </div>
               </div>

             </form>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>

 <div class="modal fade" id="defaultModalDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 id="myModalLabel" class="semi-bold mymodal-customer-title">Edit Description</h4>
            </div>
            <div class="modal-body mymodal-customer-body" id="myModalDescription">
                  <div class="grid-body no-border">
                       <textarea id="myeditor" name="temp_quotation_desc" class="form-control" placeholder=""></textarea>
                       <input type="hidden" id="textbox">
                  </div>
              <div class="modal-footer">
                <button class="btn btn-success edit_desc_button" type="button">
                  Submit
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>
       <!-- Modal -->

 <!-- END BASIC FORM ELEMENTS-->
<script>
   
$(document).ready(function() {
    $('.custom_select').select2({
            minimumInputLength: 4
    });
    var page = '<?php echo $page_title; ?>';
    var check_select = 0;
    if (page != "add") {
        check_select = 1;
    }
    var invoices = "";
    var proforma_invoices = "";
    window.setting = "<?= addslashes(json_encode($setting)); ?>";
    find_invoices();

    function find_invoices() {
        $.ajax({
        url: "<?php echo site_url('Project_summary/find_invoices'); ?>",
        dataType: "json",
        type: "GET",
        cache: false,
        success: function(data) {
            window.invoices = data.invoice;
            window.proforma_invoices = data.proforma_invoice;
        }
        });
    }
    $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true

    });
    $("form.validate").validate({
        rules: {
            project_name: {
                required: true
            },
            project_date: {
                required: true
            },
            primary_techinician: {
                required: true
            },
            secondary_techinician: {
                required: true
            },
            

        },
        messages: {
            project_name: "This field is required.",
            project_date: "This field is required.",
            "product_id[]": "This field is required.",
        },
        invalidHandler: function(event, validator) {
            //display error alert on form submit    
        },
        errorPlacement: function(label, element) { // render error placement for each input type   
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
            $('<span class="error"></span>').insertAfter(element).append(label);
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('success-control').addClass('error-control');
        },
        highlight: function(element) { // hightlight error inputs
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
            var parent = $(element).parent();
            parent.removeClass('success-control').addClass('error-control');
        },
        unhighlight: function(element) { // revert the change done by hightlight
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
            var parent = $(element).parent();
            parent.removeClass('error-control').addClass('success-control');
        },
        success: function(label, element) {
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('error-control').addClass('success-control');

        }
    });
    $('.select2', "form.validate").change(function() {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });

    $(document).on('change', ".doc_type", function() {
        $("#customFields .txtMult").html('');
        var doc_type = $('input[name="doc_type"]:checked').val();
        data = (doc_type == 1) ? window.proforma_invoices : window.invoices;
        html = "";
        var this_setting = JSON.parse(window.setting);
        var prefix = this_setting['company_prefix'];
        prefix = (doc_type == 1) ? prefix + this_setting['quotation_prfx'] : prefix + this_setting['invoice_prfx'];
        html += "<option value='0'>--Select P.I/Invoice--</option>"
        for (var i = 0; i < data.length; i++) {
            var title = (data[i].revised_no > 0) ? data[i].invoice_no + '-R' + data[i].revised_no : data[i].invoice_no;
            if (data[i].invoice_found !== undefined && data[i].invoice_found > 0) {} else {
                html += "<option value='" + data[i].ID + "'>" + prefix + title + "</option>"
            }
        }
        $('#doc_id').html(html).trigger('change');
        $('#doc_id').val(0).trigger('change');
    });
    var field_count = 0;
    $(".addTask").live('click', function(e){
        e.preventDefault();
        field_count++;
        var body_id = $(this).data('tbody');
        var array_key = $(this).data('key');
        var html = '';
        html += '<tr class="txtMult">';
        html += '<td class="text-center">';
        html += '<a href="javascript:void(0);" class="remCF testt"><span class="glyphicon glyphicon-remove-circle" style="color:red; font-size:25px;"></span></a>';
        html += '</td>';

        html += '<td>';
        html += "<input id='pt_description"+field_count+"' type='text' name='milestone["+array_key+"][pt_description][]' required>";
        html += '</td>';
        
        html += '<td>';
        html += "<select id='type_of_task"+field_count+"' name='milestone["+array_key+"][type_of_task][]' style='width:100%' required>";
        html += '<option selected disabled ">--- Select Task Type ---</option>';
        html += '<?php foreach($projectCateogryData as $k => $v){ ?><option value="<?= $v['pc_id']; ?>"> <?= $v['pc_name']; ?></option><?php } ?>';
        html += '</select>';
        html += '</td>';

        html += '<td>';
        html += "<input id='no_of_hours"+field_count+"' type='text' class='txtboxToFilter' name='milestone["+array_key+"][no_of_hours][]' required>";
        html += '</td>';

        html += '<td>';
        html += "<input id='remarks"+field_count+"' type='text' name='milestone["+array_key+"][remarks][]' >";
        html += '</td>';

        html += '</tr>';
        $("#"+body_id).append(html);
    });

    $(document).on('click', '.remCF', function() {
        $(this).parent().parent().remove();
    });

    $('#fetch_data').on('click', function() {
        var doc_valid = $('#doc_id').valid();
        var doc_id = $('#doc_id').find(':selected').val();
         
        if (!doc_valid) {
          swal('Select Invoice No');
          return false;
        }

       
        var doc_type = $('input[name="doc_type"]:checked').val(); //1 => PI, 2 => Invoice
        var html = '';
        var count = 0;
        $.ajax({
        url: "<?php echo site_url('Project_summary/invoice_detail'); ?>",
        data:{
            doc_type : doc_type,
            doc_id : doc_id,
        },
        dataType: "json",
        type: "POST",
        cache: false,
        success: function(result) {
            html += '<table border="1" cellspacing="0"  style="border-collapse:collapse; border:solid windowtext 1.0pt; margin-left:18.0pt">';
                html += '<thead>';
                    html += '<tr>';
                    html += '<th >S.No</th>';
                    html += '<th >Item & Description</th>';
                    html += '<th >Qty</th>';
                    html += '</tr>';
                html += '</thead>';
            html += '<tbody>';
            for (var i = 0; i < result.detail.length; i++) {
                count++;
                html += '<tr>';
                html += '<td style="text-align:center">'+count+'</td>';
                html += '<td style="text-align:center">'+result.detail[i].product_name+'</td>';
                if(doc_type == 1)
                {
                    html += '<td style="text-align:center">'+result.detail[i].quotation_detail_quantity+'</td>';
                }else
                {
                    html += '<td style="text-align:center">'+result.detail[i].invoice_detail_quantity+'</td>';
                }
                html += '</tr>';
                
            }
            html += '</tbody>';
            html += '</table>';
            console.log(html);
            CKEDITOR.instances['myeditor'].setData(html);
        }
        });
        
    });

});
</script>