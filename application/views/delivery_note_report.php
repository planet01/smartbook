<style type="text/css">
  .table_align{
        margin-left: 10%;
        margin-right: 10%;
  }
  .table1-td-h4 {
        color: #333333;
  }
  .table1-td-h4-2{
        color: #7f7f7f;
  }
  .table1-td-quotHeading {
        text-align: right;
        font-size: 20px;
        font-weight: bold;
  }
  .product-table1-txt-align {
        position: relative;
        right: 0;
  }
  .product-table2-th-details {
        /*border: 1px solid #adadad; */
        background-color:#3c3d3a;
        color: #fff;
  }
  .product-table2-td-details {
        border:1px solid #adadad;
  }
  .div-controls{
    margin-left: 8.5%;
    margin-right: 8.5%;
  }
  .div-font-controls{
    font-family: Calibri;
    font-size: 12px;
  }
  @page toc { sheet-size: A4; }
  span, strong, em, i, p{
    font-family: calibri;
  }
  .td-border{
    border: 7px double #000;
  }
  .inp-field{

  }
  
</style>   

          <table style="border-collapse: collapse;margin-bottom: 10px;" width="100%" class="table_align">
              <tr>
                <td align="left" width="400px">
                  <h4 class="table1-td-h4"><span>Accounts<!-- <?= @$employee_data['department_title'] ?> --> Department</span><br><span><?= @$customer_data['user_company_name'] ?></span></h4>
                </td>

                <td align="right" width="400px">
                  <span class="table1-td-quotHeading">DELIVERY NOTE</span><br>
                  <h4 class="product-table1-txt-align" style="text-align: right;font-weight: bold;"><?= @$setval["company_prefix"].@$setval["delivery_prfx"] ?><?= @$deliver_note['delivery_note_no'] ?></h4><br>
                </td>
              </tr>

              <tr>
                <td align="left" width="400px" >
                  <h5 style="font-size: 14px;" class="table1-td-h4-2"><span><?= @$customer_data['user_address'] ?></span><!-- <br><span>Al-Qusais Ind.Area 2,Sharjah,</span><br><span>U.A.E</span> --></h5>
                </td>
                <td align="right" width="400px" style="padding-top: 30px;">
                  <?php 
                   if($deliver_note["delivery_po_no"] != ''){?>
                    <span>P.O.#:<?= @$deliver_note["delivery_po_no"]?></span><br>
                  <?php
                    }
                  ?>
                    <span>Date : <?= @$deliver_note['delivery_date'] ?></span> 
                </td>
              </tr>
          </table>

          <table style="border-collapse: collapse;" width="100%" class="table_align">
              <thead>

                  <tr>
                    <th align="center" class="product-table2-th-details" height="30px" width="40px">#</th>

                    <th align="left" class="product-table2-th-details" height="30px" width="680px" style="padding:0px 10px;">Item</th>

                    <th class="product-table2-th-details product-table2-txt-align" height="30px" width="80px">Qty</th>
                  
                  </tr>

              </thead>
              <tbody>
                <?php
                  $subtotal = 0;
                  if (isset($deliver_note_detail) && @$deliver_note_detail != null) {
                    $s_no = 0;
                    foreach ($deliver_note_detail as $k => $v) {
                      $s_no++;
                    ?>
                        <tr>
                            <td align="center" class="product-table2-td-details" height="30px"><?= $s_no;?></td>
                            
                            <td align="left" class="product-table2-td-details" height="30px" style="padding:15px 15px;">
                              <div style="<?= ($v['delivery_note_detail_description'] != '')?'font-weight:bold':''?>">
                                <?= $v['product_sku']?> - <?= $v['product_name'];?>
                              </div>
                              <?php if($v['delivery_note_detail_description'] != ''){?>
                                <br><?=  $v['delivery_note_detail_description']?>
                              <?php } ?>
                            </td>
                          
                            <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 10px;">
                              <?= $v['delivery_note_detail_quantity'];?>
                            </td>
                        </tr>
                    <?php 
                    }
                  }
                    ?>
                
              </tbody>

            </table>
            
            <br/>

            <div class="div-controls div-font-controls">
              <?php
                if($deliver_note['delivery_notes'] != ''){
              ?>
               <label><b>Notes:</b></label><br>
               <label> <?= $deliver_note['delivery_notes']; ?> </label>
             <?php } ?>
            </div>

            <br/><br/>

            <table style="border-collapse: collapse;margin-top: 10%;" width="100%" class="table_align">
                  <tr>
                    <td align="left" class="" height="80px" width="400px" >
                      <span><b>Thanks</b></span>
                    </td>

                    <td align="left" rowspan="2" class="td-border" width="400px" style="padding: 10px;">
                      <div>
                        <span>Received the above in good condition</span>
                        <br><br>
                        <label><b>Name &nbsp;&nbsp; :</b></label>
                        <span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Sign &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                        <span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Contact No &nbsp; :</b></label>
                        <span class="inp-field"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                        <br><br>
                        <label><b>Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b></label>
                        <span class="inp-field"><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
                      </div>
                  </tr>

                  <tr>
                    <td align="left" class="" height="80px" width="300px">
                      <span><b>For <?=/* @$customer_data['user_company_name']*/ @$setval["setting_company_name"] ?></b></span>
                    </td>
                  </tr>
            </table>




          <!-- <table class="table_align">

            <tr>


              <td width="730px"  style="text-align:center; background-color:#a6a6a6;"><h2>Financial Details</h2></td>

            </tr>

            

            <tr>

              <td colspan="2" style="text-align:center; "><h3 style="border-bottom: 1px solid black;">Smartbooks <span style="color:red;">(UAE)</span> – Queue Management System </h3></td>

            </tr>

            <br>

            <tr>

              <td colspan="2" style="text-align:center;color:blue;"><h3>(Product of Canada)</h3></td>

            </tr>

           

          </table>

          <br><br>

          <table style="border-collapse: collapse;" class="table_align">

            <thead>

              <tr>


                <th  style="border: 1px solid #adadad;background-color:#3c3d3a;color:#fff;" height="30px" width="40px" >#</th>

                <th style="border: 1px solid #adadad;background-color:#3c3d3a;color:#fff;" height="30px" width="150px">Product Name</th>

                <th style="border: 1px solid #adadad;background-color:#3c3d3a;color:#fff;" height="30px" width="400px">Product Description</th>

                <th style="border: 1px solid #adadad;background-color:#3c3d3a;color:#fff;" height="30px" width="50px">Qty</th>

              </tr>

            </thead>

            <tbody>
              <?php
               $subtotal = 0;
               // print_b($quotation_detail_data);
               if (isset($deliver_note_detail) && @$deliver_note_detail != null) {
                 $current_img = '';
                 $s_no = 0;

                foreach ($deliver_note_detail as $k => $v) {

                  $s_no++;
                  
                    
                    $file_path = dir_path().$this->image_upload_dir.'/'.@$v['product_image'];
                    if (!is_dir($file_path) && file_exists($file_path)) {
                      $current_img = site_url($this->image_upload_dir.'/'.$v['product_image']);
                    }else{
                      $current_img = site_url("uploads/dummy.jpg");
                    }
                 ?>
                    <tr>


                      <td style="border: 1px solid black; text-align:center;" height="30px"><?= $s_no;?></td>

                      <td style="border: 1px solid black;padding-left: 5px; "><label><?= $v['product_name'];?></label></td>

                      <td style="border: 1px solid black;padding-left: 5px; "><label><?= $v['product_description'];?></label></td>

                      <td style="border: 1px solid black;  "><center><?= $v['delivery_note_detail_quantity'];?></center></td>

                     
                    </tr>
                 <?php
                      $subtotal += $v['delivery_note_detail_quantity'] ;
                    
                  }
                }
               ?>
            </tbody>

          </table>

          <br>



          <table style="border-collapse: collapse;" class="table_align">

            <tr>


              <td style="border: 1px solid #adadad;background-color:#3c3d3a;color:#fff;padding-left: 5px;" width="590px">Total quantity</td>

              <td style="border: 1px solid #adadad;background-color:#3c3d3a;color:#fff;text-align:center;" width="50px" ><?= $subtotal; ?></td>


            </tr>

          </table> -->
