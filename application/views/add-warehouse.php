<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':'Edit'; ?>
                        <?= (isset($page_heading))?$page_heading:''; ?>
                </a>
            </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--    <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-4">
                                                <label class="form-label">Name:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="warehouse_name" type="text" value="<?= @$data['warehouse_name']; ?>" class="form-control" placeholder="Enter Name">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-4">
                                                <label class="form-label">Address:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                   <!--  <input name="warehouse_address" type="text" value="<?= @$data['warehouse_address']; ?>" class="form-control" placeholder="Enter Address"> -->
                                                     <textarea name="warehouse_address" type="textarea" class="form-control" placeholder="Enter Address"><?= @$data['warehouse_address']; ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-4">
                                                <label class="form-label">City:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="warehouse_city" type="text" value="<?= @$data['warehouse_city']; ?>" class="form-control" placeholder="Enter City">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-4">
                                                <label class="form-label">Country:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control input-signin-mystyle select2" id="warehouse_country" name="warehouse_country">
                                                      <option value="" selected="" disabled="">-- Select Country --</option>
                                                        <?php
                                                        foreach ($countries as $k => $v) {
                                                        ?>
                                                            <option value="<?= $v['country_id'] ?>" <?=( $v['country_id']==@ $data['warehouse_country']) ? 'selected' : ''; ?> >
                                                                <?= $v['country_name']; ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-4">
                                                <label class="form-label">No. Of Shelves:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="warehouse_no_of_shelf" type="text" value="<?= @$data['warehouse_no_of_shelf']; ?>" class="form-control" placeholder="Enter No. Of Shelf">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-4">
                                                <label class="form-label">No. Of Sections:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="warehouse_no_of_section" type="text" value="<?= @$data['warehouse_no_of_section']; ?>" class="form-control" placeholder="Enter No. Of Section">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-4">
                                                <label class="form-label">Status:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select class="form-control input-signin-mystyle select2" id="warehouse_is_active" name="warehouse_is_active">
                                                        <option value="" selected="selected" disabled="">- Select Status -</option>
                                                        <option value="1"  <?= ($page_title == 'add')?'selected':''?> <?=( @$data['warehouse_is_active']==1 )? 'selected': ''; ?> >Enabled</option>
                                                        <option value="0" <?=( @$data[ 'warehouse_is_active']==0 && $page_title != 'add')? 'selected': ''; ?>>Disabled</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-offset-3 col-md-6">
                                    <div class="form-group">
                                        <?php 
                                            if(!empty($data['warehouse_id']) && @$data['warehouse_id'] != null){
                                                $appendedFiles = array();
                                                $file_path = dir_path().$image_upload_dir.'/'.@$data['warehouse_image'];
                                                  if (!is_dir($file_path) && file_exists($file_path)) {
                                                    // $current_img = site_url($image_upload_dir."/".$categoryData['warehouse_image']);
                                                    @$file_details = getimagesize(site_url($image_upload_dir."/".@$data['warehouse_image']));
                                                    $appendedFiles[] = array(
                                                      "name" => @$data['warehouse_image'],
                                                      "type" => $file_details['mime'],
                                                      "size" => filesize($file_path),
                                                      "file" => site_url($image_upload_dir."/". @$data['warehouse_image']),
                                                      "data" => array(
                                                        "url" => site_url($image_upload_dir."/". @$data['warehouse_image']),
                                                        "image_file_id" => $data['warehouse_id']
                                                      )
                                                    );
                                                  }
                                                $appendedFiles = json_encode($appendedFiles);
                                            }
                                        ?>
                                            <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files='<?php echo @$appendedFiles;?>'>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <input name="id" type="hidden" value="<?= @$data['warehouse_id']; ?>">
                                        <input type="hidden" name="image_old" value="<?= @$data['warehouse_image']; ?>">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter my-bttn" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <!--   <button class="btn btn-success btn-cons" type="submit"><?php // ($page_title == 'add')?'Add New':'Edit'; ?> Ware House</button>-->
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {
        $('#fileUploader').fileuploader({
            changeInput: '<div class="fileuploader-input">' +
                '<div class="fileuploader-input-inner">' +
                '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                '<p>or</p>' +
                '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                '</div>' +
                '</div>',
            theme: 'dragdrop',
            limit: 1,
            extensions: ['jpg', 'jpeg', 'png'],
               onRemove: function(item) {
                 $.post('<?= site_url('warehouse/imageDelete'); ?>', {
                 file: item.name,
                 data: {
                   image_file_id:"<?= @$data['warehouse_id']; ?>",
                   file:item.name,
                   image_post_file_id:item.data.image_file_id
                 }
                 });
               },
            captions: {
                feedback: 'Drag and drop files here',
                feedback2: 'Drag and drop files here',
                drop: 'Drag and drop files here'
            },
        });
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("form.validate").validate({
            rules: {
                warehouse_name: {
                    required: true
                },
                warehouse_address: {
                    required: true
                },
                warehouse_city: {
                    required: true
                },
                warehouse_country: {
                    required: true
                },
                warehouse_no_of_shelf: {
                    required: true,
                    digits: true
                },
                warehouse_no_of_section: {
                    required: true,
                    digits: true
                },
                warehouse_is_active: {
                    required: true
                }
            },
            messages: {
                warehouse_name: "This field is required.",
                warehouse_address: "This field is required.",
                warehouse_city: "This field is required.",
                warehouse_country: "This field is required.",
                warehouse_no_of_shelf:{
                    required: "This field is required.",
                    digits: "Please enter only digits."
                },
                warehouse_no_of_section:{
                    required: "This field is required.",
                    digits: "Please enter only digits."
                },
                warehouse_is_active: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit    
                error("Please input all the mandatory values marked as red");

            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
</script>