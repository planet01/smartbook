
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      <!--<div class="page-title"> <i class="icon-custom-left"></i>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4><?= ($page_title == 'add')?'Add New':''; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Profile | <?= $id; ?></span></h4>
                </div>
                <div class="grid-body no-border"> <br>
                   <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Type</label>
                                </div>
                                <div class="col-md-10">
                                      <div class="radio radio-success">
                                        <input id="good" type="radio" name="type" value="Good" checked="checked">
                                        <label for="good">Good</label>
                                        <input id="bad" type="radio" name="type" value="Bad">
                                        <label for="bad">Bad</label>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Name</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <input name="name"  type="text" value="<?= @$data['name']; ?>"  class="form-control" placeholder="Name">
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                          
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Unit <i class=" fa fa-question-circle"></i></label>
                                </div>
                                <div class="col-md-10">
                                  <select name="unit"  class="select2 form-control"  >
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                  </select>
                                </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-12">
                                  <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <div class="checkbox check-success 	">
                                        <input id="sales" type="checkbox" value="1" checked="checked">
                                        <label for="sales">Sales Information</label>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Rate</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <input name="rate"  type="text" value="<?= @$data['rate']; ?>"  class="form-control" placeholder="">
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Account</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <select name="account"  class="select2 form-control"  >
                                        <option value="1">Sales</option>
                                        <option value="2">Etc</option>
                                      </select>
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Description</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <textarea name="description" class="form-control" placeholder="Description" ><?= @$data['description']; ?></textarea>
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Tax <i class=" fa fa-question-circle"></i></label>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <select name="account"  class="select2 form-control"  >
                                        <option value="1">Standard Rate [5%]</option>
                                        <option value="2">Etc</option>
                                      </select>
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-12">
                                  <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <div class="checkbox check-success 	">
                                        <input id="saless" type="checkbox" value="1" checked="checked">
                                        <label for="saless">Track Inventory for this item</label>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Opening Stock</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <input name="stock"  type="text" value="<?= @$data['stock']; ?>"  class="form-control" placeholder="">
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-2">
                                  <label class="form-label">Reorder Point</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <input name="point"  type="text" value="<?= @$data['point']; ?>"  class="form-control" placeholder="">
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <button class="btn btn-success btn-cons pull-right my-bttn" type="submit">Submit</button>
                          </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6" >
                        <div class="form-group">
                          <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files=''>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script>
  
  $(document).ready(function() {
    $("#user_type").change(function () {
      var val = $(this).val();
      if (val == 2) {
        $('.myUserType').slideDown();
      }else{
        $('.myUserType').slideUp();
      }
    });
    $(function(){
        $(".select2").select2();
    });
    $('.datepicker').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true
    });
    var max_fields      = 6; 
    var wrapper         = $(".amenities_field");
    var add_button      = $(".amenities_field_button");
    var x = 1; 
    $(add_button).click(function(e){
        e.preventDefault();
        $('form.validate').validate();
        if(x < max_fields){
            x++;
            $('.amenities_field').append('<div class="row form-row"> <div class="col-md-6"> <div class="form-row"> <label class="loc_name">Location Name</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="loc_name" name="loc_name[]" class="form-control" placeholder="Enter Location Name" > </div></div></div><div class="col-md-6"> <div class="form-row"> <label class="location">Location</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="location" name="location[]" class="form-control" placeholder="Enter Location" > </div></div></div><div class="col-md-6"> <div class="form-row"> <label class="ad_phone">Phone</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="ad_phone" name="ad_phone[]" class="form-control" placeholder="Enter Phone" > </div></div></div><div class=col-md-12><span class=pull-right><a class="btn btn-danger remove_field "href=# title=remove_field>Remove <i class="fa fa-times ml-10 "></i></a></span></div></div>');
        }else{
            alert("Extra Max Limit Are 6");
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent().parent().parent().remove(); x--;
    });
    $('#fileUploader').fileuploader({
      changeInput: '<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                          '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                        '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                        '<p>or</p>' +
                        '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                      '</div>' +
                    '</div>',
      theme: 'dragdrop',
      // limit: 4,
      // extensions: ['jpg', 'jpeg', 'png', 'gif'],
      onRemove: function(item) {
        
      },
      captions: {
              feedback: 'Drag and drop files here',
              feedback2: 'Drag and drop files here',
              drop: 'Drag and drop files here'
          },
    });
  });
</script>
<?php 
if (isset($data['id']) && @$data['id'] != null) {
?>
<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        fname:{
          required: true
        },
        lname:{
          required: true
        },
        email:{
          required: true,
          email: true
        },
        date_of_birth:{
          required: true
        },
        nationality:{
          required: true
        },
        phone:{
          required: true
        },
        price:{
          required: true,
          number:true
        },
        about:{
          required: true
        },
        user_type:{
          required: true
        },
        'category[]':{
          required: true
        },
        'loc_name[]':{
          required: true
        },
        'location[]':{
          required: true
        },
        'ad_phone[]':{
          required: true
        },
        status:{
          required: true
        }
      }, 
      messages: {
        fname: "This field is required.",
        lname: "This field is required.",
        email:{
          required: "This field is required.",
          email: "Invalid E-mail Address"
        },
        date_of_birth: "This field is required.",
        nationality: "This field is required.",
        phone: "This field is required.",
        price:{
          required: "This field is required.",
          number: "Please Insert Number"
        },
        about: "This field is required.",
        user_type: "This field is required.",
        'category[]': "This field is required.",
        'loc_name[]': "This field is required.",
        'location[]': "This field is required.",
        'ad_phone[]': "This field is required.",
        status: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    
  });
</script>
<?php }else{ ?>
<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        fname:{
          required: true
        },
        lname:{
          required: true
        },
        email:{
          required: true,
          email: true
        },
        password:{
          required: true
        },
        re_password:{
          required: true,
          equalTo: '#password'
        },
        date_of_birth:{
          required: true
        },
        nationality:{
          required: true
        },
        phone:{
          required: true
        },
        price:{
          required: true,
          number:true
        },
        about:{
          required: true
        },
        'category[]':{
          required: true
        },
        'loc_name[]':{
          required: true
        },
        'location[]':{
          required: true
        },
        'ad_phone[]':{
          required: true
        },
        user_type:{
          required: true
        },
        status:{
          required: true
        }
      }, 
      messages: {
        fname: "This field is required.",
        lname: "This field is required.",
        email:{
          required: "This field is required.",
          email: "Invalid E-mail Address"
        },
        password: "This field is required.",
        re_password:{
          required: "This field is required.",
          equalTo: "Password Not Match To Confirm Password"
        },
        date_of_birth: "This field is required.",
        nationality: "This field is required.",
        phone: "This field is required.",
        price:{
          required: "This field is required.",
          number: "Please Insert Number"
        },
        about: "This field is required.", 
        user_type: "This field is required.",
        'category[]': "This field is required.",
        'loc_name[]': "This field is required.",
        'location[]': "This field is required.",
        'ad_phone[]': "This field is required.",
        status: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
  });
</script>
<?php } ?>