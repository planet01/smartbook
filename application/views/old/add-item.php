
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      <!--<div class="page-title"> <i class="icon-custom-left"></i>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border">
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border"> <br>
                  <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Code</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="code"  type="text" value="<?= @$data['code']; ?>"  class="form-control" placeholder="Name">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                              <div class="row form-row">
                                <div class="col-md-4">
                                  <label class="form-label">Description</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-with-icon right controls">
                                      <i class=""></i>
                                      <textarea name="description" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                  
                                </div>
                              </div>
                          </div>
                       <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Detail 1</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="detail_1"  type="text" value="<?= @$data['detail_1']; ?>"  class="form-control" placeholder="Detail 1">
                                  </div>
                                
                              </div>
                            </div>
                        </div><div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Details 2</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="detail_2"  type="text" value="<?= @$data['detail_2']; ?>"  class="form-control" placeholder="Detail 2">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Retail Price</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="retail_price"  type="text" value="<?= @$data['retail_price']; ?>"  class="form-control" placeholder="Retail Price">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Reseller price</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="reseller_price"  type="text" value="<?= @$data['reseller_price']; ?>"  class="form-control" placeholder="Reseller price">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Reseller Max Discount</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="reseller_maxdiscount"  type="text" value="<?= @$data['reseller_maxdiscount']; ?>"  class="form-control" placeholder="Reseller Max Discount">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Gold Partner Price</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="goldpartner_price"  type="text" value="<?= @$data['goldpartner_price']; ?>"  class="form-control" placeholder="Gold Partner Price">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Gold Partner Max Discount</label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="goldpartner_maxdiscount"  type="text" value="<?= @$data['goldpartner_maxdiscount']; ?>"  class="form-control" placeholder="Gold Partner Max Discount">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Level 1 Notification </label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="level1_notification"  value="<?= @$data['level1_notification']; ?>"  type="text" class="form-control auto" data-v-max="9999" data-v-min="0" placeholder="Level 1 Notification">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Level 2 Notification </label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="level2_notification"  value="<?= @$data['level2_notification']; ?>" type="text" class="form-control auto" data-v-max="9999" data-v-min="0" placeholder="Level 2 Notification">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Level 3 Notification </label>
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="level3_notification"  value="<?= @$data['level3_notification']; ?>" type="text" class="form-control auto" data-v-max="9999" data-v-min="0" placeholder="Level 3 Notification">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-md-4">
                                <label class="form-label">Status</label>
                                 
                              </div>
                              <div class="col-md-8">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select class="form-control input-signin-mystyle select2" id="status" name="status">
                              <option value="" selected="selected" disabled="">- Select Status -</option>
                              <option value="1" <?= (@$data['is_active'] == 1)?'selected':''; ?>>Enabled</option>
                              <option value="0" <?= (@$data['is_active'] == 0)?'selected':''; ?>>Disabled</option>
                            </select>
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                       
                        
                      </div>
                      
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3" >
                            <div class="form-group">
                              <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files=''>
                            </div>
                           
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-md-offset-5" >
                            <div class="form-group">
                              <button class="btn btn-success btn-cons pull-right my-btn" type="submit">Add Item</button>
                            </div>
                        </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script>
  
  $(document).ready(function() {
    $("#user_type").change(function () {
      var val = $(this).val();
      if (val == 2) {
        $('.myUserType').slideDown();
      }else{
        $('.myUserType').slideUp();
      }
    });
    $(function(){
        $(".select2").select2();
    });
    $('.datepicker').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true
    });
    var max_fields      = 6; 
    var wrapper         = $(".amenities_field");
    var add_button      = $(".amenities_field_button");
    var x = 1; 
    $(add_button).click(function(e){
        e.preventDefault();
        $('form.validate').validate();
        if(x < max_fields){
            x++;
            $('.amenities_field').append('<div class="row form-row"> <div class="col-md-6"> <div class="form-row"> <label class="loc_name">Location Name</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="loc_name" name="loc_name[]" class="form-control" placeholder="Enter Location Name" > </div></div></div><div class="col-md-6"> <div class="form-row"> <label class="location">Location</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="location" name="location[]" class="form-control" placeholder="Enter Location" > </div></div></div><div class="col-md-6"> <div class="form-row"> <label class="ad_phone">Phone</label> <div class="input-with-icon right controls"> <i class=""></i> <input type="text" id="ad_phone" name="ad_phone[]" class="form-control" placeholder="Enter Phone" > </div></div></div><div class=col-md-12><span class=pull-right><a class="btn btn-danger remove_field "href=# title=remove_field>Remove <i class="fa fa-times ml-10 "></i></a></span></div></div>');
        }else{
            alert("Extra Max Limit Are 6");
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        $(this).parent().parent().parent().remove(); x--;
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("form.validate").validate({
      rules: {
        code:{
          required: true
        },
        description:{
          required: true
        },
        detail_1:{
          required: true,
          email: true
        },
        detail_2:{
          required: true
        },
        retail_price:{
          required: true,
          equalTo: '#password'
        },
        reseller_price:{
          required: true
        },
        reseller_maxdiscount:{
          required: true
        },
        goldpartner_price:{
          required: true
        },
        goldpartner_maxdiscount:{
          required: true,
          number:true
        },
        level1_notification:{
          required: true
        },
        level2_notification:{
          required: true
        },
        level3_notification:{
          required: true
        },
        status:{
          required: true
        },
        fileToUpload:{
            required: true
        }
      }, 
      messages: {
        code: "This field is required.",
        lname: "This field is required.",
        email:{
          required: "This field is required.",
          email: "Invalid E-mail Address"
        },
        password: "This field is required.",
        re_password:{
          required: "This field is required.",
          equalTo: "Password Not Match To Confirm Password"
        },
        date_of_birth: "This field is required.",
        nationality: "This field is required.",
        phone: "This field is required.",
        price:{
          required: "This field is required.",
          number: "Please Insert Number"
        },
        about: "This field is required.", 
        user_type: "This field is required.",
        'category[]': "This field is required.",
        'loc_name[]': "This field is required.",
        'location[]': "This field is required.",
        'ad_phone[]': "This field is required.",
        status: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
    $('.select2', "form.validate").change(function () {
        $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
    $('#fileUploader').fileuploader({
      changeInput: '<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                          '<img src="<?= site_url(); ?>assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png">' +
                        '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
                        '<p>or</p>' +
                        '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                      '</div>' +
                    '</div>',
      theme: 'dragdrop',
      // limit: 4,
      // extensions: ['jpg', 'jpeg', 'png', 'gif'],
      onRemove: function(item) {
        
      },
      captions: {
              feedback: 'Drag and drop files here',
              feedback2: 'Drag and drop files here',
              drop: 'Drag and drop files here'
          },
    });
  });
</script>