  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <?php
      $check_add =false;
      if ($this->user_type == 2) {
        foreach ($this->user_role as $k => $v) {
          if ($v['module_id'] == 14) {
            if ($v['add'] == 1) {
              $check_add = true;
            }
          }
        }
      } else {
        $check_add = true;
      }
      ?>
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All AMC <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple ">
            <div class="grid-title">
              <h4>All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span> <span class="fa fa-sort-desc"></spam></h4>
              <!--<div class="pull-right">-->
              <!--  <a href="<?php echo site_url($add_product)?>" class="btn btn-danger"><i class="fa fa-plus"></i>  New </a>-->
              <!--  <a class="btn  " href="#"> <span class="fa  fa-gear"></span> </a>-->
              <!--  <a class="btn  dropdown-toggle " data-toggle="dropdown" href="#"> <span class="fa fa-align-justify"></span> </a>-->
              <!--  <ul class="dropdown-menu">-->
              <!--    <li><a href="#"><i class="fa  fa-download" aria-hidden="true"></i> Import Item </a></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Item </a></li>-->
              <!--    <li class="divider"></li>-->
              <!--    <li><a href="#"><i class="fa  fa-upload" aria-hidden="true"></i> Export Current View </a></li>-->
              <!--    <li><a href="#"><i class="fa fa-undo" aria-hidden="true"></i> Refresh List </a></li>-->
              <!--  </ul>-->
              <!--  <span class="h-seperate"></span>-->
              <!--  <a class="" data-toggle="dropdown" href="#"> <span class="fa -"></span> </a>-->
              <!--</div>-->
            </div>
            <div class="receivable-total-amount-div">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    
                    <div class="col-md-3 receivable-subdiv-amount">
                      <label class="text-center"><b>Total AMC Receivables</b></label>
                      <h3 class="text-center" style="font-weight: 600;">AED <?= $total_recv_amount?></h3>
                      <hr style="width:100%;text-align:right;margin-right:0;border: 3px solid lightblue;">
                    </div>

                    <div class="col-md-3 receivable-subdiv-amount">
                      <label class="text-center"><b>Total AMC Overdue</b></label>   
                      <h3 class="text-center" style="font-weight: 600;">AED <?= $total_unpaid_amount?></h3> 
                      <hr style="width:100%;text-align:right;margin-right:0;border: 3px solid red;">
                    </div>

                  </div>
                </div>
              </div>
            
            </div>
            <div class="grid-body amc_quot-dt-pad">
              <form id="report_form" class="validate" target="_blank"  action="<?= site_url($receivable_report)?>" method="get">
              <div class="row">
                <div class="col-md-4 col-lg-4">
                  <!-- <div class="row"> -->
                    <!-- <div class="col-md-9"> -->
                      <div class="form-group">
                        <div class="input-with-icon right controls">
                          <i class=""></i>
                          <div class="radio radio-success responsve-receivable-radio">
                            <input id="checkbox1" type="radio" name="invoice_type" value="0" <?= (@$data['invoice_type'] == 0)?'checked="checked"':''; ?>  >
                            <label class="" for="checkbox1"><b>Pending</b></label>
                                
                            <input id="checkbox2" type="radio" name="invoice_type" value="1" <?= (@$data['invoice_type'] == 1)?'checked="checked"':'';?> <?= (!isset($data['invoice_type']) ? 'checked="checked"' : '')?> >
                            <label class="" for="checkbox2"><b>All</b></label>      
                          </div> 
                        </div>
                      </div>
                    <!-- </div> -->
                  <!-- </div> -->
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="row">
                      <div class="col-md-6 col-lg-4 date_input">
                        <div class="form-group mg-10">
                          <div class="input-with-icon right controls">
                              <label for="checkbox1"><b>From Date</b></label>     
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 date_input">
                        <i class=""></i>
                        <input class="datepicker form-control  " type="text" name="date_from" id="date_from" value="<?= @$data['from_date']  ?>"  >
                      </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                  <div class="row">
                    <div class="col-md-4 col-lg-4 date_input">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label for="checkbox1"><b>To Date</b></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 date_input">
                      <i class=""></i>
                      <input class="datepicker form-control " type="text" id="date_to" name="date_to" value="<?= @$data['from_date']  ?>"  >     
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4 col-lg-4">
                  <div class="row">
                    <div class="col-md-3 col-lg-3">
                      <div class="form-group mg-10">
                        <div class="input-with-icon right controls">
                          <label>Customer</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-lg-9">
                      <i class=""></i>
                      <select name="customer_id" id="customer_id" class="select2 form-control">
                        <option value="0" selected <?= (@$data['invoice_status'] == 3)? '' : 'disabled'?>>--- Select Customer ---</option>
                        <?php
                        foreach($customerData as $k => $v){
                        ?>
                        <option value="<?= $v['user_id']; ?>" <?=( $v['user_id'] == @$data['customer_id'])? 'selected': ''; ?> >
                        <?= $v['user_name']; ?>
                        </option>
                        <?php
                        }
                        ?>
                      </select>  
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-lg-4 responsve-mt-10">
                  <div class="row">
                    <div class="col-md-9 col-lg-10">
                      <div class="form-group">
                         <input class="btn btn-success btn-cons  soa_btn" type="submit" value="Print Statement Of Accounts"> 
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>

              </div>
              </form>
              
              <table class="table dataTables_wrapper amc_quot-dtable-disp" id="example3"  >
                <thead>
                  <tr>
                    <th class="view_receivables-dt-th1">Sr.No.</th>
                    <th class="text-center view_receivables-dt-th2">Date</th>
                    <th class="text-center view_receivables-dt-th3">P.I # / Invoice #</th>
                    <th class="view_receivables-dt-th4">Customer Name</th>
                    <th class="text-center view_receivables-dt-th5">Net Amount</th>
                    <th class="text-center view_receivables-dt-th6">Total Recieved</th>
                    <th class="text-center view_receivables-dt-th7">Overdue Since</th>
                    <th class="text-center view_receivables-dt-th8">Overdue Amount</th>
                    <!-- <th>Create Date</th> -->
                    <th class="text-center view_receivables-dt-th9">Action</th>
                  </tr>
                </thead>
                <tbody>
                
                  <?php
                  $count = 1;
                  if (isset($data) && @$data != null) {
                    foreach(@$data as $v) {
                 ?>
                    <tr>
                    <td class="text-center"><?php echo $count; ?></td>
                    <td class="text-center"><?php echo date("Y-m-d", strtotime($v['invoice_date']))?></td>
                    <td class="text-center">
                      <?php
                      $print_url = $quotation_print.'?id='.@$v['invoice_id'].'&header_check=0&view=0';
                      ?>
                      <a href="<?= site_url($print_url)?>" target='_blank' >
                        <?= $v['invoice_no'];?>
                       </a>
                        
                      </td>
                    <td class=""><?= $v['user_company_name']; ?></td>
                    <td class="text-center" >
                      <a href="javascript:void(0)" class="find_payment" data-id="<?= $v['is_invoice'];?>" data-total-amount="<?= $v['total_amount_base'] ?>" data-total-received="<?= number_format((float)$v['paid_amount_base'], 2, '.', '') ?>" data-overdue-since="<?= $v['expiry_date_by_pyment_term']?>" data-overdue-amount="<?= $v['expiry_amount_by_pyment_term']?>" id="<?= $v['invoice_id'];?>" ><?= $v['total_amount_base'];?></a></td>
                    <td class="text-center"><?= number_format((float)$v['paid_amount_base'], 2, '.', ''); ?></td>
                    <td class="text-center"><?php echo $v['expiry_date_by_pyment_term'];?></td>
                    
                    <td class="text-center"><!-- <?= $v['amount_left_base'];?> --><?=                               number_format((float)$v['expiry_amount_by_pyment_term'], 2, '.', '')?></td>
                    
                    <td class="text-center amc_quot_dt-align-items">
                     <!--  <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['invoice_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a> -->
                     
                     <!--  <a href="<?php echo site_url($edit_payment.'/'.@$v['invoice_id'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a> -->
                      <?php if($check_add){?>
                      <a href="<?php echo site_url($add_payment.'?id='.@$v['invoice_id'].'&is_invoice='.@$v['is_invoice'].'&total_amount='.$v['total_amount_base'].'&toal_received='.number_format((float)$v['paid_amount_base'], 2, '.', '').'&overdue_since='.$v['expiry_date_by_pyment_term'].'&overdue_amount='.$v['expiry_amount_by_pyment_term'].'&pre_invoice_no='.$v['invoice_no'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Add">Add Payment</a>
                      <?php } ?>
                      <!-- <a href="<?= site_url($print.'?id='.@$v['customer_id'].'&header_check=0')?>" class="btn-info btn btn-sm " target='_blank'><i class="fa fa-print"></i></a>
                      <a href="#" class="btn-warning btn btn-sm send_mail" data-id="<?= @$v['invoice_id']; ?>" data-toggle="tooltip" title="Mail"><i class="fa fa-envelope"></i></a> -->
                      
                    </td>
                  </tr>
                  
                <?php
                  $count++;
                  }
                }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script>
      $("form.validate").validate({
      rules: {
        customer_id:{
          required: true
        },  
        date_from:{
          required: true
        }, 
        date_to:{
          required: true
        }, 
        
      }, 
      messages: {
        customer_id: "This field is required.",
        "product_id[]":"This field is required.",
        "invoice_detail_quantity[]":{
          required:"This field is required.",
          number:"Please Insert Number."
        },
        "discount_amount[]":"Please Insert Number.",
        total_discount_amount:"Please Insert Number.",
        invoice_no:{
          required: "This field is required.",
          digits: "Please enter only digits."
        },
        invoice_customer_note: "This field is required.",
        invoice_terms_conditions: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
          error("Please input all the mandatory values marked as red");
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
      $(document).ready(function() {
          $("#report_form").on("click", ".soa_btn", function(event){
              event.preventDefault();
              var customer_id = $("#customer_id").valid();
              if(!customer_id){
                return false;
              }
              var checked = $("input[name='invoice_type']:checked").val();
              if(checked == 1){
                var date_from = $("#date_from").valid();
                var date_to   = $("#date_to").valid();
                if(!date_from || !date_to){
                  return false;
                }
              }
              $("#report_form").submit();
          });
      });


      $(document).ready(function() {
        setTimeout(function(){ 
          $('input[type=radio][name=invoice_type]').trigger("change");
         }, 500);

        $('input[type=radio][name=invoice_type]').change(function() {
            var checked = $("input[name='invoice_type']:checked").val();
            if(checked == 1){
              $('.date_input').show();
            }else{
              $('.date_input').hide();
            }
        });
        $( ".datepicker" ).datepicker({
           format: "yyyy-mm-dd",
           autoclose: true
           
        });
        $( ".status" ).change(function() {
          $( "form" ).submit();
        });
        $(".find_payment").click(function(event) {
           var id = $(this).attr("id");
           var total_amount = $(this).data("total-amount");
           var total_received = $(this).data("total-received");
           var overdue_since = $(this).data("overdue-since");
           var overdue_amount = $(this).data("overdue-amount");
           var is_invoice = $(this).data('id');

            $.ajax({
                url: "<?= site_url($invoice_payment)?>",
                type: "POST",
                data: {'id': id, 'is_invoice' : is_invoice},
                success: function(data) {
                    if(data){
                      var html = "";
                      data = $.parseJSON(data);
                      for(var i=0; i<data.length; i++){
                        var count = i;
                        data[i]['s_no'] = count+1;
                        data[i]['total_amount'] = total_amount;
                        data[i]['total_received'] = total_received;
                        data[i]['overdue_since'] = overdue_since;
                        data[i]['overdue_amount'] = overdue_amount;
                        html += payment_template(data[i]);
                      }
                       $('#payment_body').html(html);
                       $('#payment_modal').modal('show');
                                     
                    };
                },
                error: function() {
                    alert('There is error while submit');
                }
            });   
        });
      });
      var quotation_prefix = '<?php echo @$setval["company_prefix"].@$setval["quotation_prfx"]; ?>';
      var invoice_prefix   = '<?php echo @$setval["company_prefix"].@$setval["invoice_prfx"]; ?>';
      var print_url         = '<?php echo @$setval["company_prefix"].@$setval["invoice_prfx"]; ?>';
      var payment_edit = "<?php echo site_url($edit_payment.'/') ?>";
      function payment_template(data){
        //var dt = new Date(data.payment_date);
        var dt = data.payment_date;
        var payment_amount = (isNaN(data.payment_amount) || data.payment_amount == null) ?  0.00 : parseFloat(data.payment_amount).toFixed(2);
        var payment_adjustment = (isNaN(data.payment_adjustment) || data.payment_adjustment == null) ?  0.00 : parseFloat(data.payment_adjustment).toFixed(2);
        /*var qot_no   = (data['quotation_revised_no'] > 0)? data['quotation_no']+'-R'+data['quotation_revised_no'] : data['quotation_no']+'-R0';*/

        var html  = '<tr>';
            html += '<td class="receivable-dt-td1">'+data.s_no+'</td>';
            html += '<td class="receivable-dt-td2">'+data.receipt_no+'</td>';
            html += '<td class="receivable-dt-td3">'+dt+'</td>';
            html += '<td class="receivable-dt-td4">';
            var prefix_data = (data.is_invoice == 0 || data.is_invoice == null)? window.quotation_prefix+data.invoice_no : window.invoice_prefix+data.invoice_no;
            html += prefix_data;
            html += '</td>';
            html += '<td class="receivable-dt-td5">'+payment_amount+'</td>';
            html += '<td class="receivable-dt-td6">'+payment_adjustment+'</td>';
            html += '<td class="receivable-dt-td7">';
            html += '<a href="'+window.payment_edit+data.amc_payment_id+'?total_amount='+data.total_amount+'&total_received='+data.total_received+'&overdue_since='+data.overdue_since+'&overdue_amount='+data.overdue_amount+'&is_invoice='+data.is_invoice+'&pre_invoice_no='+prefix_data+'" class="mb-2 mr-2 disableClick btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
            
            html += '<a class="page_reload btn btn-white mb-2 mr-2" href="<?= site_url(@$print) ?>?id='+data.amc_payment_id+'&header_check=0" target="_blank"><i class="fa fa-print"></i></a>';
            html += '<a href="<?php echo site_url(@$delete_payment.'/')?>'+data.amc_payment_id+'" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm mb-2"><i class="fa fa-times"></i></a>'; 
            html += '</td>';
            
            html += '</tr>';
        return html;
      }
        
    </script>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
    <?php include APPPATH.'views/include/receivable_payments_modal.php'; ?>
    <!-- /.modal -->
  </div>
