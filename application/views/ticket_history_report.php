<style>
   .div-controls {
      margin-left: 3%;
      margin-right: 3%;
   }

   .font-controls {
      font-size: 14px;
      font-family: TimesNewRoman;
      font-weight: none;
   }

   .main-heading {
      font-weight: none;
      font-family: TimesNewRoman;
      font-size: 20px;
   }

   .main-second-heading {
      font-size: 16px;
      letter-spacing: 3px;
      font-family: TimesNewRoman;
      font-weight: none;
   }

   .table_align {
      margin-left: 2%;
      margin-right: 2%;
   }

   .bottom-table-div {
      margin-left: 3%;
      margin-right: 3%;
   }

   .service-desc {
      background: green;
      display: inline-block;
      padding-left: 100px;
      padding-right: 100px;
      margin-left: 300px;
      margin-right: 300px;
      width: 400px;
   }
</style>

<div class="div-controls div-font-controls" style="text-align:center;padding-top:-15%;">
   <h3 class="main-heading" style="margin-bottom:10px">Smart Matrix General Trading LLC</h3>
   <h4 class="main-second-heading" style="margin-top:10px;margin-bottom:10px">Ticket Management</h4>
   <h4 class="main-second-heading" style="margin-top:10px;margin-bottom:10px">From Date: <?= $from_date ?> To Date: <?= $to_date ?></h4>
</div>



<table style="width:100%;border-collapse: collapse;border-top:2px solid #000;border-bottom:2px solid #000;margin-bottom:15%;" class="table_align">
   <!-- first record -->
   <tr>
      <th align="left" width="60px" style="padding:5px 0 5px 10px;border-bottom:2px solid #000;text-align:center;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>S.No.</b></th>
      <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Ticket No</b></th>
      <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Ticket Date</b></th>
      <th align="left" width="100px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Invoice No</b></th>
      <th align="left" width="200px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Customer /<br>End User</b></th>
      <th align="left" width="50px" style="padding:5px 0;border-bottom:2px solid #000;text-align:left;font-weight:700;font-family: TimesNewRoman;font-size: 12px;"><b>Status</b></th>
   </tr>
   <?php
   $i = 0;
   foreach ($data as $v) {
      $i++;
   ?>

      <tr>
         <td align="left" style="border-top: 1px solid black;border-bottom: 1px solid black;padding:5px 0 5px 10px;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $i; ?></td>
         <td align="left" style="border-top: 1px solid black;border-bottom: 1px solid black;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><b><?= $setting['company_prefix'] . $setting['ticket_prfx'] . $v['ticket_no'] ?></b></td>
         <td align="left" style="border-top: 1px solid black;border-bottom: 1px solid black;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $v['ticket_date'] ?></td>
         <td align="left" style="border-top: 1px solid black;border-bottom: 1px solid black;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $v['invoice_no'] ?></td>
         <td align="left" style="border-top: 1px solid black;border-bottom: 1px solid black;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;"><?= $v['user_name'] ?> /<br><?= $v['end_user_name'] ?></td>
         <td align="left" style="border-top: 1px solid black;border-bottom: 1px solid black;padding:5px 0;text-align:center;font-family: TimesNewRoman;font-size: 12px;">
            <?php

            if ($v['status'] == 1) {
               if ($v['TicketStatus'] == 0) {
                  echo 'Pending';
               } else if ($v['TicketStatus'] == 1) {
                  echo 'Partially';
               } else if ($v['TicketStatus'] == 2) {
                  echo 'Completed';
               } else if ($v['TicketStatus'] == 3) {
                  echo 'Pending By Client';
               } else {
                  echo 'Not Initiated';
               }
            } else if ($v['status'] == 2) {
               echo 'Accepted';
            } else if ($v['status'] == 3) {
               echo 'Rejected';
            }
            ?>
         </td>
      </tr>
      <?php
      foreach ($statusData as $v2) {
         if ($v2['ticket_id'] == $v['ticket_id']) {
      ?>
            <tr>
               <td colspan='6' style='border-bottom: 1px dotted black;padding-top:10px;padding-bottom:10px'>
                  
                  <span class="" style="">
                     Task Assigned To:
                     <?php
                     foreach ($employeeData as $v3) {
                        if ($v['assigned_to'] == $v3['user_id']) {
                           echo $v3['user_name'].',';
                        }
                     ?>

                     <?php
                     }
                     ?>
                  </span>
                  
                     &nbsp;&nbsp;&nbsp;&nbsp;<span class="" style="">Service Date: <?= $v2['service_date'].',' ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
                     &nbsp;&nbsp;&nbsp;&nbsp;<span class="">Time: <?= $v2['start_time'].',' ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                     &nbsp;&nbsp;&nbsp;&nbsp;<span class="">End Time: <?= $v2['end_time'].',' ?></span>&nbsp;&nbsp;&nbsp;&nbsp;
                     &nbsp;&nbsp;&nbsp;&nbsp;<span class="">No of Hours: <?= $v2['no_of_hours'].',' ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<br>
                     <span class="">Status:
                        <?php
                        if ($v2['status'] == 0) {
                           echo 'Pending';
                        } else if ($v2['status'] == 1) {
                           echo 'Partially';
                        } else if ($v2['status'] == 2) {
                           echo 'Completed';
                        } else if ($v2['status'] == 3) {
                           echo 'Pending By Client';
                        }
                        echo ',';
                        ?>
                     </span>
                     <span class="">Detail: <?= $v2['service_detail'] ?></span>
                  
                  
               </td>
            </tr>
      <?php
         }
      }
      ?>

   <?php } ?>


</table>