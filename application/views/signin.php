<div class="container">
  <div class="row login-container">
      <div class="text-center">
          <center><img src="http://smartbooks.planet01.net/assets/admin/img/logo.png" class="logo img-responsive" alt=""></center>
      </div>  
    <div class="col-md-offset-3 col-md-6">
     <h2>Sign In</h2>
	 <form class="ajaxForm validate" action="<?php echo site_url($this->add_product)?>" method="post">
	 <div class="row">
	 <div class="form-group col-md-12">
        <label class="form-label">E-mail</label>
        <div class="controls">
  			<div class="input-with-icon right">                                       
  				<i class=""></i>
  				<input type="text" name="user_email" class="form-control" placeholder="Enter E-mail">
  			</div>
        </div>
      </div>
      </div>
	  <div class="row">
      <div class="form-group col-md-12">
        <label class="form-label">Password</label>
        <span class="help"></span>
        <div class="controls">
			<div class="input-with-icon right">                                       
				<i class=""></i>
				<input type="password" name="user_password" class="form-control" placeholder="Enter Password">
			</div>
        </div>
      </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <button class="btn btn-success btn-cons  pull-right" type="submit">Login</button>
        </div>
      </div>
	  </form>
    </div>
  </div>
</div>
<script>

  $(document).ready(function() {

    $("form.validate").validate({
      rules: {
        user_email:{
          required: true,
          email: true
        },
        user_password:{
          required: true
        }
      }, 
      messages: {
        user_email:{
          required: "This field is required.",
          email: "Invalid E-mail Address"
        },
        user_password: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit    
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  

          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');

          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');

          
        }
        // submitHandler: function (form) {

        // }
      });

    });
  $('form.validate').change(function () {
        $('form.validate').valid(); //revalidate the chosen dropdown value and show error or success message for the input
    });
</script>


