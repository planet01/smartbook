<!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li>
          Receivables
        </li>
        <li>
          Payment
        </li>
        <li><a href="#" class="active"><?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
      </ul>
      <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
      <!--  <h3><?= ($page_title == 'add')?'Add New':'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
      <!--</div>-->
    <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
              <div class="grid simple">
                <div class="grid-title no-border"> 
                  <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                </div>
                <div class="grid-body no-border">
                  <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Receipt No</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input  readonly type="text" value="<?= ($page_title == 'add')?@$setval["company_prefix"].@$setval["receivable_prefix"].serial_no_receivable():@$setval["company_prefix"].@$setval["receivable_prefix"].@$data['receipt_no']; ?>"  class="form-control" placeholder="" readonly>
                                    <input type="hidden" name="receipt_no" value="<?= ($page_title == 'add')? serial_no_receivable() : @$data['receipt_no']; ?>" placeholder="">
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>


                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Invoice No</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input  type="text" value="<?= @$data['pre_invoice_no']; ?>"  class="form-control" placeholder="" readonly>
                                    <input name="invoice_no"  type="hidden" value="<?= @$data['invoice_no']; ?>"  class="form-control" placeholder="" readonly>
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group">
                            <div class="row ">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label">Customer Name</label>
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="customer_name"  type="text" value="<?= @$customer['user_company_name']; ?>"  class="form-control" placeholder="" readonly>
                                     <input name="payment_customer_id"  type="hidden" value="<?= @$customer['user_id']; ?>"  />
                                  </div>
                                
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Date</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="payment_date"  type="text" value="<?= @$data['payment_date']; ?>"  class="form-control datepicker" placeholder="" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Net Amount</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input  type="text" readonly value="<?= @$data['total_amount']; ?>" name="total_net_amount" class="form-control numinput" placeholder="" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Total Received</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-12">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input  type="text" readonly value="<?= @$data['toal_received']; ?>"  class="form-control numinput" placeholder="" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      
                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Overdue Amount</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input  type="text" readonly value="<?= @$data['overdue_amount']; ?>"  class="form-control numinput" placeholder="" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Cheque</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="payment_cheque"  type="text" value="<?= @$data['payment_cheque']; ?>"  class="form-control" placeholder="Enter Payment Cheque" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Amount</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="payment_amount"  type="text" value="<?= @$data['payment_amount']; ?>"  class="form-control rate_num" placeholder="Enter Payment Amount" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <!-- <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Tax</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-10">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input  type="text" name="tax" value="<?= @$data['invoice_tax']?>" class="form-control numinput" placeholder="" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div> -->

                      

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Adjustment Amount</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input name="payment_adjustment"  type="text" value="<?= @$data['payment_adjustment']; ?>"  class="form-control  rate_num" placeholder="" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                       <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Overdue Since</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <input  type="text" readonly value="<?= @$data['overdue_since']; ?>"  class="form-control numinput" placeholder="" >
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>


                      <!-- <div class="clearfix"></div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Credit Account</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select name="credit_account" id="credit_account" class="-control acc_name mb-3 select2" style="width: 100%;" required>
                                          <option selected disabled>--- Select Account ---</option>
                                          <?php foreach($accounts as $k => $v){ ?>
                                              <option <?= (@$data['credit_account'] == $v['AccountNo']?'Selected':'' ) ?> value="<?= $v['AccountNo']; ?>"> <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?> </option>
                                          <?php } ?>
                                      </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Debit Account</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-9">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <select name="debit_account" id="debit_account" class="-control acc_name mb-3 select2" style="width: 100%;" required>
                                          <option selected disabled>--- Select Account ---</option>
                                          <?php foreach($accounts as $k => $v){ ?>
                                              <option <?= (@$data['debit_account'] == $v['AccountNo']?'Selected':'' ) ?> value="<?= $v['AccountNo']; ?>"> <?= $v['AccountNo'].'-'.$v['AccountDesc']; ?> </option>
                                          <?php } ?>
                                      </select>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div> -->
                      
                      <div class="col-md-12">
                        <div class="form-group">
                            <div class="row form-row">
                              <div class="col-sm-12 col-md-12">
                                <label class="form-label my-label-style">Notes</label>
                                 
                              </div>
                              <div class="col-sm-12 col-md-12">
                                  <div class="input-with-icon right controls">
                                    <i class=""></i>
                                    <textarea  name="payment_notes" class="form-control numinput" placeholder="" ><?= @$data['payment_notes']; ?></textarea>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                      

                      <div class="clearfix"></div>
                      <div class="col-md-12">
                        <div class="form-group text-center">
                          <!--  <button class="btn btn-success btn-cons pull-right" type="submit"><?= ($page_title == 'add')?'Add New':'Edit'; ?> Category</button>-->
                          </br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"><?= ($page_title == 'add')?'':''; ?> Submit</button>
                          <input name="id"  type="hidden" value="<?= @$data['payment_id']; ?>">
                          <input name="invoice_id"  type="hidden" value="<?= @$data['invoice_id']; ?>">
                          <input name="is_invoice"  type="hidden" value="<?= @$data['is_invoice']; ?>">
                          <input name="from_invoice"  type="hidden" value="<?= @$_GET['from_invoice']; ?>">
                          
                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!-- END BASIC FORM ELEMENTS-->  
<script type="text/javascript">
  $(document).ready(function() {
    $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $("form.validate").validate({
      rules: {
        payment_date:{
          required: true
        },
        payment_amount:{
          required: true
        },
        payment_cheque:{
          required: true
        },
        invoice_no:{
          required: true
        },
       
      }, 
      messages: {
        payment_date: "This field is required.",
        payment_amount: "This field is required.",
        invoice_no: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
    // $('.select2', "form.validate").change(function () {
    //     $('fosrm.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    // });
  });
$(document).on('keypress', ".rate_num", function(e) {
        var x = e.which || e.keycode;
      if((x>=48 && x<=57) || x == 46){
          //console.log(true);
        return true;
      }else{
          e.preventDefault();
            //console.log(x);
        return false;
      }
});
</script>