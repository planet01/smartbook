<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">View All <?= (isset($page_heading)) ? $page_heading : ''; ?></a> </li>
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h4>
                        <?php
                        $location_add = false;
                        $location_edit = false;
                        $location_status = false;
                        $location_delete = false;
                        $location_history = false;
                        $check_print = false;
                        if ($this->user_type == 2) {
                            foreach ($this->user_role as $k => $v) {
                                if ($v['module_id'] == 23) {
                                    if ($v['add'] == 1) {
                                        $location_add = true;
                                    }
                                    if ($v['edit'] == 1) {
                                        $location_edit = true;
                                    }
                                    if ($v['status'] == 1) {
                                        $location_status = true;
                                    }
                                    if ($v['delete'] == 1) {
                                        $location_delete = true;
                                    }
                                    if ($v['history'] == 1) {
                                        $location_history = true;
                                    }
                                    if ($v['print'] == 1) {
                                        $check_print = true;
                                    }
                                }
                            }
                        } else {
                            $location_add = true;
                            $location_edit = true;
                            $location_status = true;
                            $location_delete = true;
                            $location_history = true;
                            $check_print = true;
                        }
                        ?>
                        <?php if ($location_add) { ?>
                            <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('voucher/add'); ?>">Add</a>
                        <?php } ?>
                    </div>
                    <div class="grid-body ">
                        <div class="row">
                            <form class="validate">
                                <div class="col-md-2 col-lg-2">
                                    <div class="form-group">
                                        <div class="input-with-icon right controls">
                                            <i class=""></i>
                                            <div class="radio radio-success responsve-receivable-radio">
                                                <input id="checkbox1" checked="true" type="radio" name="voucher_type" value="0">
                                                <label class="" for="checkbox1"><b>PV</b></label>

                                                <input id="checkbox2" type="radio" name="voucher_type" value="2">
                                                <label class="" for="checkbox2"><b>RV</b></label>

                                                <input id="checkbox3" type="radio" name="voucher_type" value="1">
                                                <label class="" for="checkbox3"><b>JV</b></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>From Date</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="from_date" id="from_date" value="<?= @$data['from_date']  ?>">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>To Date</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="to_date" id="to_date" value="<?= @$data['to_date']  ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2">
                                    <div class="row">
                                        <div class="col-md-3 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label for="checkbox1"><b>Detail</b></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <i class=""></i>
                                            <input class="form-control" type="text" name="detail" id="detail" >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 col-lg-2">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <table class="table" id="custom_table">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th width="200px">Transaction No</th>
                                    <th width="200px">Date</th>
                                    <th width="400px">Detail</th>
                                    <th width="120px">Create Date</th>
                                    <th>Status</th>
                                    <th width="300px">Action</th>
                                </tr>
                            </thead>
                            <tbody id="custom_row">
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach (@$data as $k => $v) {
                                ?>
                                        <tr>
                                            <td>
                                                <?php echo $count; ?>
                                            </td>
                                            <td>
                                                <?php
                                                $rev = ($v['voucher_revised_no'] > 0) ? '-R' . $v['voucher_revised_no'] : '';
                                                echo $v['TransactionNo'] . $rev;
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo date("Y-m-d", strtotime($v["Transaction_Date"])) ?>
                                            </td>
                                            <td>
                                                <?php echo $v['Transaction_Detail'] ?>
                                            </td>
                                            <td>
                                                <?php echo date("Y-m-d H:i:s a", strtotime($v["voucher_created_at"])) ?>
                                            </td>
                                            <td align="">
                                                <?php
                                                if ($v['Posted'] == 1) {
                                                ?>
                                                    <span class="label label-success">Posted</span>
                                                <?php
                                                } else {
                                                ?>
                                                    <span class="label label-danger">Un Posted</span>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                            <td class="">
                                                <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['voucher_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>
                                                <?php if ($location_edit) {
                                                    if ($v['Posted'] == 0) { ?>
                                                        <a href="<?php echo site_url($edit_product . '/' . @$v['voucher_id']) ?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <?php }
                                                } ?>
                                                <?php if ($location_status) {
                                                    if ($v['Posted'] == 0) {
                                                ?>

                                                        <a href="<?php echo site_url($status_product . '/' . @$v['voucher_id']) ?>" class=" btn btn-sm ajaxBtnAlter" style="background-color: #0aa61d;" data-toggle="tooltip" title="Edit"><b style="color: white;">Post</b></a>
                                                <?php }
                                                }
                                                ?>

                                                <?php if ($location_history) { ?>
                                                    <a href="#" class="btn-primary btn btn-sm myModalBtn" data-path="<?= $history ?>" data-id="<?= @$v['voucher_id']; ?>" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>
                                                <?php } ?>

                                                <?php if ($check_print || $check_visible) { ?>
                                                    <a href="<?= site_url('voucher/print_report?id=' . @$v['voucher_id'] . '&header_check=1&view=0&history=0') ?>" target='_blank' title="View Detail" data-id="<?= @$v['invoice_id']; ?>" class="btn-primary btn btn-sm"><i class="fa fa-print"></i></a>
                                                <?php } ?>
                                                <?php if ($location_delete) {
                                                    if ($v['Posted'] == 0) {
                                                ?>
                                                        <a href="<?php echo site_url($delete_product . '/' . @$v['voucher_id']) ?>" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm"><i class="fa fa-times"></i></a>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                <!--        <a href="<?php // echo site_url($delete_product.'/'.@$v['warehouse_id'])
                                                                        ?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a> -->
                                            </td>
                                        </tr>
                                <?php
                                        $count++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $( ".datepicker" ).datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $(document).on("click", "#fetch", function() {
            
            var from_date = $('#from_date').val();
            if (from_date == '') {
                from_date = 0;
            }

            var to_date = $('#to_date').val();
            if (to_date == '') {
                to_date = 0;
            }
            var detail = $('#detail').val();
            var voucher_type = $('[name="voucher_type"]:checked').val();
            $("#custom_row").find("tr").remove();
            $.ajax({
                url: "<?php echo site_url('Voucher/search_view'); ?>",
                dataType: "json",
                type: "POST",
                data: {
                    voucher_type: voucher_type,
                    from_date: from_date,
                    to_date: to_date,
                    detail : detail
                },
                cache: false,
                success: function(invoiceData) {
                    
                    custom_table.clear().draw();
                    var count =0;
                    for (var i = 0; i < invoiceData.data.length; i++) {
                        count++;
                        var html = '';
                        html += '<tr>';

                        html += '<td>';
                        html += count;
                        html += '</td>';

                        html += '<td>';
                        var $rev;
                        if (invoiceData.data[i].voucher_revised_no > 0) {
                            $rev = '-R' + invoiceData.data[i].voucher_revised_no
                        } else {
                            $rev = '';
                        }
                        
                        html += invoiceData.data[i].TransactionNo + $rev;
                        html += '</td>';

                        html += '<td>';
                        html += invoiceData.data[i].Transaction_Date;
                        html += '</td>';

                        html += '<td>';
                        html += invoiceData.data[i].Transaction_Detail;
                        html += '</td>';

                        html += '<td>';
                        html += invoiceData.data[i].voucher_created_at;
                        html += '</td>';

                        html += '<td>';
                        if(invoiceData.data[i].Posted == 1)
                        {
                            html += '<span class="label label-success">Posted</span>';
                        }                  
                        else
                        {
                            html += '<span class="label label-danger">Un Posted</span>';
                        }      
                        html += '</td>';

                        
                        html += '<td class="text-center">';
                        html += '<a href="#" class="btn-primary btn btn-sm myModalBtn mr-1" data-toggle="tooltip" title="View Detail" data-id="'+invoiceData.data[i].voucher_id+'" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a>';
                        html += '<?php if ($location_edit) {?>'; 
                        if (invoiceData.data[i].Posted == 0) { 
                            html += '<a href="<?php echo site_url($edit_product . "/") ?>'+invoiceData.data[i].voucher_id+'" class="btn-warning btn btn-sm mr-1" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                        }
                        html += '<?php } ?>';
                        html += '<?php if ($location_status) { ?>';
                            if (invoiceData.data[i].Posted == 0) { 
                                html += '<a href="<?php echo site_url($status_product . "/" ) ?>'+invoiceData.data[i].voucher_id+'" class=" btn btn-sm ajaxBtnAlter mr-1" style="background-color: #0aa61d;" data-toggle="tooltip" title="Edit"><b style="color: white;">Post</b></a>';
                            }
                        html += '<?php } ?>';
                        
                        html += '<?php if ($location_history) { ?><a href="#" class="btn-primary btn btn-sm myModalBtn mr-2" data-path="<?= $history ?>" data-id="'+invoiceData.data[i].voucher_id+'" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a><?php } ?>';
                        var print = 'voucher/print_report?id='+invoiceData.data[i].voucher_id+'&header_check=1&view=0&history=0';
                        html += '<?php if ($check_print || $check_visible) { ?><a href="<?= site_url() ?>'+print+'" target="_blank" title="View Detail" data-id="'+invoiceData.data[i].voucher_id+'" class=" mr-1 btn-primary btn btn-sm"><i class="fa fa-print"></i></a><?php } ?>';
                        var delete_url = 'voucher/delete/'+invoiceData.data[i].voucher_id;
                        html += '<?php if ($location_delete) { ?>';
                            if (invoiceData.data[i].Posted == 0) { 
                                html += '<a href="'+delete_url+'" rel="delete" class="ajaxBtnAlter btn-danger btn btn-sm mr-1"><i class="fa fa-times"></i></a>';
                            }
                        
                        html += '<?php } ?>';
                        
                        html += '</td>';
                        
                        html += '</tr>';
                        custom_table.row.add($(html)).draw(false);
                    }
                    // $('#custom_row').append(html);
                }
            });

        });
    });
</script>