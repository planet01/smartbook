<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Price Plan
            </li>
            <li><a href="#" class="active">View All <!-- <?= (isset($page_heading))?$page_heading:''; ?> --></a> </li>
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span> <span class="fa fa-sort-desc"></spam></h4>
                    </div>
                    <div class="grid-body ">
                        <table class="table" id="example3">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Price Id</th>
                                    <th>Description</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach(@$data as $k => $v) {
                                ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td>
                                            <?php echo $v['plan_slog']; ?>
                                        </td>
                                        <td>
                                            <?php echo $v['plan_description']; ?>
                                        </td>
                                        <td class="">
                                           <!--  <a href="#" class="btn-primary btn btn-sm myModalBtn" data-toggle="tooltip" title="View Detail" data-id="<?= @$v['product_id']; ?>" data-path="<?= $detail_product; ?>"><i class="fa fa-eye"></i></a> -->
                                            <a href="<?php echo site_url($edit_product.'/'.@$v['price_plan_id'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo site_url($delete_product.'/'.@$v['price_plan_id'])?>" rel="delete" class="ajax btn-danger btn btn-sm"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                    $count++; 
                                    }
                                } 
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>