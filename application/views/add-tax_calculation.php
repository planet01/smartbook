<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Tax Calculation
            </li>
            <li>
                <a href="#" class="active">
                    <?= ($page_title == 'add')?'Add New':'Edit'; ?> <!-- <?= (isset($page_heading))?$page_heading:''; ?> -->
                </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add')?'Add New':'Edit'; ?> <?= (isset($page_heading))?$page_heading:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product)?>" method="post" enctype="multipart/form-data">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Tax Date:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="tax_date"  type="text" value="<?= @$data['tax_date']; ?>" class="form-control datepicker" placeholder="Enter Tax Date">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Remarks:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="remarks" type="text" value="<?= @$data['remarks']; ?>" class="form-control" placeholder="Enter Remarks">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">Start Date:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="start_date" id="start_date" type="text" value="<?= @$data['start_date']; ?>" class="form-control datepicker" placeholder="Enter Start Date">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row form-row">
                                            <div class="col-md-12">
                                                <label class="form-label">End Date:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input name="end_date" id="end_date" type="text" value="<?= @$data['end_date']; ?>" class="form-control datepicker" placeholder="Enter End Date">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-with-icon right controls">
                                                    <button type="button" id="fetch_data" class="btn-warning btn btn-sm">
                                                        Fetch Data
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                                    
                                <div class="col-md-12 ">
                                    <h3>Tax Entries</h3>
                                      <div class="dataTables_wrapper-768">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>

                                              <!-- <th class="text-center cust_pay_term-dt-th1" >Add/Remove Row</th> -->
                                              <th class="text-center cust_pay_term-dt-th1" ></th>
                                              <th class="text-center cust_pay_term-dt-th2" >Invoice No</th>
                                              <th class="text-center cust_pay_term-dt-th3" >Invoice Date</th>
                                              <th class="text-center cust_pay_term-dt-th4" >Tax</th>
                                              <th class="text-center cust_pay_term-dt-th4" >Payment Recived</th>
                                            </tr>
                                          </thead>
                                          <tbody id="customFields">
                                            <!-- <tr class="txtMult">
                                              <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                              <td colspan="6"></td>
                                            </tr> -->
                                              <?php
                                              $x = 0;
                                              if(@$detail_data != null && !empty(@$detail_data)){
                                              foreach($detail_data as $v){
                                             ?>
                                                <tr class="txtMult">
                                                    <td>
                                                        <div class='checkbox'>
                                                        <input name='invoice_id_hd[]' type='hidden' value='<?= $v['invoice_id'];?>'>
                                                        <input type='checkbox' checked='checked' value='<?= $v['invoice_id'];?>'   class='invoice_id'  id='invoice_id<?= $x; ?>' name='invoice_id[]'  placeholder='' />
                                                        <label style='padding-left:25px' for='invoice_id<?= $x; ?>'>Include</label>
                                                        </div>
                                                    </td>
                                                    
                                                    <td>
                                                        <input type='text' name="invoice_no[]" readonly value='<?= $v['invoice_no']?>'>
                                                    </td>

                                                    <td>
                                                        <input type='text' name="invoice_date[]" readonly value='<?= $v['invoice_date']?>'>
                                                    </td>

                                                    <td>
                                                        <input type='text' name='tax[]' class='tax numeric-align' readonly value='<?= number_format((float)$v['tax'], 2, '.', '') ; ?>'>
                                                    </td>

                                                    <td>
                                                        <input type='text' name='paid_amount[]' class='paid_amount numeric-align' readonly value='<?= number_format((float)$v['paid_amount'], 2, '.', ''); ?>'>
                                                        <input type='hidden' name='is_amc[]' value='<?= $v['paid_amount']?>'>
                                                    </td>

                                                </tr>
                                            <?php } 
                                            } ?>

                                          </tbody>
                                          <tr>
                                                <th colspan="3" class="text-right" >Total</th>
                                                <th class="text-center"> 
                                                    <input type="text" readonly="readonly" class="numeric-align" id="grandtotaltax" name="totaltax" style="width:100%" value=""  placeholder="0"  /> 
                                                </th>
                                                
                                                <th class="text-center">
                                                    <input type="text" readonly="readonly" class="numeric-align" id="grandtotalpayment" name="totalpayment" style="width:100%" value=""  placeholder="0"  />
                                                </th>
                                                
                                          </tr>
                                        </table>
                                      </div>  
                                </div>
                              

                                <div class="col-md-12 mt-4">
                                    <div class="form-group text-center">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button">
                                            <?= ($page_title == 'add')?'':''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['tax_id']; ?>">
                                        <input name="tax_revised_no" type="hidden" value="<?= @$data['tax_revised_no']; ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script>
    $(document).ready(function() {
        
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {    
        jQuery.validator.addMethod("greaterThan", 
            function(value, element, params) {
                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) >= new Date($(params).val());
                }
                return isNaN(value) && isNaN($(params).val()) 
                    || (Number(value) > Number($(params).val())); 
            },'Must be greater than start date.');        
       $("form.validate").validate({

            rules: {
                tax_date: {
                    required: true
                },
                start_date: {
                    required: true,
                    
                },
                end_date: {
                    required: true,
                    greaterThan: "#start_date"  
                },
            },
            messages: {
                tax_date: "This field is required.",
                start_date: "This field is required.",
                
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit   
                error("Please input all the mandatory values marked as red");
 
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    
    $(document).ready(function(){

        
        var page = '<?php echo $page_title; ?>';
        var check_select = 0;
        if (page != "add") {
            check_select = 1;
            cal_total();
        }
        $(document).on('click', "#fetch_data", function() {

            var start = $('#start_date').valid();
            var end = $('#end_date').valid();
            if (!start || !end ) {
                return false;
            }
            
            if (check_select == 0) {
            check_select++;
            get_invoice();
            return true;
            }
            swal({
                title: "Are you sure, form will be reset.?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                get_invoice();
                } else {
                return false;
                }
            });
        });

    function get_invoice() {
    
        var start = $('#start_date').valid();
        var end = $('#end_date').valid();
        if (!start || !end ) {
            return false;
        }

        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        $('#customFields').empty();
        $.ajax({
            url: "<?php echo site_url('tax_calculation/invoice_data'); ?>",
            dataType: "json",
            type: "POST",
            data: {
                start_date: start_date,
                end_date: end_date,
            },
            cache: false,
            success: function(data) {
                var error = 1;
                var html = '';
                var x = 0;
                if (data) {
                if (data['invoice_data'] != '' && data['invoice_data'] != null) {
                    error = 0;
                    for (var i = 0; i < data['invoice_data'].length; i++) {
                        if(data['invoice_data'][i]['invoice_tax_amount'] > 0)
                        {

                            html += '<tr class="txtMult">';
                            html += '<td>';
                            html += "<div class='checkbox'>";
                            html += "<input name='invoice_id_hd[]' type='hidden' value='" + data['invoice_data'][i]['invoice_id'] + "'>";
                            html += "<input type='checkbox' checked='checked' value='" + data['invoice_data'][i]['invoice_id'] + "'   class='invoice_id'  id='invoice_id" + x + "' name='invoice_id[]'  placeholder='' />";
                            html += "<label style='padding-left:25px' for='invoice_id" + x + "'>Include</label>";
                            html += "</div>";
                            html += '</td>';
                            
                            html += '<td>';
                            html += "<input type='text' name='invoice_no[]' readonly value='"+data['invoice_data'][i]['invoice_no']+"'>";
                            html += '</td>';

                            html += '<td>';
                            html += "<input type='text' name='invoice_date[]' readonly value='"+data['invoice_data'][i]['invoice_date']+"'>";
                            html += '</td>';

                            html += '<td>';
                            html += "<input type='text' name='tax[]' class='tax numeric-align' readonly value='"+parseFloat(data['invoice_data'][i]['invoice_tax_amount']).toFixed(2)+"'>";
                            html += '</td>';

                            html += '<td>';
                            html += "<input type='text' name='paid_amount[]' class='paid_amount numeric-align' readonly value='"+parseFloat(data['invoice_data'][i]['paid_amount']).toFixed(2)+"'>";
                            html += "<input type='hidden' name='is_amc[]' value='"+data['invoice_data'][i]['is_amc']+"'>";
                            html += '</td>';

                            html += "</tr>";
                            
                            x++;
                        }
                    }
                    }
                    $("#customFields").append(html);
                    cal_total();
                }
                if (error == 1) {
                window.error("Data not found.");
                }
            }
        });
   }

   $(document).on('change', ".invoice_id", function() {
        
        cal_total();
    });
    
    function cal_total()
    {
        
        var total_tax = 0;
        var total_paid = 0;
        $("#customFields tr.txtMult").each(function() {
            var row = $(this).closest('tr'); 

            var invoice_id = row.find('.invoice_id').is(":checked");
            if(invoice_id)
            {
                var tax = row.find('.tax').val();   
                tax = (tax == undefined || tax == '') ? 0 : tax;
                total_tax += parseFloat(tax);

                var paid_amount = row.find('.paid_amount').val();
                paid_amount = (paid_amount == undefined || paid_amount == '') ? 0 : paid_amount;
                total_paid += parseFloat(paid_amount);
            }
             
        });
        $('#grandtotaltax').val(total_tax.toFixed(2));
        $('#grandtotalpayment').val(total_paid.toFixed(2));
    }
   
});
</script>