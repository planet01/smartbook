<!-- BEGIN PAGE CONTAINER-->
<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li>
                Follow Up
            </li>
            <li><a href="#" class="active"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?>
                    <!-- <?= (isset($page_heading)) ? $page_heading : ''; ?> -->
                </a> </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> - <span class="semi-bold"><?= (isset($page_heading)) ? $page_heading : ''; ?></span></h3>-->
        <!--</div>-->
        <!-- BEGIN BASIC FORM ELEMENTS-->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> <?= (isset($page_heading)) ? $page_heading : ''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="<?php echo site_url($add_product) ?>" method="post">
                            <div class="row">



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Doc Type</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <div class="radio radio-success responsve-radio">
                                                        <div style="display:inline;">
                                                            <input type="radio" class="follow_up_doc_type" id="checkbox6" name="follow_up_doc_type" value="6" <?= (@$data['follow_up_doc_type'] == 6) ? 'checked="checked"' : '' ?>>
                                                            <label for="checkbox6"><b>Inquiry Recieved History.</b></label>
                                                        </div>    
                                                        <div style="display:inline;">
                                                            <input type="radio" class="follow_up_doc_type" id="checkbox1" name="follow_up_doc_type" value="1" <?= (@$data['follow_up_doc_type'] == 1) ? 'checked="checked"' : '' ?>>
                                                            <label for="checkbox1"><b>Quotation</b></label>
                                                        </div>
                                                        <div style="display:inline;">
                                                            <input type="radio" class="follow_up_doc_type" id="checkbox2" name="follow_up_doc_type" value="2" <?= (@$data['follow_up_doc_type'] == 2) ? 'checked="checked"' : '' ?>>
                                                            <label for="checkbox2"><b>Proforma Invoice</b></label>
                                                        </div>
                                                        <div style="display:inline;">
                                                            <input type="radio" class="follow_up_doc_type" id="checkbox3" name="follow_up_doc_type" value="3" <?= (@$data['follow_up_doc_type'] == 3) ? 'checked="checked"' : '' ?>>
                                                            <label for="checkbox3"><b>Invoice No.</b></label>
                                                        </div>
                                                        <div style="display:inline;">
                                                            <input type="radio" class="follow_up_doc_type" id="checkbox4" name="follow_up_doc_type" value="4" <?= (@$data['follow_up_doc_type'] == 4) ? 'checked="checked"' : '' ?>>
                                                            <label for="checkbox4"><b>AMC Invoice No.</b></label>
                                                        </div>
                                                        <div style="display:inline;">
                                                            <input type="radio" class="follow_up_doc_type" id="checkbox5" name="follow_up_doc_type" value="5" <?= (@$data['follow_up_doc_type'] == 5) ? 'checked="checked"' : '' ?>>
                                                            <label for="checkbox5"><b>AMC Quotation No.</b></label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Doc No</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select name="follow_up_doc_id" id="follow_up_doc_id" class="custom_select" style="width:100%">

                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Date</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="datepicker" name="follow_up_date" value="<?= @$data['follow_up_date'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Customer Name</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="" readonly id="follow_up_company_name" name="follow_up_company_name" value="<?= ($page_title == 'add')? @$setting['setting_company_name']:@$data['follow_up_company_name'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Sales Person</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <select name="follow_up_sales_person" id="follow_up_sales_person" class="select2 form-control">
                                                    <option value="0" selected disabled>--- Select Sales Person ---</option>
                                                    <?php
                                                        foreach ($employee as $k => $v) {
                                                        ?>
                                                        <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['follow_up_sales_person']) ? 'selected' : ''; ?>>
                                                        <?= $v['user_name']; ?></option>
                                                    <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Contact Person</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="" name="follow_up_contact_person" id="follow_up_contact_person" value="<?= @$data['follow_up_contact_person'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Contact No</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <input type="text" class="" name="follow_up_contact_no" id="follow_up_contact_no" value="<?= @$data['follow_up_contact_no'] ?>" placeholder="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-sm-12 col-md-12">
                                                <label class="form-label">Remarks</label>
                                            </div>
                                            <div class="col-sm-12 col-md-9">
                                                <div class="input-with-icon right controls">
                                                    <i class=""></i>
                                                    <textarea name="follow_up_remarks" id="follow_up_remarks" rows="4" cols="50"><?= @$data['follow_up_remarks'] ?></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <!--  <button class="btn btn-success btn-cons pull-right" type="submit"><?= ($page_title == 'add') ? 'Add New' : 'Edit'; ?> Category</button>-->
                                        </br><button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="button"><?= ($page_title == 'add') ? '' : ''; ?> Submit</button>
                                        <input name="id" type="hidden" value="<?= @$data['follow_up_id']; ?>">

                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BASIC FORM ELEMENTS-->
<script type="text/javascript">
    $(document).ready(function() {

        $('.custom_select').select2({
            minimumInputLength: 4
        });

        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });

        $("form.validate").validate({
            rules: {
                follow_up_doc_type:
                {
                    required:true
                },
                follow_up_doc_id:
                {
                    required:true
                },
                follow_up_date: {
                    required: true
                },
                follow_up_company_name: {
                    required: true
                },
                follow_up_sales_person: {
                    required: true
                },
                follow_up_contact_person: {
                    required: true
                },
                follow_up_contact_no: {
                    required: true
                },
                follow_up_remarks: {
                    required: true
                },

            },
            messages: {
                payment_date: "This field is required.",
                payment_amount: "This field is required.",
                invoice_no: "This field is required."
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit 
                error("Please input all the mandatory values marked as red");

            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('error-control').addClass('success-control');

            }
            // submitHandler: function (form) {
            // }
        });
        // $('.select2', "form.validate").change(function () {
        //     $('fosrm.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        // });
    });
    find_invoices();
    function find_invoices()
    {
        $.ajax({
       url: "<?php echo site_url('Follow_Up/find_invoices'); ?>",
       dataType: "json",
       type: "POST",
       data: {
         doc_type: 0,
       },
       cache: false,
       success: function(data) {

            window.quotation = data.quotation;
            window.invoices = data.invoice;
            window.proforma_invoices = data.proforma_invoice;
            window.amc_quotation = data.amc_quotation;
            window.amc_invoice = data.amc_invoice;
            window.inquiry_history = data.inquiry_history;
       }
     });
    }
    $(document).on('change', ".follow_up_doc_type", function() {

        var invoice_type = $('input[name="follow_up_doc_type"]:checked').val();
        var data;
        html = "";
        var this_setting = JSON.parse("<?= addslashes(json_encode($setting)); ?>");
        var prefix = this_setting['company_prefix'];
        if(invoice_type == 1)
        {
            data =    window.quotation;
            prefix = prefix + this_setting['quotation_prfx'];
            html += "<option value='0' disabled selected>--Select Quotation--</option>"
        }
        else if(invoice_type == 2)
        {
            data =    window.proforma_invoices;
            prefix = prefix + this_setting['quotation_prfx'];
            html += "<option value='0' disabled selected>--Select Perfoma Invoice--</option>"
        }
        else if(invoice_type == 3)
        {
            data =    window.invoices;
            prefix = prefix + this_setting['invoice_prfx']
            html += "<option value='0' disabled selected>--Select Invoice--</option>"
        }
        else if(invoice_type == 4)
        {
            data =    window.amc_invoice;
            html += "<option value='0' disabled selected>--Select AMC Invoice--</option>"
        }
        else if(invoice_type == 5)
        {
            data =    window.amc_quotation;
            html += "<option value='0' disabled selected>--Select AMC Quotation--</option>"
        }else if(invoice_type == 6)
        {
            data =    window.inquiry_history;
            html += "<option value='0' disabled selected>--Select Inquiry Recieved History--</option>"
        }
        
        
        for (var i = 0; i < data.length; i++) {
            var title='';
            if(invoice_type == 4 || invoice_type == 5)
            {
                
                title = (data[i].amc_quotation_status == 4 || data[i].amc_quotation_status == 7)?'AMCINV-'+data[i].amc_quotation_no:this_setting['quotation_prfx'];
                title += (data[i].amc_quotation_revised_no > 0) ? data[i].amc_quotation_no + '-R' + data[i].amc_quotation_revised_no : data[i].amc_quotation_no;
            }
            else if(invoice_type == 6)
            {
                title = 'SM-INQ-'+data[i].invoice_no;
            }
            else
            {
                 title = (data[i].revised_no > 0) ? data[i].invoice_no + '-R' + data[i].revised_no : data[i].invoice_no;
            }
        
            if(invoice_type == 6)
            {
                html += "<option value='" + data[i].id + "' data-remarks='"+data[i].inquiry_received_history_remarks+"' data-contact_no='"+ data[i].inquiry_received_history_contact_no+"' data-contact_person='"+ data[i].inquiry_received_history_contact_person+"' data-company='"+ data[i].company+"' data-employee='"+ data[i].employee+"'>" + title + "</option>"
            }else
            {
                html += "<option value='" + data[i].id + "' data-company='"+ data[i].company+"'>" + prefix + title + "</option>"
            }
            
        
        }
        $('#follow_up_doc_id').html(html).trigger('change');
        
    });

    $(document).on('change', "#follow_up_doc_id", function() {
        var invoice_type = $('input[name="follow_up_doc_type"]:checked').val();
        if(invoice_type == 6)
        {
            var employee = $("#follow_up_doc_id option:selected").data("employee");
            var contact_person = $("#follow_up_doc_id option:selected").data("contact_person");
            var contact_no = $("#follow_up_doc_id option:selected").data("contact_no");
            var remarks = $("#follow_up_doc_id option:selected").data("remarks");
            $('#follow_up_sales_person').val(employee).trigger('change');
            $('#follow_up_contact_person').val(contact_person);
            $('#follow_up_contact_no').val(contact_no);
            $('#follow_up_remarks').val(remarks);
        }
        
            var company = $("#follow_up_doc_id option:selected").data("company");
            $('#follow_up_company_name').val(company);
        
        
    });
</script>