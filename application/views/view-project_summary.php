<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">View All <?= (isset($page_heading))?$page_heading:''; ?></a> </li>
        </ul>
        <!--<div class="page-title"> <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="active"><i class="icon-custom-left"></i></a>-->
        <!--  <h3>View All - <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h3>-->
        <!--</div>-->
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>View All <span class="semi-bold"><?= (isset($page_heading))?$page_heading:''; ?></span></h4>
                        <?php
                            $location_add = false;
                            $location_edit = false;
                            $location_status = false;
                            $location_delete = false;
                            $location_history = false;
                            $check_print = false;
                            if ($this->user_type == 2) {
                                foreach ($this->user_role as $k => $v) {
                                if($v['module_id'] == 32)
                                {
                                    if($v['add'] == 1)
                                    {
                                        $location_add = true;
                                    }
                                    if($v['edit'] == 1)
                                    {
                                        $location_edit = true;
                                    }
                                    if($v['status'] == 1)
                                    {
                                        $location_status = true;
                                    }
                                    if($v['delete'] == 1)
                                    {
                                        $location_delete = true;
                                    }
                                    if($v['history'] == 1)
                                    {
                                        $location_history = true;
                                    }
                                    if ($v['print'] == 1) {
                                        $check_print = true;
                                    }
                                }
                                }
                            }else
                            {
                                $location_add = true;
                                $location_edit = true;
                                $location_status = true;
                                $location_delete = true;
                                $location_history = true;
                                $check_print = true;
                            }
                        ?>
                        <?php if($location_add){?>
                        <a style="float: right;" class="btn btn-success btn-cons" href="<?= site_url('project_summary/add'); ?>">Add</a>
                        <?php } ?>
                       
                    </div>
                    <div class="grid-body ">
                    <div class="row">
                            <form class="validate">
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>From Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="from_date" id="from_date">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>To Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control datepicker" type="text" name="to_date" id="to_date" >
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                        <div class="col-md-4 date_input">
                                            <div class="form-group mg-10">
                                                <div class="input-with-icon right controls">
                                                    <label>Project Name</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 date_input">
                                            <i class=""></i>
                                            <input class="form-control" type="text" name="project_name" id="project_name" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group mg-10">
                                        <div class="input-with-icon right controls">
                                            <label>Project Status</label>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-lg-8">
                                        <i class=""></i>
                                        <select name="project_status" id="project_status" class="select2 form-control">
                                        <option value="0" selected disabled>--- Select Status ---</option>
                                        <?php
                                        foreach ($projectStatusData as $k => $v) {
                                        ?>
                                            <option value="<?= $v['status_id']; ?>">
                                            <?= $v['status_name']; ?>
                                            </option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-3 col-lg-3">
                                    <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group mg-10">
                                        <div class="input-with-icon right controls">
                                            <label>Customer</label>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-lg-8">
                                        <i class=""></i>
                                        <select name="customer_id" id="customer_id" class="select2 form-control">
                                        <option value="0" selected disabled>--- Select Customer ---</option>
                                        <?php
                                        foreach ($customerData as $k => $v) {
                                        ?>
                                            <option value="<?= $v['user_id']; ?>" <?= ($v['user_id'] == @$data['customer_id']) ? 'selected' : ''; ?>>
                                            <?= $v['user_name']; ?>
                                            </option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                    </div>
                                    </div>
                                </div> -->

                                <div class="col-md-2 col-lg-2">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                            <div class="form-group">
                                                <input class="btn btn-success btn-cons" id="fetch" type="button" value="Fetch">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <table class="table" id="custom_table">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th >Project Date</th>
                                    <th >Project Name</th>
                                    <th >Customer</th>
                                    <th >Technician</th>
                                    <th >Status</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($data) && @$data != null) {
                                    $count = 1;
                                    foreach(@$data as $k => $v) {
                                ?>
                                    <tr>

                                        <td>
                                            <?php echo $count; ?>
                                        </td>
                                        <td>
                                            <?= $v['project_date']; ?>
                                        </td>
                                        <td>
                                            <?php 
                                                echo $v['project_name'];
                                            ?>
                                        </td>
                                        <td>
                                        <?php 
                                            if($v['doc_type'] == 1) //PI
                                            {
                                                foreach(@$customerData as $k2 => $v2) 
                                                {
                                                    if($v2['user_id'] == $v['quotation_customer_id'])
                                                    {
                                                        echo $v2['user_name'];
                                                    }
                                                }
                                                
                                            }
                                            else
                                            {
                                                if($v['end_user'] == null || $v['end_user'] == 0)
                                                {
                                                    foreach(@$customerData as $k2 => $v2) 
                                                    {
                                                        if($v2['user_id'] == $v['invoice_customer_id'])
                                                        {
                                                            echo $v2['user_name'];
                                                        }
                                                    }
                                                }else
                                                {
                                                    foreach(@$endUserData as $k2 => $v2) 
                                                    {
                                                        if($v2['end_user_id'] == $v['end_user'])
                                                        {
                                                            echo $v2['end_user_name'];
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                            <br>
                                            <?php 
                                                if($v['doc_type'] == 1) //PI
                                                {
                                                    echo @$setval["company_prefix"] . @$setval["quotation_prfx"]; ?><?= ($v['quotation_revised_no'] > 0) ? @$v['quotation_no'] . '-R' . number_format(@$v['quotation_revised_no']) : @$v['quotation_no'];
                                                }else   //INvoice
                                                {
                                                    $rev = ($v['invoice_revised_no'] > 0)?'-R'.$v['invoice_revised_no'] : '';
                                                    $rev1 = ($v['invoice_status'] == 2)?'-'.$v['invoice_revised_no'] : '';
                                                    echo @$setval["company_prefix"].@$setval["invoice_prfx"]; ?><?php echo $v['invoice_no'].$rev ?> <br> <?= ($v['invoice_proforma_no'] != 0)?@$setval["company_prefix"].@$setval["quotation_prfx"].$v['invoice_proforma_no'].find_rev_no($v['invoice_proforma_no']):'';
                                                }
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            foreach(@$employeeData as $k2 => $v2) 
                                            {
                                                if($v['primary_techinician'] == $v2['user_id'])
                                                {
                                                    echo $v2['user_name'].',<br>';

                                                }
                                                if($v['secondary_techinician'] == $v2['user_id'])
                                                {
                                                    echo $v2['user_name'];
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <!-- <?php echo date("Y-m-d H:i:s a", strtotime($v["project_date"]))?> -->
                                            <?php
                                            if($v['status_id'] != 5){
                                                ?>
                                            <button class="btn btn-success btn-demo-space project_status" data-status-id="<?= $v['status_id']?>" data-id="<?= $v['project_id']; ?>"> <?= $v['status_name']?></button>
                                            <?php }
                                             else{
                                                ?>
                                                <button class="btn btn-success btn-demo-space" data-status-id="<?= $v['status_id']?>" data-id="<?= $v['project_id']; ?>"> <?= $v['status_name']?></button>
                                                <?php
                                            } ?>
                                        </td>
                                        <td class="">
                                            <a href="<?php echo site_url('project_summary/detail/'.@$v['project_id'])?>" class="btn-primary btn btn-sm" data-toggle="tooltip" title="View Detail"><i class="fa fa-eye"></i></a>
                                            <?php if($location_edit){
                                                if($v['status_id'] != 5){
                                                ?>
                                                <a href="<?php echo site_url('project_summary/edit/'.@$v['project_id'])?>" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <a href="" data-primary="<?= @$v['primary_techinician']?>" data-secondary="<?= @$v['secondary_techinician']?>" data-id="<?= @$v['project_id']?>" class="mr-1 btn-warning btn btn-sm edit_technician" data-toggle="tooltip" title="Add">Edit Technician</a>
                                            <?php }
                                                ?>
                                            
                                            <?php 
                                            }?>

                                            <?php if ($location_history){ ?>
                                            <a href="#" class="btn-primary btn btn-sm myModalBtn" data-path="Project_summary/view_status_history/" data-id="<?= @$v['project_id']; ?>" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>
                                            <?php }?>

                                            </td>
                                    </tr>
                                <?php
                                    $count++; 
                                    }
                                } 
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH . 'views/include/modal.php'; ?>
    <?php include APPPATH.'views/include/edit_project_modal.php';?>
    <?php include APPPATH.'views/include/project_status_modal.php';?>
        <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        var custom_table = $('#custom_table').DataTable();
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $(document).on("click", "#fetch", function() {

            var from_date = $('#from_date').val();
            if (from_date == '') {
                from_date = 0;
            }

            var to_date = $('#to_date').val();
            if (to_date == '') {
                to_date = 0;
            }
            var project_name = $('#project_name').val();
            var project_status = $('#project_status option:selected').val();
            
            $.ajax({
                url: "<?php echo site_url('Project_summary/search_view'); ?>",
                dataType: "json",
                type: "POST",
                data: {
                    from_date: from_date,
                    to_date: to_date,
                    project_name:project_name,
                    project_status:project_status
                },
                cache: false,
                success: function(result) {
                    custom_table.clear().draw();
                    var count = 0;
                    for (var i = 0; i < result.data.length; i++) {
                        count++;
                        var html = '';
                        html += '<tr>';

                        html += '<td >';
                        html += count;
                        html += '</td>';

                        html += '<td >';
                        html += result.data[i].project_date;
                        html += '</td>';

                        html += '<td >';
                        html += result.data[i].project_name;
                        html += '</td>';

                        html += '<td >';
                        html +=  result.data[i].user_name;
                        html += '</td>';

                        html += '<td >';
                        html += result.data[i].employee_name;
                        html += '</td>';

                        html += '<td >';
                        html += result.data[i].status;
                        html += '</td>';


                        html += '<td >';
                        html += '<a href="<?php echo site_url("project_summary/detail/")?>'+result.data[i].project_id+'" class="mr-1 btn-primary btn btn-sm" data-toggle="tooltip" title="View Detail"><i class="fa fa-eye"></i></a>';
                        html += '<?php if($location_edit){ ?>';
                            if(result.data[i].status_id != 5){
                            
                            html += '<a href="<?php echo site_url('project_summary/edit/')?>'+result.data[i].project_id+'" class="mr-1 btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>';
                            html += '<a href="" data-primary="'+result.data[i].primary_techinician+'" data-secondary="'+result.data[i].secondary_techinician+'" data-id="'+result.data[i].project_id+'" class="mr-1 btn-warning btn btn-sm edit_technician" data-toggle="tooltip" title="Add">Edit Technician</a>';
                            } 
                        
                            html += '<?php } ?>';

                            html += '<?php if ($location_history){ ?>';
                                html += '<a href="#" class="btn-primary btn btn-sm myModalBtn" data-path="Project_summary/view_status_history/" data-id="'+result.data[i].project_id+'" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>';
                                html += '<?php }?>';
                        html += '</td>';
                        html += '</tr>';
                        custom_table.row.add($(html)).draw(false);
                    }

                }
            });

        });
    });
</script>