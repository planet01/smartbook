<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>Dashboard</p>
            </li>
            <li><a href="#" class="active">Invoice Delivery Not Matching</a> </li>
            
        </ul>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        <h4>Invoice Delivery Not Matching</h4>
                    </div>
                    <div class="grid-body">
                        <form class="row validate" autocomplete="off" id="dashboard-form" style="margin-bottom:20px">
                            <div class="form-group col-sm-12 col-md-6 col-lg-3 date_input">
                                <label >From Date:</label>
                                <input type="text" name="fromdate" value="<?=@$fromdate?>" id="fromdate" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >To Date:</label>
                                <input type="text" name="todate" id="todate" value="<?=@$todate?>" class="datepicker form-control" >
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                                <label >Customer:</label>
                                <select name="customer_id" id="customer_id" class="form-control select2" >
                                    <option value>Select Customer</option>
                                    <?php
                                        foreach($customerData as $k => $v){
                                        ?>
                                        <option value="<?= $v['user_id']; ?>" <?= @$customer_id == $v['user_id']?'selected':''?> >
                                        <?= $v['user_name']; ?>
                                        </option>
                                        <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-sm-12 col-md-6 col-lg-3">
                            <button type="submit" class="btn btn-primary" style="margin-top:25px">Submit</button>
                            </div>
                            
                        </form>
                        <table class="table" id="example3" >
                            <thead>
                                <tr>
                                    <th class="prod_sr_no">Sr.No.</th>
                                    <th class="prod_name">Date</th>
                                    <th>Invoice #</th>
                                    <th>Item Name</th>
                                    <th>Total Qty.</th>
                                    <th>Pending Qty.</th>
                                    <th>Delivered Qty.</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                              foreach($delivery_note as $key =>$item){
                                $rev = ($item['invoice_revised_no'] > 0)?'-R'.$item['invoice_revised_no'] : '';
                                $rev1 = ($item['invoice_status'] == 2)?'-'.$item['invoice_revised_no'] : '';
                            ?>
                                <tr>
                                    <td><?= ++$key ?></td>
                                    <td><?= @$item['delivery_date'] ?></td>
                                    <td><?php echo @$setting["company_prefix"].@$setting["invoice_prfx"]; ?><?php echo $item['invoice_no'].$rev ?></td>
                                    <td><?= @$item['product_name'] ?></td>
                                    <td><?= @$item['total_quantity'] ?></td>
                                    <td><?= @$item['pending_quantity'] ?></td>
                                    <td><?= @$item['delivered_quantity'] ?></td>
                                    <td>
                                    <a class="btn btn-primary" href="<?= site_url($print.'?id='.@$item['invoice_id'].'&header_check=0')?>" target='_blank' ><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <?php
                              }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <?php include APPPATH.'views/include/modal.php'; ?>
        <!-- /.modal -->
</div>
<script>
    $(document).ready(function() {
        
        $( ".datepicker" ).datepicker({
           format: "dd-mm-yyyy",
           autoclose: true
           
        });
    });
    $("form.validate").validate({
      rules: { 
        fromdate:{
          required: true
        }, 
        todate:{
          required: true
        }, 
        
      }, 
      messages: {
        fromdate:"This field is required.",
        todate:"This field is required."
      },
        invalidHandler: function (event, validator) {
        //display error alert on form submit    
          error("Please input all the mandatory values marked as red");
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
      });
      $(document).ready(function() {
          $("#dashboard-form").on("submit", function(event){
              event.preventDefault();
              // var customer_id = $("#customer_id").valid();
              var date_from = $("#fromdate").valid();
              var date_to   = $("#todate").valid();
              if(!date_from || !date_to){
                return false;
              }
              else
              {
                document.getElementById('dashboard-form').submit();
              }
          });
      });
</script>